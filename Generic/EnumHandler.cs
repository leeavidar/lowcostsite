﻿using DL_Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    public class EnumHandler
    {
        public enum AirlineTypes
        {
            MarketingAirline = 1,
            OperatingAirline = 2,
        }
        /// <summary>
        /// שפות
        /// </summary>
        public enum Langeages
        {
            // Adding a language here requires adding the prefix in the "LanguagePrefix" enum.
            Hebrew = 1,
            English = 2,
        }

        /// <summary>
        /// קוד שפה 
        /// </summary>
        public enum LangeagePrefix
        {
            he = 1,
            en = 2,
        }


        /// <summary>
        /// חודשים
        /// </summary>
        public enum MonthNames
        {
            /// <summary>
            /// January
            /// </summary>
            January = 1,
            /// <summary>
            /// February
            /// </summary>
            February = 2,
            /// <summary>
            /// March
            /// </summary>
            March = 3,
            /// <summary>
            /// April
            /// </summary>
            April = 4,
            /// <summary>
            /// May
            /// </summary>
            May = 5,
            /// <summary>
            /// June
            /// </summary>
            June = 6,
            /// <summary>
            /// July
            /// </summary>
            July = 7,
            /// <summary>
            /// August
            /// </summary>
            August = 8,
            /// <summary>
            /// September
            /// </summary>
            September = 9,
            /// <summary>
            /// October
            /// </summary>
            October = 10,
            /// <summary>
            /// November
            /// </summary>
            November = 11,
            /// <summary>
            /// December
            /// </summary>
            December = 12,
        }

        /// <summary>
        /// Days Of Week Types
        /// </summary>
        public enum DaysOfWeek
        {
            /// <summary>
            /// Sunday
            /// </summary>
            Sunday = 1,
            /// <summary>
            /// Monday
            /// </summary>
            Monday = 2,
            /// <summary>
            /// Tuesday
            /// </summary>
            Tuesday = 3,
            /// <summary>
            /// Wednesday
            /// </summary>
            Wednesday = 4,
            /// <summary>
            /// Thursday
            /// </summary>
            Thursday = 5,
            /// <summary>
            /// Friday
            /// </summary>
            Friday = 6,
            /// <summary>
            /// Saturday
            /// </summary>
            Saturday = 7,
        }

        /// <summary>
        /// QueryString type you need to use in your site 
        /// in case of need for new QueryString type add his name here
        /// this enum used by generic function that retrieve the QueryString by his enum name
        /// </summary>
        public enum QueryStrings
        {
            URL,
            ID,
            DestinationPageId,
            CompanyPageId,
            Type,
            Export,
        }

        /// <summary>
        /// Flight types that you will need in order to get all the outward flights of an order, or all the return flights.
        /// </summary>
        public enum FlightTypes
        {
            Outward,
            Return // the capital 'R' is to differentiate from the reserved word 'return'
        }

        /// <summary>
        /// Types of check-in a passenger can use for an order.
        /// </summary>
        public enum CheckinType
        {
            Internet = 1,
            Airport = 2

        }


        public enum OrderStatus
        {
            All = 0,
            Succeeded = 1,
            BookingInProgress = 2,
            Failed = 3,
            Unconfirmed = 4,
            UnconfirmedBySupplier = 5,
            DuplicateBooking = 6,
        }
        /// <summary>
        /// Types of images related objects.
        /// </summary>
        public enum ImageRelatedObject
        {
            DestinationPage = 2,
            CompanyPage = 1,
            Stamps = 3,
        }

        /// <summary>
        /// This Enum, needt to hold all the pages on your website.
        /// It is used by the SetLinkHanfler for building the frindly URL Format accoring to the Enum
        /// </summary>
        public enum LinkPages
        {
            Home,
            AboutUs,
            ContactUs,
            Companies,
            Destinations,
            Tips,
            Error,
            NotFound,
            QuestionsAndAnswers,
            Policies

        }

        /// <summary>
        /// This Enum, needt to hold all the pages on your website.
        /// It is used by the SetLinkHanfler for building the frindly URL Format accoring to the Enum
        /// </summary>
        public enum RelatedObjects
        {
            CompanyPage = 1,
            DestinationPage = 2,
            Template = 3,
            HomePage = 4,
        }

        /// <summary>
        /// enum for types of templates in static pages (like destination or company pages)
        /// </summary>
        public enum TemplateType
        {
            MainInformation = 1,
            CenterWithImage = 2,
            SideInformation = 3,
            SideWithImage = 4
        }


        public enum ImageTypes
        {
            Cover, Main, Thumbnail, Logo
        }

        public enum ImageFoldersTypes
        {
            CategoryImageFolder, PackageImageFolder, EventImageFolder, ArticlesImageFolder
        }

        public enum UserRoles
        {
            ChiefAdmin = 0,
            Admin = 1,
            SiteOperator = 2,  // Can only insert static data.
            SalesOperator = 3,// Can only manage orders
        }

        public enum PageType
        {
            Static = 1,
            Dynamic = 2,
        }


        public enum MenuPages
        {
            HomePage,
            AboutUs,
            PopularDestination,
            Airlines,
            Faq,
            ContactUs,
            Policies
        }

        public enum RequiredFields
        {
            PassportNumber,
            PassportExpiryDate,
            DateOfBirth,
            SeatOptions,
            MealType,
            PassportCountryOfIssue,
            BaggageWeight,
            NumberOfBags,
            FrequentFlyerNumber,
            FrequentFlyerType,
            InsuranceType,
        }

        public enum OrderStage
        {
            BookFlight = 1,
            IssueFlight = 2
        }
        public enum OrderXmlType
        {
            Request = 1,
            Response = 2
        }
        public enum OrderXmlStatus
        {
            None = 0,
            PriceChanged = 1,
            OutOfTime = 2,
            Succeed = 3,
            Failed = 4,
            Duplicate = 4,

        }
        public enum PaymentStatus
        {
            Pending = 1,
            Failed = 2,
            Succeed = 3,
        }

        public enum FlightStatus
        {
            OnTime = 1,
            Canceled = 2,
            Delayed = 3,
        }
        public enum MailSendOptions
        {
            OnlySupport = 1,
            SupportAndClient = 2,
            SupportAndAlgoma = 3,
        }
        public enum OrderPaymentStatus
        {
            /// <summary>
            /// Payment Not Paid 
            /// </summary>
            NotPaid = 1,
            /// <summary>
            /// Payment Failed 
            /// </summary>
            Failed = 2,
            /// <summary>
            /// Payment Succeed 
            /// </summary>
            Paid = 3,
        }
        protected string GetOrderStatusType(object obj)
        {
            int orderStatus = ConvertToValue.ConvertToInt(obj);
            if (orderStatus > 0)
            {
                return ((EnumHandler.OrderStatusType)orderStatus).ToLangString();
            }
            return "";
        }
        public enum OrderStatusType
        {
            /// <summary>
            /// Order is Panding
            /// </summary>
            Panding = 1,
            /// <summary>
            /// Booking On Request 
            /// </summary>
            BookingOnRequest = 2,
            /// <summary>
            /// Booking Failed
            /// </summary>
            BookingFailed = 3,
            /// <summary>
            /// Booking Succeed
            /// </summary>
            BookingSucceed = 4,
            /// <summary>
            /// Booking Cancelled
            /// </summary>
            BookingCancelled = 5,
            /// <summary>
            /// Order Closed
            /// </summary>
            OrderClosed = 6,
        }
    }
}