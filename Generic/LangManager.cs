﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;

namespace Generic
{
    /// Class To Translate Text In The WebSite
    /// </summary>
    public class LangManager : Page
    {
        /// </summary>
        /// Retrieves the subdomain from the specified URL.
        /// </summary>
        /// <param name="url">The URL from which to retrieve the subdomain.</param>
        /// <returns>The subdomain if it exist, otherwise null.</returns>
        private static string GetLanguagePrefix(Uri url)
        {
            // to test english (for user control you need to also change the Uc_BasePage)
           // return "en";


            if (url.HostNameType == UriHostNameType.Dns)
            {
               string host = url.Host;
               return host.Split(new char[] { '.' })[0];
            }
            return null;
        }

        public static int LangId
        {
            get
            {
                string langPrefix = GetLanguagePrefix(HttpContext.Current.Request.Url);

                //Languages oLanguages = LanguagesContainer.SelectByKeysView_Code(langPrefix, null).Single;
                //if (oLanguages != null)
                //{
                //    return oLanguages.DocId_Value;
                //}
                return WebConfigHandler.DefaultLanguageID;
            }
        }

        public static string Lang
        {

            get
            {
                // FOR TESTING
                //return "English";

                // GET THE LANGUAGE PREFIX FROM THE URL (EN / HE)
                string langPrefix = GetLanguagePrefix(HttpContext.Current.Request.Url).ToLower();
                switch (langPrefix)
                {
                    case "en":
                        return EnumHandler.Langeages.English.ToString();
                    case "he":
                        return EnumHandler.Langeages.Hebrew.ToString();
                    default:
                        return EnumHandler.Langeages.Hebrew.ToString();
                }
             
            }
        }

        public virtual string GetText(string text)
        {
            return GetLangText(text);

        }

        public static string GetLangText(string text)
        {
            try
            {
                ResourceManager temp =
                    new ResourceManager("LowCostSite.Resources." + Lang,
   System.Reflection.Assembly.GetExecutingAssembly());
                return temp.GetString(text);

            }
            catch (Exception)
            {
                return "";
            }
        }

    }
}
