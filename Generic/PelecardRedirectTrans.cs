﻿using DL_Generic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace PelecardInterface
{
    public class PelecardRedirectTrans
    {
        private UrlBulider Url { get; set; }

        #region Public Properties
        /// <summary> Properties descriptions
        /// pelecard user name
        /// password 
        /// terminal number
        /// 
        /// **configuration parameters for pelecard pay page, all parameters can changed**
        /// 
        ///  goodUrl = Path to return the success result of a transaction (Addresses can be set identical in both situations)
        ///  errordUrl = Path to return the result of a refused transaction (Addresses can be set identical in both situations)
        ///  ValidateLink = On Success send result to Url (Server side),this path gets the same answer that gets the path parameter goodUrl the same structure
        ///  ErrorLink = On Error send result to Url (Server side),this path gets the same answer that gets the path parameter errorUrl the same structure
        ///  
        ///  total = Amount of NIS (agorot)
        ///  currency = ( 1 - NIS, 2 - US, $ 978 - EUR) Credit transactions and payments will be made in nis(shekel) only
        ///  
        ///  maxPayments = Limiting the number of payments to choose from Max 1 to 99,sending a field blank will lock only one payment choice
        ///  minPaymentsNo = Limiting the number of minimum payments to choose from, 1 to 99,sending a field blank will lock only one payment choice
        ///  creditTypeFrom = If want to set the credit transaction represents the selection of a certain amount of payments
        ///  If choose one pay - will be considered standard transaction.
        ///  If choose between the two payments for 12 payments - payments will be considered the transaction fees 
        ///  If creditTypeFrom defined and user choose payments over defined number - payments will be considered the credit transaction
        ///  
        ///  Logo = Logo image with a maximum size of: width - 370 pixels and height - 85 pixels
        ///  smallLogo = Logo image with a maximum size of: width - 180 pixels and height - 45 pixels
        ///  hidePciDssLogo = I want to hide the Standard logo PCI - pass True in parameter
        ///  hidePelecardLogo = If want to hide the logo a pelecard - pass True in parameter
        ///  
        ///  theme = You can choose from seven (0-6) themes, sending empty will set default color - gray
        ///  background = You can send the background color code - the code should send without first hash (example = 000000)
        ///  backgroundImage = You can set a background image + replication and location settings
        ///  Location options: left top , left center , left bottom- --- right top , right center , right bottom ---- center top , center center , center bottom
        ///  Replication options: repeat , repeat-x , repeat-y , no-repeat
        ///  
        ///  topMargin = You can set the height of it will start page
        ///  
        ///  supportedCardTypes = If want to view the types of cards that respects the business.
        ///  For each card type separation of commas - True.
        ///  The order is: Visa, MasterCard, American Express, Diners, Local Israkard
        ///  Example to display all types of cards - True, True, True, True, True
        ///
        ///  Parmx = More details Free text up to 19 characters
        ///  hideParmx = If want to hide the field details pass True in parameter
        ///   
        ///  cancelUrl = You can send address canceled, the customer will be returned to the address to be set when click on Cancel
        ///
        ///  MustConfirm = If want to charge the customer to confirm the click of a button committed payment - pass True in parameter
        ///  ConfirmationText = Free text to be displayed when the customer click the payment button and set that must approve the pressing
        ///
        ///  SupportPhone =  In the case of a critical system error message appears suitable, you can view Phone
        ///  errorText = You can send an error message to appear on clearance,used to send the customer wants to return to the case of a transaction charge refused (not intended initial appeal)
        ///
        ///  id =  If want to charge the customer type id - pass in parameter - Must, in addition you can send the number directly if you have been known to
        ///  If want to hide the field - pass in parameter - Hide
        ///
        ///  cvv2 = If want to charge the customer type cvv - pass in parameter - Must
        ///  If want to hide the field - pass in parameter - Hide
        ///
        ///  authNum = You can enter a confirmation number, Web sites is used only in the test 
        ///
        ///  shopNo = store number - typically 001
        ///
        ///  frmAction = if I want to get a token addition String answer - pass in parameter "CreateToken"
        ///  TokenForTerminal = Used for multiple terminals wish to take action on the terminal A and saving token on the terminal B
        ///
        ///  J5 If want to deal confirmation, ie the concept of a frame without charge - pass in parameter True
        ///
        ///  keepSSL = If want to get the string answer under encryption, encryption is intended only for qualified - pass in parameter True 
        ///  
        /// sessionIdNumber
        /// showTotalOnRight 
        /// </summary>

        private string Password
        {
            get
            {
                string password = ConfigurationManager.AppSettings["pelecardUserPassword"];
                password.Replace("+", "`9`");
                password = password.Replace("&", "`8`");
                password = password.Replace("%", "`7`");
                return password;
            }
        }
        private string UserName { get { return ConfigurationManager.AppSettings["pelecardUserName"]; } }
        private string TerminalNumber { get { return ConfigurationManager.AppSettings["pelecardTermNo"]; } }
        private string GoodUrl { get { return ConfigurationManager.AppSettings["GoodUrl"]; } }
        private string ErrorUrl { get { return ConfigurationManager.AppSettings["ErrorUrl"]; } }
        private string ValidateLink { get { return ConfigurationManager.AppSettings["ValidateLink"]; } }
        private string ErrorLink { get { return ConfigurationManager.AppSettings["ErrorLink"]; } }
        //private string Currency { get { return ConfigurationManager.AppSettings["currency"]; } }
        private string MaxPayments { get { return ConfigurationManager.AppSettings["maxPayments"]; } }
        private string MinPaymentsNo { get { return ConfigurationManager.AppSettings["minPaymentsNo"]; } }
        private string CreditTypeFrom { get { return ConfigurationManager.AppSettings["creditTypeFrom"]; } }
        private string HidePelecardLogo { get { return ConfigurationManager.AppSettings["hidePelecardLogo"]; } }
        private string Background { get { return ConfigurationManager.AppSettings["background"]; } }
        private string BackgroundImage { get { return ConfigurationManager.AppSettings["backgroundImage"]; } }
        private string TopMargin { get { return ConfigurationManager.AppSettings["topMargin"]; } }
        private string SupportedCardTypes { get { return ConfigurationManager.AppSettings["supportedCardTypes"]; } }
        private string CancelUrl { get { return ConfigurationManager.AppSettings["cancelUrl"]; } }
        private string SupportPhone { get { return ConfigurationManager.AppSettings["SupportPhone"]; } }
        private string ErrorText { get { return ConfigurationManager.AppSettings["errorText"]; } }
        private string AuthNum { get { return ConfigurationManager.AppSettings["authNum"]; } }
        private string ShopNo { get { return ConfigurationManager.AppSettings["shopNo"]; } }
        private string FrmAction { get { return ConfigurationManager.AppSettings["frmAction"]; } }
        private string TokenForTerminal { get { return ConfigurationManager.AppSettings["TokenForTerminal"]; } }
        private string KeepSSL { get { return ConfigurationManager.AppSettings["keepSSL"]; } }
        private string SessionIdNumber { get { return ConfigurationManager.AppSettings["sessionIdNumber"]; } }
        public static string DestinationUrl { get { return ConfigurationManager.AppSettings["destinationUrl"]; } }
        private string Cvv2 { get { return ConfigurationManager.AppSettings["cvv2"]; } }
        private string Id { get { return ConfigurationManager.AppSettings["id"]; } }

        private string StyleSheetAddress { get { return ConfigurationManager.AppSettings["styleSheetAddress"]; } }
        private string DesignInput { get { return ConfigurationManager.AppSettings["DesignInput"]; } }
        private string CCDash { get { return ConfigurationManager.AppSettings["CCDash"]; } }
        private string CreditCardHolder { get { return ConfigurationManager.AppSettings["CreditCardHolder"]; } }

        private double total { get; set; }
        public double Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
                Url.Add("total", total.ToString());
            }
        }

        private string currency { get; set; }
        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
                Url.Add("currency", currency);
            }
        }

        private string j5 { get; set; }
        private string J5
        {
            get
            {
                return j5;
            }
            set
            {
                j5 = value;
                Url.Add("J5", j5.ToString());
            }
        }

        private bool isJ5 { set; get; }
        private bool IsJ5
        {
            get
            {
                return isJ5;
            }
            set
            {
                isJ5 = value;
                if (value)
                {
                    J5 = "True";
                }
                else
                {
                    J5 = "";
                }
            }
        }

        public string HideParmx
        {
            get
            {
                if (!string.IsNullOrEmpty(Parmx))
                {
                    return "";
                }
                return "True";
            }
        }
        private string parmx { get; set; }
        public string Parmx
        {
            get
            {
                return parmx;
            }
            set
            {
                parmx = value;
                Url.Add("Parmx", parmx.ToString());
                Url.Add("hideParmx", "");

            }
        }

        private string headText { get; set; }
        public string HeadText
        {
            get
            {
                return headText;
            }
            set
            {
                headText = value;
                Url.Add("headText", headText.ToString());
            }
        }
        private string bottomText { get; set; }
        public string BottomText
        {
            get
            {
                return bottomText;
            }
            set
            {
                bottomText = value;
                Url.Add("bottomText", bottomText.ToString());
            }
        }

        #endregion

        private PelecardRedirectTrans()
        {
            Url = new UrlBulider();
            //Init with configuration
            Url.Add("password", Password);
            Url.Add("userName", UserName);
            Url.Add("termNo", TerminalNumber);
            Url.Add("goodUrl", HttpUtility.UrlEncode(GoodUrl));
            if (ConvertToValue.ConvertToBool(ConfigurationSettings.AppSettings["FakePalecardPayment"]))
            {
                Url.Add("errorUrl", HttpUtility.UrlEncode(GoodUrl));

            }
            else
            {
                Url.Add("errorUrl", HttpUtility.UrlEncode(ErrorUrl));

            }
            Url.Add("ValidateLink", HttpUtility.UrlEncode(ValidateLink));
            Url.Add("ErrorLink", HttpUtility.UrlEncode(ErrorLink));
            //Url.Add("currency", Currency);
            Url.Add("maxPayments", MaxPayments);
            Url.Add("minPaymentsNo", MinPaymentsNo);
            Url.Add("creditTypeFrom", CreditTypeFrom);
            Url.Add("hidePelecardLogo", HidePelecardLogo);
            Url.Add("background", Background);
            Url.Add("backgroundImage", BackgroundImage);
            Url.Add("topMargin", TopMargin);
            Url.Add("supportedCardTypes", SupportedCardTypes);
            Url.Add("cancelUrl", CancelUrl);
            Url.Add("SupportPhone", SupportPhone);
            Url.Add("errorText", ErrorText);
            Url.Add("cvv2", Cvv2);
            Url.Add("id", Id);
            if (ConvertToValue.ConvertToBool(ConfigurationSettings.AppSettings["FakePalecardPayment"]))
            {
                Url.Add("authNum", "123456");
            }else
            {
                Url.Add("authNum", AuthNum);
            }
            Url.Add("shopNo", ShopNo);
            Url.Add("frmAction", FrmAction);
            Url.Add("TokenForTerminal", TokenForTerminal);
            Url.Add("keepSSL", KeepSSL);
            Url.Add("sessionIdNumber", SessionIdNumber);
            Url.Add("styleSheetAddress", StyleSheetAddress);
            Url.Add("DesignInput", DesignInput);
            Url.Add("CCDash", CCDash);
            Url.Add("CreditCardHolder", CreditCardHolder);
            //Url.Add("Language", "HEB");
        }

        public PelecardRedirectTrans(bool isJ5, double totalPrice, string currency, string headText = "", string bottomText = "", string parmx = "")
            : this()
        {
            this.IsJ5 = isJ5;
            this.Total = totalPrice;
            this.HeadText = headText;
            this.BottomText = bottomText;
            this.Parmx = parmx;
            this.Currency = currency;
        }
        public string GetPostData()
        {
            if (Total > 0)
            {
                return Url.ToString();
            } return null;
        }

        //private string GetResponseFormPelecardPage()
        //{
        //    // Create a request using a URL that can receive a post. 
        //    System.Net.WebRequest request = System.Net.WebRequest.Create(DestinationUrl);
        //    // Set the Method property of the request to POST.
        //    request.Method = "POST";
        //    // Create POST data and convert it to a byte array.
        //    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(GetPostData());
        //    // Set the ContentType property of the WebRequest.
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    // Set the ContentLength property of the WebRequest.
        //    request.ContentLength = byteArray.Length;
        //    // Get the request stream.
        //    System.IO.Stream dataStream = request.GetRequestStream();
        //    // Write the data to the request stream.
        //    dataStream.Write(byteArray, 0, byteArray.Length);
        //    // Close the Stream object.
        //    dataStream.Close();
        //    // Get the response.
        //    System.Net.WebResponse response = request.GetResponse();
        //    // Display the status.
        //    //Response.Write(((System.Net.HttpWebResponse)response).StatusDescription);
        //    // Get the stream containing content returned by the server.
        //    dataStream = response.GetResponseStream();
        //    // Open the stream using a StreamReader for easy access.
        //    System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
        //    // Read the content.
        //    string responseFromServer = reader.ReadToEnd();
        //    // Display the content.
        //    //Response.Write(responseFromServer);
        //    // Clean up the streams.
        //    reader.Close();
        //    dataStream.Close();
        //    response.Close();

        //    return responseFromServer;
        //}

        //private string PreparePOSTForm(string url, string responseFromServer)
        //{
        //    //Set a name for the form
        //    string formID = "formPelecard";

        //    //Build the form using the specified data to be posted.
        //    StringBuilder strForm = new StringBuilder();
        //    strForm.Append("<form id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");
        //    strForm.Append(responseFromServer);
        //    strForm.Append("<input type=\"hidden\" name=\"noCheck\" value=\"true\" id=\"noCheck\" />");
        //    strForm.Append("</form>");
        //    //Build the JavaScript which will do the Posting operation.
        //    StringBuilder strScript = new StringBuilder();
        //    strScript.Append("<script language='javascript'>");
        //    strScript.Append("var v" + formID + " = document." + formID + ";");
        //    strScript.Append("v" + formID + ".submit();return false;");
        //    strScript.Append("</script>");
        //    //Return the form and the script concatenated.
        //    //(The order is important, Form then JavaScript)
        //    return strForm.ToString() + strScript.ToString();
        //}

        //public void WritePelecardPage(Page page)
        //{
        //    //Prepare the Posting form
        //    string strForm = PreparePOSTForm(DestinationUrl, GetResponseFormPelecardPage());
        //    //Add a literal control the specified page holding 
        //    //the Post Form, this is to submit the Posting form with the request.
        //    page.Response.Write(strForm);
        //}
    }

    public class UrlBulider
    {
        private Dictionary<string, string> urlParams { get; set; }

        public Dictionary<string, string> UrlParams
        {
            get
            {
                if (urlParams == null)
                {
                    urlParams = new Dictionary<string, string>();
                }
                return urlParams;
            }
            set
            {
                urlParams = value;
            }
        }
        public UrlBulider()
        {
            UrlParams = new Dictionary<string, string>();
        }
        public void Add(string name, string value)
        {
            if (UrlParams.ContainsKey(name))
            {
                UrlParams[name] = value;
            }
            else
            {
                UrlParams.Add(name, value);
            }
        }
        public override string ToString()
        {
            List<string> lstr = new List<string>();
            foreach (var item in UrlParams)
            {
                lstr.Add(string.Format("{0}={1}", item.Key, item.Value));
            }
            return string.Join("&", lstr);
        }
    }
}