﻿using DL_Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Generic
{
    public class QueryStringManager
    {
        /// <summary>
        /// Generic Function to return QueryString Value by Enum
        /// Only Send Enum From Generic.EnumHandler.QueryStrings you need and it will retrive the QuerySyring
        /// </summary>
        /// <param name="_QueryStrings">Generic.EnumHandler.QueryStrings of the QueryString You Need</param>
        /// <returns>HttpContext.Current.Request.QueryString["YourValue"]</returns>
        public static string QueryString(Generic.EnumHandler.QueryStrings _QueryStrings)
        {
            return HttpContext.Current.Request.QueryString[_QueryStrings.ToString("g")];
        }

        /// <summary>
        /// function to get the query string page object id
        /// </summary>
        /// <param name="_QueryStrings"></param>
        /// <returns></returns>
        public static int QueryStringID(Generic.EnumHandler.QueryStrings QueryStringName)
        {
            return ConvertToValue.ConvertToInt(HttpContext.Current.Request.QueryString[QueryStringName.ToString()]);
        }

        public static string RedirectWithQuery(string path, EnumHandler.QueryStrings queryName, string queryValue)
        {
            return string.Format("{0}?{1}={2}", path, queryName, queryValue); 
        }
        //Old Handle
        ///// <summary>
        ///// static readonly string that define CityID for all Query Strings
        ///// </summary>
        //public static readonly string ID = "ID";
        //
        ///// <summary>
        ///// get the City id value from the QueryString
        ///// </summary>
        //public static string QueryStringID
        //{
        //    get
        //    {
        //        return HttpContext.Current.Request.QueryString[ID];
        //    }
        //}
        //
        ///// <summary>
        ///// static readonly string that define CityID for all Query Strings
        ///// </summary>
        //public static readonly string URL = "URL";
        //
        ///// <summary>
        ///// get the City id value from the QueryString
        ///// </summary>
        //public static string QueryStringURL
        //{
        //    get
        //    {
        //        return HttpContext.Current.Request.QueryString[URL];
        //    }
        //}
    }
}
