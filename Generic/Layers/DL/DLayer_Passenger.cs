

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Passenger  : ContainerItem<Passenger>{

#region CTOR

    #region Constractor
    static Passenger()
    {
        ConvertEvent = Passenger.OnConvert;
    }  
    //public KeyValuesContainer<Passenger> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Passenger()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.PassengerKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Passenger OnConvert(DataRow dr)
    {
        int LangId = Translator<Passenger>.LangId;            
        Passenger oPassenger = null;
        if (dr != null)
        {
            oPassenger = new Passenger();
            #region Create Object
            oPassenger.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Passenger.Field_DateCreated]);
             oPassenger.DocId = ConvertTo.ConvertToInt(dr[TBNames_Passenger.Field_DocId]);
             oPassenger.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Passenger.Field_IsDeleted]);
             oPassenger.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Passenger.Field_IsActive]);
             oPassenger.NetPrice = ConvertTo.ConvertToDouble(dr[TBNames_Passenger.Field_NetPrice]);
             oPassenger.FirstName = ConvertTo.ConvertToString(dr[TBNames_Passenger.Field_FirstName]);
             oPassenger.LastName = ConvertTo.ConvertToString(dr[TBNames_Passenger.Field_LastName]);
             oPassenger.DateOfBirth = ConvertTo.ConvertToDateTime(dr[TBNames_Passenger.Field_DateOfBirth]);
             oPassenger.Gender = ConvertTo.ConvertToString(dr[TBNames_Passenger.Field_Gender]);
             oPassenger.Title = ConvertTo.ConvertToString(dr[TBNames_Passenger.Field_Title]);
             oPassenger.PassportNumber = ConvertTo.ConvertToString(dr[TBNames_Passenger.Field_PassportNumber]);
             oPassenger.PassportExpiryDate = ConvertTo.ConvertToDateTime(dr[TBNames_Passenger.Field_PassportExpiryDate]);
             oPassenger.PassportIssueCountry = ConvertTo.ConvertToString(dr[TBNames_Passenger.Field_PassportIssueCountry]);
             oPassenger.CheckInTypeOutward = ConvertTo.ConvertToString(dr[TBNames_Passenger.Field_CheckInTypeOutward]);
             oPassenger.CheckInTypeReturn = ConvertTo.ConvertToString(dr[TBNames_Passenger.Field_CheckInTypeReturn]);
             oPassenger.CheckInPriceOutward = ConvertTo.ConvertToDouble(dr[TBNames_Passenger.Field_CheckInPriceOutward]);
             oPassenger.CheckInPriceReturn = ConvertTo.ConvertToDouble(dr[TBNames_Passenger.Field_CheckInPriceReturn]);
             oPassenger.PriceMarkUp = ConvertTo.ConvertToDouble(dr[TBNames_Passenger.Field_PriceMarkUp]);
 
//FK     KeySeatPerFlightLegDocId
            oPassenger.SeatPerFlightLegDocId = ConvertTo.ConvertToInt(dr[TBNames_Passenger.Field_SeatPerFlightLegDocId]);
 
//FK     KeyOrdersDocId
            oPassenger.OrdersDocId = ConvertTo.ConvertToInt(dr[TBNames_Passenger.Field_OrdersDocId]);
 
//FK     KeyWhiteLabelDocId
            oPassenger.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Passenger.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<Passenger>.Translate(oPassenger.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oPassenger;
    }

    
#endregion

//#REP_HERE 
#region Passenger Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyPassengerDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyPassengerDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyPassengerIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyPassengerIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_NetPrice;
private Double _net_price;
public String FriendlyNetPrice
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerNetPrice);   
    }
}
public  Double? NetPrice
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyPassengerNetPrice));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerNetPrice, value);
         _net_price = ConvertToValue.ConvertToDouble(value);
        isSetOnce_NetPrice = true;
    }
}


public Double NetPrice_Value
{
    get
    {
        //return _net_price; //ConvertToValue.ConvertToDouble(NetPrice);
        if(isSetOnce_NetPrice) {return _net_price;}
        else {return ConvertToValue.ConvertToDouble(NetPrice);}
    }
}

public string NetPrice_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_net_price).ToString();
               //if(isSetOnce_NetPrice) {return ConvertToValue.ConvertToDouble(_net_price).ToString();}
               //else {return ConvertToValue.ConvertToDouble(NetPrice).ToString();}
            ConvertToValue.ConvertToDouble(NetPrice).ToString();
    }
}

private bool isSetOnce_FirstName;
private string _first_name;
public String FriendlyFirstName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerFirstName);   
    }
}
public  string FirstName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPassengerFirstName));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerFirstName, value);
         _first_name = ConvertToValue.ConvertToString(value);
        isSetOnce_FirstName = true;
    }
}


public string FirstName_Value
{
    get
    {
        //return _first_name; //ConvertToValue.ConvertToString(FirstName);
        if(isSetOnce_FirstName) {return _first_name;}
        else {return ConvertToValue.ConvertToString(FirstName);}
    }
}

public string FirstName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_first_name).ToString();
               //if(isSetOnce_FirstName) {return ConvertToValue.ConvertToString(_first_name).ToString();}
               //else {return ConvertToValue.ConvertToString(FirstName).ToString();}
            ConvertToValue.ConvertToString(FirstName).ToString();
    }
}

private bool isSetOnce_LastName;
private string _last_name;
public String FriendlyLastName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerLastName);   
    }
}
public  string LastName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPassengerLastName));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerLastName, value);
         _last_name = ConvertToValue.ConvertToString(value);
        isSetOnce_LastName = true;
    }
}


public string LastName_Value
{
    get
    {
        //return _last_name; //ConvertToValue.ConvertToString(LastName);
        if(isSetOnce_LastName) {return _last_name;}
        else {return ConvertToValue.ConvertToString(LastName);}
    }
}

public string LastName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_last_name).ToString();
               //if(isSetOnce_LastName) {return ConvertToValue.ConvertToString(_last_name).ToString();}
               //else {return ConvertToValue.ConvertToString(LastName).ToString();}
            ConvertToValue.ConvertToString(LastName).ToString();
    }
}

private bool isSetOnce_DateOfBirth;
private DateTime _date_of_birth;
public String FriendlyDateOfBirth
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerDateOfBirth);   
    }
}
public  DateTime? DateOfBirth
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyPassengerDateOfBirth));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerDateOfBirth, value);
         _date_of_birth = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateOfBirth = true;
    }
}


public DateTime DateOfBirth_Value
{
    get
    {
        //return _date_of_birth; //ConvertToValue.ConvertToDateTime(DateOfBirth);
        if(isSetOnce_DateOfBirth) {return _date_of_birth;}
        else {return ConvertToValue.ConvertToDateTime(DateOfBirth);}
    }
}

public string DateOfBirth_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_of_birth).ToShortDateString();
               //if(isSetOnce_DateOfBirth) {return ConvertToValue.ConvertToDateTime(_date_of_birth).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateOfBirth).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateOfBirth).ToShortDateString();
    }
}

private bool isSetOnce_Gender;
private string _gender;
public String FriendlyGender
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerGender);   
    }
}
public  string Gender
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPassengerGender));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerGender, value);
         _gender = ConvertToValue.ConvertToString(value);
        isSetOnce_Gender = true;
    }
}


public string Gender_Value
{
    get
    {
        //return _gender; //ConvertToValue.ConvertToString(Gender);
        if(isSetOnce_Gender) {return _gender;}
        else {return ConvertToValue.ConvertToString(Gender);}
    }
}

public string Gender_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_gender).ToString();
               //if(isSetOnce_Gender) {return ConvertToValue.ConvertToString(_gender).ToString();}
               //else {return ConvertToValue.ConvertToString(Gender).ToString();}
            ConvertToValue.ConvertToString(Gender).ToString();
    }
}

private bool isSetOnce_Title;
private string _title;
public String FriendlyTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerTitle);   
    }
}
public  string Title
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPassengerTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerTitle, value);
         _title = ConvertToValue.ConvertToString(value);
        isSetOnce_Title = true;
    }
}


public string Title_Value
{
    get
    {
        //return _title; //ConvertToValue.ConvertToString(Title);
        if(isSetOnce_Title) {return _title;}
        else {return ConvertToValue.ConvertToString(Title);}
    }
}

public string Title_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_title).ToString();
               //if(isSetOnce_Title) {return ConvertToValue.ConvertToString(_title).ToString();}
               //else {return ConvertToValue.ConvertToString(Title).ToString();}
            ConvertToValue.ConvertToString(Title).ToString();
    }
}

private bool isSetOnce_PassportNumber;
private string _passport_number;
public String FriendlyPassportNumber
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerPassportNumber);   
    }
}
public  string PassportNumber
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPassengerPassportNumber));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerPassportNumber, value);
         _passport_number = ConvertToValue.ConvertToString(value);
        isSetOnce_PassportNumber = true;
    }
}


public string PassportNumber_Value
{
    get
    {
        //return _passport_number; //ConvertToValue.ConvertToString(PassportNumber);
        if(isSetOnce_PassportNumber) {return _passport_number;}
        else {return ConvertToValue.ConvertToString(PassportNumber);}
    }
}

public string PassportNumber_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_passport_number).ToString();
               //if(isSetOnce_PassportNumber) {return ConvertToValue.ConvertToString(_passport_number).ToString();}
               //else {return ConvertToValue.ConvertToString(PassportNumber).ToString();}
            ConvertToValue.ConvertToString(PassportNumber).ToString();
    }
}

private bool isSetOnce_PassportExpiryDate;
private DateTime _passport_expiry_date;
public String FriendlyPassportExpiryDate
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerPassportExpiryDate);   
    }
}
public  DateTime? PassportExpiryDate
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyPassengerPassportExpiryDate));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerPassportExpiryDate, value);
         _passport_expiry_date = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_PassportExpiryDate = true;
    }
}


public DateTime PassportExpiryDate_Value
{
    get
    {
        //return _passport_expiry_date; //ConvertToValue.ConvertToDateTime(PassportExpiryDate);
        if(isSetOnce_PassportExpiryDate) {return _passport_expiry_date;}
        else {return ConvertToValue.ConvertToDateTime(PassportExpiryDate);}
    }
}

public string PassportExpiryDate_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_passport_expiry_date).ToShortDateString();
               //if(isSetOnce_PassportExpiryDate) {return ConvertToValue.ConvertToDateTime(_passport_expiry_date).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(PassportExpiryDate).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(PassportExpiryDate).ToShortDateString();
    }
}

private bool isSetOnce_PassportIssueCountry;
private string _passport_issue_country;
public String FriendlyPassportIssueCountry
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerPassportIssueCountry);   
    }
}
public  string PassportIssueCountry
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPassengerPassportIssueCountry));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerPassportIssueCountry, value);
         _passport_issue_country = ConvertToValue.ConvertToString(value);
        isSetOnce_PassportIssueCountry = true;
    }
}


public string PassportIssueCountry_Value
{
    get
    {
        //return _passport_issue_country; //ConvertToValue.ConvertToString(PassportIssueCountry);
        if(isSetOnce_PassportIssueCountry) {return _passport_issue_country;}
        else {return ConvertToValue.ConvertToString(PassportIssueCountry);}
    }
}

public string PassportIssueCountry_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_passport_issue_country).ToString();
               //if(isSetOnce_PassportIssueCountry) {return ConvertToValue.ConvertToString(_passport_issue_country).ToString();}
               //else {return ConvertToValue.ConvertToString(PassportIssueCountry).ToString();}
            ConvertToValue.ConvertToString(PassportIssueCountry).ToString();
    }
}

private bool isSetOnce_CheckInTypeOutward;
private string _check_in_type_outward;
public String FriendlyCheckInTypeOutward
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerCheckInTypeOutward);   
    }
}
public  string CheckInTypeOutward
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPassengerCheckInTypeOutward));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerCheckInTypeOutward, value);
         _check_in_type_outward = ConvertToValue.ConvertToString(value);
        isSetOnce_CheckInTypeOutward = true;
    }
}


public string CheckInTypeOutward_Value
{
    get
    {
        //return _check_in_type_outward; //ConvertToValue.ConvertToString(CheckInTypeOutward);
        if(isSetOnce_CheckInTypeOutward) {return _check_in_type_outward;}
        else {return ConvertToValue.ConvertToString(CheckInTypeOutward);}
    }
}

public string CheckInTypeOutward_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_check_in_type_outward).ToString();
               //if(isSetOnce_CheckInTypeOutward) {return ConvertToValue.ConvertToString(_check_in_type_outward).ToString();}
               //else {return ConvertToValue.ConvertToString(CheckInTypeOutward).ToString();}
            ConvertToValue.ConvertToString(CheckInTypeOutward).ToString();
    }
}

private bool isSetOnce_CheckInTypeReturn;
private string _check_in_type_return;
public String FriendlyCheckInTypeReturn
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerCheckInTypeReturn);   
    }
}
public  string CheckInTypeReturn
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPassengerCheckInTypeReturn));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerCheckInTypeReturn, value);
         _check_in_type_return = ConvertToValue.ConvertToString(value);
        isSetOnce_CheckInTypeReturn = true;
    }
}


public string CheckInTypeReturn_Value
{
    get
    {
        //return _check_in_type_return; //ConvertToValue.ConvertToString(CheckInTypeReturn);
        if(isSetOnce_CheckInTypeReturn) {return _check_in_type_return;}
        else {return ConvertToValue.ConvertToString(CheckInTypeReturn);}
    }
}

public string CheckInTypeReturn_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_check_in_type_return).ToString();
               //if(isSetOnce_CheckInTypeReturn) {return ConvertToValue.ConvertToString(_check_in_type_return).ToString();}
               //else {return ConvertToValue.ConvertToString(CheckInTypeReturn).ToString();}
            ConvertToValue.ConvertToString(CheckInTypeReturn).ToString();
    }
}

private bool isSetOnce_CheckInPriceOutward;
private Double _check_in_price_outward;
public String FriendlyCheckInPriceOutward
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerCheckInPriceOutward);   
    }
}
public  Double? CheckInPriceOutward
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyPassengerCheckInPriceOutward));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerCheckInPriceOutward, value);
         _check_in_price_outward = ConvertToValue.ConvertToDouble(value);
        isSetOnce_CheckInPriceOutward = true;
    }
}


public Double CheckInPriceOutward_Value
{
    get
    {
        //return _check_in_price_outward; //ConvertToValue.ConvertToDouble(CheckInPriceOutward);
        if(isSetOnce_CheckInPriceOutward) {return _check_in_price_outward;}
        else {return ConvertToValue.ConvertToDouble(CheckInPriceOutward);}
    }
}

public string CheckInPriceOutward_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_check_in_price_outward).ToString();
               //if(isSetOnce_CheckInPriceOutward) {return ConvertToValue.ConvertToDouble(_check_in_price_outward).ToString();}
               //else {return ConvertToValue.ConvertToDouble(CheckInPriceOutward).ToString();}
            ConvertToValue.ConvertToDouble(CheckInPriceOutward).ToString();
    }
}

private bool isSetOnce_CheckInPriceReturn;
private Double _check_in_price_return;
public String FriendlyCheckInPriceReturn
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerCheckInPriceReturn);   
    }
}
public  Double? CheckInPriceReturn
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyPassengerCheckInPriceReturn));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerCheckInPriceReturn, value);
         _check_in_price_return = ConvertToValue.ConvertToDouble(value);
        isSetOnce_CheckInPriceReturn = true;
    }
}


public Double CheckInPriceReturn_Value
{
    get
    {
        //return _check_in_price_return; //ConvertToValue.ConvertToDouble(CheckInPriceReturn);
        if(isSetOnce_CheckInPriceReturn) {return _check_in_price_return;}
        else {return ConvertToValue.ConvertToDouble(CheckInPriceReturn);}
    }
}

public string CheckInPriceReturn_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_check_in_price_return).ToString();
               //if(isSetOnce_CheckInPriceReturn) {return ConvertToValue.ConvertToDouble(_check_in_price_return).ToString();}
               //else {return ConvertToValue.ConvertToDouble(CheckInPriceReturn).ToString();}
            ConvertToValue.ConvertToDouble(CheckInPriceReturn).ToString();
    }
}

private bool isSetOnce_PriceMarkUp;
private Double _price_mark_up;
public String FriendlyPriceMarkUp
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerPriceMarkUp);   
    }
}
public  Double? PriceMarkUp
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyPassengerPriceMarkUp));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerPriceMarkUp, value);
         _price_mark_up = ConvertToValue.ConvertToDouble(value);
        isSetOnce_PriceMarkUp = true;
    }
}


public Double PriceMarkUp_Value
{
    get
    {
        //return _price_mark_up; //ConvertToValue.ConvertToDouble(PriceMarkUp);
        if(isSetOnce_PriceMarkUp) {return _price_mark_up;}
        else {return ConvertToValue.ConvertToDouble(PriceMarkUp);}
    }
}

public string PriceMarkUp_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_price_mark_up).ToString();
               //if(isSetOnce_PriceMarkUp) {return ConvertToValue.ConvertToDouble(_price_mark_up).ToString();}
               //else {return ConvertToValue.ConvertToDouble(PriceMarkUp).ToString();}
            ConvertToValue.ConvertToDouble(PriceMarkUp).ToString();
    }
}

private bool isSetOnce_SeatPerFlightLegDocId;
private int _seat_per_flight_leg_doc_id;
public String FriendlySeatPerFlightLegDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeatPerFlightLegDocId);   
    }
}
public  int? SeatPerFlightLegDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySeatPerFlightLegDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeySeatPerFlightLegDocId, value);
         _seat_per_flight_leg_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_SeatPerFlightLegDocId = true;
    }
}


public int SeatPerFlightLegDocId_Value
{
    get
    {
        //return _seat_per_flight_leg_doc_id; //ConvertToValue.ConvertToInt(SeatPerFlightLegDocId);
        if(isSetOnce_SeatPerFlightLegDocId) {return _seat_per_flight_leg_doc_id;}
        else {return ConvertToValue.ConvertToInt(SeatPerFlightLegDocId);}
    }
}

public string SeatPerFlightLegDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_seat_per_flight_leg_doc_id).ToString();
               //if(isSetOnce_SeatPerFlightLegDocId) {return ConvertToValue.ConvertToInt(_seat_per_flight_leg_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(SeatPerFlightLegDocId).ToString();}
            ConvertToValue.ConvertToInt(SeatPerFlightLegDocId).ToString();
    }
}

private bool isSetOnce_OrdersDocId;
private int _orders_doc_id;
public String FriendlyOrdersDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersDocId);   
    }
}
public  int? OrdersDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersDocId, value);
         _orders_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrdersDocId = true;
    }
}


public int OrdersDocId_Value
{
    get
    {
        //return _orders_doc_id; //ConvertToValue.ConvertToInt(OrdersDocId);
        if(isSetOnce_OrdersDocId) {return _orders_doc_id;}
        else {return ConvertToValue.ConvertToInt(OrdersDocId);}
    }
}

public string OrdersDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_orders_doc_id).ToString();
               //if(isSetOnce_OrdersDocId) {return ConvertToValue.ConvertToInt(_orders_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrdersDocId).ToString();}
            ConvertToValue.ConvertToInt(OrdersDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //U_A
        //20_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //U_B
        //20_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_C
        //20_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //U_E
        //20_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice", ex.Message));

            }
            return paramsSelect;
        }



        //U_F
        //20_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_G
        //20_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_H
        //20_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth", ex.Message));

            }
            return paramsSelect;
        }



        //U_I
        //20_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender", ex.Message));

            }
            return paramsSelect;
        }



        //U_J
        //20_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_K
        //20_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_L
        //20_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_M
        //20_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_N
        //20_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_O
        //20_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_P
        //20_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q
        //20_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_R
        //20_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_S
        //20_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_T
        //20_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_B
        //20_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_C
        //20_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_E
        //20_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_NetPrice(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_NetPrice", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_F
        //20_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_G
        //20_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LastName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_H
        //20_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DateOfBirth(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_DateOfBirth", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_I
        //20_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Gender(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_Gender", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_J
        //20_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_K
        //20_0_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_L
        //20_0_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_M
        //20_0_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_N
        //20_0_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_O
        //20_0_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_P
        //20_0_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_Q
        //20_0_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_R
        //20_0_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_S
        //20_0_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_T
        //20_0_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateCreated)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_C
        //20_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_E
        //20_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_NetPrice(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_NetPrice", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_F
        //20_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FirstName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_G
        //20_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LastName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_H
        //20_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DateOfBirth(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_DateOfBirth", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_I
        //20_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Gender(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_Gender", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_J
        //20_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_K
        //20_1_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_L
        //20_1_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_M
        //20_1_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_N
        //20_1_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_O
        //20_1_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_P
        //20_1_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_Q
        //20_1_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_R
        //20_1_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_S
        //20_1_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_T
        //20_1_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.DocId)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_E
        //20_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_NetPrice(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_NetPrice", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_F
        //20_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_G
        //20_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_H
        //20_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DateOfBirth(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_DateOfBirth", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_I
        //20_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Gender(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_Gender", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_J
        //20_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_K
        //20_2_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_L
        //20_2_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_M
        //20_2_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_N
        //20_2_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_O
        //20_2_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_P
        //20_2_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_Q
        //20_2_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_R
        //20_2_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_S
        //20_2_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_T
        //20_2_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPassenger.IsDeleted)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_F
        //20_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_FirstName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_G
        //20_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_LastName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_H
        //20_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_DateOfBirth(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_DateOfBirth", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_I
        //20_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_Gender(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_Gender", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_J
        //20_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_K
        //20_4_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_L
        //20_4_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_M
        //20_4_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_N
        //20_4_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_O
        //20_4_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_P
        //20_4_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_Q
        //20_4_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_R
        //20_4_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_S
        //20_4_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_T
        //20_4_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_NetPrice, ConvertTo.ConvertEmptyToDBNull(oPassenger.NetPrice)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_G
        //20_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_LastName(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_H
        //20_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_DateOfBirth(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_DateOfBirth", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_I
        //20_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Gender(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_Gender", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_J
        //20_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_K
        //20_5_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_L
        //20_5_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_M
        //20_5_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_N
        //20_5_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_O
        //20_5_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_P
        //20_5_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_Q
        //20_5_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_R
        //20_5_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_S
        //20_5_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_T
        //20_5_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oPassenger.FirstName)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_H
        //20_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_DateOfBirth(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_DateOfBirth", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_I
        //20_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Gender(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_Gender", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_J
        //20_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_K
        //20_6_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_L
        //20_6_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_M
        //20_6_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_N
        //20_6_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_O
        //20_6_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_P
        //20_6_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_Q
        //20_6_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_R
        //20_6_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_S
        //20_6_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_T
        //20_6_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oPassenger.LastName)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_I
        //20_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_Gender(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_Gender", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_J
        //20_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_K
        //20_7_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_L
        //20_7_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_M
        //20_7_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_N
        //20_7_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_O
        //20_7_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_P
        //20_7_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_Q
        //20_7_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_R
        //20_7_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_S
        //20_7_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_T
        //20_7_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_DateOfBirth, ConvertTo.ConvertEmptyToDBNull(oPassenger.DateOfBirth)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_J
        //20_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_Title(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_K
        //20_8_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_L
        //20_8_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_M
        //20_8_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_N
        //20_8_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_O
        //20_8_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_P
        //20_8_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_Q
        //20_8_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_R
        //20_8_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_S
        //20_8_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_T
        //20_8_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Gender, ConvertTo.ConvertEmptyToDBNull(oPassenger.Gender)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_K
        //20_9_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PassportNumber(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_L
        //20_9_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_M
        //20_9_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_N
        //20_9_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_O
        //20_9_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_P
        //20_9_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_Q
        //20_9_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_R
        //20_9_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_S
        //20_9_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_T
        //20_9_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oPassenger.Title)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_L
        //20_10_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_PassportExpiryDate(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PassportExpiryDate", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_M
        //20_10_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_N
        //20_10_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_O
        //20_10_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_P
        //20_10_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_Q
        //20_10_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_R
        //20_10_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_S
        //20_10_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_T
        //20_10_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportNumber, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportNumber)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_M
        //20_11_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_PassportIssueCountry(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_PassportIssueCountry", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_N
        //20_11_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_O
        //20_11_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_P
        //20_11_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_Q
        //20_11_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_R
        //20_11_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_S
        //20_11_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_T
        //20_11_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportExpiryDate, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportExpiryDate)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_N
        //20_12_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInTypeOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInTypeOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_O
        //20_12_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_P
        //20_12_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_Q
        //20_12_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_R
        //20_12_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_S
        //20_12_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_T
        //20_12_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PassportIssueCountry, ConvertTo.ConvertEmptyToDBNull(oPassenger.PassportIssueCountry)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_O
        //20_13_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInTypeReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInTypeReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_P
        //20_13_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_Q
        //20_13_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_R
        //20_13_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_S
        //20_13_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_T
        //20_13_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeOutward)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_P
        //20_14_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceOutward(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceOutward", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_Q
        //20_14_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_R
        //20_14_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_S
        //20_14_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_T
        //20_14_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInTypeReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInTypeReturn)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_Q
        //20_15_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_CheckInPriceReturn(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_CheckInPriceReturn", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_R
        //20_15_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_S
        //20_15_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_T
        //20_15_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceOutward, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceOutward)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q_R
        //20_16_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_PriceMarkUp(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q_S
        //20_16_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q_T
        //20_16_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_CheckInPriceReturn, ConvertTo.ConvertEmptyToDBNull(oPassenger.CheckInPriceReturn)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_R_S
        //20_17_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp_SeatPerFlightLegDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp_SeatPerFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_R_T
        //20_17_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oPassenger.PriceMarkUp)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_S_T
        //20_18_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeatPerFlightLegDocId_OrdersDocId(Passenger oPassenger)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Passenger.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.WhiteLabelDocId)); 
db.AddParameter(TBNames_Passenger.PRM_SeatPerFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.SeatPerFlightLegDocId)); 
db.AddParameter(TBNames_Passenger.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oPassenger.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Passenger, "Select_Passenger_By_Keys_View_WhiteLabelDocId_SeatPerFlightLegDocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
