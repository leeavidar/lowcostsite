

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Template  : ContainerItem<Template>{

#region CTOR

    #region Constractor
    static Template()
    {
        ConvertEvent = Template.OnConvert;
    }  
    //public KeyValuesContainer<Template> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Template()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.TemplateKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Template OnConvert(DataRow dr)
    {
        int LangId = Translator<Template>.LangId;            
        Template oTemplate = null;
        if (dr != null)
        {
            oTemplate = new Template();
            #region Create Object
            oTemplate.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Template.Field_DateCreated]);
             oTemplate.DocId = ConvertTo.ConvertToInt(dr[TBNames_Template.Field_DocId]);
             oTemplate.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Template.Field_IsDeleted]);
             oTemplate.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Template.Field_IsActive]);
             oTemplate.Title = ConvertTo.ConvertToString(dr[TBNames_Template.Field_Title]);
             oTemplate.SubTitle = ConvertTo.ConvertToString(dr[TBNames_Template.Field_SubTitle]);
             oTemplate.Type = ConvertTo.ConvertToString(dr[TBNames_Template.Field_Type]);
             oTemplate.Order = ConvertTo.ConvertToInt(dr[TBNames_Template.Field_Order]);
 
//FK     KeyWhiteLabelDocId
            oTemplate.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Template.Field_WhiteLabelDocId]);
             oTemplate.RelatedObjectType = ConvertTo.ConvertToInt(dr[TBNames_Template.Field_RelatedObjectType]);
             oTemplate.RelatedObjectDocId = ConvertTo.ConvertToInt(dr[TBNames_Template.Field_RelatedObjectDocId]);
             oTemplate.TextLong = ConvertTo.ConvertToString(dr[TBNames_Template.Field_TextLong]);
             oTemplate.TextShort = ConvertTo.ConvertToString(dr[TBNames_Template.Field_TextShort]);
 
            #endregion
            Translator<Template>.Translate(oTemplate.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oTemplate;
    }

    
#endregion

//#REP_HERE 
#region Template Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyTemplateDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyTemplateDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyTemplateIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyTemplateIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Title;
private string _title;
public String FriendlyTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateTitle);   
    }
}
public  string Title
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyTemplateTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateTitle, value);
         _title = ConvertToValue.ConvertToString(value);
        isSetOnce_Title = true;
    }
}


public string Title_Value
{
    get
    {
        //return _title; //ConvertToValue.ConvertToString(Title);
        if(isSetOnce_Title) {return _title;}
        else {return ConvertToValue.ConvertToString(Title);}
    }
}

public string Title_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_title).ToString();
               //if(isSetOnce_Title) {return ConvertToValue.ConvertToString(_title).ToString();}
               //else {return ConvertToValue.ConvertToString(Title).ToString();}
            ConvertToValue.ConvertToString(Title).ToString();
    }
}

private bool isSetOnce_SubTitle;
private string _sub_title;
public String FriendlySubTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateSubTitle);   
    }
}
public  string SubTitle
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyTemplateSubTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateSubTitle, value);
         _sub_title = ConvertToValue.ConvertToString(value);
        isSetOnce_SubTitle = true;
    }
}


public string SubTitle_Value
{
    get
    {
        //return _sub_title; //ConvertToValue.ConvertToString(SubTitle);
        if(isSetOnce_SubTitle) {return _sub_title;}
        else {return ConvertToValue.ConvertToString(SubTitle);}
    }
}

public string SubTitle_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_sub_title).ToString();
               //if(isSetOnce_SubTitle) {return ConvertToValue.ConvertToString(_sub_title).ToString();}
               //else {return ConvertToValue.ConvertToString(SubTitle).ToString();}
            ConvertToValue.ConvertToString(SubTitle).ToString();
    }
}

private bool isSetOnce_Type;
private string _type;
public String FriendlyType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateType);   
    }
}
public  string Type
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyTemplateType));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateType, value);
         _type = ConvertToValue.ConvertToString(value);
        isSetOnce_Type = true;
    }
}


public string Type_Value
{
    get
    {
        //return _type; //ConvertToValue.ConvertToString(Type);
        if(isSetOnce_Type) {return _type;}
        else {return ConvertToValue.ConvertToString(Type);}
    }
}

public string Type_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_type).ToString();
               //if(isSetOnce_Type) {return ConvertToValue.ConvertToString(_type).ToString();}
               //else {return ConvertToValue.ConvertToString(Type).ToString();}
            ConvertToValue.ConvertToString(Type).ToString();
    }
}

private bool isSetOnce_Order;
private int _order;
public String FriendlyOrder
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateOrder);   
    }
}
public  int? Order
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyTemplateOrder));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateOrder, value);
         _order = ConvertToValue.ConvertToInt(value);
        isSetOnce_Order = true;
    }
}


public int Order_Value
{
    get
    {
        //return _order; //ConvertToValue.ConvertToInt(Order);
        if(isSetOnce_Order) {return _order;}
        else {return ConvertToValue.ConvertToInt(Order);}
    }
}

public string Order_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order).ToString();
               //if(isSetOnce_Order) {return ConvertToValue.ConvertToInt(_order).ToString();}
               //else {return ConvertToValue.ConvertToInt(Order).ToString();}
            ConvertToValue.ConvertToInt(Order).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_RelatedObjectType;
private int _related_object_type;
public String FriendlyRelatedObjectType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateRelatedObjectType);   
    }
}
public  int? RelatedObjectType
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyTemplateRelatedObjectType));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateRelatedObjectType, value);
         _related_object_type = ConvertToValue.ConvertToInt(value);
        isSetOnce_RelatedObjectType = true;
    }
}


public int RelatedObjectType_Value
{
    get
    {
        //return _related_object_type; //ConvertToValue.ConvertToInt(RelatedObjectType);
        if(isSetOnce_RelatedObjectType) {return _related_object_type;}
        else {return ConvertToValue.ConvertToInt(RelatedObjectType);}
    }
}

public string RelatedObjectType_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_related_object_type).ToString();
               //if(isSetOnce_RelatedObjectType) {return ConvertToValue.ConvertToInt(_related_object_type).ToString();}
               //else {return ConvertToValue.ConvertToInt(RelatedObjectType).ToString();}
            ConvertToValue.ConvertToInt(RelatedObjectType).ToString();
    }
}

private bool isSetOnce_RelatedObjectDocId;
private int _related_object_doc_id;
public String FriendlyRelatedObjectDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateRelatedObjectDocId);   
    }
}
public  int? RelatedObjectDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyTemplateRelatedObjectDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateRelatedObjectDocId, value);
         _related_object_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_RelatedObjectDocId = true;
    }
}


public int RelatedObjectDocId_Value
{
    get
    {
        //return _related_object_doc_id; //ConvertToValue.ConvertToInt(RelatedObjectDocId);
        if(isSetOnce_RelatedObjectDocId) {return _related_object_doc_id;}
        else {return ConvertToValue.ConvertToInt(RelatedObjectDocId);}
    }
}

public string RelatedObjectDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_related_object_doc_id).ToString();
               //if(isSetOnce_RelatedObjectDocId) {return ConvertToValue.ConvertToInt(_related_object_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(RelatedObjectDocId).ToString();}
            ConvertToValue.ConvertToInt(RelatedObjectDocId).ToString();
    }
}

private bool isSetOnce_TextLong;
private string _text_long;
public String FriendlyTextLong
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateTextLong);   
    }
}
public  string TextLong
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyTemplateTextLong));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateTextLong, value);
         _text_long = ConvertToValue.ConvertToString(value);
        isSetOnce_TextLong = true;
    }
}


public string TextLong_Value
{
    get
    {
        //return _text_long; //ConvertToValue.ConvertToString(TextLong);
        if(isSetOnce_TextLong) {return _text_long;}
        else {return ConvertToValue.ConvertToString(TextLong);}
    }
}

public string TextLong_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text_long).ToString();
               //if(isSetOnce_TextLong) {return ConvertToValue.ConvertToString(_text_long).ToString();}
               //else {return ConvertToValue.ConvertToString(TextLong).ToString();}
            ConvertToValue.ConvertToString(TextLong).ToString();
    }
}

private bool isSetOnce_TextShort;
private string _text_short;
public String FriendlyTextShort
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateTextShort);   
    }
}
public  string TextShort
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyTemplateTextShort));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateTextShort, value);
         _text_short = ConvertToValue.ConvertToString(value);
        isSetOnce_TextShort = true;
    }
}


public string TextShort_Value
{
    get
    {
        //return _text_short; //ConvertToValue.ConvertToString(TextShort);
        if(isSetOnce_TextShort) {return _text_short;}
        else {return ConvertToValue.ConvertToString(TextShort);}
    }
}

public string TextShort_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text_short).ToString();
               //if(isSetOnce_TextShort) {return ConvertToValue.ConvertToString(_text_short).ToString();}
               //else {return ConvertToValue.ConvertToString(TextShort).ToString();}
            ConvertToValue.ConvertToString(TextShort).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //I_A
        //8_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //I_B
        //8_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C
        //8_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_E
        //8_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //I_F
        //8_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //I_G
        //8_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Type", ex.Message));

            }
            return paramsSelect;
        }



        //I_H
        //8_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //I_J
        //8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectType(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I_K
        //8_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_L
        //8_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_M
        //8_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_B
        //8_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_C
        //8_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_E
        //8_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Title", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_F
        //8_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SubTitle(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_G
        //8_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Type(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Type", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_H
        //8_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Order", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_J
        //8_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObjectType(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_K
        //8_0_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_L
        //8_0_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_M
        //8_0_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTemplate.DateCreated)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_C
        //8_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_E
        //8_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_F
        //8_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SubTitle(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_G
        //8_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Type(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Type", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_H
        //8_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_J
        //8_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_RelatedObjectType(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_K
        //8_1_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_L
        //8_1_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_M
        //8_1_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.DocId)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_DocId_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_E
        //8_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Title", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_F
        //8_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SubTitle(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_G
        //8_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Type(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Type", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_H
        //8_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Order", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_J
        //8_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObjectType(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_K
        //8_2_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_L
        //8_2_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_M
        //8_2_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTemplate.IsDeleted)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_F
        //8_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_SubTitle(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Title_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_G
        //8_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Type(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Title_Type", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_H
        //8_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Order(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Title_Order", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_J
        //8_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_RelatedObjectType(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Title_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_K
        //8_4_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Title_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_L
        //8_4_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Title_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_M
        //8_4_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTemplate.Title)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Title_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_G
        //8_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_Type(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_Type", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_H
        //8_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_Order(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_Order", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_J
        //8_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_RelatedObjectType(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_K
        //8_5_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_L
        //8_5_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_M
        //8_5_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oTemplate.SubTitle)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_H
        //8_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_Order(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Type_Order", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_J
        //8_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_RelatedObjectType(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Type_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_K
        //8_6_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Type_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_L
        //8_6_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Type_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_M
        //8_6_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oTemplate.Type)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Type_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_H_J
        //8_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_RelatedObjectType(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Order_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I_H_K
        //8_7_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Order_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_H_L
        //8_7_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Order_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_H_M
        //8_7_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTemplate.Order)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_Order_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_J_K
        //8_9_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_J_L
        //8_9_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectType_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_J_M
        //8_9_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectType_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectType)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_K_L
        //8_10_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectDocId_TextLong(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId_TextLong", ex.Message));

            }
            return paramsSelect;
        }



        //I_K_M
        //8_10_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectDocId_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.RelatedObjectDocId)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId_TextShort", ex.Message));

            }
            return paramsSelect;
        }



        //I_L_M
        //8_11_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLong_TextShort(Template oTemplate)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Template.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTemplate.WhiteLabelDocId)); 
db.AddParameter(TBNames_Template.PRM_TextLong, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextLong)); 
db.AddParameter(TBNames_Template.PRM_TextShort, ConvertTo.ConvertEmptyToDBNull(oTemplate.TextShort));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Template, "Select_Template_By_Keys_View_WhiteLabelDocId_TextLong_TextShort", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
