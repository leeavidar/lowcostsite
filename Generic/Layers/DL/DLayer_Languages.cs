

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Languages  : ContainerItem<Languages>{

#region CTOR

    #region Constractor
    static Languages()
    {
        ConvertEvent = Languages.OnConvert;
    }  
    //public KeyValuesContainer<Languages> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Languages()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.LanguagesKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Languages OnConvert(DataRow dr)
    {
        int LangId = Translator<Languages>.LangId;            
        Languages oLanguages = null;
        if (dr != null)
        {
            oLanguages = new Languages();
            #region Create Object
            oLanguages.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Languages.Field_DateCreated]);
             oLanguages.DocId = ConvertTo.ConvertToInt(dr[TBNames_Languages.Field_DocId]);
             oLanguages.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Languages.Field_IsDeleted]);
             oLanguages.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Languages.Field_IsActive]);
             oLanguages.Name = ConvertTo.ConvertToString(dr[TBNames_Languages.Field_Name]);
             oLanguages.Code = ConvertTo.ConvertToString(dr[TBNames_Languages.Field_Code]);
 
            #endregion
            Translator<Languages>.Translate(oLanguages.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oLanguages;
    }

    
#endregion

//#REP_HERE 
#region Languages Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLanguagesDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyLanguagesDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyLanguagesDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLanguagesDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyLanguagesDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyLanguagesDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLanguagesIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyLanguagesIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyLanguagesIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLanguagesIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyLanguagesIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyLanguagesIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLanguagesName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyLanguagesName));
    }
    set
    {
        SetKey(KeyValuesType.KeyLanguagesName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_Code;
private string _code;
public String FriendlyCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLanguagesCode);   
    }
}
public  string Code
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyLanguagesCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyLanguagesCode, value);
         _code = ConvertToValue.ConvertToString(value);
        isSetOnce_Code = true;
    }
}


public string Code_Value
{
    get
    {
        //return _code; //ConvertToValue.ConvertToString(Code);
        if(isSetOnce_Code) {return _code;}
        else {return ConvertToValue.ConvertToString(Code);}
    }
}

public string Code_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_code).ToString();
               //if(isSetOnce_Code) {return ConvertToValue.ConvertToString(_code).ToString();}
               //else {return ConvertToValue.ConvertToString(Code).ToString();}
            ConvertToValue.ConvertToString(Code).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLanguages.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLanguages.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLanguages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oLanguages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_Name", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Code(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oLanguages.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_Code", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLanguages.DateCreated)); 
db.AddParameter(TBNames_Languages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLanguages.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLanguages.DateCreated)); 
db.AddParameter(TBNames_Languages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLanguages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Name(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLanguages.DateCreated)); 
db.AddParameter(TBNames_Languages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oLanguages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Code(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLanguages.DateCreated)); 
db.AddParameter(TBNames_Languages.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oLanguages.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DateCreated_Code", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLanguages.DocId)); 
db.AddParameter(TBNames_Languages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLanguages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Name(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLanguages.DocId)); 
db.AddParameter(TBNames_Languages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oLanguages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Code(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLanguages.DocId)); 
db.AddParameter(TBNames_Languages.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oLanguages.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_DocId_Code", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Name(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLanguages.IsDeleted)); 
db.AddParameter(TBNames_Languages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oLanguages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Code(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLanguages.IsDeleted)); 
db.AddParameter(TBNames_Languages.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oLanguages.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_IsDeleted_Code", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_Code(Languages oLanguages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Languages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oLanguages.Name)); 
db.AddParameter(TBNames_Languages.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oLanguages.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Languages, "Select_Languages_By_Keys_View_Name_Code", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
