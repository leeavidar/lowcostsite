

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class SubBox  : ContainerItem<SubBox>{

#region CTOR

    #region Constractor
    static SubBox()
    {
        ConvertEvent = SubBox.OnConvert;
    }  
    //public KeyValuesContainer<SubBox> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public SubBox()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.SubBoxKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static SubBox OnConvert(DataRow dr)
    {
        int LangId = Translator<SubBox>.LangId;            
        SubBox oSubBox = null;
        if (dr != null)
        {
            oSubBox = new SubBox();
            #region Create Object
            oSubBox.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_SubBox.Field_DateCreated]);
             oSubBox.DocId = ConvertTo.ConvertToInt(dr[TBNames_SubBox.Field_DocId]);
             oSubBox.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_SubBox.Field_IsDeleted]);
             oSubBox.IsActive = ConvertTo.ConvertToBool(dr[TBNames_SubBox.Field_IsActive]);
             oSubBox.SubTitle = ConvertTo.ConvertToString(dr[TBNames_SubBox.Field_SubTitle]);
             oSubBox.ShortText = ConvertTo.ConvertToString(dr[TBNames_SubBox.Field_ShortText]);
             oSubBox.LongText = ConvertTo.ConvertToString(dr[TBNames_SubBox.Field_LongText]);
             oSubBox.Order = ConvertTo.ConvertToInt(dr[TBNames_SubBox.Field_Order]);
 
//FK     KeyTemplateDocId
            oSubBox.TemplateDocId = ConvertTo.ConvertToInt(dr[TBNames_SubBox.Field_TemplateDocId]);
 
//FK     KeyWhiteLabelDocId
            oSubBox.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_SubBox.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<SubBox>.Translate(oSubBox.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oSubBox;
    }

    
#endregion

//#REP_HERE 
#region SubBox Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySubBoxDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeySubBoxDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeySubBoxDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySubBoxDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySubBoxDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeySubBoxDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySubBoxIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeySubBoxIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeySubBoxIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySubBoxIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeySubBoxIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeySubBoxIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_SubTitle;
private string _sub_title;
public String FriendlySubTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySubBoxSubTitle);   
    }
}
public  string SubTitle
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySubBoxSubTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeySubBoxSubTitle, value);
         _sub_title = ConvertToValue.ConvertToString(value);
        isSetOnce_SubTitle = true;
    }
}


public string SubTitle_Value
{
    get
    {
        //return _sub_title; //ConvertToValue.ConvertToString(SubTitle);
        if(isSetOnce_SubTitle) {return _sub_title;}
        else {return ConvertToValue.ConvertToString(SubTitle);}
    }
}

public string SubTitle_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_sub_title).ToString();
               //if(isSetOnce_SubTitle) {return ConvertToValue.ConvertToString(_sub_title).ToString();}
               //else {return ConvertToValue.ConvertToString(SubTitle).ToString();}
            ConvertToValue.ConvertToString(SubTitle).ToString();
    }
}

private bool isSetOnce_ShortText;
private string _short_text;
public String FriendlyShortText
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySubBoxShortText);   
    }
}
public  string ShortText
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySubBoxShortText));
    }
    set
    {
        SetKey(KeyValuesType.KeySubBoxShortText, value);
         _short_text = ConvertToValue.ConvertToString(value);
        isSetOnce_ShortText = true;
    }
}


public string ShortText_Value
{
    get
    {
        //return _short_text; //ConvertToValue.ConvertToString(ShortText);
        if(isSetOnce_ShortText) {return _short_text;}
        else {return ConvertToValue.ConvertToString(ShortText);}
    }
}

public string ShortText_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_short_text).ToString();
               //if(isSetOnce_ShortText) {return ConvertToValue.ConvertToString(_short_text).ToString();}
               //else {return ConvertToValue.ConvertToString(ShortText).ToString();}
            ConvertToValue.ConvertToString(ShortText).ToString();
    }
}

private bool isSetOnce_LongText;
private string _long_text;
public String FriendlyLongText
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySubBoxLongText);   
    }
}
public  string LongText
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySubBoxLongText));
    }
    set
    {
        SetKey(KeyValuesType.KeySubBoxLongText, value);
         _long_text = ConvertToValue.ConvertToString(value);
        isSetOnce_LongText = true;
    }
}


public string LongText_Value
{
    get
    {
        //return _long_text; //ConvertToValue.ConvertToString(LongText);
        if(isSetOnce_LongText) {return _long_text;}
        else {return ConvertToValue.ConvertToString(LongText);}
    }
}

public string LongText_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_long_text).ToString();
               //if(isSetOnce_LongText) {return ConvertToValue.ConvertToString(_long_text).ToString();}
               //else {return ConvertToValue.ConvertToString(LongText).ToString();}
            ConvertToValue.ConvertToString(LongText).ToString();
    }
}

private bool isSetOnce_Order;
private int _order;
public String FriendlyOrder
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySubBoxOrder);   
    }
}
public  int? Order
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySubBoxOrder));
    }
    set
    {
        SetKey(KeyValuesType.KeySubBoxOrder, value);
         _order = ConvertToValue.ConvertToInt(value);
        isSetOnce_Order = true;
    }
}


public int Order_Value
{
    get
    {
        //return _order; //ConvertToValue.ConvertToInt(Order);
        if(isSetOnce_Order) {return _order;}
        else {return ConvertToValue.ConvertToInt(Order);}
    }
}

public string Order_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order).ToString();
               //if(isSetOnce_Order) {return ConvertToValue.ConvertToInt(_order).ToString();}
               //else {return ConvertToValue.ConvertToInt(Order).ToString();}
            ConvertToValue.ConvertToInt(Order).ToString();
    }
}

private bool isSetOnce_TemplateDocId;
private int _template_doc_id;
public String FriendlyTemplateDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTemplateDocId);   
    }
}
public  int? TemplateDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyTemplateDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyTemplateDocId, value);
         _template_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_TemplateDocId = true;
    }
}


public int TemplateDocId_Value
{
    get
    {
        //return _template_doc_id; //ConvertToValue.ConvertToInt(TemplateDocId);
        if(isSetOnce_TemplateDocId) {return _template_doc_id;}
        else {return ConvertToValue.ConvertToInt(TemplateDocId);}
    }
}

public string TemplateDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_template_doc_id).ToString();
               //if(isSetOnce_TemplateDocId) {return ConvertToValue.ConvertToInt(_template_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(TemplateDocId).ToString();}
            ConvertToValue.ConvertToInt(TemplateDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //J_A
        //9_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSubBox.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //J_B
        //9_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C
        //9_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSubBox.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_E
        //9_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oSubBox.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_F
        //9_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oSubBox.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //J_G
        //9_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LongText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oSubBox.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //J_H
        //9_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oSubBox.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //J_I
        //9_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TemplateDocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_TemplateDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.TemplateDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_TemplateDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_B
        //9_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSubBox.DateCreated)); 
db.AddParameter(TBNames_SubBox.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_C
        //9_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSubBox.DateCreated)); 
db.AddParameter(TBNames_SubBox.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSubBox.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_E
        //9_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SubTitle(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSubBox.DateCreated)); 
db.AddParameter(TBNames_SubBox.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oSubBox.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_F
        //9_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ShortText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSubBox.DateCreated)); 
db.AddParameter(TBNames_SubBox.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oSubBox.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_G
        //9_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LongText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSubBox.DateCreated)); 
db.AddParameter(TBNames_SubBox.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oSubBox.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_H
        //9_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSubBox.DateCreated)); 
db.AddParameter(TBNames_SubBox.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oSubBox.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_Order", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_I
        //9_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TemplateDocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSubBox.DateCreated)); 
db.AddParameter(TBNames_SubBox.PRM_TemplateDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.TemplateDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_TemplateDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_C
        //9_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.DocId)); 
db.AddParameter(TBNames_SubBox.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSubBox.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_E
        //9_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SubTitle(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.DocId)); 
db.AddParameter(TBNames_SubBox.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oSubBox.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_F
        //9_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ShortText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.DocId)); 
db.AddParameter(TBNames_SubBox.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oSubBox.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_G
        //9_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LongText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.DocId)); 
db.AddParameter(TBNames_SubBox.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oSubBox.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_H
        //9_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.DocId)); 
db.AddParameter(TBNames_SubBox.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oSubBox.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_I
        //9_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TemplateDocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.DocId)); 
db.AddParameter(TBNames_SubBox.PRM_TemplateDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.TemplateDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_TemplateDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_E
        //9_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SubTitle(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSubBox.IsDeleted)); 
db.AddParameter(TBNames_SubBox.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oSubBox.SubTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_SubTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_F
        //9_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ShortText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSubBox.IsDeleted)); 
db.AddParameter(TBNames_SubBox.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oSubBox.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_G
        //9_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LongText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSubBox.IsDeleted)); 
db.AddParameter(TBNames_SubBox.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oSubBox.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_H
        //9_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSubBox.IsDeleted)); 
db.AddParameter(TBNames_SubBox.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oSubBox.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_Order", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_I
        //9_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TemplateDocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSubBox.IsDeleted)); 
db.AddParameter(TBNames_SubBox.PRM_TemplateDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.TemplateDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_TemplateDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_F
        //9_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_ShortText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oSubBox.SubTitle)); 
db.AddParameter(TBNames_SubBox.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oSubBox.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_G
        //9_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_LongText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oSubBox.SubTitle)); 
db.AddParameter(TBNames_SubBox.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oSubBox.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_H
        //9_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_Order(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oSubBox.SubTitle)); 
db.AddParameter(TBNames_SubBox.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oSubBox.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_Order", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_I
        //9_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_TemplateDocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_SubTitle, ConvertTo.ConvertEmptyToDBNull(oSubBox.SubTitle)); 
db.AddParameter(TBNames_SubBox.PRM_TemplateDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.TemplateDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_TemplateDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_G
        //9_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_LongText(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oSubBox.ShortText)); 
db.AddParameter(TBNames_SubBox.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oSubBox.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_H
        //9_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_Order(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oSubBox.ShortText)); 
db.AddParameter(TBNames_SubBox.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oSubBox.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_Order", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_I
        //9_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_TemplateDocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oSubBox.ShortText)); 
db.AddParameter(TBNames_SubBox.PRM_TemplateDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.TemplateDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_TemplateDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_H
        //9_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LongText_Order(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oSubBox.LongText)); 
db.AddParameter(TBNames_SubBox.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oSubBox.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText_Order", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_I
        //9_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LongText_TemplateDocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oSubBox.LongText)); 
db.AddParameter(TBNames_SubBox.PRM_TemplateDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.TemplateDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText_TemplateDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_H_I
        //9_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_TemplateDocId(SubBox oSubBox)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SubBox.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.WhiteLabelDocId)); 
db.AddParameter(TBNames_SubBox.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oSubBox.Order)); 
db.AddParameter(TBNames_SubBox.PRM_TemplateDocId, ConvertTo.ConvertEmptyToDBNull(oSubBox.TemplateDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SubBox, "Select_SubBox_By_Keys_View_WhiteLabelDocId_Order_TemplateDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
