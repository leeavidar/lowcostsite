

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class SeatPerFlightLeg  : ContainerItem<SeatPerFlightLeg>{

#region CTOR

    #region Constractor
    static SeatPerFlightLeg()
    {
        ConvertEvent = SeatPerFlightLeg.OnConvert;
    }  
    //public KeyValuesContainer<SeatPerFlightLeg> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public SeatPerFlightLeg()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.SeatPerFlightLegKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static SeatPerFlightLeg OnConvert(DataRow dr)
    {
        int LangId = Translator<SeatPerFlightLeg>.LangId;            
        SeatPerFlightLeg oSeatPerFlightLeg = null;
        if (dr != null)
        {
            oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Create Object
            oSeatPerFlightLeg.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_SeatPerFlightLeg.Field_DateCreated]);
             oSeatPerFlightLeg.DocId = ConvertTo.ConvertToInt(dr[TBNames_SeatPerFlightLeg.Field_DocId]);
             oSeatPerFlightLeg.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_SeatPerFlightLeg.Field_IsDeleted]);
             oSeatPerFlightLeg.IsActive = ConvertTo.ConvertToBool(dr[TBNames_SeatPerFlightLeg.Field_IsActive]);
             oSeatPerFlightLeg.PassengerId = ConvertTo.ConvertToString(dr[TBNames_SeatPerFlightLeg.Field_PassengerId]);
             oSeatPerFlightLeg.Seat = ConvertTo.ConvertToString(dr[TBNames_SeatPerFlightLeg.Field_Seat]);
             oSeatPerFlightLeg.FlightNumber = ConvertTo.ConvertToString(dr[TBNames_SeatPerFlightLeg.Field_FlightNumber]);
 
//FK     KeyPassengerDocId
            oSeatPerFlightLeg.PassengerDocId = ConvertTo.ConvertToInt(dr[TBNames_SeatPerFlightLeg.Field_PassengerDocId]);
 
//FK     KeyWhiteLabelDocId
            oSeatPerFlightLeg.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_SeatPerFlightLeg.Field_WhiteLabelDocId]);
 
//FK     KeyFlightLegDocId
            oSeatPerFlightLeg.FlightLegDocId = ConvertTo.ConvertToInt(dr[TBNames_SeatPerFlightLeg.Field_FlightLegDocId]);
 
            #endregion
            Translator<SeatPerFlightLeg>.Translate(oSeatPerFlightLeg.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oSeatPerFlightLeg;
    }

    
#endregion

//#REP_HERE 
#region SeatPerFlightLeg Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeatPerFlightLegDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeySeatPerFlightLegDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeySeatPerFlightLegDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeatPerFlightLegDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySeatPerFlightLegDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeySeatPerFlightLegDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeatPerFlightLegIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeySeatPerFlightLegIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeySeatPerFlightLegIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeatPerFlightLegIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeySeatPerFlightLegIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeySeatPerFlightLegIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_PassengerId;
private string _passenger_id;
public String FriendlyPassengerId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeatPerFlightLegPassengerId);   
    }
}
public  string PassengerId
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySeatPerFlightLegPassengerId));
    }
    set
    {
        SetKey(KeyValuesType.KeySeatPerFlightLegPassengerId, value);
         _passenger_id = ConvertToValue.ConvertToString(value);
        isSetOnce_PassengerId = true;
    }
}


public string PassengerId_Value
{
    get
    {
        //return _passenger_id; //ConvertToValue.ConvertToString(PassengerId);
        if(isSetOnce_PassengerId) {return _passenger_id;}
        else {return ConvertToValue.ConvertToString(PassengerId);}
    }
}

public string PassengerId_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_passenger_id).ToString();
               //if(isSetOnce_PassengerId) {return ConvertToValue.ConvertToString(_passenger_id).ToString();}
               //else {return ConvertToValue.ConvertToString(PassengerId).ToString();}
            ConvertToValue.ConvertToString(PassengerId).ToString();
    }
}

private bool isSetOnce_Seat;
private string _seat;
public String FriendlySeat
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeatPerFlightLegSeat);   
    }
}
public  string Seat
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySeatPerFlightLegSeat));
    }
    set
    {
        SetKey(KeyValuesType.KeySeatPerFlightLegSeat, value);
         _seat = ConvertToValue.ConvertToString(value);
        isSetOnce_Seat = true;
    }
}


public string Seat_Value
{
    get
    {
        //return _seat; //ConvertToValue.ConvertToString(Seat);
        if(isSetOnce_Seat) {return _seat;}
        else {return ConvertToValue.ConvertToString(Seat);}
    }
}

public string Seat_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_seat).ToString();
               //if(isSetOnce_Seat) {return ConvertToValue.ConvertToString(_seat).ToString();}
               //else {return ConvertToValue.ConvertToString(Seat).ToString();}
            ConvertToValue.ConvertToString(Seat).ToString();
    }
}

private bool isSetOnce_FlightNumber;
private string _flight_number;
public String FriendlyFlightNumber
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeatPerFlightLegFlightNumber);   
    }
}
public  string FlightNumber
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySeatPerFlightLegFlightNumber));
    }
    set
    {
        SetKey(KeyValuesType.KeySeatPerFlightLegFlightNumber, value);
         _flight_number = ConvertToValue.ConvertToString(value);
        isSetOnce_FlightNumber = true;
    }
}


public string FlightNumber_Value
{
    get
    {
        //return _flight_number; //ConvertToValue.ConvertToString(FlightNumber);
        if(isSetOnce_FlightNumber) {return _flight_number;}
        else {return ConvertToValue.ConvertToString(FlightNumber);}
    }
}

public string FlightNumber_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_flight_number).ToString();
               //if(isSetOnce_FlightNumber) {return ConvertToValue.ConvertToString(_flight_number).ToString();}
               //else {return ConvertToValue.ConvertToString(FlightNumber).ToString();}
            ConvertToValue.ConvertToString(FlightNumber).ToString();
    }
}

private bool isSetOnce_PassengerDocId;
private int _passenger_doc_id;
public String FriendlyPassengerDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerDocId);   
    }
}
public  int? PassengerDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyPassengerDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerDocId, value);
         _passenger_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_PassengerDocId = true;
    }
}


public int PassengerDocId_Value
{
    get
    {
        //return _passenger_doc_id; //ConvertToValue.ConvertToInt(PassengerDocId);
        if(isSetOnce_PassengerDocId) {return _passenger_doc_id;}
        else {return ConvertToValue.ConvertToInt(PassengerDocId);}
    }
}

public string PassengerDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_passenger_doc_id).ToString();
               //if(isSetOnce_PassengerDocId) {return ConvertToValue.ConvertToInt(_passenger_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(PassengerDocId).ToString();}
            ConvertToValue.ConvertToInt(PassengerDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_FlightLegDocId;
private int _flight_leg_doc_id;
public String FriendlyFlightLegDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegDocId);   
    }
}
public  int? FlightLegDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyFlightLegDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegDocId, value);
         _flight_leg_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_FlightLegDocId = true;
    }
}


public int FlightLegDocId_Value
{
    get
    {
        //return _flight_leg_doc_id; //ConvertToValue.ConvertToInt(FlightLegDocId);
        if(isSetOnce_FlightLegDocId) {return _flight_leg_doc_id;}
        else {return ConvertToValue.ConvertToInt(FlightLegDocId);}
    }
}

public string FlightLegDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_flight_leg_doc_id).ToString();
               //if(isSetOnce_FlightLegDocId) {return ConvertToValue.ConvertToInt(_flight_leg_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(FlightLegDocId).ToString();}
            ConvertToValue.ConvertToInt(FlightLegDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //I_A
        //8_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //I_B
        //8_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C
        //8_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_E
        //8_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId", ex.Message));

            }
            return paramsSelect;
        }



        //I_F
        //8_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Seat(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_Seat, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.Seat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat", ex.Message));

            }
            return paramsSelect;
        }



        //I_G
        //8_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //I_H
        //8_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_J
        //8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightLegDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_B
        //8_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DateCreated)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_C
        //8_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DateCreated)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_E
        //8_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassengerId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DateCreated)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_F
        //8_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Seat(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DateCreated)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_Seat, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.Seat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Seat", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_G
        //8_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightNumber(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DateCreated)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_H
        //8_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassengerDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DateCreated)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_J
        //8_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightLegDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DateCreated)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_C
        //8_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_E
        //8_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassengerId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PassengerId", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_F
        //8_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Seat(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_Seat, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.Seat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Seat", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_G
        //8_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightNumber(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_H
        //8_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassengerDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_J
        //8_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightLegDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.DocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_E
        //8_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_F
        //8_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Seat(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_Seat, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.Seat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Seat", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_G
        //8_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightNumber(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_H
        //8_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_J
        //8_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightLegDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_F
        //8_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId_Seat(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_Seat, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.Seat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_Seat", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_G
        //8_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId_FlightNumber(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_H
        //8_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId_PassengerDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_J
        //8_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId_FlightLegDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_G
        //8_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Seat_FlightNumber(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_Seat, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.Seat)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_H
        //8_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Seat_PassengerDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_Seat, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.Seat)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_J
        //8_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Seat_FlightLegDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_Seat, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.Seat)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_H
        //8_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_PassengerDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_J
        //8_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_FlightLegDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_H_J
        //8_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerDocId_FlightLegDocId(SeatPerFlightLeg oSeatPerFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_SeatPerFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.PassengerDocId)); 
db.AddParameter(TBNames_SeatPerFlightLeg.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oSeatPerFlightLeg.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.SeatPerFlightLeg, "Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerDocId_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
