

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class ContactDetails  : ContainerItem<ContactDetails>{

#region CTOR

    #region Constractor
    static ContactDetails()
    {
        ConvertEvent = ContactDetails.OnConvert;
    }  
    //public KeyValuesContainer<ContactDetails> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public ContactDetails()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.ContactDetailsKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static ContactDetails OnConvert(DataRow dr)
    {
        int LangId = Translator<ContactDetails>.LangId;            
        ContactDetails oContactDetails = null;
        if (dr != null)
        {
            oContactDetails = new ContactDetails();
            #region Create Object
            oContactDetails.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_ContactDetails.Field_DateCreated]);
             oContactDetails.DocId = ConvertTo.ConvertToInt(dr[TBNames_ContactDetails.Field_DocId]);
             oContactDetails.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_ContactDetails.Field_IsDeleted]);
             oContactDetails.IsActive = ConvertTo.ConvertToBool(dr[TBNames_ContactDetails.Field_IsActive]);
             oContactDetails.FirstName = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_FirstName]);
             oContactDetails.LastName = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_LastName]);
             oContactDetails.Company = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_Company]);
             oContactDetails.Flat = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_Flat]);
             oContactDetails.BuildingName = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_BuildingName]);
             oContactDetails.BuildingNumber = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_BuildingNumber]);
             oContactDetails.Street = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_Street]);
             oContactDetails.Locality = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_Locality]);
             oContactDetails.City = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_City]);
             oContactDetails.Province = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_Province]);
             oContactDetails.ZipCode = ConvertTo.ConvertToInt(dr[TBNames_ContactDetails.Field_ZipCode]);
             oContactDetails.CountryCode = ConvertTo.ConvertToInt(dr[TBNames_ContactDetails.Field_CountryCode]);
             oContactDetails.Email = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_Email]);
             oContactDetails.MainPhone = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_MainPhone]);
             oContactDetails.MobliePhone = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_MobliePhone]);
 
//FK     KeyOrdersDocId
            oContactDetails.OrdersDocId = ConvertTo.ConvertToInt(dr[TBNames_ContactDetails.Field_OrdersDocId]);
 
//FK     KeyWhiteLabelDocId
            oContactDetails.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_ContactDetails.Field_WhiteLabelDocId]);
             oContactDetails.Title = ConvertTo.ConvertToString(dr[TBNames_ContactDetails.Field_Title]);
 
            #endregion
            Translator<ContactDetails>.Translate(oContactDetails.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oContactDetails;
    }

    
#endregion

//#REP_HERE 
#region ContactDetails Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyContactDetailsDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyContactDetailsDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyContactDetailsIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyContactDetailsIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_FirstName;
private string _first_name;
public String FriendlyFirstName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsFirstName);   
    }
}
public  string FirstName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsFirstName));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsFirstName, value);
         _first_name = ConvertToValue.ConvertToString(value);
        isSetOnce_FirstName = true;
    }
}


public string FirstName_Value
{
    get
    {
        //return _first_name; //ConvertToValue.ConvertToString(FirstName);
        if(isSetOnce_FirstName) {return _first_name;}
        else {return ConvertToValue.ConvertToString(FirstName);}
    }
}

public string FirstName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_first_name).ToString();
               //if(isSetOnce_FirstName) {return ConvertToValue.ConvertToString(_first_name).ToString();}
               //else {return ConvertToValue.ConvertToString(FirstName).ToString();}
            ConvertToValue.ConvertToString(FirstName).ToString();
    }
}

private bool isSetOnce_LastName;
private string _last_name;
public String FriendlyLastName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsLastName);   
    }
}
public  string LastName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsLastName));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsLastName, value);
         _last_name = ConvertToValue.ConvertToString(value);
        isSetOnce_LastName = true;
    }
}


public string LastName_Value
{
    get
    {
        //return _last_name; //ConvertToValue.ConvertToString(LastName);
        if(isSetOnce_LastName) {return _last_name;}
        else {return ConvertToValue.ConvertToString(LastName);}
    }
}

public string LastName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_last_name).ToString();
               //if(isSetOnce_LastName) {return ConvertToValue.ConvertToString(_last_name).ToString();}
               //else {return ConvertToValue.ConvertToString(LastName).ToString();}
            ConvertToValue.ConvertToString(LastName).ToString();
    }
}

private bool isSetOnce_Company;
private string _company;
public String FriendlyCompany
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsCompany);   
    }
}
public  string Company
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsCompany));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsCompany, value);
         _company = ConvertToValue.ConvertToString(value);
        isSetOnce_Company = true;
    }
}


public string Company_Value
{
    get
    {
        //return _company; //ConvertToValue.ConvertToString(Company);
        if(isSetOnce_Company) {return _company;}
        else {return ConvertToValue.ConvertToString(Company);}
    }
}

public string Company_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_company).ToString();
               //if(isSetOnce_Company) {return ConvertToValue.ConvertToString(_company).ToString();}
               //else {return ConvertToValue.ConvertToString(Company).ToString();}
            ConvertToValue.ConvertToString(Company).ToString();
    }
}

private bool isSetOnce_Flat;
private string _flat;
public String FriendlyFlat
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsFlat);   
    }
}
public  string Flat
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsFlat));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsFlat, value);
         _flat = ConvertToValue.ConvertToString(value);
        isSetOnce_Flat = true;
    }
}


public string Flat_Value
{
    get
    {
        //return _flat; //ConvertToValue.ConvertToString(Flat);
        if(isSetOnce_Flat) {return _flat;}
        else {return ConvertToValue.ConvertToString(Flat);}
    }
}

public string Flat_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_flat).ToString();
               //if(isSetOnce_Flat) {return ConvertToValue.ConvertToString(_flat).ToString();}
               //else {return ConvertToValue.ConvertToString(Flat).ToString();}
            ConvertToValue.ConvertToString(Flat).ToString();
    }
}

private bool isSetOnce_BuildingName;
private string _building_name;
public String FriendlyBuildingName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsBuildingName);   
    }
}
public  string BuildingName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsBuildingName));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsBuildingName, value);
         _building_name = ConvertToValue.ConvertToString(value);
        isSetOnce_BuildingName = true;
    }
}


public string BuildingName_Value
{
    get
    {
        //return _building_name; //ConvertToValue.ConvertToString(BuildingName);
        if(isSetOnce_BuildingName) {return _building_name;}
        else {return ConvertToValue.ConvertToString(BuildingName);}
    }
}

public string BuildingName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_building_name).ToString();
               //if(isSetOnce_BuildingName) {return ConvertToValue.ConvertToString(_building_name).ToString();}
               //else {return ConvertToValue.ConvertToString(BuildingName).ToString();}
            ConvertToValue.ConvertToString(BuildingName).ToString();
    }
}

private bool isSetOnce_BuildingNumber;
private string _building_number;
public String FriendlyBuildingNumber
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsBuildingNumber);   
    }
}
public  string BuildingNumber
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsBuildingNumber));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsBuildingNumber, value);
         _building_number = ConvertToValue.ConvertToString(value);
        isSetOnce_BuildingNumber = true;
    }
}


public string BuildingNumber_Value
{
    get
    {
        //return _building_number; //ConvertToValue.ConvertToString(BuildingNumber);
        if(isSetOnce_BuildingNumber) {return _building_number;}
        else {return ConvertToValue.ConvertToString(BuildingNumber);}
    }
}

public string BuildingNumber_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_building_number).ToString();
               //if(isSetOnce_BuildingNumber) {return ConvertToValue.ConvertToString(_building_number).ToString();}
               //else {return ConvertToValue.ConvertToString(BuildingNumber).ToString();}
            ConvertToValue.ConvertToString(BuildingNumber).ToString();
    }
}

private bool isSetOnce_Street;
private string _street;
public String FriendlyStreet
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsStreet);   
    }
}
public  string Street
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsStreet));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsStreet, value);
         _street = ConvertToValue.ConvertToString(value);
        isSetOnce_Street = true;
    }
}


public string Street_Value
{
    get
    {
        //return _street; //ConvertToValue.ConvertToString(Street);
        if(isSetOnce_Street) {return _street;}
        else {return ConvertToValue.ConvertToString(Street);}
    }
}

public string Street_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_street).ToString();
               //if(isSetOnce_Street) {return ConvertToValue.ConvertToString(_street).ToString();}
               //else {return ConvertToValue.ConvertToString(Street).ToString();}
            ConvertToValue.ConvertToString(Street).ToString();
    }
}

private bool isSetOnce_Locality;
private string _locality;
public String FriendlyLocality
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsLocality);   
    }
}
public  string Locality
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsLocality));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsLocality, value);
         _locality = ConvertToValue.ConvertToString(value);
        isSetOnce_Locality = true;
    }
}


public string Locality_Value
{
    get
    {
        //return _locality; //ConvertToValue.ConvertToString(Locality);
        if(isSetOnce_Locality) {return _locality;}
        else {return ConvertToValue.ConvertToString(Locality);}
    }
}

public string Locality_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_locality).ToString();
               //if(isSetOnce_Locality) {return ConvertToValue.ConvertToString(_locality).ToString();}
               //else {return ConvertToValue.ConvertToString(Locality).ToString();}
            ConvertToValue.ConvertToString(Locality).ToString();
    }
}

private bool isSetOnce_City;
private string _city;
public String FriendlyCity
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsCity);   
    }
}
public  string City
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsCity));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsCity, value);
         _city = ConvertToValue.ConvertToString(value);
        isSetOnce_City = true;
    }
}


public string City_Value
{
    get
    {
        //return _city; //ConvertToValue.ConvertToString(City);
        if(isSetOnce_City) {return _city;}
        else {return ConvertToValue.ConvertToString(City);}
    }
}

public string City_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_city).ToString();
               //if(isSetOnce_City) {return ConvertToValue.ConvertToString(_city).ToString();}
               //else {return ConvertToValue.ConvertToString(City).ToString();}
            ConvertToValue.ConvertToString(City).ToString();
    }
}

private bool isSetOnce_Province;
private string _province;
public String FriendlyProvince
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsProvince);   
    }
}
public  string Province
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsProvince));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsProvince, value);
         _province = ConvertToValue.ConvertToString(value);
        isSetOnce_Province = true;
    }
}


public string Province_Value
{
    get
    {
        //return _province; //ConvertToValue.ConvertToString(Province);
        if(isSetOnce_Province) {return _province;}
        else {return ConvertToValue.ConvertToString(Province);}
    }
}

public string Province_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_province).ToString();
               //if(isSetOnce_Province) {return ConvertToValue.ConvertToString(_province).ToString();}
               //else {return ConvertToValue.ConvertToString(Province).ToString();}
            ConvertToValue.ConvertToString(Province).ToString();
    }
}

private bool isSetOnce_ZipCode;
private int _zip_code;
public String FriendlyZipCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsZipCode);   
    }
}
public  int? ZipCode
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyContactDetailsZipCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsZipCode, value);
         _zip_code = ConvertToValue.ConvertToInt(value);
        isSetOnce_ZipCode = true;
    }
}


public int ZipCode_Value
{
    get
    {
        //return _zip_code; //ConvertToValue.ConvertToInt(ZipCode);
        if(isSetOnce_ZipCode) {return _zip_code;}
        else {return ConvertToValue.ConvertToInt(ZipCode);}
    }
}

public string ZipCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_zip_code).ToString();
               //if(isSetOnce_ZipCode) {return ConvertToValue.ConvertToInt(_zip_code).ToString();}
               //else {return ConvertToValue.ConvertToInt(ZipCode).ToString();}
            ConvertToValue.ConvertToInt(ZipCode).ToString();
    }
}

private bool isSetOnce_CountryCode;
private int _country_code;
public String FriendlyCountryCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsCountryCode);   
    }
}
public  int? CountryCode
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyContactDetailsCountryCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsCountryCode, value);
         _country_code = ConvertToValue.ConvertToInt(value);
        isSetOnce_CountryCode = true;
    }
}


public int CountryCode_Value
{
    get
    {
        //return _country_code; //ConvertToValue.ConvertToInt(CountryCode);
        if(isSetOnce_CountryCode) {return _country_code;}
        else {return ConvertToValue.ConvertToInt(CountryCode);}
    }
}

public string CountryCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_country_code).ToString();
               //if(isSetOnce_CountryCode) {return ConvertToValue.ConvertToInt(_country_code).ToString();}
               //else {return ConvertToValue.ConvertToInt(CountryCode).ToString();}
            ConvertToValue.ConvertToInt(CountryCode).ToString();
    }
}

private bool isSetOnce_Email;
private string _email;
public String FriendlyEmail
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsEmail);   
    }
}
public  string Email
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsEmail));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsEmail, value);
         _email = ConvertToValue.ConvertToString(value);
        isSetOnce_Email = true;
    }
}


public string Email_Value
{
    get
    {
        //return _email; //ConvertToValue.ConvertToString(Email);
        if(isSetOnce_Email) {return _email;}
        else {return ConvertToValue.ConvertToString(Email);}
    }
}

public string Email_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_email).ToString();
               //if(isSetOnce_Email) {return ConvertToValue.ConvertToString(_email).ToString();}
               //else {return ConvertToValue.ConvertToString(Email).ToString();}
            ConvertToValue.ConvertToString(Email).ToString();
    }
}

private bool isSetOnce_MainPhone;
private string _main_phone;
public String FriendlyMainPhone
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsMainPhone);   
    }
}
public  string MainPhone
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsMainPhone));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsMainPhone, value);
         _main_phone = ConvertToValue.ConvertToString(value);
        isSetOnce_MainPhone = true;
    }
}


public string MainPhone_Value
{
    get
    {
        //return _main_phone; //ConvertToValue.ConvertToString(MainPhone);
        if(isSetOnce_MainPhone) {return _main_phone;}
        else {return ConvertToValue.ConvertToString(MainPhone);}
    }
}

public string MainPhone_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_main_phone).ToString();
               //if(isSetOnce_MainPhone) {return ConvertToValue.ConvertToString(_main_phone).ToString();}
               //else {return ConvertToValue.ConvertToString(MainPhone).ToString();}
            ConvertToValue.ConvertToString(MainPhone).ToString();
    }
}

private bool isSetOnce_MobliePhone;
private string _moblie_phone;
public String FriendlyMobliePhone
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsMobliePhone);   
    }
}
public  string MobliePhone
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsMobliePhone));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsMobliePhone, value);
         _moblie_phone = ConvertToValue.ConvertToString(value);
        isSetOnce_MobliePhone = true;
    }
}


public string MobliePhone_Value
{
    get
    {
        //return _moblie_phone; //ConvertToValue.ConvertToString(MobliePhone);
        if(isSetOnce_MobliePhone) {return _moblie_phone;}
        else {return ConvertToValue.ConvertToString(MobliePhone);}
    }
}

public string MobliePhone_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_moblie_phone).ToString();
               //if(isSetOnce_MobliePhone) {return ConvertToValue.ConvertToString(_moblie_phone).ToString();}
               //else {return ConvertToValue.ConvertToString(MobliePhone).ToString();}
            ConvertToValue.ConvertToString(MobliePhone).ToString();
    }
}

private bool isSetOnce_OrdersDocId;
private int _orders_doc_id;
public String FriendlyOrdersDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersDocId);   
    }
}
public  int? OrdersDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersDocId, value);
         _orders_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrdersDocId = true;
    }
}


public int OrdersDocId_Value
{
    get
    {
        //return _orders_doc_id; //ConvertToValue.ConvertToInt(OrdersDocId);
        if(isSetOnce_OrdersDocId) {return _orders_doc_id;}
        else {return ConvertToValue.ConvertToInt(OrdersDocId);}
    }
}

public string OrdersDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_orders_doc_id).ToString();
               //if(isSetOnce_OrdersDocId) {return ConvertToValue.ConvertToInt(_orders_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrdersDocId).ToString();}
            ConvertToValue.ConvertToInt(OrdersDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_Title;
private string _title;
public String FriendlyTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsTitle);   
    }
}
public  string Title
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyContactDetailsTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsTitle, value);
         _title = ConvertToValue.ConvertToString(value);
        isSetOnce_Title = true;
    }
}


public string Title_Value
{
    get
    {
        //return _title; //ConvertToValue.ConvertToString(Title);
        if(isSetOnce_Title) {return _title;}
        else {return ConvertToValue.ConvertToString(Title);}
    }
}

public string Title_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_title).ToString();
               //if(isSetOnce_Title) {return ConvertToValue.ConvertToString(_title).ToString();}
               //else {return ConvertToValue.ConvertToString(Title).ToString();}
            ConvertToValue.ConvertToString(Title).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //U_A
        //20_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //U_B
        //20_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_C
        //20_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //U_E
        //20_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_F
        //20_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_G
        //20_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company", ex.Message));

            }
            return paramsSelect;
        }



        //U_H
        //20_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat", ex.Message));

            }
            return paramsSelect;
        }



        //U_I
        //20_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName", ex.Message));

            }
            return paramsSelect;
        }



        //U_J
        //20_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_K
        //20_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_L
        //20_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_M
        //20_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_N
        //20_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_O
        //20_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_P
        //20_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q
        //20_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_R
        //20_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_S
        //20_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_T
        //20_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_V
        //20_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_B
        //20_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_C
        //20_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_E
        //20_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_F
        //20_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LastName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_G
        //20_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Company(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Company", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_H
        //20_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Flat(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Flat", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_I
        //20_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_BuildingName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_BuildingName", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_J
        //20_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_K
        //20_0_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_L
        //20_0_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_M
        //20_0_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_N
        //20_0_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_O
        //20_0_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_P
        //20_0_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_Q
        //20_0_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_R
        //20_0_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_S
        //20_0_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_T
        //20_0_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_A_V
        //20_0_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DateCreated)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_C
        //20_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_E
        //20_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FirstName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_F
        //20_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LastName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_G
        //20_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Company(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Company", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_H
        //20_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Flat(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Flat", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_I
        //20_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_BuildingName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_BuildingName", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_J
        //20_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_K
        //20_1_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_L
        //20_1_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_M
        //20_1_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_N
        //20_1_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_O
        //20_1_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_P
        //20_1_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_Q
        //20_1_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_R
        //20_1_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_S
        //20_1_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_T
        //20_1_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_B_V
        //20_1_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.DocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_E
        //20_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_F
        //20_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_G
        //20_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Company(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Company", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_H
        //20_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Flat(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Flat", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_I
        //20_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_BuildingName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_BuildingName", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_J
        //20_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_K
        //20_2_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_L
        //20_2_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_M
        //20_2_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_N
        //20_2_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_O
        //20_2_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_P
        //20_2_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_Q
        //20_2_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_R
        //20_2_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_S
        //20_2_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_T
        //20_2_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_C_V
        //20_2_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oContactDetails.IsDeleted)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_F
        //20_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_LastName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_G
        //20_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Company(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Company", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_H
        //20_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Flat(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Flat", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_I
        //20_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_BuildingName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_BuildingName", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_J
        //20_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_K
        //20_4_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_L
        //20_4_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_M
        //20_4_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_N
        //20_4_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_O
        //20_4_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_P
        //20_4_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_Q
        //20_4_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_R
        //20_4_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_S
        //20_4_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_T
        //20_4_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_E_V
        //20_4_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.FirstName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_G
        //20_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Company(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Company", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_H
        //20_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Flat(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Flat", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_I
        //20_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_BuildingName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_BuildingName", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_J
        //20_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_K
        //20_5_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_L
        //20_5_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_M
        //20_5_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_N
        //20_5_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_O
        //20_5_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_P
        //20_5_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_Q
        //20_5_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_R
        //20_5_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_S
        //20_5_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_T
        //20_5_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_F_V
        //20_5_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.LastName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_H
        //20_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Flat(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Flat", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_I
        //20_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_BuildingName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_BuildingName", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_J
        //20_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_K
        //20_6_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_L
        //20_6_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_M
        //20_6_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_N
        //20_6_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_O
        //20_6_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_P
        //20_6_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_Q
        //20_6_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_R
        //20_6_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_S
        //20_6_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_T
        //20_6_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_G_V
        //20_6_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Company, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Company)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_I
        //20_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_BuildingName(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_BuildingName", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_J
        //20_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_K
        //20_7_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_L
        //20_7_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_M
        //20_7_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_N
        //20_7_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_O
        //20_7_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_P
        //20_7_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_Q
        //20_7_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_R
        //20_7_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_S
        //20_7_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_T
        //20_7_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_H_V
        //20_7_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Flat, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Flat)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_J
        //20_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_BuildingNumber(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_BuildingNumber", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_K
        //20_8_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_L
        //20_8_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_M
        //20_8_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_N
        //20_8_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_O
        //20_8_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_P
        //20_8_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_Q
        //20_8_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_R
        //20_8_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_S
        //20_8_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_T
        //20_8_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_I_V
        //20_8_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingName, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingName)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_K
        //20_9_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Street(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Street", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_L
        //20_9_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_M
        //20_9_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_N
        //20_9_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_O
        //20_9_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_P
        //20_9_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_Q
        //20_9_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_R
        //20_9_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_S
        //20_9_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_T
        //20_9_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_J_V
        //20_9_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_BuildingNumber, ConvertTo.ConvertEmptyToDBNull(oContactDetails.BuildingNumber)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_L
        //20_10_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_Locality(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Locality", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_M
        //20_10_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_N
        //20_10_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_O
        //20_10_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_P
        //20_10_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_Q
        //20_10_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_R
        //20_10_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_S
        //20_10_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_T
        //20_10_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_K_V
        //20_10_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Street_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Street, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Street)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_M
        //20_11_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_City(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_City", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_N
        //20_11_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_O
        //20_11_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_P
        //20_11_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_Q
        //20_11_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_R
        //20_11_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_S
        //20_11_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_T
        //20_11_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_L_V
        //20_11_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Locality, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Locality)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_N
        //20_12_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City_Province(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Province", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_O
        //20_12_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_P
        //20_12_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_Q
        //20_12_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_R
        //20_12_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_S
        //20_12_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_T
        //20_12_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_M_V
        //20_12_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_City_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_City, ConvertTo.ConvertEmptyToDBNull(oContactDetails.City)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_O
        //20_13_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Province_ZipCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_ZipCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_P
        //20_13_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Province_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_Q
        //20_13_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Province_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_R
        //20_13_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Province_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_S
        //20_13_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Province_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_T
        //20_13_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Province_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_N_V
        //20_13_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Province_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Province, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Province)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_P
        //20_14_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_CountryCode(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_Q
        //20_14_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_R
        //20_14_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_S
        //20_14_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_T
        //20_14_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_O_V
        //20_14_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_ZipCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.ZipCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_Q
        //20_15_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_Email(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_Email", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_R
        //20_15_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_S
        //20_15_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_T
        //20_15_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_P_V
        //20_15_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oContactDetails.CountryCode)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q_R
        //20_16_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email_MainPhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_MainPhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q_S
        //20_16_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q_T
        //20_16_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_Q_V
        //20_16_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Email)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_R_S
        //20_17_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MainPhone_MobliePhone(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_MobliePhone", ex.Message));

            }
            return paramsSelect;
        }



        //U_R_T
        //20_17_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MainPhone_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_R_V
        //20_17_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MainPhone_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MainPhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MainPhone)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_S_T
        //20_18_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MobliePhone_OrdersDocId(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //U_S_V
        //20_18_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MobliePhone_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_MobliePhone, ConvertTo.ConvertEmptyToDBNull(oContactDetails.MobliePhone)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone_Title", ex.Message));

            }
            return paramsSelect;
        }



        //U_T_V
        //20_19_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId_Title(ContactDetails oContactDetails)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_ContactDetails.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.WhiteLabelDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oContactDetails.OrdersDocId)); 
db.AddParameter(TBNames_ContactDetails.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oContactDetails.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.ContactDetails, "Select_ContactDetails_By_Keys_View_WhiteLabelDocId_OrdersDocId_Title", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
