

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Seo  : ContainerItem<Seo>{

#region CTOR

    #region Constractor
    static Seo()
    {
        ConvertEvent = Seo.OnConvert;
    }  
    //public KeyValuesContainer<Seo> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Seo()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.SeoKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Seo OnConvert(DataRow dr)
    {
        int LangId = Translator<Seo>.LangId;            
        Seo oSeo = null;
        if (dr != null)
        {
            oSeo = new Seo();
            #region Create Object
            oSeo.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Seo.Field_DateCreated]);
             oSeo.DocId = ConvertTo.ConvertToInt(dr[TBNames_Seo.Field_DocId]);
             oSeo.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Seo.Field_IsDeleted]);
             oSeo.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Seo.Field_IsActive]);
             oSeo.FriendlyUrl = ConvertTo.ConvertToString(dr[TBNames_Seo.Field_FriendlyUrl]);
             oSeo.SeoTitle = ConvertTo.ConvertToString(dr[TBNames_Seo.Field_SeoTitle]);
             oSeo.SeoDescription = ConvertTo.ConvertToString(dr[TBNames_Seo.Field_SeoDescription]);
             oSeo.SeoKeyWords = ConvertTo.ConvertToString(dr[TBNames_Seo.Field_SeoKeyWords]);
             oSeo.RelatedObject = ConvertTo.ConvertToInt(dr[TBNames_Seo.Field_RelatedObject]);
 
//FK     KeyWhiteLabelDocId
            oSeo.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Seo.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<Seo>.Translate(oSeo.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oSeo;
    }

    
#endregion

//#REP_HERE 
#region Seo Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeySeoDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySeoDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeySeoIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeySeoIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_FriendlyUrl;
private string _friendly_url;
public String FriendlyFriendlyUrl
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoFriendlyUrl);   
    }
}
public  string FriendlyUrl
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySeoFriendlyUrl));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoFriendlyUrl, value);
         _friendly_url = ConvertToValue.ConvertToString(value);
        isSetOnce_FriendlyUrl = true;
    }
}


public string FriendlyUrl_Value
{
    get
    {
        //return _friendly_url; //ConvertToValue.ConvertToString(FriendlyUrl);
        if(isSetOnce_FriendlyUrl) {return _friendly_url;}
        else {return ConvertToValue.ConvertToString(FriendlyUrl);}
    }
}

public string FriendlyUrl_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_friendly_url).ToString();
               //if(isSetOnce_FriendlyUrl) {return ConvertToValue.ConvertToString(_friendly_url).ToString();}
               //else {return ConvertToValue.ConvertToString(FriendlyUrl).ToString();}
            ConvertToValue.ConvertToString(FriendlyUrl).ToString();
    }
}

private bool isSetOnce_SeoTitle;
private string _seo_title;
public String FriendlySeoTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoSeoTitle);   
    }
}
public  string SeoTitle
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySeoSeoTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoSeoTitle, value);
         _seo_title = ConvertToValue.ConvertToString(value);
        isSetOnce_SeoTitle = true;
    }
}


public string SeoTitle_Value
{
    get
    {
        //return _seo_title; //ConvertToValue.ConvertToString(SeoTitle);
        if(isSetOnce_SeoTitle) {return _seo_title;}
        else {return ConvertToValue.ConvertToString(SeoTitle);}
    }
}

public string SeoTitle_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_seo_title).ToString();
               //if(isSetOnce_SeoTitle) {return ConvertToValue.ConvertToString(_seo_title).ToString();}
               //else {return ConvertToValue.ConvertToString(SeoTitle).ToString();}
            ConvertToValue.ConvertToString(SeoTitle).ToString();
    }
}

private bool isSetOnce_SeoDescription;
private string _seo_description;
public String FriendlySeoDescription
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoSeoDescription);   
    }
}
public  string SeoDescription
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySeoSeoDescription));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoSeoDescription, value);
         _seo_description = ConvertToValue.ConvertToString(value);
        isSetOnce_SeoDescription = true;
    }
}


public string SeoDescription_Value
{
    get
    {
        //return _seo_description; //ConvertToValue.ConvertToString(SeoDescription);
        if(isSetOnce_SeoDescription) {return _seo_description;}
        else {return ConvertToValue.ConvertToString(SeoDescription);}
    }
}

public string SeoDescription_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_seo_description).ToString();
               //if(isSetOnce_SeoDescription) {return ConvertToValue.ConvertToString(_seo_description).ToString();}
               //else {return ConvertToValue.ConvertToString(SeoDescription).ToString();}
            ConvertToValue.ConvertToString(SeoDescription).ToString();
    }
}

private bool isSetOnce_SeoKeyWords;
private string _seo_key_words;
public String FriendlySeoKeyWords
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoSeoKeyWords);   
    }
}
public  string SeoKeyWords
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeySeoSeoKeyWords));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoSeoKeyWords, value);
         _seo_key_words = ConvertToValue.ConvertToString(value);
        isSetOnce_SeoKeyWords = true;
    }
}


public string SeoKeyWords_Value
{
    get
    {
        //return _seo_key_words; //ConvertToValue.ConvertToString(SeoKeyWords);
        if(isSetOnce_SeoKeyWords) {return _seo_key_words;}
        else {return ConvertToValue.ConvertToString(SeoKeyWords);}
    }
}

public string SeoKeyWords_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_seo_key_words).ToString();
               //if(isSetOnce_SeoKeyWords) {return ConvertToValue.ConvertToString(_seo_key_words).ToString();}
               //else {return ConvertToValue.ConvertToString(SeoKeyWords).ToString();}
            ConvertToValue.ConvertToString(SeoKeyWords).ToString();
    }
}

private bool isSetOnce_RelatedObject;
private int _related_object;
public String FriendlyRelatedObject
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoRelatedObject);   
    }
}
public  int? RelatedObject
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySeoRelatedObject));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoRelatedObject, value);
         _related_object = ConvertToValue.ConvertToInt(value);
        isSetOnce_RelatedObject = true;
    }
}


public int RelatedObject_Value
{
    get
    {
        //return _related_object; //ConvertToValue.ConvertToInt(RelatedObject);
        if(isSetOnce_RelatedObject) {return _related_object;}
        else {return ConvertToValue.ConvertToInt(RelatedObject);}
    }
}

public string RelatedObject_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_related_object).ToString();
               //if(isSetOnce_RelatedObject) {return ConvertToValue.ConvertToInt(_related_object).ToString();}
               //else {return ConvertToValue.ConvertToInt(RelatedObject).ToString();}
            ConvertToValue.ConvertToInt(RelatedObject).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //J_A
        //9_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeo.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //J_B
        //9_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeo.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C
        //9_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeo.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_E
        //9_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oSeo.FriendlyUrl));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl", ex.Message));

            }
            return paramsSelect;
        }



        //J_F
        //9_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoTitle(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoTitle, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_G
        //9_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDescription(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoDescription, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoDescription));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoDescription", ex.Message));

            }
            return paramsSelect;
        }



        //J_H
        //9_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoKeyWords(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoKeyWords, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoKeyWords));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoKeyWords", ex.Message));

            }
            return paramsSelect;
        }



        //J_I
        //9_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObject(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_RelatedObject, ConvertTo.ConvertEmptyToDBNull(oSeo.RelatedObject));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_RelatedObject", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_B
        //9_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeo.DateCreated)); 
db.AddParameter(TBNames_Seo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeo.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_C
        //9_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeo.DateCreated)); 
db.AddParameter(TBNames_Seo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeo.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_E
        //9_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FriendlyUrl(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeo.DateCreated)); 
db.AddParameter(TBNames_Seo.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oSeo.FriendlyUrl));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_FriendlyUrl", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_F
        //9_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoTitle(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeo.DateCreated)); 
db.AddParameter(TBNames_Seo.PRM_SeoTitle, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_SeoTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_G
        //9_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDescription(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeo.DateCreated)); 
db.AddParameter(TBNames_Seo.PRM_SeoDescription, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoDescription));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDescription", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_H
        //9_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoKeyWords(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeo.DateCreated)); 
db.AddParameter(TBNames_Seo.PRM_SeoKeyWords, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoKeyWords));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_SeoKeyWords", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_I
        //9_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObject(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oSeo.DateCreated)); 
db.AddParameter(TBNames_Seo.PRM_RelatedObject, ConvertTo.ConvertEmptyToDBNull(oSeo.RelatedObject));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_RelatedObject", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_C
        //9_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeo.DocId)); 
db.AddParameter(TBNames_Seo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeo.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_E
        //9_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FriendlyUrl(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeo.DocId)); 
db.AddParameter(TBNames_Seo.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oSeo.FriendlyUrl));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_FriendlyUrl", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_F
        //9_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoTitle(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeo.DocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoTitle, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_SeoTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_G
        //9_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDescription(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeo.DocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoDescription, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoDescription));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_SeoDescription", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_H
        //9_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoKeyWords(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeo.DocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoKeyWords, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoKeyWords));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_SeoKeyWords", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_I
        //9_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_RelatedObject(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oSeo.DocId)); 
db.AddParameter(TBNames_Seo.PRM_RelatedObject, ConvertTo.ConvertEmptyToDBNull(oSeo.RelatedObject));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_RelatedObject", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_E
        //9_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FriendlyUrl(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeo.IsDeleted)); 
db.AddParameter(TBNames_Seo.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oSeo.FriendlyUrl));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_FriendlyUrl", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_F
        //9_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoTitle(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeo.IsDeleted)); 
db.AddParameter(TBNames_Seo.PRM_SeoTitle, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_G
        //9_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDescription(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeo.IsDeleted)); 
db.AddParameter(TBNames_Seo.PRM_SeoDescription, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoDescription));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDescription", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_H
        //9_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoKeyWords(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeo.IsDeleted)); 
db.AddParameter(TBNames_Seo.PRM_SeoKeyWords, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoKeyWords));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoKeyWords", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_I
        //9_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObject(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oSeo.IsDeleted)); 
db.AddParameter(TBNames_Seo.PRM_RelatedObject, ConvertTo.ConvertEmptyToDBNull(oSeo.RelatedObject));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_RelatedObject", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_F
        //9_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoTitle(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oSeo.FriendlyUrl)); 
db.AddParameter(TBNames_Seo.PRM_SeoTitle, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoTitle));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoTitle", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_G
        //9_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoDescription(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oSeo.FriendlyUrl)); 
db.AddParameter(TBNames_Seo.PRM_SeoDescription, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoDescription));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoDescription", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_H
        //9_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoKeyWords(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oSeo.FriendlyUrl)); 
db.AddParameter(TBNames_Seo.PRM_SeoKeyWords, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoKeyWords));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoKeyWords", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_I
        //9_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_RelatedObject(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oSeo.FriendlyUrl)); 
db.AddParameter(TBNames_Seo.PRM_RelatedObject, ConvertTo.ConvertEmptyToDBNull(oSeo.RelatedObject));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl_RelatedObject", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_G
        //9_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoTitle_SeoDescription(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoTitle, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoTitle)); 
db.AddParameter(TBNames_Seo.PRM_SeoDescription, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoDescription));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoTitle_SeoDescription", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_H
        //9_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoTitle_SeoKeyWords(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoTitle, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoTitle)); 
db.AddParameter(TBNames_Seo.PRM_SeoKeyWords, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoKeyWords));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoTitle_SeoKeyWords", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_I
        //9_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoTitle_RelatedObject(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoTitle, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoTitle)); 
db.AddParameter(TBNames_Seo.PRM_RelatedObject, ConvertTo.ConvertEmptyToDBNull(oSeo.RelatedObject));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoTitle_RelatedObject", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_H
        //9_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDescription_SeoKeyWords(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoDescription, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoDescription)); 
db.AddParameter(TBNames_Seo.PRM_SeoKeyWords, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoKeyWords));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoDescription_SeoKeyWords", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_I
        //9_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDescription_RelatedObject(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoDescription, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoDescription)); 
db.AddParameter(TBNames_Seo.PRM_RelatedObject, ConvertTo.ConvertEmptyToDBNull(oSeo.RelatedObject));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoDescription_RelatedObject", ex.Message));

            }
            return paramsSelect;
        }



        //J_H_I
        //9_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoKeyWords_RelatedObject(Seo oSeo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Seo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oSeo.WhiteLabelDocId)); 
db.AddParameter(TBNames_Seo.PRM_SeoKeyWords, ConvertTo.ConvertEmptyToDBNull(oSeo.SeoKeyWords)); 
db.AddParameter(TBNames_Seo.PRM_RelatedObject, ConvertTo.ConvertEmptyToDBNull(oSeo.RelatedObject));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Seo, "Select_Seo_By_Keys_View_WhiteLabelDocId_SeoKeyWords_RelatedObject", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
