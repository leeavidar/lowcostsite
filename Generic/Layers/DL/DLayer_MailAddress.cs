

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class MailAddress  : ContainerItem<MailAddress>{

#region CTOR

    #region Constractor
    static MailAddress()
    {
        ConvertEvent = MailAddress.OnConvert;
    }  
    //public KeyValuesContainer<MailAddress> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public MailAddress()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.MailAddressKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static MailAddress OnConvert(DataRow dr)
    {
        int LangId = Translator<MailAddress>.LangId;            
        MailAddress oMailAddress = null;
        if (dr != null)
        {
            oMailAddress = new MailAddress();
            #region Create Object
            oMailAddress.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_MailAddress.Field_DateCreated]);
             oMailAddress.DocId = ConvertTo.ConvertToInt(dr[TBNames_MailAddress.Field_DocId]);
             oMailAddress.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_MailAddress.Field_IsDeleted]);
             oMailAddress.IsActive = ConvertTo.ConvertToBool(dr[TBNames_MailAddress.Field_IsActive]);
             oMailAddress.Address = ConvertTo.ConvertToString(dr[TBNames_MailAddress.Field_Address]);
 
//FK     KeyWhiteLabelDocId
            oMailAddress.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_MailAddress.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<MailAddress>.Translate(oMailAddress.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oMailAddress;
    }

    
#endregion

//#REP_HERE 
#region MailAddress Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMailAddressDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyMailAddressDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyMailAddressDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMailAddressDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyMailAddressDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyMailAddressDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMailAddressIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyMailAddressIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyMailAddressIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMailAddressIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyMailAddressIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyMailAddressIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Address;
private string _address;
public String FriendlyAddress
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMailAddressAddress);   
    }
}
public  string Address
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyMailAddressAddress));
    }
    set
    {
        SetKey(KeyValuesType.KeyMailAddressAddress, value);
         _address = ConvertToValue.ConvertToString(value);
        isSetOnce_Address = true;
    }
}


public string Address_Value
{
    get
    {
        //return _address; //ConvertToValue.ConvertToString(Address);
        if(isSetOnce_Address) {return _address;}
        else {return ConvertToValue.ConvertToString(Address);}
    }
}

public string Address_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_address).ToString();
               //if(isSetOnce_Address) {return ConvertToValue.ConvertToString(_address).ToString();}
               //else {return ConvertToValue.ConvertToString(Address).ToString();}
            ConvertToValue.ConvertToString(Address).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //F_A
        //5_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMailAddress.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //F_B
        //5_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //F_C
        //5_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMailAddress.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //F_E
        //5_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Address(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oMailAddress.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_Address", ex.Message));

            }
            return paramsSelect;
        }



        //F_A_B
        //5_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMailAddress.DateCreated)); 
db.AddParameter(TBNames_MailAddress.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //F_A_C
        //5_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMailAddress.DateCreated)); 
db.AddParameter(TBNames_MailAddress.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMailAddress.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //F_A_E
        //5_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Address(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMailAddress.DateCreated)); 
db.AddParameter(TBNames_MailAddress.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oMailAddress.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_Address", ex.Message));

            }
            return paramsSelect;
        }



        //F_B_C
        //5_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.DocId)); 
db.AddParameter(TBNames_MailAddress.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMailAddress.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //F_B_E
        //5_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Address(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.DocId)); 
db.AddParameter(TBNames_MailAddress.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oMailAddress.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId_Address", ex.Message));

            }
            return paramsSelect;
        }



        //F_C_E
        //5_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Address(MailAddress oMailAddress)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_MailAddress.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMailAddress.WhiteLabelDocId)); 
db.AddParameter(TBNames_MailAddress.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMailAddress.IsDeleted)); 
db.AddParameter(TBNames_MailAddress.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oMailAddress.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.MailAddress, "Select_MailAddress_By_Keys_View_WhiteLabelDocId_IsDeleted_Address", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
