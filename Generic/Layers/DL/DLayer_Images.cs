

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Images  : ContainerItem<Images>{

#region CTOR

    #region Constractor
    static Images()
    {
        ConvertEvent = Images.OnConvert;
    }  
    //public KeyValuesContainer<Images> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Images()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.ImagesKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Images OnConvert(DataRow dr)
    {
        int LangId = Translator<Images>.LangId;            
        Images oImages = null;
        if (dr != null)
        {
            oImages = new Images();
            #region Create Object
            oImages.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Images.Field_DateCreated]);
             oImages.DocId = ConvertTo.ConvertToInt(dr[TBNames_Images.Field_DocId]);
             oImages.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Images.Field_IsDeleted]);
             oImages.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Images.Field_IsActive]);
             oImages.ImageFileName = ConvertTo.ConvertToString(dr[TBNames_Images.Field_ImageFileName]);
             oImages.RelatedObjectDocId = ConvertTo.ConvertToInt(dr[TBNames_Images.Field_RelatedObjectDocId]);
             oImages.ImageType = ConvertTo.ConvertToString(dr[TBNames_Images.Field_ImageType]);
             oImages.RelatedObjectType = ConvertTo.ConvertToString(dr[TBNames_Images.Field_RelatedObjectType]);
             oImages.Title = ConvertTo.ConvertToString(dr[TBNames_Images.Field_Title]);
             oImages.Alt = ConvertTo.ConvertToString(dr[TBNames_Images.Field_Alt]);
 
            #endregion
            Translator<Images>.Translate(oImages.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oImages;
    }

    
#endregion

//#REP_HERE 
#region Images Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyImagesDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyImagesDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyImagesIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyImagesIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_ImageFileName;
private string _image_file_name;
public String FriendlyImageFileName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesImageFileName);   
    }
}
public  string ImageFileName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyImagesImageFileName));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesImageFileName, value);
         _image_file_name = ConvertToValue.ConvertToString(value);
        isSetOnce_ImageFileName = true;
    }
}


public string ImageFileName_Value
{
    get
    {
        //return _image_file_name; //ConvertToValue.ConvertToString(ImageFileName);
        if(isSetOnce_ImageFileName) {return _image_file_name;}
        else {return ConvertToValue.ConvertToString(ImageFileName);}
    }
}

public string ImageFileName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_image_file_name).ToString();
               //if(isSetOnce_ImageFileName) {return ConvertToValue.ConvertToString(_image_file_name).ToString();}
               //else {return ConvertToValue.ConvertToString(ImageFileName).ToString();}
            ConvertToValue.ConvertToString(ImageFileName).ToString();
    }
}

private bool isSetOnce_RelatedObjectDocId;
private int _related_object_doc_id;
public String FriendlyRelatedObjectDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesRelatedObjectDocId);   
    }
}
public  int? RelatedObjectDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyImagesRelatedObjectDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesRelatedObjectDocId, value);
         _related_object_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_RelatedObjectDocId = true;
    }
}


public int RelatedObjectDocId_Value
{
    get
    {
        //return _related_object_doc_id; //ConvertToValue.ConvertToInt(RelatedObjectDocId);
        if(isSetOnce_RelatedObjectDocId) {return _related_object_doc_id;}
        else {return ConvertToValue.ConvertToInt(RelatedObjectDocId);}
    }
}

public string RelatedObjectDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_related_object_doc_id).ToString();
               //if(isSetOnce_RelatedObjectDocId) {return ConvertToValue.ConvertToInt(_related_object_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(RelatedObjectDocId).ToString();}
            ConvertToValue.ConvertToInt(RelatedObjectDocId).ToString();
    }
}

private bool isSetOnce_ImageType;
private string _image_type;
public String FriendlyImageType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesImageType);   
    }
}
public  string ImageType
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyImagesImageType));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesImageType, value);
         _image_type = ConvertToValue.ConvertToString(value);
        isSetOnce_ImageType = true;
    }
}


public string ImageType_Value
{
    get
    {
        //return _image_type; //ConvertToValue.ConvertToString(ImageType);
        if(isSetOnce_ImageType) {return _image_type;}
        else {return ConvertToValue.ConvertToString(ImageType);}
    }
}

public string ImageType_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_image_type).ToString();
               //if(isSetOnce_ImageType) {return ConvertToValue.ConvertToString(_image_type).ToString();}
               //else {return ConvertToValue.ConvertToString(ImageType).ToString();}
            ConvertToValue.ConvertToString(ImageType).ToString();
    }
}

private bool isSetOnce_RelatedObjectType;
private string _related_object_type;
public String FriendlyRelatedObjectType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesRelatedObjectType);   
    }
}
public  string RelatedObjectType
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyImagesRelatedObjectType));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesRelatedObjectType, value);
         _related_object_type = ConvertToValue.ConvertToString(value);
        isSetOnce_RelatedObjectType = true;
    }
}


public string RelatedObjectType_Value
{
    get
    {
        //return _related_object_type; //ConvertToValue.ConvertToString(RelatedObjectType);
        if(isSetOnce_RelatedObjectType) {return _related_object_type;}
        else {return ConvertToValue.ConvertToString(RelatedObjectType);}
    }
}

public string RelatedObjectType_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_related_object_type).ToString();
               //if(isSetOnce_RelatedObjectType) {return ConvertToValue.ConvertToString(_related_object_type).ToString();}
               //else {return ConvertToValue.ConvertToString(RelatedObjectType).ToString();}
            ConvertToValue.ConvertToString(RelatedObjectType).ToString();
    }
}

private bool isSetOnce_Title;
private string _title;
public String FriendlyTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesTitle);   
    }
}
public  string Title
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyImagesTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesTitle, value);
         _title = ConvertToValue.ConvertToString(value);
        isSetOnce_Title = true;
    }
}


public string Title_Value
{
    get
    {
        //return _title; //ConvertToValue.ConvertToString(Title);
        if(isSetOnce_Title) {return _title;}
        else {return ConvertToValue.ConvertToString(Title);}
    }
}

public string Title_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_title).ToString();
               //if(isSetOnce_Title) {return ConvertToValue.ConvertToString(_title).ToString();}
               //else {return ConvertToValue.ConvertToString(Title).ToString();}
            ConvertToValue.ConvertToString(Title).ToString();
    }
}

private bool isSetOnce_Alt;
private string _alt;
public String FriendlyAlt
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyImagesAlt);   
    }
}
public  string Alt
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyImagesAlt));
    }
    set
    {
        SetKey(KeyValuesType.KeyImagesAlt, value);
         _alt = ConvertToValue.ConvertToString(value);
        isSetOnce_Alt = true;
    }
}


public string Alt_Value
{
    get
    {
        //return _alt; //ConvertToValue.ConvertToString(Alt);
        if(isSetOnce_Alt) {return _alt;}
        else {return ConvertToValue.ConvertToString(Alt);}
    }
}

public string Alt_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_alt).ToString();
               //if(isSetOnce_Alt) {return ConvertToValue.ConvertToString(_alt).ToString();}
               //else {return ConvertToValue.ConvertToString(Alt).ToString();}
            ConvertToValue.ConvertToString(Alt).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageFileName(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageFileName", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_RelatedObjectDocId(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G
        //6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageType", ex.Message));

            }
            return paramsSelect;
        }



        //H
        //7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_RelatedObjectType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //I
        //8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Title(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_Title", ex.Message));

            }
            return paramsSelect;
        }



        //J
        //9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_Alt", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated)); 
db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated)); 
db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_ImageFileName(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated)); 
db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated_ImageFileName", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_RelatedObjectDocId(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_G
        //0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_ImageType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated)); 
db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated_ImageType", ex.Message));

            }
            return paramsSelect;
        }



        //A_H
        //0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_RelatedObjectType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //A_I
        //0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Title(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated)); 
db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated_Title", ex.Message));

            }
            return paramsSelect;
        }



        //A_J
        //0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oImages.DateCreated)); 
db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DateCreated_Alt", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId)); 
db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_ImageFileName(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId)); 
db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DocId_ImageFileName", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_RelatedObjectDocId(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DocId_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //B_G
        //1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_ImageType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId)); 
db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DocId_ImageType", ex.Message));

            }
            return paramsSelect;
        }



        //B_H
        //1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_RelatedObjectType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DocId_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //B_I
        //1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Title(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId)); 
db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //B_J
        //1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oImages.DocId)); 
db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_DocId_Alt", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_ImageFileName(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted)); 
db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_IsDeleted_ImageFileName", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_RelatedObjectDocId(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_IsDeleted_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //C_G
        //2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_ImageType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted)); 
db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_IsDeleted_ImageType", ex.Message));

            }
            return paramsSelect;
        }



        //C_H
        //2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_RelatedObjectType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_IsDeleted_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //C_I
        //2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Title(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted)); 
db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_IsDeleted_Title", ex.Message));

            }
            return paramsSelect;
        }



        //C_J
        //2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oImages.IsDeleted)); 
db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_IsDeleted_Alt", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageFileName_RelatedObjectDocId(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageFileName_RelatedObjectDocId", ex.Message));

            }
            return paramsSelect;
        }



        //E_G
        //4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageFileName_ImageType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName)); 
db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageFileName_ImageType", ex.Message));

            }
            return paramsSelect;
        }



        //E_H
        //4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageFileName_RelatedObjectType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageFileName_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //E_I
        //4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageFileName_Title(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName)); 
db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageFileName_Title", ex.Message));

            }
            return paramsSelect;
        }



        //E_J
        //4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageFileName_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageFileName, ConvertTo.ConvertEmptyToDBNull(oImages.ImageFileName)); 
db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageFileName_Alt", ex.Message));

            }
            return paramsSelect;
        }



        //F_G
        //5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_RelatedObjectDocId_ImageType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId)); 
db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_RelatedObjectDocId_ImageType", ex.Message));

            }
            return paramsSelect;
        }



        //F_H
        //5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_RelatedObjectDocId_RelatedObjectType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_RelatedObjectDocId_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //F_I
        //5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_RelatedObjectDocId_Title(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId)); 
db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_RelatedObjectDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //F_J
        //5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_RelatedObjectDocId_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_RelatedObjectDocId, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectDocId)); 
db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_RelatedObjectDocId_Alt", ex.Message));

            }
            return paramsSelect;
        }



        //G_H
        //6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageType_RelatedObjectType(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType)); 
db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageType_RelatedObjectType", ex.Message));

            }
            return paramsSelect;
        }



        //G_I
        //6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageType_Title(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType)); 
db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageType_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_J
        //6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_ImageType_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_ImageType, ConvertTo.ConvertEmptyToDBNull(oImages.ImageType)); 
db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_ImageType_Alt", ex.Message));

            }
            return paramsSelect;
        }



        //H_I
        //7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_RelatedObjectType_Title(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType)); 
db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_RelatedObjectType_Title", ex.Message));

            }
            return paramsSelect;
        }



        //H_J
        //7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_RelatedObjectType_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_RelatedObjectType, ConvertTo.ConvertEmptyToDBNull(oImages.RelatedObjectType)); 
db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_RelatedObjectType_Alt", ex.Message));

            }
            return paramsSelect;
        }



        //I_J
        //8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Title_Alt(Images oImages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Images.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oImages.Title)); 
db.AddParameter(TBNames_Images.PRM_Alt, ConvertTo.ConvertEmptyToDBNull(oImages.Alt));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Images, "Select_Images_By_Keys_View_Title_Alt", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
