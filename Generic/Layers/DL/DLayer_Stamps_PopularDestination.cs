

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Stamps_PopularDestination  : ContainerItem<Stamps_PopularDestination>{

#region CTOR

    #region Constractor
    static Stamps_PopularDestination()
    {
        ConvertEvent = Stamps_PopularDestination.OnConvert;
    }  
    //public KeyValuesContainer<Stamps_PopularDestination> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Stamps_PopularDestination()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.Stamps_PopularDestinationKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Stamps_PopularDestination OnConvert(DataRow dr)
    {
        int LangId = Translator<Stamps_PopularDestination>.LangId;            
        Stamps_PopularDestination oStamps_PopularDestination = null;
        if (dr != null)
        {
            oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Create Object
            oStamps_PopularDestination.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Stamps_PopularDestination.Field_DateCreated]);
             oStamps_PopularDestination.DocId = ConvertTo.ConvertToInt(dr[TBNames_Stamps_PopularDestination.Field_DocId]);
             oStamps_PopularDestination.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Stamps_PopularDestination.Field_IsDeleted]);
             oStamps_PopularDestination.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Stamps_PopularDestination.Field_IsActive]);
 
//FK     KeyPopularDestinationDocId
            oStamps_PopularDestination.PopularDestinationDocId = ConvertTo.ConvertToInt(dr[TBNames_Stamps_PopularDestination.Field_PopularDestinationDocId]);
 
//FK     KeyStampsDocId
            oStamps_PopularDestination.StampsDocId = ConvertTo.ConvertToInt(dr[TBNames_Stamps_PopularDestination.Field_StampsDocId]);
 
            #endregion
            Translator<Stamps_PopularDestination>.Translate(oStamps_PopularDestination.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oStamps_PopularDestination;
    }

    
#endregion

//#REP_HERE 
#region Stamps_PopularDestination Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStamps_PopularDestinationDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyStamps_PopularDestinationDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyStamps_PopularDestinationDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStamps_PopularDestinationDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyStamps_PopularDestinationDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyStamps_PopularDestinationDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStamps_PopularDestinationIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyStamps_PopularDestinationIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyStamps_PopularDestinationIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStamps_PopularDestinationIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyStamps_PopularDestinationIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyStamps_PopularDestinationIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_PopularDestinationDocId;
private int _popular_destination_doc_id;
public String FriendlyPopularDestinationDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPopularDestinationDocId);   
    }
}
public  int? PopularDestinationDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyPopularDestinationDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyPopularDestinationDocId, value);
         _popular_destination_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_PopularDestinationDocId = true;
    }
}


public int PopularDestinationDocId_Value
{
    get
    {
        //return _popular_destination_doc_id; //ConvertToValue.ConvertToInt(PopularDestinationDocId);
        if(isSetOnce_PopularDestinationDocId) {return _popular_destination_doc_id;}
        else {return ConvertToValue.ConvertToInt(PopularDestinationDocId);}
    }
}

public string PopularDestinationDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_popular_destination_doc_id).ToString();
               //if(isSetOnce_PopularDestinationDocId) {return ConvertToValue.ConvertToInt(_popular_destination_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(PopularDestinationDocId).ToString();}
            ConvertToValue.ConvertToInt(PopularDestinationDocId).ToString();
    }
}

private bool isSetOnce_StampsDocId;
private int _stamps_doc_id;
public String FriendlyStampsDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStampsDocId);   
    }
}
public  int? StampsDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyStampsDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyStampsDocId, value);
         _stamps_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_StampsDocId = true;
    }
}


public int StampsDocId_Value
{
    get
    {
        //return _stamps_doc_id; //ConvertToValue.ConvertToInt(StampsDocId);
        if(isSetOnce_StampsDocId) {return _stamps_doc_id;}
        else {return ConvertToValue.ConvertToInt(StampsDocId);}
    }
}

public string StampsDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_stamps_doc_id).ToString();
               //if(isSetOnce_StampsDocId) {return ConvertToValue.ConvertToInt(_stamps_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(StampsDocId).ToString();}
            ConvertToValue.ConvertToInt(StampsDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_PopularDestinationDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_PopularDestinationDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.PopularDestinationDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_PopularDestinationDocId", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_StampsDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_StampsDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.StampsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_StampsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DateCreated)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DateCreated)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_PopularDestinationDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DateCreated)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_PopularDestinationDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.PopularDestinationDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DateCreated_PopularDestinationDocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_StampsDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DateCreated)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_StampsDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.StampsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DateCreated_StampsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DocId)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_PopularDestinationDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DocId)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_PopularDestinationDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.PopularDestinationDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DocId_PopularDestinationDocId", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_StampsDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.DocId)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_StampsDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.StampsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_DocId_StampsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_PopularDestinationDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.IsDeleted)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_PopularDestinationDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.PopularDestinationDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_IsDeleted_PopularDestinationDocId", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_StampsDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.IsDeleted)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_StampsDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.StampsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_IsDeleted_StampsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_PopularDestinationDocId_StampsDocId(Stamps_PopularDestination oStamps_PopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps_PopularDestination.PRM_PopularDestinationDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.PopularDestinationDocId)); 
db.AddParameter(TBNames_Stamps_PopularDestination.PRM_StampsDocId, ConvertTo.ConvertEmptyToDBNull(oStamps_PopularDestination.StampsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps_PopularDestination, "Select_Stamps_PopularDestination_By_Keys_View_PopularDestinationDocId_StampsDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
