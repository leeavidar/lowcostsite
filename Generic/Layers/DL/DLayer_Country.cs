

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Country  : ContainerItem<Country>{

#region CTOR

    #region Constractor
    static Country()
    {
        ConvertEvent = Country.OnConvert;
    }  
    //public KeyValuesContainer<Country> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Country()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.CountryKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Country OnConvert(DataRow dr)
    {
        int LangId = Translator<Country>.LangId;            
        Country oCountry = null;
        if (dr != null)
        {
            oCountry = new Country();
            #region Create Object
            oCountry.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Country.Field_DateCreated]);
             oCountry.DocId = ConvertTo.ConvertToInt(dr[TBNames_Country.Field_DocId]);
             oCountry.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Country.Field_IsDeleted]);
             oCountry.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Country.Field_IsActive]);
             oCountry.CountryCode = ConvertTo.ConvertToString(dr[TBNames_Country.Field_CountryCode]);
             oCountry.Name = ConvertTo.ConvertToString(dr[TBNames_Country.Field_Name]);
             oCountry.EnglishName = ConvertTo.ConvertToString(dr[TBNames_Country.Field_EnglishName]);
 
            #endregion
            Translator<Country>.Translate(oCountry.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oCountry;
    }

    
#endregion

//#REP_HERE 
#region Country Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCountryDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyCountryDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyCountryDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCountryDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyCountryDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyCountryDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCountryIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyCountryIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyCountryIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCountryIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyCountryIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyCountryIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_CountryCode;
private string _country_code;
public String FriendlyCountryCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCountryCountryCode);   
    }
}
public  string CountryCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCountryCountryCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyCountryCountryCode, value);
         _country_code = ConvertToValue.ConvertToString(value);
        isSetOnce_CountryCode = true;
    }
}


public string CountryCode_Value
{
    get
    {
        //return _country_code; //ConvertToValue.ConvertToString(CountryCode);
        if(isSetOnce_CountryCode) {return _country_code;}
        else {return ConvertToValue.ConvertToString(CountryCode);}
    }
}

public string CountryCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_country_code).ToString();
               //if(isSetOnce_CountryCode) {return ConvertToValue.ConvertToString(_country_code).ToString();}
               //else {return ConvertToValue.ConvertToString(CountryCode).ToString();}
            ConvertToValue.ConvertToString(CountryCode).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCountryName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCountryName));
    }
    set
    {
        SetKey(KeyValuesType.KeyCountryName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_EnglishName;
private string _english_name;
public String FriendlyEnglishName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCountryEnglishName);   
    }
}
public  string EnglishName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCountryEnglishName));
    }
    set
    {
        SetKey(KeyValuesType.KeyCountryEnglishName, value);
         _english_name = ConvertToValue.ConvertToString(value);
        isSetOnce_EnglishName = true;
    }
}


public string EnglishName_Value
{
    get
    {
        //return _english_name; //ConvertToValue.ConvertToString(EnglishName);
        if(isSetOnce_EnglishName) {return _english_name;}
        else {return ConvertToValue.ConvertToString(EnglishName);}
    }
}

public string EnglishName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_english_name).ToString();
               //if(isSetOnce_EnglishName) {return ConvertToValue.ConvertToString(_english_name).ToString();}
               //else {return ConvertToValue.ConvertToString(EnglishName).ToString();}
            ConvertToValue.ConvertToString(EnglishName).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCountry.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCountry.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCountry.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_CountryCode(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCountry.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCountry.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G
        //6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_EnglishName(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCountry.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCountry.DateCreated)); 
db.AddParameter(TBNames_Country.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCountry.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCountry.DateCreated)); 
db.AddParameter(TBNames_Country.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCountry.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_CountryCode(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCountry.DateCreated)); 
db.AddParameter(TBNames_Country.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCountry.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DateCreated_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Name(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCountry.DateCreated)); 
db.AddParameter(TBNames_Country.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCountry.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //A_G
        //0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_EnglishName(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCountry.DateCreated)); 
db.AddParameter(TBNames_Country.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCountry.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DateCreated_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCountry.DocId)); 
db.AddParameter(TBNames_Country.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCountry.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_CountryCode(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCountry.DocId)); 
db.AddParameter(TBNames_Country.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCountry.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DocId_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Name(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCountry.DocId)); 
db.AddParameter(TBNames_Country.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCountry.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //B_G
        //1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_EnglishName(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCountry.DocId)); 
db.AddParameter(TBNames_Country.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCountry.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_DocId_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_CountryCode(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCountry.IsDeleted)); 
db.AddParameter(TBNames_Country.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCountry.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_IsDeleted_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Name(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCountry.IsDeleted)); 
db.AddParameter(TBNames_Country.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCountry.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //C_G
        //2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_EnglishName(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCountry.IsDeleted)); 
db.AddParameter(TBNames_Country.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCountry.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_IsDeleted_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_CountryCode_Name(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCountry.CountryCode)); 
db.AddParameter(TBNames_Country.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCountry.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_CountryCode_Name", ex.Message));

            }
            return paramsSelect;
        }



        //E_G
        //4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_CountryCode_EnglishName(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCountry.CountryCode)); 
db.AddParameter(TBNames_Country.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCountry.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_CountryCode_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //F_G
        //5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_EnglishName(Country oCountry)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Country.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCountry.Name)); 
db.AddParameter(TBNames_Country.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCountry.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Country, "Select_Country_By_Keys_View_Name_EnglishName", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
