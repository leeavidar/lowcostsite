

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class IndexPagePromo  : ContainerItem<IndexPagePromo>{

#region CTOR

    #region Constractor
    static IndexPagePromo()
    {
        ConvertEvent = IndexPagePromo.OnConvert;
    }  
    //public KeyValuesContainer<IndexPagePromo> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public IndexPagePromo()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.IndexPagePromoKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static IndexPagePromo OnConvert(DataRow dr)
    {
        int LangId = Translator<IndexPagePromo>.LangId;            
        IndexPagePromo oIndexPagePromo = null;
        if (dr != null)
        {
            oIndexPagePromo = new IndexPagePromo();
            #region Create Object
            oIndexPagePromo.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_IndexPagePromo.Field_DateCreated]);
             oIndexPagePromo.DocId = ConvertTo.ConvertToInt(dr[TBNames_IndexPagePromo.Field_DocId]);
             oIndexPagePromo.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_IndexPagePromo.Field_IsDeleted]);
             oIndexPagePromo.IsActive = ConvertTo.ConvertToBool(dr[TBNames_IndexPagePromo.Field_IsActive]);
             oIndexPagePromo.ImagesDocId = ConvertTo.ConvertToInt(dr[TBNames_IndexPagePromo.Field_ImagesDocId]);
             oIndexPagePromo.Title = ConvertTo.ConvertToString(dr[TBNames_IndexPagePromo.Field_Title]);
             oIndexPagePromo.Text = ConvertTo.ConvertToString(dr[TBNames_IndexPagePromo.Field_Text]);
             oIndexPagePromo.Link = ConvertTo.ConvertToString(dr[TBNames_IndexPagePromo.Field_Link]);
             oIndexPagePromo.PromoType = ConvertTo.ConvertToInt(dr[TBNames_IndexPagePromo.Field_PromoType]);
             oIndexPagePromo.Order = ConvertTo.ConvertToInt(dr[TBNames_IndexPagePromo.Field_Order]);
 
//FK     KeyWhiteLabelDocId
            oIndexPagePromo.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_IndexPagePromo.Field_WhiteLabelDocId]);
             oIndexPagePromo.PriceAfter = ConvertTo.ConvertToDouble(dr[TBNames_IndexPagePromo.Field_PriceAfter]);
             oIndexPagePromo.PriceBefore = ConvertTo.ConvertToDouble(dr[TBNames_IndexPagePromo.Field_PriceBefore]);
 
            #endregion
            Translator<IndexPagePromo>.Translate(oIndexPagePromo.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oIndexPagePromo;
    }

    
#endregion

//#REP_HERE 
#region IndexPagePromo Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyIndexPagePromoDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyIndexPagePromoDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyIndexPagePromoIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyIndexPagePromoIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_ImagesDocId;
private int _images_doc_id;
public String FriendlyImagesDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoImagesDocId);   
    }
}
public  int? ImagesDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyIndexPagePromoImagesDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoImagesDocId, value);
         _images_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_ImagesDocId = true;
    }
}


public int ImagesDocId_Value
{
    get
    {
        //return _images_doc_id; //ConvertToValue.ConvertToInt(ImagesDocId);
        if(isSetOnce_ImagesDocId) {return _images_doc_id;}
        else {return ConvertToValue.ConvertToInt(ImagesDocId);}
    }
}

public string ImagesDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_images_doc_id).ToString();
               //if(isSetOnce_ImagesDocId) {return ConvertToValue.ConvertToInt(_images_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(ImagesDocId).ToString();}
            ConvertToValue.ConvertToInt(ImagesDocId).ToString();
    }
}

private bool isSetOnce_Title;
private string _title;
public String FriendlyTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoTitle);   
    }
}
public  string Title
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyIndexPagePromoTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoTitle, value);
         _title = ConvertToValue.ConvertToString(value);
        isSetOnce_Title = true;
    }
}


public string Title_Value
{
    get
    {
        //return _title; //ConvertToValue.ConvertToString(Title);
        if(isSetOnce_Title) {return _title;}
        else {return ConvertToValue.ConvertToString(Title);}
    }
}

public string Title_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_title).ToString();
               //if(isSetOnce_Title) {return ConvertToValue.ConvertToString(_title).ToString();}
               //else {return ConvertToValue.ConvertToString(Title).ToString();}
            ConvertToValue.ConvertToString(Title).ToString();
    }
}

private bool isSetOnce_Text;
private string _text;
public String FriendlyText
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoText);   
    }
}
public  string Text
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyIndexPagePromoText));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoText, value);
         _text = ConvertToValue.ConvertToString(value);
        isSetOnce_Text = true;
    }
}


public string Text_Value
{
    get
    {
        //return _text; //ConvertToValue.ConvertToString(Text);
        if(isSetOnce_Text) {return _text;}
        else {return ConvertToValue.ConvertToString(Text);}
    }
}

public string Text_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text).ToString();
               //if(isSetOnce_Text) {return ConvertToValue.ConvertToString(_text).ToString();}
               //else {return ConvertToValue.ConvertToString(Text).ToString();}
            ConvertToValue.ConvertToString(Text).ToString();
    }
}

private bool isSetOnce_Link;
private string _link;
public String FriendlyLink
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoLink);   
    }
}
public  string Link
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyIndexPagePromoLink));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoLink, value);
         _link = ConvertToValue.ConvertToString(value);
        isSetOnce_Link = true;
    }
}


public string Link_Value
{
    get
    {
        //return _link; //ConvertToValue.ConvertToString(Link);
        if(isSetOnce_Link) {return _link;}
        else {return ConvertToValue.ConvertToString(Link);}
    }
}

public string Link_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_link).ToString();
               //if(isSetOnce_Link) {return ConvertToValue.ConvertToString(_link).ToString();}
               //else {return ConvertToValue.ConvertToString(Link).ToString();}
            ConvertToValue.ConvertToString(Link).ToString();
    }
}

private bool isSetOnce_PromoType;
private int _promo_type;
public String FriendlyPromoType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoPromoType);   
    }
}
public  int? PromoType
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyIndexPagePromoPromoType));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoPromoType, value);
         _promo_type = ConvertToValue.ConvertToInt(value);
        isSetOnce_PromoType = true;
    }
}


public int PromoType_Value
{
    get
    {
        //return _promo_type; //ConvertToValue.ConvertToInt(PromoType);
        if(isSetOnce_PromoType) {return _promo_type;}
        else {return ConvertToValue.ConvertToInt(PromoType);}
    }
}

public string PromoType_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_promo_type).ToString();
               //if(isSetOnce_PromoType) {return ConvertToValue.ConvertToInt(_promo_type).ToString();}
               //else {return ConvertToValue.ConvertToInt(PromoType).ToString();}
            ConvertToValue.ConvertToInt(PromoType).ToString();
    }
}

private bool isSetOnce_Order;
private int _order;
public String FriendlyOrder
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoOrder);   
    }
}
public  int? Order
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyIndexPagePromoOrder));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoOrder, value);
         _order = ConvertToValue.ConvertToInt(value);
        isSetOnce_Order = true;
    }
}


public int Order_Value
{
    get
    {
        //return _order; //ConvertToValue.ConvertToInt(Order);
        if(isSetOnce_Order) {return _order;}
        else {return ConvertToValue.ConvertToInt(Order);}
    }
}

public string Order_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order).ToString();
               //if(isSetOnce_Order) {return ConvertToValue.ConvertToInt(_order).ToString();}
               //else {return ConvertToValue.ConvertToInt(Order).ToString();}
            ConvertToValue.ConvertToInt(Order).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_PriceAfter;
private Double _price_after;
public String FriendlyPriceAfter
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoPriceAfter);   
    }
}
public  Double? PriceAfter
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyIndexPagePromoPriceAfter));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoPriceAfter, value);
         _price_after = ConvertToValue.ConvertToDouble(value);
        isSetOnce_PriceAfter = true;
    }
}


public Double PriceAfter_Value
{
    get
    {
        //return _price_after; //ConvertToValue.ConvertToDouble(PriceAfter);
        if(isSetOnce_PriceAfter) {return _price_after;}
        else {return ConvertToValue.ConvertToDouble(PriceAfter);}
    }
}

public string PriceAfter_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_price_after).ToString();
               //if(isSetOnce_PriceAfter) {return ConvertToValue.ConvertToDouble(_price_after).ToString();}
               //else {return ConvertToValue.ConvertToDouble(PriceAfter).ToString();}
            ConvertToValue.ConvertToDouble(PriceAfter).ToString();
    }
}

private bool isSetOnce_PriceBefore;
private Double _price_before;
public String FriendlyPriceBefore
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyIndexPagePromoPriceBefore);   
    }
}
public  Double? PriceBefore
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyIndexPagePromoPriceBefore));
    }
    set
    {
        SetKey(KeyValuesType.KeyIndexPagePromoPriceBefore, value);
         _price_before = ConvertToValue.ConvertToDouble(value);
        isSetOnce_PriceBefore = true;
    }
}


public Double PriceBefore_Value
{
    get
    {
        //return _price_before; //ConvertToValue.ConvertToDouble(PriceBefore);
        if(isSetOnce_PriceBefore) {return _price_before;}
        else {return ConvertToValue.ConvertToDouble(PriceBefore);}
    }
}

public string PriceBefore_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_price_before).ToString();
               //if(isSetOnce_PriceBefore) {return ConvertToValue.ConvertToDouble(_price_before).ToString();}
               //else {return ConvertToValue.ConvertToDouble(PriceBefore).ToString();}
            ConvertToValue.ConvertToDouble(PriceBefore).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //K_A
        //10_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //K_B
        //10_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //K_C
        //10_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //K_E
        //10_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //K_F
        //10_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //K_G
        //10_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //K_H
        //10_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Link(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link", ex.Message));

            }
            return paramsSelect;
        }



        //K_I
        //10_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PromoType(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType", ex.Message));

            }
            return paramsSelect;
        }



        //K_J
        //10_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_L
        //10_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_M
        //10_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_B
        //10_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_C
        //10_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_E
        //10_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ImagesDocId(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_ImagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_F
        //10_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Title", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_G
        //10_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Text(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Text", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_H
        //10_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Link(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Link", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_I
        //10_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PromoType(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PromoType", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_J
        //10_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_L
        //10_0_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_M
        //10_0_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DateCreated)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_C
        //10_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_E
        //10_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ImagesDocId(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_ImagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_F
        //10_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_G
        //10_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Text(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_H
        //10_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Link(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Link", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_I
        //10_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PromoType(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PromoType", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_J
        //10_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_L
        //10_1_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_M
        //10_1_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.DocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_E
        //10_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ImagesDocId(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_ImagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_F
        //10_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Title", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_G
        //10_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Text(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Text", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_H
        //10_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Link(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Link", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_I
        //10_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PromoType(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PromoType", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_J
        //10_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_L
        //10_2_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_M
        //10_2_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.IsDeleted)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_F
        //10_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_Title(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_G
        //10_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_Text(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_H
        //10_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_Link(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Link", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_I
        //10_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_PromoType(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PromoType", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_J
        //10_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_L
        //10_4_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_M
        //10_4_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_ImagesDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.ImagesDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_G
        //10_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Text(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Text", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_H
        //10_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Link(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Link", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_I
        //10_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PromoType(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PromoType", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_J
        //10_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_L
        //10_5_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_M
        //10_5_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Title)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_G_H
        //10_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Link(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_Link", ex.Message));

            }
            return paramsSelect;
        }



        //K_G_I
        //10_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_PromoType(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PromoType", ex.Message));

            }
            return paramsSelect;
        }



        //K_G_J
        //10_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_G_L
        //10_6_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_G_M
        //10_6_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Text)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_H_I
        //10_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Link_PromoType(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PromoType", ex.Message));

            }
            return paramsSelect;
        }



        //K_H_J
        //10_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Link_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_H_L
        //10_7_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Link_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_H_M
        //10_7_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Link_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Link, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Link)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_I_J
        //10_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PromoType_Order(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_Order", ex.Message));

            }
            return paramsSelect;
        }



        //K_I_L
        //10_8_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PromoType_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_I_M
        //10_8_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PromoType_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PromoType, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PromoType)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_J_L
        //10_9_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_PriceAfter(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order_PriceAfter", ex.Message));

            }
            return paramsSelect;
        }



        //K_J_M
        //10_9_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.Order)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }



        //K_L_M
        //10_11_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceAfter_PriceBefore(IndexPagePromo oIndexPagePromo)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_IndexPagePromo.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.WhiteLabelDocId)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceAfter, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceAfter)); 
db.AddParameter(TBNames_IndexPagePromo.PRM_PriceBefore, ConvertTo.ConvertEmptyToDBNull(oIndexPagePromo.PriceBefore));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.IndexPagePromo, "Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceAfter_PriceBefore", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
