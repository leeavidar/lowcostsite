

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;

    public partial class FlightLeg : ContainerItem<FlightLeg>
    {
        #region FlightLeg Properties
        public string DepartureDateTimeWithTime_UI
        {
            get
            {
                return string.Format("{0} at {1}", this.DepartureDateTime_Value.ToString("dd/MM/yyyy"), this.DepartureDateTime_Value.ToString("HH:mm"));
            }
        }

        public string ArrivalDateTimeWithTime_UI
        {
            get
            {
                return string.Format("{0} at {1}", this.ArrivalDateTime_Value.ToString("dd/MM/yyyy"), this.ArrivalDateTime_Value.ToString("HH:mm"));
            }
        }



        #endregion

        #region Basic functions

        #endregion

        #region Combinations functions

        #endregion

    }
}
