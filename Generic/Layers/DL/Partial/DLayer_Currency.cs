

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;

    public partial class Currency : ContainerItem<Currency>
    {
        #region Currency Properties

        public string CodeAndSign_UI
        {
            get
            {
                if (Code != null && Sign != null)
                {
                    return string.Format("{0} ( {1})", Code_UI, Sign_UI);

                }
                if (Code == null && Name != null && Sign != null)
                {
                    return string.Format("{0} ( {1})", Name_UI, Sign_UI);
                }
                return "";
            }
        }

        #endregion

    }
}
