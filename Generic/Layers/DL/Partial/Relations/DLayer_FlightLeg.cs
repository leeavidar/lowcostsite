

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class FlightLeg  : ContainerItem<FlightLeg>{

                #region Relations Code
                
                            //Relation From:[FlightLeg] To:[OrderFlightLeg] -Type M:1
                            
		//-----M Side----------------------//
		#region OrderFlightLeg object
        private OrderFlightLeg _order_flight_leg;
        public bool IsOrderFlightLegNullAble = true;
        private bool _isOrderFlightLegSingleInit = false;

        public OrderFlightLeg OrderFlightLegSingle
        {
            get
            {
                if (_order_flight_leg != null)
                {
                    return _order_flight_leg;
                }
                else 
                {
                    if (_isOrderFlightLegSingleInit)
                    {
                        if(IsOrderFlightLegNullAble) {return null;}
                        else  {return new OrderFlightLeg();}
                    }
                    else
                    {
                        Init_OrderFlightLeg(false);
                    }
                }
                return _order_flight_leg;
            }
            set
            {
                _order_flight_leg = value;
                //if (value == null)
                //{
                //    _order_flight_leg = new OrderFlightLeg();
                //}
                //else
                //{
                //    if (_order_flight_leg == null)
                //    {
                //        _order_flight_leg = value;
                //    }
                //    else
                //    {
                //        lock (_order_flight_leg)
                //        {
                //            _order_flight_leg = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with OrderFlightLeg 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_OrderFlightLeg(bool isInitAnyway)
        {
            bool _isNullAble = IsOrderFlightLegNullAble;
            IsOrderFlightLegNullAble = true;
            if (isInitAnyway || !_isOrderFlightLegSingleInit)//OrderFlightLegSingle == null)
            {
                //Select by shared key
                this.OrderFlightLegSingle = OrderFlightLegContainer.SelectByID(this.OrderFlightLegDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isOrderFlightLegSingleInit = true;
            }
            IsOrderFlightLegNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[FlightLeg] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[Stops] To:[FlightLeg] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region Stops Container Object
        public bool IsStopsNullAble = true;
        private bool _isStopsContainerInit = false;
        
        private StopsContainer _stopsContainer;
        public StopsContainer Stopss
        {
            get
            {
                if (_stopsContainer != null)
                {
                    return _stopsContainer;
                }
                else 
                {
                    if (_isStopsContainerInit)
                    {
                        if(IsStopsNullAble) {return null;}
                        else { return  new StopsContainer();}
                    }
                    else
                    {
                        Init_StopsContainer(false);
                    }
                }
                return _stopsContainer;
            }
            set
            {
                 _stopsContainer = value;
                //if (value == null)
                //{
                //    _stopsContainer = new StopsContainer();
                //}
                //else
                //{
                //    if (_stopsContainer == null)
                //    {
                //        _stopsContainer = value;
                //    }
                //    else
                //    {
                //        lock (_stopsContainer)
                //        {
                //            _stopsContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with StopsContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_StopsContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsStopsNullAble;
            IsStopsNullAble = true;
            if (isInitAnyway || !_isStopsContainerInit) //this.Stopss == null)
            {
                this.Stopss = StopsContainer.SelectByKeysView_FlightLegDocId(this.DocId_Value,true);
                _isStopsContainerInit = true;
            }
            IsStopsNullAble = _isNullAble;
        }
        #endregion

        
                        

                            //Relation From:[SeatPerFlightLeg] To:[FlightLeg] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region SeatPerFlightLeg Container Object
        public bool IsSeatPerFlightLegNullAble = true;
        private bool _isSeatPerFlightLegContainerInit = false;
        
        private SeatPerFlightLegContainer _seat_per_flight_legContainer;
        public SeatPerFlightLegContainer SeatPerFlightLegs
        {
            get
            {
                if (_seat_per_flight_legContainer != null)
                {
                    return _seat_per_flight_legContainer;
                }
                else 
                {
                    if (_isSeatPerFlightLegContainerInit)
                    {
                        if(IsSeatPerFlightLegNullAble) {return null;}
                        else { return  new SeatPerFlightLegContainer();}
                    }
                    else
                    {
                        Init_SeatPerFlightLegContainer(false);
                    }
                }
                return _seat_per_flight_legContainer;
            }
            set
            {
                 _seat_per_flight_legContainer = value;
                //if (value == null)
                //{
                //    _seat_per_flight_legContainer = new SeatPerFlightLegContainer();
                //}
                //else
                //{
                //    if (_seat_per_flight_legContainer == null)
                //    {
                //        _seat_per_flight_legContainer = value;
                //    }
                //    else
                //    {
                //        lock (_seat_per_flight_legContainer)
                //        {
                //            _seat_per_flight_legContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with SeatPerFlightLegContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_SeatPerFlightLegContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsSeatPerFlightLegNullAble;
            IsSeatPerFlightLegNullAble = true;
            if (isInitAnyway || !_isSeatPerFlightLegContainerInit) //this.SeatPerFlightLegs == null)
            {
                this.SeatPerFlightLegs = SeatPerFlightLegContainer.SelectByKeysView_WhiteLabelDocId_FlightLegDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isSeatPerFlightLegContainerInit = true;
            }
            IsSeatPerFlightLegNullAble = _isNullAble;
        }
        #endregion

        
                        
                #endregion
                
}
}
