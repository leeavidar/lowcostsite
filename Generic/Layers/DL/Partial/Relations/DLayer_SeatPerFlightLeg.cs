

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class SeatPerFlightLeg  : ContainerItem<SeatPerFlightLeg>{

                #region Relations Code
                
                            //Relation From:[SeatPerFlightLeg] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[SeatPerFlightLeg] To:[FlightLeg] -Type M:1
                            
		//-----M Side----------------------//
		#region FlightLeg object
        private FlightLeg _flight_leg;
        public bool IsFlightLegNullAble = true;
        private bool _isFlightLegSingleInit = false;

        public FlightLeg FlightLegSingle
        {
            get
            {
                if (_flight_leg != null)
                {
                    return _flight_leg;
                }
                else 
                {
                    if (_isFlightLegSingleInit)
                    {
                        if(IsFlightLegNullAble) {return null;}
                        else  {return new FlightLeg();}
                    }
                    else
                    {
                        Init_FlightLeg(false);
                    }
                }
                return _flight_leg;
            }
            set
            {
                _flight_leg = value;
                //if (value == null)
                //{
                //    _flight_leg = new FlightLeg();
                //}
                //else
                //{
                //    if (_flight_leg == null)
                //    {
                //        _flight_leg = value;
                //    }
                //    else
                //    {
                //        lock (_flight_leg)
                //        {
                //            _flight_leg = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with FlightLeg 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_FlightLeg(bool isInitAnyway)
        {
            bool _isNullAble = IsFlightLegNullAble;
            IsFlightLegNullAble = true;
            if (isInitAnyway || !_isFlightLegSingleInit)//FlightLegSingle == null)
            {
                //Select by shared key
                this.FlightLegSingle = FlightLegContainer.SelectByID(this.FlightLegDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isFlightLegSingleInit = true;
            }
            IsFlightLegNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[Passenger] To:[SeatPerFlightLeg] -Type 1:1
                            
        //-------1 Side--------------------------------------//
        #region Passenger Container Object
        public bool IsPassengerNullAble = true;
        private bool _isPassengerContainerInit = false;
        
        private PassengerContainer _passengerContainer;
        public PassengerContainer Passengers
        {
            get
            {
                if (_passengerContainer != null)
                {
                    return _passengerContainer;
                }
                else 
                {
                    if (_isPassengerContainerInit)
                    {
                        if(IsPassengerNullAble) {return null;}
                        else { return  new PassengerContainer();}
                    }
                    else
                    {
                        Init_PassengerContainer(false);
                    }
                }
                return _passengerContainer;
            }
            set
            {
                 _passengerContainer = value;
                //if (value == null)
                //{
                //    _passengerContainer = new PassengerContainer();
                //}
                //else
                //{
                //    if (_passengerContainer == null)
                //    {
                //        _passengerContainer = value;
                //    }
                //    else
                //    {
                //        lock (_passengerContainer)
                //        {
                //            _passengerContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with PassengerContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_PassengerContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsPassengerNullAble;
            IsPassengerNullAble = true;
            if (isInitAnyway || !_isPassengerContainerInit) //this.Passengers == null)
            {
                this.Passengers = PassengerContainer.SelectByID(this.DocId_Value,this.WhiteLabelDocId_Value,true);
                _isPassengerContainerInit = true;
            }
            IsPassengerNullAble = _isNullAble;
        }
        #endregion

        
                        
                #endregion
                
}
}
