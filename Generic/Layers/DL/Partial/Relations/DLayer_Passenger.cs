

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Passenger  : ContainerItem<Passenger>{

                #region Relations Code
                
                            //Relation From:[Passenger] To:[SeatPerFlightLeg] -Type 1:1
                            
        //-------1 Side--------------------------------------//
        #region SeatPerFlightLeg Container Object
        public bool IsSeatPerFlightLegNullAble = true;
        private bool _isSeatPerFlightLegContainerInit = false;
        
        private SeatPerFlightLegContainer _seat_per_flight_legContainer;
        public SeatPerFlightLegContainer SeatPerFlightLegs
        {
            get
            {
                if (_seat_per_flight_legContainer != null)
                {
                    return _seat_per_flight_legContainer;
                }
                else 
                {
                    if (_isSeatPerFlightLegContainerInit)
                    {
                        if(IsSeatPerFlightLegNullAble) {return null;}
                        else { return  new SeatPerFlightLegContainer();}
                    }
                    else
                    {
                        Init_SeatPerFlightLegContainer(false);
                    }
                }
                return _seat_per_flight_legContainer;
            }
            set
            {
                 _seat_per_flight_legContainer = value;
                //if (value == null)
                //{
                //    _seat_per_flight_legContainer = new SeatPerFlightLegContainer();
                //}
                //else
                //{
                //    if (_seat_per_flight_legContainer == null)
                //    {
                //        _seat_per_flight_legContainer = value;
                //    }
                //    else
                //    {
                //        lock (_seat_per_flight_legContainer)
                //        {
                //            _seat_per_flight_legContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with SeatPerFlightLegContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_SeatPerFlightLegContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsSeatPerFlightLegNullAble;
            IsSeatPerFlightLegNullAble = true;
            if (isInitAnyway || !_isSeatPerFlightLegContainerInit) //this.SeatPerFlightLegs == null)
            {
                this.SeatPerFlightLegs = SeatPerFlightLegContainer.SelectByID(this.DocId_Value,this.WhiteLabelDocId_Value,true);
                _isSeatPerFlightLegContainerInit = true;
            }
            IsSeatPerFlightLegNullAble = _isNullAble;
        }
        #endregion

        
                        

                            //Relation From:[Passenger] To:[Orders] -Type M:1
                            
		//-----M Side----------------------//
		#region Orders object
        private Orders _orders;
        public bool IsOrdersNullAble = true;
        private bool _isOrdersSingleInit = false;

        public Orders OrdersSingle
        {
            get
            {
                if (_orders != null)
                {
                    return _orders;
                }
                else 
                {
                    if (_isOrdersSingleInit)
                    {
                        if(IsOrdersNullAble) {return null;}
                        else  {return new Orders();}
                    }
                    else
                    {
                        Init_Orders(false);
                    }
                }
                return _orders;
            }
            set
            {
                _orders = value;
                //if (value == null)
                //{
                //    _orders = new Orders();
                //}
                //else
                //{
                //    if (_orders == null)
                //    {
                //        _orders = value;
                //    }
                //    else
                //    {
                //        lock (_orders)
                //        {
                //            _orders = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with Orders 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Orders(bool isInitAnyway)
        {
            bool _isNullAble = IsOrdersNullAble;
            IsOrdersNullAble = true;
            if (isInitAnyway || !_isOrdersSingleInit)//OrdersSingle == null)
            {
                //Select by shared key
                this.OrdersSingle = OrdersContainer.SelectByID(this.OrdersDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isOrdersSingleInit = true;
            }
            IsOrdersNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[Passenger] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[Luggage] To:[Passenger] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region Luggage Container Object
        public bool IsLuggageNullAble = true;
        private bool _isLuggageContainerInit = false;
        
        private LuggageContainer _luggageContainer;
        public LuggageContainer Luggages
        {
            get
            {
                if (_luggageContainer != null)
                {
                    return _luggageContainer;
                }
                else 
                {
                    if (_isLuggageContainerInit)
                    {
                        if(IsLuggageNullAble) {return null;}
                        else { return  new LuggageContainer();}
                    }
                    else
                    {
                        Init_LuggageContainer(false);
                    }
                }
                return _luggageContainer;
            }
            set
            {
                 _luggageContainer = value;
                //if (value == null)
                //{
                //    _luggageContainer = new LuggageContainer();
                //}
                //else
                //{
                //    if (_luggageContainer == null)
                //    {
                //        _luggageContainer = value;
                //    }
                //    else
                //    {
                //        lock (_luggageContainer)
                //        {
                //            _luggageContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with LuggageContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_LuggageContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsLuggageNullAble;
            IsLuggageNullAble = true;
            if (isInitAnyway || !_isLuggageContainerInit) //this.Luggages == null)
            {
                this.Luggages = LuggageContainer.SelectByKeysView_WhiteLabelDocId_PassengerDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isLuggageContainerInit = true;
            }
            IsLuggageNullAble = _isNullAble;
        }
        #endregion

        
                        
                #endregion
                
}
}
