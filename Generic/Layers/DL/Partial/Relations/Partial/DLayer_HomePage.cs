

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;
    using Generic;

    public partial class HomePage : ContainerItem<HomePage>
    {

        #region Relations Code


        //  Relation From:[Images]  To:[HomePage] -Type M:1


        //-------1 Side--------------------------------------//

        #region Images container

        public bool IsImageNullAble = true;
        private bool _isImageInit = false;

        private Images _image;
        public Images Image
        {
            get
            {
                if (_image != null)
                {
                    return _image;
                }
                else
                {
                    if (_isImageInit)
                    {
                        if (IsImageNullAble) { return null; }
                        else { return new Images(); }
                    }
                    else
                    {
                        Init_Image(false);
                    }
                }
                return _image;
            }
            set
            {
                _image = value;
            }
        }
        /// <summary>
        /// Init object with Image
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Image(bool isInitAnyway)
        {
            bool _isNullAble = IsImageNullAble;
            IsImageNullAble = true;
            if (isInitAnyway || !_isImageInit)
            {
                ImagesContainer oImagesContainer = ImagesContainer.SelectByKeysView_RelatedObjectDocId_RelatedObjectType(this.DocId_Value, EnumHandler.RelatedObjects.HomePage.ToString(), null);
                if (oImagesContainer != null && oImagesContainer.Count > 0)
                {
                    this.Image = oImagesContainer.Single;
                }
                else
                {
                    this.Image = new Images();
                }

                _isImageInit = true;
            }
            IsImageNullAble = _isNullAble;
        }

        #endregion



        #endregion

    }
}
