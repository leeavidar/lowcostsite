

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Airline  : ContainerItem<Airline>{

                #region Relations Code
    //Relation From:[FlightLeg] To:[Airline] -Type M:1                     

    //-------1 Side--------------------------------------//
    #region FlightLeg Container Object
    public bool IsFlightLegNullAble = true;
    private bool _isFlightLegContainerInit = true;

    private FlightLegContainer _flightLegContainer;
    public FlightLegContainer FlightLegs
    {
        get
        {
            if (_flightLegContainer != null)
            {
                return _flightLegContainer;
            }
            else
            {
                if (_isFlightLegContainerInit)
                {
                    if (IsFlightLegNullAble) { return null; }
                    else { return new FlightLegContainer(); }
                }
                else
                {
                    Init_FlightLegContainer(false);
                }
            }
            return _flightLegContainer;
        }
        set
        {
            if (value == null)
            {
                _flightLegContainer = new FlightLegContainer();
            }
            else
            {
                if (_flightLegContainer == null)
                {
                    _flightLegContainer = value;
                }
                else
                {
                    lock (_flightLegContainer)
                    {
                        _flightLegContainer = value;
                    }

                }
            }
        }
    }
    /// <summary>
    /// Init object with FlightLegContainer
    /// </summary>
    /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
    public void Init_FlightLegContainer(bool isInitAnyway)
    {
        bool _isNullAble = IsFlightLegNullAble;
        IsFlightLegNullAble = true;
        if (isInitAnyway || !_isFlightLegContainerInit) //this.FlightLegs == null)
        {
         // this.FlightLegs = FlightLegContainer.SelectByKeysView_WhiteLabelDocId_AirlineIataCode(this.WhiteLabelDocId_Value, this.DocId_Value, true);

            //this.FlightLegs = FlightLegContainer.SelectByKeysView_WhiteLabelDocId_AirlineIataCode(this.WhiteLabelDocId_Value, this.DocId_Value, true);

            _isFlightLegContainerInit = true;
        }
        IsFlightLegNullAble = _isNullAble;
    }
    #endregion





                #endregion
                
}
}
