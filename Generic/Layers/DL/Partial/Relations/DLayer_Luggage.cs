

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Luggage  : ContainerItem<Luggage>{

                #region Relations Code
                
                            //Relation From:[Luggage] To:[Passenger] -Type M:1
                            
		//-----M Side----------------------//
		#region Passenger object
        private Passenger _passenger;
        public bool IsPassengerNullAble = true;
        private bool _isPassengerSingleInit = false;

        public Passenger PassengerSingle
        {
            get
            {
                if (_passenger != null)
                {
                    return _passenger;
                }
                else 
                {
                    if (_isPassengerSingleInit)
                    {
                        if(IsPassengerNullAble) {return null;}
                        else  {return new Passenger();}
                    }
                    else
                    {
                        Init_Passenger(false);
                    }
                }
                return _passenger;
            }
            set
            {
                _passenger = value;
                //if (value == null)
                //{
                //    _passenger = new Passenger();
                //}
                //else
                //{
                //    if (_passenger == null)
                //    {
                //        _passenger = value;
                //    }
                //    else
                //    {
                //        lock (_passenger)
                //        {
                //            _passenger = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with Passenger 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Passenger(bool isInitAnyway)
        {
            bool _isNullAble = IsPassengerNullAble;
            IsPassengerNullAble = true;
            if (isInitAnyway || !_isPassengerSingleInit)//PassengerSingle == null)
            {
                //Select by shared key
                this.PassengerSingle = PassengerContainer.SelectByID(this.PassengerDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isPassengerSingleInit = true;
            }
            IsPassengerNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[Luggage] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        
                #endregion
                
}
}
