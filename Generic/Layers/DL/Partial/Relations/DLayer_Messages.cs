

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Messages  : ContainerItem<Messages>{

                #region Relations Code
                
                            //Relation From:[Messages] To:[Orders] -Type M:1
                            
		//-----M Side----------------------//
		#region Orders object
        private Orders _orders;
        public bool IsOrdersNullAble = true;
        private bool _isOrdersSingleInit = false;

        public Orders OrdersSingle
        {
            get
            {
                if (_orders != null)
                {
                    return _orders;
                }
                else 
                {
                    if (_isOrdersSingleInit)
                    {
                        if(IsOrdersNullAble) {return null;}
                        else  {return new Orders();}
                    }
                    else
                    {
                        Init_Orders(false);
                    }
                }
                return _orders;
            }
            set
            {
                _orders = value;
                //if (value == null)
                //{
                //    _orders = new Orders();
                //}
                //else
                //{
                //    if (_orders == null)
                //    {
                //        _orders = value;
                //    }
                //    else
                //    {
                //        lock (_orders)
                //        {
                //            _orders = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with Orders 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Orders(bool isInitAnyway)
        {
            bool _isNullAble = IsOrdersNullAble;
            IsOrdersNullAble = true;
            if (isInitAnyway || !_isOrdersSingleInit)//OrdersSingle == null)
            {
                //Select by shared key
                this.OrdersSingle = OrdersContainer.SelectByID(this.OrdersDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isOrdersSingleInit = true;
            }
            IsOrdersNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[Messages] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        
                #endregion
                
}
}
