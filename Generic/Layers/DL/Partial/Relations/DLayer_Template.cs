

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Template  : ContainerItem<Template>{

                #region Relations Code
                
                            //Relation From:[Template] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[SubBox] To:[Template] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region SubBox Container Object
        public bool IsSubBoxNullAble = true;
        private bool _isSubBoxContainerInit = false;
        
        private SubBoxContainer _sub_boxContainer;
        public SubBoxContainer SubBoxs
        {
            get
            {
                if (_sub_boxContainer != null)
                {
                    return _sub_boxContainer;
                }
                else 
                {
                    if (_isSubBoxContainerInit)
                    {
                        if(IsSubBoxNullAble) {return null;}
                        else { return  new SubBoxContainer();}
                    }
                    else
                    {
                        Init_SubBoxContainer(false);
                    }
                }
                return _sub_boxContainer;
            }
            set
            {
                 _sub_boxContainer = value;
                //if (value == null)
                //{
                //    _sub_boxContainer = new SubBoxContainer();
                //}
                //else
                //{
                //    if (_sub_boxContainer == null)
                //    {
                //        _sub_boxContainer = value;
                //    }
                //    else
                //    {
                //        lock (_sub_boxContainer)
                //        {
                //            _sub_boxContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with SubBoxContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_SubBoxContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsSubBoxNullAble;
            IsSubBoxNullAble = true;
            if (isInitAnyway || !_isSubBoxContainerInit) //this.SubBoxs == null)
            {
                this.SubBoxs = SubBoxContainer.SelectByKeysView_WhiteLabelDocId_TemplateDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isSubBoxContainerInit = true;
            }
            IsSubBoxNullAble = _isNullAble;
        }
        #endregion

        
                        
                #endregion
                
}
}
