

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class ContactDetails  : ContainerItem<ContactDetails>{

                #region Relations Code
                
                            //Relation From:[ContactDetails] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[Orders] To:[ContactDetails] -Type 1:1
                            
        //-------1 Side--------------------------------------//
        #region Orders Container Object
        public bool IsOrdersNullAble = true;
        private bool _isOrdersContainerInit = false;
        
        private OrdersContainer _ordersContainer;
        public OrdersContainer Orderss
        {
            get
            {
                if (_ordersContainer != null)
                {
                    return _ordersContainer;
                }
                else 
                {
                    if (_isOrdersContainerInit)
                    {
                        if(IsOrdersNullAble) {return null;}
                        else { return  new OrdersContainer();}
                    }
                    else
                    {
                        Init_OrdersContainer(false);
                    }
                }
                return _ordersContainer;
            }
            set
            {
                 _ordersContainer = value;
                //if (value == null)
                //{
                //    _ordersContainer = new OrdersContainer();
                //}
                //else
                //{
                //    if (_ordersContainer == null)
                //    {
                //        _ordersContainer = value;
                //    }
                //    else
                //    {
                //        lock (_ordersContainer)
                //        {
                //            _ordersContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with OrdersContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_OrdersContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsOrdersNullAble;
            IsOrdersNullAble = true;
            if (isInitAnyway || !_isOrdersContainerInit) //this.Orderss == null)
            {
                this.Orderss = OrdersContainer.SelectByID(this.DocId_Value,this.WhiteLabelDocId_Value,true);
                _isOrdersContainerInit = true;
            }
            IsOrdersNullAble = _isNullAble;
        }
        #endregion

        
                        
                #endregion
                
}
}
