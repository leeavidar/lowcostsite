

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class SubBox  : ContainerItem<SubBox>{

                #region Relations Code
                
                            //Relation From:[SubBox] To:[Template] -Type M:1
                            
		//-----M Side----------------------//
		#region Template object
        private Template _template;
        public bool IsTemplateNullAble = true;
        private bool _isTemplateSingleInit = false;

        public Template TemplateSingle
        {
            get
            {
                if (_template != null)
                {
                    return _template;
                }
                else 
                {
                    if (_isTemplateSingleInit)
                    {
                        if(IsTemplateNullAble) {return null;}
                        else  {return new Template();}
                    }
                    else
                    {
                        Init_Template(false);
                    }
                }
                return _template;
            }
            set
            {
                _template = value;
                //if (value == null)
                //{
                //    _template = new Template();
                //}
                //else
                //{
                //    if (_template == null)
                //    {
                //        _template = value;
                //    }
                //    else
                //    {
                //        lock (_template)
                //        {
                //            _template = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with Template 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Template(bool isInitAnyway)
        {
            bool _isNullAble = IsTemplateNullAble;
            IsTemplateNullAble = true;
            if (isInitAnyway || !_isTemplateSingleInit)//TemplateSingle == null)
            {
                //Select by shared key
                this.TemplateSingle = TemplateContainer.SelectByID(this.TemplateDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isTemplateSingleInit = true;
            }
            IsTemplateNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[SubBox] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        
                #endregion
                
}
}
