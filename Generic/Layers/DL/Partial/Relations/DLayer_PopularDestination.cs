

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class PopularDestination  : ContainerItem<PopularDestination>{

                #region Relations Code
                
                            //Relation From:[Stamps_PopularDestination] To:[PopularDestination] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region Stamps_PopularDestination Container Object
        public bool IsStamps_PopularDestinationNullAble = true;
        private bool _isStamps_PopularDestinationContainerInit = false;
        
        private Stamps_PopularDestinationContainer _stamps_popular_destinationContainer;
        public Stamps_PopularDestinationContainer Stamps_PopularDestinations
        {
            get
            {
                if (_stamps_popular_destinationContainer != null)
                {
                    return _stamps_popular_destinationContainer;
                }
                else 
                {
                    if (_isStamps_PopularDestinationContainerInit)
                    {
                        if(IsStamps_PopularDestinationNullAble) {return null;}
                        else { return  new Stamps_PopularDestinationContainer();}
                    }
                    else
                    {
                        Init_Stamps_PopularDestinationContainer(false);
                    }
                }
                return _stamps_popular_destinationContainer;
            }
            set
            {
                 _stamps_popular_destinationContainer = value;
                //if (value == null)
                //{
                //    _stamps_popular_destinationContainer = new Stamps_PopularDestinationContainer();
                //}
                //else
                //{
                //    if (_stamps_popular_destinationContainer == null)
                //    {
                //        _stamps_popular_destinationContainer = value;
                //    }
                //    else
                //    {
                //        lock (_stamps_popular_destinationContainer)
                //        {
                //            _stamps_popular_destinationContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with Stamps_PopularDestinationContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Stamps_PopularDestinationContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsStamps_PopularDestinationNullAble;
            IsStamps_PopularDestinationNullAble = true;
            if (isInitAnyway || !_isStamps_PopularDestinationContainerInit) //this.Stamps_PopularDestinations == null)
            {
                this.Stamps_PopularDestinations = Stamps_PopularDestinationContainer.SelectByKeysView_PopularDestinationDocId(this.DocId_Value,true);
                _isStamps_PopularDestinationContainerInit = true;
            }
            IsStamps_PopularDestinationNullAble = _isNullAble;
        }
        #endregion

        
                        

                            //Relation From:[Stamps] To:[PopularDestination] -Type M:M
                            //NO CODE SUPPORT FOR THIS
                        
                #endregion
                
}
}
