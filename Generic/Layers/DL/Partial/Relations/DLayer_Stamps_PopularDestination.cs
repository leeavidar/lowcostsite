

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Stamps_PopularDestination  : ContainerItem<Stamps_PopularDestination>{

                #region Relations Code
                
                            //Relation From:[Stamps_PopularDestination] To:[Stamps] -Type M:1
                            
		//-----M Side----------------------//
		#region Stamps object
        private Stamps _stamps;
        public bool IsStampsNullAble = true;
        private bool _isStampsSingleInit = false;

        public Stamps StampsSingle
        {
            get
            {
                if (_stamps != null)
                {
                    return _stamps;
                }
                else 
                {
                    if (_isStampsSingleInit)
                    {
                        if(IsStampsNullAble) {return null;}
                        else  {return new Stamps();}
                    }
                    else
                    {
                        Init_Stamps(false);
                    }
                }
                return _stamps;
            }
            set
            {
                _stamps = value;
                //if (value == null)
                //{
                //    _stamps = new Stamps();
                //}
                //else
                //{
                //    if (_stamps == null)
                //    {
                //        _stamps = value;
                //    }
                //    else
                //    {
                //        lock (_stamps)
                //        {
                //            _stamps = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with Stamps 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Stamps(bool isInitAnyway)
        {
            bool _isNullAble = IsStampsNullAble;
            IsStampsNullAble = true;
            if (isInitAnyway || !_isStampsSingleInit)//StampsSingle == null)
            {
                //Select by shared key
                this.StampsSingle = StampsContainer.SelectByID(this.StampsDocId_Value,true).Single;
                _isStampsSingleInit = true;
            }
            IsStampsNullAble = _isNullAble;
        }
        #endregion
        
                        
                #endregion
                
}
}
