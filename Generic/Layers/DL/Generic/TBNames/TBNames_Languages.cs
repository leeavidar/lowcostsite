

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Languages
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertLanguages = PROC_Prefix + "save_languages";
        #endregion
        #region Select
        public static readonly string PROC_Select_Languages_By_DocId = PROC_Prefix + "Select_languages_By_doc_id";        
        public static readonly string PROC_Select_Languages_By_Keys_View = PROC_Prefix + "Select_languages_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteLanguages = PROC_Prefix + "delete_languages";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesIsActive);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesName);

        public static readonly string PRM_Code = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesCode);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Languages.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesIsActive);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesName);

        public static readonly string Field_Code = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLanguagesCode);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Languages_By_Keys_View_DateCreated = "select_Languages_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Languages_By_Keys_View_DocId = "select_Languages_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Languages_By_Keys_View_IsDeleted = "select_Languages_by_keys_view_2";

//E
//4
//Name
public static readonly string  PROC_Select_Languages_By_Keys_View_Name = "select_Languages_by_keys_view_4";

//F
//5
//Code
public static readonly string  PROC_Select_Languages_By_Keys_View_Code = "select_Languages_by_keys_view_5";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Languages_By_Keys_View_DateCreated_DocId = "select_Languages_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Languages_By_Keys_View_DateCreated_IsDeleted = "select_Languages_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_Name
public static readonly string  PROC_Select_Languages_By_Keys_View_DateCreated_Name = "select_Languages_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_Code
public static readonly string  PROC_Select_Languages_By_Keys_View_DateCreated_Code = "select_Languages_by_keys_view_0_5";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Languages_By_Keys_View_DocId_IsDeleted = "select_Languages_by_keys_view_1_2";

//B_E
//1_4
//DocId_Name
public static readonly string  PROC_Select_Languages_By_Keys_View_DocId_Name = "select_Languages_by_keys_view_1_4";

//B_F
//1_5
//DocId_Code
public static readonly string  PROC_Select_Languages_By_Keys_View_DocId_Code = "select_Languages_by_keys_view_1_5";

//C_E
//2_4
//IsDeleted_Name
public static readonly string  PROC_Select_Languages_By_Keys_View_IsDeleted_Name = "select_Languages_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_Code
public static readonly string  PROC_Select_Languages_By_Keys_View_IsDeleted_Code = "select_Languages_by_keys_view_2_5";

//E_F
//4_5
//Name_Code
public static readonly string  PROC_Select_Languages_By_Keys_View_Name_Code = "select_Languages_by_keys_view_4_5";
         #endregion

    }

}
