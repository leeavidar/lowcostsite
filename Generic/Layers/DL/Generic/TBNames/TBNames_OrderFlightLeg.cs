

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_OrderFlightLeg
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertOrderFlightLeg = PROC_Prefix + "save_order_flight_leg";
        #endregion
        #region Select
        public static readonly string PROC_Select_OrderFlightLeg_By_DocId = PROC_Prefix + "Select_order_flight_leg_By_doc_id";        
        public static readonly string PROC_Select_OrderFlightLeg_By_Keys_View = PROC_Prefix + "Select_order_flight_leg_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteOrderFlightLeg = PROC_Prefix + "delete_order_flight_leg";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegIsActive);

        public static readonly string PRM_LowCostPNR = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegLowCostPNR);

        public static readonly string PRM_PriceNet = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegPriceNet);

        public static readonly string PRM_PriceMarkUp = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegPriceMarkUp);

        public static readonly string PRM_FlightType = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegFlightType);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_OrdersDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "OrderFlightLeg.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegIsActive);

        public static readonly string Field_LowCostPNR = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegLowCostPNR);

        public static readonly string Field_PriceNet = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegPriceNet);

        public static readonly string Field_PriceMarkUp = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegPriceMarkUp);

        public static readonly string Field_FlightType = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegFlightType);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_OrdersDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//I_A
//8_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated = "select_OrderFlightLeg_by_keys_view_8_0";

//I_B
//8_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId = "select_OrderFlightLeg_by_keys_view_8_1";

//I_C
//8_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_OrderFlightLeg_by_keys_view_8_2";

//I_E
//8_4
//WhiteLabelDocId_LowCostPNR
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR = "select_OrderFlightLeg_by_keys_view_8_4";

//I_F
//8_5
//WhiteLabelDocId_PriceNet
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet = "select_OrderFlightLeg_by_keys_view_8_5";

//I_G
//8_6
//WhiteLabelDocId_PriceMarkUp
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp = "select_OrderFlightLeg_by_keys_view_8_6";

//I_H
//8_7
//WhiteLabelDocId_FlightType
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_FlightType = "select_OrderFlightLeg_by_keys_view_8_7";

//I_J
//8_9
//WhiteLabelDocId_OrdersDocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_OrdersDocId = "select_OrderFlightLeg_by_keys_view_8_9";

//I_A_B
//8_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_OrderFlightLeg_by_keys_view_8_0_1";

//I_A_C
//8_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_OrderFlightLeg_by_keys_view_8_0_2";

//I_A_E
//8_0_4
//WhiteLabelDocId_DateCreated_LowCostPNR
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR = "select_OrderFlightLeg_by_keys_view_8_0_4";

//I_A_F
//8_0_5
//WhiteLabelDocId_DateCreated_PriceNet
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PriceNet = "select_OrderFlightLeg_by_keys_view_8_0_5";

//I_A_G
//8_0_6
//WhiteLabelDocId_DateCreated_PriceMarkUp
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PriceMarkUp = "select_OrderFlightLeg_by_keys_view_8_0_6";

//I_A_H
//8_0_7
//WhiteLabelDocId_DateCreated_FlightType
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightType = "select_OrderFlightLeg_by_keys_view_8_0_7";

//I_A_J
//8_0_9
//WhiteLabelDocId_DateCreated_OrdersDocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId = "select_OrderFlightLeg_by_keys_view_8_0_9";

//I_B_C
//8_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_OrderFlightLeg_by_keys_view_8_1_2";

//I_B_E
//8_1_4
//WhiteLabelDocId_DocId_LowCostPNR
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR = "select_OrderFlightLeg_by_keys_view_8_1_4";

//I_B_F
//8_1_5
//WhiteLabelDocId_DocId_PriceNet
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PriceNet = "select_OrderFlightLeg_by_keys_view_8_1_5";

//I_B_G
//8_1_6
//WhiteLabelDocId_DocId_PriceMarkUp
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PriceMarkUp = "select_OrderFlightLeg_by_keys_view_8_1_6";

//I_B_H
//8_1_7
//WhiteLabelDocId_DocId_FlightType
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightType = "select_OrderFlightLeg_by_keys_view_8_1_7";

//I_B_J
//8_1_9
//WhiteLabelDocId_DocId_OrdersDocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId = "select_OrderFlightLeg_by_keys_view_8_1_9";

//I_C_E
//8_2_4
//WhiteLabelDocId_IsDeleted_LowCostPNR
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR = "select_OrderFlightLeg_by_keys_view_8_2_4";

//I_C_F
//8_2_5
//WhiteLabelDocId_IsDeleted_PriceNet
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceNet = "select_OrderFlightLeg_by_keys_view_8_2_5";

//I_C_G
//8_2_6
//WhiteLabelDocId_IsDeleted_PriceMarkUp
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceMarkUp = "select_OrderFlightLeg_by_keys_view_8_2_6";

//I_C_H
//8_2_7
//WhiteLabelDocId_IsDeleted_FlightType
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightType = "select_OrderFlightLeg_by_keys_view_8_2_7";

//I_C_J
//8_2_9
//WhiteLabelDocId_IsDeleted_OrdersDocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId = "select_OrderFlightLeg_by_keys_view_8_2_9";

//I_E_F
//8_4_5
//WhiteLabelDocId_LowCostPNR_PriceNet
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_PriceNet = "select_OrderFlightLeg_by_keys_view_8_4_5";

//I_E_G
//8_4_6
//WhiteLabelDocId_LowCostPNR_PriceMarkUp
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_PriceMarkUp = "select_OrderFlightLeg_by_keys_view_8_4_6";

//I_E_H
//8_4_7
//WhiteLabelDocId_LowCostPNR_FlightType
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_FlightType = "select_OrderFlightLeg_by_keys_view_8_4_7";

//I_E_J
//8_4_9
//WhiteLabelDocId_LowCostPNR_OrdersDocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrdersDocId = "select_OrderFlightLeg_by_keys_view_8_4_9";

//I_F_G
//8_5_6
//WhiteLabelDocId_PriceNet_PriceMarkUp
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_PriceMarkUp = "select_OrderFlightLeg_by_keys_view_8_5_6";

//I_F_H
//8_5_7
//WhiteLabelDocId_PriceNet_FlightType
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_FlightType = "select_OrderFlightLeg_by_keys_view_8_5_7";

//I_F_J
//8_5_9
//WhiteLabelDocId_PriceNet_OrdersDocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_OrdersDocId = "select_OrderFlightLeg_by_keys_view_8_5_9";

//I_G_H
//8_6_7
//WhiteLabelDocId_PriceMarkUp_FlightType
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp_FlightType = "select_OrderFlightLeg_by_keys_view_8_6_7";

//I_G_J
//8_6_9
//WhiteLabelDocId_PriceMarkUp_OrdersDocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp_OrdersDocId = "select_OrderFlightLeg_by_keys_view_8_6_9";

//I_H_J
//8_7_9
//WhiteLabelDocId_FlightType_OrdersDocId
public static readonly string  PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_FlightType_OrdersDocId = "select_OrderFlightLeg_by_keys_view_8_7_9";
         #endregion

    }

}
