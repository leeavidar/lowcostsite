

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Images
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertImages = PROC_Prefix + "save_images";
        #endregion
        #region Select
        public static readonly string PROC_Select_Images_By_DocId = PROC_Prefix + "Select_images_By_doc_id";        
        public static readonly string PROC_Select_Images_By_Keys_View = PROC_Prefix + "Select_images_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteImages = PROC_Prefix + "delete_images";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesIsActive);

        public static readonly string PRM_ImageFileName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesImageFileName);

        public static readonly string PRM_RelatedObjectDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesRelatedObjectDocId);

        public static readonly string PRM_ImageType = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesImageType);

        public static readonly string PRM_RelatedObjectType = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesRelatedObjectType);

        public static readonly string PRM_Title = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesTitle);

        public static readonly string PRM_Alt = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesAlt);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Images.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesIsActive);

        public static readonly string Field_ImageFileName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesImageFileName);

        public static readonly string Field_RelatedObjectDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesRelatedObjectDocId);

        public static readonly string Field_ImageType = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesImageType);

        public static readonly string Field_RelatedObjectType = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesRelatedObjectType);

        public static readonly string Field_Title = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesTitle);

        public static readonly string Field_Alt = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyImagesAlt);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated = "select_Images_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Images_By_Keys_View_DocId = "select_Images_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Images_By_Keys_View_IsDeleted = "select_Images_by_keys_view_2";

//E
//4
//ImageFileName
public static readonly string  PROC_Select_Images_By_Keys_View_ImageFileName = "select_Images_by_keys_view_4";

//F
//5
//RelatedObjectDocId
public static readonly string  PROC_Select_Images_By_Keys_View_RelatedObjectDocId = "select_Images_by_keys_view_5";

//G
//6
//ImageType
public static readonly string  PROC_Select_Images_By_Keys_View_ImageType = "select_Images_by_keys_view_6";

//H
//7
//RelatedObjectType
public static readonly string  PROC_Select_Images_By_Keys_View_RelatedObjectType = "select_Images_by_keys_view_7";

//I
//8
//Title
public static readonly string  PROC_Select_Images_By_Keys_View_Title = "select_Images_by_keys_view_8";

//J
//9
//Alt
public static readonly string  PROC_Select_Images_By_Keys_View_Alt = "select_Images_by_keys_view_9";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated_DocId = "select_Images_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated_IsDeleted = "select_Images_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_ImageFileName
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated_ImageFileName = "select_Images_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_RelatedObjectDocId
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated_RelatedObjectDocId = "select_Images_by_keys_view_0_5";

//A_G
//0_6
//DateCreated_ImageType
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated_ImageType = "select_Images_by_keys_view_0_6";

//A_H
//0_7
//DateCreated_RelatedObjectType
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated_RelatedObjectType = "select_Images_by_keys_view_0_7";

//A_I
//0_8
//DateCreated_Title
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated_Title = "select_Images_by_keys_view_0_8";

//A_J
//0_9
//DateCreated_Alt
public static readonly string  PROC_Select_Images_By_Keys_View_DateCreated_Alt = "select_Images_by_keys_view_0_9";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Images_By_Keys_View_DocId_IsDeleted = "select_Images_by_keys_view_1_2";

//B_E
//1_4
//DocId_ImageFileName
public static readonly string  PROC_Select_Images_By_Keys_View_DocId_ImageFileName = "select_Images_by_keys_view_1_4";

//B_F
//1_5
//DocId_RelatedObjectDocId
public static readonly string  PROC_Select_Images_By_Keys_View_DocId_RelatedObjectDocId = "select_Images_by_keys_view_1_5";

//B_G
//1_6
//DocId_ImageType
public static readonly string  PROC_Select_Images_By_Keys_View_DocId_ImageType = "select_Images_by_keys_view_1_6";

//B_H
//1_7
//DocId_RelatedObjectType
public static readonly string  PROC_Select_Images_By_Keys_View_DocId_RelatedObjectType = "select_Images_by_keys_view_1_7";

//B_I
//1_8
//DocId_Title
public static readonly string  PROC_Select_Images_By_Keys_View_DocId_Title = "select_Images_by_keys_view_1_8";

//B_J
//1_9
//DocId_Alt
public static readonly string  PROC_Select_Images_By_Keys_View_DocId_Alt = "select_Images_by_keys_view_1_9";

//C_E
//2_4
//IsDeleted_ImageFileName
public static readonly string  PROC_Select_Images_By_Keys_View_IsDeleted_ImageFileName = "select_Images_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_RelatedObjectDocId
public static readonly string  PROC_Select_Images_By_Keys_View_IsDeleted_RelatedObjectDocId = "select_Images_by_keys_view_2_5";

//C_G
//2_6
//IsDeleted_ImageType
public static readonly string  PROC_Select_Images_By_Keys_View_IsDeleted_ImageType = "select_Images_by_keys_view_2_6";

//C_H
//2_7
//IsDeleted_RelatedObjectType
public static readonly string  PROC_Select_Images_By_Keys_View_IsDeleted_RelatedObjectType = "select_Images_by_keys_view_2_7";

//C_I
//2_8
//IsDeleted_Title
public static readonly string  PROC_Select_Images_By_Keys_View_IsDeleted_Title = "select_Images_by_keys_view_2_8";

//C_J
//2_9
//IsDeleted_Alt
public static readonly string  PROC_Select_Images_By_Keys_View_IsDeleted_Alt = "select_Images_by_keys_view_2_9";

//E_F
//4_5
//ImageFileName_RelatedObjectDocId
public static readonly string  PROC_Select_Images_By_Keys_View_ImageFileName_RelatedObjectDocId = "select_Images_by_keys_view_4_5";

//E_G
//4_6
//ImageFileName_ImageType
public static readonly string  PROC_Select_Images_By_Keys_View_ImageFileName_ImageType = "select_Images_by_keys_view_4_6";

//E_H
//4_7
//ImageFileName_RelatedObjectType
public static readonly string  PROC_Select_Images_By_Keys_View_ImageFileName_RelatedObjectType = "select_Images_by_keys_view_4_7";

//E_I
//4_8
//ImageFileName_Title
public static readonly string  PROC_Select_Images_By_Keys_View_ImageFileName_Title = "select_Images_by_keys_view_4_8";

//E_J
//4_9
//ImageFileName_Alt
public static readonly string  PROC_Select_Images_By_Keys_View_ImageFileName_Alt = "select_Images_by_keys_view_4_9";

//F_G
//5_6
//RelatedObjectDocId_ImageType
public static readonly string  PROC_Select_Images_By_Keys_View_RelatedObjectDocId_ImageType = "select_Images_by_keys_view_5_6";

//F_H
//5_7
//RelatedObjectDocId_RelatedObjectType
public static readonly string  PROC_Select_Images_By_Keys_View_RelatedObjectDocId_RelatedObjectType = "select_Images_by_keys_view_5_7";

//F_I
//5_8
//RelatedObjectDocId_Title
public static readonly string  PROC_Select_Images_By_Keys_View_RelatedObjectDocId_Title = "select_Images_by_keys_view_5_8";

//F_J
//5_9
//RelatedObjectDocId_Alt
public static readonly string  PROC_Select_Images_By_Keys_View_RelatedObjectDocId_Alt = "select_Images_by_keys_view_5_9";

//G_H
//6_7
//ImageType_RelatedObjectType
public static readonly string  PROC_Select_Images_By_Keys_View_ImageType_RelatedObjectType = "select_Images_by_keys_view_6_7";

//G_I
//6_8
//ImageType_Title
public static readonly string  PROC_Select_Images_By_Keys_View_ImageType_Title = "select_Images_by_keys_view_6_8";

//G_J
//6_9
//ImageType_Alt
public static readonly string  PROC_Select_Images_By_Keys_View_ImageType_Alt = "select_Images_by_keys_view_6_9";

//H_I
//7_8
//RelatedObjectType_Title
public static readonly string  PROC_Select_Images_By_Keys_View_RelatedObjectType_Title = "select_Images_by_keys_view_7_8";

//H_J
//7_9
//RelatedObjectType_Alt
public static readonly string  PROC_Select_Images_By_Keys_View_RelatedObjectType_Alt = "select_Images_by_keys_view_7_9";

//I_J
//8_9
//Title_Alt
public static readonly string  PROC_Select_Images_By_Keys_View_Title_Alt = "select_Images_by_keys_view_8_9";
         #endregion

    }

}
