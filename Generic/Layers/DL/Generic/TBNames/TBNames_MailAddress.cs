

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_MailAddress
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertMailAddress = PROC_Prefix + "save_mail_address";
        #endregion
        #region Select
        public static readonly string PROC_Select_MailAddress_By_DocId = PROC_Prefix + "Select_mail_address_By_doc_id";        
        public static readonly string PROC_Select_MailAddress_By_Keys_View = PROC_Prefix + "Select_mail_address_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteMailAddress = PROC_Prefix + "delete_mail_address";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressIsActive);

        public static readonly string PRM_Address = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressAddress);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "MailAddress.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressIsActive);

        public static readonly string Field_Address = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMailAddressAddress);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//F_A
//5_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated = "select_MailAddress_by_keys_view_5_0";

//F_B
//5_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId = "select_MailAddress_by_keys_view_5_1";

//F_C
//5_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_MailAddress_by_keys_view_5_2";

//F_E
//5_4
//WhiteLabelDocId_Address
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_Address = "select_MailAddress_by_keys_view_5_4";

//F_A_B
//5_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_MailAddress_by_keys_view_5_0_1";

//F_A_C
//5_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_MailAddress_by_keys_view_5_0_2";

//F_A_E
//5_0_4
//WhiteLabelDocId_DateCreated_Address
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_Address = "select_MailAddress_by_keys_view_5_0_4";

//F_B_C
//5_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_MailAddress_by_keys_view_5_1_2";

//F_B_E
//5_1_4
//WhiteLabelDocId_DocId_Address
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId_Address = "select_MailAddress_by_keys_view_5_1_4";

//F_C_E
//5_2_4
//WhiteLabelDocId_IsDeleted_Address
public static readonly string  PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_IsDeleted_Address = "select_MailAddress_by_keys_view_5_2_4";
         #endregion

    }

}
