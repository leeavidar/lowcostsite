

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_PopularDestination
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertPopularDestination = PROC_Prefix + "save_popular_destination";
        #endregion
        #region Select
        public static readonly string PROC_Select_PopularDestination_By_DocId = PROC_Prefix + "Select_popular_destination_By_doc_id";        
        public static readonly string PROC_Select_PopularDestination_By_Keys_View = PROC_Prefix + "Select_popular_destination_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeletePopularDestination = PROC_Prefix + "delete_popular_destination";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationIsActive);

        public static readonly string PRM_IataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationIataCode);

        public static readonly string PRM_CityIataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationCityIataCode);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationName);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "PopularDestination.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationIsActive);

        public static readonly string Field_IataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationIataCode);

        public static readonly string Field_CityIataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationCityIataCode);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationName);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//H_A
//7_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated = "select_PopularDestination_by_keys_view_7_0";

//H_B
//7_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId = "select_PopularDestination_by_keys_view_7_1";

//H_C
//7_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_PopularDestination_by_keys_view_7_2";

//H_E
//7_4
//WhiteLabelDocId_IataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode = "select_PopularDestination_by_keys_view_7_4";

//H_F
//7_5
//WhiteLabelDocId_CityIataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_CityIataCode = "select_PopularDestination_by_keys_view_7_5";

//H_G
//7_6
//WhiteLabelDocId_Name
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_Name = "select_PopularDestination_by_keys_view_7_6";

//H_A_B
//7_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_PopularDestination_by_keys_view_7_0_1";

//H_A_C
//7_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_PopularDestination_by_keys_view_7_0_2";

//H_A_E
//7_0_4
//WhiteLabelDocId_DateCreated_IataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_IataCode = "select_PopularDestination_by_keys_view_7_0_4";

//H_A_F
//7_0_5
//WhiteLabelDocId_DateCreated_CityIataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_CityIataCode = "select_PopularDestination_by_keys_view_7_0_5";

//H_A_G
//7_0_6
//WhiteLabelDocId_DateCreated_Name
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_Name = "select_PopularDestination_by_keys_view_7_0_6";

//H_B_C
//7_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_PopularDestination_by_keys_view_7_1_2";

//H_B_E
//7_1_4
//WhiteLabelDocId_DocId_IataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_IataCode = "select_PopularDestination_by_keys_view_7_1_4";

//H_B_F
//7_1_5
//WhiteLabelDocId_DocId_CityIataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_CityIataCode = "select_PopularDestination_by_keys_view_7_1_5";

//H_B_G
//7_1_6
//WhiteLabelDocId_DocId_Name
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_Name = "select_PopularDestination_by_keys_view_7_1_6";

//H_C_E
//7_2_4
//WhiteLabelDocId_IsDeleted_IataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_IataCode = "select_PopularDestination_by_keys_view_7_2_4";

//H_C_F
//7_2_5
//WhiteLabelDocId_IsDeleted_CityIataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_CityIataCode = "select_PopularDestination_by_keys_view_7_2_5";

//H_C_G
//7_2_6
//WhiteLabelDocId_IsDeleted_Name
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_Name = "select_PopularDestination_by_keys_view_7_2_6";

//H_E_F
//7_4_5
//WhiteLabelDocId_IataCode_CityIataCode
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode_CityIataCode = "select_PopularDestination_by_keys_view_7_4_5";

//H_E_G
//7_4_6
//WhiteLabelDocId_IataCode_Name
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode_Name = "select_PopularDestination_by_keys_view_7_4_6";

//H_F_G
//7_5_6
//WhiteLabelDocId_CityIataCode_Name
public static readonly string  PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_CityIataCode_Name = "select_PopularDestination_by_keys_view_7_5_6";
         #endregion

    }

}
