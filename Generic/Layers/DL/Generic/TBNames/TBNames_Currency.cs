

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Currency
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertCurrency = PROC_Prefix + "save_currency";
        #endregion
        #region Select
        public static readonly string PROC_Select_Currency_By_DocId = PROC_Prefix + "Select_currency_By_doc_id";        
        public static readonly string PROC_Select_Currency_By_Keys_View = PROC_Prefix + "Select_currency_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteCurrency = PROC_Prefix + "delete_currency";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyIsActive);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyName);

        public static readonly string PRM_Code = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyCode);

        public static readonly string PRM_Sign = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencySign);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Currency.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyIsActive);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyName);

        public static readonly string Field_Code = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencyCode);

        public static readonly string Field_Sign = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCurrencySign);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Currency_By_Keys_View_DateCreated = "select_Currency_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Currency_By_Keys_View_DocId = "select_Currency_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Currency_By_Keys_View_IsDeleted = "select_Currency_by_keys_view_2";

//E
//4
//Name
public static readonly string  PROC_Select_Currency_By_Keys_View_Name = "select_Currency_by_keys_view_4";

//F
//5
//Code
public static readonly string  PROC_Select_Currency_By_Keys_View_Code = "select_Currency_by_keys_view_5";

//G
//6
//Sign
public static readonly string  PROC_Select_Currency_By_Keys_View_Sign = "select_Currency_by_keys_view_6";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Currency_By_Keys_View_DateCreated_DocId = "select_Currency_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Currency_By_Keys_View_DateCreated_IsDeleted = "select_Currency_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_Name
public static readonly string  PROC_Select_Currency_By_Keys_View_DateCreated_Name = "select_Currency_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_Code
public static readonly string  PROC_Select_Currency_By_Keys_View_DateCreated_Code = "select_Currency_by_keys_view_0_5";

//A_G
//0_6
//DateCreated_Sign
public static readonly string  PROC_Select_Currency_By_Keys_View_DateCreated_Sign = "select_Currency_by_keys_view_0_6";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Currency_By_Keys_View_DocId_IsDeleted = "select_Currency_by_keys_view_1_2";

//B_E
//1_4
//DocId_Name
public static readonly string  PROC_Select_Currency_By_Keys_View_DocId_Name = "select_Currency_by_keys_view_1_4";

//B_F
//1_5
//DocId_Code
public static readonly string  PROC_Select_Currency_By_Keys_View_DocId_Code = "select_Currency_by_keys_view_1_5";

//B_G
//1_6
//DocId_Sign
public static readonly string  PROC_Select_Currency_By_Keys_View_DocId_Sign = "select_Currency_by_keys_view_1_6";

//C_E
//2_4
//IsDeleted_Name
public static readonly string  PROC_Select_Currency_By_Keys_View_IsDeleted_Name = "select_Currency_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_Code
public static readonly string  PROC_Select_Currency_By_Keys_View_IsDeleted_Code = "select_Currency_by_keys_view_2_5";

//C_G
//2_6
//IsDeleted_Sign
public static readonly string  PROC_Select_Currency_By_Keys_View_IsDeleted_Sign = "select_Currency_by_keys_view_2_6";

//E_F
//4_5
//Name_Code
public static readonly string  PROC_Select_Currency_By_Keys_View_Name_Code = "select_Currency_by_keys_view_4_5";

//E_G
//4_6
//Name_Sign
public static readonly string  PROC_Select_Currency_By_Keys_View_Name_Sign = "select_Currency_by_keys_view_4_6";

//F_G
//5_6
//Code_Sign
public static readonly string  PROC_Select_Currency_By_Keys_View_Code_Sign = "select_Currency_by_keys_view_5_6";
         #endregion

    }

}
