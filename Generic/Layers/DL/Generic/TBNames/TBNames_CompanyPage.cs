

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_CompanyPage
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertCompanyPage = PROC_Prefix + "save_company_page";
        #endregion
        #region Select
        public static readonly string PROC_Select_CompanyPage_By_DocId = PROC_Prefix + "Select_company_page_By_doc_id";        
        public static readonly string PROC_Select_CompanyPage_By_Keys_View = PROC_Prefix + "Select_company_page_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteCompanyPage = PROC_Prefix + "delete_company_page";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageIsActive);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageName);

        public static readonly string PRM_SeoDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeoDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_Text = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageText);

        public static readonly string PRM_Title = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageTitle);

        public static readonly string PRM_AirlineDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "CompanyPage.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageIsActive);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageName);

        public static readonly string Field_SeoDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeoDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_Text = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageText);

        public static readonly string Field_Title = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCompanyPageTitle);

        public static readonly string Field_AirlineDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//G_A
//6_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated = "select_CompanyPage_by_keys_view_6_0";

//G_B
//6_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId = "select_CompanyPage_by_keys_view_6_1";

//G_C
//6_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_CompanyPage_by_keys_view_6_2";

//G_E
//6_4
//WhiteLabelDocId_Name
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name = "select_CompanyPage_by_keys_view_6_4";

//G_F
//6_5
//WhiteLabelDocId_SeoDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId = "select_CompanyPage_by_keys_view_6_5";

//G_H
//6_7
//WhiteLabelDocId_Text
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text = "select_CompanyPage_by_keys_view_6_7";

//G_I
//6_8
//WhiteLabelDocId_Title
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Title = "select_CompanyPage_by_keys_view_6_8";

//G_J
//6_9
//WhiteLabelDocId_AirlineDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_AirlineDocId = "select_CompanyPage_by_keys_view_6_9";

//G_A_B
//6_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_CompanyPage_by_keys_view_6_0_1";

//G_A_C
//6_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_CompanyPage_by_keys_view_6_0_2";

//G_A_E
//6_0_4
//WhiteLabelDocId_DateCreated_Name
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Name = "select_CompanyPage_by_keys_view_6_0_4";

//G_A_F
//6_0_5
//WhiteLabelDocId_DateCreated_SeoDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId = "select_CompanyPage_by_keys_view_6_0_5";

//G_A_H
//6_0_7
//WhiteLabelDocId_DateCreated_Text
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Text = "select_CompanyPage_by_keys_view_6_0_7";

//G_A_I
//6_0_8
//WhiteLabelDocId_DateCreated_Title
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Title = "select_CompanyPage_by_keys_view_6_0_8";

//G_A_J
//6_0_9
//WhiteLabelDocId_DateCreated_AirlineDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineDocId = "select_CompanyPage_by_keys_view_6_0_9";

//G_B_C
//6_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_CompanyPage_by_keys_view_6_1_2";

//G_B_E
//6_1_4
//WhiteLabelDocId_DocId_Name
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Name = "select_CompanyPage_by_keys_view_6_1_4";

//G_B_F
//6_1_5
//WhiteLabelDocId_DocId_SeoDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId = "select_CompanyPage_by_keys_view_6_1_5";

//G_B_H
//6_1_7
//WhiteLabelDocId_DocId_Text
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Text = "select_CompanyPage_by_keys_view_6_1_7";

//G_B_I
//6_1_8
//WhiteLabelDocId_DocId_Title
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Title = "select_CompanyPage_by_keys_view_6_1_8";

//G_B_J
//6_1_9
//WhiteLabelDocId_DocId_AirlineDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_AirlineDocId = "select_CompanyPage_by_keys_view_6_1_9";

//G_C_E
//6_2_4
//WhiteLabelDocId_IsDeleted_Name
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Name = "select_CompanyPage_by_keys_view_6_2_4";

//G_C_F
//6_2_5
//WhiteLabelDocId_IsDeleted_SeoDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId = "select_CompanyPage_by_keys_view_6_2_5";

//G_C_H
//6_2_7
//WhiteLabelDocId_IsDeleted_Text
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Text = "select_CompanyPage_by_keys_view_6_2_7";

//G_C_I
//6_2_8
//WhiteLabelDocId_IsDeleted_Title
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Title = "select_CompanyPage_by_keys_view_6_2_8";

//G_C_J
//6_2_9
//WhiteLabelDocId_IsDeleted_AirlineDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineDocId = "select_CompanyPage_by_keys_view_6_2_9";

//G_E_F
//6_4_5
//WhiteLabelDocId_Name_SeoDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_SeoDocId = "select_CompanyPage_by_keys_view_6_4_5";

//G_E_H
//6_4_7
//WhiteLabelDocId_Name_Text
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_Text = "select_CompanyPage_by_keys_view_6_4_7";

//G_E_I
//6_4_8
//WhiteLabelDocId_Name_Title
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_Title = "select_CompanyPage_by_keys_view_6_4_8";

//G_E_J
//6_4_9
//WhiteLabelDocId_Name_AirlineDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_AirlineDocId = "select_CompanyPage_by_keys_view_6_4_9";

//G_F_H
//6_5_7
//WhiteLabelDocId_SeoDocId_Text
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Text = "select_CompanyPage_by_keys_view_6_5_7";

//G_F_I
//6_5_8
//WhiteLabelDocId_SeoDocId_Title
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Title = "select_CompanyPage_by_keys_view_6_5_8";

//G_F_J
//6_5_9
//WhiteLabelDocId_SeoDocId_AirlineDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_AirlineDocId = "select_CompanyPage_by_keys_view_6_5_9";

//G_H_I
//6_7_8
//WhiteLabelDocId_Text_Title
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text_Title = "select_CompanyPage_by_keys_view_6_7_8";

//G_H_J
//6_7_9
//WhiteLabelDocId_Text_AirlineDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text_AirlineDocId = "select_CompanyPage_by_keys_view_6_7_9";

//G_I_J
//6_8_9
//WhiteLabelDocId_Title_AirlineDocId
public static readonly string  PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Title_AirlineDocId = "select_CompanyPage_by_keys_view_6_8_9";
         #endregion

    }

}
