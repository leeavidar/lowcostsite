

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_AirlineFare
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertAirlineFare = PROC_Prefix + "save_airline_fare";
        #endregion
        #region Select
        public static readonly string PROC_Select_AirlineFare_By_DocId = PROC_Prefix + "Select_airline_fare_By_doc_id";        
        public static readonly string PROC_Select_AirlineFare_By_Keys_View = PROC_Prefix + "Select_airline_fare_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteAirlineFare = PROC_Prefix + "delete_airline_fare";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareIsActive);

        public static readonly string PRM_AirlineCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareAirlineCode);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareName);

        public static readonly string PRM_Standard = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareStandard);

        public static readonly string PRM_Flexible = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareFlexible);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "AirlineFare.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareIsActive);

        public static readonly string Field_AirlineCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareAirlineCode);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareName);

        public static readonly string Field_Standard = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareStandard);

        public static readonly string Field_Flexible = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineFareFlexible);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//I_A
//8_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated = "select_AirlineFare_by_keys_view_8_0";

//I_B
//8_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId = "select_AirlineFare_by_keys_view_8_1";

//I_C
//8_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_AirlineFare_by_keys_view_8_2";

//I_E
//8_4
//WhiteLabelDocId_AirlineCode
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode = "select_AirlineFare_by_keys_view_8_4";

//I_F
//8_5
//WhiteLabelDocId_Name
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name = "select_AirlineFare_by_keys_view_8_5";

//I_G
//8_6
//WhiteLabelDocId_Standard
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Standard = "select_AirlineFare_by_keys_view_8_6";

//I_H
//8_7
//WhiteLabelDocId_Flexible
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Flexible = "select_AirlineFare_by_keys_view_8_7";

//I_A_B
//8_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_AirlineFare_by_keys_view_8_0_1";

//I_A_C
//8_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_AirlineFare_by_keys_view_8_0_2";

//I_A_E
//8_0_4
//WhiteLabelDocId_DateCreated_AirlineCode
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineCode = "select_AirlineFare_by_keys_view_8_0_4";

//I_A_F
//8_0_5
//WhiteLabelDocId_DateCreated_Name
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Name = "select_AirlineFare_by_keys_view_8_0_5";

//I_A_G
//8_0_6
//WhiteLabelDocId_DateCreated_Standard
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Standard = "select_AirlineFare_by_keys_view_8_0_6";

//I_A_H
//8_0_7
//WhiteLabelDocId_DateCreated_Flexible
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Flexible = "select_AirlineFare_by_keys_view_8_0_7";

//I_B_C
//8_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_AirlineFare_by_keys_view_8_1_2";

//I_B_E
//8_1_4
//WhiteLabelDocId_DocId_AirlineCode
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_AirlineCode = "select_AirlineFare_by_keys_view_8_1_4";

//I_B_F
//8_1_5
//WhiteLabelDocId_DocId_Name
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Name = "select_AirlineFare_by_keys_view_8_1_5";

//I_B_G
//8_1_6
//WhiteLabelDocId_DocId_Standard
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Standard = "select_AirlineFare_by_keys_view_8_1_6";

//I_B_H
//8_1_7
//WhiteLabelDocId_DocId_Flexible
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Flexible = "select_AirlineFare_by_keys_view_8_1_7";

//I_C_E
//8_2_4
//WhiteLabelDocId_IsDeleted_AirlineCode
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineCode = "select_AirlineFare_by_keys_view_8_2_4";

//I_C_F
//8_2_5
//WhiteLabelDocId_IsDeleted_Name
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Name = "select_AirlineFare_by_keys_view_8_2_5";

//I_C_G
//8_2_6
//WhiteLabelDocId_IsDeleted_Standard
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Standard = "select_AirlineFare_by_keys_view_8_2_6";

//I_C_H
//8_2_7
//WhiteLabelDocId_IsDeleted_Flexible
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Flexible = "select_AirlineFare_by_keys_view_8_2_7";

//I_E_F
//8_4_5
//WhiteLabelDocId_AirlineCode_Name
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Name = "select_AirlineFare_by_keys_view_8_4_5";

//I_E_G
//8_4_6
//WhiteLabelDocId_AirlineCode_Standard
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Standard = "select_AirlineFare_by_keys_view_8_4_6";

//I_E_H
//8_4_7
//WhiteLabelDocId_AirlineCode_Flexible
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Flexible = "select_AirlineFare_by_keys_view_8_4_7";

//I_F_G
//8_5_6
//WhiteLabelDocId_Name_Standard
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name_Standard = "select_AirlineFare_by_keys_view_8_5_6";

//I_F_H
//8_5_7
//WhiteLabelDocId_Name_Flexible
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name_Flexible = "select_AirlineFare_by_keys_view_8_5_7";

//I_G_H
//8_6_7
//WhiteLabelDocId_Standard_Flexible
public static readonly string  PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Standard_Flexible = "select_AirlineFare_by_keys_view_8_6_7";
         #endregion

    }

}
