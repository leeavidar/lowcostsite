

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Airline
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertAirline = PROC_Prefix + "save_airline";
        #endregion
        #region Select
        public static readonly string PROC_Select_Airline_By_DocId = PROC_Prefix + "Select_airline_By_doc_id";        
        public static readonly string PROC_Select_Airline_By_Keys_View = PROC_Prefix + "Select_airline_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteAirline = PROC_Prefix + "delete_airline";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineIsActive);

        public static readonly string PRM_IataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineIataCode);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineName);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Airline.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineIsActive);

        public static readonly string Field_IataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineIataCode);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirlineName);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Airline_By_Keys_View_DateCreated = "select_Airline_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Airline_By_Keys_View_DocId = "select_Airline_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Airline_By_Keys_View_IsDeleted = "select_Airline_by_keys_view_2";

//E
//4
//IataCode
public static readonly string  PROC_Select_Airline_By_Keys_View_IataCode = "select_Airline_by_keys_view_4";

//F
//5
//Name
public static readonly string  PROC_Select_Airline_By_Keys_View_Name = "select_Airline_by_keys_view_5";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Airline_By_Keys_View_DateCreated_DocId = "select_Airline_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Airline_By_Keys_View_DateCreated_IsDeleted = "select_Airline_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_IataCode
public static readonly string  PROC_Select_Airline_By_Keys_View_DateCreated_IataCode = "select_Airline_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_Name
public static readonly string  PROC_Select_Airline_By_Keys_View_DateCreated_Name = "select_Airline_by_keys_view_0_5";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Airline_By_Keys_View_DocId_IsDeleted = "select_Airline_by_keys_view_1_2";

//B_E
//1_4
//DocId_IataCode
public static readonly string  PROC_Select_Airline_By_Keys_View_DocId_IataCode = "select_Airline_by_keys_view_1_4";

//B_F
//1_5
//DocId_Name
public static readonly string  PROC_Select_Airline_By_Keys_View_DocId_Name = "select_Airline_by_keys_view_1_5";

//C_E
//2_4
//IsDeleted_IataCode
public static readonly string  PROC_Select_Airline_By_Keys_View_IsDeleted_IataCode = "select_Airline_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_Name
public static readonly string  PROC_Select_Airline_By_Keys_View_IsDeleted_Name = "select_Airline_by_keys_view_2_5";

//E_F
//4_5
//IataCode_Name
public static readonly string  PROC_Select_Airline_By_Keys_View_IataCode_Name = "select_Airline_by_keys_view_4_5";
         #endregion

    }

}
