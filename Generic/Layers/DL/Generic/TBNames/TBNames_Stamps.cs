

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Stamps
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertStamps = PROC_Prefix + "save_stamps";
        #endregion
        #region Select
        public static readonly string PROC_Select_Stamps_By_DocId = PROC_Prefix + "Select_stamps_By_doc_id";        
        public static readonly string PROC_Select_Stamps_By_Keys_View = PROC_Prefix + "Select_stamps_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteStamps = PROC_Prefix + "delete_stamps";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsIsActive);

        public static readonly string PRM_Text = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsText);

        public static readonly string PRM_Type = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsType);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Stamps.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsIsActive);

        public static readonly string Field_Text = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsText);

        public static readonly string Field_Type = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsType);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Stamps_By_Keys_View_DateCreated = "select_Stamps_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Stamps_By_Keys_View_DocId = "select_Stamps_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Stamps_By_Keys_View_IsDeleted = "select_Stamps_by_keys_view_2";

//E
//4
//Text
public static readonly string  PROC_Select_Stamps_By_Keys_View_Text = "select_Stamps_by_keys_view_4";

//F
//5
//Type
public static readonly string  PROC_Select_Stamps_By_Keys_View_Type = "select_Stamps_by_keys_view_5";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Stamps_By_Keys_View_DateCreated_DocId = "select_Stamps_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Stamps_By_Keys_View_DateCreated_IsDeleted = "select_Stamps_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_Text
public static readonly string  PROC_Select_Stamps_By_Keys_View_DateCreated_Text = "select_Stamps_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_Type
public static readonly string  PROC_Select_Stamps_By_Keys_View_DateCreated_Type = "select_Stamps_by_keys_view_0_5";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Stamps_By_Keys_View_DocId_IsDeleted = "select_Stamps_by_keys_view_1_2";

//B_E
//1_4
//DocId_Text
public static readonly string  PROC_Select_Stamps_By_Keys_View_DocId_Text = "select_Stamps_by_keys_view_1_4";

//B_F
//1_5
//DocId_Type
public static readonly string  PROC_Select_Stamps_By_Keys_View_DocId_Type = "select_Stamps_by_keys_view_1_5";

//C_E
//2_4
//IsDeleted_Text
public static readonly string  PROC_Select_Stamps_By_Keys_View_IsDeleted_Text = "select_Stamps_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_Type
public static readonly string  PROC_Select_Stamps_By_Keys_View_IsDeleted_Type = "select_Stamps_by_keys_view_2_5";

//E_F
//4_5
//Text_Type
public static readonly string  PROC_Select_Stamps_By_Keys_View_Text_Type = "select_Stamps_by_keys_view_4_5";
         #endregion

    }

}
