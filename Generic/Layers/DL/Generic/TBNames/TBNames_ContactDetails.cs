

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_ContactDetails
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertContactDetails = PROC_Prefix + "save_contact_details";
        #endregion
        #region Select
        public static readonly string PROC_Select_ContactDetails_By_DocId = PROC_Prefix + "Select_contact_details_By_doc_id";        
        public static readonly string PROC_Select_ContactDetails_By_Keys_View = PROC_Prefix + "Select_contact_details_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteContactDetails = PROC_Prefix + "delete_contact_details";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsIsActive);

        public static readonly string PRM_FirstName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsFirstName);

        public static readonly string PRM_LastName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsLastName);

        public static readonly string PRM_Company = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsCompany);

        public static readonly string PRM_Flat = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsFlat);

        public static readonly string PRM_BuildingName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsBuildingName);

        public static readonly string PRM_BuildingNumber = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsBuildingNumber);

        public static readonly string PRM_Street = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsStreet);

        public static readonly string PRM_Locality = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsLocality);

        public static readonly string PRM_City = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsCity);

        public static readonly string PRM_Province = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsProvince);

        public static readonly string PRM_ZipCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsZipCode);

        public static readonly string PRM_CountryCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsCountryCode);

        public static readonly string PRM_Email = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsEmail);

        public static readonly string PRM_MainPhone = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsMainPhone);

        public static readonly string PRM_MobliePhone = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsMobliePhone);

        public static readonly string PRM_OrdersDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_Title = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsTitle);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "ContactDetails.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsIsActive);

        public static readonly string Field_FirstName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsFirstName);

        public static readonly string Field_LastName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsLastName);

        public static readonly string Field_Company = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsCompany);

        public static readonly string Field_Flat = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsFlat);

        public static readonly string Field_BuildingName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsBuildingName);

        public static readonly string Field_BuildingNumber = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsBuildingNumber);

        public static readonly string Field_Street = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsStreet);

        public static readonly string Field_Locality = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsLocality);

        public static readonly string Field_City = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsCity);

        public static readonly string Field_Province = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsProvince);

        public static readonly string Field_ZipCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsZipCode);

        public static readonly string Field_CountryCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsCountryCode);

        public static readonly string Field_Email = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsEmail);

        public static readonly string Field_MainPhone = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsMainPhone);

        public static readonly string Field_MobliePhone = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsMobliePhone);

        public static readonly string Field_OrdersDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_Title = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsTitle);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//U_A
//20_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated = "select_ContactDetails_by_keys_view_20_0";

//U_B
//20_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId = "select_ContactDetails_by_keys_view_20_1";

//U_C
//20_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_ContactDetails_by_keys_view_20_2";

//U_E
//20_4
//WhiteLabelDocId_FirstName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName = "select_ContactDetails_by_keys_view_20_4";

//U_F
//20_5
//WhiteLabelDocId_LastName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName = "select_ContactDetails_by_keys_view_20_5";

//U_G
//20_6
//WhiteLabelDocId_Company
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company = "select_ContactDetails_by_keys_view_20_6";

//U_H
//20_7
//WhiteLabelDocId_Flat
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat = "select_ContactDetails_by_keys_view_20_7";

//U_I
//20_8
//WhiteLabelDocId_BuildingName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName = "select_ContactDetails_by_keys_view_20_8";

//U_J
//20_9
//WhiteLabelDocId_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber = "select_ContactDetails_by_keys_view_20_9";

//U_K
//20_10
//WhiteLabelDocId_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street = "select_ContactDetails_by_keys_view_20_10";

//U_L
//20_11
//WhiteLabelDocId_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality = "select_ContactDetails_by_keys_view_20_11";

//U_M
//20_12
//WhiteLabelDocId_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City = "select_ContactDetails_by_keys_view_20_12";

//U_N
//20_13
//WhiteLabelDocId_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province = "select_ContactDetails_by_keys_view_20_13";

//U_O
//20_14
//WhiteLabelDocId_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode = "select_ContactDetails_by_keys_view_20_14";

//U_P
//20_15
//WhiteLabelDocId_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode = "select_ContactDetails_by_keys_view_20_15";

//U_Q
//20_16
//WhiteLabelDocId_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email = "select_ContactDetails_by_keys_view_20_16";

//U_R
//20_17
//WhiteLabelDocId_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone = "select_ContactDetails_by_keys_view_20_17";

//U_S
//20_18
//WhiteLabelDocId_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone = "select_ContactDetails_by_keys_view_20_18";

//U_T
//20_19
//WhiteLabelDocId_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_OrdersDocId = "select_ContactDetails_by_keys_view_20_19";

//U_V
//20_21
//WhiteLabelDocId_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Title = "select_ContactDetails_by_keys_view_20_21";

//U_A_B
//20_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_ContactDetails_by_keys_view_20_0_1";

//U_A_C
//20_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_ContactDetails_by_keys_view_20_0_2";

//U_A_E
//20_0_4
//WhiteLabelDocId_DateCreated_FirstName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName = "select_ContactDetails_by_keys_view_20_0_4";

//U_A_F
//20_0_5
//WhiteLabelDocId_DateCreated_LastName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_LastName = "select_ContactDetails_by_keys_view_20_0_5";

//U_A_G
//20_0_6
//WhiteLabelDocId_DateCreated_Company
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Company = "select_ContactDetails_by_keys_view_20_0_6";

//U_A_H
//20_0_7
//WhiteLabelDocId_DateCreated_Flat
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Flat = "select_ContactDetails_by_keys_view_20_0_7";

//U_A_I
//20_0_8
//WhiteLabelDocId_DateCreated_BuildingName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_BuildingName = "select_ContactDetails_by_keys_view_20_0_8";

//U_A_J
//20_0_9
//WhiteLabelDocId_DateCreated_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_BuildingNumber = "select_ContactDetails_by_keys_view_20_0_9";

//U_A_K
//20_0_10
//WhiteLabelDocId_DateCreated_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Street = "select_ContactDetails_by_keys_view_20_0_10";

//U_A_L
//20_0_11
//WhiteLabelDocId_DateCreated_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Locality = "select_ContactDetails_by_keys_view_20_0_11";

//U_A_M
//20_0_12
//WhiteLabelDocId_DateCreated_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_City = "select_ContactDetails_by_keys_view_20_0_12";

//U_A_N
//20_0_13
//WhiteLabelDocId_DateCreated_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Province = "select_ContactDetails_by_keys_view_20_0_13";

//U_A_O
//20_0_14
//WhiteLabelDocId_DateCreated_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_ZipCode = "select_ContactDetails_by_keys_view_20_0_14";

//U_A_P
//20_0_15
//WhiteLabelDocId_DateCreated_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_CountryCode = "select_ContactDetails_by_keys_view_20_0_15";

//U_A_Q
//20_0_16
//WhiteLabelDocId_DateCreated_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Email = "select_ContactDetails_by_keys_view_20_0_16";

//U_A_R
//20_0_17
//WhiteLabelDocId_DateCreated_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_MainPhone = "select_ContactDetails_by_keys_view_20_0_17";

//U_A_S
//20_0_18
//WhiteLabelDocId_DateCreated_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_MobliePhone = "select_ContactDetails_by_keys_view_20_0_18";

//U_A_T
//20_0_19
//WhiteLabelDocId_DateCreated_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId = "select_ContactDetails_by_keys_view_20_0_19";

//U_A_V
//20_0_21
//WhiteLabelDocId_DateCreated_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Title = "select_ContactDetails_by_keys_view_20_0_21";

//U_B_C
//20_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_ContactDetails_by_keys_view_20_1_2";

//U_B_E
//20_1_4
//WhiteLabelDocId_DocId_FirstName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_FirstName = "select_ContactDetails_by_keys_view_20_1_4";

//U_B_F
//20_1_5
//WhiteLabelDocId_DocId_LastName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_LastName = "select_ContactDetails_by_keys_view_20_1_5";

//U_B_G
//20_1_6
//WhiteLabelDocId_DocId_Company
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Company = "select_ContactDetails_by_keys_view_20_1_6";

//U_B_H
//20_1_7
//WhiteLabelDocId_DocId_Flat
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Flat = "select_ContactDetails_by_keys_view_20_1_7";

//U_B_I
//20_1_8
//WhiteLabelDocId_DocId_BuildingName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_BuildingName = "select_ContactDetails_by_keys_view_20_1_8";

//U_B_J
//20_1_9
//WhiteLabelDocId_DocId_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_BuildingNumber = "select_ContactDetails_by_keys_view_20_1_9";

//U_B_K
//20_1_10
//WhiteLabelDocId_DocId_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Street = "select_ContactDetails_by_keys_view_20_1_10";

//U_B_L
//20_1_11
//WhiteLabelDocId_DocId_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Locality = "select_ContactDetails_by_keys_view_20_1_11";

//U_B_M
//20_1_12
//WhiteLabelDocId_DocId_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_City = "select_ContactDetails_by_keys_view_20_1_12";

//U_B_N
//20_1_13
//WhiteLabelDocId_DocId_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Province = "select_ContactDetails_by_keys_view_20_1_13";

//U_B_O
//20_1_14
//WhiteLabelDocId_DocId_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_ZipCode = "select_ContactDetails_by_keys_view_20_1_14";

//U_B_P
//20_1_15
//WhiteLabelDocId_DocId_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_CountryCode = "select_ContactDetails_by_keys_view_20_1_15";

//U_B_Q
//20_1_16
//WhiteLabelDocId_DocId_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Email = "select_ContactDetails_by_keys_view_20_1_16";

//U_B_R
//20_1_17
//WhiteLabelDocId_DocId_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_MainPhone = "select_ContactDetails_by_keys_view_20_1_17";

//U_B_S
//20_1_18
//WhiteLabelDocId_DocId_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_MobliePhone = "select_ContactDetails_by_keys_view_20_1_18";

//U_B_T
//20_1_19
//WhiteLabelDocId_DocId_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId = "select_ContactDetails_by_keys_view_20_1_19";

//U_B_V
//20_1_21
//WhiteLabelDocId_DocId_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Title = "select_ContactDetails_by_keys_view_20_1_21";

//U_C_E
//20_2_4
//WhiteLabelDocId_IsDeleted_FirstName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName = "select_ContactDetails_by_keys_view_20_2_4";

//U_C_F
//20_2_5
//WhiteLabelDocId_IsDeleted_LastName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName = "select_ContactDetails_by_keys_view_20_2_5";

//U_C_G
//20_2_6
//WhiteLabelDocId_IsDeleted_Company
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Company = "select_ContactDetails_by_keys_view_20_2_6";

//U_C_H
//20_2_7
//WhiteLabelDocId_IsDeleted_Flat
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Flat = "select_ContactDetails_by_keys_view_20_2_7";

//U_C_I
//20_2_8
//WhiteLabelDocId_IsDeleted_BuildingName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_BuildingName = "select_ContactDetails_by_keys_view_20_2_8";

//U_C_J
//20_2_9
//WhiteLabelDocId_IsDeleted_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_BuildingNumber = "select_ContactDetails_by_keys_view_20_2_9";

//U_C_K
//20_2_10
//WhiteLabelDocId_IsDeleted_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Street = "select_ContactDetails_by_keys_view_20_2_10";

//U_C_L
//20_2_11
//WhiteLabelDocId_IsDeleted_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Locality = "select_ContactDetails_by_keys_view_20_2_11";

//U_C_M
//20_2_12
//WhiteLabelDocId_IsDeleted_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_City = "select_ContactDetails_by_keys_view_20_2_12";

//U_C_N
//20_2_13
//WhiteLabelDocId_IsDeleted_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Province = "select_ContactDetails_by_keys_view_20_2_13";

//U_C_O
//20_2_14
//WhiteLabelDocId_IsDeleted_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_ZipCode = "select_ContactDetails_by_keys_view_20_2_14";

//U_C_P
//20_2_15
//WhiteLabelDocId_IsDeleted_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_CountryCode = "select_ContactDetails_by_keys_view_20_2_15";

//U_C_Q
//20_2_16
//WhiteLabelDocId_IsDeleted_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Email = "select_ContactDetails_by_keys_view_20_2_16";

//U_C_R
//20_2_17
//WhiteLabelDocId_IsDeleted_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_MainPhone = "select_ContactDetails_by_keys_view_20_2_17";

//U_C_S
//20_2_18
//WhiteLabelDocId_IsDeleted_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_MobliePhone = "select_ContactDetails_by_keys_view_20_2_18";

//U_C_T
//20_2_19
//WhiteLabelDocId_IsDeleted_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId = "select_ContactDetails_by_keys_view_20_2_19";

//U_C_V
//20_2_21
//WhiteLabelDocId_IsDeleted_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Title = "select_ContactDetails_by_keys_view_20_2_21";

//U_E_F
//20_4_5
//WhiteLabelDocId_FirstName_LastName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_LastName = "select_ContactDetails_by_keys_view_20_4_5";

//U_E_G
//20_4_6
//WhiteLabelDocId_FirstName_Company
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Company = "select_ContactDetails_by_keys_view_20_4_6";

//U_E_H
//20_4_7
//WhiteLabelDocId_FirstName_Flat
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Flat = "select_ContactDetails_by_keys_view_20_4_7";

//U_E_I
//20_4_8
//WhiteLabelDocId_FirstName_BuildingName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_BuildingName = "select_ContactDetails_by_keys_view_20_4_8";

//U_E_J
//20_4_9
//WhiteLabelDocId_FirstName_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_BuildingNumber = "select_ContactDetails_by_keys_view_20_4_9";

//U_E_K
//20_4_10
//WhiteLabelDocId_FirstName_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Street = "select_ContactDetails_by_keys_view_20_4_10";

//U_E_L
//20_4_11
//WhiteLabelDocId_FirstName_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Locality = "select_ContactDetails_by_keys_view_20_4_11";

//U_E_M
//20_4_12
//WhiteLabelDocId_FirstName_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_City = "select_ContactDetails_by_keys_view_20_4_12";

//U_E_N
//20_4_13
//WhiteLabelDocId_FirstName_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Province = "select_ContactDetails_by_keys_view_20_4_13";

//U_E_O
//20_4_14
//WhiteLabelDocId_FirstName_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_ZipCode = "select_ContactDetails_by_keys_view_20_4_14";

//U_E_P
//20_4_15
//WhiteLabelDocId_FirstName_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_CountryCode = "select_ContactDetails_by_keys_view_20_4_15";

//U_E_Q
//20_4_16
//WhiteLabelDocId_FirstName_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Email = "select_ContactDetails_by_keys_view_20_4_16";

//U_E_R
//20_4_17
//WhiteLabelDocId_FirstName_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_MainPhone = "select_ContactDetails_by_keys_view_20_4_17";

//U_E_S
//20_4_18
//WhiteLabelDocId_FirstName_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_MobliePhone = "select_ContactDetails_by_keys_view_20_4_18";

//U_E_T
//20_4_19
//WhiteLabelDocId_FirstName_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_OrdersDocId = "select_ContactDetails_by_keys_view_20_4_19";

//U_E_V
//20_4_21
//WhiteLabelDocId_FirstName_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Title = "select_ContactDetails_by_keys_view_20_4_21";

//U_F_G
//20_5_6
//WhiteLabelDocId_LastName_Company
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Company = "select_ContactDetails_by_keys_view_20_5_6";

//U_F_H
//20_5_7
//WhiteLabelDocId_LastName_Flat
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Flat = "select_ContactDetails_by_keys_view_20_5_7";

//U_F_I
//20_5_8
//WhiteLabelDocId_LastName_BuildingName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_BuildingName = "select_ContactDetails_by_keys_view_20_5_8";

//U_F_J
//20_5_9
//WhiteLabelDocId_LastName_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_BuildingNumber = "select_ContactDetails_by_keys_view_20_5_9";

//U_F_K
//20_5_10
//WhiteLabelDocId_LastName_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Street = "select_ContactDetails_by_keys_view_20_5_10";

//U_F_L
//20_5_11
//WhiteLabelDocId_LastName_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Locality = "select_ContactDetails_by_keys_view_20_5_11";

//U_F_M
//20_5_12
//WhiteLabelDocId_LastName_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_City = "select_ContactDetails_by_keys_view_20_5_12";

//U_F_N
//20_5_13
//WhiteLabelDocId_LastName_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Province = "select_ContactDetails_by_keys_view_20_5_13";

//U_F_O
//20_5_14
//WhiteLabelDocId_LastName_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_ZipCode = "select_ContactDetails_by_keys_view_20_5_14";

//U_F_P
//20_5_15
//WhiteLabelDocId_LastName_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_CountryCode = "select_ContactDetails_by_keys_view_20_5_15";

//U_F_Q
//20_5_16
//WhiteLabelDocId_LastName_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Email = "select_ContactDetails_by_keys_view_20_5_16";

//U_F_R
//20_5_17
//WhiteLabelDocId_LastName_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_MainPhone = "select_ContactDetails_by_keys_view_20_5_17";

//U_F_S
//20_5_18
//WhiteLabelDocId_LastName_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_MobliePhone = "select_ContactDetails_by_keys_view_20_5_18";

//U_F_T
//20_5_19
//WhiteLabelDocId_LastName_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_OrdersDocId = "select_ContactDetails_by_keys_view_20_5_19";

//U_F_V
//20_5_21
//WhiteLabelDocId_LastName_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Title = "select_ContactDetails_by_keys_view_20_5_21";

//U_G_H
//20_6_7
//WhiteLabelDocId_Company_Flat
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Flat = "select_ContactDetails_by_keys_view_20_6_7";

//U_G_I
//20_6_8
//WhiteLabelDocId_Company_BuildingName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_BuildingName = "select_ContactDetails_by_keys_view_20_6_8";

//U_G_J
//20_6_9
//WhiteLabelDocId_Company_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_BuildingNumber = "select_ContactDetails_by_keys_view_20_6_9";

//U_G_K
//20_6_10
//WhiteLabelDocId_Company_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Street = "select_ContactDetails_by_keys_view_20_6_10";

//U_G_L
//20_6_11
//WhiteLabelDocId_Company_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Locality = "select_ContactDetails_by_keys_view_20_6_11";

//U_G_M
//20_6_12
//WhiteLabelDocId_Company_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_City = "select_ContactDetails_by_keys_view_20_6_12";

//U_G_N
//20_6_13
//WhiteLabelDocId_Company_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Province = "select_ContactDetails_by_keys_view_20_6_13";

//U_G_O
//20_6_14
//WhiteLabelDocId_Company_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_ZipCode = "select_ContactDetails_by_keys_view_20_6_14";

//U_G_P
//20_6_15
//WhiteLabelDocId_Company_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_CountryCode = "select_ContactDetails_by_keys_view_20_6_15";

//U_G_Q
//20_6_16
//WhiteLabelDocId_Company_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Email = "select_ContactDetails_by_keys_view_20_6_16";

//U_G_R
//20_6_17
//WhiteLabelDocId_Company_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_MainPhone = "select_ContactDetails_by_keys_view_20_6_17";

//U_G_S
//20_6_18
//WhiteLabelDocId_Company_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_MobliePhone = "select_ContactDetails_by_keys_view_20_6_18";

//U_G_T
//20_6_19
//WhiteLabelDocId_Company_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_OrdersDocId = "select_ContactDetails_by_keys_view_20_6_19";

//U_G_V
//20_6_21
//WhiteLabelDocId_Company_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Title = "select_ContactDetails_by_keys_view_20_6_21";

//U_H_I
//20_7_8
//WhiteLabelDocId_Flat_BuildingName
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_BuildingName = "select_ContactDetails_by_keys_view_20_7_8";

//U_H_J
//20_7_9
//WhiteLabelDocId_Flat_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_BuildingNumber = "select_ContactDetails_by_keys_view_20_7_9";

//U_H_K
//20_7_10
//WhiteLabelDocId_Flat_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Street = "select_ContactDetails_by_keys_view_20_7_10";

//U_H_L
//20_7_11
//WhiteLabelDocId_Flat_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Locality = "select_ContactDetails_by_keys_view_20_7_11";

//U_H_M
//20_7_12
//WhiteLabelDocId_Flat_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_City = "select_ContactDetails_by_keys_view_20_7_12";

//U_H_N
//20_7_13
//WhiteLabelDocId_Flat_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Province = "select_ContactDetails_by_keys_view_20_7_13";

//U_H_O
//20_7_14
//WhiteLabelDocId_Flat_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_ZipCode = "select_ContactDetails_by_keys_view_20_7_14";

//U_H_P
//20_7_15
//WhiteLabelDocId_Flat_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_CountryCode = "select_ContactDetails_by_keys_view_20_7_15";

//U_H_Q
//20_7_16
//WhiteLabelDocId_Flat_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Email = "select_ContactDetails_by_keys_view_20_7_16";

//U_H_R
//20_7_17
//WhiteLabelDocId_Flat_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_MainPhone = "select_ContactDetails_by_keys_view_20_7_17";

//U_H_S
//20_7_18
//WhiteLabelDocId_Flat_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_MobliePhone = "select_ContactDetails_by_keys_view_20_7_18";

//U_H_T
//20_7_19
//WhiteLabelDocId_Flat_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_OrdersDocId = "select_ContactDetails_by_keys_view_20_7_19";

//U_H_V
//20_7_21
//WhiteLabelDocId_Flat_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Title = "select_ContactDetails_by_keys_view_20_7_21";

//U_I_J
//20_8_9
//WhiteLabelDocId_BuildingName_BuildingNumber
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_BuildingNumber = "select_ContactDetails_by_keys_view_20_8_9";

//U_I_K
//20_8_10
//WhiteLabelDocId_BuildingName_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Street = "select_ContactDetails_by_keys_view_20_8_10";

//U_I_L
//20_8_11
//WhiteLabelDocId_BuildingName_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Locality = "select_ContactDetails_by_keys_view_20_8_11";

//U_I_M
//20_8_12
//WhiteLabelDocId_BuildingName_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_City = "select_ContactDetails_by_keys_view_20_8_12";

//U_I_N
//20_8_13
//WhiteLabelDocId_BuildingName_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Province = "select_ContactDetails_by_keys_view_20_8_13";

//U_I_O
//20_8_14
//WhiteLabelDocId_BuildingName_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_ZipCode = "select_ContactDetails_by_keys_view_20_8_14";

//U_I_P
//20_8_15
//WhiteLabelDocId_BuildingName_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_CountryCode = "select_ContactDetails_by_keys_view_20_8_15";

//U_I_Q
//20_8_16
//WhiteLabelDocId_BuildingName_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Email = "select_ContactDetails_by_keys_view_20_8_16";

//U_I_R
//20_8_17
//WhiteLabelDocId_BuildingName_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_MainPhone = "select_ContactDetails_by_keys_view_20_8_17";

//U_I_S
//20_8_18
//WhiteLabelDocId_BuildingName_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_MobliePhone = "select_ContactDetails_by_keys_view_20_8_18";

//U_I_T
//20_8_19
//WhiteLabelDocId_BuildingName_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_OrdersDocId = "select_ContactDetails_by_keys_view_20_8_19";

//U_I_V
//20_8_21
//WhiteLabelDocId_BuildingName_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Title = "select_ContactDetails_by_keys_view_20_8_21";

//U_J_K
//20_9_10
//WhiteLabelDocId_BuildingNumber_Street
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Street = "select_ContactDetails_by_keys_view_20_9_10";

//U_J_L
//20_9_11
//WhiteLabelDocId_BuildingNumber_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Locality = "select_ContactDetails_by_keys_view_20_9_11";

//U_J_M
//20_9_12
//WhiteLabelDocId_BuildingNumber_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_City = "select_ContactDetails_by_keys_view_20_9_12";

//U_J_N
//20_9_13
//WhiteLabelDocId_BuildingNumber_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Province = "select_ContactDetails_by_keys_view_20_9_13";

//U_J_O
//20_9_14
//WhiteLabelDocId_BuildingNumber_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_ZipCode = "select_ContactDetails_by_keys_view_20_9_14";

//U_J_P
//20_9_15
//WhiteLabelDocId_BuildingNumber_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_CountryCode = "select_ContactDetails_by_keys_view_20_9_15";

//U_J_Q
//20_9_16
//WhiteLabelDocId_BuildingNumber_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Email = "select_ContactDetails_by_keys_view_20_9_16";

//U_J_R
//20_9_17
//WhiteLabelDocId_BuildingNumber_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_MainPhone = "select_ContactDetails_by_keys_view_20_9_17";

//U_J_S
//20_9_18
//WhiteLabelDocId_BuildingNumber_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_MobliePhone = "select_ContactDetails_by_keys_view_20_9_18";

//U_J_T
//20_9_19
//WhiteLabelDocId_BuildingNumber_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_OrdersDocId = "select_ContactDetails_by_keys_view_20_9_19";

//U_J_V
//20_9_21
//WhiteLabelDocId_BuildingNumber_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Title = "select_ContactDetails_by_keys_view_20_9_21";

//U_K_L
//20_10_11
//WhiteLabelDocId_Street_Locality
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Locality = "select_ContactDetails_by_keys_view_20_10_11";

//U_K_M
//20_10_12
//WhiteLabelDocId_Street_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_City = "select_ContactDetails_by_keys_view_20_10_12";

//U_K_N
//20_10_13
//WhiteLabelDocId_Street_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Province = "select_ContactDetails_by_keys_view_20_10_13";

//U_K_O
//20_10_14
//WhiteLabelDocId_Street_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_ZipCode = "select_ContactDetails_by_keys_view_20_10_14";

//U_K_P
//20_10_15
//WhiteLabelDocId_Street_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_CountryCode = "select_ContactDetails_by_keys_view_20_10_15";

//U_K_Q
//20_10_16
//WhiteLabelDocId_Street_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Email = "select_ContactDetails_by_keys_view_20_10_16";

//U_K_R
//20_10_17
//WhiteLabelDocId_Street_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_MainPhone = "select_ContactDetails_by_keys_view_20_10_17";

//U_K_S
//20_10_18
//WhiteLabelDocId_Street_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_MobliePhone = "select_ContactDetails_by_keys_view_20_10_18";

//U_K_T
//20_10_19
//WhiteLabelDocId_Street_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_OrdersDocId = "select_ContactDetails_by_keys_view_20_10_19";

//U_K_V
//20_10_21
//WhiteLabelDocId_Street_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Title = "select_ContactDetails_by_keys_view_20_10_21";

//U_L_M
//20_11_12
//WhiteLabelDocId_Locality_City
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_City = "select_ContactDetails_by_keys_view_20_11_12";

//U_L_N
//20_11_13
//WhiteLabelDocId_Locality_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Province = "select_ContactDetails_by_keys_view_20_11_13";

//U_L_O
//20_11_14
//WhiteLabelDocId_Locality_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_ZipCode = "select_ContactDetails_by_keys_view_20_11_14";

//U_L_P
//20_11_15
//WhiteLabelDocId_Locality_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_CountryCode = "select_ContactDetails_by_keys_view_20_11_15";

//U_L_Q
//20_11_16
//WhiteLabelDocId_Locality_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Email = "select_ContactDetails_by_keys_view_20_11_16";

//U_L_R
//20_11_17
//WhiteLabelDocId_Locality_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_MainPhone = "select_ContactDetails_by_keys_view_20_11_17";

//U_L_S
//20_11_18
//WhiteLabelDocId_Locality_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_MobliePhone = "select_ContactDetails_by_keys_view_20_11_18";

//U_L_T
//20_11_19
//WhiteLabelDocId_Locality_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_OrdersDocId = "select_ContactDetails_by_keys_view_20_11_19";

//U_L_V
//20_11_21
//WhiteLabelDocId_Locality_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Title = "select_ContactDetails_by_keys_view_20_11_21";

//U_M_N
//20_12_13
//WhiteLabelDocId_City_Province
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Province = "select_ContactDetails_by_keys_view_20_12_13";

//U_M_O
//20_12_14
//WhiteLabelDocId_City_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_ZipCode = "select_ContactDetails_by_keys_view_20_12_14";

//U_M_P
//20_12_15
//WhiteLabelDocId_City_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_CountryCode = "select_ContactDetails_by_keys_view_20_12_15";

//U_M_Q
//20_12_16
//WhiteLabelDocId_City_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Email = "select_ContactDetails_by_keys_view_20_12_16";

//U_M_R
//20_12_17
//WhiteLabelDocId_City_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_MainPhone = "select_ContactDetails_by_keys_view_20_12_17";

//U_M_S
//20_12_18
//WhiteLabelDocId_City_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_MobliePhone = "select_ContactDetails_by_keys_view_20_12_18";

//U_M_T
//20_12_19
//WhiteLabelDocId_City_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_OrdersDocId = "select_ContactDetails_by_keys_view_20_12_19";

//U_M_V
//20_12_21
//WhiteLabelDocId_City_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Title = "select_ContactDetails_by_keys_view_20_12_21";

//U_N_O
//20_13_14
//WhiteLabelDocId_Province_ZipCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_ZipCode = "select_ContactDetails_by_keys_view_20_13_14";

//U_N_P
//20_13_15
//WhiteLabelDocId_Province_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_CountryCode = "select_ContactDetails_by_keys_view_20_13_15";

//U_N_Q
//20_13_16
//WhiteLabelDocId_Province_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_Email = "select_ContactDetails_by_keys_view_20_13_16";

//U_N_R
//20_13_17
//WhiteLabelDocId_Province_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_MainPhone = "select_ContactDetails_by_keys_view_20_13_17";

//U_N_S
//20_13_18
//WhiteLabelDocId_Province_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_MobliePhone = "select_ContactDetails_by_keys_view_20_13_18";

//U_N_T
//20_13_19
//WhiteLabelDocId_Province_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_OrdersDocId = "select_ContactDetails_by_keys_view_20_13_19";

//U_N_V
//20_13_21
//WhiteLabelDocId_Province_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_Title = "select_ContactDetails_by_keys_view_20_13_21";

//U_O_P
//20_14_15
//WhiteLabelDocId_ZipCode_CountryCode
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_CountryCode = "select_ContactDetails_by_keys_view_20_14_15";

//U_O_Q
//20_14_16
//WhiteLabelDocId_ZipCode_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_Email = "select_ContactDetails_by_keys_view_20_14_16";

//U_O_R
//20_14_17
//WhiteLabelDocId_ZipCode_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_MainPhone = "select_ContactDetails_by_keys_view_20_14_17";

//U_O_S
//20_14_18
//WhiteLabelDocId_ZipCode_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_MobliePhone = "select_ContactDetails_by_keys_view_20_14_18";

//U_O_T
//20_14_19
//WhiteLabelDocId_ZipCode_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_OrdersDocId = "select_ContactDetails_by_keys_view_20_14_19";

//U_O_V
//20_14_21
//WhiteLabelDocId_ZipCode_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_Title = "select_ContactDetails_by_keys_view_20_14_21";

//U_P_Q
//20_15_16
//WhiteLabelDocId_CountryCode_Email
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_Email = "select_ContactDetails_by_keys_view_20_15_16";

//U_P_R
//20_15_17
//WhiteLabelDocId_CountryCode_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_MainPhone = "select_ContactDetails_by_keys_view_20_15_17";

//U_P_S
//20_15_18
//WhiteLabelDocId_CountryCode_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_MobliePhone = "select_ContactDetails_by_keys_view_20_15_18";

//U_P_T
//20_15_19
//WhiteLabelDocId_CountryCode_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_OrdersDocId = "select_ContactDetails_by_keys_view_20_15_19";

//U_P_V
//20_15_21
//WhiteLabelDocId_CountryCode_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_Title = "select_ContactDetails_by_keys_view_20_15_21";

//U_Q_R
//20_16_17
//WhiteLabelDocId_Email_MainPhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_MainPhone = "select_ContactDetails_by_keys_view_20_16_17";

//U_Q_S
//20_16_18
//WhiteLabelDocId_Email_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_MobliePhone = "select_ContactDetails_by_keys_view_20_16_18";

//U_Q_T
//20_16_19
//WhiteLabelDocId_Email_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_OrdersDocId = "select_ContactDetails_by_keys_view_20_16_19";

//U_Q_V
//20_16_21
//WhiteLabelDocId_Email_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_Title = "select_ContactDetails_by_keys_view_20_16_21";

//U_R_S
//20_17_18
//WhiteLabelDocId_MainPhone_MobliePhone
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_MobliePhone = "select_ContactDetails_by_keys_view_20_17_18";

//U_R_T
//20_17_19
//WhiteLabelDocId_MainPhone_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_OrdersDocId = "select_ContactDetails_by_keys_view_20_17_19";

//U_R_V
//20_17_21
//WhiteLabelDocId_MainPhone_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_Title = "select_ContactDetails_by_keys_view_20_17_21";

//U_S_T
//20_18_19
//WhiteLabelDocId_MobliePhone_OrdersDocId
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone_OrdersDocId = "select_ContactDetails_by_keys_view_20_18_19";

//U_S_V
//20_18_21
//WhiteLabelDocId_MobliePhone_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone_Title = "select_ContactDetails_by_keys_view_20_18_21";

//U_T_V
//20_19_21
//WhiteLabelDocId_OrdersDocId_Title
public static readonly string  PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_OrdersDocId_Title = "select_ContactDetails_by_keys_view_20_19_21";
         #endregion

    }

}
