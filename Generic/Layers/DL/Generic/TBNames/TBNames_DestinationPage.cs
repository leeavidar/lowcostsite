

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_DestinationPage
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertDestinationPage = PROC_Prefix + "save_destination_page";
        #endregion
        #region Select
        public static readonly string PROC_Select_DestinationPage_By_DocId = PROC_Prefix + "Select_destination_page_By_doc_id";        
        public static readonly string PROC_Select_DestinationPage_By_Keys_View = PROC_Prefix + "Select_destination_page_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteDestinationPage = PROC_Prefix + "delete_destination_page";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageIsActive);

        public static readonly string PRM_Order = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageOrder);

        public static readonly string PRM_DestinationIataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageDestinationIataCode);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_SeoDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeoDocId);

        public static readonly string PRM_Price = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPagePrice);

        public static readonly string PRM_LocalCurrency = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageLocalCurrency);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageName);

        public static readonly string PRM_Time = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageTime);

        public static readonly string PRM_Title = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageTitle);

        public static readonly string PRM_PriceCurrency = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPagePriceCurrency);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "DestinationPage.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageIsActive);

        public static readonly string Field_Order = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageOrder);

        public static readonly string Field_DestinationIataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageDestinationIataCode);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_SeoDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeoDocId);

        public static readonly string Field_Price = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPagePrice);

        public static readonly string Field_LocalCurrency = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageLocalCurrency);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageName);

        public static readonly string Field_Time = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageTime);

        public static readonly string Field_Title = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageTitle);

        public static readonly string Field_PriceCurrency = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPagePriceCurrency);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//G_A
//6_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated = "select_DestinationPage_by_keys_view_6_0";

//G_B
//6_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId = "select_DestinationPage_by_keys_view_6_1";

//G_C
//6_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_DestinationPage_by_keys_view_6_2";

//G_E
//6_4
//WhiteLabelDocId_Order
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order = "select_DestinationPage_by_keys_view_6_4";

//G_F
//6_5
//WhiteLabelDocId_DestinationIataCode
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode = "select_DestinationPage_by_keys_view_6_5";

//G_H
//6_7
//WhiteLabelDocId_SeoDocId
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId = "select_DestinationPage_by_keys_view_6_7";

//G_I
//6_8
//WhiteLabelDocId_Price
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price = "select_DestinationPage_by_keys_view_6_8";

//G_J
//6_9
//WhiteLabelDocId_LocalCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency = "select_DestinationPage_by_keys_view_6_9";

//G_K
//6_10
//WhiteLabelDocId_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name = "select_DestinationPage_by_keys_view_6_10";

//G_L
//6_11
//WhiteLabelDocId_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time = "select_DestinationPage_by_keys_view_6_11";

//G_M
//6_12
//WhiteLabelDocId_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Title = "select_DestinationPage_by_keys_view_6_12";

//G_N
//6_13
//WhiteLabelDocId_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_PriceCurrency = "select_DestinationPage_by_keys_view_6_13";

//G_A_B
//6_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_DestinationPage_by_keys_view_6_0_1";

//G_A_C
//6_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_DestinationPage_by_keys_view_6_0_2";

//G_A_E
//6_0_4
//WhiteLabelDocId_DateCreated_Order
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Order = "select_DestinationPage_by_keys_view_6_0_4";

//G_A_F
//6_0_5
//WhiteLabelDocId_DateCreated_DestinationIataCode
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationIataCode = "select_DestinationPage_by_keys_view_6_0_5";

//G_A_H
//6_0_7
//WhiteLabelDocId_DateCreated_SeoDocId
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId = "select_DestinationPage_by_keys_view_6_0_7";

//G_A_I
//6_0_8
//WhiteLabelDocId_DateCreated_Price
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Price = "select_DestinationPage_by_keys_view_6_0_8";

//G_A_J
//6_0_9
//WhiteLabelDocId_DateCreated_LocalCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_LocalCurrency = "select_DestinationPage_by_keys_view_6_0_9";

//G_A_K
//6_0_10
//WhiteLabelDocId_DateCreated_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Name = "select_DestinationPage_by_keys_view_6_0_10";

//G_A_L
//6_0_11
//WhiteLabelDocId_DateCreated_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Time = "select_DestinationPage_by_keys_view_6_0_11";

//G_A_M
//6_0_12
//WhiteLabelDocId_DateCreated_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Title = "select_DestinationPage_by_keys_view_6_0_12";

//G_A_N
//6_0_13
//WhiteLabelDocId_DateCreated_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_PriceCurrency = "select_DestinationPage_by_keys_view_6_0_13";

//G_B_C
//6_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_DestinationPage_by_keys_view_6_1_2";

//G_B_E
//6_1_4
//WhiteLabelDocId_DocId_Order
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Order = "select_DestinationPage_by_keys_view_6_1_4";

//G_B_F
//6_1_5
//WhiteLabelDocId_DocId_DestinationIataCode
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_DestinationIataCode = "select_DestinationPage_by_keys_view_6_1_5";

//G_B_H
//6_1_7
//WhiteLabelDocId_DocId_SeoDocId
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId = "select_DestinationPage_by_keys_view_6_1_7";

//G_B_I
//6_1_8
//WhiteLabelDocId_DocId_Price
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Price = "select_DestinationPage_by_keys_view_6_1_8";

//G_B_J
//6_1_9
//WhiteLabelDocId_DocId_LocalCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_LocalCurrency = "select_DestinationPage_by_keys_view_6_1_9";

//G_B_K
//6_1_10
//WhiteLabelDocId_DocId_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Name = "select_DestinationPage_by_keys_view_6_1_10";

//G_B_L
//6_1_11
//WhiteLabelDocId_DocId_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Time = "select_DestinationPage_by_keys_view_6_1_11";

//G_B_M
//6_1_12
//WhiteLabelDocId_DocId_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Title = "select_DestinationPage_by_keys_view_6_1_12";

//G_B_N
//6_1_13
//WhiteLabelDocId_DocId_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_PriceCurrency = "select_DestinationPage_by_keys_view_6_1_13";

//G_C_E
//6_2_4
//WhiteLabelDocId_IsDeleted_Order
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Order = "select_DestinationPage_by_keys_view_6_2_4";

//G_C_F
//6_2_5
//WhiteLabelDocId_IsDeleted_DestinationIataCode
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationIataCode = "select_DestinationPage_by_keys_view_6_2_5";

//G_C_H
//6_2_7
//WhiteLabelDocId_IsDeleted_SeoDocId
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId = "select_DestinationPage_by_keys_view_6_2_7";

//G_C_I
//6_2_8
//WhiteLabelDocId_IsDeleted_Price
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Price = "select_DestinationPage_by_keys_view_6_2_8";

//G_C_J
//6_2_9
//WhiteLabelDocId_IsDeleted_LocalCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_LocalCurrency = "select_DestinationPage_by_keys_view_6_2_9";

//G_C_K
//6_2_10
//WhiteLabelDocId_IsDeleted_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Name = "select_DestinationPage_by_keys_view_6_2_10";

//G_C_L
//6_2_11
//WhiteLabelDocId_IsDeleted_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Time = "select_DestinationPage_by_keys_view_6_2_11";

//G_C_M
//6_2_12
//WhiteLabelDocId_IsDeleted_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Title = "select_DestinationPage_by_keys_view_6_2_12";

//G_C_N
//6_2_13
//WhiteLabelDocId_IsDeleted_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceCurrency = "select_DestinationPage_by_keys_view_6_2_13";

//G_E_F
//6_4_5
//WhiteLabelDocId_Order_DestinationIataCode
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_DestinationIataCode = "select_DestinationPage_by_keys_view_6_4_5";

//G_E_H
//6_4_7
//WhiteLabelDocId_Order_SeoDocId
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_SeoDocId = "select_DestinationPage_by_keys_view_6_4_7";

//G_E_I
//6_4_8
//WhiteLabelDocId_Order_Price
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Price = "select_DestinationPage_by_keys_view_6_4_8";

//G_E_J
//6_4_9
//WhiteLabelDocId_Order_LocalCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_LocalCurrency = "select_DestinationPage_by_keys_view_6_4_9";

//G_E_K
//6_4_10
//WhiteLabelDocId_Order_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Name = "select_DestinationPage_by_keys_view_6_4_10";

//G_E_L
//6_4_11
//WhiteLabelDocId_Order_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Time = "select_DestinationPage_by_keys_view_6_4_11";

//G_E_M
//6_4_12
//WhiteLabelDocId_Order_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Title = "select_DestinationPage_by_keys_view_6_4_12";

//G_E_N
//6_4_13
//WhiteLabelDocId_Order_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_PriceCurrency = "select_DestinationPage_by_keys_view_6_4_13";

//G_F_H
//6_5_7
//WhiteLabelDocId_DestinationIataCode_SeoDocId
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_SeoDocId = "select_DestinationPage_by_keys_view_6_5_7";

//G_F_I
//6_5_8
//WhiteLabelDocId_DestinationIataCode_Price
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Price = "select_DestinationPage_by_keys_view_6_5_8";

//G_F_J
//6_5_9
//WhiteLabelDocId_DestinationIataCode_LocalCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_LocalCurrency = "select_DestinationPage_by_keys_view_6_5_9";

//G_F_K
//6_5_10
//WhiteLabelDocId_DestinationIataCode_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Name = "select_DestinationPage_by_keys_view_6_5_10";

//G_F_L
//6_5_11
//WhiteLabelDocId_DestinationIataCode_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Time = "select_DestinationPage_by_keys_view_6_5_11";

//G_F_M
//6_5_12
//WhiteLabelDocId_DestinationIataCode_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Title = "select_DestinationPage_by_keys_view_6_5_12";

//G_F_N
//6_5_13
//WhiteLabelDocId_DestinationIataCode_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_PriceCurrency = "select_DestinationPage_by_keys_view_6_5_13";

//G_H_I
//6_7_8
//WhiteLabelDocId_SeoDocId_Price
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Price = "select_DestinationPage_by_keys_view_6_7_8";

//G_H_J
//6_7_9
//WhiteLabelDocId_SeoDocId_LocalCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_LocalCurrency = "select_DestinationPage_by_keys_view_6_7_9";

//G_H_K
//6_7_10
//WhiteLabelDocId_SeoDocId_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Name = "select_DestinationPage_by_keys_view_6_7_10";

//G_H_L
//6_7_11
//WhiteLabelDocId_SeoDocId_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Time = "select_DestinationPage_by_keys_view_6_7_11";

//G_H_M
//6_7_12
//WhiteLabelDocId_SeoDocId_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Title = "select_DestinationPage_by_keys_view_6_7_12";

//G_H_N
//6_7_13
//WhiteLabelDocId_SeoDocId_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_PriceCurrency = "select_DestinationPage_by_keys_view_6_7_13";

//G_I_J
//6_8_9
//WhiteLabelDocId_Price_LocalCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_LocalCurrency = "select_DestinationPage_by_keys_view_6_8_9";

//G_I_K
//6_8_10
//WhiteLabelDocId_Price_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Name = "select_DestinationPage_by_keys_view_6_8_10";

//G_I_L
//6_8_11
//WhiteLabelDocId_Price_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Time = "select_DestinationPage_by_keys_view_6_8_11";

//G_I_M
//6_8_12
//WhiteLabelDocId_Price_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Title = "select_DestinationPage_by_keys_view_6_8_12";

//G_I_N
//6_8_13
//WhiteLabelDocId_Price_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_PriceCurrency = "select_DestinationPage_by_keys_view_6_8_13";

//G_J_K
//6_9_10
//WhiteLabelDocId_LocalCurrency_Name
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Name = "select_DestinationPage_by_keys_view_6_9_10";

//G_J_L
//6_9_11
//WhiteLabelDocId_LocalCurrency_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Time = "select_DestinationPage_by_keys_view_6_9_11";

//G_J_M
//6_9_12
//WhiteLabelDocId_LocalCurrency_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Title = "select_DestinationPage_by_keys_view_6_9_12";

//G_J_N
//6_9_13
//WhiteLabelDocId_LocalCurrency_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_PriceCurrency = "select_DestinationPage_by_keys_view_6_9_13";

//G_K_L
//6_10_11
//WhiteLabelDocId_Name_Time
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_Time = "select_DestinationPage_by_keys_view_6_10_11";

//G_K_M
//6_10_12
//WhiteLabelDocId_Name_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_Title = "select_DestinationPage_by_keys_view_6_10_12";

//G_K_N
//6_10_13
//WhiteLabelDocId_Name_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_PriceCurrency = "select_DestinationPage_by_keys_view_6_10_13";

//G_L_M
//6_11_12
//WhiteLabelDocId_Time_Title
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time_Title = "select_DestinationPage_by_keys_view_6_11_12";

//G_L_N
//6_11_13
//WhiteLabelDocId_Time_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time_PriceCurrency = "select_DestinationPage_by_keys_view_6_11_13";

//G_M_N
//6_12_13
//WhiteLabelDocId_Title_PriceCurrency
public static readonly string  PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Title_PriceCurrency = "select_DestinationPage_by_keys_view_6_12_13";
         #endregion

    }

}
