

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Country
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertCountry = PROC_Prefix + "save_country";
        #endregion
        #region Select
        public static readonly string PROC_Select_Country_By_DocId = PROC_Prefix + "Select_country_By_doc_id";        
        public static readonly string PROC_Select_Country_By_Keys_View = PROC_Prefix + "Select_country_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteCountry = PROC_Prefix + "delete_country";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryIsActive);

        public static readonly string PRM_CountryCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryCountryCode);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryName);

        public static readonly string PRM_EnglishName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryEnglishName);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Country.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryIsActive);

        public static readonly string Field_CountryCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryCountryCode);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryName);

        public static readonly string Field_EnglishName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCountryEnglishName);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Country_By_Keys_View_DateCreated = "select_Country_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Country_By_Keys_View_DocId = "select_Country_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Country_By_Keys_View_IsDeleted = "select_Country_by_keys_view_2";

//E
//4
//CountryCode
public static readonly string  PROC_Select_Country_By_Keys_View_CountryCode = "select_Country_by_keys_view_4";

//F
//5
//Name
public static readonly string  PROC_Select_Country_By_Keys_View_Name = "select_Country_by_keys_view_5";

//G
//6
//EnglishName
public static readonly string  PROC_Select_Country_By_Keys_View_EnglishName = "select_Country_by_keys_view_6";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Country_By_Keys_View_DateCreated_DocId = "select_Country_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Country_By_Keys_View_DateCreated_IsDeleted = "select_Country_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_CountryCode
public static readonly string  PROC_Select_Country_By_Keys_View_DateCreated_CountryCode = "select_Country_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_Name
public static readonly string  PROC_Select_Country_By_Keys_View_DateCreated_Name = "select_Country_by_keys_view_0_5";

//A_G
//0_6
//DateCreated_EnglishName
public static readonly string  PROC_Select_Country_By_Keys_View_DateCreated_EnglishName = "select_Country_by_keys_view_0_6";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Country_By_Keys_View_DocId_IsDeleted = "select_Country_by_keys_view_1_2";

//B_E
//1_4
//DocId_CountryCode
public static readonly string  PROC_Select_Country_By_Keys_View_DocId_CountryCode = "select_Country_by_keys_view_1_4";

//B_F
//1_5
//DocId_Name
public static readonly string  PROC_Select_Country_By_Keys_View_DocId_Name = "select_Country_by_keys_view_1_5";

//B_G
//1_6
//DocId_EnglishName
public static readonly string  PROC_Select_Country_By_Keys_View_DocId_EnglishName = "select_Country_by_keys_view_1_6";

//C_E
//2_4
//IsDeleted_CountryCode
public static readonly string  PROC_Select_Country_By_Keys_View_IsDeleted_CountryCode = "select_Country_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_Name
public static readonly string  PROC_Select_Country_By_Keys_View_IsDeleted_Name = "select_Country_by_keys_view_2_5";

//C_G
//2_6
//IsDeleted_EnglishName
public static readonly string  PROC_Select_Country_By_Keys_View_IsDeleted_EnglishName = "select_Country_by_keys_view_2_6";

//E_F
//4_5
//CountryCode_Name
public static readonly string  PROC_Select_Country_By_Keys_View_CountryCode_Name = "select_Country_by_keys_view_4_5";

//E_G
//4_6
//CountryCode_EnglishName
public static readonly string  PROC_Select_Country_By_Keys_View_CountryCode_EnglishName = "select_Country_by_keys_view_4_6";

//F_G
//5_6
//Name_EnglishName
public static readonly string  PROC_Select_Country_By_Keys_View_Name_EnglishName = "select_Country_by_keys_view_5_6";
         #endregion

    }

}
