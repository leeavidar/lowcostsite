

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Airport
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertAirport = PROC_Prefix + "save_airport";
        #endregion
        #region Select
        public static readonly string PROC_Select_Airport_By_DocId = PROC_Prefix + "Select_airport_By_doc_id";        
        public static readonly string PROC_Select_Airport_By_Keys_View = PROC_Prefix + "Select_airport_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteAirport = PROC_Prefix + "delete_airport";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportIsActive);

        public static readonly string PRM_IataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportIataCode);

        public static readonly string PRM_CityIataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportCityIataCode);

        public static readonly string PRM_EnglishName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportEnglishName);

        public static readonly string PRM_NameByLang = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportNameByLang);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Airport.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportIsActive);

        public static readonly string Field_IataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportIataCode);

        public static readonly string Field_CityIataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportCityIataCode);

        public static readonly string Field_EnglishName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportEnglishName);

        public static readonly string Field_NameByLang = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportNameByLang);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Airport_By_Keys_View_DateCreated = "select_Airport_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Airport_By_Keys_View_DocId = "select_Airport_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Airport_By_Keys_View_IsDeleted = "select_Airport_by_keys_view_2";

//E
//4
//IataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_IataCode = "select_Airport_by_keys_view_4";

//F
//5
//CityIataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_CityIataCode = "select_Airport_by_keys_view_5";

//G
//6
//EnglishName
public static readonly string  PROC_Select_Airport_By_Keys_View_EnglishName = "select_Airport_by_keys_view_6";

//H
//7
//NameByLang
public static readonly string  PROC_Select_Airport_By_Keys_View_NameByLang = "select_Airport_by_keys_view_7";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Airport_By_Keys_View_DateCreated_DocId = "select_Airport_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Airport_By_Keys_View_DateCreated_IsDeleted = "select_Airport_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_IataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_DateCreated_IataCode = "select_Airport_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_CityIataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_DateCreated_CityIataCode = "select_Airport_by_keys_view_0_5";

//A_G
//0_6
//DateCreated_EnglishName
public static readonly string  PROC_Select_Airport_By_Keys_View_DateCreated_EnglishName = "select_Airport_by_keys_view_0_6";

//A_H
//0_7
//DateCreated_NameByLang
public static readonly string  PROC_Select_Airport_By_Keys_View_DateCreated_NameByLang = "select_Airport_by_keys_view_0_7";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Airport_By_Keys_View_DocId_IsDeleted = "select_Airport_by_keys_view_1_2";

//B_E
//1_4
//DocId_IataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_DocId_IataCode = "select_Airport_by_keys_view_1_4";

//B_F
//1_5
//DocId_CityIataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_DocId_CityIataCode = "select_Airport_by_keys_view_1_5";

//B_G
//1_6
//DocId_EnglishName
public static readonly string  PROC_Select_Airport_By_Keys_View_DocId_EnglishName = "select_Airport_by_keys_view_1_6";

//B_H
//1_7
//DocId_NameByLang
public static readonly string  PROC_Select_Airport_By_Keys_View_DocId_NameByLang = "select_Airport_by_keys_view_1_7";

//C_E
//2_4
//IsDeleted_IataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_IsDeleted_IataCode = "select_Airport_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_CityIataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_IsDeleted_CityIataCode = "select_Airport_by_keys_view_2_5";

//C_G
//2_6
//IsDeleted_EnglishName
public static readonly string  PROC_Select_Airport_By_Keys_View_IsDeleted_EnglishName = "select_Airport_by_keys_view_2_6";

//C_H
//2_7
//IsDeleted_NameByLang
public static readonly string  PROC_Select_Airport_By_Keys_View_IsDeleted_NameByLang = "select_Airport_by_keys_view_2_7";

//E_F
//4_5
//IataCode_CityIataCode
public static readonly string  PROC_Select_Airport_By_Keys_View_IataCode_CityIataCode = "select_Airport_by_keys_view_4_5";

//E_G
//4_6
//IataCode_EnglishName
public static readonly string  PROC_Select_Airport_By_Keys_View_IataCode_EnglishName = "select_Airport_by_keys_view_4_6";

//E_H
//4_7
//IataCode_NameByLang
public static readonly string  PROC_Select_Airport_By_Keys_View_IataCode_NameByLang = "select_Airport_by_keys_view_4_7";

//F_G
//5_6
//CityIataCode_EnglishName
public static readonly string  PROC_Select_Airport_By_Keys_View_CityIataCode_EnglishName = "select_Airport_by_keys_view_5_6";

//F_H
//5_7
//CityIataCode_NameByLang
public static readonly string  PROC_Select_Airport_By_Keys_View_CityIataCode_NameByLang = "select_Airport_by_keys_view_5_7";

//G_H
//6_7
//EnglishName_NameByLang
public static readonly string  PROC_Select_Airport_By_Keys_View_EnglishName_NameByLang = "select_Airport_by_keys_view_6_7";
         #endregion

    }

}
