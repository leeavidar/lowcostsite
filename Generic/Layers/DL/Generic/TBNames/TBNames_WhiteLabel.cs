

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_WhiteLabel
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertWhiteLabel = PROC_Prefix + "save_white_label";
        #endregion
        #region Select
        public static readonly string PROC_Select_WhiteLabel_By_DocId = PROC_Prefix + "Select_white_label_By_doc_id";        
        public static readonly string PROC_Select_WhiteLabel_By_Keys_View = PROC_Prefix + "Select_white_label_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteWhiteLabel = PROC_Prefix + "delete_white_label";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelIsActive);

        public static readonly string PRM_Domain = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDomain);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelName);

        public static readonly string PRM_LanguagesDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelLanguagesDocId);

        public static readonly string PRM_Phone = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelPhone);

        public static readonly string PRM_Email = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelEmail);

        public static readonly string PRM_DiscountType = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDiscountType);

        public static readonly string PRM_Address = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelAddress);

        public static readonly string PRM_Discount = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDiscount);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "WhiteLabel.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelIsActive);

        public static readonly string Field_Domain = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDomain);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelName);

        public static readonly string Field_LanguagesDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelLanguagesDocId);

        public static readonly string Field_Phone = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelPhone);

        public static readonly string Field_Email = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelEmail);

        public static readonly string Field_DiscountType = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDiscountType);

        public static readonly string Field_Address = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelAddress);

        public static readonly string Field_Discount = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDiscount);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated = "select_WhiteLabel_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId = "select_WhiteLabel_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted = "select_WhiteLabel_by_keys_view_2";

//E
//4
//Domain
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Domain = "select_WhiteLabel_by_keys_view_4";

//F
//5
//Name
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Name = "select_WhiteLabel_by_keys_view_5";

//G
//6
//LanguagesDocId
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId = "select_WhiteLabel_by_keys_view_6";

//H
//7
//Phone
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Phone = "select_WhiteLabel_by_keys_view_7";

//I
//8
//Email
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Email = "select_WhiteLabel_by_keys_view_8";

//J
//9
//DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DiscountType = "select_WhiteLabel_by_keys_view_9";

//K
//10
//Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Address = "select_WhiteLabel_by_keys_view_10";

//L
//11
//Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Discount = "select_WhiteLabel_by_keys_view_11";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_DocId = "select_WhiteLabel_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_IsDeleted = "select_WhiteLabel_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_Domain
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Domain = "select_WhiteLabel_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_Name
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Name = "select_WhiteLabel_by_keys_view_0_5";

//A_G
//0_6
//DateCreated_LanguagesDocId
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_LanguagesDocId = "select_WhiteLabel_by_keys_view_0_6";

//A_H
//0_7
//DateCreated_Phone
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Phone = "select_WhiteLabel_by_keys_view_0_7";

//A_I
//0_8
//DateCreated_Email
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Email = "select_WhiteLabel_by_keys_view_0_8";

//A_J
//0_9
//DateCreated_DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_DiscountType = "select_WhiteLabel_by_keys_view_0_9";

//A_K
//0_10
//DateCreated_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Address = "select_WhiteLabel_by_keys_view_0_10";

//A_L
//0_11
//DateCreated_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Discount = "select_WhiteLabel_by_keys_view_0_11";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_IsDeleted = "select_WhiteLabel_by_keys_view_1_2";

//B_E
//1_4
//DocId_Domain
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_Domain = "select_WhiteLabel_by_keys_view_1_4";

//B_F
//1_5
//DocId_Name
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_Name = "select_WhiteLabel_by_keys_view_1_5";

//B_G
//1_6
//DocId_LanguagesDocId
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_LanguagesDocId = "select_WhiteLabel_by_keys_view_1_6";

//B_H
//1_7
//DocId_Phone
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_Phone = "select_WhiteLabel_by_keys_view_1_7";

//B_I
//1_8
//DocId_Email
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_Email = "select_WhiteLabel_by_keys_view_1_8";

//B_J
//1_9
//DocId_DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_DiscountType = "select_WhiteLabel_by_keys_view_1_9";

//B_K
//1_10
//DocId_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_Address = "select_WhiteLabel_by_keys_view_1_10";

//B_L
//1_11
//DocId_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DocId_Discount = "select_WhiteLabel_by_keys_view_1_11";

//C_E
//2_4
//IsDeleted_Domain
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Domain = "select_WhiteLabel_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_Name
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Name = "select_WhiteLabel_by_keys_view_2_5";

//C_G
//2_6
//IsDeleted_LanguagesDocId
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_LanguagesDocId = "select_WhiteLabel_by_keys_view_2_6";

//C_H
//2_7
//IsDeleted_Phone
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Phone = "select_WhiteLabel_by_keys_view_2_7";

//C_I
//2_8
//IsDeleted_Email
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Email = "select_WhiteLabel_by_keys_view_2_8";

//C_J
//2_9
//IsDeleted_DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_DiscountType = "select_WhiteLabel_by_keys_view_2_9";

//C_K
//2_10
//IsDeleted_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Address = "select_WhiteLabel_by_keys_view_2_10";

//C_L
//2_11
//IsDeleted_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Discount = "select_WhiteLabel_by_keys_view_2_11";

//E_F
//4_5
//Domain_Name
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Domain_Name = "select_WhiteLabel_by_keys_view_4_5";

//E_G
//4_6
//Domain_LanguagesDocId
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Domain_LanguagesDocId = "select_WhiteLabel_by_keys_view_4_6";

//E_H
//4_7
//Domain_Phone
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Domain_Phone = "select_WhiteLabel_by_keys_view_4_7";

//E_I
//4_8
//Domain_Email
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Domain_Email = "select_WhiteLabel_by_keys_view_4_8";

//E_J
//4_9
//Domain_DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Domain_DiscountType = "select_WhiteLabel_by_keys_view_4_9";

//E_K
//4_10
//Domain_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Domain_Address = "select_WhiteLabel_by_keys_view_4_10";

//E_L
//4_11
//Domain_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Domain_Discount = "select_WhiteLabel_by_keys_view_4_11";

//F_G
//5_6
//Name_LanguagesDocId
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Name_LanguagesDocId = "select_WhiteLabel_by_keys_view_5_6";

//F_H
//5_7
//Name_Phone
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Name_Phone = "select_WhiteLabel_by_keys_view_5_7";

//F_I
//5_8
//Name_Email
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Name_Email = "select_WhiteLabel_by_keys_view_5_8";

//F_J
//5_9
//Name_DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Name_DiscountType = "select_WhiteLabel_by_keys_view_5_9";

//F_K
//5_10
//Name_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Name_Address = "select_WhiteLabel_by_keys_view_5_10";

//F_L
//5_11
//Name_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Name_Discount = "select_WhiteLabel_by_keys_view_5_11";

//G_H
//6_7
//LanguagesDocId_Phone
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_Phone = "select_WhiteLabel_by_keys_view_6_7";

//G_I
//6_8
//LanguagesDocId_Email
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_Email = "select_WhiteLabel_by_keys_view_6_8";

//G_J
//6_9
//LanguagesDocId_DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_DiscountType = "select_WhiteLabel_by_keys_view_6_9";

//G_K
//6_10
//LanguagesDocId_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_Address = "select_WhiteLabel_by_keys_view_6_10";

//G_L
//6_11
//LanguagesDocId_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_Discount = "select_WhiteLabel_by_keys_view_6_11";

//H_I
//7_8
//Phone_Email
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Phone_Email = "select_WhiteLabel_by_keys_view_7_8";

//H_J
//7_9
//Phone_DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Phone_DiscountType = "select_WhiteLabel_by_keys_view_7_9";

//H_K
//7_10
//Phone_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Phone_Address = "select_WhiteLabel_by_keys_view_7_10";

//H_L
//7_11
//Phone_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Phone_Discount = "select_WhiteLabel_by_keys_view_7_11";

//I_J
//8_9
//Email_DiscountType
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Email_DiscountType = "select_WhiteLabel_by_keys_view_8_9";

//I_K
//8_10
//Email_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Email_Address = "select_WhiteLabel_by_keys_view_8_10";

//I_L
//8_11
//Email_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Email_Discount = "select_WhiteLabel_by_keys_view_8_11";

//J_K
//9_10
//DiscountType_Address
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DiscountType_Address = "select_WhiteLabel_by_keys_view_9_10";

//J_L
//9_11
//DiscountType_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_DiscountType_Discount = "select_WhiteLabel_by_keys_view_9_11";

//K_L
//10_11
//Address_Discount
public static readonly string  PROC_Select_WhiteLabel_By_Keys_View_Address_Discount = "select_WhiteLabel_by_keys_view_10_11";
         #endregion

    }

}
