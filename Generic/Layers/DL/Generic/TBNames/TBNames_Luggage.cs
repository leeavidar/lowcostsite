

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Luggage
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertLuggage = PROC_Prefix + "save_luggage";
        #endregion
        #region Select
        public static readonly string PROC_Select_Luggage_By_DocId = PROC_Prefix + "Select_luggage_By_doc_id";        
        public static readonly string PROC_Select_Luggage_By_Keys_View = PROC_Prefix + "Select_luggage_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteLuggage = PROC_Prefix + "delete_luggage";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageIsActive);

        public static readonly string PRM_Price = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggagePrice);

        public static readonly string PRM_Quantity = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageQuantity);

        public static readonly string PRM_TotalWeight = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageTotalWeight);

        public static readonly string PRM_FlightType = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageFlightType);

        public static readonly string PRM_PassengerDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Luggage.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageIsActive);

        public static readonly string Field_Price = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggagePrice);

        public static readonly string Field_Quantity = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageQuantity);

        public static readonly string Field_TotalWeight = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageTotalWeight);

        public static readonly string Field_FlightType = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyLuggageFlightType);

        public static readonly string Field_PassengerDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//J_A
//9_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated = "select_Luggage_by_keys_view_9_0";

//J_B
//9_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId = "select_Luggage_by_keys_view_9_1";

//J_C
//9_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_Luggage_by_keys_view_9_2";

//J_E
//9_4
//WhiteLabelDocId_Price
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price = "select_Luggage_by_keys_view_9_4";

//J_F
//9_5
//WhiteLabelDocId_Quantity
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity = "select_Luggage_by_keys_view_9_5";

//J_G
//9_6
//WhiteLabelDocId_TotalWeight
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight = "select_Luggage_by_keys_view_9_6";

//J_H
//9_7
//WhiteLabelDocId_FlightType
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_FlightType = "select_Luggage_by_keys_view_9_7";

//J_I
//9_8
//WhiteLabelDocId_PassengerDocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_PassengerDocId = "select_Luggage_by_keys_view_9_8";

//J_A_B
//9_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_Luggage_by_keys_view_9_0_1";

//J_A_C
//9_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_Luggage_by_keys_view_9_0_2";

//J_A_E
//9_0_4
//WhiteLabelDocId_DateCreated_Price
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_Price = "select_Luggage_by_keys_view_9_0_4";

//J_A_F
//9_0_5
//WhiteLabelDocId_DateCreated_Quantity
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_Quantity = "select_Luggage_by_keys_view_9_0_5";

//J_A_G
//9_0_6
//WhiteLabelDocId_DateCreated_TotalWeight
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_TotalWeight = "select_Luggage_by_keys_view_9_0_6";

//J_A_H
//9_0_7
//WhiteLabelDocId_DateCreated_FlightType
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_FlightType = "select_Luggage_by_keys_view_9_0_7";

//J_A_I
//9_0_8
//WhiteLabelDocId_DateCreated_PassengerDocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerDocId = "select_Luggage_by_keys_view_9_0_8";

//J_B_C
//9_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_Luggage_by_keys_view_9_1_2";

//J_B_E
//9_1_4
//WhiteLabelDocId_DocId_Price
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_Price = "select_Luggage_by_keys_view_9_1_4";

//J_B_F
//9_1_5
//WhiteLabelDocId_DocId_Quantity
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_Quantity = "select_Luggage_by_keys_view_9_1_5";

//J_B_G
//9_1_6
//WhiteLabelDocId_DocId_TotalWeight
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_TotalWeight = "select_Luggage_by_keys_view_9_1_6";

//J_B_H
//9_1_7
//WhiteLabelDocId_DocId_FlightType
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_FlightType = "select_Luggage_by_keys_view_9_1_7";

//J_B_I
//9_1_8
//WhiteLabelDocId_DocId_PassengerDocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_PassengerDocId = "select_Luggage_by_keys_view_9_1_8";

//J_C_E
//9_2_4
//WhiteLabelDocId_IsDeleted_Price
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_Price = "select_Luggage_by_keys_view_9_2_4";

//J_C_F
//9_2_5
//WhiteLabelDocId_IsDeleted_Quantity
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_Quantity = "select_Luggage_by_keys_view_9_2_5";

//J_C_G
//9_2_6
//WhiteLabelDocId_IsDeleted_TotalWeight
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_TotalWeight = "select_Luggage_by_keys_view_9_2_6";

//J_C_H
//9_2_7
//WhiteLabelDocId_IsDeleted_FlightType
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightType = "select_Luggage_by_keys_view_9_2_7";

//J_C_I
//9_2_8
//WhiteLabelDocId_IsDeleted_PassengerDocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerDocId = "select_Luggage_by_keys_view_9_2_8";

//J_E_F
//9_4_5
//WhiteLabelDocId_Price_Quantity
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_Quantity = "select_Luggage_by_keys_view_9_4_5";

//J_E_G
//9_4_6
//WhiteLabelDocId_Price_TotalWeight
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_TotalWeight = "select_Luggage_by_keys_view_9_4_6";

//J_E_H
//9_4_7
//WhiteLabelDocId_Price_FlightType
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_FlightType = "select_Luggage_by_keys_view_9_4_7";

//J_E_I
//9_4_8
//WhiteLabelDocId_Price_PassengerDocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_PassengerDocId = "select_Luggage_by_keys_view_9_4_8";

//J_F_G
//9_5_6
//WhiteLabelDocId_Quantity_TotalWeight
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_TotalWeight = "select_Luggage_by_keys_view_9_5_6";

//J_F_H
//9_5_7
//WhiteLabelDocId_Quantity_FlightType
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_FlightType = "select_Luggage_by_keys_view_9_5_7";

//J_F_I
//9_5_8
//WhiteLabelDocId_Quantity_PassengerDocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_PassengerDocId = "select_Luggage_by_keys_view_9_5_8";

//J_G_H
//9_6_7
//WhiteLabelDocId_TotalWeight_FlightType
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight_FlightType = "select_Luggage_by_keys_view_9_6_7";

//J_G_I
//9_6_8
//WhiteLabelDocId_TotalWeight_PassengerDocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight_PassengerDocId = "select_Luggage_by_keys_view_9_6_8";

//J_H_I
//9_7_8
//WhiteLabelDocId_FlightType_PassengerDocId
public static readonly string  PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_FlightType_PassengerDocId = "select_Luggage_by_keys_view_9_7_8";
         #endregion

    }

}
