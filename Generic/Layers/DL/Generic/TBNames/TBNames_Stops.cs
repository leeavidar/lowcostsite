

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Stops
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertStops = PROC_Prefix + "save_stops";
        #endregion
        #region Select
        public static readonly string PROC_Select_Stops_By_DocId = PROC_Prefix + "Select_stops_By_doc_id";        
        public static readonly string PROC_Select_Stops_By_Keys_View = PROC_Prefix + "Select_stops_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteStops = PROC_Prefix + "delete_stops";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsIsActive);

        public static readonly string PRM_Arrival = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsArrival);

        public static readonly string PRM_Departure = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsDeparture);

        public static readonly string PRM_FlightLegDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDocId);

        public static readonly string PRM_AirportIataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportIataCode);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Stops.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsIsActive);

        public static readonly string Field_Arrival = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsArrival);

        public static readonly string Field_Departure = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStopsDeparture);

        public static readonly string Field_FlightLegDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDocId);

        public static readonly string Field_AirportIataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyAirportIataCode);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Stops_By_Keys_View_DateCreated = "select_Stops_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Stops_By_Keys_View_DocId = "select_Stops_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Stops_By_Keys_View_IsDeleted = "select_Stops_by_keys_view_2";

//E
//4
//Arrival
public static readonly string  PROC_Select_Stops_By_Keys_View_Arrival = "select_Stops_by_keys_view_4";

//F
//5
//Departure
public static readonly string  PROC_Select_Stops_By_Keys_View_Departure = "select_Stops_by_keys_view_5";

//G
//6
//FlightLegDocId
public static readonly string  PROC_Select_Stops_By_Keys_View_FlightLegDocId = "select_Stops_by_keys_view_6";

//H
//7
//AirportIataCode
public static readonly string  PROC_Select_Stops_By_Keys_View_AirportIataCode = "select_Stops_by_keys_view_7";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Stops_By_Keys_View_DateCreated_DocId = "select_Stops_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Stops_By_Keys_View_DateCreated_IsDeleted = "select_Stops_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_Arrival
public static readonly string  PROC_Select_Stops_By_Keys_View_DateCreated_Arrival = "select_Stops_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_Departure
public static readonly string  PROC_Select_Stops_By_Keys_View_DateCreated_Departure = "select_Stops_by_keys_view_0_5";

//A_G
//0_6
//DateCreated_FlightLegDocId
public static readonly string  PROC_Select_Stops_By_Keys_View_DateCreated_FlightLegDocId = "select_Stops_by_keys_view_0_6";

//A_H
//0_7
//DateCreated_AirportIataCode
public static readonly string  PROC_Select_Stops_By_Keys_View_DateCreated_AirportIataCode = "select_Stops_by_keys_view_0_7";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Stops_By_Keys_View_DocId_IsDeleted = "select_Stops_by_keys_view_1_2";

//B_E
//1_4
//DocId_Arrival
public static readonly string  PROC_Select_Stops_By_Keys_View_DocId_Arrival = "select_Stops_by_keys_view_1_4";

//B_F
//1_5
//DocId_Departure
public static readonly string  PROC_Select_Stops_By_Keys_View_DocId_Departure = "select_Stops_by_keys_view_1_5";

//B_G
//1_6
//DocId_FlightLegDocId
public static readonly string  PROC_Select_Stops_By_Keys_View_DocId_FlightLegDocId = "select_Stops_by_keys_view_1_6";

//B_H
//1_7
//DocId_AirportIataCode
public static readonly string  PROC_Select_Stops_By_Keys_View_DocId_AirportIataCode = "select_Stops_by_keys_view_1_7";

//C_E
//2_4
//IsDeleted_Arrival
public static readonly string  PROC_Select_Stops_By_Keys_View_IsDeleted_Arrival = "select_Stops_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_Departure
public static readonly string  PROC_Select_Stops_By_Keys_View_IsDeleted_Departure = "select_Stops_by_keys_view_2_5";

//C_G
//2_6
//IsDeleted_FlightLegDocId
public static readonly string  PROC_Select_Stops_By_Keys_View_IsDeleted_FlightLegDocId = "select_Stops_by_keys_view_2_6";

//C_H
//2_7
//IsDeleted_AirportIataCode
public static readonly string  PROC_Select_Stops_By_Keys_View_IsDeleted_AirportIataCode = "select_Stops_by_keys_view_2_7";

//E_F
//4_5
//Arrival_Departure
public static readonly string  PROC_Select_Stops_By_Keys_View_Arrival_Departure = "select_Stops_by_keys_view_4_5";

//E_G
//4_6
//Arrival_FlightLegDocId
public static readonly string  PROC_Select_Stops_By_Keys_View_Arrival_FlightLegDocId = "select_Stops_by_keys_view_4_6";

//E_H
//4_7
//Arrival_AirportIataCode
public static readonly string  PROC_Select_Stops_By_Keys_View_Arrival_AirportIataCode = "select_Stops_by_keys_view_4_7";

//F_G
//5_6
//Departure_FlightLegDocId
public static readonly string  PROC_Select_Stops_By_Keys_View_Departure_FlightLegDocId = "select_Stops_by_keys_view_5_6";

//F_H
//5_7
//Departure_AirportIataCode
public static readonly string  PROC_Select_Stops_By_Keys_View_Departure_AirportIataCode = "select_Stops_by_keys_view_5_7";

//G_H
//6_7
//FlightLegDocId_AirportIataCode
public static readonly string  PROC_Select_Stops_By_Keys_View_FlightLegDocId_AirportIataCode = "select_Stops_by_keys_view_6_7";
         #endregion

    }

}
