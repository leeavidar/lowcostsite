

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Template
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertTemplate = PROC_Prefix + "save_template";
        #endregion
        #region Select
        public static readonly string PROC_Select_Template_By_DocId = PROC_Prefix + "Select_template_By_doc_id";        
        public static readonly string PROC_Select_Template_By_Keys_View = PROC_Prefix + "Select_template_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteTemplate = PROC_Prefix + "delete_template";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateIsActive);

        public static readonly string PRM_Title = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateTitle);

        public static readonly string PRM_SubTitle = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateSubTitle);

        public static readonly string PRM_Type = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateType);

        public static readonly string PRM_Order = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateOrder);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_RelatedObjectType = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateRelatedObjectType);

        public static readonly string PRM_RelatedObjectDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateRelatedObjectDocId);

        public static readonly string PRM_TextLong = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateTextLong);

        public static readonly string PRM_TextShort = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateTextShort);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Template.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateIsActive);

        public static readonly string Field_Title = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateTitle);

        public static readonly string Field_SubTitle = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateSubTitle);

        public static readonly string Field_Type = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateType);

        public static readonly string Field_Order = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateOrder);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_RelatedObjectType = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateRelatedObjectType);

        public static readonly string Field_RelatedObjectDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateRelatedObjectDocId);

        public static readonly string Field_TextLong = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateTextLong);

        public static readonly string Field_TextShort = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateTextShort);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//I_A
//8_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated = "select_Template_by_keys_view_8_0";

//I_B
//8_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId = "select_Template_by_keys_view_8_1";

//I_C
//8_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_Template_by_keys_view_8_2";

//I_E
//8_4
//WhiteLabelDocId_Title
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title = "select_Template_by_keys_view_8_4";

//I_F
//8_5
//WhiteLabelDocId_SubTitle
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle = "select_Template_by_keys_view_8_5";

//I_G
//8_6
//WhiteLabelDocId_Type
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type = "select_Template_by_keys_view_8_6";

//I_H
//8_7
//WhiteLabelDocId_Order
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order = "select_Template_by_keys_view_8_7";

//I_J
//8_9
//WhiteLabelDocId_RelatedObjectType
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType = "select_Template_by_keys_view_8_9";

//I_K
//8_10
//WhiteLabelDocId_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId = "select_Template_by_keys_view_8_10";

//I_L
//8_11
//WhiteLabelDocId_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_TextLong = "select_Template_by_keys_view_8_11";

//I_M
//8_12
//WhiteLabelDocId_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_TextShort = "select_Template_by_keys_view_8_12";

//I_A_B
//8_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_Template_by_keys_view_8_0_1";

//I_A_C
//8_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_Template_by_keys_view_8_0_2";

//I_A_E
//8_0_4
//WhiteLabelDocId_DateCreated_Title
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Title = "select_Template_by_keys_view_8_0_4";

//I_A_F
//8_0_5
//WhiteLabelDocId_DateCreated_SubTitle
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_SubTitle = "select_Template_by_keys_view_8_0_5";

//I_A_G
//8_0_6
//WhiteLabelDocId_DateCreated_Type
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Type = "select_Template_by_keys_view_8_0_6";

//I_A_H
//8_0_7
//WhiteLabelDocId_DateCreated_Order
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Order = "select_Template_by_keys_view_8_0_7";

//I_A_J
//8_0_9
//WhiteLabelDocId_DateCreated_RelatedObjectType
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_RelatedObjectType = "select_Template_by_keys_view_8_0_9";

//I_A_K
//8_0_10
//WhiteLabelDocId_DateCreated_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_RelatedObjectDocId = "select_Template_by_keys_view_8_0_10";

//I_A_L
//8_0_11
//WhiteLabelDocId_DateCreated_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_TextLong = "select_Template_by_keys_view_8_0_11";

//I_A_M
//8_0_12
//WhiteLabelDocId_DateCreated_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_TextShort = "select_Template_by_keys_view_8_0_12";

//I_B_C
//8_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_Template_by_keys_view_8_1_2";

//I_B_E
//8_1_4
//WhiteLabelDocId_DocId_Title
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Title = "select_Template_by_keys_view_8_1_4";

//I_B_F
//8_1_5
//WhiteLabelDocId_DocId_SubTitle
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_SubTitle = "select_Template_by_keys_view_8_1_5";

//I_B_G
//8_1_6
//WhiteLabelDocId_DocId_Type
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Type = "select_Template_by_keys_view_8_1_6";

//I_B_H
//8_1_7
//WhiteLabelDocId_DocId_Order
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Order = "select_Template_by_keys_view_8_1_7";

//I_B_J
//8_1_9
//WhiteLabelDocId_DocId_RelatedObjectType
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_RelatedObjectType = "select_Template_by_keys_view_8_1_9";

//I_B_K
//8_1_10
//WhiteLabelDocId_DocId_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_RelatedObjectDocId = "select_Template_by_keys_view_8_1_10";

//I_B_L
//8_1_11
//WhiteLabelDocId_DocId_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_TextLong = "select_Template_by_keys_view_8_1_11";

//I_B_M
//8_1_12
//WhiteLabelDocId_DocId_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_TextShort = "select_Template_by_keys_view_8_1_12";

//I_C_E
//8_2_4
//WhiteLabelDocId_IsDeleted_Title
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Title = "select_Template_by_keys_view_8_2_4";

//I_C_F
//8_2_5
//WhiteLabelDocId_IsDeleted_SubTitle
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_SubTitle = "select_Template_by_keys_view_8_2_5";

//I_C_G
//8_2_6
//WhiteLabelDocId_IsDeleted_Type
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Type = "select_Template_by_keys_view_8_2_6";

//I_C_H
//8_2_7
//WhiteLabelDocId_IsDeleted_Order
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Order = "select_Template_by_keys_view_8_2_7";

//I_C_J
//8_2_9
//WhiteLabelDocId_IsDeleted_RelatedObjectType
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_RelatedObjectType = "select_Template_by_keys_view_8_2_9";

//I_C_K
//8_2_10
//WhiteLabelDocId_IsDeleted_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_RelatedObjectDocId = "select_Template_by_keys_view_8_2_10";

//I_C_L
//8_2_11
//WhiteLabelDocId_IsDeleted_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLong = "select_Template_by_keys_view_8_2_11";

//I_C_M
//8_2_12
//WhiteLabelDocId_IsDeleted_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_TextShort = "select_Template_by_keys_view_8_2_12";

//I_E_F
//8_4_5
//WhiteLabelDocId_Title_SubTitle
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_SubTitle = "select_Template_by_keys_view_8_4_5";

//I_E_G
//8_4_6
//WhiteLabelDocId_Title_Type
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_Type = "select_Template_by_keys_view_8_4_6";

//I_E_H
//8_4_7
//WhiteLabelDocId_Title_Order
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_Order = "select_Template_by_keys_view_8_4_7";

//I_E_J
//8_4_9
//WhiteLabelDocId_Title_RelatedObjectType
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_RelatedObjectType = "select_Template_by_keys_view_8_4_9";

//I_E_K
//8_4_10
//WhiteLabelDocId_Title_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_RelatedObjectDocId = "select_Template_by_keys_view_8_4_10";

//I_E_L
//8_4_11
//WhiteLabelDocId_Title_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_TextLong = "select_Template_by_keys_view_8_4_11";

//I_E_M
//8_4_12
//WhiteLabelDocId_Title_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_TextShort = "select_Template_by_keys_view_8_4_12";

//I_F_G
//8_5_6
//WhiteLabelDocId_SubTitle_Type
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_Type = "select_Template_by_keys_view_8_5_6";

//I_F_H
//8_5_7
//WhiteLabelDocId_SubTitle_Order
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_Order = "select_Template_by_keys_view_8_5_7";

//I_F_J
//8_5_9
//WhiteLabelDocId_SubTitle_RelatedObjectType
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_RelatedObjectType = "select_Template_by_keys_view_8_5_9";

//I_F_K
//8_5_10
//WhiteLabelDocId_SubTitle_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_RelatedObjectDocId = "select_Template_by_keys_view_8_5_10";

//I_F_L
//8_5_11
//WhiteLabelDocId_SubTitle_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_TextLong = "select_Template_by_keys_view_8_5_11";

//I_F_M
//8_5_12
//WhiteLabelDocId_SubTitle_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_TextShort = "select_Template_by_keys_view_8_5_12";

//I_G_H
//8_6_7
//WhiteLabelDocId_Type_Order
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_Order = "select_Template_by_keys_view_8_6_7";

//I_G_J
//8_6_9
//WhiteLabelDocId_Type_RelatedObjectType
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_RelatedObjectType = "select_Template_by_keys_view_8_6_9";

//I_G_K
//8_6_10
//WhiteLabelDocId_Type_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_RelatedObjectDocId = "select_Template_by_keys_view_8_6_10";

//I_G_L
//8_6_11
//WhiteLabelDocId_Type_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_TextLong = "select_Template_by_keys_view_8_6_11";

//I_G_M
//8_6_12
//WhiteLabelDocId_Type_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_TextShort = "select_Template_by_keys_view_8_6_12";

//I_H_J
//8_7_9
//WhiteLabelDocId_Order_RelatedObjectType
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order_RelatedObjectType = "select_Template_by_keys_view_8_7_9";

//I_H_K
//8_7_10
//WhiteLabelDocId_Order_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order_RelatedObjectDocId = "select_Template_by_keys_view_8_7_10";

//I_H_L
//8_7_11
//WhiteLabelDocId_Order_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order_TextLong = "select_Template_by_keys_view_8_7_11";

//I_H_M
//8_7_12
//WhiteLabelDocId_Order_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order_TextShort = "select_Template_by_keys_view_8_7_12";

//I_J_K
//8_9_10
//WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId = "select_Template_by_keys_view_8_9_10";

//I_J_L
//8_9_11
//WhiteLabelDocId_RelatedObjectType_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_TextLong = "select_Template_by_keys_view_8_9_11";

//I_J_M
//8_9_12
//WhiteLabelDocId_RelatedObjectType_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_TextShort = "select_Template_by_keys_view_8_9_12";

//I_K_L
//8_10_11
//WhiteLabelDocId_RelatedObjectDocId_TextLong
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId_TextLong = "select_Template_by_keys_view_8_10_11";

//I_K_M
//8_10_12
//WhiteLabelDocId_RelatedObjectDocId_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId_TextShort = "select_Template_by_keys_view_8_10_12";

//I_L_M
//8_11_12
//WhiteLabelDocId_TextLong_TextShort
public static readonly string  PROC_Select_Template_By_Keys_View_WhiteLabelDocId_TextLong_TextShort = "select_Template_by_keys_view_8_11_12";
         #endregion

    }

}
