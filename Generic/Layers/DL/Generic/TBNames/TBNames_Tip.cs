

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Tip
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertTip = PROC_Prefix + "save_tip";
        #endregion
        #region Select
        public static readonly string PROC_Select_Tip_By_DocId = PROC_Prefix + "Select_tip_By_doc_id";        
        public static readonly string PROC_Select_Tip_By_Keys_View = PROC_Prefix + "Select_tip_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteTip = PROC_Prefix + "delete_tip";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipIsActive);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_Title = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipTitle);

        public static readonly string PRM_ShortText = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipShortText);

        public static readonly string PRM_LongText = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipLongText);

        public static readonly string PRM_Order = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipOrder);

        public static readonly string PRM_Date = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipDate);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Tip.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipIsActive);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_Title = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipTitle);

        public static readonly string Field_ShortText = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipShortText);

        public static readonly string Field_LongText = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipLongText);

        public static readonly string Field_Order = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipOrder);

        public static readonly string Field_Date = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipDate);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//E_A
//4_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated = "select_Tip_by_keys_view_4_0";

//E_B
//4_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId = "select_Tip_by_keys_view_4_1";

//E_C
//4_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_Tip_by_keys_view_4_2";

//E_F
//4_5
//WhiteLabelDocId_Title
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title = "select_Tip_by_keys_view_4_5";

//E_G
//4_6
//WhiteLabelDocId_ShortText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText = "select_Tip_by_keys_view_4_6";

//E_H
//4_7
//WhiteLabelDocId_LongText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_LongText = "select_Tip_by_keys_view_4_7";

//E_I
//4_8
//WhiteLabelDocId_Order
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Order = "select_Tip_by_keys_view_4_8";

//E_J
//4_9
//WhiteLabelDocId_Date
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Date = "select_Tip_by_keys_view_4_9";

//E_A_B
//4_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_Tip_by_keys_view_4_0_1";

//E_A_C
//4_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_Tip_by_keys_view_4_0_2";

//E_A_F
//4_0_5
//WhiteLabelDocId_DateCreated_Title
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Title = "select_Tip_by_keys_view_4_0_5";

//E_A_G
//4_0_6
//WhiteLabelDocId_DateCreated_ShortText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_ShortText = "select_Tip_by_keys_view_4_0_6";

//E_A_H
//4_0_7
//WhiteLabelDocId_DateCreated_LongText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_LongText = "select_Tip_by_keys_view_4_0_7";

//E_A_I
//4_0_8
//WhiteLabelDocId_DateCreated_Order
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Order = "select_Tip_by_keys_view_4_0_8";

//E_A_J
//4_0_9
//WhiteLabelDocId_DateCreated_Date
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Date = "select_Tip_by_keys_view_4_0_9";

//E_B_C
//4_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_Tip_by_keys_view_4_1_2";

//E_B_F
//4_1_5
//WhiteLabelDocId_DocId_Title
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Title = "select_Tip_by_keys_view_4_1_5";

//E_B_G
//4_1_6
//WhiteLabelDocId_DocId_ShortText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_ShortText = "select_Tip_by_keys_view_4_1_6";

//E_B_H
//4_1_7
//WhiteLabelDocId_DocId_LongText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_LongText = "select_Tip_by_keys_view_4_1_7";

//E_B_I
//4_1_8
//WhiteLabelDocId_DocId_Order
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Order = "select_Tip_by_keys_view_4_1_8";

//E_B_J
//4_1_9
//WhiteLabelDocId_DocId_Date
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Date = "select_Tip_by_keys_view_4_1_9";

//E_C_F
//4_2_5
//WhiteLabelDocId_IsDeleted_Title
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Title = "select_Tip_by_keys_view_4_2_5";

//E_C_G
//4_2_6
//WhiteLabelDocId_IsDeleted_ShortText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_ShortText = "select_Tip_by_keys_view_4_2_6";

//E_C_H
//4_2_7
//WhiteLabelDocId_IsDeleted_LongText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_LongText = "select_Tip_by_keys_view_4_2_7";

//E_C_I
//4_2_8
//WhiteLabelDocId_IsDeleted_Order
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Order = "select_Tip_by_keys_view_4_2_8";

//E_C_J
//4_2_9
//WhiteLabelDocId_IsDeleted_Date
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Date = "select_Tip_by_keys_view_4_2_9";

//E_F_G
//4_5_6
//WhiteLabelDocId_Title_ShortText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title_ShortText = "select_Tip_by_keys_view_4_5_6";

//E_F_H
//4_5_7
//WhiteLabelDocId_Title_LongText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title_LongText = "select_Tip_by_keys_view_4_5_7";

//E_F_I
//4_5_8
//WhiteLabelDocId_Title_Order
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title_Order = "select_Tip_by_keys_view_4_5_8";

//E_F_J
//4_5_9
//WhiteLabelDocId_Title_Date
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title_Date = "select_Tip_by_keys_view_4_5_9";

//E_G_H
//4_6_7
//WhiteLabelDocId_ShortText_LongText
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_LongText = "select_Tip_by_keys_view_4_6_7";

//E_G_I
//4_6_8
//WhiteLabelDocId_ShortText_Order
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_Order = "select_Tip_by_keys_view_4_6_8";

//E_G_J
//4_6_9
//WhiteLabelDocId_ShortText_Date
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_Date = "select_Tip_by_keys_view_4_6_9";

//E_H_I
//4_7_8
//WhiteLabelDocId_LongText_Order
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_LongText_Order = "select_Tip_by_keys_view_4_7_8";

//E_H_J
//4_7_9
//WhiteLabelDocId_LongText_Date
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_LongText_Date = "select_Tip_by_keys_view_4_7_9";

//E_I_J
//4_8_9
//WhiteLabelDocId_Order_Date
public static readonly string  PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Order_Date = "select_Tip_by_keys_view_4_8_9";
         #endregion

    }

}
