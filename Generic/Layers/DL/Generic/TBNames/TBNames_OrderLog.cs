

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_OrderLog
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertOrderLog = PROC_Prefix + "save_order_log";
        #endregion
        #region Select
        public static readonly string PROC_Select_OrderLog_By_DocId = PROC_Prefix + "Select_order_log_By_doc_id";        
        public static readonly string PROC_Select_OrderLog_By_Keys_View = PROC_Prefix + "Select_order_log_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteOrderLog = PROC_Prefix + "delete_order_log";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogIsActive);

        public static readonly string PRM_Type = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogType);

        public static readonly string PRM_Status = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogStatus);

        public static readonly string PRM_XML = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogXML);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_OrdersDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string PRM_OrderStage = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogOrderStage);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "OrderLog.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogIsActive);

        public static readonly string Field_Type = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogType);

        public static readonly string Field_Status = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogStatus);

        public static readonly string Field_XML = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogXML);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_OrdersDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string Field_OrderStage = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderLogOrderStage);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//H_A
//7_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated = "select_OrderLog_by_keys_view_7_0";

//H_B
//7_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId = "select_OrderLog_by_keys_view_7_1";

//H_C
//7_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_OrderLog_by_keys_view_7_2";

//H_E
//7_4
//WhiteLabelDocId_Type
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type = "select_OrderLog_by_keys_view_7_4";

//H_F
//7_5
//WhiteLabelDocId_Status
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status = "select_OrderLog_by_keys_view_7_5";

//H_G
//7_6
//WhiteLabelDocId_XML
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML = "select_OrderLog_by_keys_view_7_6";

//H_I
//7_8
//WhiteLabelDocId_OrdersDocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrdersDocId = "select_OrderLog_by_keys_view_7_8";

//H_J
//7_9
//WhiteLabelDocId_OrderStage
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrderStage = "select_OrderLog_by_keys_view_7_9";

//H_A_B
//7_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_OrderLog_by_keys_view_7_0_1";

//H_A_C
//7_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_OrderLog_by_keys_view_7_0_2";

//H_A_E
//7_0_4
//WhiteLabelDocId_DateCreated_Type
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_Type = "select_OrderLog_by_keys_view_7_0_4";

//H_A_F
//7_0_5
//WhiteLabelDocId_DateCreated_Status
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_Status = "select_OrderLog_by_keys_view_7_0_5";

//H_A_G
//7_0_6
//WhiteLabelDocId_DateCreated_XML
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_XML = "select_OrderLog_by_keys_view_7_0_6";

//H_A_I
//7_0_8
//WhiteLabelDocId_DateCreated_OrdersDocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId = "select_OrderLog_by_keys_view_7_0_8";

//H_A_J
//7_0_9
//WhiteLabelDocId_DateCreated_OrderStage
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_OrderStage = "select_OrderLog_by_keys_view_7_0_9";

//H_B_C
//7_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_OrderLog_by_keys_view_7_1_2";

//H_B_E
//7_1_4
//WhiteLabelDocId_DocId_Type
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_Type = "select_OrderLog_by_keys_view_7_1_4";

//H_B_F
//7_1_5
//WhiteLabelDocId_DocId_Status
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_Status = "select_OrderLog_by_keys_view_7_1_5";

//H_B_G
//7_1_6
//WhiteLabelDocId_DocId_XML
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_XML = "select_OrderLog_by_keys_view_7_1_6";

//H_B_I
//7_1_8
//WhiteLabelDocId_DocId_OrdersDocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId = "select_OrderLog_by_keys_view_7_1_8";

//H_B_J
//7_1_9
//WhiteLabelDocId_DocId_OrderStage
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_OrderStage = "select_OrderLog_by_keys_view_7_1_9";

//H_C_E
//7_2_4
//WhiteLabelDocId_IsDeleted_Type
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_Type = "select_OrderLog_by_keys_view_7_2_4";

//H_C_F
//7_2_5
//WhiteLabelDocId_IsDeleted_Status
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_Status = "select_OrderLog_by_keys_view_7_2_5";

//H_C_G
//7_2_6
//WhiteLabelDocId_IsDeleted_XML
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_XML = "select_OrderLog_by_keys_view_7_2_6";

//H_C_I
//7_2_8
//WhiteLabelDocId_IsDeleted_OrdersDocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId = "select_OrderLog_by_keys_view_7_2_8";

//H_C_J
//7_2_9
//WhiteLabelDocId_IsDeleted_OrderStage
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderStage = "select_OrderLog_by_keys_view_7_2_9";

//H_E_F
//7_4_5
//WhiteLabelDocId_Type_Status
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_Status = "select_OrderLog_by_keys_view_7_4_5";

//H_E_G
//7_4_6
//WhiteLabelDocId_Type_XML
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_XML = "select_OrderLog_by_keys_view_7_4_6";

//H_E_I
//7_4_8
//WhiteLabelDocId_Type_OrdersDocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_OrdersDocId = "select_OrderLog_by_keys_view_7_4_8";

//H_E_J
//7_4_9
//WhiteLabelDocId_Type_OrderStage
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_OrderStage = "select_OrderLog_by_keys_view_7_4_9";

//H_F_G
//7_5_6
//WhiteLabelDocId_Status_XML
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_XML = "select_OrderLog_by_keys_view_7_5_6";

//H_F_I
//7_5_8
//WhiteLabelDocId_Status_OrdersDocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_OrdersDocId = "select_OrderLog_by_keys_view_7_5_8";

//H_F_J
//7_5_9
//WhiteLabelDocId_Status_OrderStage
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_OrderStage = "select_OrderLog_by_keys_view_7_5_9";

//H_G_I
//7_6_8
//WhiteLabelDocId_XML_OrdersDocId
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML_OrdersDocId = "select_OrderLog_by_keys_view_7_6_8";

//H_G_J
//7_6_9
//WhiteLabelDocId_XML_OrderStage
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML_OrderStage = "select_OrderLog_by_keys_view_7_6_9";

//H_I_J
//7_8_9
//WhiteLabelDocId_OrdersDocId_OrderStage
public static readonly string  PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrdersDocId_OrderStage = "select_OrderLog_by_keys_view_7_8_9";
         #endregion

    }

}
