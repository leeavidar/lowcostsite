

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Messages
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertMessages = PROC_Prefix + "save_messages";
        #endregion
        #region Select
        public static readonly string PROC_Select_Messages_By_DocId = PROC_Prefix + "Select_messages_By_doc_id";        
        public static readonly string PROC_Select_Messages_By_Keys_View = PROC_Prefix + "Select_messages_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteMessages = PROC_Prefix + "delete_messages";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesIsActive);

        public static readonly string PRM_SentDateTime = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesSentDateTime);

        public static readonly string PRM_MessageType = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesMessageType);

        public static readonly string PRM_LowCostPNR = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesLowCostPNR);

        public static readonly string PRM_OrdersDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string PRM_Content = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesContent);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Messages.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesIsActive);

        public static readonly string Field_SentDateTime = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesSentDateTime);

        public static readonly string Field_MessageType = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesMessageType);

        public static readonly string Field_LowCostPNR = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesLowCostPNR);

        public static readonly string Field_OrdersDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string Field_Content = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyMessagesContent);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//J_A
//9_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated = "select_Messages_by_keys_view_9_0";

//J_B
//9_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId = "select_Messages_by_keys_view_9_1";

//J_C
//9_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_Messages_by_keys_view_9_2";

//J_E
//9_4
//WhiteLabelDocId_SentDateTime
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime = "select_Messages_by_keys_view_9_4";

//J_F
//9_5
//WhiteLabelDocId_MessageType
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType = "select_Messages_by_keys_view_9_5";

//J_G
//9_6
//WhiteLabelDocId_LowCostPNR
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR = "select_Messages_by_keys_view_9_6";

//J_H
//9_7
//WhiteLabelDocId_OrdersDocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_OrdersDocId = "select_Messages_by_keys_view_9_7";

//J_I
//9_8
//WhiteLabelDocId_Content
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_Content = "select_Messages_by_keys_view_9_8";

//J_A_B
//9_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_Messages_by_keys_view_9_0_1";

//J_A_C
//9_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_Messages_by_keys_view_9_0_2";

//J_A_E
//9_0_4
//WhiteLabelDocId_DateCreated_SentDateTime
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_SentDateTime = "select_Messages_by_keys_view_9_0_4";

//J_A_F
//9_0_5
//WhiteLabelDocId_DateCreated_MessageType
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_MessageType = "select_Messages_by_keys_view_9_0_5";

//J_A_G
//9_0_6
//WhiteLabelDocId_DateCreated_LowCostPNR
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR = "select_Messages_by_keys_view_9_0_6";

//J_A_H
//9_0_7
//WhiteLabelDocId_DateCreated_OrdersDocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId = "select_Messages_by_keys_view_9_0_7";

//J_A_I
//9_0_8
//WhiteLabelDocId_DateCreated_Content
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_Content = "select_Messages_by_keys_view_9_0_8";

//J_B_C
//9_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_Messages_by_keys_view_9_1_2";

//J_B_E
//9_1_4
//WhiteLabelDocId_DocId_SentDateTime
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_SentDateTime = "select_Messages_by_keys_view_9_1_4";

//J_B_F
//9_1_5
//WhiteLabelDocId_DocId_MessageType
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_MessageType = "select_Messages_by_keys_view_9_1_5";

//J_B_G
//9_1_6
//WhiteLabelDocId_DocId_LowCostPNR
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR = "select_Messages_by_keys_view_9_1_6";

//J_B_H
//9_1_7
//WhiteLabelDocId_DocId_OrdersDocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId = "select_Messages_by_keys_view_9_1_7";

//J_B_I
//9_1_8
//WhiteLabelDocId_DocId_Content
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_Content = "select_Messages_by_keys_view_9_1_8";

//J_C_E
//9_2_4
//WhiteLabelDocId_IsDeleted_SentDateTime
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_SentDateTime = "select_Messages_by_keys_view_9_2_4";

//J_C_F
//9_2_5
//WhiteLabelDocId_IsDeleted_MessageType
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_MessageType = "select_Messages_by_keys_view_9_2_5";

//J_C_G
//9_2_6
//WhiteLabelDocId_IsDeleted_LowCostPNR
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR = "select_Messages_by_keys_view_9_2_6";

//J_C_H
//9_2_7
//WhiteLabelDocId_IsDeleted_OrdersDocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId = "select_Messages_by_keys_view_9_2_7";

//J_C_I
//9_2_8
//WhiteLabelDocId_IsDeleted_Content
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_Content = "select_Messages_by_keys_view_9_2_8";

//J_E_F
//9_4_5
//WhiteLabelDocId_SentDateTime_MessageType
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_MessageType = "select_Messages_by_keys_view_9_4_5";

//J_E_G
//9_4_6
//WhiteLabelDocId_SentDateTime_LowCostPNR
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_LowCostPNR = "select_Messages_by_keys_view_9_4_6";

//J_E_H
//9_4_7
//WhiteLabelDocId_SentDateTime_OrdersDocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_OrdersDocId = "select_Messages_by_keys_view_9_4_7";

//J_E_I
//9_4_8
//WhiteLabelDocId_SentDateTime_Content
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_Content = "select_Messages_by_keys_view_9_4_8";

//J_F_G
//9_5_6
//WhiteLabelDocId_MessageType_LowCostPNR
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_LowCostPNR = "select_Messages_by_keys_view_9_5_6";

//J_F_H
//9_5_7
//WhiteLabelDocId_MessageType_OrdersDocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_OrdersDocId = "select_Messages_by_keys_view_9_5_7";

//J_F_I
//9_5_8
//WhiteLabelDocId_MessageType_Content
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_Content = "select_Messages_by_keys_view_9_5_8";

//J_G_H
//9_6_7
//WhiteLabelDocId_LowCostPNR_OrdersDocId
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrdersDocId = "select_Messages_by_keys_view_9_6_7";

//J_G_I
//9_6_8
//WhiteLabelDocId_LowCostPNR_Content
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR_Content = "select_Messages_by_keys_view_9_6_8";

//J_H_I
//9_7_8
//WhiteLabelDocId_OrdersDocId_Content
public static readonly string  PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_OrdersDocId_Content = "select_Messages_by_keys_view_9_7_8";
         #endregion

    }

}
