

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Passenger
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertPassenger = PROC_Prefix + "save_passenger";
        #endregion
        #region Select
        public static readonly string PROC_Select_Passenger_By_DocId = PROC_Prefix + "Select_passenger_By_doc_id";        
        public static readonly string PROC_Select_Passenger_By_Keys_View = PROC_Prefix + "Select_passenger_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeletePassenger = PROC_Prefix + "delete_passenger";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerIsActive);

        public static readonly string PRM_NetPrice = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerNetPrice);

        public static readonly string PRM_FirstName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerFirstName);

        public static readonly string PRM_LastName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerLastName);

        public static readonly string PRM_DateOfBirth = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDateOfBirth);

        public static readonly string PRM_Gender = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerGender);

        public static readonly string PRM_Title = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerTitle);

        public static readonly string PRM_PassportNumber = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerPassportNumber);

        public static readonly string PRM_PassportExpiryDate = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerPassportExpiryDate);

        public static readonly string PRM_PassportIssueCountry = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerPassportIssueCountry);

        public static readonly string PRM_CheckInTypeOutward = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerCheckInTypeOutward);

        public static readonly string PRM_CheckInTypeReturn = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerCheckInTypeReturn);

        public static readonly string PRM_CheckInPriceOutward = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerCheckInPriceOutward);

        public static readonly string PRM_CheckInPriceReturn = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerCheckInPriceReturn);

        public static readonly string PRM_PriceMarkUp = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerPriceMarkUp);

        public static readonly string PRM_SeatPerFlightLegDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegDocId);

        public static readonly string PRM_OrdersDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Passenger.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerIsActive);

        public static readonly string Field_NetPrice = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerNetPrice);

        public static readonly string Field_FirstName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerFirstName);

        public static readonly string Field_LastName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerLastName);

        public static readonly string Field_DateOfBirth = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDateOfBirth);

        public static readonly string Field_Gender = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerGender);

        public static readonly string Field_Title = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerTitle);

        public static readonly string Field_PassportNumber = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerPassportNumber);

        public static readonly string Field_PassportExpiryDate = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerPassportExpiryDate);

        public static readonly string Field_PassportIssueCountry = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerPassportIssueCountry);

        public static readonly string Field_CheckInTypeOutward = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerCheckInTypeOutward);

        public static readonly string Field_CheckInTypeReturn = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerCheckInTypeReturn);

        public static readonly string Field_CheckInPriceOutward = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerCheckInPriceOutward);

        public static readonly string Field_CheckInPriceReturn = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerCheckInPriceReturn);

        public static readonly string Field_PriceMarkUp = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerPriceMarkUp);

        public static readonly string Field_SeatPerFlightLegDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegDocId);

        public static readonly string Field_OrdersDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//U_A
//20_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated = "select_Passenger_by_keys_view_20_0";

//U_B
//20_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId = "select_Passenger_by_keys_view_20_1";

//U_C
//20_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_Passenger_by_keys_view_20_2";

//U_E
//20_4
//WhiteLabelDocId_NetPrice
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice = "select_Passenger_by_keys_view_20_4";

//U_F
//20_5
//WhiteLabelDocId_FirstName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName = "select_Passenger_by_keys_view_20_5";

//U_G
//20_6
//WhiteLabelDocId_LastName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName = "select_Passenger_by_keys_view_20_6";

//U_H
//20_7
//WhiteLabelDocId_DateOfBirth
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth = "select_Passenger_by_keys_view_20_7";

//U_I
//20_8
//WhiteLabelDocId_Gender
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender = "select_Passenger_by_keys_view_20_8";

//U_J
//20_9
//WhiteLabelDocId_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title = "select_Passenger_by_keys_view_20_9";

//U_K
//20_10
//WhiteLabelDocId_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber = "select_Passenger_by_keys_view_20_10";

//U_L
//20_11
//WhiteLabelDocId_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate = "select_Passenger_by_keys_view_20_11";

//U_M
//20_12
//WhiteLabelDocId_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry = "select_Passenger_by_keys_view_20_12";

//U_N
//20_13
//WhiteLabelDocId_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward = "select_Passenger_by_keys_view_20_13";

//U_O
//20_14
//WhiteLabelDocId_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn = "select_Passenger_by_keys_view_20_14";

//U_P
//20_15
//WhiteLabelDocId_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward = "select_Passenger_by_keys_view_20_15";

//U_Q
//20_16
//WhiteLabelDocId_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn = "select_Passenger_by_keys_view_20_16";

//U_R
//20_17
//WhiteLabelDocId_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp = "select_Passenger_by_keys_view_20_17";

//U_S
//20_18
//WhiteLabelDocId_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_18";

//U_T
//20_19
//WhiteLabelDocId_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_OrdersDocId = "select_Passenger_by_keys_view_20_19";

//U_A_B
//20_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_Passenger_by_keys_view_20_0_1";

//U_A_C
//20_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_Passenger_by_keys_view_20_0_2";

//U_A_E
//20_0_4
//WhiteLabelDocId_DateCreated_NetPrice
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_NetPrice = "select_Passenger_by_keys_view_20_0_4";

//U_A_F
//20_0_5
//WhiteLabelDocId_DateCreated_FirstName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName = "select_Passenger_by_keys_view_20_0_5";

//U_A_G
//20_0_6
//WhiteLabelDocId_DateCreated_LastName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_LastName = "select_Passenger_by_keys_view_20_0_6";

//U_A_H
//20_0_7
//WhiteLabelDocId_DateCreated_DateOfBirth
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_DateOfBirth = "select_Passenger_by_keys_view_20_0_7";

//U_A_I
//20_0_8
//WhiteLabelDocId_DateCreated_Gender
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_Gender = "select_Passenger_by_keys_view_20_0_8";

//U_A_J
//20_0_9
//WhiteLabelDocId_DateCreated_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_Title = "select_Passenger_by_keys_view_20_0_9";

//U_A_K
//20_0_10
//WhiteLabelDocId_DateCreated_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportNumber = "select_Passenger_by_keys_view_20_0_10";

//U_A_L
//20_0_11
//WhiteLabelDocId_DateCreated_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportExpiryDate = "select_Passenger_by_keys_view_20_0_11";

//U_A_M
//20_0_12
//WhiteLabelDocId_DateCreated_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportIssueCountry = "select_Passenger_by_keys_view_20_0_12";

//U_A_N
//20_0_13
//WhiteLabelDocId_DateCreated_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInTypeOutward = "select_Passenger_by_keys_view_20_0_13";

//U_A_O
//20_0_14
//WhiteLabelDocId_DateCreated_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInTypeReturn = "select_Passenger_by_keys_view_20_0_14";

//U_A_P
//20_0_15
//WhiteLabelDocId_DateCreated_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInPriceOutward = "select_Passenger_by_keys_view_20_0_15";

//U_A_Q
//20_0_16
//WhiteLabelDocId_DateCreated_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInPriceReturn = "select_Passenger_by_keys_view_20_0_16";

//U_A_R
//20_0_17
//WhiteLabelDocId_DateCreated_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PriceMarkUp = "select_Passenger_by_keys_view_20_0_17";

//U_A_S
//20_0_18
//WhiteLabelDocId_DateCreated_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_0_18";

//U_A_T
//20_0_19
//WhiteLabelDocId_DateCreated_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId = "select_Passenger_by_keys_view_20_0_19";

//U_B_C
//20_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_Passenger_by_keys_view_20_1_2";

//U_B_E
//20_1_4
//WhiteLabelDocId_DocId_NetPrice
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_NetPrice = "select_Passenger_by_keys_view_20_1_4";

//U_B_F
//20_1_5
//WhiteLabelDocId_DocId_FirstName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_FirstName = "select_Passenger_by_keys_view_20_1_5";

//U_B_G
//20_1_6
//WhiteLabelDocId_DocId_LastName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_LastName = "select_Passenger_by_keys_view_20_1_6";

//U_B_H
//20_1_7
//WhiteLabelDocId_DocId_DateOfBirth
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_DateOfBirth = "select_Passenger_by_keys_view_20_1_7";

//U_B_I
//20_1_8
//WhiteLabelDocId_DocId_Gender
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_Gender = "select_Passenger_by_keys_view_20_1_8";

//U_B_J
//20_1_9
//WhiteLabelDocId_DocId_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_Title = "select_Passenger_by_keys_view_20_1_9";

//U_B_K
//20_1_10
//WhiteLabelDocId_DocId_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportNumber = "select_Passenger_by_keys_view_20_1_10";

//U_B_L
//20_1_11
//WhiteLabelDocId_DocId_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportExpiryDate = "select_Passenger_by_keys_view_20_1_11";

//U_B_M
//20_1_12
//WhiteLabelDocId_DocId_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportIssueCountry = "select_Passenger_by_keys_view_20_1_12";

//U_B_N
//20_1_13
//WhiteLabelDocId_DocId_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInTypeOutward = "select_Passenger_by_keys_view_20_1_13";

//U_B_O
//20_1_14
//WhiteLabelDocId_DocId_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInTypeReturn = "select_Passenger_by_keys_view_20_1_14";

//U_B_P
//20_1_15
//WhiteLabelDocId_DocId_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInPriceOutward = "select_Passenger_by_keys_view_20_1_15";

//U_B_Q
//20_1_16
//WhiteLabelDocId_DocId_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInPriceReturn = "select_Passenger_by_keys_view_20_1_16";

//U_B_R
//20_1_17
//WhiteLabelDocId_DocId_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PriceMarkUp = "select_Passenger_by_keys_view_20_1_17";

//U_B_S
//20_1_18
//WhiteLabelDocId_DocId_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_1_18";

//U_B_T
//20_1_19
//WhiteLabelDocId_DocId_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId = "select_Passenger_by_keys_view_20_1_19";

//U_C_E
//20_2_4
//WhiteLabelDocId_IsDeleted_NetPrice
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_NetPrice = "select_Passenger_by_keys_view_20_2_4";

//U_C_F
//20_2_5
//WhiteLabelDocId_IsDeleted_FirstName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName = "select_Passenger_by_keys_view_20_2_5";

//U_C_G
//20_2_6
//WhiteLabelDocId_IsDeleted_LastName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName = "select_Passenger_by_keys_view_20_2_6";

//U_C_H
//20_2_7
//WhiteLabelDocId_IsDeleted_DateOfBirth
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_DateOfBirth = "select_Passenger_by_keys_view_20_2_7";

//U_C_I
//20_2_8
//WhiteLabelDocId_IsDeleted_Gender
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_Gender = "select_Passenger_by_keys_view_20_2_8";

//U_C_J
//20_2_9
//WhiteLabelDocId_IsDeleted_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_Title = "select_Passenger_by_keys_view_20_2_9";

//U_C_K
//20_2_10
//WhiteLabelDocId_IsDeleted_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportNumber = "select_Passenger_by_keys_view_20_2_10";

//U_C_L
//20_2_11
//WhiteLabelDocId_IsDeleted_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportExpiryDate = "select_Passenger_by_keys_view_20_2_11";

//U_C_M
//20_2_12
//WhiteLabelDocId_IsDeleted_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportIssueCountry = "select_Passenger_by_keys_view_20_2_12";

//U_C_N
//20_2_13
//WhiteLabelDocId_IsDeleted_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInTypeOutward = "select_Passenger_by_keys_view_20_2_13";

//U_C_O
//20_2_14
//WhiteLabelDocId_IsDeleted_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInTypeReturn = "select_Passenger_by_keys_view_20_2_14";

//U_C_P
//20_2_15
//WhiteLabelDocId_IsDeleted_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInPriceOutward = "select_Passenger_by_keys_view_20_2_15";

//U_C_Q
//20_2_16
//WhiteLabelDocId_IsDeleted_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInPriceReturn = "select_Passenger_by_keys_view_20_2_16";

//U_C_R
//20_2_17
//WhiteLabelDocId_IsDeleted_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceMarkUp = "select_Passenger_by_keys_view_20_2_17";

//U_C_S
//20_2_18
//WhiteLabelDocId_IsDeleted_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_2_18";

//U_C_T
//20_2_19
//WhiteLabelDocId_IsDeleted_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId = "select_Passenger_by_keys_view_20_2_19";

//U_E_F
//20_4_5
//WhiteLabelDocId_NetPrice_FirstName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_FirstName = "select_Passenger_by_keys_view_20_4_5";

//U_E_G
//20_4_6
//WhiteLabelDocId_NetPrice_LastName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_LastName = "select_Passenger_by_keys_view_20_4_6";

//U_E_H
//20_4_7
//WhiteLabelDocId_NetPrice_DateOfBirth
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_DateOfBirth = "select_Passenger_by_keys_view_20_4_7";

//U_E_I
//20_4_8
//WhiteLabelDocId_NetPrice_Gender
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_Gender = "select_Passenger_by_keys_view_20_4_8";

//U_E_J
//20_4_9
//WhiteLabelDocId_NetPrice_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_Title = "select_Passenger_by_keys_view_20_4_9";

//U_E_K
//20_4_10
//WhiteLabelDocId_NetPrice_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportNumber = "select_Passenger_by_keys_view_20_4_10";

//U_E_L
//20_4_11
//WhiteLabelDocId_NetPrice_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportExpiryDate = "select_Passenger_by_keys_view_20_4_11";

//U_E_M
//20_4_12
//WhiteLabelDocId_NetPrice_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportIssueCountry = "select_Passenger_by_keys_view_20_4_12";

//U_E_N
//20_4_13
//WhiteLabelDocId_NetPrice_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInTypeOutward = "select_Passenger_by_keys_view_20_4_13";

//U_E_O
//20_4_14
//WhiteLabelDocId_NetPrice_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInTypeReturn = "select_Passenger_by_keys_view_20_4_14";

//U_E_P
//20_4_15
//WhiteLabelDocId_NetPrice_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInPriceOutward = "select_Passenger_by_keys_view_20_4_15";

//U_E_Q
//20_4_16
//WhiteLabelDocId_NetPrice_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInPriceReturn = "select_Passenger_by_keys_view_20_4_16";

//U_E_R
//20_4_17
//WhiteLabelDocId_NetPrice_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PriceMarkUp = "select_Passenger_by_keys_view_20_4_17";

//U_E_S
//20_4_18
//WhiteLabelDocId_NetPrice_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_4_18";

//U_E_T
//20_4_19
//WhiteLabelDocId_NetPrice_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_OrdersDocId = "select_Passenger_by_keys_view_20_4_19";

//U_F_G
//20_5_6
//WhiteLabelDocId_FirstName_LastName
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_LastName = "select_Passenger_by_keys_view_20_5_6";

//U_F_H
//20_5_7
//WhiteLabelDocId_FirstName_DateOfBirth
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_DateOfBirth = "select_Passenger_by_keys_view_20_5_7";

//U_F_I
//20_5_8
//WhiteLabelDocId_FirstName_Gender
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_Gender = "select_Passenger_by_keys_view_20_5_8";

//U_F_J
//20_5_9
//WhiteLabelDocId_FirstName_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_Title = "select_Passenger_by_keys_view_20_5_9";

//U_F_K
//20_5_10
//WhiteLabelDocId_FirstName_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportNumber = "select_Passenger_by_keys_view_20_5_10";

//U_F_L
//20_5_11
//WhiteLabelDocId_FirstName_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportExpiryDate = "select_Passenger_by_keys_view_20_5_11";

//U_F_M
//20_5_12
//WhiteLabelDocId_FirstName_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportIssueCountry = "select_Passenger_by_keys_view_20_5_12";

//U_F_N
//20_5_13
//WhiteLabelDocId_FirstName_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInTypeOutward = "select_Passenger_by_keys_view_20_5_13";

//U_F_O
//20_5_14
//WhiteLabelDocId_FirstName_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInTypeReturn = "select_Passenger_by_keys_view_20_5_14";

//U_F_P
//20_5_15
//WhiteLabelDocId_FirstName_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInPriceOutward = "select_Passenger_by_keys_view_20_5_15";

//U_F_Q
//20_5_16
//WhiteLabelDocId_FirstName_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInPriceReturn = "select_Passenger_by_keys_view_20_5_16";

//U_F_R
//20_5_17
//WhiteLabelDocId_FirstName_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PriceMarkUp = "select_Passenger_by_keys_view_20_5_17";

//U_F_S
//20_5_18
//WhiteLabelDocId_FirstName_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_5_18";

//U_F_T
//20_5_19
//WhiteLabelDocId_FirstName_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_OrdersDocId = "select_Passenger_by_keys_view_20_5_19";

//U_G_H
//20_6_7
//WhiteLabelDocId_LastName_DateOfBirth
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_DateOfBirth = "select_Passenger_by_keys_view_20_6_7";

//U_G_I
//20_6_8
//WhiteLabelDocId_LastName_Gender
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_Gender = "select_Passenger_by_keys_view_20_6_8";

//U_G_J
//20_6_9
//WhiteLabelDocId_LastName_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_Title = "select_Passenger_by_keys_view_20_6_9";

//U_G_K
//20_6_10
//WhiteLabelDocId_LastName_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportNumber = "select_Passenger_by_keys_view_20_6_10";

//U_G_L
//20_6_11
//WhiteLabelDocId_LastName_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportExpiryDate = "select_Passenger_by_keys_view_20_6_11";

//U_G_M
//20_6_12
//WhiteLabelDocId_LastName_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportIssueCountry = "select_Passenger_by_keys_view_20_6_12";

//U_G_N
//20_6_13
//WhiteLabelDocId_LastName_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInTypeOutward = "select_Passenger_by_keys_view_20_6_13";

//U_G_O
//20_6_14
//WhiteLabelDocId_LastName_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInTypeReturn = "select_Passenger_by_keys_view_20_6_14";

//U_G_P
//20_6_15
//WhiteLabelDocId_LastName_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInPriceOutward = "select_Passenger_by_keys_view_20_6_15";

//U_G_Q
//20_6_16
//WhiteLabelDocId_LastName_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInPriceReturn = "select_Passenger_by_keys_view_20_6_16";

//U_G_R
//20_6_17
//WhiteLabelDocId_LastName_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PriceMarkUp = "select_Passenger_by_keys_view_20_6_17";

//U_G_S
//20_6_18
//WhiteLabelDocId_LastName_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_6_18";

//U_G_T
//20_6_19
//WhiteLabelDocId_LastName_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_OrdersDocId = "select_Passenger_by_keys_view_20_6_19";

//U_H_I
//20_7_8
//WhiteLabelDocId_DateOfBirth_Gender
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_Gender = "select_Passenger_by_keys_view_20_7_8";

//U_H_J
//20_7_9
//WhiteLabelDocId_DateOfBirth_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_Title = "select_Passenger_by_keys_view_20_7_9";

//U_H_K
//20_7_10
//WhiteLabelDocId_DateOfBirth_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportNumber = "select_Passenger_by_keys_view_20_7_10";

//U_H_L
//20_7_11
//WhiteLabelDocId_DateOfBirth_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportExpiryDate = "select_Passenger_by_keys_view_20_7_11";

//U_H_M
//20_7_12
//WhiteLabelDocId_DateOfBirth_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportIssueCountry = "select_Passenger_by_keys_view_20_7_12";

//U_H_N
//20_7_13
//WhiteLabelDocId_DateOfBirth_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInTypeOutward = "select_Passenger_by_keys_view_20_7_13";

//U_H_O
//20_7_14
//WhiteLabelDocId_DateOfBirth_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInTypeReturn = "select_Passenger_by_keys_view_20_7_14";

//U_H_P
//20_7_15
//WhiteLabelDocId_DateOfBirth_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInPriceOutward = "select_Passenger_by_keys_view_20_7_15";

//U_H_Q
//20_7_16
//WhiteLabelDocId_DateOfBirth_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInPriceReturn = "select_Passenger_by_keys_view_20_7_16";

//U_H_R
//20_7_17
//WhiteLabelDocId_DateOfBirth_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PriceMarkUp = "select_Passenger_by_keys_view_20_7_17";

//U_H_S
//20_7_18
//WhiteLabelDocId_DateOfBirth_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_7_18";

//U_H_T
//20_7_19
//WhiteLabelDocId_DateOfBirth_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_OrdersDocId = "select_Passenger_by_keys_view_20_7_19";

//U_I_J
//20_8_9
//WhiteLabelDocId_Gender_Title
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_Title = "select_Passenger_by_keys_view_20_8_9";

//U_I_K
//20_8_10
//WhiteLabelDocId_Gender_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportNumber = "select_Passenger_by_keys_view_20_8_10";

//U_I_L
//20_8_11
//WhiteLabelDocId_Gender_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportExpiryDate = "select_Passenger_by_keys_view_20_8_11";

//U_I_M
//20_8_12
//WhiteLabelDocId_Gender_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportIssueCountry = "select_Passenger_by_keys_view_20_8_12";

//U_I_N
//20_8_13
//WhiteLabelDocId_Gender_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInTypeOutward = "select_Passenger_by_keys_view_20_8_13";

//U_I_O
//20_8_14
//WhiteLabelDocId_Gender_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInTypeReturn = "select_Passenger_by_keys_view_20_8_14";

//U_I_P
//20_8_15
//WhiteLabelDocId_Gender_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInPriceOutward = "select_Passenger_by_keys_view_20_8_15";

//U_I_Q
//20_8_16
//WhiteLabelDocId_Gender_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInPriceReturn = "select_Passenger_by_keys_view_20_8_16";

//U_I_R
//20_8_17
//WhiteLabelDocId_Gender_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PriceMarkUp = "select_Passenger_by_keys_view_20_8_17";

//U_I_S
//20_8_18
//WhiteLabelDocId_Gender_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_8_18";

//U_I_T
//20_8_19
//WhiteLabelDocId_Gender_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_OrdersDocId = "select_Passenger_by_keys_view_20_8_19";

//U_J_K
//20_9_10
//WhiteLabelDocId_Title_PassportNumber
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportNumber = "select_Passenger_by_keys_view_20_9_10";

//U_J_L
//20_9_11
//WhiteLabelDocId_Title_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportExpiryDate = "select_Passenger_by_keys_view_20_9_11";

//U_J_M
//20_9_12
//WhiteLabelDocId_Title_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportIssueCountry = "select_Passenger_by_keys_view_20_9_12";

//U_J_N
//20_9_13
//WhiteLabelDocId_Title_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInTypeOutward = "select_Passenger_by_keys_view_20_9_13";

//U_J_O
//20_9_14
//WhiteLabelDocId_Title_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInTypeReturn = "select_Passenger_by_keys_view_20_9_14";

//U_J_P
//20_9_15
//WhiteLabelDocId_Title_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInPriceOutward = "select_Passenger_by_keys_view_20_9_15";

//U_J_Q
//20_9_16
//WhiteLabelDocId_Title_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInPriceReturn = "select_Passenger_by_keys_view_20_9_16";

//U_J_R
//20_9_17
//WhiteLabelDocId_Title_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PriceMarkUp = "select_Passenger_by_keys_view_20_9_17";

//U_J_S
//20_9_18
//WhiteLabelDocId_Title_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_9_18";

//U_J_T
//20_9_19
//WhiteLabelDocId_Title_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_OrdersDocId = "select_Passenger_by_keys_view_20_9_19";

//U_K_L
//20_10_11
//WhiteLabelDocId_PassportNumber_PassportExpiryDate
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PassportExpiryDate = "select_Passenger_by_keys_view_20_10_11";

//U_K_M
//20_10_12
//WhiteLabelDocId_PassportNumber_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PassportIssueCountry = "select_Passenger_by_keys_view_20_10_12";

//U_K_N
//20_10_13
//WhiteLabelDocId_PassportNumber_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInTypeOutward = "select_Passenger_by_keys_view_20_10_13";

//U_K_O
//20_10_14
//WhiteLabelDocId_PassportNumber_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInTypeReturn = "select_Passenger_by_keys_view_20_10_14";

//U_K_P
//20_10_15
//WhiteLabelDocId_PassportNumber_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInPriceOutward = "select_Passenger_by_keys_view_20_10_15";

//U_K_Q
//20_10_16
//WhiteLabelDocId_PassportNumber_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInPriceReturn = "select_Passenger_by_keys_view_20_10_16";

//U_K_R
//20_10_17
//WhiteLabelDocId_PassportNumber_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PriceMarkUp = "select_Passenger_by_keys_view_20_10_17";

//U_K_S
//20_10_18
//WhiteLabelDocId_PassportNumber_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_10_18";

//U_K_T
//20_10_19
//WhiteLabelDocId_PassportNumber_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_OrdersDocId = "select_Passenger_by_keys_view_20_10_19";

//U_L_M
//20_11_12
//WhiteLabelDocId_PassportExpiryDate_PassportIssueCountry
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_PassportIssueCountry = "select_Passenger_by_keys_view_20_11_12";

//U_L_N
//20_11_13
//WhiteLabelDocId_PassportExpiryDate_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInTypeOutward = "select_Passenger_by_keys_view_20_11_13";

//U_L_O
//20_11_14
//WhiteLabelDocId_PassportExpiryDate_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInTypeReturn = "select_Passenger_by_keys_view_20_11_14";

//U_L_P
//20_11_15
//WhiteLabelDocId_PassportExpiryDate_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInPriceOutward = "select_Passenger_by_keys_view_20_11_15";

//U_L_Q
//20_11_16
//WhiteLabelDocId_PassportExpiryDate_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInPriceReturn = "select_Passenger_by_keys_view_20_11_16";

//U_L_R
//20_11_17
//WhiteLabelDocId_PassportExpiryDate_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_PriceMarkUp = "select_Passenger_by_keys_view_20_11_17";

//U_L_S
//20_11_18
//WhiteLabelDocId_PassportExpiryDate_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_11_18";

//U_L_T
//20_11_19
//WhiteLabelDocId_PassportExpiryDate_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_OrdersDocId = "select_Passenger_by_keys_view_20_11_19";

//U_M_N
//20_12_13
//WhiteLabelDocId_PassportIssueCountry_CheckInTypeOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInTypeOutward = "select_Passenger_by_keys_view_20_12_13";

//U_M_O
//20_12_14
//WhiteLabelDocId_PassportIssueCountry_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInTypeReturn = "select_Passenger_by_keys_view_20_12_14";

//U_M_P
//20_12_15
//WhiteLabelDocId_PassportIssueCountry_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInPriceOutward = "select_Passenger_by_keys_view_20_12_15";

//U_M_Q
//20_12_16
//WhiteLabelDocId_PassportIssueCountry_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInPriceReturn = "select_Passenger_by_keys_view_20_12_16";

//U_M_R
//20_12_17
//WhiteLabelDocId_PassportIssueCountry_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_PriceMarkUp = "select_Passenger_by_keys_view_20_12_17";

//U_M_S
//20_12_18
//WhiteLabelDocId_PassportIssueCountry_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_12_18";

//U_M_T
//20_12_19
//WhiteLabelDocId_PassportIssueCountry_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_OrdersDocId = "select_Passenger_by_keys_view_20_12_19";

//U_N_O
//20_13_14
//WhiteLabelDocId_CheckInTypeOutward_CheckInTypeReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInTypeReturn = "select_Passenger_by_keys_view_20_13_14";

//U_N_P
//20_13_15
//WhiteLabelDocId_CheckInTypeOutward_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceOutward = "select_Passenger_by_keys_view_20_13_15";

//U_N_Q
//20_13_16
//WhiteLabelDocId_CheckInTypeOutward_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceReturn = "select_Passenger_by_keys_view_20_13_16";

//U_N_R
//20_13_17
//WhiteLabelDocId_CheckInTypeOutward_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_PriceMarkUp = "select_Passenger_by_keys_view_20_13_17";

//U_N_S
//20_13_18
//WhiteLabelDocId_CheckInTypeOutward_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_13_18";

//U_N_T
//20_13_19
//WhiteLabelDocId_CheckInTypeOutward_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_OrdersDocId = "select_Passenger_by_keys_view_20_13_19";

//U_O_P
//20_14_15
//WhiteLabelDocId_CheckInTypeReturn_CheckInPriceOutward
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceOutward = "select_Passenger_by_keys_view_20_14_15";

//U_O_Q
//20_14_16
//WhiteLabelDocId_CheckInTypeReturn_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceReturn = "select_Passenger_by_keys_view_20_14_16";

//U_O_R
//20_14_17
//WhiteLabelDocId_CheckInTypeReturn_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_PriceMarkUp = "select_Passenger_by_keys_view_20_14_17";

//U_O_S
//20_14_18
//WhiteLabelDocId_CheckInTypeReturn_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_14_18";

//U_O_T
//20_14_19
//WhiteLabelDocId_CheckInTypeReturn_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_OrdersDocId = "select_Passenger_by_keys_view_20_14_19";

//U_P_Q
//20_15_16
//WhiteLabelDocId_CheckInPriceOutward_CheckInPriceReturn
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_CheckInPriceReturn = "select_Passenger_by_keys_view_20_15_16";

//U_P_R
//20_15_17
//WhiteLabelDocId_CheckInPriceOutward_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_PriceMarkUp = "select_Passenger_by_keys_view_20_15_17";

//U_P_S
//20_15_18
//WhiteLabelDocId_CheckInPriceOutward_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_15_18";

//U_P_T
//20_15_19
//WhiteLabelDocId_CheckInPriceOutward_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_OrdersDocId = "select_Passenger_by_keys_view_20_15_19";

//U_Q_R
//20_16_17
//WhiteLabelDocId_CheckInPriceReturn_PriceMarkUp
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_PriceMarkUp = "select_Passenger_by_keys_view_20_16_17";

//U_Q_S
//20_16_18
//WhiteLabelDocId_CheckInPriceReturn_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_16_18";

//U_Q_T
//20_16_19
//WhiteLabelDocId_CheckInPriceReturn_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_OrdersDocId = "select_Passenger_by_keys_view_20_16_19";

//U_R_S
//20_17_18
//WhiteLabelDocId_PriceMarkUp_SeatPerFlightLegDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp_SeatPerFlightLegDocId = "select_Passenger_by_keys_view_20_17_18";

//U_R_T
//20_17_19
//WhiteLabelDocId_PriceMarkUp_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp_OrdersDocId = "select_Passenger_by_keys_view_20_17_19";

//U_S_T
//20_18_19
//WhiteLabelDocId_SeatPerFlightLegDocId_OrdersDocId
public static readonly string  PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_SeatPerFlightLegDocId_OrdersDocId = "select_Passenger_by_keys_view_20_18_19";
         #endregion

    }

}
