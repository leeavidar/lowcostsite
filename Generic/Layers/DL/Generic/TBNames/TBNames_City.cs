

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_City
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertCity = PROC_Prefix + "save_city";
        #endregion
        #region Select
        public static readonly string PROC_Select_City_By_DocId = PROC_Prefix + "Select_city_By_doc_id";        
        public static readonly string PROC_Select_City_By_Keys_View = PROC_Prefix + "Select_city_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteCity = PROC_Prefix + "delete_city";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityIsActive);

        public static readonly string PRM_IataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityIataCode);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityName);

        public static readonly string PRM_CountryCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityCountryCode);

        public static readonly string PRM_EnglishName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityEnglishName);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "City.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityIsActive);

        public static readonly string Field_IataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityIataCode);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityName);

        public static readonly string Field_CountryCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityCountryCode);

        public static readonly string Field_EnglishName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyCityEnglishName);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_City_By_Keys_View_DateCreated = "select_City_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_City_By_Keys_View_DocId = "select_City_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_City_By_Keys_View_IsDeleted = "select_City_by_keys_view_2";

//E
//4
//IataCode
public static readonly string  PROC_Select_City_By_Keys_View_IataCode = "select_City_by_keys_view_4";

//F
//5
//Name
public static readonly string  PROC_Select_City_By_Keys_View_Name = "select_City_by_keys_view_5";

//G
//6
//CountryCode
public static readonly string  PROC_Select_City_By_Keys_View_CountryCode = "select_City_by_keys_view_6";

//H
//7
//EnglishName
public static readonly string  PROC_Select_City_By_Keys_View_EnglishName = "select_City_by_keys_view_7";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_City_By_Keys_View_DateCreated_DocId = "select_City_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_City_By_Keys_View_DateCreated_IsDeleted = "select_City_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_IataCode
public static readonly string  PROC_Select_City_By_Keys_View_DateCreated_IataCode = "select_City_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_Name
public static readonly string  PROC_Select_City_By_Keys_View_DateCreated_Name = "select_City_by_keys_view_0_5";

//A_G
//0_6
//DateCreated_CountryCode
public static readonly string  PROC_Select_City_By_Keys_View_DateCreated_CountryCode = "select_City_by_keys_view_0_6";

//A_H
//0_7
//DateCreated_EnglishName
public static readonly string  PROC_Select_City_By_Keys_View_DateCreated_EnglishName = "select_City_by_keys_view_0_7";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_City_By_Keys_View_DocId_IsDeleted = "select_City_by_keys_view_1_2";

//B_E
//1_4
//DocId_IataCode
public static readonly string  PROC_Select_City_By_Keys_View_DocId_IataCode = "select_City_by_keys_view_1_4";

//B_F
//1_5
//DocId_Name
public static readonly string  PROC_Select_City_By_Keys_View_DocId_Name = "select_City_by_keys_view_1_5";

//B_G
//1_6
//DocId_CountryCode
public static readonly string  PROC_Select_City_By_Keys_View_DocId_CountryCode = "select_City_by_keys_view_1_6";

//B_H
//1_7
//DocId_EnglishName
public static readonly string  PROC_Select_City_By_Keys_View_DocId_EnglishName = "select_City_by_keys_view_1_7";

//C_E
//2_4
//IsDeleted_IataCode
public static readonly string  PROC_Select_City_By_Keys_View_IsDeleted_IataCode = "select_City_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_Name
public static readonly string  PROC_Select_City_By_Keys_View_IsDeleted_Name = "select_City_by_keys_view_2_5";

//C_G
//2_6
//IsDeleted_CountryCode
public static readonly string  PROC_Select_City_By_Keys_View_IsDeleted_CountryCode = "select_City_by_keys_view_2_6";

//C_H
//2_7
//IsDeleted_EnglishName
public static readonly string  PROC_Select_City_By_Keys_View_IsDeleted_EnglishName = "select_City_by_keys_view_2_7";

//E_F
//4_5
//IataCode_Name
public static readonly string  PROC_Select_City_By_Keys_View_IataCode_Name = "select_City_by_keys_view_4_5";

//E_G
//4_6
//IataCode_CountryCode
public static readonly string  PROC_Select_City_By_Keys_View_IataCode_CountryCode = "select_City_by_keys_view_4_6";

//E_H
//4_7
//IataCode_EnglishName
public static readonly string  PROC_Select_City_By_Keys_View_IataCode_EnglishName = "select_City_by_keys_view_4_7";

//F_G
//5_6
//Name_CountryCode
public static readonly string  PROC_Select_City_By_Keys_View_Name_CountryCode = "select_City_by_keys_view_5_6";

//F_H
//5_7
//Name_EnglishName
public static readonly string  PROC_Select_City_By_Keys_View_Name_EnglishName = "select_City_by_keys_view_5_7";

//G_H
//6_7
//CountryCode_EnglishName
public static readonly string  PROC_Select_City_By_Keys_View_CountryCode_EnglishName = "select_City_by_keys_view_6_7";
         #endregion

    }

}
