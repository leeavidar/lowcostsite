

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_HomePageDestinationPage
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertHomePageDestinationPage = PROC_Prefix + "save_home_page_destination_page";
        #endregion
        #region Select
        public static readonly string PROC_Select_HomePageDestinationPage_By_DocId = PROC_Prefix + "Select_home_page_destination_page_By_doc_id";        
        public static readonly string PROC_Select_HomePageDestinationPage_By_Keys_View = PROC_Prefix + "Select_home_page_destination_page_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteHomePageDestinationPage = PROC_Prefix + "delete_home_page_destination_page";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageIsActive);

        public static readonly string PRM_HomePageDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDocId);

        public static readonly string PRM_Order = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageOrder);

        public static readonly string PRM_DestinationPageDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "HomePageDestinationPage.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageIsActive);

        public static readonly string Field_HomePageDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDocId);

        public static readonly string Field_Order = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDestinationPageOrder);

        public static readonly string Field_DestinationPageDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyDestinationPageDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//H_A
//7_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated = "select_HomePageDestinationPage_by_keys_view_7_0";

//H_B
//7_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId = "select_HomePageDestinationPage_by_keys_view_7_1";

//H_C
//7_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_HomePageDestinationPage_by_keys_view_7_2";

//H_E
//7_4
//WhiteLabelDocId_HomePageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId = "select_HomePageDestinationPage_by_keys_view_7_4";

//H_F
//7_5
//WhiteLabelDocId_Order
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_Order = "select_HomePageDestinationPage_by_keys_view_7_5";

//H_G
//7_6
//WhiteLabelDocId_DestinationPageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DestinationPageDocId = "select_HomePageDestinationPage_by_keys_view_7_6";

//H_A_B
//7_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_HomePageDestinationPage_by_keys_view_7_0_1";

//H_A_C
//7_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_HomePageDestinationPage_by_keys_view_7_0_2";

//H_A_E
//7_0_4
//WhiteLabelDocId_DateCreated_HomePageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_HomePageDocId = "select_HomePageDestinationPage_by_keys_view_7_0_4";

//H_A_F
//7_0_5
//WhiteLabelDocId_DateCreated_Order
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Order = "select_HomePageDestinationPage_by_keys_view_7_0_5";

//H_A_G
//7_0_6
//WhiteLabelDocId_DateCreated_DestinationPageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationPageDocId = "select_HomePageDestinationPage_by_keys_view_7_0_6";

//H_B_C
//7_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_HomePageDestinationPage_by_keys_view_7_1_2";

//H_B_E
//7_1_4
//WhiteLabelDocId_DocId_HomePageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_HomePageDocId = "select_HomePageDestinationPage_by_keys_view_7_1_4";

//H_B_F
//7_1_5
//WhiteLabelDocId_DocId_Order
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Order = "select_HomePageDestinationPage_by_keys_view_7_1_5";

//H_B_G
//7_1_6
//WhiteLabelDocId_DocId_DestinationPageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_DestinationPageDocId = "select_HomePageDestinationPage_by_keys_view_7_1_6";

//H_C_E
//7_2_4
//WhiteLabelDocId_IsDeleted_HomePageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_HomePageDocId = "select_HomePageDestinationPage_by_keys_view_7_2_4";

//H_C_F
//7_2_5
//WhiteLabelDocId_IsDeleted_Order
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Order = "select_HomePageDestinationPage_by_keys_view_7_2_5";

//H_C_G
//7_2_6
//WhiteLabelDocId_IsDeleted_DestinationPageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationPageDocId = "select_HomePageDestinationPage_by_keys_view_7_2_6";

//H_E_F
//7_4_5
//WhiteLabelDocId_HomePageDocId_Order
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId_Order = "select_HomePageDestinationPage_by_keys_view_7_4_5";

//H_E_G
//7_4_6
//WhiteLabelDocId_HomePageDocId_DestinationPageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId_DestinationPageDocId = "select_HomePageDestinationPage_by_keys_view_7_4_6";

//H_F_G
//7_5_6
//WhiteLabelDocId_Order_DestinationPageDocId
public static readonly string  PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_Order_DestinationPageDocId = "select_HomePageDestinationPage_by_keys_view_7_5_6";
         #endregion

    }

}
