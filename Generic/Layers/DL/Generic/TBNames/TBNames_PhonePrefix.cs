

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_PhonePrefix
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertPhonePrefix = PROC_Prefix + "save_phone_prefix";
        #endregion
        #region Select
        public static readonly string PROC_Select_PhonePrefix_By_DocId = PROC_Prefix + "Select_phone_prefix_By_doc_id";        
        public static readonly string PROC_Select_PhonePrefix_By_Keys_View = PROC_Prefix + "Select_phone_prefix_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeletePhonePrefix = PROC_Prefix + "delete_phone_prefix";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixIsActive);

        public static readonly string PRM_PrefixNumber = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixPrefixNumber);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "PhonePrefix.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixIsActive);

        public static readonly string Field_PrefixNumber = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPhonePrefixPrefixNumber);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_DateCreated = "select_PhonePrefix_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_DocId = "select_PhonePrefix_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_IsDeleted = "select_PhonePrefix_by_keys_view_2";

//E
//4
//PrefixNumber
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_PrefixNumber = "select_PhonePrefix_by_keys_view_4";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_DateCreated_DocId = "select_PhonePrefix_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_DateCreated_IsDeleted = "select_PhonePrefix_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_PrefixNumber
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_DateCreated_PrefixNumber = "select_PhonePrefix_by_keys_view_0_4";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_DocId_IsDeleted = "select_PhonePrefix_by_keys_view_1_2";

//B_E
//1_4
//DocId_PrefixNumber
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_DocId_PrefixNumber = "select_PhonePrefix_by_keys_view_1_4";

//C_E
//2_4
//IsDeleted_PrefixNumber
public static readonly string  PROC_Select_PhonePrefix_By_Keys_View_IsDeleted_PrefixNumber = "select_PhonePrefix_by_keys_view_2_4";
         #endregion

    }

}
