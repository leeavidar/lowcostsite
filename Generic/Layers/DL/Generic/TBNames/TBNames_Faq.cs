

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Faq
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertFaq = PROC_Prefix + "save_faq";
        #endregion
        #region Select
        public static readonly string PROC_Select_Faq_By_DocId = PROC_Prefix + "Select_faq_By_doc_id";        
        public static readonly string PROC_Select_Faq_By_Keys_View = PROC_Prefix + "Select_faq_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteFaq = PROC_Prefix + "delete_faq";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqIsActive);

        public static readonly string PRM_Category = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqCategory);

        public static readonly string PRM_Question = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqQuestion);

        public static readonly string PRM_Answer = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqAnswer);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_Order = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqOrder);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Faq.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqIsActive);

        public static readonly string Field_Category = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqCategory);

        public static readonly string Field_Question = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqQuestion);

        public static readonly string Field_Answer = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqAnswer);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_Order = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFaqOrder);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//H_A
//7_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated = "select_Faq_by_keys_view_7_0";

//H_B
//7_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId = "select_Faq_by_keys_view_7_1";

//H_C
//7_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_Faq_by_keys_view_7_2";

//H_E
//7_4
//WhiteLabelDocId_Category
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Category = "select_Faq_by_keys_view_7_4";

//H_F
//7_5
//WhiteLabelDocId_Question
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Question = "select_Faq_by_keys_view_7_5";

//H_G
//7_6
//WhiteLabelDocId_Answer
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Answer = "select_Faq_by_keys_view_7_6";

//H_I
//7_8
//WhiteLabelDocId_Order
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Order = "select_Faq_by_keys_view_7_8";

//H_A_B
//7_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_Faq_by_keys_view_7_0_1";

//H_A_C
//7_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_Faq_by_keys_view_7_0_2";

//H_A_E
//7_0_4
//WhiteLabelDocId_DateCreated_Category
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Category = "select_Faq_by_keys_view_7_0_4";

//H_A_F
//7_0_5
//WhiteLabelDocId_DateCreated_Question
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Question = "select_Faq_by_keys_view_7_0_5";

//H_A_G
//7_0_6
//WhiteLabelDocId_DateCreated_Answer
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Answer = "select_Faq_by_keys_view_7_0_6";

//H_A_I
//7_0_8
//WhiteLabelDocId_DateCreated_Order
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Order = "select_Faq_by_keys_view_7_0_8";

//H_B_C
//7_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_Faq_by_keys_view_7_1_2";

//H_B_E
//7_1_4
//WhiteLabelDocId_DocId_Category
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Category = "select_Faq_by_keys_view_7_1_4";

//H_B_F
//7_1_5
//WhiteLabelDocId_DocId_Question
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Question = "select_Faq_by_keys_view_7_1_5";

//H_B_G
//7_1_6
//WhiteLabelDocId_DocId_Answer
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Answer = "select_Faq_by_keys_view_7_1_6";

//H_B_I
//7_1_8
//WhiteLabelDocId_DocId_Order
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Order = "select_Faq_by_keys_view_7_1_8";

//H_C_E
//7_2_4
//WhiteLabelDocId_IsDeleted_Category
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Category = "select_Faq_by_keys_view_7_2_4";

//H_C_F
//7_2_5
//WhiteLabelDocId_IsDeleted_Question
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Question = "select_Faq_by_keys_view_7_2_5";

//H_C_G
//7_2_6
//WhiteLabelDocId_IsDeleted_Answer
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Answer = "select_Faq_by_keys_view_7_2_6";

//H_C_I
//7_2_8
//WhiteLabelDocId_IsDeleted_Order
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Order = "select_Faq_by_keys_view_7_2_8";

//H_E_F
//7_4_5
//WhiteLabelDocId_Category_Question
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Question = "select_Faq_by_keys_view_7_4_5";

//H_E_G
//7_4_6
//WhiteLabelDocId_Category_Answer
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Answer = "select_Faq_by_keys_view_7_4_6";

//H_E_I
//7_4_8
//WhiteLabelDocId_Category_Order
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Order = "select_Faq_by_keys_view_7_4_8";

//H_F_G
//7_5_6
//WhiteLabelDocId_Question_Answer
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Question_Answer = "select_Faq_by_keys_view_7_5_6";

//H_F_I
//7_5_8
//WhiteLabelDocId_Question_Order
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Question_Order = "select_Faq_by_keys_view_7_5_8";

//H_G_I
//7_6_8
//WhiteLabelDocId_Answer_Order
public static readonly string  PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Answer_Order = "select_Faq_by_keys_view_7_6_8";
         #endregion

    }

}
