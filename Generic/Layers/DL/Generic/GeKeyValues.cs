

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public enum KeyValuesType
    {
        None = 0,

    #region ContactDetails
    KeyContactDetailsDateCreated,
    KeyContactDetailsDocId,
    KeyContactDetailsIsDeleted,
    KeyContactDetailsIsActive,
    KeyContactDetailsFirstName,
    KeyContactDetailsLastName,
    KeyContactDetailsCompany,
    KeyContactDetailsFlat,
    KeyContactDetailsBuildingName,
    KeyContactDetailsBuildingNumber,
    KeyContactDetailsStreet,
    KeyContactDetailsLocality,
    KeyContactDetailsCity,
    KeyContactDetailsProvince,
    KeyContactDetailsZipCode,
    KeyContactDetailsCountryCode,
    KeyContactDetailsEmail,
    KeyContactDetailsMainPhone,
    KeyContactDetailsMobliePhone,

//FK     KeyOrdersDocId

//FK     KeyWhiteLabelDocId
    KeyContactDetailsTitle,

    #endregion

    #region Passenger
    KeyPassengerDateCreated,
    KeyPassengerDocId,
    KeyPassengerIsDeleted,
    KeyPassengerIsActive,
    KeyPassengerNetPrice,
    KeyPassengerFirstName,
    KeyPassengerLastName,
    KeyPassengerDateOfBirth,
    KeyPassengerGender,
    KeyPassengerTitle,
    KeyPassengerPassportNumber,
    KeyPassengerPassportExpiryDate,
    KeyPassengerPassportIssueCountry,
    KeyPassengerCheckInTypeOutward,
    KeyPassengerCheckInTypeReturn,
    KeyPassengerCheckInPriceOutward,
    KeyPassengerCheckInPriceReturn,
    KeyPassengerPriceMarkUp,

//FK     KeySeatPerFlightLegDocId

//FK     KeyOrdersDocId

//FK     KeyWhiteLabelDocId

    #endregion

    #region SeatPerFlightLeg
    KeySeatPerFlightLegDateCreated,
    KeySeatPerFlightLegDocId,
    KeySeatPerFlightLegIsDeleted,
    KeySeatPerFlightLegIsActive,
    KeySeatPerFlightLegPassengerId,
    KeySeatPerFlightLegSeat,
    KeySeatPerFlightLegFlightNumber,

//FK     KeyPassengerDocId

//FK     KeyWhiteLabelDocId

//FK     KeyFlightLegDocId

    #endregion

    #region Luggage
    KeyLuggageDateCreated,
    KeyLuggageDocId,
    KeyLuggageIsDeleted,
    KeyLuggageIsActive,
    KeyLuggagePrice,
    KeyLuggageQuantity,
    KeyLuggageTotalWeight,
    KeyLuggageFlightType,

//FK     KeyPassengerDocId

//FK     KeyWhiteLabelDocId

    #endregion

    #region FlightLeg
    KeyFlightLegDateCreated,
    KeyFlightLegDocId,
    KeyFlightLegIsDeleted,
    KeyFlightLegIsActive,
    KeyFlightLegFlightNumber,
    KeyFlightLegDepartureDateTime,
    KeyFlightLegArrivalDateTime,
    KeyFlightLegStatus,
    KeyFlightLegDuration,
    KeyFlightLegHops,
    KeyFlightLegAirlineIataCode,
    KeyFlightLegOriginIataCode,
    KeyFlightLegDestinationIataCode,

//FK     KeyOrderFlightLegDocId

//FK     KeyWhiteLabelDocId

    #endregion

    #region Airline
    KeyAirlineDateCreated,
    KeyAirlineDocId,
    KeyAirlineIsDeleted,
    KeyAirlineIsActive,
    KeyAirlineIataCode,
    KeyAirlineName,

    #endregion

    #region PopularDestination
    KeyPopularDestinationDateCreated,
    KeyPopularDestinationDocId,
    KeyPopularDestinationIsDeleted,
    KeyPopularDestinationIsActive,
    KeyPopularDestinationIataCode,
    KeyPopularDestinationCityIataCode,
    KeyPopularDestinationName,

//FK     KeyWhiteLabelDocId

    #endregion

    #region Airport
    KeyAirportDateCreated,
    KeyAirportDocId,
    KeyAirportIsDeleted,
    KeyAirportIsActive,
    KeyAirportIataCode,
    KeyAirportCityIataCode,
    KeyAirportEnglishName,
    KeyAirportNameByLang,

    #endregion

    #region City
    KeyCityDateCreated,
    KeyCityDocId,
    KeyCityIsDeleted,
    KeyCityIsActive,
    KeyCityIataCode,
    KeyCityName,
    KeyCityCountryCode,
    KeyCityEnglishName,

    #endregion

    #region Country
    KeyCountryDateCreated,
    KeyCountryDocId,
    KeyCountryIsDeleted,
    KeyCountryIsActive,
    KeyCountryCountryCode,
    KeyCountryName,
    KeyCountryEnglishName,

    #endregion

    #region DestinationPage
    KeyDestinationPageDateCreated,
    KeyDestinationPageDocId,
    KeyDestinationPageIsDeleted,
    KeyDestinationPageIsActive,
    KeyDestinationPageOrder,
    KeyDestinationPageDestinationIataCode,

//FK     KeyWhiteLabelDocId

//FK     KeySeoDocId
    KeyDestinationPagePrice,
    KeyDestinationPageLocalCurrency,
    KeyDestinationPageName,
    KeyDestinationPageTime,
    KeyDestinationPageTitle,
    KeyDestinationPagePriceCurrency,

    #endregion

    #region OrderFlightLeg
    KeyOrderFlightLegDateCreated,
    KeyOrderFlightLegDocId,
    KeyOrderFlightLegIsDeleted,
    KeyOrderFlightLegIsActive,
    KeyOrderFlightLegLowCostPNR,
    KeyOrderFlightLegPriceNet,
    KeyOrderFlightLegPriceMarkUp,
    KeyOrderFlightLegFlightType,

//FK     KeyWhiteLabelDocId

//FK     KeyOrdersDocId

    #endregion

    #region Orders
    KeyOrdersDateCreated,
    KeyOrdersDocId,
    KeyOrdersIsDeleted,
    KeyOrdersIsActive,
    KeyOrdersLowCostPNR,
    KeyOrdersSupplierPNR,
    KeyOrdersTotalPrice,
    KeyOrdersDateOfBook,
    KeyOrdersReceiveEmail,
    KeyOrdersReceiveSms,
    KeyOrdersMarketingCabin,

//FK     KeyContactDetailsDocId

//FK     KeyWhiteLabelDocId
    KeyOrdersAirlineIataCode,
    KeyOrdersOrderStatus,
    KeyOrdersAdminRemarks,
    KeyOrdersStatusPayment,
    KeyOrdersOrderToken,
    KeyOrdersPaymentTransaction,
    KeyOrdersXmlBookRequest,
    KeyOrdersXmlBookResponse,
    KeyOrdersOrderRemarks,
    KeyOrdersOrderPayment,
    KeyOrdersPaymentToken,
    KeyOrdersPaymentRemarks,
    KeyOrdersCurrency,

    #endregion

    #region Images
    KeyImagesDateCreated,
    KeyImagesDocId,
    KeyImagesIsDeleted,
    KeyImagesIsActive,
    KeyImagesImageFileName,
    KeyImagesRelatedObjectDocId,
    KeyImagesImageType,
    KeyImagesRelatedObjectType,
    KeyImagesTitle,
    KeyImagesAlt,

    #endregion

    #region Messages
    KeyMessagesDateCreated,
    KeyMessagesDocId,
    KeyMessagesIsDeleted,
    KeyMessagesIsActive,
    KeyMessagesSentDateTime,
    KeyMessagesMessageType,
    KeyMessagesLowCostPNR,

//FK     KeyOrdersDocId
    KeyMessagesContent,

//FK     KeyWhiteLabelDocId

    #endregion

    #region Stamps
    KeyStampsDateCreated,
    KeyStampsDocId,
    KeyStampsIsDeleted,
    KeyStampsIsActive,
    KeyStampsText,
    KeyStampsType,

    #endregion

    #region Stamps_PopularDestination
    KeyStamps_PopularDestinationDateCreated,
    KeyStamps_PopularDestinationDocId,
    KeyStamps_PopularDestinationIsDeleted,
    KeyStamps_PopularDestinationIsActive,

//FK     KeyPopularDestinationDocId

//FK     KeyStampsDocId

    #endregion

    #region WhiteLabel
    KeyWhiteLabelDateCreated,
    KeyWhiteLabelDocId,
    KeyWhiteLabelIsDeleted,
    KeyWhiteLabelIsActive,
    KeyWhiteLabelDomain,
    KeyWhiteLabelName,
    KeyWhiteLabelLanguagesDocId,
    KeyWhiteLabelPhone,
    KeyWhiteLabelEmail,
    KeyWhiteLabelDiscountType,
    KeyWhiteLabelAddress,
    KeyWhiteLabelDiscount,

    #endregion

    #region Seo
    KeySeoDateCreated,
    KeySeoDocId,
    KeySeoIsDeleted,
    KeySeoIsActive,
    KeySeoFriendlyUrl,
    KeySeoSeoTitle,
    KeySeoSeoDescription,
    KeySeoSeoKeyWords,
    KeySeoRelatedObject,

//FK     KeyWhiteLabelDocId

    #endregion

    #region IndexPagePromo
    KeyIndexPagePromoDateCreated,
    KeyIndexPagePromoDocId,
    KeyIndexPagePromoIsDeleted,
    KeyIndexPagePromoIsActive,
    KeyIndexPagePromoImagesDocId,
    KeyIndexPagePromoTitle,
    KeyIndexPagePromoText,
    KeyIndexPagePromoLink,
    KeyIndexPagePromoPromoType,
    KeyIndexPagePromoOrder,

//FK     KeyWhiteLabelDocId
    KeyIndexPagePromoPriceAfter,
    KeyIndexPagePromoPriceBefore,

    #endregion

    #region StaticPages
    KeyStaticPagesDateCreated,
    KeyStaticPagesDocId,
    KeyStaticPagesIsDeleted,
    KeyStaticPagesIsActive,
    KeyStaticPagesFriendlyUrl,
    KeyStaticPagesText,
    KeyStaticPagesName,
    KeyStaticPagesType,

//FK     KeySeoDocId

//FK     KeyWhiteLabelDocId

    #endregion

    #region Stops
    KeyStopsDateCreated,
    KeyStopsDocId,
    KeyStopsIsDeleted,
    KeyStopsIsActive,
    KeyStopsArrival,
    KeyStopsDeparture,

//FK     KeyFlightLegDocId

//FK     KeyAirportIataCode

    #endregion

    #region Template
    KeyTemplateDateCreated,
    KeyTemplateDocId,
    KeyTemplateIsDeleted,
    KeyTemplateIsActive,
    KeyTemplateTitle,
    KeyTemplateSubTitle,
    KeyTemplateType,
    KeyTemplateOrder,

//FK     KeyWhiteLabelDocId
    KeyTemplateRelatedObjectType,
    KeyTemplateRelatedObjectDocId,
    KeyTemplateTextLong,
    KeyTemplateTextShort,

    #endregion

    #region SubBox
    KeySubBoxDateCreated,
    KeySubBoxDocId,
    KeySubBoxIsDeleted,
    KeySubBoxIsActive,
    KeySubBoxSubTitle,
    KeySubBoxShortText,
    KeySubBoxLongText,
    KeySubBoxOrder,

//FK     KeyTemplateDocId

//FK     KeyWhiteLabelDocId

    #endregion

    #region Currency
    KeyCurrencyDateCreated,
    KeyCurrencyDocId,
    KeyCurrencyIsDeleted,
    KeyCurrencyIsActive,
    KeyCurrencyName,
    KeyCurrencyCode,
    KeyCurrencySign,

    #endregion

    #region CompanyPage
    KeyCompanyPageDateCreated,
    KeyCompanyPageDocId,
    KeyCompanyPageIsDeleted,
    KeyCompanyPageIsActive,
    KeyCompanyPageName,

//FK     KeySeoDocId

//FK     KeyWhiteLabelDocId
    KeyCompanyPageText,
    KeyCompanyPageTitle,

//FK     KeyAirlineDocId

    #endregion

    #region Company
    KeyCompanyDateCreated,
    KeyCompanyDocId,
    KeyCompanyIsDeleted,
    KeyCompanyIsActive,
    KeyCompanyName,

//FK     KeyWhiteLabelDocId

    #endregion

    #region Tip
    KeyTipDateCreated,
    KeyTipDocId,
    KeyTipIsDeleted,
    KeyTipIsActive,

//FK     KeyWhiteLabelDocId
    KeyTipTitle,
    KeyTipShortText,
    KeyTipLongText,
    KeyTipOrder,
    KeyTipDate,

    #endregion

    #region Users
    KeyUsersDateCreated,
    KeyUsersDocId,
    KeyUsersIsDeleted,
    KeyUsersIsActive,
    KeyUsersUserName,
    KeyUsersPassword,
    KeyUsersEmail,
    KeyUsersFirstName,
    KeyUsersLastName,
    KeyUsersRole,

//FK     KeyWhiteLabelDocId

    #endregion

    #region HomePage
    KeyHomePageDateCreated,
    KeyHomePageDocId,
    KeyHomePageIsDeleted,
    KeyHomePageIsActive,
    KeyHomePageTextLine1,
    KeyHomePageTextLine2,
    KeyHomePageTextLine3,
    KeyHomePageTextLine4,

//FK     KeyWhiteLabelDocId

//FK     KeySeoDocId

//FK     KeyTipDocId

    #endregion

    #region HomePageDestinationPage
    KeyHomePageDestinationPageDateCreated,
    KeyHomePageDestinationPageDocId,
    KeyHomePageDestinationPageIsDeleted,
    KeyHomePageDestinationPageIsActive,

//FK     KeyHomePageDocId
    KeyHomePageDestinationPageOrder,

//FK     KeyDestinationPageDocId

//FK     KeyWhiteLabelDocId

    #endregion

    #region Faq
    KeyFaqDateCreated,
    KeyFaqDocId,
    KeyFaqIsDeleted,
    KeyFaqIsActive,
    KeyFaqCategory,
    KeyFaqQuestion,
    KeyFaqAnswer,

//FK     KeyWhiteLabelDocId
    KeyFaqOrder,

    #endregion

    #region MailAddress
    KeyMailAddressDateCreated,
    KeyMailAddressDocId,
    KeyMailAddressIsDeleted,
    KeyMailAddressIsActive,
    KeyMailAddressAddress,

//FK     KeyWhiteLabelDocId

    #endregion

    #region PhonePrefix
    KeyPhonePrefixDateCreated,
    KeyPhonePrefixDocId,
    KeyPhonePrefixIsDeleted,
    KeyPhonePrefixIsActive,
    KeyPhonePrefixPrefixNumber,

    #endregion

    #region OrderLog
    KeyOrderLogDateCreated,
    KeyOrderLogDocId,
    KeyOrderLogIsDeleted,
    KeyOrderLogIsActive,
    KeyOrderLogType,
    KeyOrderLogStatus,
    KeyOrderLogXML,

//FK     KeyWhiteLabelDocId

//FK     KeyOrdersDocId
    KeyOrderLogOrderStage,

    #endregion

    #region Languages
    KeyLanguagesDateCreated,
    KeyLanguagesDocId,
    KeyLanguagesIsDeleted,
    KeyLanguagesIsActive,
    KeyLanguagesName,
    KeyLanguagesCode,

    #endregion

    #region AirlineFare
    KeyAirlineFareDateCreated,
    KeyAirlineFareDocId,
    KeyAirlineFareIsDeleted,
    KeyAirlineFareIsActive,
    KeyAirlineFareAirlineCode,
    KeyAirlineFareName,
    KeyAirlineFareStandard,
    KeyAirlineFareFlexible,

//FK     KeyWhiteLabelDocId

    #endregion

    }


    #region KeyFactory
    public static class FactoryKeyValuesContainer
    {

    #region ContactDetails
    public static KeyValuesContainer<ContactDetails> ContactDetailsKeys()
    {
        KeyValuesContainer<ContactDetails> oKvContainer = new KeyValuesContainer<ContactDetails>();
        oKvContainer.Add(KeyValuesType.KeyContactDetailsDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsFirstName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsLastName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsCompany, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsFlat, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsBuildingName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsBuildingNumber, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsStreet, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsLocality, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsCity, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsProvince, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsZipCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsCountryCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsEmail, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsMainPhone, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsMobliePhone, false, true, false,false,false);

//FK KeyContactDetailsOrdersDocId
        oKvContainer.Add(KeyValuesType.KeyOrdersDocId, false, true, false,false,true);

//FK KeyContactDetailsWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyContactDetailsTitle, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region Passenger
    public static KeyValuesContainer<Passenger> PassengerKeys()
    {
        KeyValuesContainer<Passenger> oKvContainer = new KeyValuesContainer<Passenger>();
        oKvContainer.Add(KeyValuesType.KeyPassengerDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerNetPrice, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerFirstName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerLastName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerDateOfBirth, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerGender, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerTitle, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerPassportNumber, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerPassportExpiryDate, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerPassportIssueCountry, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerCheckInTypeOutward, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerCheckInTypeReturn, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerCheckInPriceOutward, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerCheckInPriceReturn, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPassengerPriceMarkUp, false, true, false,false,false);

//FK KeyPassengerSeatPerFlightLegDocId
        oKvContainer.Add(KeyValuesType.KeySeatPerFlightLegDocId, false, true, false,false,true);

//FK KeyPassengerOrdersDocId
        oKvContainer.Add(KeyValuesType.KeyOrdersDocId, false, true, false,false,true);

//FK KeyPassengerWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region SeatPerFlightLeg
    public static KeyValuesContainer<SeatPerFlightLeg> SeatPerFlightLegKeys()
    {
        KeyValuesContainer<SeatPerFlightLeg> oKvContainer = new KeyValuesContainer<SeatPerFlightLeg>();
        oKvContainer.Add(KeyValuesType.KeySeatPerFlightLegDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeatPerFlightLegDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeySeatPerFlightLegIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeatPerFlightLegIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeatPerFlightLegPassengerId, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeatPerFlightLegSeat, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeatPerFlightLegFlightNumber, false, true, false,false,false);

//FK KeySeatPerFlightLegPassengerDocId
        oKvContainer.Add(KeyValuesType.KeyPassengerDocId, false, true, false,false,true);

//FK KeySeatPerFlightLegWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);

//FK KeySeatPerFlightLegFlightLegDocId
        oKvContainer.Add(KeyValuesType.KeyFlightLegDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Luggage
    public static KeyValuesContainer<Luggage> LuggageKeys()
    {
        KeyValuesContainer<Luggage> oKvContainer = new KeyValuesContainer<Luggage>();
        oKvContainer.Add(KeyValuesType.KeyLuggageDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLuggageDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyLuggageIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLuggageIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLuggagePrice, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLuggageQuantity, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLuggageTotalWeight, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLuggageFlightType, false, true, false,false,false);

//FK KeyLuggagePassengerDocId
        oKvContainer.Add(KeyValuesType.KeyPassengerDocId, false, true, false,false,true);

//FK KeyLuggageWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region FlightLeg
    public static KeyValuesContainer<FlightLeg> FlightLegKeys()
    {
        KeyValuesContainer<FlightLeg> oKvContainer = new KeyValuesContainer<FlightLeg>();
        oKvContainer.Add(KeyValuesType.KeyFlightLegDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegFlightNumber, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegDepartureDateTime, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegArrivalDateTime, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegStatus, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegDuration, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegHops, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegAirlineIataCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegOriginIataCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFlightLegDestinationIataCode, false, true, false,false,false);

//FK KeyFlightLegOrderFlightLegDocId
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegDocId, false, true, false,false,true);

//FK KeyFlightLegWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Airline
    public static KeyValuesContainer<Airline> AirlineKeys()
    {
        KeyValuesContainer<Airline> oKvContainer = new KeyValuesContainer<Airline>();
        oKvContainer.Add(KeyValuesType.KeyAirlineDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineIataCode, true, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineName, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region PopularDestination
    public static KeyValuesContainer<PopularDestination> PopularDestinationKeys()
    {
        KeyValuesContainer<PopularDestination> oKvContainer = new KeyValuesContainer<PopularDestination>();
        oKvContainer.Add(KeyValuesType.KeyPopularDestinationDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPopularDestinationDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyPopularDestinationIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPopularDestinationIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPopularDestinationIataCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPopularDestinationCityIataCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPopularDestinationName, false, true, false,true,false);

//FK KeyPopularDestinationWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Airport
    public static KeyValuesContainer<Airport> AirportKeys()
    {
        KeyValuesContainer<Airport> oKvContainer = new KeyValuesContainer<Airport>();
        oKvContainer.Add(KeyValuesType.KeyAirportDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirportDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirportIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirportIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirportIataCode, true, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirportCityIataCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirportEnglishName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirportNameByLang, false, true, false,true,false);
    
    return oKvContainer;
    }
    #endregion

    #region City
    public static KeyValuesContainer<City> CityKeys()
    {
        KeyValuesContainer<City> oKvContainer = new KeyValuesContainer<City>();
        oKvContainer.Add(KeyValuesType.KeyCityDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCityDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyCityIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCityIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCityIataCode, true, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCityName, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyCityCountryCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCityEnglishName, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region Country
    public static KeyValuesContainer<Country> CountryKeys()
    {
        KeyValuesContainer<Country> oKvContainer = new KeyValuesContainer<Country>();
        oKvContainer.Add(KeyValuesType.KeyCountryDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCountryDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyCountryIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCountryIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCountryCountryCode, true, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCountryName, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyCountryEnglishName, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region DestinationPage
    public static KeyValuesContainer<DestinationPage> DestinationPageKeys()
    {
        KeyValuesContainer<DestinationPage> oKvContainer = new KeyValuesContainer<DestinationPage>();
        oKvContainer.Add(KeyValuesType.KeyDestinationPageDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageOrder, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageDestinationIataCode, false, true, false,false,false);

//FK KeyDestinationPageWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);

//FK KeyDestinationPageSeoDocId
        oKvContainer.Add(KeyValuesType.KeySeoDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyDestinationPagePrice, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageLocalCurrency, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageName, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageTime, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPageTitle, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyDestinationPagePriceCurrency, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region OrderFlightLeg
    public static KeyValuesContainer<OrderFlightLeg> OrderFlightLegKeys()
    {
        KeyValuesContainer<OrderFlightLeg> oKvContainer = new KeyValuesContainer<OrderFlightLeg>();
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegLowCostPNR, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegPriceNet, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegPriceMarkUp, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderFlightLegFlightType, false, true, false,false,false);

//FK KeyOrderFlightLegWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);

//FK KeyOrderFlightLegOrdersDocId
        oKvContainer.Add(KeyValuesType.KeyOrdersDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Orders
    public static KeyValuesContainer<Orders> OrdersKeys()
    {
        KeyValuesContainer<Orders> oKvContainer = new KeyValuesContainer<Orders>();
        oKvContainer.Add(KeyValuesType.KeyOrdersDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersLowCostPNR, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersSupplierPNR, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersTotalPrice, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersDateOfBook, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersReceiveEmail, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersReceiveSms, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersMarketingCabin, false, true, false,false,false);

//FK KeyOrdersContactDetailsDocId
        oKvContainer.Add(KeyValuesType.KeyContactDetailsDocId, false, true, false,false,true);

//FK KeyOrdersWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyOrdersAirlineIataCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersOrderStatus, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersAdminRemarks, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersStatusPayment, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersOrderToken, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersPaymentTransaction, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersXmlBookRequest, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersXmlBookResponse, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersOrderRemarks, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersOrderPayment, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersPaymentToken, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersPaymentRemarks, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrdersCurrency, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region Images
    public static KeyValuesContainer<Images> ImagesKeys()
    {
        KeyValuesContainer<Images> oKvContainer = new KeyValuesContainer<Images>();
        oKvContainer.Add(KeyValuesType.KeyImagesDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyImagesDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyImagesIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyImagesIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyImagesImageFileName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyImagesRelatedObjectDocId, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyImagesImageType, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyImagesRelatedObjectType, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyImagesTitle, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyImagesAlt, false, true, false,true,false);
    
    return oKvContainer;
    }
    #endregion

    #region Messages
    public static KeyValuesContainer<Messages> MessagesKeys()
    {
        KeyValuesContainer<Messages> oKvContainer = new KeyValuesContainer<Messages>();
        oKvContainer.Add(KeyValuesType.KeyMessagesDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyMessagesDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyMessagesIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyMessagesIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyMessagesSentDateTime, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyMessagesMessageType, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyMessagesLowCostPNR, false, true, false,false,false);

//FK KeyMessagesOrdersDocId
        oKvContainer.Add(KeyValuesType.KeyOrdersDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyMessagesContent, false, true, false,false,false);

//FK KeyMessagesWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Stamps
    public static KeyValuesContainer<Stamps> StampsKeys()
    {
        KeyValuesContainer<Stamps> oKvContainer = new KeyValuesContainer<Stamps>();
        oKvContainer.Add(KeyValuesType.KeyStampsDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStampsDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyStampsIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStampsIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStampsText, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyStampsType, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region Stamps_PopularDestination
    public static KeyValuesContainer<Stamps_PopularDestination> Stamps_PopularDestinationKeys()
    {
        KeyValuesContainer<Stamps_PopularDestination> oKvContainer = new KeyValuesContainer<Stamps_PopularDestination>();
        oKvContainer.Add(KeyValuesType.KeyStamps_PopularDestinationDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStamps_PopularDestinationDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyStamps_PopularDestinationIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStamps_PopularDestinationIsActive, false, true, false,false,false);

//FK KeyStamps_PopularDestinationPopularDestinationDocId
        oKvContainer.Add(KeyValuesType.KeyPopularDestinationDocId, false, true, false,false,true);

//FK KeyStamps_PopularDestinationStampsDocId
        oKvContainer.Add(KeyValuesType.KeyStampsDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region WhiteLabel
    public static KeyValuesContainer<WhiteLabel> WhiteLabelKeys()
    {
        KeyValuesContainer<WhiteLabel> oKvContainer = new KeyValuesContainer<WhiteLabel>();
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDomain, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelName, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelLanguagesDocId, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelPhone, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelEmail, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDiscountType, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelAddress, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDiscount, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region Seo
    public static KeyValuesContainer<Seo> SeoKeys()
    {
        KeyValuesContainer<Seo> oKvContainer = new KeyValuesContainer<Seo>();
        oKvContainer.Add(KeyValuesType.KeySeoDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeoDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeySeoIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeoIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeoFriendlyUrl, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySeoSeoTitle, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeySeoSeoDescription, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeySeoSeoKeyWords, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeySeoRelatedObject, false, true, false,false,false);

//FK KeySeoWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region IndexPagePromo
    public static KeyValuesContainer<IndexPagePromo> IndexPagePromoKeys()
    {
        KeyValuesContainer<IndexPagePromo> oKvContainer = new KeyValuesContainer<IndexPagePromo>();
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoImagesDocId, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoTitle, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoText, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoLink, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoPromoType, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoOrder, false, true, false,false,false);

//FK KeyIndexPagePromoWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoPriceAfter, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyIndexPagePromoPriceBefore, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region StaticPages
    public static KeyValuesContainer<StaticPages> StaticPagesKeys()
    {
        KeyValuesContainer<StaticPages> oKvContainer = new KeyValuesContainer<StaticPages>();
        oKvContainer.Add(KeyValuesType.KeyStaticPagesDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStaticPagesDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyStaticPagesIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStaticPagesIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStaticPagesFriendlyUrl, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyStaticPagesText, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyStaticPagesName, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyStaticPagesType, false, true, false,false,false);

//FK KeyStaticPagesSeoDocId
        oKvContainer.Add(KeyValuesType.KeySeoDocId, false, true, false,false,true);

//FK KeyStaticPagesWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Stops
    public static KeyValuesContainer<Stops> StopsKeys()
    {
        KeyValuesContainer<Stops> oKvContainer = new KeyValuesContainer<Stops>();
        oKvContainer.Add(KeyValuesType.KeyStopsDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStopsDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyStopsIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStopsIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStopsArrival, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyStopsDeparture, false, true, false,false,false);

//FK KeyStopsFlightLegDocId
        oKvContainer.Add(KeyValuesType.KeyFlightLegDocId, false, true, false,false,true);

//FK KeyStopsAirportIataCode
        oKvContainer.Add(KeyValuesType.KeyAirportIataCode, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Template
    public static KeyValuesContainer<Template> TemplateKeys()
    {
        KeyValuesContainer<Template> oKvContainer = new KeyValuesContainer<Template>();
        oKvContainer.Add(KeyValuesType.KeyTemplateDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateTitle, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateSubTitle, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateType, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateOrder, false, true, false,false,false);

//FK KeyTemplateWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyTemplateRelatedObjectType, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateRelatedObjectDocId, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateTextLong, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyTemplateTextShort, false, true, false,true,false);
    
    return oKvContainer;
    }
    #endregion

    #region SubBox
    public static KeyValuesContainer<SubBox> SubBoxKeys()
    {
        KeyValuesContainer<SubBox> oKvContainer = new KeyValuesContainer<SubBox>();
        oKvContainer.Add(KeyValuesType.KeySubBoxDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySubBoxDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeySubBoxIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySubBoxIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeySubBoxSubTitle, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeySubBoxShortText, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeySubBoxLongText, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeySubBoxOrder, false, true, false,false,false);

//FK KeySubBoxTemplateDocId
        oKvContainer.Add(KeyValuesType.KeyTemplateDocId, false, true, false,false,true);

//FK KeySubBoxWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Currency
    public static KeyValuesContainer<Currency> CurrencyKeys()
    {
        KeyValuesContainer<Currency> oKvContainer = new KeyValuesContainer<Currency>();
        oKvContainer.Add(KeyValuesType.KeyCurrencyDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCurrencyDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyCurrencyIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCurrencyIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCurrencyName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCurrencyCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCurrencySign, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region CompanyPage
    public static KeyValuesContainer<CompanyPage> CompanyPageKeys()
    {
        KeyValuesContainer<CompanyPage> oKvContainer = new KeyValuesContainer<CompanyPage>();
        oKvContainer.Add(KeyValuesType.KeyCompanyPageDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyPageDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyPageIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyPageIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyPageName, false, true, false,false,false);

//FK KeyCompanyPageSeoDocId
        oKvContainer.Add(KeyValuesType.KeySeoDocId, false, true, false,false,true);

//FK KeyCompanyPageWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyCompanyPageText, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyPageTitle, false, true, false,true,false);

//FK KeyCompanyPageAirlineDocId
        oKvContainer.Add(KeyValuesType.KeyAirlineDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Company
    public static KeyValuesContainer<Company> CompanyKeys()
    {
        KeyValuesContainer<Company> oKvContainer = new KeyValuesContainer<Company>();
        oKvContainer.Add(KeyValuesType.KeyCompanyDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyCompanyName, false, true, false,false,false);

//FK KeyCompanyWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Tip
    public static KeyValuesContainer<Tip> TipKeys()
    {
        KeyValuesContainer<Tip> oKvContainer = new KeyValuesContainer<Tip>();
        oKvContainer.Add(KeyValuesType.KeyTipDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTipDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyTipIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTipIsActive, false, true, false,false,false);

//FK KeyTipWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyTipTitle, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyTipShortText, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyTipLongText, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyTipOrder, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyTipDate, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region Users
    public static KeyValuesContainer<Users> UsersKeys()
    {
        KeyValuesContainer<Users> oKvContainer = new KeyValuesContainer<Users>();
        oKvContainer.Add(KeyValuesType.KeyUsersDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersUserName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersPassword, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersEmail, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersFirstName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersLastName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyUsersRole, false, true, false,false,false);

//FK KeyUsersWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region HomePage
    public static KeyValuesContainer<HomePage> HomePageKeys()
    {
        KeyValuesContainer<HomePage> oKvContainer = new KeyValuesContainer<HomePage>();
        oKvContainer.Add(KeyValuesType.KeyHomePageDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageTextLine1, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageTextLine2, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageTextLine3, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageTextLine4, false, true, false,true,false);

//FK KeyHomePageWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);

//FK KeyHomePageSeoDocId
        oKvContainer.Add(KeyValuesType.KeySeoDocId, false, true, false,false,true);

//FK KeyHomePageTipDocId
        oKvContainer.Add(KeyValuesType.KeyTipDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region HomePageDestinationPage
    public static KeyValuesContainer<HomePageDestinationPage> HomePageDestinationPageKeys()
    {
        KeyValuesContainer<HomePageDestinationPage> oKvContainer = new KeyValuesContainer<HomePageDestinationPage>();
        oKvContainer.Add(KeyValuesType.KeyHomePageDestinationPageDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageDestinationPageDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageDestinationPageIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyHomePageDestinationPageIsActive, false, true, false,false,false);

//FK KeyHomePageDestinationPageHomePageDocId
        oKvContainer.Add(KeyValuesType.KeyHomePageDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyHomePageDestinationPageOrder, false, true, false,false,false);

//FK KeyHomePageDestinationPageDestinationPageDocId
        oKvContainer.Add(KeyValuesType.KeyDestinationPageDocId, false, true, false,false,true);

//FK KeyHomePageDestinationPageWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region Faq
    public static KeyValuesContainer<Faq> FaqKeys()
    {
        KeyValuesContainer<Faq> oKvContainer = new KeyValuesContainer<Faq>();
        oKvContainer.Add(KeyValuesType.KeyFaqDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFaqDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyFaqIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFaqIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFaqCategory, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyFaqQuestion, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyFaqAnswer, false, true, false,true,false);

//FK KeyFaqWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyFaqOrder, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region MailAddress
    public static KeyValuesContainer<MailAddress> MailAddressKeys()
    {
        KeyValuesContainer<MailAddress> oKvContainer = new KeyValuesContainer<MailAddress>();
        oKvContainer.Add(KeyValuesType.KeyMailAddressDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyMailAddressDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyMailAddressIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyMailAddressIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyMailAddressAddress, false, true, false,false,false);

//FK KeyMailAddressWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    #region PhonePrefix
    public static KeyValuesContainer<PhonePrefix> PhonePrefixKeys()
    {
        KeyValuesContainer<PhonePrefix> oKvContainer = new KeyValuesContainer<PhonePrefix>();
        oKvContainer.Add(KeyValuesType.KeyPhonePrefixDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPhonePrefixDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyPhonePrefixIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPhonePrefixIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyPhonePrefixPrefixNumber, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region OrderLog
    public static KeyValuesContainer<OrderLog> OrderLogKeys()
    {
        KeyValuesContainer<OrderLog> oKvContainer = new KeyValuesContainer<OrderLog>();
        oKvContainer.Add(KeyValuesType.KeyOrderLogDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderLogDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderLogIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderLogIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderLogType, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderLogStatus, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyOrderLogXML, false, true, false,false,false);

//FK KeyOrderLogWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);

//FK KeyOrderLogOrdersDocId
        oKvContainer.Add(KeyValuesType.KeyOrdersDocId, false, true, false,false,true);
        oKvContainer.Add(KeyValuesType.KeyOrderLogOrderStage, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region Languages
    public static KeyValuesContainer<Languages> LanguagesKeys()
    {
        KeyValuesContainer<Languages> oKvContainer = new KeyValuesContainer<Languages>();
        oKvContainer.Add(KeyValuesType.KeyLanguagesDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLanguagesDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyLanguagesIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLanguagesIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLanguagesName, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyLanguagesCode, false, true, false,false,false);
    
    return oKvContainer;
    }
    #endregion

    #region AirlineFare
    public static KeyValuesContainer<AirlineFare> AirlineFareKeys()
    {
        KeyValuesContainer<AirlineFare> oKvContainer = new KeyValuesContainer<AirlineFare>();
        oKvContainer.Add(KeyValuesType.KeyAirlineFareDateCreated, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineFareDocId, true, true, true,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineFareIsDeleted, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineFareIsActive, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineFareAirlineCode, false, true, false,false,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineFareName, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineFareStandard, false, true, false,true,false);
        oKvContainer.Add(KeyValuesType.KeyAirlineFareFlexible, false, true, false,true,false);

//FK KeyAirlineFareWhiteLabelDocId
        oKvContainer.Add(KeyValuesType.KeyWhiteLabelDocId, false, true, false,false,true);
    
    return oKvContainer;
    }
    #endregion

    }
    #endregion  


    #region DBKeyValurPair
    public static class DBKeyValurPair
    {
        public static string GetDBFieldNameByKeyValueType(KeyValuesType KeyType)
        {
            return GetDBKeyValueByDBKey(KeyType).Key;
        }
        public static KeyValuePair<string, string> GetDBKeyValueByDBKey(KeyValuesType KeyTybe)
        {
            switch (KeyTybe)
            {

                case KeyValuesType.KeyContactDetailsDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyContactDetailsDocId:
                    return new KeyValuePair<string, string>("contact_details_doc_id", DBType.Int);

                case KeyValuesType.KeyContactDetailsIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyContactDetailsIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyContactDetailsFirstName:
                    return new KeyValuePair<string, string>("first_name", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsLastName:
                    return new KeyValuePair<string, string>("last_name", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsCompany:
                    return new KeyValuePair<string, string>("company", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsFlat:
                    return new KeyValuePair<string, string>("flat", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsBuildingName:
                    return new KeyValuePair<string, string>("building_name", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsBuildingNumber:
                    return new KeyValuePair<string, string>("building_number", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsStreet:
                    return new KeyValuePair<string, string>("street", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsLocality:
                    return new KeyValuePair<string, string>("locality", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsCity:
                    return new KeyValuePair<string, string>("city", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsProvince:
                    return new KeyValuePair<string, string>("province", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsZipCode:
                    return new KeyValuePair<string, string>("zip_code", DBType.Int);

                case KeyValuesType.KeyContactDetailsCountryCode:
                    return new KeyValuePair<string, string>("country_code", DBType.Int);

                case KeyValuesType.KeyContactDetailsEmail:
                    return new KeyValuePair<string, string>("email", DBType.Nvarchar_100);

                case KeyValuesType.KeyContactDetailsMainPhone:
                    return new KeyValuePair<string, string>("main_phone", DBType.Nvarchar_50);

                case KeyValuesType.KeyContactDetailsMobliePhone:
                    return new KeyValuePair<string, string>("moblie_phone", DBType.Nvarchar_50);

//FK KeyContactDetailsOrdersDocId|Int|orders_doc_id

//FK KeyContactDetailsWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyContactDetailsTitle:
                    return new KeyValuePair<string, string>("title", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyPassengerDocId:
                    return new KeyValuePair<string, string>("passenger_doc_id", DBType.Int);

                case KeyValuesType.KeyPassengerIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyPassengerIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyPassengerNetPrice:
                    return new KeyValuePair<string, string>("net_price", DBType.Float);

                case KeyValuesType.KeyPassengerFirstName:
                    return new KeyValuePair<string, string>("first_name", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerLastName:
                    return new KeyValuePair<string, string>("last_name", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerDateOfBirth:
                    return new KeyValuePair<string, string>("date_of_birth", DBType.DateTime);

                case KeyValuesType.KeyPassengerGender:
                    return new KeyValuePair<string, string>("gender", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerTitle:
                    return new KeyValuePair<string, string>("title", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerPassportNumber:
                    return new KeyValuePair<string, string>("passport_number", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerPassportExpiryDate:
                    return new KeyValuePair<string, string>("passport_expiry_date", DBType.DateTime);

                case KeyValuesType.KeyPassengerPassportIssueCountry:
                    return new KeyValuePair<string, string>("passport_issue_country", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerCheckInTypeOutward:
                    return new KeyValuePair<string, string>("check_in_type_outward", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerCheckInTypeReturn:
                    return new KeyValuePair<string, string>("check_in_type_return", DBType.Nvarchar_50);

                case KeyValuesType.KeyPassengerCheckInPriceOutward:
                    return new KeyValuePair<string, string>("check_in_price_outward", DBType.Float);

                case KeyValuesType.KeyPassengerCheckInPriceReturn:
                    return new KeyValuePair<string, string>("check_in_price_return", DBType.Float);

                case KeyValuesType.KeyPassengerPriceMarkUp:
                    return new KeyValuePair<string, string>("price_mark_up", DBType.Float);

//FK KeyPassengerSeatPerFlightLegDocId|Int|seat_per_flight_leg_doc_id

//FK KeyPassengerOrdersDocId|Int|orders_doc_id

//FK KeyPassengerWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeySeatPerFlightLegDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeySeatPerFlightLegDocId:
                    return new KeyValuePair<string, string>("seat_per_flight_leg_doc_id", DBType.Int);

                case KeyValuesType.KeySeatPerFlightLegIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeySeatPerFlightLegIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeySeatPerFlightLegPassengerId:
                    return new KeyValuePair<string, string>("passenger_id", DBType.Nvarchar_50);

                case KeyValuesType.KeySeatPerFlightLegSeat:
                    return new KeyValuePair<string, string>("seat", DBType.Nvarchar_50);

                case KeyValuesType.KeySeatPerFlightLegFlightNumber:
                    return new KeyValuePair<string, string>("flight_number", DBType.Nvarchar_50);

//FK KeySeatPerFlightLegPassengerDocId|Int|passenger_doc_id

//FK KeySeatPerFlightLegWhiteLabelDocId|Int|white_label_doc_id

//FK KeySeatPerFlightLegFlightLegDocId|Int|flight_leg_doc_id

                case KeyValuesType.KeyLuggageDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyLuggageDocId:
                    return new KeyValuePair<string, string>("luggage_doc_id", DBType.Int);

                case KeyValuesType.KeyLuggageIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyLuggageIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyLuggagePrice:
                    return new KeyValuePair<string, string>("price", DBType.Float);

                case KeyValuesType.KeyLuggageQuantity:
                    return new KeyValuePair<string, string>("quantity", DBType.Smallint);

                case KeyValuesType.KeyLuggageTotalWeight:
                    return new KeyValuePair<string, string>("total_weight", DBType.Float);

                case KeyValuesType.KeyLuggageFlightType:
                    return new KeyValuePair<string, string>("flight_type", DBType.Nvarchar_50);

//FK KeyLuggagePassengerDocId|Int|passenger_doc_id

//FK KeyLuggageWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyFlightLegDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyFlightLegDocId:
                    return new KeyValuePair<string, string>("flight_leg_doc_id", DBType.Int);

                case KeyValuesType.KeyFlightLegIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyFlightLegIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyFlightLegFlightNumber:
                    return new KeyValuePair<string, string>("flight_number", DBType.Nvarchar_50);

                case KeyValuesType.KeyFlightLegDepartureDateTime:
                    return new KeyValuePair<string, string>("departure_date_time", DBType.DateTime);

                case KeyValuesType.KeyFlightLegArrivalDateTime:
                    return new KeyValuePair<string, string>("arrival_date_time", DBType.DateTime);

                case KeyValuesType.KeyFlightLegStatus:
                    return new KeyValuePair<string, string>("status", DBType.Nvarchar_50);

                case KeyValuesType.KeyFlightLegDuration:
                    return new KeyValuePair<string, string>("duration", DBType.Int);

                case KeyValuesType.KeyFlightLegHops:
                    return new KeyValuePair<string, string>("hops", DBType.Smallint);

                case KeyValuesType.KeyFlightLegAirlineIataCode:
                    return new KeyValuePair<string, string>("airline_iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyFlightLegOriginIataCode:
                    return new KeyValuePair<string, string>("origin_iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyFlightLegDestinationIataCode:
                    return new KeyValuePair<string, string>("destination_iata_code", DBType.Nvarchar_50);

//FK KeyFlightLegOrderFlightLegDocId|Int|order_flight_leg_doc_id

//FK KeyFlightLegWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyAirlineDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyAirlineDocId:
                    return new KeyValuePair<string, string>("airline_doc_id", DBType.Int);

                case KeyValuesType.KeyAirlineIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyAirlineIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyAirlineIataCode:
                    return new KeyValuePair<string, string>("iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyAirlineName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_50);

                case KeyValuesType.KeyPopularDestinationDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyPopularDestinationDocId:
                    return new KeyValuePair<string, string>("popular_destination_doc_id", DBType.Int);

                case KeyValuesType.KeyPopularDestinationIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyPopularDestinationIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyPopularDestinationIataCode:
                    return new KeyValuePair<string, string>("iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyPopularDestinationCityIataCode:
                    return new KeyValuePair<string, string>("city_iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyPopularDestinationName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_50);

//FK KeyPopularDestinationWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyAirportDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyAirportDocId:
                    return new KeyValuePair<string, string>("airport_doc_id", DBType.Int);

                case KeyValuesType.KeyAirportIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyAirportIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyAirportIataCode:
                    return new KeyValuePair<string, string>("iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyAirportCityIataCode:
                    return new KeyValuePair<string, string>("city_iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyAirportEnglishName:
                    return new KeyValuePair<string, string>("english_name", DBType.Nvarchar_50);

                case KeyValuesType.KeyAirportNameByLang:
                    return new KeyValuePair<string, string>("name_by_lang", DBType.Nvarchar_50);

                case KeyValuesType.KeyCityDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyCityDocId:
                    return new KeyValuePair<string, string>("city_doc_id", DBType.Int);

                case KeyValuesType.KeyCityIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyCityIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyCityIataCode:
                    return new KeyValuePair<string, string>("iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyCityName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_50);

                case KeyValuesType.KeyCityCountryCode:
                    return new KeyValuePair<string, string>("country_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyCityEnglishName:
                    return new KeyValuePair<string, string>("english_name", DBType.Nvarchar_250);

                case KeyValuesType.KeyCountryDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyCountryDocId:
                    return new KeyValuePair<string, string>("country_doc_id", DBType.Int);

                case KeyValuesType.KeyCountryIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyCountryIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyCountryCountryCode:
                    return new KeyValuePair<string, string>("country_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyCountryName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_50);

                case KeyValuesType.KeyCountryEnglishName:
                    return new KeyValuePair<string, string>("english_name", DBType.Nvarchar_250);

                case KeyValuesType.KeyDestinationPageDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyDestinationPageDocId:
                    return new KeyValuePair<string, string>("destination_page_doc_id", DBType.Int);

                case KeyValuesType.KeyDestinationPageIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyDestinationPageIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyDestinationPageOrder:
                    return new KeyValuePair<string, string>("order", DBType.Int);

                case KeyValuesType.KeyDestinationPageDestinationIataCode:
                    return new KeyValuePair<string, string>("destination_iata_code", DBType.Nvarchar_50);

//FK KeyDestinationPageWhiteLabelDocId|Int|white_label_doc_id

//FK KeyDestinationPageSeoDocId|Int|seo_doc_id

                case KeyValuesType.KeyDestinationPagePrice:
                    return new KeyValuePair<string, string>("price", DBType.Float);

                case KeyValuesType.KeyDestinationPageLocalCurrency:
                    return new KeyValuePair<string, string>("local_currency", DBType.Nvarchar_50);

                case KeyValuesType.KeyDestinationPageName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_200);

                case KeyValuesType.KeyDestinationPageTime:
                    return new KeyValuePair<string, string>("time", DBType.Nvarchar_50);

                case KeyValuesType.KeyDestinationPageTitle:
                    return new KeyValuePair<string, string>("title", DBType.Nvarchar_50);

                case KeyValuesType.KeyDestinationPagePriceCurrency:
                    return new KeyValuePair<string, string>("price_currency", DBType.Nvarchar_50);

                case KeyValuesType.KeyOrderFlightLegDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyOrderFlightLegDocId:
                    return new KeyValuePair<string, string>("order_flight_leg_doc_id", DBType.Int);

                case KeyValuesType.KeyOrderFlightLegIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyOrderFlightLegIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyOrderFlightLegLowCostPNR:
                    return new KeyValuePair<string, string>("low_cost_pnr", DBType.Nvarchar_50);

                case KeyValuesType.KeyOrderFlightLegPriceNet:
                    return new KeyValuePair<string, string>("price_net", DBType.Float);

                case KeyValuesType.KeyOrderFlightLegPriceMarkUp:
                    return new KeyValuePair<string, string>("price_mark_up", DBType.Float);

                case KeyValuesType.KeyOrderFlightLegFlightType:
                    return new KeyValuePair<string, string>("flight_type", DBType.Nvarchar_50);

//FK KeyOrderFlightLegWhiteLabelDocId|Int|white_label_doc_id

//FK KeyOrderFlightLegOrdersDocId|Int|orders_doc_id

                case KeyValuesType.KeyOrdersDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyOrdersDocId:
                    return new KeyValuePair<string, string>("orders_doc_id", DBType.Int);

                case KeyValuesType.KeyOrdersIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyOrdersIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyOrdersLowCostPNR:
                    return new KeyValuePair<string, string>("low_cost_pnr", DBType.Nvarchar_50);

                case KeyValuesType.KeyOrdersSupplierPNR:
                    return new KeyValuePair<string, string>("supplier_pnr", DBType.Nvarchar_50);

                case KeyValuesType.KeyOrdersTotalPrice:
                    return new KeyValuePair<string, string>("total_price", DBType.Float);

                case KeyValuesType.KeyOrdersDateOfBook:
                    return new KeyValuePair<string, string>("date_of_book", DBType.DateTime);

                case KeyValuesType.KeyOrdersReceiveEmail:
                    return new KeyValuePair<string, string>("receive_email", DBType.Bit);

                case KeyValuesType.KeyOrdersReceiveSms:
                    return new KeyValuePair<string, string>("receive_sms", DBType.Bit);

                case KeyValuesType.KeyOrdersMarketingCabin:
                    return new KeyValuePair<string, string>("marketing_cabin", DBType.Nvarchar_50);

//FK KeyOrdersContactDetailsDocId|Int|contact_details_doc_id

//FK KeyOrdersWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyOrdersAirlineIataCode:
                    return new KeyValuePair<string, string>("airline_iata_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyOrdersOrderStatus:
                    return new KeyValuePair<string, string>("order_status", DBType.Int);

                case KeyValuesType.KeyOrdersAdminRemarks:
                    return new KeyValuePair<string, string>("admin_remarks", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyOrdersStatusPayment:
                    return new KeyValuePair<string, string>("status_payment", DBType.Int);

                case KeyValuesType.KeyOrdersOrderToken:
                    return new KeyValuePair<string, string>("order_token", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyOrdersPaymentTransaction:
                    return new KeyValuePair<string, string>("payment_transaction", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyOrdersXmlBookRequest:
                    return new KeyValuePair<string, string>("xml_book_request", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyOrdersXmlBookResponse:
                    return new KeyValuePair<string, string>("xml_book_response", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyOrdersOrderRemarks:
                    return new KeyValuePair<string, string>("order_remarks", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyOrdersOrderPayment:
                    return new KeyValuePair<string, string>("order_payment", DBType.Int);

                case KeyValuesType.KeyOrdersPaymentToken:
                    return new KeyValuePair<string, string>("payment_token", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyOrdersPaymentRemarks:
                    return new KeyValuePair<string, string>("payment_remarks", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyOrdersCurrency:
                    return new KeyValuePair<string, string>("currency", DBType.Nvarchar_50);

                case KeyValuesType.KeyImagesDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyImagesDocId:
                    return new KeyValuePair<string, string>("images_doc_id", DBType.Int);

                case KeyValuesType.KeyImagesIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyImagesIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyImagesImageFileName:
                    return new KeyValuePair<string, string>("image_file_name", DBType.Nvarchar_50);

                case KeyValuesType.KeyImagesRelatedObjectDocId:
                    return new KeyValuePair<string, string>("related_object_doc_id", DBType.Int);

                case KeyValuesType.KeyImagesImageType:
                    return new KeyValuePair<string, string>("image_type", DBType.Nvarchar_50);

                case KeyValuesType.KeyImagesRelatedObjectType:
                    return new KeyValuePair<string, string>("related_object_type", DBType.Nvarchar_50);

                case KeyValuesType.KeyImagesTitle:
                    return new KeyValuePair<string, string>("title", DBType.Nvarchar_150);

                case KeyValuesType.KeyImagesAlt:
                    return new KeyValuePair<string, string>("alt", DBType.Nvarchar_150);

                case KeyValuesType.KeyMessagesDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyMessagesDocId:
                    return new KeyValuePair<string, string>("messages_doc_id", DBType.Int);

                case KeyValuesType.KeyMessagesIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyMessagesIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyMessagesSentDateTime:
                    return new KeyValuePair<string, string>("sent_date_time", DBType.DateTime);

                case KeyValuesType.KeyMessagesMessageType:
                    return new KeyValuePair<string, string>("message_type", DBType.DateTime);

                case KeyValuesType.KeyMessagesLowCostPNR:
                    return new KeyValuePair<string, string>("low_cost_pnr", DBType.Nvarchar_50);

//FK KeyMessagesOrdersDocId|Int|orders_doc_id

                case KeyValuesType.KeyMessagesContent:
                    return new KeyValuePair<string, string>("content", DBType.Nvarchar_2048);

//FK KeyMessagesWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyStampsDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyStampsDocId:
                    return new KeyValuePair<string, string>("stamps_doc_id", DBType.Int);

                case KeyValuesType.KeyStampsIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyStampsIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyStampsText:
                    return new KeyValuePair<string, string>("text", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyStampsType:
                    return new KeyValuePair<string, string>("type", DBType.Int);

                case KeyValuesType.KeyStamps_PopularDestinationDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyStamps_PopularDestinationDocId:
                    return new KeyValuePair<string, string>("stamps_popular_destination_doc_id", DBType.Int);

                case KeyValuesType.KeyStamps_PopularDestinationIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyStamps_PopularDestinationIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

//FK KeyStamps_PopularDestinationPopularDestinationDocId|Int|popular_destination_doc_id

//FK KeyStamps_PopularDestinationStampsDocId|Int|stamps_doc_id

                case KeyValuesType.KeyWhiteLabelDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyWhiteLabelDocId:
                    return new KeyValuePair<string, string>("white_label_doc_id", DBType.Int);

                case KeyValuesType.KeyWhiteLabelIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyWhiteLabelIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyWhiteLabelDomain:
                    return new KeyValuePair<string, string>("domain", DBType.Nvarchar_1024);

                case KeyValuesType.KeyWhiteLabelName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyWhiteLabelLanguagesDocId:
                    return new KeyValuePair<string, string>("languages_doc_id", DBType.Int);

                case KeyValuesType.KeyWhiteLabelPhone:
                    return new KeyValuePair<string, string>("phone", DBType.Nvarchar_50);

                case KeyValuesType.KeyWhiteLabelEmail:
                    return new KeyValuePair<string, string>("email", DBType.Nvarchar_250);

                case KeyValuesType.KeyWhiteLabelDiscountType:
                    return new KeyValuePair<string, string>("discount_type", DBType.Int);

                case KeyValuesType.KeyWhiteLabelAddress:
                    return new KeyValuePair<string, string>("address", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyWhiteLabelDiscount:
                    return new KeyValuePair<string, string>("discount", DBType.Numeric_18_2);

                case KeyValuesType.KeySeoDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeySeoDocId:
                    return new KeyValuePair<string, string>("seo_doc_id", DBType.Int);

                case KeyValuesType.KeySeoIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeySeoIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeySeoFriendlyUrl:
                    return new KeyValuePair<string, string>("friendly_url", DBType.Nvarchar_100);

                case KeyValuesType.KeySeoSeoTitle:
                    return new KeyValuePair<string, string>("seo_title", DBType.Nvarchar_MAX);

                case KeyValuesType.KeySeoSeoDescription:
                    return new KeyValuePair<string, string>("seo_description", DBType.Nvarchar_MAX);

                case KeyValuesType.KeySeoSeoKeyWords:
                    return new KeyValuePair<string, string>("seo_key_words", DBType.Nvarchar_MAX);

                case KeyValuesType.KeySeoRelatedObject:
                    return new KeyValuePair<string, string>("related_object", DBType.Int);

//FK KeySeoWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyIndexPagePromoDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyIndexPagePromoDocId:
                    return new KeyValuePair<string, string>("index_page_promo_doc_id", DBType.Int);

                case KeyValuesType.KeyIndexPagePromoIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyIndexPagePromoIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyIndexPagePromoImagesDocId:
                    return new KeyValuePair<string, string>("images_doc_id", DBType.Int);

                case KeyValuesType.KeyIndexPagePromoTitle:
                    return new KeyValuePair<string, string>("title", DBType.Nvarchar_1024);

                case KeyValuesType.KeyIndexPagePromoText:
                    return new KeyValuePair<string, string>("text", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyIndexPagePromoLink:
                    return new KeyValuePair<string, string>("link", DBType.Nvarchar_1024);

                case KeyValuesType.KeyIndexPagePromoPromoType:
                    return new KeyValuePair<string, string>("promo_type", DBType.Int);

                case KeyValuesType.KeyIndexPagePromoOrder:
                    return new KeyValuePair<string, string>("order", DBType.Int);

//FK KeyIndexPagePromoWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyIndexPagePromoPriceAfter:
                    return new KeyValuePair<string, string>("price_after", DBType.Numeric_18_2);

                case KeyValuesType.KeyIndexPagePromoPriceBefore:
                    return new KeyValuePair<string, string>("price_before", DBType.Numeric_18_2);

                case KeyValuesType.KeyStaticPagesDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyStaticPagesDocId:
                    return new KeyValuePair<string, string>("static_pages_doc_id", DBType.Int);

                case KeyValuesType.KeyStaticPagesIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyStaticPagesIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyStaticPagesFriendlyUrl:
                    return new KeyValuePair<string, string>("friendly_url", DBType.Nvarchar_250);

                case KeyValuesType.KeyStaticPagesText:
                    return new KeyValuePair<string, string>("text", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyStaticPagesName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_1024);

                case KeyValuesType.KeyStaticPagesType:
                    return new KeyValuePair<string, string>("type", DBType.Int);

//FK KeyStaticPagesSeoDocId|Int|seo_doc_id

//FK KeyStaticPagesWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyStopsDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyStopsDocId:
                    return new KeyValuePair<string, string>("stops_doc_id", DBType.Int);

                case KeyValuesType.KeyStopsIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyStopsIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyStopsArrival:
                    return new KeyValuePair<string, string>("arrival", DBType.DateTime);

                case KeyValuesType.KeyStopsDeparture:
                    return new KeyValuePair<string, string>("departure", DBType.DateTime);

//FK KeyStopsFlightLegDocId|Int|flight_leg_doc_id

//FK KeyStopsAirportIataCode|Nvarchar_50|airport_iata_code

                case KeyValuesType.KeyTemplateDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyTemplateDocId:
                    return new KeyValuePair<string, string>("template_doc_id", DBType.Int);

                case KeyValuesType.KeyTemplateIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyTemplateIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyTemplateTitle:
                    return new KeyValuePair<string, string>("title", DBType.Nvarchar_250);

                case KeyValuesType.KeyTemplateSubTitle:
                    return new KeyValuePair<string, string>("sub_title", DBType.Nvarchar_250);

                case KeyValuesType.KeyTemplateType:
                    return new KeyValuePair<string, string>("type", DBType.Nvarchar_50);

                case KeyValuesType.KeyTemplateOrder:
                    return new KeyValuePair<string, string>("order", DBType.Int);

//FK KeyTemplateWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyTemplateRelatedObjectType:
                    return new KeyValuePair<string, string>("related_object_type", DBType.Int);

                case KeyValuesType.KeyTemplateRelatedObjectDocId:
                    return new KeyValuePair<string, string>("related_object_doc_id", DBType.Int);

                case KeyValuesType.KeyTemplateTextLong:
                    return new KeyValuePair<string, string>("text_long", DBType.Nvarchar_MAX);

                case KeyValuesType.KeyTemplateTextShort:
                    return new KeyValuePair<string, string>("text_short", DBType.Nvarchar_2048);

                case KeyValuesType.KeySubBoxDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeySubBoxDocId:
                    return new KeyValuePair<string, string>("sub_box_doc_id", DBType.Int);

                case KeyValuesType.KeySubBoxIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeySubBoxIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeySubBoxSubTitle:
                    return new KeyValuePair<string, string>("sub_title", DBType.Nvarchar_150);

                case KeyValuesType.KeySubBoxShortText:
                    return new KeyValuePair<string, string>("short_text", DBType.Nvarchar_250);

                case KeyValuesType.KeySubBoxLongText:
                    return new KeyValuePair<string, string>("long_text", DBType.Nvarchar_2048);

                case KeyValuesType.KeySubBoxOrder:
                    return new KeyValuePair<string, string>("order", DBType.Int);

//FK KeySubBoxTemplateDocId|Int|template_doc_id

//FK KeySubBoxWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyCurrencyDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyCurrencyDocId:
                    return new KeyValuePair<string, string>("currency_doc_id", DBType.Int);

                case KeyValuesType.KeyCurrencyIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyCurrencyIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyCurrencyName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_50);

                case KeyValuesType.KeyCurrencyCode:
                    return new KeyValuePair<string, string>("code", DBType.Nvarchar_50);

                case KeyValuesType.KeyCurrencySign:
                    return new KeyValuePair<string, string>("sign", DBType.Nchar_10);

                case KeyValuesType.KeyCompanyPageDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyCompanyPageDocId:
                    return new KeyValuePair<string, string>("company_page_doc_id", DBType.Int);

                case KeyValuesType.KeyCompanyPageIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyCompanyPageIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyCompanyPageName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_50);

//FK KeyCompanyPageSeoDocId|Int|seo_doc_id

//FK KeyCompanyPageWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyCompanyPageText:
                    return new KeyValuePair<string, string>("text", DBType.Nvarchar_1024);

                case KeyValuesType.KeyCompanyPageTitle:
                    return new KeyValuePair<string, string>("title", DBType.Nvarchar_250);

//FK KeyCompanyPageAirlineDocId|Int|airline_doc_id

                case KeyValuesType.KeyCompanyDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyCompanyDocId:
                    return new KeyValuePair<string, string>("company_doc_id", DBType.Int);

                case KeyValuesType.KeyCompanyIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyCompanyIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyCompanyName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_50);

//FK KeyCompanyWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyTipDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyTipDocId:
                    return new KeyValuePair<string, string>("tip_doc_id", DBType.Int);

                case KeyValuesType.KeyTipIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyTipIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

//FK KeyTipWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyTipTitle:
                    return new KeyValuePair<string, string>("title", DBType.Nvarchar_150);

                case KeyValuesType.KeyTipShortText:
                    return new KeyValuePair<string, string>("short_text", DBType.Nvarchar_250);

                case KeyValuesType.KeyTipLongText:
                    return new KeyValuePair<string, string>("long_text", DBType.Nvarchar_2048);

                case KeyValuesType.KeyTipOrder:
                    return new KeyValuePair<string, string>("order", DBType.Int);

                case KeyValuesType.KeyTipDate:
                    return new KeyValuePair<string, string>("date", DBType.DateTime);

                case KeyValuesType.KeyUsersDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyUsersDocId:
                    return new KeyValuePair<string, string>("users_doc_id", DBType.Int);

                case KeyValuesType.KeyUsersIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyUsersIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyUsersUserName:
                    return new KeyValuePair<string, string>("user_name", DBType.Nvarchar_250);

                case KeyValuesType.KeyUsersPassword:
                    return new KeyValuePair<string, string>("password", DBType.Nvarchar_250);

                case KeyValuesType.KeyUsersEmail:
                    return new KeyValuePair<string, string>("email", DBType.Nvarchar_250);

                case KeyValuesType.KeyUsersFirstName:
                    return new KeyValuePair<string, string>("first_name", DBType.Nvarchar_250);

                case KeyValuesType.KeyUsersLastName:
                    return new KeyValuePair<string, string>("last_name", DBType.Nvarchar_250);

                case KeyValuesType.KeyUsersRole:
                    return new KeyValuePair<string, string>("role", DBType.Int);

//FK KeyUsersWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyHomePageDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyHomePageDocId:
                    return new KeyValuePair<string, string>("home_page_doc_id", DBType.Int);

                case KeyValuesType.KeyHomePageIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyHomePageIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyHomePageTextLine1:
                    return new KeyValuePair<string, string>("text_line_1", DBType.Nvarchar_1024);

                case KeyValuesType.KeyHomePageTextLine2:
                    return new KeyValuePair<string, string>("text_line_2", DBType.Nvarchar_1024);

                case KeyValuesType.KeyHomePageTextLine3:
                    return new KeyValuePair<string, string>("text_line_3", DBType.Nvarchar_1024);

                case KeyValuesType.KeyHomePageTextLine4:
                    return new KeyValuePair<string, string>("text_line_4", DBType.Nvarchar_1024);

//FK KeyHomePageWhiteLabelDocId|Int|white_label_doc_id

//FK KeyHomePageSeoDocId|Int|seo_doc_id

//FK KeyHomePageTipDocId|Int|tip_doc_id

                case KeyValuesType.KeyHomePageDestinationPageDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyHomePageDestinationPageDocId:
                    return new KeyValuePair<string, string>("home_page_destination_page_doc_id", DBType.Int);

                case KeyValuesType.KeyHomePageDestinationPageIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyHomePageDestinationPageIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

//FK KeyHomePageDestinationPageHomePageDocId|Int|home_page_doc_id

                case KeyValuesType.KeyHomePageDestinationPageOrder:
                    return new KeyValuePair<string, string>("order", DBType.Int);

//FK KeyHomePageDestinationPageDestinationPageDocId|Int|destination_page_doc_id

//FK KeyHomePageDestinationPageWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyFaqDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyFaqDocId:
                    return new KeyValuePair<string, string>("faq_doc_id", DBType.Int);

                case KeyValuesType.KeyFaqIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyFaqIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyFaqCategory:
                    return new KeyValuePair<string, string>("category", DBType.Int);

                case KeyValuesType.KeyFaqQuestion:
                    return new KeyValuePair<string, string>("question", DBType.Nvarchar_1024);

                case KeyValuesType.KeyFaqAnswer:
                    return new KeyValuePair<string, string>("answer", DBType.Nvarchar_1024);

//FK KeyFaqWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyFaqOrder:
                    return new KeyValuePair<string, string>("order", DBType.Int);

                case KeyValuesType.KeyMailAddressDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyMailAddressDocId:
                    return new KeyValuePair<string, string>("mail_address_doc_id", DBType.Int);

                case KeyValuesType.KeyMailAddressIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyMailAddressIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyMailAddressAddress:
                    return new KeyValuePair<string, string>("address", DBType.Nvarchar_250);

//FK KeyMailAddressWhiteLabelDocId|Int|white_label_doc_id

                case KeyValuesType.KeyPhonePrefixDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyPhonePrefixDocId:
                    return new KeyValuePair<string, string>("phone_prefix_doc_id", DBType.Int);

                case KeyValuesType.KeyPhonePrefixIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyPhonePrefixIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyPhonePrefixPrefixNumber:
                    return new KeyValuePair<string, string>("prefix_number", DBType.Nvarchar_50);

                case KeyValuesType.KeyOrderLogDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyOrderLogDocId:
                    return new KeyValuePair<string, string>("order_log_doc_id", DBType.Int);

                case KeyValuesType.KeyOrderLogIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyOrderLogIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyOrderLogType:
                    return new KeyValuePair<string, string>("type", DBType.Int);

                case KeyValuesType.KeyOrderLogStatus:
                    return new KeyValuePair<string, string>("status", DBType.Int);

                case KeyValuesType.KeyOrderLogXML:
                    return new KeyValuePair<string, string>("xml", DBType.Nvarchar_MAX);

//FK KeyOrderLogWhiteLabelDocId|Int|white_label_doc_id

//FK KeyOrderLogOrdersDocId|Int|orders_doc_id

                case KeyValuesType.KeyOrderLogOrderStage:
                    return new KeyValuePair<string, string>("order_stage", DBType.Int);

                case KeyValuesType.KeyLanguagesDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyLanguagesDocId:
                    return new KeyValuePair<string, string>("languages_doc_id", DBType.Int);

                case KeyValuesType.KeyLanguagesIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyLanguagesIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyLanguagesName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_50);

                case KeyValuesType.KeyLanguagesCode:
                    return new KeyValuePair<string, string>("code", DBType.Nvarchar_50);

                case KeyValuesType.KeyAirlineFareDateCreated:
                    return new KeyValuePair<string, string>("date_created", DBType.DateTime);

                case KeyValuesType.KeyAirlineFareDocId:
                    return new KeyValuePair<string, string>("airline_fare_doc_id", DBType.Int);

                case KeyValuesType.KeyAirlineFareIsDeleted:
                    return new KeyValuePair<string, string>("is_deleted", DBType.Bit);

                case KeyValuesType.KeyAirlineFareIsActive:
                    return new KeyValuePair<string, string>("is_active", DBType.Bit);

                case KeyValuesType.KeyAirlineFareAirlineCode:
                    return new KeyValuePair<string, string>("airline_code", DBType.Nvarchar_50);

                case KeyValuesType.KeyAirlineFareName:
                    return new KeyValuePair<string, string>("name", DBType.Nvarchar_250);

                case KeyValuesType.KeyAirlineFareStandard:
                    return new KeyValuePair<string, string>("standard", DBType.Nvarchar_250);

                case KeyValuesType.KeyAirlineFareFlexible:
                    return new KeyValuePair<string, string>("flexible", DBType.Nvarchar_250);

//FK KeyAirlineFareWhiteLabelDocId|Int|white_label_doc_id

            default:
                return new KeyValuePair<string, string>("null", "null");
            }
        }

    // Replace Or Add Function GetTypeCodeByKey in KeyValuesContainer<ITEM> generic class
    #region GetTypeCodeByKey
    public static TypeCode GetTypeCodeByKey(KeyValuesType SortKey)
    {
        TypeCode typeCode = TypeCode.Empty;
        switch (SortKey)
        {

            #region ContactDetails

            case KeyValuesType.KeyContactDetailsDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyContactDetailsDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyContactDetailsIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyContactDetailsIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyContactDetailsFirstName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsLastName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsCompany:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsFlat:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsBuildingName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsBuildingNumber:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsStreet:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsLocality:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsCity:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsProvince:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsZipCode:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyContactDetailsCountryCode:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyContactDetailsEmail:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsMainPhone:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyContactDetailsMobliePhone:
                typeCode = TypeCode.String;
                break;

//FK KeyContactDetailsOrdersDocId|Int

//FK KeyContactDetailsWhiteLabelDocId|Int

            case KeyValuesType.KeyContactDetailsTitle:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region Passenger

            case KeyValuesType.KeyPassengerDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyPassengerDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyPassengerIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyPassengerIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyPassengerNetPrice:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyPassengerFirstName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPassengerLastName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPassengerDateOfBirth:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyPassengerGender:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPassengerTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPassengerPassportNumber:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPassengerPassportExpiryDate:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyPassengerPassportIssueCountry:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPassengerCheckInTypeOutward:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPassengerCheckInTypeReturn:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPassengerCheckInPriceOutward:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyPassengerCheckInPriceReturn:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyPassengerPriceMarkUp:
                typeCode = TypeCode.Double;
                break;

//FK KeyPassengerSeatPerFlightLegDocId|Int

//FK KeyPassengerOrdersDocId|Int

//FK KeyPassengerWhiteLabelDocId|Int

            #endregion

            #region SeatPerFlightLeg

            case KeyValuesType.KeySeatPerFlightLegDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeySeatPerFlightLegDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeySeatPerFlightLegIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeySeatPerFlightLegIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeySeatPerFlightLegPassengerId:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySeatPerFlightLegSeat:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySeatPerFlightLegFlightNumber:
                typeCode = TypeCode.String;
                break;

//FK KeySeatPerFlightLegPassengerDocId|Int

//FK KeySeatPerFlightLegWhiteLabelDocId|Int

//FK KeySeatPerFlightLegFlightLegDocId|Int

            #endregion

            #region Luggage

            case KeyValuesType.KeyLuggageDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyLuggageDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyLuggageIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyLuggageIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyLuggagePrice:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyLuggageQuantity:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyLuggageTotalWeight:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyLuggageFlightType:
                typeCode = TypeCode.String;
                break;

//FK KeyLuggagePassengerDocId|Int

//FK KeyLuggageWhiteLabelDocId|Int

            #endregion

            #region FlightLeg

            case KeyValuesType.KeyFlightLegDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyFlightLegDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyFlightLegIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyFlightLegIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyFlightLegFlightNumber:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyFlightLegDepartureDateTime:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyFlightLegArrivalDateTime:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyFlightLegStatus:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyFlightLegDuration:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyFlightLegHops:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyFlightLegAirlineIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyFlightLegOriginIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyFlightLegDestinationIataCode:
                typeCode = TypeCode.String;
                break;

//FK KeyFlightLegOrderFlightLegDocId|Int

//FK KeyFlightLegWhiteLabelDocId|Int

            #endregion

            #region Airline

            case KeyValuesType.KeyAirlineDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyAirlineDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyAirlineIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyAirlineIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyAirlineIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyAirlineName:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region PopularDestination

            case KeyValuesType.KeyPopularDestinationDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyPopularDestinationDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyPopularDestinationIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyPopularDestinationIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyPopularDestinationIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPopularDestinationCityIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyPopularDestinationName:
                typeCode = TypeCode.String;
                break;

//FK KeyPopularDestinationWhiteLabelDocId|Int

            #endregion

            #region Airport

            case KeyValuesType.KeyAirportDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyAirportDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyAirportIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyAirportIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyAirportIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyAirportCityIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyAirportEnglishName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyAirportNameByLang:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region City

            case KeyValuesType.KeyCityDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyCityDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyCityIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCityIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCityIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyCityName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyCityCountryCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyCityEnglishName:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region Country

            case KeyValuesType.KeyCountryDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyCountryDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyCountryIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCountryIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCountryCountryCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyCountryName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyCountryEnglishName:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region DestinationPage

            case KeyValuesType.KeyDestinationPageDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyDestinationPageDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyDestinationPageIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyDestinationPageIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyDestinationPageOrder:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyDestinationPageDestinationIataCode:
                typeCode = TypeCode.String;
                break;

//FK KeyDestinationPageWhiteLabelDocId|Int

//FK KeyDestinationPageSeoDocId|Int

            case KeyValuesType.KeyDestinationPagePrice:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyDestinationPageLocalCurrency:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyDestinationPageName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyDestinationPageTime:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyDestinationPageTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyDestinationPagePriceCurrency:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region OrderFlightLeg

            case KeyValuesType.KeyOrderFlightLegDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyOrderFlightLegDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyOrderFlightLegIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyOrderFlightLegIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyOrderFlightLegLowCostPNR:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrderFlightLegPriceNet:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyOrderFlightLegPriceMarkUp:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyOrderFlightLegFlightType:
                typeCode = TypeCode.String;
                break;

//FK KeyOrderFlightLegWhiteLabelDocId|Int

//FK KeyOrderFlightLegOrdersDocId|Int

            #endregion

            #region Orders

            case KeyValuesType.KeyOrdersDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyOrdersDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyOrdersIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyOrdersIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyOrdersLowCostPNR:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersSupplierPNR:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersTotalPrice:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyOrdersDateOfBook:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyOrdersReceiveEmail:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyOrdersReceiveSms:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyOrdersMarketingCabin:
                typeCode = TypeCode.String;
                break;

//FK KeyOrdersContactDetailsDocId|Int

//FK KeyOrdersWhiteLabelDocId|Int

            case KeyValuesType.KeyOrdersAirlineIataCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersOrderStatus:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyOrdersAdminRemarks:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersStatusPayment:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyOrdersOrderToken:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersPaymentTransaction:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersXmlBookRequest:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersXmlBookResponse:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersOrderRemarks:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersOrderPayment:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyOrdersPaymentToken:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersPaymentRemarks:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyOrdersCurrency:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region Images

            case KeyValuesType.KeyImagesDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyImagesDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyImagesIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyImagesIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyImagesImageFileName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyImagesRelatedObjectDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyImagesImageType:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyImagesRelatedObjectType:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyImagesTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyImagesAlt:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region Messages

            case KeyValuesType.KeyMessagesDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyMessagesDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyMessagesIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyMessagesIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyMessagesSentDateTime:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyMessagesMessageType:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyMessagesLowCostPNR:
                typeCode = TypeCode.String;
                break;

//FK KeyMessagesOrdersDocId|Int

            case KeyValuesType.KeyMessagesContent:
                typeCode = TypeCode.String;
                break;

//FK KeyMessagesWhiteLabelDocId|Int

            #endregion

            #region Stamps

            case KeyValuesType.KeyStampsDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyStampsDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyStampsIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyStampsIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyStampsText:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyStampsType:
                typeCode = TypeCode.Int32;
                break;

            #endregion

            #region Stamps_PopularDestination

            case KeyValuesType.KeyStamps_PopularDestinationDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyStamps_PopularDestinationDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyStamps_PopularDestinationIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyStamps_PopularDestinationIsActive:
                typeCode = TypeCode.Boolean;
                break;

//FK KeyStamps_PopularDestinationPopularDestinationDocId|Int

//FK KeyStamps_PopularDestinationStampsDocId|Int

            #endregion

            #region WhiteLabel

            case KeyValuesType.KeyWhiteLabelDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyWhiteLabelDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyWhiteLabelIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyWhiteLabelIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyWhiteLabelDomain:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyWhiteLabelName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyWhiteLabelLanguagesDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyWhiteLabelPhone:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyWhiteLabelEmail:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyWhiteLabelDiscountType:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyWhiteLabelAddress:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyWhiteLabelDiscount:
                typeCode = TypeCode.Double;
                break;

            #endregion

            #region Seo

            case KeyValuesType.KeySeoDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeySeoDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeySeoIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeySeoIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeySeoFriendlyUrl:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySeoSeoTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySeoSeoDescription:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySeoSeoKeyWords:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySeoRelatedObject:
                typeCode = TypeCode.Int32;
                break;

//FK KeySeoWhiteLabelDocId|Int

            #endregion

            #region IndexPagePromo

            case KeyValuesType.KeyIndexPagePromoDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyIndexPagePromoDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyIndexPagePromoIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyIndexPagePromoIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyIndexPagePromoImagesDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyIndexPagePromoTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyIndexPagePromoText:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyIndexPagePromoLink:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyIndexPagePromoPromoType:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyIndexPagePromoOrder:
                typeCode = TypeCode.Int32;
                break;

//FK KeyIndexPagePromoWhiteLabelDocId|Int

            case KeyValuesType.KeyIndexPagePromoPriceAfter:
                typeCode = TypeCode.Double;
                break;

            case KeyValuesType.KeyIndexPagePromoPriceBefore:
                typeCode = TypeCode.Double;
                break;

            #endregion

            #region StaticPages

            case KeyValuesType.KeyStaticPagesDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyStaticPagesDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyStaticPagesIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyStaticPagesIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyStaticPagesFriendlyUrl:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyStaticPagesText:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyStaticPagesName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyStaticPagesType:
                typeCode = TypeCode.Int32;
                break;

//FK KeyStaticPagesSeoDocId|Int

//FK KeyStaticPagesWhiteLabelDocId|Int

            #endregion

            #region Stops

            case KeyValuesType.KeyStopsDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyStopsDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyStopsIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyStopsIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyStopsArrival:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyStopsDeparture:
                typeCode = TypeCode.DateTime;
                break;

//FK KeyStopsFlightLegDocId|Int

//FK KeyStopsAirportIataCode|String

            #endregion

            #region Template

            case KeyValuesType.KeyTemplateDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyTemplateDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyTemplateIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyTemplateIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyTemplateTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyTemplateSubTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyTemplateType:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyTemplateOrder:
                typeCode = TypeCode.Int32;
                break;

//FK KeyTemplateWhiteLabelDocId|Int

            case KeyValuesType.KeyTemplateRelatedObjectType:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyTemplateRelatedObjectDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyTemplateTextLong:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyTemplateTextShort:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region SubBox

            case KeyValuesType.KeySubBoxDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeySubBoxDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeySubBoxIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeySubBoxIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeySubBoxSubTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySubBoxShortText:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySubBoxLongText:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeySubBoxOrder:
                typeCode = TypeCode.Int32;
                break;

//FK KeySubBoxTemplateDocId|Int

//FK KeySubBoxWhiteLabelDocId|Int

            #endregion

            #region Currency

            case KeyValuesType.KeyCurrencyDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyCurrencyDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyCurrencyIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCurrencyIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCurrencyName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyCurrencyCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyCurrencySign:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region CompanyPage

            case KeyValuesType.KeyCompanyPageDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyCompanyPageDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyCompanyPageIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCompanyPageIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCompanyPageName:
                typeCode = TypeCode.String;
                break;

//FK KeyCompanyPageSeoDocId|Int

//FK KeyCompanyPageWhiteLabelDocId|Int

            case KeyValuesType.KeyCompanyPageText:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyCompanyPageTitle:
                typeCode = TypeCode.String;
                break;

//FK KeyCompanyPageAirlineDocId|Int

            #endregion

            #region Company

            case KeyValuesType.KeyCompanyDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyCompanyDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyCompanyIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCompanyIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyCompanyName:
                typeCode = TypeCode.String;
                break;

//FK KeyCompanyWhiteLabelDocId|Int

            #endregion

            #region Tip

            case KeyValuesType.KeyTipDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyTipDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyTipIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyTipIsActive:
                typeCode = TypeCode.Boolean;
                break;

//FK KeyTipWhiteLabelDocId|Int

            case KeyValuesType.KeyTipTitle:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyTipShortText:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyTipLongText:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyTipOrder:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyTipDate:
                typeCode = TypeCode.DateTime;
                break;

            #endregion

            #region Users

            case KeyValuesType.KeyUsersDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyUsersDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyUsersIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyUsersIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyUsersUserName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyUsersPassword:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyUsersEmail:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyUsersFirstName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyUsersLastName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyUsersRole:
                typeCode = TypeCode.Int32;
                break;

//FK KeyUsersWhiteLabelDocId|Int

            #endregion

            #region HomePage

            case KeyValuesType.KeyHomePageDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyHomePageDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyHomePageIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyHomePageIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyHomePageTextLine1:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyHomePageTextLine2:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyHomePageTextLine3:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyHomePageTextLine4:
                typeCode = TypeCode.String;
                break;

//FK KeyHomePageWhiteLabelDocId|Int

//FK KeyHomePageSeoDocId|Int

//FK KeyHomePageTipDocId|Int

            #endregion

            #region HomePageDestinationPage

            case KeyValuesType.KeyHomePageDestinationPageDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyHomePageDestinationPageDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyHomePageDestinationPageIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyHomePageDestinationPageIsActive:
                typeCode = TypeCode.Boolean;
                break;

//FK KeyHomePageDestinationPageHomePageDocId|Int

            case KeyValuesType.KeyHomePageDestinationPageOrder:
                typeCode = TypeCode.Int32;
                break;

//FK KeyHomePageDestinationPageDestinationPageDocId|Int

//FK KeyHomePageDestinationPageWhiteLabelDocId|Int

            #endregion

            #region Faq

            case KeyValuesType.KeyFaqDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyFaqDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyFaqIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyFaqIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyFaqCategory:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyFaqQuestion:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyFaqAnswer:
                typeCode = TypeCode.String;
                break;

//FK KeyFaqWhiteLabelDocId|Int

            case KeyValuesType.KeyFaqOrder:
                typeCode = TypeCode.Int32;
                break;

            #endregion

            #region MailAddress

            case KeyValuesType.KeyMailAddressDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyMailAddressDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyMailAddressIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyMailAddressIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyMailAddressAddress:
                typeCode = TypeCode.String;
                break;

//FK KeyMailAddressWhiteLabelDocId|Int

            #endregion

            #region PhonePrefix

            case KeyValuesType.KeyPhonePrefixDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyPhonePrefixDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyPhonePrefixIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyPhonePrefixIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyPhonePrefixPrefixNumber:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region OrderLog

            case KeyValuesType.KeyOrderLogDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyOrderLogDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyOrderLogIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyOrderLogIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyOrderLogType:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyOrderLogStatus:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyOrderLogXML:
                typeCode = TypeCode.String;
                break;

//FK KeyOrderLogWhiteLabelDocId|Int

//FK KeyOrderLogOrdersDocId|Int

            case KeyValuesType.KeyOrderLogOrderStage:
                typeCode = TypeCode.Int32;
                break;

            #endregion

            #region Languages

            case KeyValuesType.KeyLanguagesDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyLanguagesDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyLanguagesIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyLanguagesIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyLanguagesName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyLanguagesCode:
                typeCode = TypeCode.String;
                break;

            #endregion

            #region AirlineFare

            case KeyValuesType.KeyAirlineFareDateCreated:
                typeCode = TypeCode.DateTime;
                break;

            case KeyValuesType.KeyAirlineFareDocId:
                typeCode = TypeCode.Int32;
                break;

            case KeyValuesType.KeyAirlineFareIsDeleted:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyAirlineFareIsActive:
                typeCode = TypeCode.Boolean;
                break;

            case KeyValuesType.KeyAirlineFareAirlineCode:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyAirlineFareName:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyAirlineFareStandard:
                typeCode = TypeCode.String;
                break;

            case KeyValuesType.KeyAirlineFareFlexible:
                typeCode = TypeCode.String;
                break;

//FK KeyAirlineFareWhiteLabelDocId|Int

            #endregion

            default:
                typeCode = TypeCode.String;
                break;
        }
        return typeCode;
    }
    #endregion  

    }
    #endregion  



}
