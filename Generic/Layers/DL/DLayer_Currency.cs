

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Currency  : ContainerItem<Currency>{

#region CTOR

    #region Constractor
    static Currency()
    {
        ConvertEvent = Currency.OnConvert;
    }  
    //public KeyValuesContainer<Currency> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Currency()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.CurrencyKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Currency OnConvert(DataRow dr)
    {
        int LangId = Translator<Currency>.LangId;            
        Currency oCurrency = null;
        if (dr != null)
        {
            oCurrency = new Currency();
            #region Create Object
            oCurrency.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Currency.Field_DateCreated]);
             oCurrency.DocId = ConvertTo.ConvertToInt(dr[TBNames_Currency.Field_DocId]);
             oCurrency.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Currency.Field_IsDeleted]);
             oCurrency.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Currency.Field_IsActive]);
             oCurrency.Name = ConvertTo.ConvertToString(dr[TBNames_Currency.Field_Name]);
             oCurrency.Code = ConvertTo.ConvertToString(dr[TBNames_Currency.Field_Code]);
             oCurrency.Sign = ConvertTo.ConvertToString(dr[TBNames_Currency.Field_Sign]);
 
            #endregion
            Translator<Currency>.Translate(oCurrency.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oCurrency;
    }

    
#endregion

//#REP_HERE 
#region Currency Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCurrencyDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyCurrencyDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyCurrencyDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCurrencyDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyCurrencyDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyCurrencyDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCurrencyIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyCurrencyIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyCurrencyIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCurrencyIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyCurrencyIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyCurrencyIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCurrencyName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCurrencyName));
    }
    set
    {
        SetKey(KeyValuesType.KeyCurrencyName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_Code;
private string _code;
public String FriendlyCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCurrencyCode);   
    }
}
public  string Code
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCurrencyCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyCurrencyCode, value);
         _code = ConvertToValue.ConvertToString(value);
        isSetOnce_Code = true;
    }
}


public string Code_Value
{
    get
    {
        //return _code; //ConvertToValue.ConvertToString(Code);
        if(isSetOnce_Code) {return _code;}
        else {return ConvertToValue.ConvertToString(Code);}
    }
}

public string Code_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_code).ToString();
               //if(isSetOnce_Code) {return ConvertToValue.ConvertToString(_code).ToString();}
               //else {return ConvertToValue.ConvertToString(Code).ToString();}
            ConvertToValue.ConvertToString(Code).ToString();
    }
}

private bool isSetOnce_Sign;
private string _sign;
public String FriendlySign
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCurrencySign);   
    }
}
public  string Sign
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCurrencySign));
    }
    set
    {
        SetKey(KeyValuesType.KeyCurrencySign, value);
         _sign = ConvertToValue.ConvertToString(value);
        isSetOnce_Sign = true;
    }
}


public string Sign_Value
{
    get
    {
        //return _sign; //ConvertToValue.ConvertToString(Sign);
        if(isSetOnce_Sign) {return _sign;}
        else {return ConvertToValue.ConvertToString(Sign);}
    }
}

public string Sign_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_sign).ToString();
               //if(isSetOnce_Sign) {return ConvertToValue.ConvertToString(_sign).ToString();}
               //else {return ConvertToValue.ConvertToString(Sign).ToString();}
            ConvertToValue.ConvertToString(Sign).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCurrency.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCurrency.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCurrency.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCurrency.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_Name", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Code(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oCurrency.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_Code", ex.Message));

            }
            return paramsSelect;
        }



        //G
        //6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Sign(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_Sign, ConvertTo.ConvertEmptyToDBNull(oCurrency.Sign));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_Sign", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCurrency.DateCreated)); 
db.AddParameter(TBNames_Currency.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCurrency.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCurrency.DateCreated)); 
db.AddParameter(TBNames_Currency.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCurrency.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Name(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCurrency.DateCreated)); 
db.AddParameter(TBNames_Currency.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCurrency.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Code(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCurrency.DateCreated)); 
db.AddParameter(TBNames_Currency.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oCurrency.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DateCreated_Code", ex.Message));

            }
            return paramsSelect;
        }



        //A_G
        //0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Sign(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCurrency.DateCreated)); 
db.AddParameter(TBNames_Currency.PRM_Sign, ConvertTo.ConvertEmptyToDBNull(oCurrency.Sign));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DateCreated_Sign", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCurrency.DocId)); 
db.AddParameter(TBNames_Currency.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCurrency.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Name(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCurrency.DocId)); 
db.AddParameter(TBNames_Currency.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCurrency.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Code(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCurrency.DocId)); 
db.AddParameter(TBNames_Currency.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oCurrency.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DocId_Code", ex.Message));

            }
            return paramsSelect;
        }



        //B_G
        //1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Sign(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCurrency.DocId)); 
db.AddParameter(TBNames_Currency.PRM_Sign, ConvertTo.ConvertEmptyToDBNull(oCurrency.Sign));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_DocId_Sign", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Name(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCurrency.IsDeleted)); 
db.AddParameter(TBNames_Currency.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCurrency.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Code(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCurrency.IsDeleted)); 
db.AddParameter(TBNames_Currency.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oCurrency.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_IsDeleted_Code", ex.Message));

            }
            return paramsSelect;
        }



        //C_G
        //2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Sign(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCurrency.IsDeleted)); 
db.AddParameter(TBNames_Currency.PRM_Sign, ConvertTo.ConvertEmptyToDBNull(oCurrency.Sign));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_IsDeleted_Sign", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_Code(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCurrency.Name)); 
db.AddParameter(TBNames_Currency.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oCurrency.Code));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_Name_Code", ex.Message));

            }
            return paramsSelect;
        }



        //E_G
        //4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_Sign(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCurrency.Name)); 
db.AddParameter(TBNames_Currency.PRM_Sign, ConvertTo.ConvertEmptyToDBNull(oCurrency.Sign));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_Name_Sign", ex.Message));

            }
            return paramsSelect;
        }



        //F_G
        //5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Code_Sign(Currency oCurrency)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Currency.PRM_Code, ConvertTo.ConvertEmptyToDBNull(oCurrency.Code)); 
db.AddParameter(TBNames_Currency.PRM_Sign, ConvertTo.ConvertEmptyToDBNull(oCurrency.Sign));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Currency, "Select_Currency_By_Keys_View_Code_Sign", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
