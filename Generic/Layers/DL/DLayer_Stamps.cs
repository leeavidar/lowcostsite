

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Stamps  : ContainerItem<Stamps>{

#region CTOR

    #region Constractor
    static Stamps()
    {
        ConvertEvent = Stamps.OnConvert;
    }  
    //public KeyValuesContainer<Stamps> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Stamps()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.StampsKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Stamps OnConvert(DataRow dr)
    {
        int LangId = Translator<Stamps>.LangId;            
        Stamps oStamps = null;
        if (dr != null)
        {
            oStamps = new Stamps();
            #region Create Object
            oStamps.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Stamps.Field_DateCreated]);
             oStamps.DocId = ConvertTo.ConvertToInt(dr[TBNames_Stamps.Field_DocId]);
             oStamps.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Stamps.Field_IsDeleted]);
             oStamps.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Stamps.Field_IsActive]);
             oStamps.Text = ConvertTo.ConvertToString(dr[TBNames_Stamps.Field_Text]);
             oStamps.Type = ConvertTo.ConvertToInt(dr[TBNames_Stamps.Field_Type]);
 
            #endregion
            Translator<Stamps>.Translate(oStamps.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oStamps;
    }

    
#endregion

//#REP_HERE 
#region Stamps Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStampsDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyStampsDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyStampsDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStampsDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyStampsDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyStampsDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStampsIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyStampsIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyStampsIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStampsIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyStampsIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyStampsIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Text;
private string _text;
public String FriendlyText
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStampsText);   
    }
}
public  string Text
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyStampsText));
    }
    set
    {
        SetKey(KeyValuesType.KeyStampsText, value);
         _text = ConvertToValue.ConvertToString(value);
        isSetOnce_Text = true;
    }
}


public string Text_Value
{
    get
    {
        //return _text; //ConvertToValue.ConvertToString(Text);
        if(isSetOnce_Text) {return _text;}
        else {return ConvertToValue.ConvertToString(Text);}
    }
}

public string Text_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text).ToString();
               //if(isSetOnce_Text) {return ConvertToValue.ConvertToString(_text).ToString();}
               //else {return ConvertToValue.ConvertToString(Text).ToString();}
            ConvertToValue.ConvertToString(Text).ToString();
    }
}

private bool isSetOnce_Type;
private int _type;
public String FriendlyType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStampsType);   
    }
}
public  int? Type
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyStampsType));
    }
    set
    {
        SetKey(KeyValuesType.KeyStampsType, value);
         _type = ConvertToValue.ConvertToInt(value);
        isSetOnce_Type = true;
    }
}


public int Type_Value
{
    get
    {
        //return _type; //ConvertToValue.ConvertToInt(Type);
        if(isSetOnce_Type) {return _type;}
        else {return ConvertToValue.ConvertToInt(Type);}
    }
}

public string Type_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_type).ToString();
               //if(isSetOnce_Type) {return ConvertToValue.ConvertToInt(_type).ToString();}
               //else {return ConvertToValue.ConvertToInt(Type).ToString();}
            ConvertToValue.ConvertToInt(Type).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Text(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStamps.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_Text", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Type(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStamps.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_Type", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps.DateCreated)); 
db.AddParameter(TBNames_Stamps.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps.DateCreated)); 
db.AddParameter(TBNames_Stamps.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Text(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps.DateCreated)); 
db.AddParameter(TBNames_Stamps.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStamps.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DateCreated_Text", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Type(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStamps.DateCreated)); 
db.AddParameter(TBNames_Stamps.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStamps.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DateCreated_Type", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps.DocId)); 
db.AddParameter(TBNames_Stamps.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Text(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps.DocId)); 
db.AddParameter(TBNames_Stamps.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStamps.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Type(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStamps.DocId)); 
db.AddParameter(TBNames_Stamps.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStamps.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_DocId_Type", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Text(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps.IsDeleted)); 
db.AddParameter(TBNames_Stamps.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStamps.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_IsDeleted_Text", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Type(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStamps.IsDeleted)); 
db.AddParameter(TBNames_Stamps.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStamps.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_IsDeleted_Type", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Text_Type(Stamps oStamps)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stamps.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStamps.Text)); 
db.AddParameter(TBNames_Stamps.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStamps.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stamps, "Select_Stamps_By_Keys_View_Text_Type", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
