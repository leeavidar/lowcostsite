

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Luggage  : ContainerItem<Luggage>{

#region CTOR

    #region Constractor
    static Luggage()
    {
        ConvertEvent = Luggage.OnConvert;
    }  
    //public KeyValuesContainer<Luggage> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Luggage()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.LuggageKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Luggage OnConvert(DataRow dr)
    {
        int LangId = Translator<Luggage>.LangId;            
        Luggage oLuggage = null;
        if (dr != null)
        {
            oLuggage = new Luggage();
            #region Create Object
            oLuggage.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Luggage.Field_DateCreated]);
             oLuggage.DocId = ConvertTo.ConvertToInt(dr[TBNames_Luggage.Field_DocId]);
             oLuggage.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Luggage.Field_IsDeleted]);
             oLuggage.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Luggage.Field_IsActive]);
             oLuggage.Price = ConvertTo.ConvertToDouble(dr[TBNames_Luggage.Field_Price]);
             oLuggage.Quantity = ConvertTo.ConvertToInt(dr[TBNames_Luggage.Field_Quantity]);
             oLuggage.TotalWeight = ConvertTo.ConvertToDouble(dr[TBNames_Luggage.Field_TotalWeight]);
             oLuggage.FlightType = ConvertTo.ConvertToString(dr[TBNames_Luggage.Field_FlightType]);
 
//FK     KeyPassengerDocId
            oLuggage.PassengerDocId = ConvertTo.ConvertToInt(dr[TBNames_Luggage.Field_PassengerDocId]);
 
//FK     KeyWhiteLabelDocId
            oLuggage.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Luggage.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<Luggage>.Translate(oLuggage.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oLuggage;
    }

    
#endregion

//#REP_HERE 
#region Luggage Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLuggageDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyLuggageDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyLuggageDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLuggageDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyLuggageDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyLuggageDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLuggageIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyLuggageIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyLuggageIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLuggageIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyLuggageIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyLuggageIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Price;
private Double _price;
public String FriendlyPrice
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLuggagePrice);   
    }
}
public  Double? Price
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyLuggagePrice));
    }
    set
    {
        SetKey(KeyValuesType.KeyLuggagePrice, value);
         _price = ConvertToValue.ConvertToDouble(value);
        isSetOnce_Price = true;
    }
}


public Double Price_Value
{
    get
    {
        //return _price; //ConvertToValue.ConvertToDouble(Price);
        if(isSetOnce_Price) {return _price;}
        else {return ConvertToValue.ConvertToDouble(Price);}
    }
}

public string Price_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_price).ToString();
               //if(isSetOnce_Price) {return ConvertToValue.ConvertToDouble(_price).ToString();}
               //else {return ConvertToValue.ConvertToDouble(Price).ToString();}
            ConvertToValue.ConvertToDouble(Price).ToString();
    }
}

private bool isSetOnce_Quantity;
private int _quantity;
public String FriendlyQuantity
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLuggageQuantity);   
    }
}
public  int? Quantity
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyLuggageQuantity));
    }
    set
    {
        SetKey(KeyValuesType.KeyLuggageQuantity, value);
         _quantity = ConvertToValue.ConvertToInt(value);
        isSetOnce_Quantity = true;
    }
}


public int Quantity_Value
{
    get
    {
        //return _quantity; //ConvertToValue.ConvertToInt(Quantity);
        if(isSetOnce_Quantity) {return _quantity;}
        else {return ConvertToValue.ConvertToInt(Quantity);}
    }
}

public string Quantity_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_quantity).ToString();
               //if(isSetOnce_Quantity) {return ConvertToValue.ConvertToInt(_quantity).ToString();}
               //else {return ConvertToValue.ConvertToInt(Quantity).ToString();}
            ConvertToValue.ConvertToInt(Quantity).ToString();
    }
}

private bool isSetOnce_TotalWeight;
private Double _total_weight;
public String FriendlyTotalWeight
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLuggageTotalWeight);   
    }
}
public  Double? TotalWeight
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyLuggageTotalWeight));
    }
    set
    {
        SetKey(KeyValuesType.KeyLuggageTotalWeight, value);
         _total_weight = ConvertToValue.ConvertToDouble(value);
        isSetOnce_TotalWeight = true;
    }
}


public Double TotalWeight_Value
{
    get
    {
        //return _total_weight; //ConvertToValue.ConvertToDouble(TotalWeight);
        if(isSetOnce_TotalWeight) {return _total_weight;}
        else {return ConvertToValue.ConvertToDouble(TotalWeight);}
    }
}

public string TotalWeight_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_total_weight).ToString();
               //if(isSetOnce_TotalWeight) {return ConvertToValue.ConvertToDouble(_total_weight).ToString();}
               //else {return ConvertToValue.ConvertToDouble(TotalWeight).ToString();}
            ConvertToValue.ConvertToDouble(TotalWeight).ToString();
    }
}

private bool isSetOnce_FlightType;
private string _flight_type;
public String FriendlyFlightType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyLuggageFlightType);   
    }
}
public  string FlightType
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyLuggageFlightType));
    }
    set
    {
        SetKey(KeyValuesType.KeyLuggageFlightType, value);
         _flight_type = ConvertToValue.ConvertToString(value);
        isSetOnce_FlightType = true;
    }
}


public string FlightType_Value
{
    get
    {
        //return _flight_type; //ConvertToValue.ConvertToString(FlightType);
        if(isSetOnce_FlightType) {return _flight_type;}
        else {return ConvertToValue.ConvertToString(FlightType);}
    }
}

public string FlightType_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_flight_type).ToString();
               //if(isSetOnce_FlightType) {return ConvertToValue.ConvertToString(_flight_type).ToString();}
               //else {return ConvertToValue.ConvertToString(FlightType).ToString();}
            ConvertToValue.ConvertToString(FlightType).ToString();
    }
}

private bool isSetOnce_PassengerDocId;
private int _passenger_doc_id;
public String FriendlyPassengerDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPassengerDocId);   
    }
}
public  int? PassengerDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyPassengerDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyPassengerDocId, value);
         _passenger_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_PassengerDocId = true;
    }
}


public int PassengerDocId_Value
{
    get
    {
        //return _passenger_doc_id; //ConvertToValue.ConvertToInt(PassengerDocId);
        if(isSetOnce_PassengerDocId) {return _passenger_doc_id;}
        else {return ConvertToValue.ConvertToInt(PassengerDocId);}
    }
}

public string PassengerDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_passenger_doc_id).ToString();
               //if(isSetOnce_PassengerDocId) {return ConvertToValue.ConvertToInt(_passenger_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(PassengerDocId).ToString();}
            ConvertToValue.ConvertToInt(PassengerDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //J_A
        //9_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLuggage.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //J_B
        //9_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C
        //9_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLuggage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_E
        //9_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oLuggage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Price", ex.Message));

            }
            return paramsSelect;
        }



        //J_F
        //9_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Quantity(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Quantity, ConvertTo.ConvertEmptyToDBNull(oLuggage.Quantity));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity", ex.Message));

            }
            return paramsSelect;
        }



        //J_G
        //9_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalWeight(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_TotalWeight, ConvertTo.ConvertEmptyToDBNull(oLuggage.TotalWeight));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight", ex.Message));

            }
            return paramsSelect;
        }



        //J_H
        //9_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightType(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oLuggage.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //J_I
        //9_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerDocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_B
        //9_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLuggage.DateCreated)); 
db.AddParameter(TBNames_Luggage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_C
        //9_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLuggage.DateCreated)); 
db.AddParameter(TBNames_Luggage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLuggage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_E
        //9_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Price(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLuggage.DateCreated)); 
db.AddParameter(TBNames_Luggage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oLuggage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_Price", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_F
        //9_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Quantity(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLuggage.DateCreated)); 
db.AddParameter(TBNames_Luggage.PRM_Quantity, ConvertTo.ConvertEmptyToDBNull(oLuggage.Quantity));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_Quantity", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_G
        //9_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TotalWeight(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLuggage.DateCreated)); 
db.AddParameter(TBNames_Luggage.PRM_TotalWeight, ConvertTo.ConvertEmptyToDBNull(oLuggage.TotalWeight));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_TotalWeight", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_H
        //9_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightType(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLuggage.DateCreated)); 
db.AddParameter(TBNames_Luggage.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oLuggage.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_I
        //9_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassengerDocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oLuggage.DateCreated)); 
db.AddParameter(TBNames_Luggage.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_C
        //9_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.DocId)); 
db.AddParameter(TBNames_Luggage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLuggage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_E
        //9_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Price(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.DocId)); 
db.AddParameter(TBNames_Luggage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oLuggage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_Price", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_F
        //9_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Quantity(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.DocId)); 
db.AddParameter(TBNames_Luggage.PRM_Quantity, ConvertTo.ConvertEmptyToDBNull(oLuggage.Quantity));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_Quantity", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_G
        //9_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TotalWeight(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.DocId)); 
db.AddParameter(TBNames_Luggage.PRM_TotalWeight, ConvertTo.ConvertEmptyToDBNull(oLuggage.TotalWeight));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_TotalWeight", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_H
        //9_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightType(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.DocId)); 
db.AddParameter(TBNames_Luggage.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oLuggage.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_I
        //9_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassengerDocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.DocId)); 
db.AddParameter(TBNames_Luggage.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_E
        //9_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Price(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLuggage.IsDeleted)); 
db.AddParameter(TBNames_Luggage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oLuggage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_Price", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_F
        //9_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Quantity(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLuggage.IsDeleted)); 
db.AddParameter(TBNames_Luggage.PRM_Quantity, ConvertTo.ConvertEmptyToDBNull(oLuggage.Quantity));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_Quantity", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_G
        //9_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TotalWeight(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLuggage.IsDeleted)); 
db.AddParameter(TBNames_Luggage.PRM_TotalWeight, ConvertTo.ConvertEmptyToDBNull(oLuggage.TotalWeight));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_TotalWeight", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_H
        //9_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightType(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLuggage.IsDeleted)); 
db.AddParameter(TBNames_Luggage.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oLuggage.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_I
        //9_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerDocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oLuggage.IsDeleted)); 
db.AddParameter(TBNames_Luggage.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_F
        //9_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_Quantity(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oLuggage.Price)); 
db.AddParameter(TBNames_Luggage.PRM_Quantity, ConvertTo.ConvertEmptyToDBNull(oLuggage.Quantity));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_Quantity", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_G
        //9_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_TotalWeight(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oLuggage.Price)); 
db.AddParameter(TBNames_Luggage.PRM_TotalWeight, ConvertTo.ConvertEmptyToDBNull(oLuggage.TotalWeight));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_TotalWeight", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_H
        //9_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_FlightType(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oLuggage.Price)); 
db.AddParameter(TBNames_Luggage.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oLuggage.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_I
        //9_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_PassengerDocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oLuggage.Price)); 
db.AddParameter(TBNames_Luggage.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_G
        //9_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Quantity_TotalWeight(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Quantity, ConvertTo.ConvertEmptyToDBNull(oLuggage.Quantity)); 
db.AddParameter(TBNames_Luggage.PRM_TotalWeight, ConvertTo.ConvertEmptyToDBNull(oLuggage.TotalWeight));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_TotalWeight", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_H
        //9_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Quantity_FlightType(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Quantity, ConvertTo.ConvertEmptyToDBNull(oLuggage.Quantity)); 
db.AddParameter(TBNames_Luggage.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oLuggage.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_I
        //9_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Quantity_PassengerDocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_Quantity, ConvertTo.ConvertEmptyToDBNull(oLuggage.Quantity)); 
db.AddParameter(TBNames_Luggage.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_H
        //9_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalWeight_FlightType(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_TotalWeight, ConvertTo.ConvertEmptyToDBNull(oLuggage.TotalWeight)); 
db.AddParameter(TBNames_Luggage.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oLuggage.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_I
        //9_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalWeight_PassengerDocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_TotalWeight, ConvertTo.ConvertEmptyToDBNull(oLuggage.TotalWeight)); 
db.AddParameter(TBNames_Luggage.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_H_I
        //9_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightType_PassengerDocId(Luggage oLuggage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Luggage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.WhiteLabelDocId)); 
db.AddParameter(TBNames_Luggage.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oLuggage.FlightType)); 
db.AddParameter(TBNames_Luggage.PRM_PassengerDocId, ConvertTo.ConvertEmptyToDBNull(oLuggage.PassengerDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Luggage, "Select_Luggage_By_Keys_View_WhiteLabelDocId_FlightType_PassengerDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
