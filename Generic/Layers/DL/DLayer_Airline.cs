

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Airline  : ContainerItem<Airline>{

#region CTOR

    #region Constractor
    static Airline()
    {
        ConvertEvent = Airline.OnConvert;
    }  
    //public KeyValuesContainer<Airline> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Airline()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.AirlineKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Airline OnConvert(DataRow dr)
    {
        int LangId = Translator<Airline>.LangId;            
        Airline oAirline = null;
        if (dr != null)
        {
            oAirline = new Airline();
            #region Create Object
            oAirline.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Airline.Field_DateCreated]);
             oAirline.DocId = ConvertTo.ConvertToInt(dr[TBNames_Airline.Field_DocId]);
             oAirline.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Airline.Field_IsDeleted]);
             oAirline.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Airline.Field_IsActive]);
             oAirline.IataCode = ConvertTo.ConvertToString(dr[TBNames_Airline.Field_IataCode]);
             oAirline.Name = ConvertTo.ConvertToString(dr[TBNames_Airline.Field_Name]);
 
            #endregion
            Translator<Airline>.Translate(oAirline.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oAirline;
    }

    
#endregion

//#REP_HERE 
#region Airline Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyAirlineDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyAirlineDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyAirlineIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyAirlineIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_IataCode;
private string _iata_code;
public String FriendlyIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineIataCode);   
    }
}
public  string IataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirlineIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineIataCode, value);
         _iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_IataCode = true;
    }
}


public string IataCode_Value
{
    get
    {
        //return _iata_code; //ConvertToValue.ConvertToString(IataCode);
        if(isSetOnce_IataCode) {return _iata_code;}
        else {return ConvertToValue.ConvertToString(IataCode);}
    }
}

public string IataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_iata_code).ToString();
               //if(isSetOnce_IataCode) {return ConvertToValue.ConvertToString(_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(IataCode).ToString();}
            ConvertToValue.ConvertToString(IataCode).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirlineName));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirline.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirline.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirline.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirline.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirline.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_Name", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirline.DateCreated)); 
db.AddParameter(TBNames_Airline.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirline.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirline.DateCreated)); 
db.AddParameter(TBNames_Airline.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirline.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IataCode(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirline.DateCreated)); 
db.AddParameter(TBNames_Airline.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirline.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DateCreated_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Name(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirline.DateCreated)); 
db.AddParameter(TBNames_Airline.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirline.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirline.DocId)); 
db.AddParameter(TBNames_Airline.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirline.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IataCode(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirline.DocId)); 
db.AddParameter(TBNames_Airline.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirline.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DocId_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Name(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirline.DocId)); 
db.AddParameter(TBNames_Airline.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirline.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_IataCode(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirline.IsDeleted)); 
db.AddParameter(TBNames_Airline.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirline.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_IsDeleted_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Name(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirline.IsDeleted)); 
db.AddParameter(TBNames_Airline.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirline.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode_Name(Airline oAirline)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airline.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirline.IataCode)); 
db.AddParameter(TBNames_Airline.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirline.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airline, "Select_Airline_By_Keys_View_IataCode_Name", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
