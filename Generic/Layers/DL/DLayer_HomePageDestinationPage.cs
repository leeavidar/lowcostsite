

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class HomePageDestinationPage  : ContainerItem<HomePageDestinationPage>{

#region CTOR

    #region Constractor
    static HomePageDestinationPage()
    {
        ConvertEvent = HomePageDestinationPage.OnConvert;
    }  
    //public KeyValuesContainer<HomePageDestinationPage> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public HomePageDestinationPage()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.HomePageDestinationPageKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static HomePageDestinationPage OnConvert(DataRow dr)
    {
        int LangId = Translator<HomePageDestinationPage>.LangId;            
        HomePageDestinationPage oHomePageDestinationPage = null;
        if (dr != null)
        {
            oHomePageDestinationPage = new HomePageDestinationPage();
            #region Create Object
            oHomePageDestinationPage.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_HomePageDestinationPage.Field_DateCreated]);
             oHomePageDestinationPage.DocId = ConvertTo.ConvertToInt(dr[TBNames_HomePageDestinationPage.Field_DocId]);
             oHomePageDestinationPage.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_HomePageDestinationPage.Field_IsDeleted]);
             oHomePageDestinationPage.IsActive = ConvertTo.ConvertToBool(dr[TBNames_HomePageDestinationPage.Field_IsActive]);
 
//FK     KeyHomePageDocId
            oHomePageDestinationPage.HomePageDocId = ConvertTo.ConvertToInt(dr[TBNames_HomePageDestinationPage.Field_HomePageDocId]);
             oHomePageDestinationPage.Order = ConvertTo.ConvertToInt(dr[TBNames_HomePageDestinationPage.Field_Order]);
 
//FK     KeyDestinationPageDocId
            oHomePageDestinationPage.DestinationPageDocId = ConvertTo.ConvertToInt(dr[TBNames_HomePageDestinationPage.Field_DestinationPageDocId]);
 
//FK     KeyWhiteLabelDocId
            oHomePageDestinationPage.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_HomePageDestinationPage.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<HomePageDestinationPage>.Translate(oHomePageDestinationPage.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oHomePageDestinationPage;
    }

    
#endregion

//#REP_HERE 
#region HomePageDestinationPage Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageDestinationPageDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyHomePageDestinationPageDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageDestinationPageDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageDestinationPageDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyHomePageDestinationPageDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageDestinationPageDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageDestinationPageIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyHomePageDestinationPageIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageDestinationPageIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageDestinationPageIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyHomePageDestinationPageIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageDestinationPageIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_HomePageDocId;
private int _home_page_doc_id;
public String FriendlyHomePageDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageDocId);   
    }
}
public  int? HomePageDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyHomePageDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageDocId, value);
         _home_page_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_HomePageDocId = true;
    }
}


public int HomePageDocId_Value
{
    get
    {
        //return _home_page_doc_id; //ConvertToValue.ConvertToInt(HomePageDocId);
        if(isSetOnce_HomePageDocId) {return _home_page_doc_id;}
        else {return ConvertToValue.ConvertToInt(HomePageDocId);}
    }
}

public string HomePageDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_home_page_doc_id).ToString();
               //if(isSetOnce_HomePageDocId) {return ConvertToValue.ConvertToInt(_home_page_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(HomePageDocId).ToString();}
            ConvertToValue.ConvertToInt(HomePageDocId).ToString();
    }
}

private bool isSetOnce_Order;
private int _order;
public String FriendlyOrder
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageDestinationPageOrder);   
    }
}
public  int? Order
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyHomePageDestinationPageOrder));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageDestinationPageOrder, value);
         _order = ConvertToValue.ConvertToInt(value);
        isSetOnce_Order = true;
    }
}


public int Order_Value
{
    get
    {
        //return _order; //ConvertToValue.ConvertToInt(Order);
        if(isSetOnce_Order) {return _order;}
        else {return ConvertToValue.ConvertToInt(Order);}
    }
}

public string Order_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order).ToString();
               //if(isSetOnce_Order) {return ConvertToValue.ConvertToInt(_order).ToString();}
               //else {return ConvertToValue.ConvertToInt(Order).ToString();}
            ConvertToValue.ConvertToInt(Order).ToString();
    }
}

private bool isSetOnce_DestinationPageDocId;
private int _destination_page_doc_id;
public String FriendlyDestinationPageDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageDocId);   
    }
}
public  int? DestinationPageDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyDestinationPageDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageDocId, value);
         _destination_page_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DestinationPageDocId = true;
    }
}


public int DestinationPageDocId_Value
{
    get
    {
        //return _destination_page_doc_id; //ConvertToValue.ConvertToInt(DestinationPageDocId);
        if(isSetOnce_DestinationPageDocId) {return _destination_page_doc_id;}
        else {return ConvertToValue.ConvertToInt(DestinationPageDocId);}
    }
}

public string DestinationPageDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_destination_page_doc_id).ToString();
               //if(isSetOnce_DestinationPageDocId) {return ConvertToValue.ConvertToInt(_destination_page_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DestinationPageDocId).ToString();}
            ConvertToValue.ConvertToInt(DestinationPageDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //H_A
        //7_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //H_B
        //7_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_C
        //7_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_E
        //7_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_HomePageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_HomePageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.HomePageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_F
        //7_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_G
        //7_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationPageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DestinationPageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DestinationPageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DestinationPageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_B
        //7_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DateCreated)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_C
        //7_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DateCreated)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_E
        //7_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_HomePageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DateCreated)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_HomePageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.HomePageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_HomePageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_F
        //7_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DateCreated)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_G
        //7_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DestinationPageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DateCreated)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DestinationPageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DestinationPageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationPageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_C
        //7_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_E
        //7_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_HomePageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_HomePageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.HomePageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_HomePageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_F
        //7_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_G
        //7_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DestinationPageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DestinationPageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DestinationPageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_DestinationPageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_E
        //7_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_HomePageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_HomePageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.HomePageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_HomePageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_F
        //7_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_G
        //7_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationPageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DestinationPageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DestinationPageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationPageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_F
        //7_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_HomePageDocId_Order(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_HomePageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.HomePageDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_G
        //7_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_HomePageDocId_DestinationPageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_HomePageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.HomePageDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DestinationPageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DestinationPageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId_DestinationPageDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_F_G
        //7_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_DestinationPageDocId(HomePageDestinationPage oHomePageDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePageDestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.Order)); 
db.AddParameter(TBNames_HomePageDestinationPage.PRM_DestinationPageDocId, ConvertTo.ConvertEmptyToDBNull(oHomePageDestinationPage.DestinationPageDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePageDestinationPage, "Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_Order_DestinationPageDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
