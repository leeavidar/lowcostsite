

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Faq  : ContainerItem<Faq>{

#region CTOR

    #region Constractor
    static Faq()
    {
        ConvertEvent = Faq.OnConvert;
    }  
    //public KeyValuesContainer<Faq> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Faq()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.FaqKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Faq OnConvert(DataRow dr)
    {
        int LangId = Translator<Faq>.LangId;            
        Faq oFaq = null;
        if (dr != null)
        {
            oFaq = new Faq();
            #region Create Object
            oFaq.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Faq.Field_DateCreated]);
             oFaq.DocId = ConvertTo.ConvertToInt(dr[TBNames_Faq.Field_DocId]);
             oFaq.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Faq.Field_IsDeleted]);
             oFaq.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Faq.Field_IsActive]);
             oFaq.Category = ConvertTo.ConvertToInt(dr[TBNames_Faq.Field_Category]);
             oFaq.Question = ConvertTo.ConvertToString(dr[TBNames_Faq.Field_Question]);
             oFaq.Answer = ConvertTo.ConvertToString(dr[TBNames_Faq.Field_Answer]);
 
//FK     KeyWhiteLabelDocId
            oFaq.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Faq.Field_WhiteLabelDocId]);
             oFaq.Order = ConvertTo.ConvertToInt(dr[TBNames_Faq.Field_Order]);
 
            #endregion
            Translator<Faq>.Translate(oFaq.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oFaq;
    }

    
#endregion

//#REP_HERE 
#region Faq Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFaqDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyFaqDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyFaqDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFaqDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyFaqDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyFaqDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFaqIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyFaqIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyFaqIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFaqIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyFaqIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyFaqIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Category;
private int _category;
public String FriendlyCategory
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFaqCategory);   
    }
}
public  int? Category
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyFaqCategory));
    }
    set
    {
        SetKey(KeyValuesType.KeyFaqCategory, value);
         _category = ConvertToValue.ConvertToInt(value);
        isSetOnce_Category = true;
    }
}


public int Category_Value
{
    get
    {
        //return _category; //ConvertToValue.ConvertToInt(Category);
        if(isSetOnce_Category) {return _category;}
        else {return ConvertToValue.ConvertToInt(Category);}
    }
}

public string Category_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_category).ToString();
               //if(isSetOnce_Category) {return ConvertToValue.ConvertToInt(_category).ToString();}
               //else {return ConvertToValue.ConvertToInt(Category).ToString();}
            ConvertToValue.ConvertToInt(Category).ToString();
    }
}

private bool isSetOnce_Question;
private string _question;
public String FriendlyQuestion
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFaqQuestion);   
    }
}
public  string Question
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyFaqQuestion));
    }
    set
    {
        SetKey(KeyValuesType.KeyFaqQuestion, value);
         _question = ConvertToValue.ConvertToString(value);
        isSetOnce_Question = true;
    }
}


public string Question_Value
{
    get
    {
        //return _question; //ConvertToValue.ConvertToString(Question);
        if(isSetOnce_Question) {return _question;}
        else {return ConvertToValue.ConvertToString(Question);}
    }
}

public string Question_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_question).ToString();
               //if(isSetOnce_Question) {return ConvertToValue.ConvertToString(_question).ToString();}
               //else {return ConvertToValue.ConvertToString(Question).ToString();}
            ConvertToValue.ConvertToString(Question).ToString();
    }
}

private bool isSetOnce_Answer;
private string _answer;
public String FriendlyAnswer
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFaqAnswer);   
    }
}
public  string Answer
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyFaqAnswer));
    }
    set
    {
        SetKey(KeyValuesType.KeyFaqAnswer, value);
         _answer = ConvertToValue.ConvertToString(value);
        isSetOnce_Answer = true;
    }
}


public string Answer_Value
{
    get
    {
        //return _answer; //ConvertToValue.ConvertToString(Answer);
        if(isSetOnce_Answer) {return _answer;}
        else {return ConvertToValue.ConvertToString(Answer);}
    }
}

public string Answer_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_answer).ToString();
               //if(isSetOnce_Answer) {return ConvertToValue.ConvertToString(_answer).ToString();}
               //else {return ConvertToValue.ConvertToString(Answer).ToString();}
            ConvertToValue.ConvertToString(Answer).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_Order;
private int _order;
public String FriendlyOrder
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFaqOrder);   
    }
}
public  int? Order
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyFaqOrder));
    }
    set
    {
        SetKey(KeyValuesType.KeyFaqOrder, value);
         _order = ConvertToValue.ConvertToInt(value);
        isSetOnce_Order = true;
    }
}


public int Order_Value
{
    get
    {
        //return _order; //ConvertToValue.ConvertToInt(Order);
        if(isSetOnce_Order) {return _order;}
        else {return ConvertToValue.ConvertToInt(Order);}
    }
}

public string Order_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order).ToString();
               //if(isSetOnce_Order) {return ConvertToValue.ConvertToInt(_order).ToString();}
               //else {return ConvertToValue.ConvertToInt(Order).ToString();}
            ConvertToValue.ConvertToInt(Order).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //H_A
        //7_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFaq.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //H_B
        //7_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFaq.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_C
        //7_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFaq.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_E
        //7_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Category(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Category, ConvertTo.ConvertEmptyToDBNull(oFaq.Category));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Category", ex.Message));

            }
            return paramsSelect;
        }



        //H_F
        //7_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Question(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Question, ConvertTo.ConvertEmptyToDBNull(oFaq.Question));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Question", ex.Message));

            }
            return paramsSelect;
        }



        //H_G
        //7_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Answer(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Answer, ConvertTo.ConvertEmptyToDBNull(oFaq.Answer));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Answer", ex.Message));

            }
            return paramsSelect;
        }



        //H_I
        //7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oFaq.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_B
        //7_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFaq.DateCreated)); 
db.AddParameter(TBNames_Faq.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFaq.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_C
        //7_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFaq.DateCreated)); 
db.AddParameter(TBNames_Faq.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFaq.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_E
        //7_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Category(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFaq.DateCreated)); 
db.AddParameter(TBNames_Faq.PRM_Category, ConvertTo.ConvertEmptyToDBNull(oFaq.Category));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Category", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_F
        //7_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Question(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFaq.DateCreated)); 
db.AddParameter(TBNames_Faq.PRM_Question, ConvertTo.ConvertEmptyToDBNull(oFaq.Question));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Question", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_G
        //7_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Answer(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFaq.DateCreated)); 
db.AddParameter(TBNames_Faq.PRM_Answer, ConvertTo.ConvertEmptyToDBNull(oFaq.Answer));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Answer", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_I
        //7_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFaq.DateCreated)); 
db.AddParameter(TBNames_Faq.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oFaq.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_C
        //7_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFaq.DocId)); 
db.AddParameter(TBNames_Faq.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFaq.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_E
        //7_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Category(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFaq.DocId)); 
db.AddParameter(TBNames_Faq.PRM_Category, ConvertTo.ConvertEmptyToDBNull(oFaq.Category));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Category", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_F
        //7_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Question(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFaq.DocId)); 
db.AddParameter(TBNames_Faq.PRM_Question, ConvertTo.ConvertEmptyToDBNull(oFaq.Question));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Question", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_G
        //7_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Answer(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFaq.DocId)); 
db.AddParameter(TBNames_Faq.PRM_Answer, ConvertTo.ConvertEmptyToDBNull(oFaq.Answer));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Answer", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_I
        //7_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFaq.DocId)); 
db.AddParameter(TBNames_Faq.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oFaq.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_E
        //7_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Category(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFaq.IsDeleted)); 
db.AddParameter(TBNames_Faq.PRM_Category, ConvertTo.ConvertEmptyToDBNull(oFaq.Category));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Category", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_F
        //7_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Question(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFaq.IsDeleted)); 
db.AddParameter(TBNames_Faq.PRM_Question, ConvertTo.ConvertEmptyToDBNull(oFaq.Question));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Question", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_G
        //7_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Answer(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFaq.IsDeleted)); 
db.AddParameter(TBNames_Faq.PRM_Answer, ConvertTo.ConvertEmptyToDBNull(oFaq.Answer));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Answer", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_I
        //7_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFaq.IsDeleted)); 
db.AddParameter(TBNames_Faq.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oFaq.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_F
        //7_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Category_Question(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Category, ConvertTo.ConvertEmptyToDBNull(oFaq.Category)); 
db.AddParameter(TBNames_Faq.PRM_Question, ConvertTo.ConvertEmptyToDBNull(oFaq.Question));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Question", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_G
        //7_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Category_Answer(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Category, ConvertTo.ConvertEmptyToDBNull(oFaq.Category)); 
db.AddParameter(TBNames_Faq.PRM_Answer, ConvertTo.ConvertEmptyToDBNull(oFaq.Answer));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Answer", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_I
        //7_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Category_Order(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Category, ConvertTo.ConvertEmptyToDBNull(oFaq.Category)); 
db.AddParameter(TBNames_Faq.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oFaq.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_F_G
        //7_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Question_Answer(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Question, ConvertTo.ConvertEmptyToDBNull(oFaq.Question)); 
db.AddParameter(TBNames_Faq.PRM_Answer, ConvertTo.ConvertEmptyToDBNull(oFaq.Answer));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Question_Answer", ex.Message));

            }
            return paramsSelect;
        }



        //H_F_I
        //7_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Question_Order(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Question, ConvertTo.ConvertEmptyToDBNull(oFaq.Question)); 
db.AddParameter(TBNames_Faq.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oFaq.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Question_Order", ex.Message));

            }
            return paramsSelect;
        }



        //H_G_I
        //7_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Answer_Order(Faq oFaq)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Faq.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFaq.WhiteLabelDocId)); 
db.AddParameter(TBNames_Faq.PRM_Answer, ConvertTo.ConvertEmptyToDBNull(oFaq.Answer)); 
db.AddParameter(TBNames_Faq.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oFaq.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Faq, "Select_Faq_By_Keys_View_WhiteLabelDocId_Answer_Order", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
