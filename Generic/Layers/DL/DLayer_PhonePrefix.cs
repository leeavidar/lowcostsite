

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class PhonePrefix  : ContainerItem<PhonePrefix>{

#region CTOR

    #region Constractor
    static PhonePrefix()
    {
        ConvertEvent = PhonePrefix.OnConvert;
    }  
    //public KeyValuesContainer<PhonePrefix> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public PhonePrefix()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.PhonePrefixKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static PhonePrefix OnConvert(DataRow dr)
    {
        int LangId = Translator<PhonePrefix>.LangId;            
        PhonePrefix oPhonePrefix = null;
        if (dr != null)
        {
            oPhonePrefix = new PhonePrefix();
            #region Create Object
            oPhonePrefix.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_PhonePrefix.Field_DateCreated]);
             oPhonePrefix.DocId = ConvertTo.ConvertToInt(dr[TBNames_PhonePrefix.Field_DocId]);
             oPhonePrefix.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_PhonePrefix.Field_IsDeleted]);
             oPhonePrefix.IsActive = ConvertTo.ConvertToBool(dr[TBNames_PhonePrefix.Field_IsActive]);
             oPhonePrefix.PrefixNumber = ConvertTo.ConvertToString(dr[TBNames_PhonePrefix.Field_PrefixNumber]);
 
            #endregion
            Translator<PhonePrefix>.Translate(oPhonePrefix.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oPhonePrefix;
    }

    
#endregion

//#REP_HERE 
#region PhonePrefix Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPhonePrefixDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyPhonePrefixDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyPhonePrefixDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPhonePrefixDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyPhonePrefixDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyPhonePrefixDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPhonePrefixIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyPhonePrefixIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyPhonePrefixIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPhonePrefixIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyPhonePrefixIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyPhonePrefixIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_PrefixNumber;
private string _prefix_number;
public String FriendlyPrefixNumber
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPhonePrefixPrefixNumber);   
    }
}
public  string PrefixNumber
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPhonePrefixPrefixNumber));
    }
    set
    {
        SetKey(KeyValuesType.KeyPhonePrefixPrefixNumber, value);
         _prefix_number = ConvertToValue.ConvertToString(value);
        isSetOnce_PrefixNumber = true;
    }
}


public string PrefixNumber_Value
{
    get
    {
        //return _prefix_number; //ConvertToValue.ConvertToString(PrefixNumber);
        if(isSetOnce_PrefixNumber) {return _prefix_number;}
        else {return ConvertToValue.ConvertToString(PrefixNumber);}
    }
}

public string PrefixNumber_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_prefix_number).ToString();
               //if(isSetOnce_PrefixNumber) {return ConvertToValue.ConvertToString(_prefix_number).ToString();}
               //else {return ConvertToValue.ConvertToString(PrefixNumber).ToString();}
            ConvertToValue.ConvertToString(PrefixNumber).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_PrefixNumber(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_PrefixNumber, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.PrefixNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_PrefixNumber", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.DateCreated)); 
db.AddParameter(TBNames_PhonePrefix.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.DateCreated)); 
db.AddParameter(TBNames_PhonePrefix.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_PrefixNumber(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.DateCreated)); 
db.AddParameter(TBNames_PhonePrefix.PRM_PrefixNumber, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.PrefixNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_DateCreated_PrefixNumber", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.DocId)); 
db.AddParameter(TBNames_PhonePrefix.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_PrefixNumber(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.DocId)); 
db.AddParameter(TBNames_PhonePrefix.PRM_PrefixNumber, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.PrefixNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_DocId_PrefixNumber", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_PrefixNumber(PhonePrefix oPhonePrefix)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PhonePrefix.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.IsDeleted)); 
db.AddParameter(TBNames_PhonePrefix.PRM_PrefixNumber, ConvertTo.ConvertEmptyToDBNull(oPhonePrefix.PrefixNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PhonePrefix, "Select_PhonePrefix_By_Keys_View_IsDeleted_PrefixNumber", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
