

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Users  : ContainerItem<Users>{

#region CTOR

    #region Constractor
    static Users()
    {
        ConvertEvent = Users.OnConvert;
    }  
    //public KeyValuesContainer<Users> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Users()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.UsersKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Users OnConvert(DataRow dr)
    {
        int LangId = Translator<Users>.LangId;            
        Users oUsers = null;
        if (dr != null)
        {
            oUsers = new Users();
            #region Create Object
            oUsers.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Users.Field_DateCreated]);
             oUsers.DocId = ConvertTo.ConvertToInt(dr[TBNames_Users.Field_DocId]);
             oUsers.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Users.Field_IsDeleted]);
             oUsers.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Users.Field_IsActive]);
             oUsers.UserName = ConvertTo.ConvertToString(dr[TBNames_Users.Field_UserName]);
             oUsers.Password = ConvertTo.ConvertToString(dr[TBNames_Users.Field_Password]);
             oUsers.Email = ConvertTo.ConvertToString(dr[TBNames_Users.Field_Email]);
             oUsers.FirstName = ConvertTo.ConvertToString(dr[TBNames_Users.Field_FirstName]);
             oUsers.LastName = ConvertTo.ConvertToString(dr[TBNames_Users.Field_LastName]);
             oUsers.Role = ConvertTo.ConvertToInt(dr[TBNames_Users.Field_Role]);
 
//FK     KeyWhiteLabelDocId
            oUsers.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Users.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<Users>.Translate(oUsers.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oUsers;
    }

    
#endregion

//#REP_HERE 
#region Users Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyUsersDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyUsersDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyUsersIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyUsersIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_UserName;
private string _user_name;
public String FriendlyUserName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersUserName);   
    }
}
public  string UserName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyUsersUserName));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersUserName, value);
         _user_name = ConvertToValue.ConvertToString(value);
        isSetOnce_UserName = true;
    }
}


public string UserName_Value
{
    get
    {
        //return _user_name; //ConvertToValue.ConvertToString(UserName);
        if(isSetOnce_UserName) {return _user_name;}
        else {return ConvertToValue.ConvertToString(UserName);}
    }
}

public string UserName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_user_name).ToString();
               //if(isSetOnce_UserName) {return ConvertToValue.ConvertToString(_user_name).ToString();}
               //else {return ConvertToValue.ConvertToString(UserName).ToString();}
            ConvertToValue.ConvertToString(UserName).ToString();
    }
}

private bool isSetOnce_Password;
private string _password;
public String FriendlyPassword
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersPassword);   
    }
}
public  string Password
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyUsersPassword));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersPassword, value);
         _password = ConvertToValue.ConvertToString(value);
        isSetOnce_Password = true;
    }
}


public string Password_Value
{
    get
    {
        //return _password; //ConvertToValue.ConvertToString(Password);
        if(isSetOnce_Password) {return _password;}
        else {return ConvertToValue.ConvertToString(Password);}
    }
}

public string Password_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_password).ToString();
               //if(isSetOnce_Password) {return ConvertToValue.ConvertToString(_password).ToString();}
               //else {return ConvertToValue.ConvertToString(Password).ToString();}
            ConvertToValue.ConvertToString(Password).ToString();
    }
}

private bool isSetOnce_Email;
private string _email;
public String FriendlyEmail
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersEmail);   
    }
}
public  string Email
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyUsersEmail));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersEmail, value);
         _email = ConvertToValue.ConvertToString(value);
        isSetOnce_Email = true;
    }
}


public string Email_Value
{
    get
    {
        //return _email; //ConvertToValue.ConvertToString(Email);
        if(isSetOnce_Email) {return _email;}
        else {return ConvertToValue.ConvertToString(Email);}
    }
}

public string Email_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_email).ToString();
               //if(isSetOnce_Email) {return ConvertToValue.ConvertToString(_email).ToString();}
               //else {return ConvertToValue.ConvertToString(Email).ToString();}
            ConvertToValue.ConvertToString(Email).ToString();
    }
}

private bool isSetOnce_FirstName;
private string _first_name;
public String FriendlyFirstName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersFirstName);   
    }
}
public  string FirstName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyUsersFirstName));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersFirstName, value);
         _first_name = ConvertToValue.ConvertToString(value);
        isSetOnce_FirstName = true;
    }
}


public string FirstName_Value
{
    get
    {
        //return _first_name; //ConvertToValue.ConvertToString(FirstName);
        if(isSetOnce_FirstName) {return _first_name;}
        else {return ConvertToValue.ConvertToString(FirstName);}
    }
}

public string FirstName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_first_name).ToString();
               //if(isSetOnce_FirstName) {return ConvertToValue.ConvertToString(_first_name).ToString();}
               //else {return ConvertToValue.ConvertToString(FirstName).ToString();}
            ConvertToValue.ConvertToString(FirstName).ToString();
    }
}

private bool isSetOnce_LastName;
private string _last_name;
public String FriendlyLastName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersLastName);   
    }
}
public  string LastName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyUsersLastName));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersLastName, value);
         _last_name = ConvertToValue.ConvertToString(value);
        isSetOnce_LastName = true;
    }
}


public string LastName_Value
{
    get
    {
        //return _last_name; //ConvertToValue.ConvertToString(LastName);
        if(isSetOnce_LastName) {return _last_name;}
        else {return ConvertToValue.ConvertToString(LastName);}
    }
}

public string LastName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_last_name).ToString();
               //if(isSetOnce_LastName) {return ConvertToValue.ConvertToString(_last_name).ToString();}
               //else {return ConvertToValue.ConvertToString(LastName).ToString();}
            ConvertToValue.ConvertToString(LastName).ToString();
    }
}

private bool isSetOnce_Role;
private int _role;
public String FriendlyRole
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyUsersRole);   
    }
}
public  int? Role
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyUsersRole));
    }
    set
    {
        SetKey(KeyValuesType.KeyUsersRole, value);
         _role = ConvertToValue.ConvertToInt(value);
        isSetOnce_Role = true;
    }
}


public int Role_Value
{
    get
    {
        //return _role; //ConvertToValue.ConvertToInt(Role);
        if(isSetOnce_Role) {return _role;}
        else {return ConvertToValue.ConvertToInt(Role);}
    }
}

public string Role_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_role).ToString();
               //if(isSetOnce_Role) {return ConvertToValue.ConvertToInt(_role).ToString();}
               //else {return ConvertToValue.ConvertToInt(Role).ToString();}
            ConvertToValue.ConvertToInt(Role).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //K_A
        //10_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //K_B
        //10_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //K_C
        //10_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //K_E
        //10_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_UserName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_UserName", ex.Message));

            }
            return paramsSelect;
        }



        //K_F
        //10_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Password(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Password", ex.Message));

            }
            return paramsSelect;
        }



        //K_G
        //10_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Email", ex.Message));

            }
            return paramsSelect;
        }



        //K_H
        //10_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //K_I
        //10_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //K_J
        //10_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Role", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_B
        //10_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_C
        //10_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_E
        //10_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_UserName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_UserName", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_F
        //10_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Password(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Password", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_G
        //10_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Email(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Email", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_H
        //10_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_I
        //10_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LastName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //K_A_J
        //10_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oUsers.DateCreated)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Role", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_C
        //10_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_E
        //10_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_UserName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DocId_UserName", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_F
        //10_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Password(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Password", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_G
        //10_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Email(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Email", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_H
        //10_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FirstName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DocId_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_I
        //10_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LastName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DocId_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //K_B_J
        //10_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oUsers.DocId)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Role", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_E
        //10_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_UserName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_UserName", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_F
        //10_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Password(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Password", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_G
        //10_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Email(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Email", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_H
        //10_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_I
        //10_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //K_C_J
        //10_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oUsers.IsDeleted)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Role", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_F
        //10_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_Password(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Password", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_G
        //10_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_Email(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Email", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_H
        //10_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_FirstName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_UserName_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_I
        //10_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_LastName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_UserName_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //K_E_J
        //10_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_UserName, ConvertTo.ConvertEmptyToDBNull(oUsers.UserName)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Role", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_G
        //10_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Password_Email(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Password_Email", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_H
        //10_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Password_FirstName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Password_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_I
        //10_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Password_LastName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Password_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //K_F_J
        //10_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Password_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Password, ConvertTo.ConvertEmptyToDBNull(oUsers.Password)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Password_Role", ex.Message));

            }
            return paramsSelect;
        }



        //K_G_H
        //10_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email_FirstName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Email_FirstName", ex.Message));

            }
            return paramsSelect;
        }



        //K_G_I
        //10_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email_LastName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Email_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //K_G_J
        //10_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Email_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oUsers.Email)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_Email_Role", ex.Message));

            }
            return paramsSelect;
        }



        //K_H_I
        //10_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_LastName(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_FirstName_LastName", ex.Message));

            }
            return paramsSelect;
        }



        //K_H_J
        //10_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_FirstName, ConvertTo.ConvertEmptyToDBNull(oUsers.FirstName)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_FirstName_Role", ex.Message));

            }
            return paramsSelect;
        }



        //K_I_J
        //10_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Role(Users oUsers)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Users.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oUsers.WhiteLabelDocId)); 
db.AddParameter(TBNames_Users.PRM_LastName, ConvertTo.ConvertEmptyToDBNull(oUsers.LastName)); 
db.AddParameter(TBNames_Users.PRM_Role, ConvertTo.ConvertEmptyToDBNull(oUsers.Role));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Users, "Select_Users_By_Keys_View_WhiteLabelDocId_LastName_Role", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
