

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class HomePage  : ContainerItem<HomePage>{

#region CTOR

    #region Constractor
    static HomePage()
    {
        ConvertEvent = HomePage.OnConvert;
    }  
    //public KeyValuesContainer<HomePage> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public HomePage()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.HomePageKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static HomePage OnConvert(DataRow dr)
    {
        int LangId = Translator<HomePage>.LangId;            
        HomePage oHomePage = null;
        if (dr != null)
        {
            oHomePage = new HomePage();
            #region Create Object
            oHomePage.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_HomePage.Field_DateCreated]);
             oHomePage.DocId = ConvertTo.ConvertToInt(dr[TBNames_HomePage.Field_DocId]);
             oHomePage.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_HomePage.Field_IsDeleted]);
             oHomePage.IsActive = ConvertTo.ConvertToBool(dr[TBNames_HomePage.Field_IsActive]);
             oHomePage.TextLine1 = ConvertTo.ConvertToString(dr[TBNames_HomePage.Field_TextLine1]);
             oHomePage.TextLine2 = ConvertTo.ConvertToString(dr[TBNames_HomePage.Field_TextLine2]);
             oHomePage.TextLine3 = ConvertTo.ConvertToString(dr[TBNames_HomePage.Field_TextLine3]);
             oHomePage.TextLine4 = ConvertTo.ConvertToString(dr[TBNames_HomePage.Field_TextLine4]);
 
//FK     KeyWhiteLabelDocId
            oHomePage.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_HomePage.Field_WhiteLabelDocId]);
 
//FK     KeySeoDocId
            oHomePage.SeoDocId = ConvertTo.ConvertToInt(dr[TBNames_HomePage.Field_SeoDocId]);
 
//FK     KeyTipDocId
            oHomePage.TipDocId = ConvertTo.ConvertToInt(dr[TBNames_HomePage.Field_TipDocId]);
 
            #endregion
            Translator<HomePage>.Translate(oHomePage.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oHomePage;
    }

    
#endregion

//#REP_HERE 
#region HomePage Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyHomePageDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyHomePageDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyHomePageIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyHomePageIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_TextLine1;
private string _text_line_1;
public String FriendlyTextLine1
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageTextLine1);   
    }
}
public  string TextLine1
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyHomePageTextLine1));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageTextLine1, value);
         _text_line_1 = ConvertToValue.ConvertToString(value);
        isSetOnce_TextLine1 = true;
    }
}


public string TextLine1_Value
{
    get
    {
        //return _text_line_1; //ConvertToValue.ConvertToString(TextLine1);
        if(isSetOnce_TextLine1) {return _text_line_1;}
        else {return ConvertToValue.ConvertToString(TextLine1);}
    }
}

public string TextLine1_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text_line_1).ToString();
               //if(isSetOnce_TextLine1) {return ConvertToValue.ConvertToString(_text_line_1).ToString();}
               //else {return ConvertToValue.ConvertToString(TextLine1).ToString();}
            ConvertToValue.ConvertToString(TextLine1).ToString();
    }
}

private bool isSetOnce_TextLine2;
private string _text_line_2;
public String FriendlyTextLine2
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageTextLine2);   
    }
}
public  string TextLine2
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyHomePageTextLine2));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageTextLine2, value);
         _text_line_2 = ConvertToValue.ConvertToString(value);
        isSetOnce_TextLine2 = true;
    }
}


public string TextLine2_Value
{
    get
    {
        //return _text_line_2; //ConvertToValue.ConvertToString(TextLine2);
        if(isSetOnce_TextLine2) {return _text_line_2;}
        else {return ConvertToValue.ConvertToString(TextLine2);}
    }
}

public string TextLine2_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text_line_2).ToString();
               //if(isSetOnce_TextLine2) {return ConvertToValue.ConvertToString(_text_line_2).ToString();}
               //else {return ConvertToValue.ConvertToString(TextLine2).ToString();}
            ConvertToValue.ConvertToString(TextLine2).ToString();
    }
}

private bool isSetOnce_TextLine3;
private string _text_line_3;
public String FriendlyTextLine3
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageTextLine3);   
    }
}
public  string TextLine3
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyHomePageTextLine3));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageTextLine3, value);
         _text_line_3 = ConvertToValue.ConvertToString(value);
        isSetOnce_TextLine3 = true;
    }
}


public string TextLine3_Value
{
    get
    {
        //return _text_line_3; //ConvertToValue.ConvertToString(TextLine3);
        if(isSetOnce_TextLine3) {return _text_line_3;}
        else {return ConvertToValue.ConvertToString(TextLine3);}
    }
}

public string TextLine3_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text_line_3).ToString();
               //if(isSetOnce_TextLine3) {return ConvertToValue.ConvertToString(_text_line_3).ToString();}
               //else {return ConvertToValue.ConvertToString(TextLine3).ToString();}
            ConvertToValue.ConvertToString(TextLine3).ToString();
    }
}

private bool isSetOnce_TextLine4;
private string _text_line_4;
public String FriendlyTextLine4
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyHomePageTextLine4);   
    }
}
public  string TextLine4
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyHomePageTextLine4));
    }
    set
    {
        SetKey(KeyValuesType.KeyHomePageTextLine4, value);
         _text_line_4 = ConvertToValue.ConvertToString(value);
        isSetOnce_TextLine4 = true;
    }
}


public string TextLine4_Value
{
    get
    {
        //return _text_line_4; //ConvertToValue.ConvertToString(TextLine4);
        if(isSetOnce_TextLine4) {return _text_line_4;}
        else {return ConvertToValue.ConvertToString(TextLine4);}
    }
}

public string TextLine4_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text_line_4).ToString();
               //if(isSetOnce_TextLine4) {return ConvertToValue.ConvertToString(_text_line_4).ToString();}
               //else {return ConvertToValue.ConvertToString(TextLine4).ToString();}
            ConvertToValue.ConvertToString(TextLine4).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_SeoDocId;
private int _seo_doc_id;
public String FriendlySeoDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoDocId);   
    }
}
public  int? SeoDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySeoDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoDocId, value);
         _seo_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_SeoDocId = true;
    }
}


public int SeoDocId_Value
{
    get
    {
        //return _seo_doc_id; //ConvertToValue.ConvertToInt(SeoDocId);
        if(isSetOnce_SeoDocId) {return _seo_doc_id;}
        else {return ConvertToValue.ConvertToInt(SeoDocId);}
    }
}

public string SeoDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_seo_doc_id).ToString();
               //if(isSetOnce_SeoDocId) {return ConvertToValue.ConvertToInt(_seo_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(SeoDocId).ToString();}
            ConvertToValue.ConvertToInt(SeoDocId).ToString();
    }
}

private bool isSetOnce_TipDocId;
private int _tip_doc_id;
public String FriendlyTipDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipDocId);   
    }
}
public  int? TipDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyTipDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipDocId, value);
         _tip_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_TipDocId = true;
    }
}


public int TipDocId_Value
{
    get
    {
        //return _tip_doc_id; //ConvertToValue.ConvertToInt(TipDocId);
        if(isSetOnce_TipDocId) {return _tip_doc_id;}
        else {return ConvertToValue.ConvertToInt(TipDocId);}
    }
}

public string TipDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_tip_doc_id).ToString();
               //if(isSetOnce_TipDocId) {return ConvertToValue.ConvertToInt(_tip_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(TipDocId).ToString();}
            ConvertToValue.ConvertToInt(TipDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //I_A
        //8_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //I_B
        //8_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C
        //8_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_E
        //8_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1", ex.Message));

            }
            return paramsSelect;
        }



        //I_F
        //8_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2", ex.Message));

            }
            return paramsSelect;
        }



        //I_G
        //8_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine3(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3", ex.Message));

            }
            return paramsSelect;
        }



        //I_H
        //8_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine4(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4", ex.Message));

            }
            return paramsSelect;
        }



        //I_J
        //8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_K
        //8_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TipDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_B
        //8_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_C
        //8_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_E
        //8_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLine1(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine1", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_F
        //8_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLine2(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine2", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_G
        //8_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLine3(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine3", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_H
        //8_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLine4(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine4", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_J
        //8_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_K
        //8_0_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oHomePage.DateCreated)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TipDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_C
        //8_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_E
        //8_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLine1(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine1", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_F
        //8_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLine2(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine2", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_G
        //8_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLine3(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine3", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_H
        //8_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLine4(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine4", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_J
        //8_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_K
        //8_1_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.DocId)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TipDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_E
        //8_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine1(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine1", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_F
        //8_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine2(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine2", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_G
        //8_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine3(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine3", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_H
        //8_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine4(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine4", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_J
        //8_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_K
        //8_2_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oHomePage.IsDeleted)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TipDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_F
        //8_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_TextLine2(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine2", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_G
        //8_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_TextLine3(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine3", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_H
        //8_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_TextLine4(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine4", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_J
        //8_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_SeoDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_K
        //8_4_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine1, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine1)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TipDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_G
        //8_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2_TextLine3(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TextLine3", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_H
        //8_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2_TextLine4(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TextLine4", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_J
        //8_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2_SeoDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_K
        //8_5_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine2, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine2)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TipDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_H
        //8_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine3_TextLine4(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_TextLine4", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_J
        //8_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine3_SeoDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_K
        //8_6_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine3_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine3, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine3)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_TipDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_H_J
        //8_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine4_SeoDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_H_K
        //8_7_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine4_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TextLine4, ConvertTo.ConvertEmptyToDBNull(oHomePage.TextLine4)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4_TipDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_J_K
        //8_9_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_TipDocId(HomePage oHomePage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_HomePage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.WhiteLabelDocId)); 
db.AddParameter(TBNames_HomePage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.SeoDocId)); 
db.AddParameter(TBNames_HomePage.PRM_TipDocId, ConvertTo.ConvertEmptyToDBNull(oHomePage.TipDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.HomePage, "Select_HomePage_By_Keys_View_WhiteLabelDocId_SeoDocId_TipDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
