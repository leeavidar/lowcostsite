

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class CompanyPage  : ContainerItem<CompanyPage>{

#region CTOR

    #region Constractor
    static CompanyPage()
    {
        ConvertEvent = CompanyPage.OnConvert;
    }  
    //public KeyValuesContainer<CompanyPage> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public CompanyPage()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.CompanyPageKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static CompanyPage OnConvert(DataRow dr)
    {
        int LangId = Translator<CompanyPage>.LangId;            
        CompanyPage oCompanyPage = null;
        if (dr != null)
        {
            oCompanyPage = new CompanyPage();
            #region Create Object
            oCompanyPage.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_CompanyPage.Field_DateCreated]);
             oCompanyPage.DocId = ConvertTo.ConvertToInt(dr[TBNames_CompanyPage.Field_DocId]);
             oCompanyPage.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_CompanyPage.Field_IsDeleted]);
             oCompanyPage.IsActive = ConvertTo.ConvertToBool(dr[TBNames_CompanyPage.Field_IsActive]);
             oCompanyPage.Name = ConvertTo.ConvertToString(dr[TBNames_CompanyPage.Field_Name]);
 
//FK     KeySeoDocId
            oCompanyPage.SeoDocId = ConvertTo.ConvertToInt(dr[TBNames_CompanyPage.Field_SeoDocId]);
 
//FK     KeyWhiteLabelDocId
            oCompanyPage.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_CompanyPage.Field_WhiteLabelDocId]);
             oCompanyPage.Text = ConvertTo.ConvertToString(dr[TBNames_CompanyPage.Field_Text]);
             oCompanyPage.Title = ConvertTo.ConvertToString(dr[TBNames_CompanyPage.Field_Title]);
 
//FK     KeyAirlineDocId
            oCompanyPage.AirlineDocId = ConvertTo.ConvertToInt(dr[TBNames_CompanyPage.Field_AirlineDocId]);
 
            #endregion
            Translator<CompanyPage>.Translate(oCompanyPage.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oCompanyPage;
    }

    
#endregion

//#REP_HERE 
#region CompanyPage Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCompanyPageDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyCompanyPageDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyCompanyPageDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCompanyPageDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyCompanyPageDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyCompanyPageDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCompanyPageIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyCompanyPageIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyCompanyPageIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCompanyPageIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyCompanyPageIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyCompanyPageIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCompanyPageName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCompanyPageName));
    }
    set
    {
        SetKey(KeyValuesType.KeyCompanyPageName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_SeoDocId;
private int _seo_doc_id;
public String FriendlySeoDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoDocId);   
    }
}
public  int? SeoDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySeoDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoDocId, value);
         _seo_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_SeoDocId = true;
    }
}


public int SeoDocId_Value
{
    get
    {
        //return _seo_doc_id; //ConvertToValue.ConvertToInt(SeoDocId);
        if(isSetOnce_SeoDocId) {return _seo_doc_id;}
        else {return ConvertToValue.ConvertToInt(SeoDocId);}
    }
}

public string SeoDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_seo_doc_id).ToString();
               //if(isSetOnce_SeoDocId) {return ConvertToValue.ConvertToInt(_seo_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(SeoDocId).ToString();}
            ConvertToValue.ConvertToInt(SeoDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_Text;
private string _text;
public String FriendlyText
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCompanyPageText);   
    }
}
public  string Text
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCompanyPageText));
    }
    set
    {
        SetKey(KeyValuesType.KeyCompanyPageText, value);
         _text = ConvertToValue.ConvertToString(value);
        isSetOnce_Text = true;
    }
}


public string Text_Value
{
    get
    {
        //return _text; //ConvertToValue.ConvertToString(Text);
        if(isSetOnce_Text) {return _text;}
        else {return ConvertToValue.ConvertToString(Text);}
    }
}

public string Text_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text).ToString();
               //if(isSetOnce_Text) {return ConvertToValue.ConvertToString(_text).ToString();}
               //else {return ConvertToValue.ConvertToString(Text).ToString();}
            ConvertToValue.ConvertToString(Text).ToString();
    }
}

private bool isSetOnce_Title;
private string _title;
public String FriendlyTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCompanyPageTitle);   
    }
}
public  string Title
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCompanyPageTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyCompanyPageTitle, value);
         _title = ConvertToValue.ConvertToString(value);
        isSetOnce_Title = true;
    }
}


public string Title_Value
{
    get
    {
        //return _title; //ConvertToValue.ConvertToString(Title);
        if(isSetOnce_Title) {return _title;}
        else {return ConvertToValue.ConvertToString(Title);}
    }
}

public string Title_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_title).ToString();
               //if(isSetOnce_Title) {return ConvertToValue.ConvertToString(_title).ToString();}
               //else {return ConvertToValue.ConvertToString(Title).ToString();}
            ConvertToValue.ConvertToString(Title).ToString();
    }
}

private bool isSetOnce_AirlineDocId;
private int _airline_doc_id;
public String FriendlyAirlineDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineDocId);   
    }
}
public  int? AirlineDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyAirlineDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineDocId, value);
         _airline_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_AirlineDocId = true;
    }
}


public int AirlineDocId_Value
{
    get
    {
        //return _airline_doc_id; //ConvertToValue.ConvertToInt(AirlineDocId);
        if(isSetOnce_AirlineDocId) {return _airline_doc_id;}
        else {return ConvertToValue.ConvertToInt(AirlineDocId);}
    }
}

public string AirlineDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_airline_doc_id).ToString();
               //if(isSetOnce_AirlineDocId) {return ConvertToValue.ConvertToInt(_airline_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(AirlineDocId).ToString();}
            ConvertToValue.ConvertToInt(AirlineDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //G_A
        //6_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //G_B
        //6_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_C
        //6_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //G_E
        //6_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_F
        //6_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_H
        //6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //G_I
        //6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_J
        //6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_AirlineDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.AirlineDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_AirlineDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_B
        //6_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DateCreated)); 
db.AddParameter(TBNames_CompanyPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_C
        //6_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DateCreated)); 
db.AddParameter(TBNames_CompanyPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_E
        //6_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DateCreated)); 
db.AddParameter(TBNames_CompanyPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_F
        //6_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DateCreated)); 
db.AddParameter(TBNames_CompanyPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_H
        //6_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Text(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DateCreated)); 
db.AddParameter(TBNames_CompanyPage.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Text", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_I
        //6_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DateCreated)); 
db.AddParameter(TBNames_CompanyPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_J
        //6_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AirlineDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DateCreated)); 
db.AddParameter(TBNames_CompanyPage.PRM_AirlineDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.AirlineDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_C
        //6_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_E
        //6_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_F
        //6_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_H
        //6_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Text(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_I
        //6_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_J
        //6_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AirlineDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.DocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_AirlineDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.AirlineDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_AirlineDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_E
        //6_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.IsDeleted)); 
db.AddParameter(TBNames_CompanyPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_F
        //6_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.IsDeleted)); 
db.AddParameter(TBNames_CompanyPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_H
        //6_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Text(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.IsDeleted)); 
db.AddParameter(TBNames_CompanyPage.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Text", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_I
        //6_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.IsDeleted)); 
db.AddParameter(TBNames_CompanyPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_J
        //6_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.IsDeleted)); 
db.AddParameter(TBNames_CompanyPage.PRM_AirlineDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.AirlineDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_F
        //6_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_SeoDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Name)); 
db.AddParameter(TBNames_CompanyPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_H
        //6_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Text(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Name)); 
db.AddParameter(TBNames_CompanyPage.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_Text", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_I
        //6_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Title(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Name)); 
db.AddParameter(TBNames_CompanyPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_J
        //6_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_AirlineDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Name)); 
db.AddParameter(TBNames_CompanyPage.PRM_AirlineDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.AirlineDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_AirlineDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_H
        //6_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Text(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.SeoDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_I
        //6_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Title(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.SeoDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_J
        //6_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_AirlineDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.SeoDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_AirlineDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.AirlineDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_AirlineDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_H_I
        //6_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Title(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Text)); 
db.AddParameter(TBNames_CompanyPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_H_J
        //6_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_AirlineDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Text)); 
db.AddParameter(TBNames_CompanyPage.PRM_AirlineDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.AirlineDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text_AirlineDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_I_J
        //6_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_AirlineDocId(CompanyPage oCompanyPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_CompanyPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_CompanyPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.Title)); 
db.AddParameter(TBNames_CompanyPage.PRM_AirlineDocId, ConvertTo.ConvertEmptyToDBNull(oCompanyPage.AirlineDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.CompanyPage, "Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Title_AirlineDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
