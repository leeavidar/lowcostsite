

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class City  : ContainerItem<City>{

#region CTOR

    #region Constractor
    static City()
    {
        ConvertEvent = City.OnConvert;
    }  
    //public KeyValuesContainer<City> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public City()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.CityKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static City OnConvert(DataRow dr)
    {
        int LangId = Translator<City>.LangId;            
        City oCity = null;
        if (dr != null)
        {
            oCity = new City();
            #region Create Object
            oCity.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_City.Field_DateCreated]);
             oCity.DocId = ConvertTo.ConvertToInt(dr[TBNames_City.Field_DocId]);
             oCity.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_City.Field_IsDeleted]);
             oCity.IsActive = ConvertTo.ConvertToBool(dr[TBNames_City.Field_IsActive]);
             oCity.IataCode = ConvertTo.ConvertToString(dr[TBNames_City.Field_IataCode]);
             oCity.Name = ConvertTo.ConvertToString(dr[TBNames_City.Field_Name]);
             oCity.CountryCode = ConvertTo.ConvertToString(dr[TBNames_City.Field_CountryCode]);
             oCity.EnglishName = ConvertTo.ConvertToString(dr[TBNames_City.Field_EnglishName]);
 
            #endregion
            Translator<City>.Translate(oCity.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oCity;
    }

    
#endregion

//#REP_HERE 
#region City Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCityDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyCityDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyCityDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCityDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyCityDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyCityDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCityIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyCityIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyCityIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCityIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyCityIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyCityIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_IataCode;
private string _iata_code;
public String FriendlyIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCityIataCode);   
    }
}
public  string IataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCityIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyCityIataCode, value);
         _iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_IataCode = true;
    }
}


public string IataCode_Value
{
    get
    {
        //return _iata_code; //ConvertToValue.ConvertToString(IataCode);
        if(isSetOnce_IataCode) {return _iata_code;}
        else {return ConvertToValue.ConvertToString(IataCode);}
    }
}

public string IataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_iata_code).ToString();
               //if(isSetOnce_IataCode) {return ConvertToValue.ConvertToString(_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(IataCode).ToString();}
            ConvertToValue.ConvertToString(IataCode).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCityName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCityName));
    }
    set
    {
        SetKey(KeyValuesType.KeyCityName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_CountryCode;
private string _country_code;
public String FriendlyCountryCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCityCountryCode);   
    }
}
public  string CountryCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCityCountryCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyCityCountryCode, value);
         _country_code = ConvertToValue.ConvertToString(value);
        isSetOnce_CountryCode = true;
    }
}


public string CountryCode_Value
{
    get
    {
        //return _country_code; //ConvertToValue.ConvertToString(CountryCode);
        if(isSetOnce_CountryCode) {return _country_code;}
        else {return ConvertToValue.ConvertToString(CountryCode);}
    }
}

public string CountryCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_country_code).ToString();
               //if(isSetOnce_CountryCode) {return ConvertToValue.ConvertToString(_country_code).ToString();}
               //else {return ConvertToValue.ConvertToString(CountryCode).ToString();}
            ConvertToValue.ConvertToString(CountryCode).ToString();
    }
}

private bool isSetOnce_EnglishName;
private string _english_name;
public String FriendlyEnglishName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyCityEnglishName);   
    }
}
public  string EnglishName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyCityEnglishName));
    }
    set
    {
        SetKey(KeyValuesType.KeyCityEnglishName, value);
         _english_name = ConvertToValue.ConvertToString(value);
        isSetOnce_EnglishName = true;
    }
}


public string EnglishName_Value
{
    get
    {
        //return _english_name; //ConvertToValue.ConvertToString(EnglishName);
        if(isSetOnce_EnglishName) {return _english_name;}
        else {return ConvertToValue.ConvertToString(EnglishName);}
    }
}

public string EnglishName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_english_name).ToString();
               //if(isSetOnce_EnglishName) {return ConvertToValue.ConvertToString(_english_name).ToString();}
               //else {return ConvertToValue.ConvertToString(EnglishName).ToString();}
            ConvertToValue.ConvertToString(EnglishName).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCity.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCity.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCity.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oCity.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCity.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G
        //6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_CountryCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCity.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //H
        //7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_EnglishName(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCity.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCity.DateCreated)); 
db.AddParameter(TBNames_City.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCity.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCity.DateCreated)); 
db.AddParameter(TBNames_City.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCity.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IataCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCity.DateCreated)); 
db.AddParameter(TBNames_City.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oCity.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DateCreated_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Name(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCity.DateCreated)); 
db.AddParameter(TBNames_City.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCity.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //A_G
        //0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_CountryCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCity.DateCreated)); 
db.AddParameter(TBNames_City.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCity.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DateCreated_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //A_H
        //0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_EnglishName(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oCity.DateCreated)); 
db.AddParameter(TBNames_City.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCity.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DateCreated_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCity.DocId)); 
db.AddParameter(TBNames_City.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCity.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IataCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCity.DocId)); 
db.AddParameter(TBNames_City.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oCity.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DocId_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Name(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCity.DocId)); 
db.AddParameter(TBNames_City.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCity.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //B_G
        //1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_CountryCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCity.DocId)); 
db.AddParameter(TBNames_City.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCity.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DocId_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //B_H
        //1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_EnglishName(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oCity.DocId)); 
db.AddParameter(TBNames_City.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCity.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_DocId_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_IataCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCity.IsDeleted)); 
db.AddParameter(TBNames_City.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oCity.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IsDeleted_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Name(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCity.IsDeleted)); 
db.AddParameter(TBNames_City.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCity.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //C_G
        //2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_CountryCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCity.IsDeleted)); 
db.AddParameter(TBNames_City.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCity.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IsDeleted_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //C_H
        //2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_EnglishName(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oCity.IsDeleted)); 
db.AddParameter(TBNames_City.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCity.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IsDeleted_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode_Name(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oCity.IataCode)); 
db.AddParameter(TBNames_City.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCity.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IataCode_Name", ex.Message));

            }
            return paramsSelect;
        }



        //E_G
        //4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode_CountryCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oCity.IataCode)); 
db.AddParameter(TBNames_City.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCity.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IataCode_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //E_H
        //4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode_EnglishName(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oCity.IataCode)); 
db.AddParameter(TBNames_City.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCity.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_IataCode_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //F_G
        //5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_CountryCode(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCity.Name)); 
db.AddParameter(TBNames_City.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCity.CountryCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_Name_CountryCode", ex.Message));

            }
            return paramsSelect;
        }



        //F_H
        //5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_EnglishName(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oCity.Name)); 
db.AddParameter(TBNames_City.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCity.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_Name_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //G_H
        //6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_CountryCode_EnglishName(City oCity)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_City.PRM_CountryCode, ConvertTo.ConvertEmptyToDBNull(oCity.CountryCode)); 
db.AddParameter(TBNames_City.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oCity.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.City, "Select_City_By_Keys_View_CountryCode_EnglishName", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
