

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class WhiteLabel  : ContainerItem<WhiteLabel>{

#region CTOR

    #region Constractor
    static WhiteLabel()
    {
        ConvertEvent = WhiteLabel.OnConvert;
    }  
    //public KeyValuesContainer<WhiteLabel> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public WhiteLabel()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.WhiteLabelKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static WhiteLabel OnConvert(DataRow dr)
    {
        int LangId = Translator<WhiteLabel>.LangId;            
        WhiteLabel oWhiteLabel = null;
        if (dr != null)
        {
            oWhiteLabel = new WhiteLabel();
            #region Create Object
            oWhiteLabel.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_WhiteLabel.Field_DateCreated]);
             oWhiteLabel.DocId = ConvertTo.ConvertToInt(dr[TBNames_WhiteLabel.Field_DocId]);
             oWhiteLabel.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_WhiteLabel.Field_IsDeleted]);
             oWhiteLabel.IsActive = ConvertTo.ConvertToBool(dr[TBNames_WhiteLabel.Field_IsActive]);
             oWhiteLabel.Domain = ConvertTo.ConvertToString(dr[TBNames_WhiteLabel.Field_Domain]);
             oWhiteLabel.Name = ConvertTo.ConvertToString(dr[TBNames_WhiteLabel.Field_Name]);
             oWhiteLabel.LanguagesDocId = ConvertTo.ConvertToInt(dr[TBNames_WhiteLabel.Field_LanguagesDocId]);
             oWhiteLabel.Phone = ConvertTo.ConvertToString(dr[TBNames_WhiteLabel.Field_Phone]);
             oWhiteLabel.Email = ConvertTo.ConvertToString(dr[TBNames_WhiteLabel.Field_Email]);
             oWhiteLabel.DiscountType = ConvertTo.ConvertToInt(dr[TBNames_WhiteLabel.Field_DiscountType]);
             oWhiteLabel.Address = ConvertTo.ConvertToString(dr[TBNames_WhiteLabel.Field_Address]);
             oWhiteLabel.Discount = ConvertTo.ConvertToDouble(dr[TBNames_WhiteLabel.Field_Discount]);
 
            #endregion
            Translator<WhiteLabel>.Translate(oWhiteLabel.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oWhiteLabel;
    }

    
#endregion

//#REP_HERE 
#region WhiteLabel Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyWhiteLabelDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyWhiteLabelIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyWhiteLabelIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Domain;
private string _domain;
public String FriendlyDomain
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDomain);   
    }
}
public  string Domain
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyWhiteLabelDomain));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDomain, value);
         _domain = ConvertToValue.ConvertToString(value);
        isSetOnce_Domain = true;
    }
}


public string Domain_Value
{
    get
    {
        //return _domain; //ConvertToValue.ConvertToString(Domain);
        if(isSetOnce_Domain) {return _domain;}
        else {return ConvertToValue.ConvertToString(Domain);}
    }
}

public string Domain_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_domain).ToString();
               //if(isSetOnce_Domain) {return ConvertToValue.ConvertToString(_domain).ToString();}
               //else {return ConvertToValue.ConvertToString(Domain).ToString();}
            ConvertToValue.ConvertToString(Domain).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyWhiteLabelName));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_LanguagesDocId;
private int _languages_doc_id;
public String FriendlyLanguagesDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelLanguagesDocId);   
    }
}
public  int? LanguagesDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelLanguagesDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelLanguagesDocId, value);
         _languages_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_LanguagesDocId = true;
    }
}


public int LanguagesDocId_Value
{
    get
    {
        //return _languages_doc_id; //ConvertToValue.ConvertToInt(LanguagesDocId);
        if(isSetOnce_LanguagesDocId) {return _languages_doc_id;}
        else {return ConvertToValue.ConvertToInt(LanguagesDocId);}
    }
}

public string LanguagesDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_languages_doc_id).ToString();
               //if(isSetOnce_LanguagesDocId) {return ConvertToValue.ConvertToInt(_languages_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(LanguagesDocId).ToString();}
            ConvertToValue.ConvertToInt(LanguagesDocId).ToString();
    }
}

private bool isSetOnce_Phone;
private string _phone;
public String FriendlyPhone
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelPhone);   
    }
}
public  string Phone
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyWhiteLabelPhone));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelPhone, value);
         _phone = ConvertToValue.ConvertToString(value);
        isSetOnce_Phone = true;
    }
}


public string Phone_Value
{
    get
    {
        //return _phone; //ConvertToValue.ConvertToString(Phone);
        if(isSetOnce_Phone) {return _phone;}
        else {return ConvertToValue.ConvertToString(Phone);}
    }
}

public string Phone_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_phone).ToString();
               //if(isSetOnce_Phone) {return ConvertToValue.ConvertToString(_phone).ToString();}
               //else {return ConvertToValue.ConvertToString(Phone).ToString();}
            ConvertToValue.ConvertToString(Phone).ToString();
    }
}

private bool isSetOnce_Email;
private string _email;
public String FriendlyEmail
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelEmail);   
    }
}
public  string Email
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyWhiteLabelEmail));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelEmail, value);
         _email = ConvertToValue.ConvertToString(value);
        isSetOnce_Email = true;
    }
}


public string Email_Value
{
    get
    {
        //return _email; //ConvertToValue.ConvertToString(Email);
        if(isSetOnce_Email) {return _email;}
        else {return ConvertToValue.ConvertToString(Email);}
    }
}

public string Email_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_email).ToString();
               //if(isSetOnce_Email) {return ConvertToValue.ConvertToString(_email).ToString();}
               //else {return ConvertToValue.ConvertToString(Email).ToString();}
            ConvertToValue.ConvertToString(Email).ToString();
    }
}

private bool isSetOnce_DiscountType;
private int _discount_type;
public String FriendlyDiscountType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDiscountType);   
    }
}
public  int? DiscountType
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDiscountType));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDiscountType, value);
         _discount_type = ConvertToValue.ConvertToInt(value);
        isSetOnce_DiscountType = true;
    }
}


public int DiscountType_Value
{
    get
    {
        //return _discount_type; //ConvertToValue.ConvertToInt(DiscountType);
        if(isSetOnce_DiscountType) {return _discount_type;}
        else {return ConvertToValue.ConvertToInt(DiscountType);}
    }
}

public string DiscountType_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_discount_type).ToString();
               //if(isSetOnce_DiscountType) {return ConvertToValue.ConvertToInt(_discount_type).ToString();}
               //else {return ConvertToValue.ConvertToInt(DiscountType).ToString();}
            ConvertToValue.ConvertToInt(DiscountType).ToString();
    }
}

private bool isSetOnce_Address;
private string _address;
public String FriendlyAddress
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelAddress);   
    }
}
public  string Address
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyWhiteLabelAddress));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelAddress, value);
         _address = ConvertToValue.ConvertToString(value);
        isSetOnce_Address = true;
    }
}


public string Address_Value
{
    get
    {
        //return _address; //ConvertToValue.ConvertToString(Address);
        if(isSetOnce_Address) {return _address;}
        else {return ConvertToValue.ConvertToString(Address);}
    }
}

public string Address_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_address).ToString();
               //if(isSetOnce_Address) {return ConvertToValue.ConvertToString(_address).ToString();}
               //else {return ConvertToValue.ConvertToString(Address).ToString();}
            ConvertToValue.ConvertToString(Address).ToString();
    }
}

private bool isSetOnce_Discount;
private Double _discount;
public String FriendlyDiscount
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDiscount);   
    }
}
public  Double? Discount
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyWhiteLabelDiscount));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDiscount, value);
         _discount = ConvertToValue.ConvertToDouble(value);
        isSetOnce_Discount = true;
    }
}


public Double Discount_Value
{
    get
    {
        //return _discount; //ConvertToValue.ConvertToDouble(Discount);
        if(isSetOnce_Discount) {return _discount;}
        else {return ConvertToValue.ConvertToDouble(Discount);}
    }
}

public string Discount_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_discount).ToString();
               //if(isSetOnce_Discount) {return ConvertToValue.ConvertToDouble(_discount).ToString();}
               //else {return ConvertToValue.ConvertToDouble(Discount).ToString();}
            ConvertToValue.ConvertToDouble(Discount).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Domain(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Domain", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G
        //6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_LanguagesDocId(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_LanguagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H
        //7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Phone(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Phone", ex.Message));

            }
            return paramsSelect;
        }



        //I
        //8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Email(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Email", ex.Message));

            }
            return paramsSelect;
        }



        //J
        //9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //K
        //10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Address", ex.Message));

            }
            return paramsSelect;
        }



        //L
        //11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Domain(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_Domain", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Name(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //A_G
        //0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_LanguagesDocId(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_LanguagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_H
        //0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Phone(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_Phone", ex.Message));

            }
            return paramsSelect;
        }



        //A_I
        //0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Email(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_Email", ex.Message));

            }
            return paramsSelect;
        }



        //A_J
        //0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //A_K
        //0_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_Address", ex.Message));

            }
            return paramsSelect;
        }



        //A_L
        //0_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DateCreated)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DateCreated_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Domain(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_Domain", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Name(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //B_G
        //1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_LanguagesDocId(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_LanguagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //B_H
        //1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Phone(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_Phone", ex.Message));

            }
            return paramsSelect;
        }



        //B_I
        //1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Email(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_Email", ex.Message));

            }
            return paramsSelect;
        }



        //B_J
        //1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //B_K
        //1_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_Address", ex.Message));

            }
            return paramsSelect;
        }



        //B_L
        //1_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DocId_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Domain(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted_Domain", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Name(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //C_G
        //2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_LanguagesDocId(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted)); 
db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted_LanguagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //C_H
        //2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Phone(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted_Phone", ex.Message));

            }
            return paramsSelect;
        }



        //C_I
        //2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Email(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted_Email", ex.Message));

            }
            return paramsSelect;
        }



        //C_J
        //2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //C_K
        //2_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted_Address", ex.Message));

            }
            return paramsSelect;
        }



        //C_L
        //2_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.IsDeleted)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_IsDeleted_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Domain_Name(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Domain_Name", ex.Message));

            }
            return paramsSelect;
        }



        //E_G
        //4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Domain_LanguagesDocId(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain)); 
db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Domain_LanguagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //E_H
        //4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Domain_Phone(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Domain_Phone", ex.Message));

            }
            return paramsSelect;
        }



        //E_I
        //4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Domain_Email(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Domain_Email", ex.Message));

            }
            return paramsSelect;
        }



        //E_J
        //4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Domain_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Domain_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //E_K
        //4_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Domain_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Domain_Address", ex.Message));

            }
            return paramsSelect;
        }



        //E_L
        //4_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Domain_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Domain, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Domain)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Domain_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //F_G
        //5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_LanguagesDocId(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name)); 
db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Name_LanguagesDocId", ex.Message));

            }
            return paramsSelect;
        }



        //F_H
        //5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_Phone(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Name_Phone", ex.Message));

            }
            return paramsSelect;
        }



        //F_I
        //5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_Email(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Name_Email", ex.Message));

            }
            return paramsSelect;
        }



        //F_J
        //5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Name_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //F_K
        //5_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Name_Address", ex.Message));

            }
            return paramsSelect;
        }



        //F_L
        //5_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Name_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Name)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Name_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //G_H
        //6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_LanguagesDocId_Phone(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_LanguagesDocId_Phone", ex.Message));

            }
            return paramsSelect;
        }



        //G_I
        //6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_LanguagesDocId_Email(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_LanguagesDocId_Email", ex.Message));

            }
            return paramsSelect;
        }



        //G_J
        //6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_LanguagesDocId_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_LanguagesDocId_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //G_K
        //6_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_LanguagesDocId_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_LanguagesDocId_Address", ex.Message));

            }
            return paramsSelect;
        }



        //G_L
        //6_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_LanguagesDocId_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_LanguagesDocId, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.LanguagesDocId)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_LanguagesDocId_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //H_I
        //7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Phone_Email(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Phone_Email", ex.Message));

            }
            return paramsSelect;
        }



        //H_J
        //7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Phone_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Phone_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //H_K
        //7_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Phone_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Phone_Address", ex.Message));

            }
            return paramsSelect;
        }



        //H_L
        //7_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Phone_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Phone, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Phone)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Phone_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //I_J
        //8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Email_DiscountType(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email)); 
db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Email_DiscountType", ex.Message));

            }
            return paramsSelect;
        }



        //I_K
        //8_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Email_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Email_Address", ex.Message));

            }
            return paramsSelect;
        }



        //I_L
        //8_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Email_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Email, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Email)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Email_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //J_K
        //9_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DiscountType_Address(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DiscountType_Address", ex.Message));

            }
            return paramsSelect;
        }



        //J_L
        //9_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DiscountType_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_DiscountType, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.DiscountType)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_DiscountType_Discount", ex.Message));

            }
            return paramsSelect;
        }



        //K_L
        //10_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Address_Discount(WhiteLabel oWhiteLabel)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_WhiteLabel.PRM_Address, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Address)); 
db.AddParameter(TBNames_WhiteLabel.PRM_Discount, ConvertTo.ConvertEmptyToDBNull(oWhiteLabel.Discount));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.WhiteLabel, "Select_WhiteLabel_By_Keys_View_Address_Discount", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
