

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class HomePageContainer  : Container<HomePageContainer, HomePage>{
#region Extra functions

#endregion

        #region Static Method
        
        public static HomePageContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            oHomePageContainer.Add(oHomePageContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oHomePageContainer;
        }

        
        public static HomePageContainer SelectAllHomePages(int? _WhiteLabelDocId,bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            oHomePageContainer.Add(oHomePageContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oHomePageContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //I_A
        //8_0
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_B
        //8_1
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DocId = _DocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_C
        //8_2
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.IsDeleted = _IsDeleted;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_E
        //8_4
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine1(
int _WhiteLabelDocId,
string _TextLine1 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine1 = _TextLine1;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1));
            #endregion
            return oHomePageContainer;
        }



        //I_F
        //8_5
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine2(
int _WhiteLabelDocId,
string _TextLine2 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine2 = _TextLine2;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine2, _TextLine2));
            #endregion
            return oHomePageContainer;
        }



        //I_G
        //8_6
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine3(
int _WhiteLabelDocId,
string _TextLine3 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine3 = _TextLine3;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine3(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine3, _TextLine3));
            #endregion
            return oHomePageContainer;
        }



        //I_H
        //8_7
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine4(
int _WhiteLabelDocId,
string _TextLine4 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine4 = _TextLine4;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine4(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_J
        //8_9
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId(
int _WhiteLabelDocId,
int _SeoDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.SeoDocId = _SeoDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_K
        //8_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TipDocId(
int _WhiteLabelDocId,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_A_B
        //8_0_1
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated; 
 oHomePage.DocId = _DocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_A_C
        //8_0_2
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated; 
 oHomePage.IsDeleted = _IsDeleted;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_A_E
        //8_0_4
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TextLine1(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _TextLine1 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated; 
 oHomePage.TextLine1 = _TextLine1;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLine1(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine1));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1));
            #endregion
            return oHomePageContainer;
        }



        //I_A_F
        //8_0_5
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TextLine2(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _TextLine2 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated; 
 oHomePage.TextLine2 = _TextLine2;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLine2(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine2));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine2, _TextLine2));
            #endregion
            return oHomePageContainer;
        }



        //I_A_G
        //8_0_6
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TextLine3(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _TextLine3 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated; 
 oHomePage.TextLine3 = _TextLine3;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLine3(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine3));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine3, _TextLine3));
            #endregion
            return oHomePageContainer;
        }



        //I_A_H
        //8_0_7
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TextLine4(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _TextLine4 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated; 
 oHomePage.TextLine4 = _TextLine4;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLine4(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine4));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_A_J
        //8_0_9
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _SeoDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated; 
 oHomePage.SeoDocId = _SeoDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_A_K
        //8_0_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TipDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DateCreated = _DateCreated; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_B_C
        //8_1_2
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DocId = _DocId; 
 oHomePage.IsDeleted = _IsDeleted;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_B_E
        //8_1_4
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DocId_TextLine1(
int _WhiteLabelDocId,
int _DocId,
string _TextLine1 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DocId = _DocId; 
 oHomePage.TextLine1 = _TextLine1;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLine1(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine1));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1));
            #endregion
            return oHomePageContainer;
        }



        //I_B_F
        //8_1_5
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DocId_TextLine2(
int _WhiteLabelDocId,
int _DocId,
string _TextLine2 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DocId = _DocId; 
 oHomePage.TextLine2 = _TextLine2;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLine2(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine2));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine2, _TextLine2));
            #endregion
            return oHomePageContainer;
        }



        //I_B_G
        //8_1_6
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DocId_TextLine3(
int _WhiteLabelDocId,
int _DocId,
string _TextLine3 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DocId = _DocId; 
 oHomePage.TextLine3 = _TextLine3;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLine3(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine3));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine3, _TextLine3));
            #endregion
            return oHomePageContainer;
        }



        //I_B_H
        //8_1_7
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DocId_TextLine4(
int _WhiteLabelDocId,
int _DocId,
string _TextLine4 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DocId = _DocId; 
 oHomePage.TextLine4 = _TextLine4;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLine4(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine4));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_B_J
        //8_1_9
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(
int _WhiteLabelDocId,
int _DocId,
int _SeoDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DocId = _DocId; 
 oHomePage.SeoDocId = _SeoDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_B_K
        //8_1_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_DocId_TipDocId(
int _WhiteLabelDocId,
int _DocId,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.DocId = _DocId; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_C_E
        //8_2_4
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine1(
int _WhiteLabelDocId,
bool _IsDeleted,
string _TextLine1 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.IsDeleted = _IsDeleted; 
 oHomePage.TextLine1 = _TextLine1;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine1(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine1));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1));
            #endregion
            return oHomePageContainer;
        }



        //I_C_F
        //8_2_5
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine2(
int _WhiteLabelDocId,
bool _IsDeleted,
string _TextLine2 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.IsDeleted = _IsDeleted; 
 oHomePage.TextLine2 = _TextLine2;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine2(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine2));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine2, _TextLine2));
            #endregion
            return oHomePageContainer;
        }



        //I_C_G
        //8_2_6
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine3(
int _WhiteLabelDocId,
bool _IsDeleted,
string _TextLine3 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.IsDeleted = _IsDeleted; 
 oHomePage.TextLine3 = _TextLine3;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine3(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine3));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine3, _TextLine3));
            #endregion
            return oHomePageContainer;
        }



        //I_C_H
        //8_2_7
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine4(
int _WhiteLabelDocId,
bool _IsDeleted,
string _TextLine4 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.IsDeleted = _IsDeleted; 
 oHomePage.TextLine4 = _TextLine4;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLine4(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine4));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_C_J
        //8_2_9
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _SeoDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.IsDeleted = _IsDeleted; 
 oHomePage.SeoDocId = _SeoDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_C_K
        //8_2_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TipDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.IsDeleted = _IsDeleted; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }



        //I_E_F
        //8_4_5
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine1_TextLine2(
int _WhiteLabelDocId,
string _TextLine1,
string _TextLine2 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine1 = _TextLine1; 
 oHomePage.TextLine2 = _TextLine2;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_TextLine2(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine2));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1) && string.Equals(R.TextLine2, _TextLine2));
            #endregion
            return oHomePageContainer;
        }



        //I_E_G
        //8_4_6
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine1_TextLine3(
int _WhiteLabelDocId,
string _TextLine1,
string _TextLine3 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine1 = _TextLine1; 
 oHomePage.TextLine3 = _TextLine3;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_TextLine3(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine3));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1) && string.Equals(R.TextLine3, _TextLine3));
            #endregion
            return oHomePageContainer;
        }



        //I_E_H
        //8_4_7
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine1_TextLine4(
int _WhiteLabelDocId,
string _TextLine1,
string _TextLine4 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine1 = _TextLine1; 
 oHomePage.TextLine4 = _TextLine4;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_TextLine4(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine4));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1) && string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_E_J
        //8_4_9
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine1_SeoDocId(
int _WhiteLabelDocId,
string _TextLine1,
int _SeoDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine1 = _TextLine1; 
 oHomePage.SeoDocId = _SeoDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_SeoDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1));
            #endregion
            return oHomePageContainer;
        }



        //I_E_K
        //8_4_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine1_TipDocId(
int _WhiteLabelDocId,
string _TextLine1,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine1 = _TextLine1; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine1_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine1, _TextLine1));
            #endregion
            return oHomePageContainer;
        }



        //I_F_G
        //8_5_6
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine2_TextLine3(
int _WhiteLabelDocId,
string _TextLine2,
string _TextLine3 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine2 = _TextLine2; 
 oHomePage.TextLine3 = _TextLine3;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2_TextLine3(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TextLine3));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine2, _TextLine2) && string.Equals(R.TextLine3, _TextLine3));
            #endregion
            return oHomePageContainer;
        }



        //I_F_H
        //8_5_7
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine2_TextLine4(
int _WhiteLabelDocId,
string _TextLine2,
string _TextLine4 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine2 = _TextLine2; 
 oHomePage.TextLine4 = _TextLine4;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2_TextLine4(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TextLine4));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine2, _TextLine2) && string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_F_J
        //8_5_9
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine2_SeoDocId(
int _WhiteLabelDocId,
string _TextLine2,
int _SeoDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine2 = _TextLine2; 
 oHomePage.SeoDocId = _SeoDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2_SeoDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine2, _TextLine2));
            #endregion
            return oHomePageContainer;
        }



        //I_F_K
        //8_5_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine2_TipDocId(
int _WhiteLabelDocId,
string _TextLine2,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine2 = _TextLine2; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine2_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine2, _TextLine2));
            #endregion
            return oHomePageContainer;
        }



        //I_G_H
        //8_6_7
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine3_TextLine4(
int _WhiteLabelDocId,
string _TextLine3,
string _TextLine4 , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine3 = _TextLine3; 
 oHomePage.TextLine4 = _TextLine4;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine3_TextLine4(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_TextLine4));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine3, _TextLine3) && string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_G_J
        //8_6_9
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine3_SeoDocId(
int _WhiteLabelDocId,
string _TextLine3,
int _SeoDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine3 = _TextLine3; 
 oHomePage.SeoDocId = _SeoDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine3_SeoDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine3, _TextLine3));
            #endregion
            return oHomePageContainer;
        }



        //I_G_K
        //8_6_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine3_TipDocId(
int _WhiteLabelDocId,
string _TextLine3,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine3 = _TextLine3; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine3_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine3, _TextLine3));
            #endregion
            return oHomePageContainer;
        }



        //I_H_J
        //8_7_9
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine4_SeoDocId(
int _WhiteLabelDocId,
string _TextLine4,
int _SeoDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine4 = _TextLine4; 
 oHomePage.SeoDocId = _SeoDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine4_SeoDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_H_K
        //8_7_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_TextLine4_TipDocId(
int _WhiteLabelDocId,
string _TextLine4,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.TextLine4 = _TextLine4; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLine4_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oHomePageContainer = oHomePageContainer.FindAllContainer(R => string.Equals(R.TextLine4, _TextLine4));
            #endregion
            return oHomePageContainer;
        }



        //I_J_K
        //8_9_10
        public static HomePageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_TipDocId(
int _WhiteLabelDocId,
int _SeoDocId,
int _TipDocId , bool? isActive)
        {
            HomePageContainer oHomePageContainer = new HomePageContainer();
            HomePage oHomePage = new HomePage();
            #region Params
            
 oHomePage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePage.SeoDocId = _SeoDocId; 
 oHomePage.TipDocId = _TipDocId;
            #endregion 
            oHomePageContainer.Add(SelectData(HomePage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_TipDocId(oHomePage), TBNames_HomePage.PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_SeoDocId_TipDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageContainer = oHomePageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageContainer;
        }


#endregion
}

    
}
