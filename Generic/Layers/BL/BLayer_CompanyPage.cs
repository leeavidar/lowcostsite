

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class CompanyPageContainer  : Container<CompanyPageContainer, CompanyPage>{
#region Extra functions

#endregion

        #region Static Method
        
        public static CompanyPageContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            oCompanyPageContainer.Add(oCompanyPageContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oCompanyPageContainer;
        }

        
        public static CompanyPageContainer SelectAllCompanyPages(int? _WhiteLabelDocId,bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            oCompanyPageContainer.Add(oCompanyPageContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oCompanyPageContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //G_A
        //6_0
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DateCreated = _DateCreated;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_B
        //6_1
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DocId = _DocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_C
        //6_2
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.IsDeleted = _IsDeleted;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_E
        //6_4
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Name(
int _WhiteLabelDocId,
string _Name , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Name = _Name;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_F
        //6_5
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId(
int _WhiteLabelDocId,
int _SeoDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.SeoDocId = _SeoDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_H
        //6_7
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Text(
int _WhiteLabelDocId,
string _Text , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Text = _Text;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Text(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oCompanyPageContainer;
        }



        //G_I
        //6_8
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Title(
int _WhiteLabelDocId,
string _Title , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Title = _Title;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Title(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oCompanyPageContainer;
        }



        //G_J
        //6_9
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_AirlineDocId(
int _WhiteLabelDocId,
int _AirlineDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.AirlineDocId = _AirlineDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_AirlineDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_A_B
        //6_0_1
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DateCreated = _DateCreated; 
 oCompanyPage.DocId = _DocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_A_C
        //6_0_2
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DateCreated = _DateCreated; 
 oCompanyPage.IsDeleted = _IsDeleted;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_A_E
        //6_0_4
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Name(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DateCreated = _DateCreated; 
 oCompanyPage.Name = _Name;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_A_F
        //6_0_5
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _SeoDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DateCreated = _DateCreated; 
 oCompanyPage.SeoDocId = _SeoDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_A_H
        //6_0_7
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Text(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Text , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DateCreated = _DateCreated; 
 oCompanyPage.Text = _Text;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Text(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Text));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oCompanyPageContainer;
        }



        //G_A_I
        //6_0_8
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Title(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Title , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DateCreated = _DateCreated; 
 oCompanyPage.Title = _Title;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_Title));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oCompanyPageContainer;
        }



        //G_A_J
        //6_0_9
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_AirlineDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _AirlineDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DateCreated = _DateCreated; 
 oCompanyPage.AirlineDocId = _AirlineDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AirlineDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_B_C
        //6_1_2
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DocId = _DocId; 
 oCompanyPage.IsDeleted = _IsDeleted;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_B_E
        //6_1_4
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Name(
int _WhiteLabelDocId,
int _DocId,
string _Name , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DocId = _DocId; 
 oCompanyPage.Name = _Name;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_B_F
        //6_1_5
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(
int _WhiteLabelDocId,
int _DocId,
int _SeoDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DocId = _DocId; 
 oCompanyPage.SeoDocId = _SeoDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_B_H
        //6_1_7
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Text(
int _WhiteLabelDocId,
int _DocId,
string _Text , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DocId = _DocId; 
 oCompanyPage.Text = _Text;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Text(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oCompanyPageContainer;
        }



        //G_B_I
        //6_1_8
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Title(
int _WhiteLabelDocId,
int _DocId,
string _Title , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DocId = _DocId; 
 oCompanyPage.Title = _Title;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oCompanyPageContainer;
        }



        //G_B_J
        //6_1_9
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_DocId_AirlineDocId(
int _WhiteLabelDocId,
int _DocId,
int _AirlineDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.DocId = _DocId; 
 oCompanyPage.AirlineDocId = _AirlineDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AirlineDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_DocId_AirlineDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_C_E
        //6_2_4
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Name(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Name , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.IsDeleted = _IsDeleted; 
 oCompanyPage.Name = _Name;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_C_F
        //6_2_5
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _SeoDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.IsDeleted = _IsDeleted; 
 oCompanyPage.SeoDocId = _SeoDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_C_H
        //6_2_7
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Text(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Text , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.IsDeleted = _IsDeleted; 
 oCompanyPage.Text = _Text;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Text(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Text));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oCompanyPageContainer;
        }



        //G_C_I
        //6_2_8
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Title(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Title , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.IsDeleted = _IsDeleted; 
 oCompanyPage.Title = _Title;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Title));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oCompanyPageContainer;
        }



        //G_C_J
        //6_2_9
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _AirlineDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.IsDeleted = _IsDeleted; 
 oCompanyPage.AirlineDocId = _AirlineDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_E_F
        //6_4_5
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Name_SeoDocId(
int _WhiteLabelDocId,
string _Name,
int _SeoDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Name = _Name; 
 oCompanyPage.SeoDocId = _SeoDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_SeoDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_E_H
        //6_4_7
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Name_Text(
int _WhiteLabelDocId,
string _Name,
string _Text , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Name = _Name; 
 oCompanyPage.Text = _Text;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Text(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_Text));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oCompanyPageContainer;
        }



        //G_E_I
        //6_4_8
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Name_Title(
int _WhiteLabelDocId,
string _Name,
string _Title , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Name = _Name; 
 oCompanyPage.Title = _Title;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Title(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_Title));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oCompanyPageContainer;
        }



        //G_E_J
        //6_4_9
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Name_AirlineDocId(
int _WhiteLabelDocId,
string _Name,
int _AirlineDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Name = _Name; 
 oCompanyPage.AirlineDocId = _AirlineDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_AirlineDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Name_AirlineDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_F_H
        //6_5_7
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_Text(
int _WhiteLabelDocId,
int _SeoDocId,
string _Text , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.SeoDocId = _SeoDocId; 
 oCompanyPage.Text = _Text;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Text(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oCompanyPageContainer;
        }



        //G_F_I
        //6_5_8
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_Title(
int _WhiteLabelDocId,
int _SeoDocId,
string _Title , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.SeoDocId = _SeoDocId; 
 oCompanyPage.Title = _Title;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Title(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oCompanyPageContainer;
        }



        //G_F_J
        //6_5_9
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_AirlineDocId(
int _WhiteLabelDocId,
int _SeoDocId,
int _AirlineDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.SeoDocId = _SeoDocId; 
 oCompanyPage.AirlineDocId = _AirlineDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_AirlineDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_SeoDocId_AirlineDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyPageContainer;
        }



        //G_H_I
        //6_7_8
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Text_Title(
int _WhiteLabelDocId,
string _Text,
string _Title , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Text = _Text; 
 oCompanyPage.Title = _Title;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Title(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text_Title));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Text, _Text) && string.Equals(R.Title, _Title));
            #endregion
            return oCompanyPageContainer;
        }



        //G_H_J
        //6_7_9
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Text_AirlineDocId(
int _WhiteLabelDocId,
string _Text,
int _AirlineDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Text = _Text; 
 oCompanyPage.AirlineDocId = _AirlineDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_AirlineDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Text_AirlineDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oCompanyPageContainer;
        }



        //G_I_J
        //6_8_9
        public static CompanyPageContainer SelectByKeysView_WhiteLabelDocId_Title_AirlineDocId(
int _WhiteLabelDocId,
string _Title,
int _AirlineDocId , bool? isActive)
        {
            CompanyPageContainer oCompanyPageContainer = new CompanyPageContainer();
            CompanyPage oCompanyPage = new CompanyPage();
            #region Params
            
 oCompanyPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompanyPage.Title = _Title; 
 oCompanyPage.AirlineDocId = _AirlineDocId;
            #endregion 
            oCompanyPageContainer.Add(SelectData(CompanyPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_AirlineDocId(oCompanyPage), TBNames_CompanyPage.PROC_Select_CompanyPage_By_Keys_View_WhiteLabelDocId_Title_AirlineDocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCompanyPageContainer = oCompanyPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oCompanyPageContainer;
        }


#endregion
}

    
}
