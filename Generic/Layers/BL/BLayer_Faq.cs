

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class FaqContainer  : Container<FaqContainer, Faq>{
#region Extra functions

#endregion

        #region Static Method
        
        public static FaqContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            oFaqContainer.Add(oFaqContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oFaqContainer;
        }

        
        public static FaqContainer SelectAllFaqs(int? _WhiteLabelDocId,bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            oFaqContainer.Add(oFaqContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oFaqContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //H_A
        //7_0
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DateCreated = _DateCreated;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_B
        //7_1
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DocId = _DocId;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_C
        //7_2
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.IsDeleted = _IsDeleted;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_E
        //7_4
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Category(
int _WhiteLabelDocId,
int _Category , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Category = _Category;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Category(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Category));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_F
        //7_5
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Question(
int _WhiteLabelDocId,
string _Question , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Question = _Question;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Question(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Question));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Question, _Question));
            #endregion
            return oFaqContainer;
        }



        //H_G
        //7_6
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Answer(
int _WhiteLabelDocId,
string _Answer , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Answer = _Answer;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Answer(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Answer));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Answer, _Answer));
            #endregion
            return oFaqContainer;
        }



        //H_I
        //7_8
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Order(
int _WhiteLabelDocId,
int _Order , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Order = _Order;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Order(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_A_B
        //7_0_1
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DateCreated = _DateCreated; 
 oFaq.DocId = _DocId;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_A_C
        //7_0_2
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DateCreated = _DateCreated; 
 oFaq.IsDeleted = _IsDeleted;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_A_E
        //7_0_4
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Category(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Category , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DateCreated = _DateCreated; 
 oFaq.Category = _Category;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Category(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Category));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_A_F
        //7_0_5
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Question(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Question , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DateCreated = _DateCreated; 
 oFaq.Question = _Question;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Question(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Question));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Question, _Question));
            #endregion
            return oFaqContainer;
        }



        //H_A_G
        //7_0_6
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Answer(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Answer , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DateCreated = _DateCreated; 
 oFaq.Answer = _Answer;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Answer(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Answer));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Answer, _Answer));
            #endregion
            return oFaqContainer;
        }



        //H_A_I
        //7_0_8
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Order(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Order , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DateCreated = _DateCreated; 
 oFaq.Order = _Order;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DateCreated_Order));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_B_C
        //7_1_2
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DocId = _DocId; 
 oFaq.IsDeleted = _IsDeleted;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_B_E
        //7_1_4
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DocId_Category(
int _WhiteLabelDocId,
int _DocId,
int _Category , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DocId = _DocId; 
 oFaq.Category = _Category;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Category(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Category));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_B_F
        //7_1_5
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DocId_Question(
int _WhiteLabelDocId,
int _DocId,
string _Question , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DocId = _DocId; 
 oFaq.Question = _Question;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Question(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Question));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Question, _Question));
            #endregion
            return oFaqContainer;
        }



        //H_B_G
        //7_1_6
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DocId_Answer(
int _WhiteLabelDocId,
int _DocId,
string _Answer , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DocId = _DocId; 
 oFaq.Answer = _Answer;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Answer(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Answer));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Answer, _Answer));
            #endregion
            return oFaqContainer;
        }



        //H_B_I
        //7_1_8
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_DocId_Order(
int _WhiteLabelDocId,
int _DocId,
int _Order , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.DocId = _DocId; 
 oFaq.Order = _Order;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_DocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_C_E
        //7_2_4
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Category(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Category , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.IsDeleted = _IsDeleted; 
 oFaq.Category = _Category;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Category(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Category));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_C_F
        //7_2_5
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Question(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Question , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.IsDeleted = _IsDeleted; 
 oFaq.Question = _Question;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Question(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Question));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Question, _Question));
            #endregion
            return oFaqContainer;
        }



        //H_C_G
        //7_2_6
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Answer(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Answer , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.IsDeleted = _IsDeleted; 
 oFaq.Answer = _Answer;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Answer(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Answer));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Answer, _Answer));
            #endregion
            return oFaqContainer;
        }



        //H_C_I
        //7_2_8
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Order(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Order , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.IsDeleted = _IsDeleted; 
 oFaq.Order = _Order;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_IsDeleted_Order));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_E_F
        //7_4_5
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Category_Question(
int _WhiteLabelDocId,
int _Category,
string _Question , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Category = _Category; 
 oFaq.Question = _Question;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Category_Question(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Question));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Question, _Question));
            #endregion
            return oFaqContainer;
        }



        //H_E_G
        //7_4_6
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Category_Answer(
int _WhiteLabelDocId,
int _Category,
string _Answer , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Category = _Category; 
 oFaq.Answer = _Answer;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Category_Answer(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Answer));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Answer, _Answer));
            #endregion
            return oFaqContainer;
        }



        //H_E_I
        //7_4_8
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Category_Order(
int _WhiteLabelDocId,
int _Category,
int _Order , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Category = _Category; 
 oFaq.Order = _Order;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Category_Order(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Category_Order));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFaqContainer;
        }



        //H_F_G
        //7_5_6
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Question_Answer(
int _WhiteLabelDocId,
string _Question,
string _Answer , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Question = _Question; 
 oFaq.Answer = _Answer;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Question_Answer(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Question_Answer));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Question, _Question) && string.Equals(R.Answer, _Answer));
            #endregion
            return oFaqContainer;
        }



        //H_F_I
        //7_5_8
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Question_Order(
int _WhiteLabelDocId,
string _Question,
int _Order , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Question = _Question; 
 oFaq.Order = _Order;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Question_Order(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Question_Order));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Question, _Question));
            #endregion
            return oFaqContainer;
        }



        //H_G_I
        //7_6_8
        public static FaqContainer SelectByKeysView_WhiteLabelDocId_Answer_Order(
int _WhiteLabelDocId,
string _Answer,
int _Order , bool? isActive)
        {
            FaqContainer oFaqContainer = new FaqContainer();
            Faq oFaq = new Faq();
            #region Params
            
 oFaq.WhiteLabelDocId = _WhiteLabelDocId; 
 oFaq.Answer = _Answer; 
 oFaq.Order = _Order;
            #endregion 
            oFaqContainer.Add(SelectData(Faq.GetParamsForSelectByKeysView_WhiteLabelDocId_Answer_Order(oFaq), TBNames_Faq.PROC_Select_Faq_By_Keys_View_WhiteLabelDocId_Answer_Order));
            #region ExtraFilters
            
if(isActive != null){
                oFaqContainer = oFaqContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oFaqContainer = oFaqContainer.FindAllContainer(R => string.Equals(R.Answer, _Answer));
            #endregion
            return oFaqContainer;
        }


#endregion
}

    
}
