

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class OrderLogContainer  : Container<OrderLogContainer, OrderLog>{
#region Extra functions

#endregion

        #region Static Method
        
        public static OrderLogContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            oOrderLogContainer.Add(oOrderLogContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oOrderLogContainer;
        }

        
        public static OrderLogContainer SelectAllOrderLogs(int? _WhiteLabelDocId,bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            oOrderLogContainer.Add(oOrderLogContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oOrderLogContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //H_A
        //7_0
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DateCreated = _DateCreated;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_B
        //7_1
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DocId = _DocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_C
        //7_2
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.IsDeleted = _IsDeleted;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_E
        //7_4
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Type(
int _WhiteLabelDocId,
int _Type , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Type = _Type;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Type(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_F
        //7_5
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Status(
int _WhiteLabelDocId,
int _Status , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Status = _Status;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Status(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_G
        //7_6
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_XML(
int _WhiteLabelDocId,
string _XML , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.XML = _XML;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_XML(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_I
        //7_8
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_OrdersDocId(
int _WhiteLabelDocId,
int _OrdersDocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_J
        //7_9
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_OrderStage(
int _WhiteLabelDocId,
int _OrderStage , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.OrderStage = _OrderStage;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStage(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrderStage));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_A_B
        //7_0_1
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DateCreated = _DateCreated; 
 oOrderLog.DocId = _DocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_A_C
        //7_0_2
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DateCreated = _DateCreated; 
 oOrderLog.IsDeleted = _IsDeleted;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_A_E
        //7_0_4
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Type(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Type , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DateCreated = _DateCreated; 
 oOrderLog.Type = _Type;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Type(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_Type));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_A_F
        //7_0_5
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Status(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Status , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DateCreated = _DateCreated; 
 oOrderLog.Status = _Status;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Status(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_Status));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_A_G
        //7_0_6
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DateCreated_XML(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _XML , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DateCreated = _DateCreated; 
 oOrderLog.XML = _XML;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_XML(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_XML));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_A_I
        //7_0_8
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrdersDocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DateCreated = _DateCreated; 
 oOrderLog.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_A_J
        //7_0_9
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrderStage(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrderStage , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DateCreated = _DateCreated; 
 oOrderLog.OrderStage = _OrderStage;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderStage(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_OrderStage));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_B_C
        //7_1_2
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DocId = _DocId; 
 oOrderLog.IsDeleted = _IsDeleted;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_B_E
        //7_1_4
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DocId_Type(
int _WhiteLabelDocId,
int _DocId,
int _Type , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DocId = _DocId; 
 oOrderLog.Type = _Type;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Type(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_Type));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_B_F
        //7_1_5
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DocId_Status(
int _WhiteLabelDocId,
int _DocId,
int _Status , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DocId = _DocId; 
 oOrderLog.Status = _Status;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Status(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_Status));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_B_G
        //7_1_6
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DocId_XML(
int _WhiteLabelDocId,
int _DocId,
string _XML , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DocId = _DocId; 
 oOrderLog.XML = _XML;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_XML(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_XML));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_B_I
        //7_1_8
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(
int _WhiteLabelDocId,
int _DocId,
int _OrdersDocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DocId = _DocId; 
 oOrderLog.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_B_J
        //7_1_9
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_DocId_OrderStage(
int _WhiteLabelDocId,
int _DocId,
int _OrderStage , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.DocId = _DocId; 
 oOrderLog.OrderStage = _OrderStage;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderStage(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_OrderStage));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_C_E
        //7_2_4
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Type(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Type , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.IsDeleted = _IsDeleted; 
 oOrderLog.Type = _Type;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Type(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_Type));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_C_F
        //7_2_5
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Status(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Status , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.IsDeleted = _IsDeleted; 
 oOrderLog.Status = _Status;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Status(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_Status));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_C_G
        //7_2_6
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_XML(
int _WhiteLabelDocId,
bool _IsDeleted,
string _XML , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.IsDeleted = _IsDeleted; 
 oOrderLog.XML = _XML;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_XML(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_XML));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_C_I
        //7_2_8
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrdersDocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.IsDeleted = _IsDeleted; 
 oOrderLog.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_C_J
        //7_2_9
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrderStage(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrderStage , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.IsDeleted = _IsDeleted; 
 oOrderLog.OrderStage = _OrderStage;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderStage(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderStage));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_E_F
        //7_4_5
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Type_Status(
int _WhiteLabelDocId,
int _Type,
int _Status , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Type = _Type; 
 oOrderLog.Status = _Status;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_Status(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_Status));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_E_G
        //7_4_6
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Type_XML(
int _WhiteLabelDocId,
int _Type,
string _XML , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Type = _Type; 
 oOrderLog.XML = _XML;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_XML(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_XML));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_E_I
        //7_4_8
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Type_OrdersDocId(
int _WhiteLabelDocId,
int _Type,
int _OrdersDocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Type = _Type; 
 oOrderLog.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_OrdersDocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_E_J
        //7_4_9
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Type_OrderStage(
int _WhiteLabelDocId,
int _Type,
int _OrderStage , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Type = _Type; 
 oOrderLog.OrderStage = _OrderStage;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_OrderStage(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_OrderStage));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_F_G
        //7_5_6
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Status_XML(
int _WhiteLabelDocId,
int _Status,
string _XML , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Status = _Status; 
 oOrderLog.XML = _XML;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_XML(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_XML));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_F_I
        //7_5_8
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Status_OrdersDocId(
int _WhiteLabelDocId,
int _Status,
int _OrdersDocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Status = _Status; 
 oOrderLog.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_OrdersDocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_F_J
        //7_5_9
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_Status_OrderStage(
int _WhiteLabelDocId,
int _Status,
int _OrderStage , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.Status = _Status; 
 oOrderLog.OrderStage = _OrderStage;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_OrderStage(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_OrderStage));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_G_I
        //7_6_8
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_XML_OrdersDocId(
int _WhiteLabelDocId,
string _XML,
int _OrdersDocId , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.XML = _XML; 
 oOrderLog.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_XML_OrdersDocId(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_G_J
        //7_6_9
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_XML_OrderStage(
int _WhiteLabelDocId,
string _XML,
int _OrderStage , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.XML = _XML; 
 oOrderLog.OrderStage = _OrderStage;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_XML_OrderStage(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML_OrderStage));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }



        //H_I_J
        //7_8_9
        public static OrderLogContainer SelectByKeysView_WhiteLabelDocId_OrdersDocId_OrderStage(
int _WhiteLabelDocId,
int _OrdersDocId,
int _OrderStage , bool? isActive)
        {
            OrderLogContainer oOrderLogContainer = new OrderLogContainer();
            OrderLog oOrderLog = new OrderLog();
            #region Params
            
 oOrderLog.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderLog.OrdersDocId = _OrdersDocId; 
 oOrderLog.OrderStage = _OrderStage;
            #endregion 
            oOrderLogContainer.Add(SelectData(OrderLog.GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId_OrderStage(oOrderLog), TBNames_OrderLog.PROC_Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrdersDocId_OrderStage));
            #region ExtraFilters
            
if(isActive != null){
                oOrderLogContainer = oOrderLogContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderLogContainer;
        }


#endregion
}

    
}
