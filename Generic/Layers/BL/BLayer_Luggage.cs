

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class LuggageContainer  : Container<LuggageContainer, Luggage>{
#region Extra functions

#endregion

        #region Static Method
        
        public static LuggageContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            oLuggageContainer.Add(oLuggageContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oLuggageContainer;
        }

        
        public static LuggageContainer SelectAllLuggages(int? _WhiteLabelDocId,bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            oLuggageContainer.Add(oLuggageContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oLuggageContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //J_A
        //9_0
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DateCreated = _DateCreated;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_B
        //9_1
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DocId = _DocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_C
        //9_2
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.IsDeleted = _IsDeleted;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_E
        //9_4
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Price(
int _WhiteLabelDocId,
Double _Price , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Price = _Price;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_F
        //9_5
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Quantity(
int _WhiteLabelDocId,
int _Quantity , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Quantity = _Quantity;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Quantity(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_G
        //9_6
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_TotalWeight(
int _WhiteLabelDocId,
Double _TotalWeight , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.TotalWeight = _TotalWeight;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalWeight(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_H
        //9_7
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_FlightType(
int _WhiteLabelDocId,
string _FlightType , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.FlightType = _FlightType;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightType(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_I
        //9_8
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_PassengerDocId(
int _WhiteLabelDocId,
int _PassengerDocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.PassengerDocId = _PassengerDocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerDocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_A_B
        //9_0_1
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DateCreated = _DateCreated; 
 oLuggage.DocId = _DocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_A_C
        //9_0_2
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DateCreated = _DateCreated; 
 oLuggage.IsDeleted = _IsDeleted;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_A_E
        //9_0_4
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Price(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _Price , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DateCreated = _DateCreated; 
 oLuggage.Price = _Price;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Price(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_Price));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_A_F
        //9_0_5
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Quantity(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Quantity , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DateCreated = _DateCreated; 
 oLuggage.Quantity = _Quantity;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Quantity(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_Quantity));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_A_G
        //9_0_6
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TotalWeight(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _TotalWeight , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DateCreated = _DateCreated; 
 oLuggage.TotalWeight = _TotalWeight;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TotalWeight(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_TotalWeight));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_A_H
        //9_0_7
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FlightType(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FlightType , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DateCreated = _DateCreated; 
 oLuggage.FlightType = _FlightType;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightType(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_A_I
        //9_0_8
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PassengerDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _PassengerDocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DateCreated = _DateCreated; 
 oLuggage.PassengerDocId = _PassengerDocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassengerDocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_B_C
        //9_1_2
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DocId = _DocId; 
 oLuggage.IsDeleted = _IsDeleted;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_B_E
        //9_1_4
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DocId_Price(
int _WhiteLabelDocId,
int _DocId,
Double _Price , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DocId = _DocId; 
 oLuggage.Price = _Price;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Price(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_Price));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_B_F
        //9_1_5
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DocId_Quantity(
int _WhiteLabelDocId,
int _DocId,
int _Quantity , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DocId = _DocId; 
 oLuggage.Quantity = _Quantity;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Quantity(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_Quantity));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_B_G
        //9_1_6
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DocId_TotalWeight(
int _WhiteLabelDocId,
int _DocId,
Double _TotalWeight , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DocId = _DocId; 
 oLuggage.TotalWeight = _TotalWeight;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TotalWeight(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_TotalWeight));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_B_H
        //9_1_7
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DocId_FlightType(
int _WhiteLabelDocId,
int _DocId,
string _FlightType , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DocId = _DocId; 
 oLuggage.FlightType = _FlightType;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightType(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_B_I
        //9_1_8
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_DocId_PassengerDocId(
int _WhiteLabelDocId,
int _DocId,
int _PassengerDocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.DocId = _DocId; 
 oLuggage.PassengerDocId = _PassengerDocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassengerDocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_DocId_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_C_E
        //9_2_4
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Price(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _Price , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.IsDeleted = _IsDeleted; 
 oLuggage.Price = _Price;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Price(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_Price));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_C_F
        //9_2_5
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Quantity(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Quantity , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.IsDeleted = _IsDeleted; 
 oLuggage.Quantity = _Quantity;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Quantity(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_Quantity));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_C_G
        //9_2_6
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TotalWeight(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _TotalWeight , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.IsDeleted = _IsDeleted; 
 oLuggage.TotalWeight = _TotalWeight;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TotalWeight(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_TotalWeight));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_C_H
        //9_2_7
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FlightType(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FlightType , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.IsDeleted = _IsDeleted; 
 oLuggage.FlightType = _FlightType;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightType(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_C_I
        //9_2_8
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _PassengerDocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.IsDeleted = _IsDeleted; 
 oLuggage.PassengerDocId = _PassengerDocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerDocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_E_F
        //9_4_5
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Price_Quantity(
int _WhiteLabelDocId,
Double _Price,
int _Quantity , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Price = _Price; 
 oLuggage.Quantity = _Quantity;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_Quantity(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_Quantity));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_E_G
        //9_4_6
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Price_TotalWeight(
int _WhiteLabelDocId,
Double _Price,
Double _TotalWeight , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Price = _Price; 
 oLuggage.TotalWeight = _TotalWeight;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_TotalWeight(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_TotalWeight));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_E_H
        //9_4_7
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Price_FlightType(
int _WhiteLabelDocId,
Double _Price,
string _FlightType , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Price = _Price; 
 oLuggage.FlightType = _FlightType;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_FlightType(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_E_I
        //9_4_8
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Price_PassengerDocId(
int _WhiteLabelDocId,
Double _Price,
int _PassengerDocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Price = _Price; 
 oLuggage.PassengerDocId = _PassengerDocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_PassengerDocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Price_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_F_G
        //9_5_6
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Quantity_TotalWeight(
int _WhiteLabelDocId,
int _Quantity,
Double _TotalWeight , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Quantity = _Quantity; 
 oLuggage.TotalWeight = _TotalWeight;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Quantity_TotalWeight(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_TotalWeight));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_F_H
        //9_5_7
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Quantity_FlightType(
int _WhiteLabelDocId,
int _Quantity,
string _FlightType , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Quantity = _Quantity; 
 oLuggage.FlightType = _FlightType;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Quantity_FlightType(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_F_I
        //9_5_8
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_Quantity_PassengerDocId(
int _WhiteLabelDocId,
int _Quantity,
int _PassengerDocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.Quantity = _Quantity; 
 oLuggage.PassengerDocId = _PassengerDocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_Quantity_PassengerDocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_Quantity_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_G_H
        //9_6_7
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_TotalWeight_FlightType(
int _WhiteLabelDocId,
Double _TotalWeight,
string _FlightType , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.TotalWeight = _TotalWeight; 
 oLuggage.FlightType = _FlightType;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalWeight_FlightType(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_G_I
        //9_6_8
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_TotalWeight_PassengerDocId(
int _WhiteLabelDocId,
Double _TotalWeight,
int _PassengerDocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.TotalWeight = _TotalWeight; 
 oLuggage.PassengerDocId = _PassengerDocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalWeight_PassengerDocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_TotalWeight_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }



        //J_H_I
        //9_7_8
        public static LuggageContainer SelectByKeysView_WhiteLabelDocId_FlightType_PassengerDocId(
int _WhiteLabelDocId,
string _FlightType,
int _PassengerDocId , bool? isActive)
        {
            LuggageContainer oLuggageContainer = new LuggageContainer();
            Luggage oLuggage = new Luggage();
            #region Params
            
 oLuggage.WhiteLabelDocId = _WhiteLabelDocId; 
 oLuggage.FlightType = _FlightType; 
 oLuggage.PassengerDocId = _PassengerDocId;
            #endregion 
            oLuggageContainer.Add(SelectData(Luggage.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightType_PassengerDocId(oLuggage), TBNames_Luggage.PROC_Select_Luggage_By_Keys_View_WhiteLabelDocId_FlightType_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oLuggageContainer = oLuggageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLuggageContainer;
        }


#endregion
}

    
}
