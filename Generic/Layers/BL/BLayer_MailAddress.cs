

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class MailAddressContainer  : Container<MailAddressContainer, MailAddress>{
#region Extra functions

#endregion

        #region Static Method
        
        public static MailAddressContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            oMailAddressContainer.Add(oMailAddressContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oMailAddressContainer;
        }

        
        public static MailAddressContainer SelectAllMailAddresss(int? _WhiteLabelDocId,bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            oMailAddressContainer.Add(oMailAddressContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oMailAddressContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //F_A
        //5_0
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.DateCreated = _DateCreated;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_B
        //5_1
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.DocId = _DocId;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_C
        //5_2
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.IsDeleted = _IsDeleted;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_E
        //5_4
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_Address(
int _WhiteLabelDocId,
string _Address , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.Address = _Address;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_Address(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_Address));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_A_B
        //5_0_1
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.DateCreated = _DateCreated; 
 oMailAddress.DocId = _DocId;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_A_C
        //5_0_2
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.DateCreated = _DateCreated; 
 oMailAddress.IsDeleted = _IsDeleted;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_A_E
        //5_0_4
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Address(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Address , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.DateCreated = _DateCreated; 
 oMailAddress.Address = _Address;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Address(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DateCreated_Address));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_B_C
        //5_1_2
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.DocId = _DocId; 
 oMailAddress.IsDeleted = _IsDeleted;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_B_E
        //5_1_4
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_DocId_Address(
int _WhiteLabelDocId,
int _DocId,
string _Address , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.DocId = _DocId; 
 oMailAddress.Address = _Address;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Address(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_DocId_Address));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }



        //F_C_E
        //5_2_4
        public static MailAddressContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Address(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Address , bool? isActive)
        {
            MailAddressContainer oMailAddressContainer = new MailAddressContainer();
            MailAddress oMailAddress = new MailAddress();
            #region Params
            
 oMailAddress.WhiteLabelDocId = _WhiteLabelDocId; 
 oMailAddress.IsDeleted = _IsDeleted; 
 oMailAddress.Address = _Address;
            #endregion 
            oMailAddressContainer.Add(SelectData(MailAddress.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Address(oMailAddress), TBNames_MailAddress.PROC_Select_MailAddress_By_Keys_View_WhiteLabelDocId_IsDeleted_Address));
            #region ExtraFilters
            
if(isActive != null){
                oMailAddressContainer = oMailAddressContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMailAddressContainer;
        }


#endregion
}

    
}
