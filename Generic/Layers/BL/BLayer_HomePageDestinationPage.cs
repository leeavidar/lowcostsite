

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class HomePageDestinationPageContainer  : Container<HomePageDestinationPageContainer, HomePageDestinationPage>{
#region Extra functions

#endregion

        #region Static Method
        
        public static HomePageDestinationPageContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            oHomePageDestinationPageContainer.Add(oHomePageDestinationPageContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oHomePageDestinationPageContainer;
        }

        
        public static HomePageDestinationPageContainer SelectAllHomePageDestinationPages(int? _WhiteLabelDocId,bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            oHomePageDestinationPageContainer.Add(oHomePageDestinationPageContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oHomePageDestinationPageContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //H_A
        //7_0
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DateCreated = _DateCreated;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_B
        //7_1
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DocId = _DocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_C
        //7_2
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.IsDeleted = _IsDeleted;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_E
        //7_4
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_HomePageDocId(
int _WhiteLabelDocId,
int _HomePageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.HomePageDocId = _HomePageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_HomePageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_F
        //7_5
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order(
int _WhiteLabelDocId,
int _Order , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.Order = _Order;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_G
        //7_6
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationPageDocId(
int _WhiteLabelDocId,
int _DestinationPageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DestinationPageDocId = _DestinationPageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationPageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DestinationPageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_A_B
        //7_0_1
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DateCreated = _DateCreated; 
 oHomePageDestinationPage.DocId = _DocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_A_C
        //7_0_2
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DateCreated = _DateCreated; 
 oHomePageDestinationPage.IsDeleted = _IsDeleted;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_A_E
        //7_0_4
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_HomePageDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _HomePageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DateCreated = _DateCreated; 
 oHomePageDestinationPage.HomePageDocId = _HomePageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_HomePageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_HomePageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_A_F
        //7_0_5
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Order(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Order , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DateCreated = _DateCreated; 
 oHomePageDestinationPage.Order = _Order;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Order));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_A_G
        //7_0_6
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DestinationPageDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DestinationPageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DateCreated = _DateCreated; 
 oHomePageDestinationPage.DestinationPageDocId = _DestinationPageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DestinationPageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationPageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_B_C
        //7_1_2
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DocId = _DocId; 
 oHomePageDestinationPage.IsDeleted = _IsDeleted;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_B_E
        //7_1_4
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_HomePageDocId(
int _WhiteLabelDocId,
int _DocId,
int _HomePageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DocId = _DocId; 
 oHomePageDestinationPage.HomePageDocId = _HomePageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_HomePageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_HomePageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_B_F
        //7_1_5
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Order(
int _WhiteLabelDocId,
int _DocId,
int _Order , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DocId = _DocId; 
 oHomePageDestinationPage.Order = _Order;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_B_G
        //7_1_6
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_DestinationPageDocId(
int _WhiteLabelDocId,
int _DocId,
int _DestinationPageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.DocId = _DocId; 
 oHomePageDestinationPage.DestinationPageDocId = _DestinationPageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DestinationPageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_DocId_DestinationPageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_C_E
        //7_2_4
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_HomePageDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _HomePageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.IsDeleted = _IsDeleted; 
 oHomePageDestinationPage.HomePageDocId = _HomePageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_HomePageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_HomePageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_C_F
        //7_2_5
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Order(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Order , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.IsDeleted = _IsDeleted; 
 oHomePageDestinationPage.Order = _Order;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Order));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_C_G
        //7_2_6
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationPageDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _DestinationPageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.IsDeleted = _IsDeleted; 
 oHomePageDestinationPage.DestinationPageDocId = _DestinationPageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationPageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationPageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_E_F
        //7_4_5
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_HomePageDocId_Order(
int _WhiteLabelDocId,
int _HomePageDocId,
int _Order , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.HomePageDocId = _HomePageDocId; 
 oHomePageDestinationPage.Order = _Order;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_HomePageDocId_Order(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_E_G
        //7_4_6
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_HomePageDocId_DestinationPageDocId(
int _WhiteLabelDocId,
int _HomePageDocId,
int _DestinationPageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.HomePageDocId = _HomePageDocId; 
 oHomePageDestinationPage.DestinationPageDocId = _DestinationPageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_HomePageDocId_DestinationPageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_HomePageDocId_DestinationPageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }



        //H_F_G
        //7_5_6
        public static HomePageDestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_DestinationPageDocId(
int _WhiteLabelDocId,
int _Order,
int _DestinationPageDocId , bool? isActive)
        {
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = new HomePageDestinationPageContainer();
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();
            #region Params
            
 oHomePageDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oHomePageDestinationPage.Order = _Order; 
 oHomePageDestinationPage.DestinationPageDocId = _DestinationPageDocId;
            #endregion 
            oHomePageDestinationPageContainer.Add(SelectData(HomePageDestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_DestinationPageDocId(oHomePageDestinationPage), TBNames_HomePageDestinationPage.PROC_Select_HomePageDestinationPage_By_Keys_View_WhiteLabelDocId_Order_DestinationPageDocId));
            #region ExtraFilters
            
if(isActive != null){
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oHomePageDestinationPageContainer;
        }


#endregion
}

    
}
