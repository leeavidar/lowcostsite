

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class TipContainer  : Container<TipContainer, Tip>{
#region Extra functions

#endregion

        #region Static Method
        
        public static TipContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            oTipContainer.Add(oTipContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oTipContainer;
        }

        
        public static TipContainer SelectAllTips(int? _WhiteLabelDocId,bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            oTipContainer.Add(oTipContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oTipContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //E_A
        //4_0
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DateCreated = _DateCreated;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_B
        //4_1
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DocId = _DocId;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_C
        //4_2
        public static TipContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.IsDeleted = _IsDeleted;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_F
        //4_5
        public static TipContainer SelectByKeysView_WhiteLabelDocId_Title(
int _WhiteLabelDocId,
string _Title , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.Title = _Title;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_Title(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTipContainer;
        }



        //E_G
        //4_6
        public static TipContainer SelectByKeysView_WhiteLabelDocId_ShortText(
int _WhiteLabelDocId,
string _ShortText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.ShortText = _ShortText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oTipContainer;
        }



        //E_H
        //4_7
        public static TipContainer SelectByKeysView_WhiteLabelDocId_LongText(
int _WhiteLabelDocId,
string _LongText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.LongText = _LongText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_LongText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oTipContainer;
        }



        //E_I
        //4_8
        public static TipContainer SelectByKeysView_WhiteLabelDocId_Order(
int _WhiteLabelDocId,
int _Order , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.Order = _Order;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_Order(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_J
        //4_9
        public static TipContainer SelectByKeysView_WhiteLabelDocId_Date(
int _WhiteLabelDocId,
DateTime _Date , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.Date = _Date;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_Date(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Date));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_A_B
        //4_0_1
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DateCreated = _DateCreated; 
 oTip.DocId = _DocId;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_A_C
        //4_0_2
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DateCreated = _DateCreated; 
 oTip.IsDeleted = _IsDeleted;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_A_F
        //4_0_5
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Title(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Title , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DateCreated = _DateCreated; 
 oTip.Title = _Title;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Title));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTipContainer;
        }



        //E_A_G
        //4_0_6
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DateCreated_ShortText(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _ShortText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DateCreated = _DateCreated; 
 oTip.ShortText = _ShortText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ShortText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oTipContainer;
        }



        //E_A_H
        //4_0_7
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LongText(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LongText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DateCreated = _DateCreated; 
 oTip.LongText = _LongText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LongText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oTipContainer;
        }



        //E_A_I
        //4_0_8
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Order(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Order , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DateCreated = _DateCreated; 
 oTip.Order = _Order;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_A_J
        //4_0_9
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Date(
int _WhiteLabelDocId,
DateTime _DateCreated,
DateTime _Date , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DateCreated = _DateCreated; 
 oTip.Date = _Date;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Date(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Date));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_B_C
        //4_1_2
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DocId = _DocId; 
 oTip.IsDeleted = _IsDeleted;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_B_F
        //4_1_5
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DocId_Title(
int _WhiteLabelDocId,
int _DocId,
string _Title , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DocId = _DocId; 
 oTip.Title = _Title;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTipContainer;
        }



        //E_B_G
        //4_1_6
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DocId_ShortText(
int _WhiteLabelDocId,
int _DocId,
string _ShortText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DocId = _DocId; 
 oTip.ShortText = _ShortText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ShortText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oTipContainer;
        }



        //E_B_H
        //4_1_7
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DocId_LongText(
int _WhiteLabelDocId,
int _DocId,
string _LongText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DocId = _DocId; 
 oTip.LongText = _LongText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LongText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oTipContainer;
        }



        //E_B_I
        //4_1_8
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DocId_Order(
int _WhiteLabelDocId,
int _DocId,
int _Order , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DocId = _DocId; 
 oTip.Order = _Order;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_B_J
        //4_1_9
        public static TipContainer SelectByKeysView_WhiteLabelDocId_DocId_Date(
int _WhiteLabelDocId,
int _DocId,
DateTime _Date , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.DocId = _DocId; 
 oTip.Date = _Date;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Date(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Date));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_C_F
        //4_2_5
        public static TipContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Title(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Title , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.IsDeleted = _IsDeleted; 
 oTip.Title = _Title;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Title));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTipContainer;
        }



        //E_C_G
        //4_2_6
        public static TipContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_ShortText(
int _WhiteLabelDocId,
bool _IsDeleted,
string _ShortText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.IsDeleted = _IsDeleted; 
 oTip.ShortText = _ShortText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ShortText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oTipContainer;
        }



        //E_C_H
        //4_2_7
        public static TipContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LongText(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LongText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.IsDeleted = _IsDeleted; 
 oTip.LongText = _LongText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LongText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oTipContainer;
        }



        //E_C_I
        //4_2_8
        public static TipContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Order(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Order , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.IsDeleted = _IsDeleted; 
 oTip.Order = _Order;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_C_J
        //4_2_9
        public static TipContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Date(
int _WhiteLabelDocId,
bool _IsDeleted,
DateTime _Date , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.IsDeleted = _IsDeleted; 
 oTip.Date = _Date;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Date(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Date));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }



        //E_F_G
        //4_5_6
        public static TipContainer SelectByKeysView_WhiteLabelDocId_Title_ShortText(
int _WhiteLabelDocId,
string _Title,
string _ShortText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.Title = _Title; 
 oTip.ShortText = _ShortText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_ShortText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.Title, _Title) && string.Equals(R.ShortText, _ShortText));
            #endregion
            return oTipContainer;
        }



        //E_F_H
        //4_5_7
        public static TipContainer SelectByKeysView_WhiteLabelDocId_Title_LongText(
int _WhiteLabelDocId,
string _Title,
string _LongText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.Title = _Title; 
 oTip.LongText = _LongText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_LongText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.Title, _Title) && string.Equals(R.LongText, _LongText));
            #endregion
            return oTipContainer;
        }



        //E_F_I
        //4_5_8
        public static TipContainer SelectByKeysView_WhiteLabelDocId_Title_Order(
int _WhiteLabelDocId,
string _Title,
int _Order , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.Title = _Title; 
 oTip.Order = _Order;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Order(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTipContainer;
        }



        //E_F_J
        //4_5_9
        public static TipContainer SelectByKeysView_WhiteLabelDocId_Title_Date(
int _WhiteLabelDocId,
string _Title,
DateTime _Date , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.Title = _Title; 
 oTip.Date = _Date;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Date(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Title_Date));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTipContainer;
        }



        //E_G_H
        //4_6_7
        public static TipContainer SelectByKeysView_WhiteLabelDocId_ShortText_LongText(
int _WhiteLabelDocId,
string _ShortText,
string _LongText , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.ShortText = _ShortText; 
 oTip.LongText = _LongText;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_LongText(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText) && string.Equals(R.LongText, _LongText));
            #endregion
            return oTipContainer;
        }



        //E_G_I
        //4_6_8
        public static TipContainer SelectByKeysView_WhiteLabelDocId_ShortText_Order(
int _WhiteLabelDocId,
string _ShortText,
int _Order , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.ShortText = _ShortText; 
 oTip.Order = _Order;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_Order(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oTipContainer;
        }



        //E_G_J
        //4_6_9
        public static TipContainer SelectByKeysView_WhiteLabelDocId_ShortText_Date(
int _WhiteLabelDocId,
string _ShortText,
DateTime _Date , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.ShortText = _ShortText; 
 oTip.Date = _Date;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_Date(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_Date));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oTipContainer;
        }



        //E_H_I
        //4_7_8
        public static TipContainer SelectByKeysView_WhiteLabelDocId_LongText_Order(
int _WhiteLabelDocId,
string _LongText,
int _Order , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.LongText = _LongText; 
 oTip.Order = _Order;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_LongText_Order(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_LongText_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oTipContainer;
        }



        //E_H_J
        //4_7_9
        public static TipContainer SelectByKeysView_WhiteLabelDocId_LongText_Date(
int _WhiteLabelDocId,
string _LongText,
DateTime _Date , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.LongText = _LongText; 
 oTip.Date = _Date;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_LongText_Date(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_LongText_Date));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTipContainer = oTipContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oTipContainer;
        }



        //E_I_J
        //4_8_9
        public static TipContainer SelectByKeysView_WhiteLabelDocId_Order_Date(
int _WhiteLabelDocId,
int _Order,
DateTime _Date , bool? isActive)
        {
            TipContainer oTipContainer = new TipContainer();
            Tip oTip = new Tip();
            #region Params
            
 oTip.WhiteLabelDocId = _WhiteLabelDocId; 
 oTip.Order = _Order; 
 oTip.Date = _Date;
            #endregion 
            oTipContainer.Add(SelectData(Tip.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Date(oTip), TBNames_Tip.PROC_Select_Tip_By_Keys_View_WhiteLabelDocId_Order_Date));
            #region ExtraFilters
            
if(isActive != null){
                oTipContainer = oTipContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTipContainer;
        }


#endregion
}

    
}
