

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class AirportContainer  : Container<AirportContainer, Airport>{
#region Extra functions

#endregion

        #region Static Method
        
        public static AirportContainer SelectByID(int doc_id,bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            oAirportContainer.Add(oAirportContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oAirportContainer;
        }

        
        public static AirportContainer SelectAllAirports(bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            oAirportContainer.Add(oAirportContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oAirportContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static AirportContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DateCreated = _DateCreated;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DateCreated(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //B
        //1
        public static AirportContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DocId = _DocId;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DocId(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //C
        //2
        public static AirportContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IsDeleted = _IsDeleted;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IsDeleted(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //E
        //4
        public static AirportContainer SelectByKeysView_IataCode(
string _IataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IataCode = _IataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //F
        //5
        public static AirportContainer SelectByKeysView_CityIataCode(
string _CityIataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.CityIataCode = _CityIataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_CityIataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //G
        //6
        public static AirportContainer SelectByKeysView_EnglishName(
string _EnglishName , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.EnglishName = _EnglishName;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_EnglishName(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //H
        //7
        public static AirportContainer SelectByKeysView_NameByLang(
string _NameByLang , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.NameByLang = _NameByLang;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_NameByLang(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_NameByLang));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirportContainer = oAirportContainer.FindAllContainer(R => string.Equals(R.NameByLang, _NameByLang));
            #endregion
            return oAirportContainer;
        }



        //A_B
        //0_1
        public static AirportContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DateCreated = _DateCreated; 
 oAirport.DocId = _DocId;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DateCreated_DocId(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //A_C
        //0_2
        public static AirportContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DateCreated = _DateCreated; 
 oAirport.IsDeleted = _IsDeleted;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //A_E
        //0_4
        public static AirportContainer SelectByKeysView_DateCreated_IataCode(
DateTime _DateCreated,
string _IataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DateCreated = _DateCreated; 
 oAirport.IataCode = _IataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DateCreated_IataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DateCreated_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //A_F
        //0_5
        public static AirportContainer SelectByKeysView_DateCreated_CityIataCode(
DateTime _DateCreated,
string _CityIataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DateCreated = _DateCreated; 
 oAirport.CityIataCode = _CityIataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DateCreated_CityIataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DateCreated_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //A_G
        //0_6
        public static AirportContainer SelectByKeysView_DateCreated_EnglishName(
DateTime _DateCreated,
string _EnglishName , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DateCreated = _DateCreated; 
 oAirport.EnglishName = _EnglishName;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DateCreated_EnglishName(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DateCreated_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //A_H
        //0_7
        public static AirportContainer SelectByKeysView_DateCreated_NameByLang(
DateTime _DateCreated,
string _NameByLang , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DateCreated = _DateCreated; 
 oAirport.NameByLang = _NameByLang;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DateCreated_NameByLang(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DateCreated_NameByLang));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirportContainer = oAirportContainer.FindAllContainer(R => string.Equals(R.NameByLang, _NameByLang));
            #endregion
            return oAirportContainer;
        }



        //B_C
        //1_2
        public static AirportContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DocId = _DocId; 
 oAirport.IsDeleted = _IsDeleted;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DocId_IsDeleted(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //B_E
        //1_4
        public static AirportContainer SelectByKeysView_DocId_IataCode(
int _DocId,
string _IataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DocId = _DocId; 
 oAirport.IataCode = _IataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DocId_IataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DocId_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //B_F
        //1_5
        public static AirportContainer SelectByKeysView_DocId_CityIataCode(
int _DocId,
string _CityIataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DocId = _DocId; 
 oAirport.CityIataCode = _CityIataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DocId_CityIataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DocId_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //B_G
        //1_6
        public static AirportContainer SelectByKeysView_DocId_EnglishName(
int _DocId,
string _EnglishName , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DocId = _DocId; 
 oAirport.EnglishName = _EnglishName;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DocId_EnglishName(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DocId_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //B_H
        //1_7
        public static AirportContainer SelectByKeysView_DocId_NameByLang(
int _DocId,
string _NameByLang , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.DocId = _DocId; 
 oAirport.NameByLang = _NameByLang;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_DocId_NameByLang(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_DocId_NameByLang));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirportContainer = oAirportContainer.FindAllContainer(R => string.Equals(R.NameByLang, _NameByLang));
            #endregion
            return oAirportContainer;
        }



        //C_E
        //2_4
        public static AirportContainer SelectByKeysView_IsDeleted_IataCode(
bool _IsDeleted,
string _IataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IsDeleted = _IsDeleted; 
 oAirport.IataCode = _IataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IsDeleted_IataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IsDeleted_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //C_F
        //2_5
        public static AirportContainer SelectByKeysView_IsDeleted_CityIataCode(
bool _IsDeleted,
string _CityIataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IsDeleted = _IsDeleted; 
 oAirport.CityIataCode = _CityIataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IsDeleted_CityIataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IsDeleted_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //C_G
        //2_6
        public static AirportContainer SelectByKeysView_IsDeleted_EnglishName(
bool _IsDeleted,
string _EnglishName , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IsDeleted = _IsDeleted; 
 oAirport.EnglishName = _EnglishName;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IsDeleted_EnglishName(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IsDeleted_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //C_H
        //2_7
        public static AirportContainer SelectByKeysView_IsDeleted_NameByLang(
bool _IsDeleted,
string _NameByLang , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IsDeleted = _IsDeleted; 
 oAirport.NameByLang = _NameByLang;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IsDeleted_NameByLang(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IsDeleted_NameByLang));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirportContainer = oAirportContainer.FindAllContainer(R => string.Equals(R.NameByLang, _NameByLang));
            #endregion
            return oAirportContainer;
        }



        //E_F
        //4_5
        public static AirportContainer SelectByKeysView_IataCode_CityIataCode(
string _IataCode,
string _CityIataCode , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IataCode = _IataCode; 
 oAirport.CityIataCode = _CityIataCode;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IataCode_CityIataCode(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IataCode_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //E_G
        //4_6
        public static AirportContainer SelectByKeysView_IataCode_EnglishName(
string _IataCode,
string _EnglishName , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IataCode = _IataCode; 
 oAirport.EnglishName = _EnglishName;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IataCode_EnglishName(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IataCode_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //E_H
        //4_7
        public static AirportContainer SelectByKeysView_IataCode_NameByLang(
string _IataCode,
string _NameByLang , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.IataCode = _IataCode; 
 oAirport.NameByLang = _NameByLang;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_IataCode_NameByLang(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_IataCode_NameByLang));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirportContainer = oAirportContainer.FindAllContainer(R => string.Equals(R.NameByLang, _NameByLang));
            #endregion
            return oAirportContainer;
        }



        //F_G
        //5_6
        public static AirportContainer SelectByKeysView_CityIataCode_EnglishName(
string _CityIataCode,
string _EnglishName , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.CityIataCode = _CityIataCode; 
 oAirport.EnglishName = _EnglishName;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_CityIataCode_EnglishName(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_CityIataCode_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirportContainer;
        }



        //F_H
        //5_7
        public static AirportContainer SelectByKeysView_CityIataCode_NameByLang(
string _CityIataCode,
string _NameByLang , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.CityIataCode = _CityIataCode; 
 oAirport.NameByLang = _NameByLang;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_CityIataCode_NameByLang(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_CityIataCode_NameByLang));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirportContainer = oAirportContainer.FindAllContainer(R => string.Equals(R.NameByLang, _NameByLang));
            #endregion
            return oAirportContainer;
        }



        //G_H
        //6_7
        public static AirportContainer SelectByKeysView_EnglishName_NameByLang(
string _EnglishName,
string _NameByLang , bool? isActive)
        {
            AirportContainer oAirportContainer = new AirportContainer();
            Airport oAirport = new Airport();
            #region Params
            
 oAirport.EnglishName = _EnglishName; 
 oAirport.NameByLang = _NameByLang;
            #endregion 
            oAirportContainer.Add(SelectData(Airport.GetParamsForSelectByKeysView_EnglishName_NameByLang(oAirport), TBNames_Airport.PROC_Select_Airport_By_Keys_View_EnglishName_NameByLang));
            #region ExtraFilters
            
if(isActive != null){
                oAirportContainer = oAirportContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirportContainer = oAirportContainer.FindAllContainer(R => string.Equals(R.NameByLang, _NameByLang));
            #endregion
            return oAirportContainer;
        }


#endregion
}

    
}
