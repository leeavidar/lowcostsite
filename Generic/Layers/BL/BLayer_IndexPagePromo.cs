

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class IndexPagePromoContainer  : Container<IndexPagePromoContainer, IndexPagePromo>{
#region Extra functions

#endregion

        #region Static Method
        
        public static IndexPagePromoContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            oIndexPagePromoContainer.Add(oIndexPagePromoContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oIndexPagePromoContainer;
        }

        
        public static IndexPagePromoContainer SelectAllIndexPagePromos(int? _WhiteLabelDocId,bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            oIndexPagePromoContainer.Add(oIndexPagePromoContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oIndexPagePromoContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //K_A
        //10_0
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B
        //10_1
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C
        //10_2
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_E
        //10_4
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_ImagesDocId(
int _WhiteLabelDocId,
int _ImagesDocId , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_F
        //10_5
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Title(
int _WhiteLabelDocId,
string _Title , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Title = _Title;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Title(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_G
        //10_6
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Text(
int _WhiteLabelDocId,
string _Text , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Text = _Text;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Text(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_H
        //10_7
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Link(
int _WhiteLabelDocId,
string _Link , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Link = _Link;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Link(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_I
        //10_8
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_PromoType(
int _WhiteLabelDocId,
int _PromoType , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.PromoType = _PromoType;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_PromoType(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_J
        //10_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Order(
int _WhiteLabelDocId,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_L
        //10_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_PriceAfter(
int _WhiteLabelDocId,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_M
        //10_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_PriceBefore(
int _WhiteLabelDocId,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_B
        //10_0_1
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.DocId = _DocId;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_C
        //10_0_2
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.IsDeleted = _IsDeleted;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_E
        //10_0_4
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_ImagesDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _ImagesDocId , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ImagesDocId(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_ImagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_F
        //10_0_5
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Title(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Title , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.Title = _Title;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Title));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_G
        //10_0_6
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Text(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Text , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.Text = _Text;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Text(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Text));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_H
        //10_0_7
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Link(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Link , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.Link = _Link;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Link(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Link));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_I
        //10_0_8
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PromoType(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _PromoType , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.PromoType = _PromoType;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PromoType(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PromoType));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_J
        //10_0_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Order(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_L
        //10_0_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PriceAfter(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_A_M
        //10_0_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PriceBefore(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DateCreated = _DateCreated; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_C
        //10_1_2
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_E
        //10_1_4
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_ImagesDocId(
int _WhiteLabelDocId,
int _DocId,
int _ImagesDocId , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ImagesDocId(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_ImagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_F
        //10_1_5
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_Title(
int _WhiteLabelDocId,
int _DocId,
string _Title , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.Title = _Title;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_G
        //10_1_6
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_Text(
int _WhiteLabelDocId,
int _DocId,
string _Text , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.Text = _Text;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Text(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_H
        //10_1_7
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_Link(
int _WhiteLabelDocId,
int _DocId,
string _Link , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.Link = _Link;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Link(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Link));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_I
        //10_1_8
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_PromoType(
int _WhiteLabelDocId,
int _DocId,
int _PromoType , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.PromoType = _PromoType;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PromoType(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PromoType));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_J
        //10_1_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_Order(
int _WhiteLabelDocId,
int _DocId,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_L
        //10_1_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_PriceAfter(
int _WhiteLabelDocId,
int _DocId,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_B_M
        //10_1_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_DocId_PriceBefore(
int _WhiteLabelDocId,
int _DocId,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.DocId = _DocId; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C_E
        //10_2_4
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_ImagesDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _ImagesDocId , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ImagesDocId(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_ImagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C_F
        //10_2_5
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Title(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Title , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted; 
 oIndexPagePromo.Title = _Title;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Title));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C_G
        //10_2_6
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Text(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Text , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted; 
 oIndexPagePromo.Text = _Text;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Text(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Text));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C_H
        //10_2_7
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Link(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Link , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted; 
 oIndexPagePromo.Link = _Link;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Link(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Link));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C_I
        //10_2_8
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PromoType(
int _WhiteLabelDocId,
bool _IsDeleted,
int _PromoType , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted; 
 oIndexPagePromo.PromoType = _PromoType;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PromoType(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PromoType));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C_J
        //10_2_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Order(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C_L
        //10_2_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PriceAfter(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_C_M
        //10_2_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PriceBefore(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.IsDeleted = _IsDeleted; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_E_F
        //10_4_5
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_ImagesDocId_Title(
int _WhiteLabelDocId,
int _ImagesDocId,
string _Title , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId; 
 oIndexPagePromo.Title = _Title;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_Title(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_E_G
        //10_4_6
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_ImagesDocId_Text(
int _WhiteLabelDocId,
int _ImagesDocId,
string _Text , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId; 
 oIndexPagePromo.Text = _Text;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_Text(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_E_H
        //10_4_7
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_ImagesDocId_Link(
int _WhiteLabelDocId,
int _ImagesDocId,
string _Link , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId; 
 oIndexPagePromo.Link = _Link;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_Link(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Link));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_E_I
        //10_4_8
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_ImagesDocId_PromoType(
int _WhiteLabelDocId,
int _ImagesDocId,
int _PromoType , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId; 
 oIndexPagePromo.PromoType = _PromoType;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_PromoType(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PromoType));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_E_J
        //10_4_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_ImagesDocId_Order(
int _WhiteLabelDocId,
int _ImagesDocId,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_E_L
        //10_4_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_ImagesDocId_PriceAfter(
int _WhiteLabelDocId,
int _ImagesDocId,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_E_M
        //10_4_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_ImagesDocId_PriceBefore(
int _WhiteLabelDocId,
int _ImagesDocId,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.ImagesDocId = _ImagesDocId; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_ImagesDocId_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_F_G
        //10_5_6
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Title_Text(
int _WhiteLabelDocId,
string _Title,
string _Text , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Title = _Title; 
 oIndexPagePromo.Text = _Text;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Text(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Text));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title) && string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_F_H
        //10_5_7
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Title_Link(
int _WhiteLabelDocId,
string _Title,
string _Link , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Title = _Title; 
 oIndexPagePromo.Link = _Link;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Link(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Link));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_F_I
        //10_5_8
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Title_PromoType(
int _WhiteLabelDocId,
string _Title,
int _PromoType , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Title = _Title; 
 oIndexPagePromo.PromoType = _PromoType;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PromoType(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PromoType));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_F_J
        //10_5_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Title_Order(
int _WhiteLabelDocId,
string _Title,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Title = _Title; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_F_L
        //10_5_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Title_PriceAfter(
int _WhiteLabelDocId,
string _Title,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Title = _Title; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_F_M
        //10_5_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Title_PriceBefore(
int _WhiteLabelDocId,
string _Title,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Title = _Title; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_G_H
        //10_6_7
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Text_Link(
int _WhiteLabelDocId,
string _Text,
string _Link , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Text = _Text; 
 oIndexPagePromo.Link = _Link;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Link(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_Link));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_G_I
        //10_6_8
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Text_PromoType(
int _WhiteLabelDocId,
string _Text,
int _PromoType , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Text = _Text; 
 oIndexPagePromo.PromoType = _PromoType;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_PromoType(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PromoType));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_G_J
        //10_6_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Text_Order(
int _WhiteLabelDocId,
string _Text,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Text = _Text; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_G_L
        //10_6_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Text_PriceAfter(
int _WhiteLabelDocId,
string _Text,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Text = _Text; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_G_M
        //10_6_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Text_PriceBefore(
int _WhiteLabelDocId,
string _Text,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Text = _Text; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_H_I
        //10_7_8
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Link_PromoType(
int _WhiteLabelDocId,
string _Link,
int _PromoType , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Link = _Link; 
 oIndexPagePromo.PromoType = _PromoType;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Link_PromoType(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PromoType));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_H_J
        //10_7_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Link_Order(
int _WhiteLabelDocId,
string _Link,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Link = _Link; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Link_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_H_L
        //10_7_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Link_PriceAfter(
int _WhiteLabelDocId,
string _Link,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Link = _Link; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Link_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_H_M
        //10_7_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Link_PriceBefore(
int _WhiteLabelDocId,
string _Link,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Link = _Link; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Link_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_I_J
        //10_8_9
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_PromoType_Order(
int _WhiteLabelDocId,
int _PromoType,
int _Order , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.PromoType = _PromoType; 
 oIndexPagePromo.Order = _Order;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_PromoType_Order(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_Order));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_I_L
        //10_8_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_PromoType_PriceAfter(
int _WhiteLabelDocId,
int _PromoType,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.PromoType = _PromoType; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_PromoType_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_I_M
        //10_8_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_PromoType_PriceBefore(
int _WhiteLabelDocId,
int _PromoType,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.PromoType = _PromoType; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_PromoType_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_J_L
        //10_9_11
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Order_PriceAfter(
int _WhiteLabelDocId,
int _Order,
Double _PriceAfter , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Order = _Order; 
 oIndexPagePromo.PriceAfter = _PriceAfter;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_PriceAfter(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order_PriceAfter));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_J_M
        //10_9_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_Order_PriceBefore(
int _WhiteLabelDocId,
int _Order,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.Order = _Order; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }



        //K_L_M
        //10_11_12
        public static IndexPagePromoContainer SelectByKeysView_WhiteLabelDocId_PriceAfter_PriceBefore(
int _WhiteLabelDocId,
Double _PriceAfter,
Double _PriceBefore , bool? isActive)
        {
            IndexPagePromoContainer oIndexPagePromoContainer = new IndexPagePromoContainer();
            IndexPagePromo oIndexPagePromo = new IndexPagePromo();
            #region Params
            
 oIndexPagePromo.WhiteLabelDocId = _WhiteLabelDocId; 
 oIndexPagePromo.PriceAfter = _PriceAfter; 
 oIndexPagePromo.PriceBefore = _PriceBefore;
            #endregion 
            oIndexPagePromoContainer.Add(SelectData(IndexPagePromo.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceAfter_PriceBefore(oIndexPagePromo), TBNames_IndexPagePromo.PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceAfter_PriceBefore));
            #region ExtraFilters
            
if(isActive != null){
                oIndexPagePromoContainer = oIndexPagePromoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oIndexPagePromoContainer;
        }


#endregion
}

    
}
