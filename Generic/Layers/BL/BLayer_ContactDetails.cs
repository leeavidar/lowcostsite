

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class ContactDetailsContainer  : Container<ContactDetailsContainer, ContactDetails>{
#region Extra functions

#endregion

        #region Static Method
        
        public static ContactDetailsContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            oContactDetailsContainer.Add(oContactDetailsContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oContactDetailsContainer;
        }

        
        public static ContactDetailsContainer SelectAllContactDetailss(int? _WhiteLabelDocId,bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            oContactDetailsContainer.Add(oContactDetailsContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oContactDetailsContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //U_A
        //20_0
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B
        //20_1
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C
        //20_2
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E
        //20_4
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName(
int _WhiteLabelDocId,
string _FirstName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F
        //20_5
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName(
int _WhiteLabelDocId,
string _LastName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G
        //20_6
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company(
int _WhiteLabelDocId,
string _Company , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H
        //20_7
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat(
int _WhiteLabelDocId,
string _Flat , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I
        //20_8
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName(
int _WhiteLabelDocId,
string _BuildingName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J
        //20_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber(
int _WhiteLabelDocId,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K
        //20_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street(
int _WhiteLabelDocId,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L
        //20_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality(
int _WhiteLabelDocId,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M
        //20_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City(
int _WhiteLabelDocId,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_N
        //20_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Province(
int _WhiteLabelDocId,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_O
        //20_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_ZipCode(
int _WhiteLabelDocId,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_P
        //20_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_CountryCode(
int _WhiteLabelDocId,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_Q
        //20_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Email(
int _WhiteLabelDocId,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_R
        //20_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_MainPhone(
int _WhiteLabelDocId,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_S
        //20_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_MobliePhone(
int _WhiteLabelDocId,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_T
        //20_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_OrdersDocId(
int _WhiteLabelDocId,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_V
        //20_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Title(
int _WhiteLabelDocId,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_B
        //20_0_1
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.DocId = _DocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_C
        //20_0_2
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.IsDeleted = _IsDeleted;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_E
        //20_0_4
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FirstName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.FirstName = _FirstName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_F
        //20_0_5
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LastName(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LastName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.LastName = _LastName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LastName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_G
        //20_0_6
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Company(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Company , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.Company = _Company;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Company(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Company));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_H
        //20_0_7
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Flat(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Flat , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.Flat = _Flat;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Flat(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Flat));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_I
        //20_0_8
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_BuildingName(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _BuildingName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.BuildingName = _BuildingName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_BuildingName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_BuildingName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_J
        //20_0_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_BuildingNumber(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_K
        //20_0_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Street(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_L
        //20_0_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Locality(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_M
        //20_0_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_City(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_N
        //20_0_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Province(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_O
        //20_0_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_ZipCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_P
        //20_0_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_CountryCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_Q
        //20_0_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Email(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_R
        //20_0_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_MainPhone(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_S
        //20_0_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_MobliePhone(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_T
        //20_0_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_A_V
        //20_0_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Title(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DateCreated = _DateCreated; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DateCreated_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_C
        //20_1_2
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.IsDeleted = _IsDeleted;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_E
        //20_1_4
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_FirstName(
int _WhiteLabelDocId,
int _DocId,
string _FirstName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.FirstName = _FirstName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FirstName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_F
        //20_1_5
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_LastName(
int _WhiteLabelDocId,
int _DocId,
string _LastName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.LastName = _LastName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LastName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_G
        //20_1_6
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_Company(
int _WhiteLabelDocId,
int _DocId,
string _Company , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.Company = _Company;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Company(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Company));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_H
        //20_1_7
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_Flat(
int _WhiteLabelDocId,
int _DocId,
string _Flat , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.Flat = _Flat;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Flat(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Flat));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_I
        //20_1_8
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_BuildingName(
int _WhiteLabelDocId,
int _DocId,
string _BuildingName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.BuildingName = _BuildingName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_BuildingName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_BuildingName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_J
        //20_1_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_BuildingNumber(
int _WhiteLabelDocId,
int _DocId,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_K
        //20_1_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_Street(
int _WhiteLabelDocId,
int _DocId,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_L
        //20_1_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_Locality(
int _WhiteLabelDocId,
int _DocId,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_M
        //20_1_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_City(
int _WhiteLabelDocId,
int _DocId,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_N
        //20_1_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_Province(
int _WhiteLabelDocId,
int _DocId,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_O
        //20_1_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_ZipCode(
int _WhiteLabelDocId,
int _DocId,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_P
        //20_1_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_CountryCode(
int _WhiteLabelDocId,
int _DocId,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_Q
        //20_1_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_Email(
int _WhiteLabelDocId,
int _DocId,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_R
        //20_1_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_MainPhone(
int _WhiteLabelDocId,
int _DocId,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_S
        //20_1_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_MobliePhone(
int _WhiteLabelDocId,
int _DocId,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_T
        //20_1_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(
int _WhiteLabelDocId,
int _DocId,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_B_V
        //20_1_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_DocId_Title(
int _WhiteLabelDocId,
int _DocId,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.DocId = _DocId; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_DocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_E
        //20_2_4
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FirstName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.FirstName = _FirstName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_F
        //20_2_5
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LastName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.LastName = _LastName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_G
        //20_2_6
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Company(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Company , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.Company = _Company;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Company(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Company));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_H
        //20_2_7
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Flat(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Flat , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.Flat = _Flat;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Flat(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Flat));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_I
        //20_2_8
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_BuildingName(
int _WhiteLabelDocId,
bool _IsDeleted,
string _BuildingName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.BuildingName = _BuildingName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_BuildingName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_BuildingName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_J
        //20_2_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_BuildingNumber(
int _WhiteLabelDocId,
bool _IsDeleted,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_K
        //20_2_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Street(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_L
        //20_2_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Locality(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_M
        //20_2_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_City(
int _WhiteLabelDocId,
bool _IsDeleted,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_N
        //20_2_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Province(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_O
        //20_2_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_ZipCode(
int _WhiteLabelDocId,
bool _IsDeleted,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_P
        //20_2_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_CountryCode(
int _WhiteLabelDocId,
bool _IsDeleted,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_Q
        //20_2_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Email(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_R
        //20_2_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_MainPhone(
int _WhiteLabelDocId,
bool _IsDeleted,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_S
        //20_2_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_MobliePhone(
int _WhiteLabelDocId,
bool _IsDeleted,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_T
        //20_2_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_C_V
        //20_2_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Title(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.IsDeleted = _IsDeleted; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_IsDeleted_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_F
        //20_4_5
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_LastName(
int _WhiteLabelDocId,
string _FirstName,
string _LastName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.LastName = _LastName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_LastName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_G
        //20_4_6
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_Company(
int _WhiteLabelDocId,
string _FirstName,
string _Company , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.Company = _Company;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Company(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Company));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_H
        //20_4_7
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_Flat(
int _WhiteLabelDocId,
string _FirstName,
string _Flat , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.Flat = _Flat;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Flat(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Flat));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_I
        //20_4_8
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_BuildingName(
int _WhiteLabelDocId,
string _FirstName,
string _BuildingName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.BuildingName = _BuildingName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_BuildingName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_BuildingName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_J
        //20_4_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_BuildingNumber(
int _WhiteLabelDocId,
string _FirstName,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_K
        //20_4_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_Street(
int _WhiteLabelDocId,
string _FirstName,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_L
        //20_4_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_Locality(
int _WhiteLabelDocId,
string _FirstName,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_M
        //20_4_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_City(
int _WhiteLabelDocId,
string _FirstName,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_N
        //20_4_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_Province(
int _WhiteLabelDocId,
string _FirstName,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_O
        //20_4_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_ZipCode(
int _WhiteLabelDocId,
string _FirstName,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_P
        //20_4_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_CountryCode(
int _WhiteLabelDocId,
string _FirstName,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_Q
        //20_4_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_Email(
int _WhiteLabelDocId,
string _FirstName,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_R
        //20_4_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_MainPhone(
int _WhiteLabelDocId,
string _FirstName,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_S
        //20_4_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_MobliePhone(
int _WhiteLabelDocId,
string _FirstName,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_T
        //20_4_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_OrdersDocId(
int _WhiteLabelDocId,
string _FirstName,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_E_V
        //20_4_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_FirstName_Title(
int _WhiteLabelDocId,
string _FirstName,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.FirstName = _FirstName; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_FirstName_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_G
        //20_5_6
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_Company(
int _WhiteLabelDocId,
string _LastName,
string _Company , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.Company = _Company;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Company(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Company));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_H
        //20_5_7
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_Flat(
int _WhiteLabelDocId,
string _LastName,
string _Flat , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.Flat = _Flat;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Flat(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Flat));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_I
        //20_5_8
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_BuildingName(
int _WhiteLabelDocId,
string _LastName,
string _BuildingName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.BuildingName = _BuildingName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_BuildingName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_BuildingName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_J
        //20_5_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_BuildingNumber(
int _WhiteLabelDocId,
string _LastName,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_K
        //20_5_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_Street(
int _WhiteLabelDocId,
string _LastName,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_L
        //20_5_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_Locality(
int _WhiteLabelDocId,
string _LastName,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_M
        //20_5_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_City(
int _WhiteLabelDocId,
string _LastName,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_N
        //20_5_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_Province(
int _WhiteLabelDocId,
string _LastName,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_O
        //20_5_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_ZipCode(
int _WhiteLabelDocId,
string _LastName,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_P
        //20_5_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_CountryCode(
int _WhiteLabelDocId,
string _LastName,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_Q
        //20_5_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_Email(
int _WhiteLabelDocId,
string _LastName,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_R
        //20_5_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_MainPhone(
int _WhiteLabelDocId,
string _LastName,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_S
        //20_5_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_MobliePhone(
int _WhiteLabelDocId,
string _LastName,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_T
        //20_5_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_OrdersDocId(
int _WhiteLabelDocId,
string _LastName,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_F_V
        //20_5_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_LastName_Title(
int _WhiteLabelDocId,
string _LastName,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.LastName = _LastName; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_LastName_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_H
        //20_6_7
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_Flat(
int _WhiteLabelDocId,
string _Company,
string _Flat , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.Flat = _Flat;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Flat(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Flat));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_I
        //20_6_8
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_BuildingName(
int _WhiteLabelDocId,
string _Company,
string _BuildingName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.BuildingName = _BuildingName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_BuildingName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_BuildingName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_J
        //20_6_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_BuildingNumber(
int _WhiteLabelDocId,
string _Company,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_K
        //20_6_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_Street(
int _WhiteLabelDocId,
string _Company,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_L
        //20_6_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_Locality(
int _WhiteLabelDocId,
string _Company,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_M
        //20_6_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_City(
int _WhiteLabelDocId,
string _Company,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_N
        //20_6_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_Province(
int _WhiteLabelDocId,
string _Company,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_O
        //20_6_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_ZipCode(
int _WhiteLabelDocId,
string _Company,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_P
        //20_6_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_CountryCode(
int _WhiteLabelDocId,
string _Company,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_Q
        //20_6_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_Email(
int _WhiteLabelDocId,
string _Company,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_R
        //20_6_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_MainPhone(
int _WhiteLabelDocId,
string _Company,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_S
        //20_6_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_MobliePhone(
int _WhiteLabelDocId,
string _Company,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_T
        //20_6_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_OrdersDocId(
int _WhiteLabelDocId,
string _Company,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_G_V
        //20_6_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Company_Title(
int _WhiteLabelDocId,
string _Company,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Company = _Company; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Company_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Company_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_I
        //20_7_8
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_BuildingName(
int _WhiteLabelDocId,
string _Flat,
string _BuildingName , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.BuildingName = _BuildingName;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_BuildingName(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_BuildingName));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_J
        //20_7_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_BuildingNumber(
int _WhiteLabelDocId,
string _Flat,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_K
        //20_7_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_Street(
int _WhiteLabelDocId,
string _Flat,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_L
        //20_7_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_Locality(
int _WhiteLabelDocId,
string _Flat,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_M
        //20_7_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_City(
int _WhiteLabelDocId,
string _Flat,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_N
        //20_7_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_Province(
int _WhiteLabelDocId,
string _Flat,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_O
        //20_7_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_ZipCode(
int _WhiteLabelDocId,
string _Flat,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_P
        //20_7_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_CountryCode(
int _WhiteLabelDocId,
string _Flat,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_Q
        //20_7_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_Email(
int _WhiteLabelDocId,
string _Flat,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_R
        //20_7_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_MainPhone(
int _WhiteLabelDocId,
string _Flat,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_S
        //20_7_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_MobliePhone(
int _WhiteLabelDocId,
string _Flat,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_T
        //20_7_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_OrdersDocId(
int _WhiteLabelDocId,
string _Flat,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_H_V
        //20_7_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Flat_Title(
int _WhiteLabelDocId,
string _Flat,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Flat = _Flat; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Flat_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Flat_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_J
        //20_8_9
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_BuildingNumber(
int _WhiteLabelDocId,
string _BuildingName,
string _BuildingNumber , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.BuildingNumber = _BuildingNumber;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_BuildingNumber(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_BuildingNumber));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_K
        //20_8_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_Street(
int _WhiteLabelDocId,
string _BuildingName,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_L
        //20_8_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_Locality(
int _WhiteLabelDocId,
string _BuildingName,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_M
        //20_8_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_City(
int _WhiteLabelDocId,
string _BuildingName,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_N
        //20_8_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_Province(
int _WhiteLabelDocId,
string _BuildingName,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_O
        //20_8_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_ZipCode(
int _WhiteLabelDocId,
string _BuildingName,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_P
        //20_8_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_CountryCode(
int _WhiteLabelDocId,
string _BuildingName,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_Q
        //20_8_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_Email(
int _WhiteLabelDocId,
string _BuildingName,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_R
        //20_8_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_MainPhone(
int _WhiteLabelDocId,
string _BuildingName,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_S
        //20_8_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_MobliePhone(
int _WhiteLabelDocId,
string _BuildingName,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_T
        //20_8_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_OrdersDocId(
int _WhiteLabelDocId,
string _BuildingName,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_I_V
        //20_8_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingName_Title(
int _WhiteLabelDocId,
string _BuildingName,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingName = _BuildingName; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingName_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingName_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_K
        //20_9_10
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_Street(
int _WhiteLabelDocId,
string _BuildingNumber,
string _Street , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.Street = _Street;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Street(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Street));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_L
        //20_9_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_Locality(
int _WhiteLabelDocId,
string _BuildingNumber,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_M
        //20_9_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_City(
int _WhiteLabelDocId,
string _BuildingNumber,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_N
        //20_9_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_Province(
int _WhiteLabelDocId,
string _BuildingNumber,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_O
        //20_9_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_ZipCode(
int _WhiteLabelDocId,
string _BuildingNumber,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_P
        //20_9_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_CountryCode(
int _WhiteLabelDocId,
string _BuildingNumber,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_Q
        //20_9_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_Email(
int _WhiteLabelDocId,
string _BuildingNumber,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_R
        //20_9_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_MainPhone(
int _WhiteLabelDocId,
string _BuildingNumber,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_S
        //20_9_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_MobliePhone(
int _WhiteLabelDocId,
string _BuildingNumber,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_T
        //20_9_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_OrdersDocId(
int _WhiteLabelDocId,
string _BuildingNumber,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_J_V
        //20_9_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_BuildingNumber_Title(
int _WhiteLabelDocId,
string _BuildingNumber,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.BuildingNumber = _BuildingNumber; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_BuildingNumber_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_BuildingNumber_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_L
        //20_10_11
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_Locality(
int _WhiteLabelDocId,
string _Street,
string _Locality , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.Locality = _Locality;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_Locality(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Locality));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_M
        //20_10_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_City(
int _WhiteLabelDocId,
string _Street,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_N
        //20_10_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_Province(
int _WhiteLabelDocId,
string _Street,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_O
        //20_10_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_ZipCode(
int _WhiteLabelDocId,
string _Street,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_P
        //20_10_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_CountryCode(
int _WhiteLabelDocId,
string _Street,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_Q
        //20_10_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_Email(
int _WhiteLabelDocId,
string _Street,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_R
        //20_10_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_MainPhone(
int _WhiteLabelDocId,
string _Street,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_S
        //20_10_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_MobliePhone(
int _WhiteLabelDocId,
string _Street,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_T
        //20_10_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_OrdersDocId(
int _WhiteLabelDocId,
string _Street,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_K_V
        //20_10_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Street_Title(
int _WhiteLabelDocId,
string _Street,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Street = _Street; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Street_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Street_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_M
        //20_11_12
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_City(
int _WhiteLabelDocId,
string _Locality,
string _City , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.City = _City;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_City(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_City));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_N
        //20_11_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_Province(
int _WhiteLabelDocId,
string _Locality,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_O
        //20_11_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_ZipCode(
int _WhiteLabelDocId,
string _Locality,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_P
        //20_11_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_CountryCode(
int _WhiteLabelDocId,
string _Locality,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_Q
        //20_11_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_Email(
int _WhiteLabelDocId,
string _Locality,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_R
        //20_11_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_MainPhone(
int _WhiteLabelDocId,
string _Locality,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_S
        //20_11_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_MobliePhone(
int _WhiteLabelDocId,
string _Locality,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_T
        //20_11_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_OrdersDocId(
int _WhiteLabelDocId,
string _Locality,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_L_V
        //20_11_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Locality_Title(
int _WhiteLabelDocId,
string _Locality,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Locality = _Locality; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Locality_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Locality_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M_N
        //20_12_13
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City_Province(
int _WhiteLabelDocId,
string _City,
string _Province , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City; 
 oContactDetails.Province = _Province;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City_Province(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Province));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M_O
        //20_12_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City_ZipCode(
int _WhiteLabelDocId,
string _City,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M_P
        //20_12_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City_CountryCode(
int _WhiteLabelDocId,
string _City,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M_Q
        //20_12_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City_Email(
int _WhiteLabelDocId,
string _City,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M_R
        //20_12_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City_MainPhone(
int _WhiteLabelDocId,
string _City,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M_S
        //20_12_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City_MobliePhone(
int _WhiteLabelDocId,
string _City,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M_T
        //20_12_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City_OrdersDocId(
int _WhiteLabelDocId,
string _City,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_M_V
        //20_12_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_City_Title(
int _WhiteLabelDocId,
string _City,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.City = _City; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_City_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_City_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_N_O
        //20_13_14
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Province_ZipCode(
int _WhiteLabelDocId,
string _Province,
int _ZipCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Province = _Province; 
 oContactDetails.ZipCode = _ZipCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Province_ZipCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_ZipCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_N_P
        //20_13_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Province_CountryCode(
int _WhiteLabelDocId,
string _Province,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Province = _Province; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Province_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_N_Q
        //20_13_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Province_Email(
int _WhiteLabelDocId,
string _Province,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Province = _Province; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Province_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_N_R
        //20_13_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Province_MainPhone(
int _WhiteLabelDocId,
string _Province,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Province = _Province; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Province_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_N_S
        //20_13_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Province_MobliePhone(
int _WhiteLabelDocId,
string _Province,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Province = _Province; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Province_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_N_T
        //20_13_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Province_OrdersDocId(
int _WhiteLabelDocId,
string _Province,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Province = _Province; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Province_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_N_V
        //20_13_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Province_Title(
int _WhiteLabelDocId,
string _Province,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Province = _Province; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Province_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Province_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_O_P
        //20_14_15
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_ZipCode_CountryCode(
int _WhiteLabelDocId,
int _ZipCode,
int _CountryCode , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.ZipCode = _ZipCode; 
 oContactDetails.CountryCode = _CountryCode;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_CountryCode(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_O_Q
        //20_14_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_ZipCode_Email(
int _WhiteLabelDocId,
int _ZipCode,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.ZipCode = _ZipCode; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_O_R
        //20_14_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_ZipCode_MainPhone(
int _WhiteLabelDocId,
int _ZipCode,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.ZipCode = _ZipCode; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_O_S
        //20_14_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_ZipCode_MobliePhone(
int _WhiteLabelDocId,
int _ZipCode,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.ZipCode = _ZipCode; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_O_T
        //20_14_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_ZipCode_OrdersDocId(
int _WhiteLabelDocId,
int _ZipCode,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.ZipCode = _ZipCode; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_O_V
        //20_14_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_ZipCode_Title(
int _WhiteLabelDocId,
int _ZipCode,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.ZipCode = _ZipCode; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_ZipCode_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_ZipCode_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_P_Q
        //20_15_16
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_CountryCode_Email(
int _WhiteLabelDocId,
int _CountryCode,
string _Email , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.CountryCode = _CountryCode; 
 oContactDetails.Email = _Email;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_Email(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_Email));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_P_R
        //20_15_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_CountryCode_MainPhone(
int _WhiteLabelDocId,
int _CountryCode,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.CountryCode = _CountryCode; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_P_S
        //20_15_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_CountryCode_MobliePhone(
int _WhiteLabelDocId,
int _CountryCode,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.CountryCode = _CountryCode; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_P_T
        //20_15_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_CountryCode_OrdersDocId(
int _WhiteLabelDocId,
int _CountryCode,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.CountryCode = _CountryCode; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_P_V
        //20_15_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_CountryCode_Title(
int _WhiteLabelDocId,
int _CountryCode,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.CountryCode = _CountryCode; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_CountryCode_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_CountryCode_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_Q_R
        //20_16_17
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Email_MainPhone(
int _WhiteLabelDocId,
string _Email,
string _MainPhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Email = _Email; 
 oContactDetails.MainPhone = _MainPhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Email_MainPhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_MainPhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_Q_S
        //20_16_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Email_MobliePhone(
int _WhiteLabelDocId,
string _Email,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Email = _Email; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Email_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_Q_T
        //20_16_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Email_OrdersDocId(
int _WhiteLabelDocId,
string _Email,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Email = _Email; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Email_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_Q_V
        //20_16_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_Email_Title(
int _WhiteLabelDocId,
string _Email,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.Email = _Email; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_Email_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_Email_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_R_S
        //20_17_18
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_MainPhone_MobliePhone(
int _WhiteLabelDocId,
string _MainPhone,
string _MobliePhone , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.MainPhone = _MainPhone; 
 oContactDetails.MobliePhone = _MobliePhone;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_MainPhone_MobliePhone(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_MobliePhone));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_R_T
        //20_17_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_MainPhone_OrdersDocId(
int _WhiteLabelDocId,
string _MainPhone,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.MainPhone = _MainPhone; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_MainPhone_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_R_V
        //20_17_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_MainPhone_Title(
int _WhiteLabelDocId,
string _MainPhone,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.MainPhone = _MainPhone; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_MainPhone_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MainPhone_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_S_T
        //20_18_19
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_MobliePhone_OrdersDocId(
int _WhiteLabelDocId,
string _MobliePhone,
int _OrdersDocId , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.MobliePhone = _MobliePhone; 
 oContactDetails.OrdersDocId = _OrdersDocId;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_MobliePhone_OrdersDocId(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_S_V
        //20_18_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_MobliePhone_Title(
int _WhiteLabelDocId,
string _MobliePhone,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.MobliePhone = _MobliePhone; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_MobliePhone_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_MobliePhone_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }



        //U_T_V
        //20_19_21
        public static ContactDetailsContainer SelectByKeysView_WhiteLabelDocId_OrdersDocId_Title(
int _WhiteLabelDocId,
int _OrdersDocId,
string _Title , bool? isActive)
        {
            ContactDetailsContainer oContactDetailsContainer = new ContactDetailsContainer();
            ContactDetails oContactDetails = new ContactDetails();
            #region Params
            
 oContactDetails.WhiteLabelDocId = _WhiteLabelDocId; 
 oContactDetails.OrdersDocId = _OrdersDocId; 
 oContactDetails.Title = _Title;
            #endregion 
            oContactDetailsContainer.Add(SelectData(ContactDetails.GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId_Title(oContactDetails), TBNames_ContactDetails.PROC_Select_ContactDetails_By_Keys_View_WhiteLabelDocId_OrdersDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oContactDetailsContainer = oContactDetailsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oContactDetailsContainer;
        }


#endregion
}

    
}
