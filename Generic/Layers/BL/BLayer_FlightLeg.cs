

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class FlightLegContainer  : Container<FlightLegContainer, FlightLeg>{
#region Extra functions

#endregion

        #region Static Method
        
        public static FlightLegContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            oFlightLegContainer.Add(oFlightLegContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oFlightLegContainer;
        }

        
        public static FlightLegContainer SelectAllFlightLegs(int? _WhiteLabelDocId,bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            oFlightLegContainer.Add(oFlightLegContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oFlightLegContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //O_A
        //14_0
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B
        //14_1
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C
        //14_2
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E
        //14_4
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber(
int _WhiteLabelDocId,
string _FlightNumber , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F
        //14_5
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime(
int _WhiteLabelDocId,
DateTime _DepartureDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_G
        //14_6
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_ArrivalDateTime(
int _WhiteLabelDocId,
DateTime _ArrivalDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_H
        //14_7
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Status(
int _WhiteLabelDocId,
string _Status , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Status = _Status;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Status(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_I
        //14_8
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Duration(
int _WhiteLabelDocId,
int _Duration , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Duration = _Duration;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Duration(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_J
        //14_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Hops(
int _WhiteLabelDocId,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_K
        //14_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode(
int _WhiteLabelDocId,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_L
        //14_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_OriginIataCode(
int _WhiteLabelDocId,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_M
        //14_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode(
int _WhiteLabelDocId,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_N
        //14_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_OrderFlightLegDocId(
int _WhiteLabelDocId,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_B
        //14_0_1
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.DocId = _DocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_C
        //14_0_2
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_E
        //14_0_4
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FlightNumber(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FlightNumber , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightNumber(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_F
        //14_0_5
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DepartureDateTime(
int _WhiteLabelDocId,
DateTime _DateCreated,
DateTime _DepartureDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DepartureDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DepartureDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_G
        //14_0_6
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_ArrivalDateTime(
int _WhiteLabelDocId,
DateTime _DateCreated,
DateTime _ArrivalDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ArrivalDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_ArrivalDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_H
        //14_0_7
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Status(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Status , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.Status = _Status;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Status(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Status));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_I
        //14_0_8
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Duration(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Duration , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.Duration = _Duration;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Duration(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Duration));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_J
        //14_0_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Hops(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_K
        //14_0_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_AirlineIataCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_L
        //14_0_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OriginIataCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_M
        //14_0_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DestinationIataCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_A_N
        //14_0_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrderFlightLegDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DateCreated = _DateCreated; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_C
        //14_1_2
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_E
        //14_1_4
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_FlightNumber(
int _WhiteLabelDocId,
int _DocId,
string _FlightNumber , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightNumber(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_F
        //14_1_5
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_DepartureDateTime(
int _WhiteLabelDocId,
int _DocId,
DateTime _DepartureDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DepartureDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_DepartureDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_G
        //14_1_6
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_ArrivalDateTime(
int _WhiteLabelDocId,
int _DocId,
DateTime _ArrivalDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ArrivalDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_ArrivalDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_H
        //14_1_7
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_Status(
int _WhiteLabelDocId,
int _DocId,
string _Status , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.Status = _Status;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Status(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Status));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_I
        //14_1_8
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_Duration(
int _WhiteLabelDocId,
int _DocId,
int _Duration , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.Duration = _Duration;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Duration(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Duration));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_J
        //14_1_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_Hops(
int _WhiteLabelDocId,
int _DocId,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_K
        //14_1_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_AirlineIataCode(
int _WhiteLabelDocId,
int _DocId,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_L
        //14_1_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_OriginIataCode(
int _WhiteLabelDocId,
int _DocId,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_M
        //14_1_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_DestinationIataCode(
int _WhiteLabelDocId,
int _DocId,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_B_N
        //14_1_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_OrderFlightLegDocId(
int _WhiteLabelDocId,
int _DocId,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DocId = _DocId; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_E
        //14_2_4
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FlightNumber(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FlightNumber , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightNumber(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_F
        //14_2_5
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_DepartureDateTime(
int _WhiteLabelDocId,
bool _IsDeleted,
DateTime _DepartureDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DepartureDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_DepartureDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_G
        //14_2_6
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_ArrivalDateTime(
int _WhiteLabelDocId,
bool _IsDeleted,
DateTime _ArrivalDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ArrivalDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_ArrivalDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_H
        //14_2_7
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Status(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Status , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.Status = _Status;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Status(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Status));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_I
        //14_2_8
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Duration(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Duration , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.Duration = _Duration;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Duration(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Duration));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_J
        //14_2_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Hops(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_K
        //14_2_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineIataCode(
int _WhiteLabelDocId,
bool _IsDeleted,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_L
        //14_2_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OriginIataCode(
int _WhiteLabelDocId,
bool _IsDeleted,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_M
        //14_2_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationIataCode(
int _WhiteLabelDocId,
bool _IsDeleted,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_C_N
        //14_2_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrderFlightLegDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.IsDeleted = _IsDeleted; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_F
        //14_4_5
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_DepartureDateTime(
int _WhiteLabelDocId,
string _FlightNumber,
DateTime _DepartureDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_DepartureDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_DepartureDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_G
        //14_4_6
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_ArrivalDateTime(
int _WhiteLabelDocId,
string _FlightNumber,
DateTime _ArrivalDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_ArrivalDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_ArrivalDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_H
        //14_4_7
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_Status(
int _WhiteLabelDocId,
string _FlightNumber,
string _Status , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.Status = _Status;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_Status(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Status));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_I
        //14_4_8
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_Duration(
int _WhiteLabelDocId,
string _FlightNumber,
int _Duration , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.Duration = _Duration;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_Duration(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Duration));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_J
        //14_4_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_Hops(
int _WhiteLabelDocId,
string _FlightNumber,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_K
        //14_4_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_AirlineIataCode(
int _WhiteLabelDocId,
string _FlightNumber,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_L
        //14_4_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_OriginIataCode(
int _WhiteLabelDocId,
string _FlightNumber,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_M
        //14_4_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_DestinationIataCode(
int _WhiteLabelDocId,
string _FlightNumber,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_E_N
        //14_4_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_OrderFlightLegDocId(
int _WhiteLabelDocId,
string _FlightNumber,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.FlightNumber = _FlightNumber; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F_G
        //14_5_6
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime_ArrivalDateTime(
int _WhiteLabelDocId,
DateTime _DepartureDateTime,
DateTime _ArrivalDateTime , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_ArrivalDateTime(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_ArrivalDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F_H
        //14_5_7
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime_Status(
int _WhiteLabelDocId,
DateTime _DepartureDateTime,
string _Status , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime; 
 oFlightLeg.Status = _Status;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_Status(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Status));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F_I
        //14_5_8
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime_Duration(
int _WhiteLabelDocId,
DateTime _DepartureDateTime,
int _Duration , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime; 
 oFlightLeg.Duration = _Duration;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_Duration(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Duration));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F_J
        //14_5_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime_Hops(
int _WhiteLabelDocId,
DateTime _DepartureDateTime,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F_K
        //14_5_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime_AirlineIataCode(
int _WhiteLabelDocId,
DateTime _DepartureDateTime,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F_L
        //14_5_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime_OriginIataCode(
int _WhiteLabelDocId,
DateTime _DepartureDateTime,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F_M
        //14_5_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime_DestinationIataCode(
int _WhiteLabelDocId,
DateTime _DepartureDateTime,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_F_N
        //14_5_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DepartureDateTime_OrderFlightLegDocId(
int _WhiteLabelDocId,
DateTime _DepartureDateTime,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DepartureDateTime = _DepartureDateTime; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_G_H
        //14_6_7
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Status(
int _WhiteLabelDocId,
DateTime _ArrivalDateTime,
string _Status , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime; 
 oFlightLeg.Status = _Status;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Status(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Status));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_G_I
        //14_6_8
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Duration(
int _WhiteLabelDocId,
DateTime _ArrivalDateTime,
int _Duration , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime; 
 oFlightLeg.Duration = _Duration;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Duration(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Duration));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_G_J
        //14_6_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Hops(
int _WhiteLabelDocId,
DateTime _ArrivalDateTime,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_G_K
        //14_6_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_ArrivalDateTime_AirlineIataCode(
int _WhiteLabelDocId,
DateTime _ArrivalDateTime,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_G_L
        //14_6_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_ArrivalDateTime_OriginIataCode(
int _WhiteLabelDocId,
DateTime _ArrivalDateTime,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_G_M
        //14_6_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_ArrivalDateTime_DestinationIataCode(
int _WhiteLabelDocId,
DateTime _ArrivalDateTime,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_G_N
        //14_6_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_ArrivalDateTime_OrderFlightLegDocId(
int _WhiteLabelDocId,
DateTime _ArrivalDateTime,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.ArrivalDateTime = _ArrivalDateTime; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_H_I
        //14_7_8
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Status_Duration(
int _WhiteLabelDocId,
string _Status,
int _Duration , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Status = _Status; 
 oFlightLeg.Duration = _Duration;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_Duration(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_Duration));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_H_J
        //14_7_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Status_Hops(
int _WhiteLabelDocId,
string _Status,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Status = _Status; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_H_K
        //14_7_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Status_AirlineIataCode(
int _WhiteLabelDocId,
string _Status,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Status = _Status; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_H_L
        //14_7_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Status_OriginIataCode(
int _WhiteLabelDocId,
string _Status,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Status = _Status; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_H_M
        //14_7_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Status_DestinationIataCode(
int _WhiteLabelDocId,
string _Status,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Status = _Status; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_H_N
        //14_7_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Status_OrderFlightLegDocId(
int _WhiteLabelDocId,
string _Status,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Status = _Status; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Status_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_I_J
        //14_8_9
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Duration_Hops(
int _WhiteLabelDocId,
int _Duration,
int _Hops , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Duration = _Duration; 
 oFlightLeg.Hops = _Hops;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_Hops(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_Hops));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_I_K
        //14_8_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Duration_AirlineIataCode(
int _WhiteLabelDocId,
int _Duration,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Duration = _Duration; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_I_L
        //14_8_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Duration_OriginIataCode(
int _WhiteLabelDocId,
int _Duration,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Duration = _Duration; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_I_M
        //14_8_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Duration_DestinationIataCode(
int _WhiteLabelDocId,
int _Duration,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Duration = _Duration; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_I_N
        //14_8_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Duration_OrderFlightLegDocId(
int _WhiteLabelDocId,
int _Duration,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Duration = _Duration; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_J_K
        //14_9_10
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Hops_AirlineIataCode(
int _WhiteLabelDocId,
int _Hops,
string _AirlineIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Hops = _Hops; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Hops_AirlineIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_J_L
        //14_9_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Hops_OriginIataCode(
int _WhiteLabelDocId,
int _Hops,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Hops = _Hops; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Hops_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_J_M
        //14_9_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Hops_DestinationIataCode(
int _WhiteLabelDocId,
int _Hops,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Hops = _Hops; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Hops_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_J_N
        //14_9_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_Hops_OrderFlightLegDocId(
int _WhiteLabelDocId,
int _Hops,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.Hops = _Hops; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Hops_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_K_L
        //14_10_11
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_OriginIataCode(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _OriginIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode; 
 oFlightLeg.OriginIataCode = _OriginIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OriginIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OriginIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_K_M
        //14_10_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_DestinationIataCode(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_K_N
        //14_10_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderFlightLegDocId(
int _WhiteLabelDocId,
string _AirlineIataCode,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.AirlineIataCode = _AirlineIataCode; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_L_M
        //14_11_12
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_OriginIataCode_DestinationIataCode(
int _WhiteLabelDocId,
string _OriginIataCode,
string _DestinationIataCode , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.OriginIataCode = _OriginIataCode; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_OriginIataCode_DestinationIataCode(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_L_N
        //14_11_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_OriginIataCode_OrderFlightLegDocId(
int _WhiteLabelDocId,
string _OriginIataCode,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.OriginIataCode = _OriginIataCode; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_OriginIataCode_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }



        //O_M_N
        //14_12_13
        public static FlightLegContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode_OrderFlightLegDocId(
int _WhiteLabelDocId,
string _DestinationIataCode,
int _OrderFlightLegDocId , bool? isActive)
        {
            FlightLegContainer oFlightLegContainer = new FlightLegContainer();
            FlightLeg oFlightLeg = new FlightLeg();
            #region Params
            
 oFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oFlightLeg.DestinationIataCode = _DestinationIataCode; 
 oFlightLeg.OrderFlightLegDocId = _OrderFlightLegDocId;
            #endregion 
            oFlightLegContainer.Add(SelectData(FlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_OrderFlightLegDocId(oFlightLeg), TBNames_FlightLeg.PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DestinationIataCode_OrderFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oFlightLegContainer = oFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oFlightLegContainer;
        }


#endregion
}

    
}
