

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class CountryContainer  : Container<CountryContainer, Country>{
#region Extra functions

#endregion

        #region Static Method
        
        public static CountryContainer SelectByID(int doc_id,bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            oCountryContainer.Add(oCountryContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oCountryContainer;
        }

        
        public static CountryContainer SelectAllCountrys(bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            oCountryContainer.Add(oCountryContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oCountryContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static CountryContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DateCreated = _DateCreated;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DateCreated(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //B
        //1
        public static CountryContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DocId = _DocId;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DocId(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //C
        //2
        public static CountryContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.IsDeleted = _IsDeleted;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_IsDeleted(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //E
        //4
        public static CountryContainer SelectByKeysView_CountryCode(
string _CountryCode , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.CountryCode = _CountryCode;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_CountryCode(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //F
        //5
        public static CountryContainer SelectByKeysView_Name(
string _Name , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.Name = _Name;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_Name(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCountryContainer = oCountryContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCountryContainer;
        }



        //G
        //6
        public static CountryContainer SelectByKeysView_EnglishName(
string _EnglishName , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.EnglishName = _EnglishName;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_EnglishName(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //A_B
        //0_1
        public static CountryContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DateCreated = _DateCreated; 
 oCountry.DocId = _DocId;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DateCreated_DocId(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //A_C
        //0_2
        public static CountryContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DateCreated = _DateCreated; 
 oCountry.IsDeleted = _IsDeleted;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //A_E
        //0_4
        public static CountryContainer SelectByKeysView_DateCreated_CountryCode(
DateTime _DateCreated,
string _CountryCode , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DateCreated = _DateCreated; 
 oCountry.CountryCode = _CountryCode;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DateCreated_CountryCode(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DateCreated_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //A_F
        //0_5
        public static CountryContainer SelectByKeysView_DateCreated_Name(
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DateCreated = _DateCreated; 
 oCountry.Name = _Name;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DateCreated_Name(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCountryContainer = oCountryContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCountryContainer;
        }



        //A_G
        //0_6
        public static CountryContainer SelectByKeysView_DateCreated_EnglishName(
DateTime _DateCreated,
string _EnglishName , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DateCreated = _DateCreated; 
 oCountry.EnglishName = _EnglishName;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DateCreated_EnglishName(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DateCreated_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //B_C
        //1_2
        public static CountryContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DocId = _DocId; 
 oCountry.IsDeleted = _IsDeleted;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DocId_IsDeleted(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //B_E
        //1_4
        public static CountryContainer SelectByKeysView_DocId_CountryCode(
int _DocId,
string _CountryCode , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DocId = _DocId; 
 oCountry.CountryCode = _CountryCode;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DocId_CountryCode(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DocId_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //B_F
        //1_5
        public static CountryContainer SelectByKeysView_DocId_Name(
int _DocId,
string _Name , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DocId = _DocId; 
 oCountry.Name = _Name;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DocId_Name(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCountryContainer = oCountryContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCountryContainer;
        }



        //B_G
        //1_6
        public static CountryContainer SelectByKeysView_DocId_EnglishName(
int _DocId,
string _EnglishName , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.DocId = _DocId; 
 oCountry.EnglishName = _EnglishName;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_DocId_EnglishName(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_DocId_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //C_E
        //2_4
        public static CountryContainer SelectByKeysView_IsDeleted_CountryCode(
bool _IsDeleted,
string _CountryCode , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.IsDeleted = _IsDeleted; 
 oCountry.CountryCode = _CountryCode;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_IsDeleted_CountryCode(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_IsDeleted_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //C_F
        //2_5
        public static CountryContainer SelectByKeysView_IsDeleted_Name(
bool _IsDeleted,
string _Name , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.IsDeleted = _IsDeleted; 
 oCountry.Name = _Name;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_IsDeleted_Name(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCountryContainer = oCountryContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCountryContainer;
        }



        //C_G
        //2_6
        public static CountryContainer SelectByKeysView_IsDeleted_EnglishName(
bool _IsDeleted,
string _EnglishName , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.IsDeleted = _IsDeleted; 
 oCountry.EnglishName = _EnglishName;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_IsDeleted_EnglishName(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_IsDeleted_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //E_F
        //4_5
        public static CountryContainer SelectByKeysView_CountryCode_Name(
string _CountryCode,
string _Name , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.CountryCode = _CountryCode; 
 oCountry.Name = _Name;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_CountryCode_Name(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_CountryCode_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCountryContainer = oCountryContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCountryContainer;
        }



        //E_G
        //4_6
        public static CountryContainer SelectByKeysView_CountryCode_EnglishName(
string _CountryCode,
string _EnglishName , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.CountryCode = _CountryCode; 
 oCountry.EnglishName = _EnglishName;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_CountryCode_EnglishName(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_CountryCode_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCountryContainer;
        }



        //F_G
        //5_6
        public static CountryContainer SelectByKeysView_Name_EnglishName(
string _Name,
string _EnglishName , bool? isActive)
        {
            CountryContainer oCountryContainer = new CountryContainer();
            Country oCountry = new Country();
            #region Params
            
 oCountry.Name = _Name; 
 oCountry.EnglishName = _EnglishName;
            #endregion 
            oCountryContainer.Add(SelectData(Country.GetParamsForSelectByKeysView_Name_EnglishName(oCountry), TBNames_Country.PROC_Select_Country_By_Keys_View_Name_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCountryContainer = oCountryContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCountryContainer = oCountryContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCountryContainer;
        }


#endregion
}

    
}
