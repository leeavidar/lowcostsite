

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class OrderFlightLegContainer  : Container<OrderFlightLegContainer, OrderFlightLeg>{
#region Extra functions

#endregion

        #region Static Method
        
        public static OrderFlightLegContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            oOrderFlightLegContainer.Add(oOrderFlightLegContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oOrderFlightLegContainer;
        }

        
        public static OrderFlightLegContainer SelectAllOrderFlightLegs(int? _WhiteLabelDocId,bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            oOrderFlightLegContainer.Add(oOrderFlightLegContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oOrderFlightLegContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //I_A
        //8_0
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DateCreated = _DateCreated;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_B
        //8_1
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DocId = _DocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_C
        //8_2
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_E
        //8_4
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR(
int _WhiteLabelDocId,
string _LowCostPNR , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.LowCostPNR = _LowCostPNR;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_F
        //8_5
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_PriceNet(
int _WhiteLabelDocId,
Double _PriceNet , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.PriceNet = _PriceNet;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceNet(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_G
        //8_6
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_PriceMarkUp(
int _WhiteLabelDocId,
Double _PriceMarkUp , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_H
        //8_7
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightType(
int _WhiteLabelDocId,
string _FlightType , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.FlightType = _FlightType;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightType(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_J
        //8_9
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_OrdersDocId(
int _WhiteLabelDocId,
int _OrdersDocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_A_B
        //8_0_1
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DateCreated = _DateCreated; 
 oOrderFlightLeg.DocId = _DocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_A_C
        //8_0_2
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DateCreated = _DateCreated; 
 oOrderFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_A_E
        //8_0_4
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LowCostPNR , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DateCreated = _DateCreated; 
 oOrderFlightLeg.LowCostPNR = _LowCostPNR;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_A_F
        //8_0_5
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PriceNet(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _PriceNet , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DateCreated = _DateCreated; 
 oOrderFlightLeg.PriceNet = _PriceNet;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceNet(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PriceNet));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_A_G
        //8_0_6
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PriceMarkUp(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _PriceMarkUp , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DateCreated = _DateCreated; 
 oOrderFlightLeg.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceMarkUp(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_A_H
        //8_0_7
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FlightType(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FlightType , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DateCreated = _DateCreated; 
 oOrderFlightLeg.FlightType = _FlightType;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightType(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_A_J
        //8_0_9
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrdersDocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DateCreated = _DateCreated; 
 oOrderFlightLeg.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_B_C
        //8_1_2
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DocId = _DocId; 
 oOrderFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_B_E
        //8_1_4
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(
int _WhiteLabelDocId,
int _DocId,
string _LowCostPNR , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DocId = _DocId; 
 oOrderFlightLeg.LowCostPNR = _LowCostPNR;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_B_F
        //8_1_5
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_PriceNet(
int _WhiteLabelDocId,
int _DocId,
Double _PriceNet , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DocId = _DocId; 
 oOrderFlightLeg.PriceNet = _PriceNet;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceNet(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PriceNet));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_B_G
        //8_1_6
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_PriceMarkUp(
int _WhiteLabelDocId,
int _DocId,
Double _PriceMarkUp , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DocId = _DocId; 
 oOrderFlightLeg.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceMarkUp(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_B_H
        //8_1_7
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_FlightType(
int _WhiteLabelDocId,
int _DocId,
string _FlightType , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DocId = _DocId; 
 oOrderFlightLeg.FlightType = _FlightType;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightType(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_B_J
        //8_1_9
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(
int _WhiteLabelDocId,
int _DocId,
int _OrdersDocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.DocId = _DocId; 
 oOrderFlightLeg.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_C_E
        //8_2_4
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LowCostPNR , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.IsDeleted = _IsDeleted; 
 oOrderFlightLeg.LowCostPNR = _LowCostPNR;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_C_F
        //8_2_5
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PriceNet(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _PriceNet , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.IsDeleted = _IsDeleted; 
 oOrderFlightLeg.PriceNet = _PriceNet;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceNet(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceNet));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_C_G
        //8_2_6
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PriceMarkUp(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _PriceMarkUp , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.IsDeleted = _IsDeleted; 
 oOrderFlightLeg.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceMarkUp(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_C_H
        //8_2_7
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FlightType(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FlightType , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.IsDeleted = _IsDeleted; 
 oOrderFlightLeg.FlightType = _FlightType;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightType(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_C_J
        //8_2_9
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrdersDocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.IsDeleted = _IsDeleted; 
 oOrderFlightLeg.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_E_F
        //8_4_5
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_PriceNet(
int _WhiteLabelDocId,
string _LowCostPNR,
Double _PriceNet , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.LowCostPNR = _LowCostPNR; 
 oOrderFlightLeg.PriceNet = _PriceNet;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PriceNet(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_PriceNet));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_E_G
        //8_4_6
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_PriceMarkUp(
int _WhiteLabelDocId,
string _LowCostPNR,
Double _PriceMarkUp , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.LowCostPNR = _LowCostPNR; 
 oOrderFlightLeg.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PriceMarkUp(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_E_H
        //8_4_7
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_FlightType(
int _WhiteLabelDocId,
string _LowCostPNR,
string _FlightType , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.LowCostPNR = _LowCostPNR; 
 oOrderFlightLeg.FlightType = _FlightType;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_FlightType(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_E_J
        //8_4_9
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_OrdersDocId(
int _WhiteLabelDocId,
string _LowCostPNR,
int _OrdersDocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.LowCostPNR = _LowCostPNR; 
 oOrderFlightLeg.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrdersDocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_F_G
        //8_5_6
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_PriceNet_PriceMarkUp(
int _WhiteLabelDocId,
Double _PriceNet,
Double _PriceMarkUp , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.PriceNet = _PriceNet; 
 oOrderFlightLeg.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceNet_PriceMarkUp(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_F_H
        //8_5_7
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_PriceNet_FlightType(
int _WhiteLabelDocId,
Double _PriceNet,
string _FlightType , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.PriceNet = _PriceNet; 
 oOrderFlightLeg.FlightType = _FlightType;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceNet_FlightType(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_F_J
        //8_5_9
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_PriceNet_OrdersDocId(
int _WhiteLabelDocId,
Double _PriceNet,
int _OrdersDocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.PriceNet = _PriceNet; 
 oOrderFlightLeg.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceNet_OrdersDocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_G_H
        //8_6_7
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_PriceMarkUp_FlightType(
int _WhiteLabelDocId,
Double _PriceMarkUp,
string _FlightType , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.PriceMarkUp = _PriceMarkUp; 
 oOrderFlightLeg.FlightType = _FlightType;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp_FlightType(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp_FlightType));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_G_J
        //8_6_9
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_PriceMarkUp_OrdersDocId(
int _WhiteLabelDocId,
Double _PriceMarkUp,
int _OrdersDocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.PriceMarkUp = _PriceMarkUp; 
 oOrderFlightLeg.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp_OrdersDocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }



        //I_H_J
        //8_7_9
        public static OrderFlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightType_OrdersDocId(
int _WhiteLabelDocId,
string _FlightType,
int _OrdersDocId , bool? isActive)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
            #region Params
            
 oOrderFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrderFlightLeg.FlightType = _FlightType; 
 oOrderFlightLeg.OrdersDocId = _OrdersDocId;
            #endregion 
            oOrderFlightLegContainer.Add(SelectData(OrderFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightType_OrdersDocId(oOrderFlightLeg), TBNames_OrderFlightLeg.PROC_Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_FlightType_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrderFlightLegContainer = oOrderFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrderFlightLegContainer;
        }


#endregion
}

    
}
