

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class PopularDestinationContainer  : Container<PopularDestinationContainer, PopularDestination>{
#region Extra functions

#endregion

        #region Static Method
        
        public static PopularDestinationContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            oPopularDestinationContainer.Add(oPopularDestinationContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oPopularDestinationContainer;
        }

        
        public static PopularDestinationContainer SelectAllPopularDestinations(int? _WhiteLabelDocId,bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            oPopularDestinationContainer.Add(oPopularDestinationContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oPopularDestinationContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //H_A
        //7_0
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DateCreated = _DateCreated;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_B
        //7_1
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DocId = _DocId;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_C
        //7_2
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.IsDeleted = _IsDeleted;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_E
        //7_4
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_IataCode(
int _WhiteLabelDocId,
string _IataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.IataCode = _IataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_IataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_F
        //7_5
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_CityIataCode(
int _WhiteLabelDocId,
string _CityIataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.CityIataCode = _CityIataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_CityIataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_G
        //7_6
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_Name(
int _WhiteLabelDocId,
string _Name , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.Name = _Name;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_Name(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oPopularDestinationContainer;
        }



        //H_A_B
        //7_0_1
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DateCreated = _DateCreated; 
 oPopularDestination.DocId = _DocId;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_A_C
        //7_0_2
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DateCreated = _DateCreated; 
 oPopularDestination.IsDeleted = _IsDeleted;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_A_E
        //7_0_4
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IataCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _IataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DateCreated = _DateCreated; 
 oPopularDestination.IataCode = _IataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_A_F
        //7_0_5
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DateCreated_CityIataCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _CityIataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DateCreated = _DateCreated; 
 oPopularDestination.CityIataCode = _CityIataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CityIataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_A_G
        //7_0_6
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Name(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DateCreated = _DateCreated; 
 oPopularDestination.Name = _Name;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oPopularDestinationContainer;
        }



        //H_B_C
        //7_1_2
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DocId = _DocId; 
 oPopularDestination.IsDeleted = _IsDeleted;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_B_E
        //7_1_4
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DocId_IataCode(
int _WhiteLabelDocId,
int _DocId,
string _IataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DocId = _DocId; 
 oPopularDestination.IataCode = _IataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_B_F
        //7_1_5
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DocId_CityIataCode(
int _WhiteLabelDocId,
int _DocId,
string _CityIataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DocId = _DocId; 
 oPopularDestination.CityIataCode = _CityIataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CityIataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_B_G
        //7_1_6
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_DocId_Name(
int _WhiteLabelDocId,
int _DocId,
string _Name , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.DocId = _DocId; 
 oPopularDestination.Name = _Name;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oPopularDestinationContainer;
        }



        //H_C_E
        //7_2_4
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_IataCode(
int _WhiteLabelDocId,
bool _IsDeleted,
string _IataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.IsDeleted = _IsDeleted; 
 oPopularDestination.IataCode = _IataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_IataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_C_F
        //7_2_5
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_CityIataCode(
int _WhiteLabelDocId,
bool _IsDeleted,
string _CityIataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.IsDeleted = _IsDeleted; 
 oPopularDestination.CityIataCode = _CityIataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CityIataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_C_G
        //7_2_6
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Name(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Name , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.IsDeleted = _IsDeleted; 
 oPopularDestination.Name = _Name;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oPopularDestinationContainer;
        }



        //H_E_F
        //7_4_5
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_IataCode_CityIataCode(
int _WhiteLabelDocId,
string _IataCode,
string _CityIataCode , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.IataCode = _IataCode; 
 oPopularDestination.CityIataCode = _CityIataCode;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_IataCode_CityIataCode(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode_CityIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPopularDestinationContainer;
        }



        //H_E_G
        //7_4_6
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_IataCode_Name(
int _WhiteLabelDocId,
string _IataCode,
string _Name , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.IataCode = _IataCode; 
 oPopularDestination.Name = _Name;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_IataCode_Name(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode_Name));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oPopularDestinationContainer;
        }



        //H_F_G
        //7_5_6
        public static PopularDestinationContainer SelectByKeysView_WhiteLabelDocId_CityIataCode_Name(
int _WhiteLabelDocId,
string _CityIataCode,
string _Name , bool? isActive)
        {
            PopularDestinationContainer oPopularDestinationContainer = new PopularDestinationContainer();
            PopularDestination oPopularDestination = new PopularDestination();
            #region Params
            
 oPopularDestination.WhiteLabelDocId = _WhiteLabelDocId; 
 oPopularDestination.CityIataCode = _CityIataCode; 
 oPopularDestination.Name = _Name;
            #endregion 
            oPopularDestinationContainer.Add(SelectData(PopularDestination.GetParamsForSelectByKeysView_WhiteLabelDocId_CityIataCode_Name(oPopularDestination), TBNames_PopularDestination.PROC_Select_PopularDestination_By_Keys_View_WhiteLabelDocId_CityIataCode_Name));
            #region ExtraFilters
            
if(isActive != null){
                oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oPopularDestinationContainer = oPopularDestinationContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oPopularDestinationContainer;
        }


#endregion
}

    
}
