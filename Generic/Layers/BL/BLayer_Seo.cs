

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class SeoContainer  : Container<SeoContainer, Seo>{
#region Extra functions

#endregion

        #region Static Method
        
        public static SeoContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            oSeoContainer.Add(oSeoContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oSeoContainer;
        }

        
        public static SeoContainer SelectAllSeos(int? _WhiteLabelDocId,bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            oSeoContainer.Add(oSeoContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oSeoContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //J_A
        //9_0
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DateCreated = _DateCreated;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_B
        //9_1
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DocId = _DocId;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_C
        //9_2
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.IsDeleted = _IsDeleted;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_E
        //9_4
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl(
int _WhiteLabelDocId,
string _FriendlyUrl , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.FriendlyUrl = _FriendlyUrl;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_F
        //9_5
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoTitle(
int _WhiteLabelDocId,
string _SeoTitle , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoTitle = _SeoTitle;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoTitle(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoTitle, _SeoTitle));
            #endregion
            return oSeoContainer;
        }



        //J_G
        //9_6
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoDescription(
int _WhiteLabelDocId,
string _SeoDescription , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoDescription = _SeoDescription;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDescription(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoDescription));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoDescription, _SeoDescription));
            #endregion
            return oSeoContainer;
        }



        //J_H
        //9_7
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoKeyWords(
int _WhiteLabelDocId,
string _SeoKeyWords , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoKeyWords = _SeoKeyWords;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoKeyWords(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoKeyWords));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoKeyWords, _SeoKeyWords));
            #endregion
            return oSeoContainer;
        }



        //J_I
        //9_8
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_RelatedObject(
int _WhiteLabelDocId,
int _RelatedObject , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.RelatedObject = _RelatedObject;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObject(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_RelatedObject));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_A_B
        //9_0_1
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DateCreated = _DateCreated; 
 oSeo.DocId = _DocId;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_A_C
        //9_0_2
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DateCreated = _DateCreated; 
 oSeo.IsDeleted = _IsDeleted;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_A_E
        //9_0_4
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FriendlyUrl(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FriendlyUrl , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DateCreated = _DateCreated; 
 oSeo.FriendlyUrl = _FriendlyUrl;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FriendlyUrl(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_FriendlyUrl));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_A_F
        //9_0_5
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SeoTitle(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _SeoTitle , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DateCreated = _DateCreated; 
 oSeo.SeoTitle = _SeoTitle;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoTitle(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_SeoTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoTitle, _SeoTitle));
            #endregion
            return oSeoContainer;
        }



        //J_A_G
        //9_0_6
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SeoDescription(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _SeoDescription , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DateCreated = _DateCreated; 
 oSeo.SeoDescription = _SeoDescription;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDescription(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDescription));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoDescription, _SeoDescription));
            #endregion
            return oSeoContainer;
        }



        //J_A_H
        //9_0_7
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SeoKeyWords(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _SeoKeyWords , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DateCreated = _DateCreated; 
 oSeo.SeoKeyWords = _SeoKeyWords;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoKeyWords(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_SeoKeyWords));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoKeyWords, _SeoKeyWords));
            #endregion
            return oSeoContainer;
        }



        //J_A_I
        //9_0_8
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObject(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _RelatedObject , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DateCreated = _DateCreated; 
 oSeo.RelatedObject = _RelatedObject;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObject(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DateCreated_RelatedObject));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_B_C
        //9_1_2
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DocId = _DocId; 
 oSeo.IsDeleted = _IsDeleted;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_B_E
        //9_1_4
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DocId_FriendlyUrl(
int _WhiteLabelDocId,
int _DocId,
string _FriendlyUrl , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DocId = _DocId; 
 oSeo.FriendlyUrl = _FriendlyUrl;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FriendlyUrl(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_FriendlyUrl));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_B_F
        //9_1_5
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DocId_SeoTitle(
int _WhiteLabelDocId,
int _DocId,
string _SeoTitle , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DocId = _DocId; 
 oSeo.SeoTitle = _SeoTitle;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoTitle(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_SeoTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoTitle, _SeoTitle));
            #endregion
            return oSeoContainer;
        }



        //J_B_G
        //9_1_6
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DocId_SeoDescription(
int _WhiteLabelDocId,
int _DocId,
string _SeoDescription , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DocId = _DocId; 
 oSeo.SeoDescription = _SeoDescription;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDescription(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_SeoDescription));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoDescription, _SeoDescription));
            #endregion
            return oSeoContainer;
        }



        //J_B_H
        //9_1_7
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DocId_SeoKeyWords(
int _WhiteLabelDocId,
int _DocId,
string _SeoKeyWords , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DocId = _DocId; 
 oSeo.SeoKeyWords = _SeoKeyWords;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoKeyWords(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_SeoKeyWords));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoKeyWords, _SeoKeyWords));
            #endregion
            return oSeoContainer;
        }



        //J_B_I
        //9_1_8
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_DocId_RelatedObject(
int _WhiteLabelDocId,
int _DocId,
int _RelatedObject , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.DocId = _DocId; 
 oSeo.RelatedObject = _RelatedObject;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_RelatedObject(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_DocId_RelatedObject));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_C_E
        //9_2_4
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FriendlyUrl(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FriendlyUrl , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.IsDeleted = _IsDeleted; 
 oSeo.FriendlyUrl = _FriendlyUrl;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FriendlyUrl(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_FriendlyUrl));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_C_F
        //9_2_5
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SeoTitle(
int _WhiteLabelDocId,
bool _IsDeleted,
string _SeoTitle , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.IsDeleted = _IsDeleted; 
 oSeo.SeoTitle = _SeoTitle;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoTitle(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoTitle, _SeoTitle));
            #endregion
            return oSeoContainer;
        }



        //J_C_G
        //9_2_6
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDescription(
int _WhiteLabelDocId,
bool _IsDeleted,
string _SeoDescription , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.IsDeleted = _IsDeleted; 
 oSeo.SeoDescription = _SeoDescription;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDescription(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDescription));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoDescription, _SeoDescription));
            #endregion
            return oSeoContainer;
        }



        //J_C_H
        //9_2_7
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SeoKeyWords(
int _WhiteLabelDocId,
bool _IsDeleted,
string _SeoKeyWords , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.IsDeleted = _IsDeleted; 
 oSeo.SeoKeyWords = _SeoKeyWords;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoKeyWords(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoKeyWords));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoKeyWords, _SeoKeyWords));
            #endregion
            return oSeoContainer;
        }



        //J_C_I
        //9_2_8
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObject(
int _WhiteLabelDocId,
bool _IsDeleted,
int _RelatedObject , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.IsDeleted = _IsDeleted; 
 oSeo.RelatedObject = _RelatedObject;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObject(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_IsDeleted_RelatedObject));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_E_F
        //9_4_5
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoTitle(
int _WhiteLabelDocId,
string _FriendlyUrl,
string _SeoTitle , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.FriendlyUrl = _FriendlyUrl; 
 oSeo.SeoTitle = _SeoTitle;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoTitle(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoTitle, _SeoTitle));
            #endregion
            return oSeoContainer;
        }



        //J_E_G
        //9_4_6
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoDescription(
int _WhiteLabelDocId,
string _FriendlyUrl,
string _SeoDescription , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.FriendlyUrl = _FriendlyUrl; 
 oSeo.SeoDescription = _SeoDescription;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoDescription(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoDescription));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoDescription, _SeoDescription));
            #endregion
            return oSeoContainer;
        }



        //J_E_H
        //9_4_7
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoKeyWords(
int _WhiteLabelDocId,
string _FriendlyUrl,
string _SeoKeyWords , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.FriendlyUrl = _FriendlyUrl; 
 oSeo.SeoKeyWords = _SeoKeyWords;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoKeyWords(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoKeyWords));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoKeyWords, _SeoKeyWords));
            #endregion
            return oSeoContainer;
        }



        //J_E_I
        //9_4_8
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl_RelatedObject(
int _WhiteLabelDocId,
string _FriendlyUrl,
int _RelatedObject , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.FriendlyUrl = _FriendlyUrl; 
 oSeo.RelatedObject = _RelatedObject;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_RelatedObject(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_FriendlyUrl_RelatedObject));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeoContainer;
        }



        //J_F_G
        //9_5_6
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoTitle_SeoDescription(
int _WhiteLabelDocId,
string _SeoTitle,
string _SeoDescription , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoTitle = _SeoTitle; 
 oSeo.SeoDescription = _SeoDescription;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoTitle_SeoDescription(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoTitle_SeoDescription));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoTitle, _SeoTitle) && string.Equals(R.SeoDescription, _SeoDescription));
            #endregion
            return oSeoContainer;
        }



        //J_F_H
        //9_5_7
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoTitle_SeoKeyWords(
int _WhiteLabelDocId,
string _SeoTitle,
string _SeoKeyWords , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoTitle = _SeoTitle; 
 oSeo.SeoKeyWords = _SeoKeyWords;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoTitle_SeoKeyWords(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoTitle_SeoKeyWords));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoTitle, _SeoTitle) && string.Equals(R.SeoKeyWords, _SeoKeyWords));
            #endregion
            return oSeoContainer;
        }



        //J_F_I
        //9_5_8
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoTitle_RelatedObject(
int _WhiteLabelDocId,
string _SeoTitle,
int _RelatedObject , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoTitle = _SeoTitle; 
 oSeo.RelatedObject = _RelatedObject;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoTitle_RelatedObject(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoTitle_RelatedObject));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoTitle, _SeoTitle));
            #endregion
            return oSeoContainer;
        }



        //J_G_H
        //9_6_7
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoDescription_SeoKeyWords(
int _WhiteLabelDocId,
string _SeoDescription,
string _SeoKeyWords , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoDescription = _SeoDescription; 
 oSeo.SeoKeyWords = _SeoKeyWords;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDescription_SeoKeyWords(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoDescription_SeoKeyWords));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoDescription, _SeoDescription) && string.Equals(R.SeoKeyWords, _SeoKeyWords));
            #endregion
            return oSeoContainer;
        }



        //J_G_I
        //9_6_8
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoDescription_RelatedObject(
int _WhiteLabelDocId,
string _SeoDescription,
int _RelatedObject , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoDescription = _SeoDescription; 
 oSeo.RelatedObject = _RelatedObject;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDescription_RelatedObject(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoDescription_RelatedObject));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoDescription, _SeoDescription));
            #endregion
            return oSeoContainer;
        }



        //J_H_I
        //9_7_8
        public static SeoContainer SelectByKeysView_WhiteLabelDocId_SeoKeyWords_RelatedObject(
int _WhiteLabelDocId,
string _SeoKeyWords,
int _RelatedObject , bool? isActive)
        {
            SeoContainer oSeoContainer = new SeoContainer();
            Seo oSeo = new Seo();
            #region Params
            
 oSeo.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeo.SeoKeyWords = _SeoKeyWords; 
 oSeo.RelatedObject = _RelatedObject;
            #endregion 
            oSeoContainer.Add(SelectData(Seo.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoKeyWords_RelatedObject(oSeo), TBNames_Seo.PROC_Select_Seo_By_Keys_View_WhiteLabelDocId_SeoKeyWords_RelatedObject));
            #region ExtraFilters
            
if(isActive != null){
                oSeoContainer = oSeoContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSeoContainer = oSeoContainer.FindAllContainer(R => string.Equals(R.SeoKeyWords, _SeoKeyWords));
            #endregion
            return oSeoContainer;
        }


#endregion
}

    
}
