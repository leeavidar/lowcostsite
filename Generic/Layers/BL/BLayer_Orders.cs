

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class OrdersContainer  : Container<OrdersContainer, Orders>{
#region Extra functions

#endregion

        #region Static Method
        
        public static OrdersContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            oOrdersContainer.Add(oOrdersContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oOrdersContainer;
        }

        
        public static OrdersContainer SelectAllOrderss(int? _WhiteLabelDocId,bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            oOrdersContainer.Add(oOrdersContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oOrdersContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //M_A
        //12_0
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B
        //12_1
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C
        //12_2
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E
        //12_4
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR(
int _WhiteLabelDocId,
string _LowCostPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F
        //12_5
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR(
int _WhiteLabelDocId,
string _SupplierPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G
        //12_6
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice(
int _WhiteLabelDocId,
Double _TotalPrice , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H
        //12_7
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook(
int _WhiteLabelDocId,
DateTime _DateOfBook , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I
        //12_8
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail(
int _WhiteLabelDocId,
bool _ReceiveEmail , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J
        //12_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms(
int _WhiteLabelDocId,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K
        //12_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin(
int _WhiteLabelDocId,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L
        //12_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId(
int _WhiteLabelDocId,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N
        //12_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode(
int _WhiteLabelDocId,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O
        //12_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus(
int _WhiteLabelDocId,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P
        //12_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks(
int _WhiteLabelDocId,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q
        //12_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment(
int _WhiteLabelDocId,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R
        //12_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken(
int _WhiteLabelDocId,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_S
        //12_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentTransaction(
int _WhiteLabelDocId,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_T
        //12_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookRequest(
int _WhiteLabelDocId,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_U
        //12_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookResponse(
int _WhiteLabelDocId,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_V
        //12_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderRemarks(
int _WhiteLabelDocId,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_W
        //12_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderPayment(
int _WhiteLabelDocId,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_X
        //12_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentToken(
int _WhiteLabelDocId,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Y
        //12_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentRemarks(
int _WhiteLabelDocId,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Z
        //12_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_Currency(
int _WhiteLabelDocId,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_B
        //12_0_1
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.DocId = _DocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_C
        //12_0_2
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.IsDeleted = _IsDeleted;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_E
        //12_0_4
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LowCostPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.LowCostPNR = _LowCostPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_F
        //12_0_5
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SupplierPNR(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _SupplierPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.SupplierPNR = _SupplierPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SupplierPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_SupplierPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_G
        //12_0_6
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TotalPrice(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _TotalPrice , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.TotalPrice = _TotalPrice;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TotalPrice(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_TotalPrice));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_H
        //12_0_7
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DateOfBook(
int _WhiteLabelDocId,
DateTime _DateCreated,
DateTime _DateOfBook , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.DateOfBook = _DateOfBook;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DateOfBook(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_DateOfBook));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_I
        //12_0_8
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_ReceiveEmail(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _ReceiveEmail , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.ReceiveEmail = _ReceiveEmail;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ReceiveEmail(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ReceiveEmail));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_J
        //12_0_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_ReceiveSms(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_K
        //12_0_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_MarketingCabin(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_L
        //12_0_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_ContactDetailsDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_N
        //12_0_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_AirlineIataCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_O
        //12_0_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrderStatus(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_P
        //12_0_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_AdminRemarks(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_Q
        //12_0_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_StatusPayment(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_R
        //12_0_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrderToken(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_S
        //12_0_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PaymentTransaction(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_T
        //12_0_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_XmlBookRequest(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_U
        //12_0_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_XmlBookResponse(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_V
        //12_0_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrderRemarks(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_W
        //12_0_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrderPayment(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_X
        //12_0_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PaymentToken(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_Y
        //12_0_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PaymentRemarks(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_A_Z
        //12_0_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Currency(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateCreated = _DateCreated; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_C
        //12_1_2
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.IsDeleted = _IsDeleted;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_E
        //12_1_4
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(
int _WhiteLabelDocId,
int _DocId,
string _LowCostPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.LowCostPNR = _LowCostPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_F
        //12_1_5
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_SupplierPNR(
int _WhiteLabelDocId,
int _DocId,
string _SupplierPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.SupplierPNR = _SupplierPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SupplierPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_SupplierPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_G
        //12_1_6
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_TotalPrice(
int _WhiteLabelDocId,
int _DocId,
Double _TotalPrice , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.TotalPrice = _TotalPrice;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TotalPrice(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_TotalPrice));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_H
        //12_1_7
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_DateOfBook(
int _WhiteLabelDocId,
int _DocId,
DateTime _DateOfBook , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.DateOfBook = _DateOfBook;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DateOfBook(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_DateOfBook));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_I
        //12_1_8
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_ReceiveEmail(
int _WhiteLabelDocId,
int _DocId,
bool _ReceiveEmail , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.ReceiveEmail = _ReceiveEmail;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ReceiveEmail(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ReceiveEmail));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_J
        //12_1_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_ReceiveSms(
int _WhiteLabelDocId,
int _DocId,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_K
        //12_1_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_MarketingCabin(
int _WhiteLabelDocId,
int _DocId,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_L
        //12_1_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_ContactDetailsDocId(
int _WhiteLabelDocId,
int _DocId,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_N
        //12_1_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_AirlineIataCode(
int _WhiteLabelDocId,
int _DocId,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_O
        //12_1_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_OrderStatus(
int _WhiteLabelDocId,
int _DocId,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_P
        //12_1_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_AdminRemarks(
int _WhiteLabelDocId,
int _DocId,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_Q
        //12_1_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_StatusPayment(
int _WhiteLabelDocId,
int _DocId,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_R
        //12_1_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_OrderToken(
int _WhiteLabelDocId,
int _DocId,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_S
        //12_1_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_PaymentTransaction(
int _WhiteLabelDocId,
int _DocId,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_T
        //12_1_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_XmlBookRequest(
int _WhiteLabelDocId,
int _DocId,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_U
        //12_1_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_XmlBookResponse(
int _WhiteLabelDocId,
int _DocId,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_V
        //12_1_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_OrderRemarks(
int _WhiteLabelDocId,
int _DocId,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_W
        //12_1_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_OrderPayment(
int _WhiteLabelDocId,
int _DocId,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_X
        //12_1_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_PaymentToken(
int _WhiteLabelDocId,
int _DocId,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_Y
        //12_1_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_PaymentRemarks(
int _WhiteLabelDocId,
int _DocId,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_B_Z
        //12_1_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DocId_Currency(
int _WhiteLabelDocId,
int _DocId,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DocId = _DocId; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_E
        //12_2_4
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LowCostPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.LowCostPNR = _LowCostPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_F
        //12_2_5
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SupplierPNR(
int _WhiteLabelDocId,
bool _IsDeleted,
string _SupplierPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.SupplierPNR = _SupplierPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SupplierPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_SupplierPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_G
        //12_2_6
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TotalPrice(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _TotalPrice , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.TotalPrice = _TotalPrice;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TotalPrice(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_TotalPrice));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_H
        //12_2_7
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_DateOfBook(
int _WhiteLabelDocId,
bool _IsDeleted,
DateTime _DateOfBook , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.DateOfBook = _DateOfBook;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DateOfBook(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_DateOfBook));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_I
        //12_2_8
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_ReceiveEmail(
int _WhiteLabelDocId,
bool _IsDeleted,
bool _ReceiveEmail , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.ReceiveEmail = _ReceiveEmail;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ReceiveEmail(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ReceiveEmail));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_J
        //12_2_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_ReceiveSms(
int _WhiteLabelDocId,
bool _IsDeleted,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_K
        //12_2_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_MarketingCabin(
int _WhiteLabelDocId,
bool _IsDeleted,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_L
        //12_2_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_ContactDetailsDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_N
        //12_2_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineIataCode(
int _WhiteLabelDocId,
bool _IsDeleted,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_O
        //12_2_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrderStatus(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_P
        //12_2_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_AdminRemarks(
int _WhiteLabelDocId,
bool _IsDeleted,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_Q
        //12_2_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_StatusPayment(
int _WhiteLabelDocId,
bool _IsDeleted,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_R
        //12_2_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrderToken(
int _WhiteLabelDocId,
bool _IsDeleted,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_S
        //12_2_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentTransaction(
int _WhiteLabelDocId,
bool _IsDeleted,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_T
        //12_2_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_XmlBookRequest(
int _WhiteLabelDocId,
bool _IsDeleted,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_U
        //12_2_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_XmlBookResponse(
int _WhiteLabelDocId,
bool _IsDeleted,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_V
        //12_2_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrderRemarks(
int _WhiteLabelDocId,
bool _IsDeleted,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_W
        //12_2_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrderPayment(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_X
        //12_2_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentToken(
int _WhiteLabelDocId,
bool _IsDeleted,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_Y
        //12_2_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentRemarks(
int _WhiteLabelDocId,
bool _IsDeleted,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_C_Z
        //12_2_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Currency(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.IsDeleted = _IsDeleted; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_F
        //12_4_5
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_SupplierPNR(
int _WhiteLabelDocId,
string _LowCostPNR,
string _SupplierPNR , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.SupplierPNR = _SupplierPNR;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_SupplierPNR(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_SupplierPNR));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_G
        //12_4_6
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_TotalPrice(
int _WhiteLabelDocId,
string _LowCostPNR,
Double _TotalPrice , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.TotalPrice = _TotalPrice;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_TotalPrice(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_TotalPrice));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_H
        //12_4_7
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_DateOfBook(
int _WhiteLabelDocId,
string _LowCostPNR,
DateTime _DateOfBook , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.DateOfBook = _DateOfBook;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_DateOfBook(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_DateOfBook));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_I
        //12_4_8
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_ReceiveEmail(
int _WhiteLabelDocId,
string _LowCostPNR,
bool _ReceiveEmail , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.ReceiveEmail = _ReceiveEmail;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_ReceiveEmail(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ReceiveEmail));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_J
        //12_4_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_ReceiveSms(
int _WhiteLabelDocId,
string _LowCostPNR,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_K
        //12_4_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_MarketingCabin(
int _WhiteLabelDocId,
string _LowCostPNR,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_L
        //12_4_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_ContactDetailsDocId(
int _WhiteLabelDocId,
string _LowCostPNR,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_N
        //12_4_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_AirlineIataCode(
int _WhiteLabelDocId,
string _LowCostPNR,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_O
        //12_4_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderStatus(
int _WhiteLabelDocId,
string _LowCostPNR,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_P
        //12_4_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_AdminRemarks(
int _WhiteLabelDocId,
string _LowCostPNR,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_Q
        //12_4_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_StatusPayment(
int _WhiteLabelDocId,
string _LowCostPNR,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_R
        //12_4_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderToken(
int _WhiteLabelDocId,
string _LowCostPNR,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_S
        //12_4_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentTransaction(
int _WhiteLabelDocId,
string _LowCostPNR,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_T
        //12_4_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_XmlBookRequest(
int _WhiteLabelDocId,
string _LowCostPNR,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_U
        //12_4_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_XmlBookResponse(
int _WhiteLabelDocId,
string _LowCostPNR,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_V
        //12_4_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderRemarks(
int _WhiteLabelDocId,
string _LowCostPNR,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_W
        //12_4_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderPayment(
int _WhiteLabelDocId,
string _LowCostPNR,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_X
        //12_4_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentToken(
int _WhiteLabelDocId,
string _LowCostPNR,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_Y
        //12_4_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentRemarks(
int _WhiteLabelDocId,
string _LowCostPNR,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_E_Z
        //12_4_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_Currency(
int _WhiteLabelDocId,
string _LowCostPNR,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.LowCostPNR = _LowCostPNR; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_G
        //12_5_6
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_TotalPrice(
int _WhiteLabelDocId,
string _SupplierPNR,
Double _TotalPrice , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.TotalPrice = _TotalPrice;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_TotalPrice(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_TotalPrice));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_H
        //12_5_7
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_DateOfBook(
int _WhiteLabelDocId,
string _SupplierPNR,
DateTime _DateOfBook , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.DateOfBook = _DateOfBook;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_DateOfBook(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_DateOfBook));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_I
        //12_5_8
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_ReceiveEmail(
int _WhiteLabelDocId,
string _SupplierPNR,
bool _ReceiveEmail , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.ReceiveEmail = _ReceiveEmail;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_ReceiveEmail(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ReceiveEmail));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_J
        //12_5_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_ReceiveSms(
int _WhiteLabelDocId,
string _SupplierPNR,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_K
        //12_5_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_MarketingCabin(
int _WhiteLabelDocId,
string _SupplierPNR,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_L
        //12_5_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_ContactDetailsDocId(
int _WhiteLabelDocId,
string _SupplierPNR,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_N
        //12_5_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_AirlineIataCode(
int _WhiteLabelDocId,
string _SupplierPNR,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_O
        //12_5_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderStatus(
int _WhiteLabelDocId,
string _SupplierPNR,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_P
        //12_5_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_AdminRemarks(
int _WhiteLabelDocId,
string _SupplierPNR,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_Q
        //12_5_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_StatusPayment(
int _WhiteLabelDocId,
string _SupplierPNR,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_R
        //12_5_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderToken(
int _WhiteLabelDocId,
string _SupplierPNR,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_S
        //12_5_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentTransaction(
int _WhiteLabelDocId,
string _SupplierPNR,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_T
        //12_5_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_XmlBookRequest(
int _WhiteLabelDocId,
string _SupplierPNR,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_U
        //12_5_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_XmlBookResponse(
int _WhiteLabelDocId,
string _SupplierPNR,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_V
        //12_5_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderRemarks(
int _WhiteLabelDocId,
string _SupplierPNR,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_W
        //12_5_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderPayment(
int _WhiteLabelDocId,
string _SupplierPNR,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_X
        //12_5_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentToken(
int _WhiteLabelDocId,
string _SupplierPNR,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_Y
        //12_5_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentRemarks(
int _WhiteLabelDocId,
string _SupplierPNR,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_F_Z
        //12_5_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_SupplierPNR_Currency(
int _WhiteLabelDocId,
string _SupplierPNR,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.SupplierPNR = _SupplierPNR; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_H
        //12_6_7
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_DateOfBook(
int _WhiteLabelDocId,
Double _TotalPrice,
DateTime _DateOfBook , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.DateOfBook = _DateOfBook;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_DateOfBook(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_DateOfBook));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_I
        //12_6_8
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_ReceiveEmail(
int _WhiteLabelDocId,
Double _TotalPrice,
bool _ReceiveEmail , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.ReceiveEmail = _ReceiveEmail;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_ReceiveEmail(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ReceiveEmail));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_J
        //12_6_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_ReceiveSms(
int _WhiteLabelDocId,
Double _TotalPrice,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_K
        //12_6_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_MarketingCabin(
int _WhiteLabelDocId,
Double _TotalPrice,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_L
        //12_6_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_ContactDetailsDocId(
int _WhiteLabelDocId,
Double _TotalPrice,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_N
        //12_6_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_AirlineIataCode(
int _WhiteLabelDocId,
Double _TotalPrice,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_O
        //12_6_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_OrderStatus(
int _WhiteLabelDocId,
Double _TotalPrice,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_P
        //12_6_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_AdminRemarks(
int _WhiteLabelDocId,
Double _TotalPrice,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_Q
        //12_6_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_StatusPayment(
int _WhiteLabelDocId,
Double _TotalPrice,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_R
        //12_6_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_OrderToken(
int _WhiteLabelDocId,
Double _TotalPrice,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_S
        //12_6_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentTransaction(
int _WhiteLabelDocId,
Double _TotalPrice,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_T
        //12_6_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_XmlBookRequest(
int _WhiteLabelDocId,
Double _TotalPrice,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_U
        //12_6_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_XmlBookResponse(
int _WhiteLabelDocId,
Double _TotalPrice,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_V
        //12_6_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_OrderRemarks(
int _WhiteLabelDocId,
Double _TotalPrice,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_W
        //12_6_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_OrderPayment(
int _WhiteLabelDocId,
Double _TotalPrice,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_X
        //12_6_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentToken(
int _WhiteLabelDocId,
Double _TotalPrice,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_Y
        //12_6_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentRemarks(
int _WhiteLabelDocId,
Double _TotalPrice,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_G_Z
        //12_6_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_TotalPrice_Currency(
int _WhiteLabelDocId,
Double _TotalPrice,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.TotalPrice = _TotalPrice; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_I
        //12_7_8
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_ReceiveEmail(
int _WhiteLabelDocId,
DateTime _DateOfBook,
bool _ReceiveEmail , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.ReceiveEmail = _ReceiveEmail;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_ReceiveEmail(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ReceiveEmail));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_J
        //12_7_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_ReceiveSms(
int _WhiteLabelDocId,
DateTime _DateOfBook,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_K
        //12_7_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_MarketingCabin(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_L
        //12_7_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_ContactDetailsDocId(
int _WhiteLabelDocId,
DateTime _DateOfBook,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_N
        //12_7_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_AirlineIataCode(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_O
        //12_7_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_OrderStatus(
int _WhiteLabelDocId,
DateTime _DateOfBook,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_P
        //12_7_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_AdminRemarks(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_Q
        //12_7_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_StatusPayment(
int _WhiteLabelDocId,
DateTime _DateOfBook,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_R
        //12_7_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_OrderToken(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_S
        //12_7_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentTransaction(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_T
        //12_7_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_XmlBookRequest(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_U
        //12_7_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_XmlBookResponse(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_V
        //12_7_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_OrderRemarks(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_W
        //12_7_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_OrderPayment(
int _WhiteLabelDocId,
DateTime _DateOfBook,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_X
        //12_7_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentToken(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_Y
        //12_7_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentRemarks(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_H_Z
        //12_7_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_DateOfBook_Currency(
int _WhiteLabelDocId,
DateTime _DateOfBook,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.DateOfBook = _DateOfBook; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_J
        //12_8_9
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_ReceiveSms(
int _WhiteLabelDocId,
bool _ReceiveEmail,
bool _ReceiveSms , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.ReceiveSms = _ReceiveSms;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_ReceiveSms(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_ReceiveSms));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_K
        //12_8_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_MarketingCabin(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_L
        //12_8_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_ContactDetailsDocId(
int _WhiteLabelDocId,
bool _ReceiveEmail,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_N
        //12_8_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_AirlineIataCode(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_O
        //12_8_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderStatus(
int _WhiteLabelDocId,
bool _ReceiveEmail,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_P
        //12_8_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_AdminRemarks(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_Q
        //12_8_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_StatusPayment(
int _WhiteLabelDocId,
bool _ReceiveEmail,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_R
        //12_8_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderToken(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_S
        //12_8_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentTransaction(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_T
        //12_8_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_XmlBookRequest(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_U
        //12_8_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_XmlBookResponse(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_V
        //12_8_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderRemarks(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_W
        //12_8_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderPayment(
int _WhiteLabelDocId,
bool _ReceiveEmail,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_X
        //12_8_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentToken(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_Y
        //12_8_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentRemarks(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_I_Z
        //12_8_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveEmail_Currency(
int _WhiteLabelDocId,
bool _ReceiveEmail,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveEmail = _ReceiveEmail; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_K
        //12_9_10
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_MarketingCabin(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _MarketingCabin , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.MarketingCabin = _MarketingCabin;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_MarketingCabin(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_MarketingCabin));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_L
        //12_9_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_ContactDetailsDocId(
int _WhiteLabelDocId,
bool _ReceiveSms,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_N
        //12_9_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_AirlineIataCode(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_O
        //12_9_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderStatus(
int _WhiteLabelDocId,
bool _ReceiveSms,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_P
        //12_9_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_AdminRemarks(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_Q
        //12_9_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_StatusPayment(
int _WhiteLabelDocId,
bool _ReceiveSms,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_R
        //12_9_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderToken(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_S
        //12_9_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentTransaction(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_T
        //12_9_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_XmlBookRequest(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_U
        //12_9_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_XmlBookResponse(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_V
        //12_9_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderRemarks(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_W
        //12_9_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderPayment(
int _WhiteLabelDocId,
bool _ReceiveSms,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_X
        //12_9_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentToken(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_Y
        //12_9_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentRemarks(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_J_Z
        //12_9_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ReceiveSms_Currency(
int _WhiteLabelDocId,
bool _ReceiveSms,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ReceiveSms = _ReceiveSms; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_L
        //12_10_11
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_ContactDetailsDocId(
int _WhiteLabelDocId,
string _MarketingCabin,
int _ContactDetailsDocId , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_ContactDetailsDocId(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_ContactDetailsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_N
        //12_10_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_AirlineIataCode(
int _WhiteLabelDocId,
string _MarketingCabin,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_O
        //12_10_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderStatus(
int _WhiteLabelDocId,
string _MarketingCabin,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_P
        //12_10_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_AdminRemarks(
int _WhiteLabelDocId,
string _MarketingCabin,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_Q
        //12_10_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_StatusPayment(
int _WhiteLabelDocId,
string _MarketingCabin,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_R
        //12_10_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderToken(
int _WhiteLabelDocId,
string _MarketingCabin,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_S
        //12_10_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentTransaction(
int _WhiteLabelDocId,
string _MarketingCabin,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_T
        //12_10_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_XmlBookRequest(
int _WhiteLabelDocId,
string _MarketingCabin,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_U
        //12_10_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_XmlBookResponse(
int _WhiteLabelDocId,
string _MarketingCabin,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_V
        //12_10_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderRemarks(
int _WhiteLabelDocId,
string _MarketingCabin,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_W
        //12_10_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderPayment(
int _WhiteLabelDocId,
string _MarketingCabin,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_X
        //12_10_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentToken(
int _WhiteLabelDocId,
string _MarketingCabin,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_Y
        //12_10_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentRemarks(
int _WhiteLabelDocId,
string _MarketingCabin,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_K_Z
        //12_10_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_MarketingCabin_Currency(
int _WhiteLabelDocId,
string _MarketingCabin,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.MarketingCabin = _MarketingCabin; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_N
        //12_11_13
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_AirlineIataCode(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _AirlineIataCode , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_AirlineIataCode(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_AirlineIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_O
        //12_11_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderStatus(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_P
        //12_11_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_AdminRemarks(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_Q
        //12_11_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_StatusPayment(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_R
        //12_11_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderToken(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_S
        //12_11_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentTransaction(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_T
        //12_11_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_XmlBookRequest(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_U
        //12_11_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_XmlBookResponse(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_V
        //12_11_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderRemarks(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_W
        //12_11_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderPayment(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_X
        //12_11_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentToken(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_Y
        //12_11_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentRemarks(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_L_Z
        //12_11_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_Currency(
int _WhiteLabelDocId,
int _ContactDetailsDocId,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.ContactDetailsDocId = _ContactDetailsDocId; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_O
        //12_13_14
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderStatus(
int _WhiteLabelDocId,
string _AirlineIataCode,
int _OrderStatus , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.OrderStatus = _OrderStatus;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderStatus(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderStatus));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_P
        //12_13_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_AdminRemarks(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_Q
        //12_13_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_StatusPayment(
int _WhiteLabelDocId,
string _AirlineIataCode,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_R
        //12_13_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderToken(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_S
        //12_13_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentTransaction(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_T
        //12_13_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_XmlBookRequest(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_U
        //12_13_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_XmlBookResponse(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_V
        //12_13_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderRemarks(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_W
        //12_13_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderPayment(
int _WhiteLabelDocId,
string _AirlineIataCode,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_X
        //12_13_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentToken(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_Y
        //12_13_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentRemarks(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_N_Z
        //12_13_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AirlineIataCode_Currency(
int _WhiteLabelDocId,
string _AirlineIataCode,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AirlineIataCode = _AirlineIataCode; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_P
        //12_14_15
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_AdminRemarks(
int _WhiteLabelDocId,
int _OrderStatus,
string _AdminRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.AdminRemarks = _AdminRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_AdminRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_AdminRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_Q
        //12_14_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_StatusPayment(
int _WhiteLabelDocId,
int _OrderStatus,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_R
        //12_14_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_OrderToken(
int _WhiteLabelDocId,
int _OrderStatus,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_S
        //12_14_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentTransaction(
int _WhiteLabelDocId,
int _OrderStatus,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_T
        //12_14_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_XmlBookRequest(
int _WhiteLabelDocId,
int _OrderStatus,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_U
        //12_14_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_XmlBookResponse(
int _WhiteLabelDocId,
int _OrderStatus,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_V
        //12_14_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_OrderRemarks(
int _WhiteLabelDocId,
int _OrderStatus,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_W
        //12_14_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_OrderPayment(
int _WhiteLabelDocId,
int _OrderStatus,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_X
        //12_14_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentToken(
int _WhiteLabelDocId,
int _OrderStatus,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_Y
        //12_14_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentRemarks(
int _WhiteLabelDocId,
int _OrderStatus,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_O_Z
        //12_14_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderStatus_Currency(
int _WhiteLabelDocId,
int _OrderStatus,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderStatus = _OrderStatus; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_Q
        //12_15_16
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_StatusPayment(
int _WhiteLabelDocId,
string _AdminRemarks,
int _StatusPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.StatusPayment = _StatusPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_StatusPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_StatusPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_R
        //12_15_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderToken(
int _WhiteLabelDocId,
string _AdminRemarks,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_S
        //12_15_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentTransaction(
int _WhiteLabelDocId,
string _AdminRemarks,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_T
        //12_15_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_XmlBookRequest(
int _WhiteLabelDocId,
string _AdminRemarks,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_U
        //12_15_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_XmlBookResponse(
int _WhiteLabelDocId,
string _AdminRemarks,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_V
        //12_15_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderRemarks(
int _WhiteLabelDocId,
string _AdminRemarks,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_W
        //12_15_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderPayment(
int _WhiteLabelDocId,
string _AdminRemarks,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_X
        //12_15_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentToken(
int _WhiteLabelDocId,
string _AdminRemarks,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_Y
        //12_15_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentRemarks(
int _WhiteLabelDocId,
string _AdminRemarks,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_P_Z
        //12_15_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_AdminRemarks_Currency(
int _WhiteLabelDocId,
string _AdminRemarks,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.AdminRemarks = _AdminRemarks; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_R
        //12_16_17
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_OrderToken(
int _WhiteLabelDocId,
int _StatusPayment,
string _OrderToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.OrderToken = _OrderToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_OrderToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_S
        //12_16_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentTransaction(
int _WhiteLabelDocId,
int _StatusPayment,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_T
        //12_16_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_XmlBookRequest(
int _WhiteLabelDocId,
int _StatusPayment,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_U
        //12_16_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_XmlBookResponse(
int _WhiteLabelDocId,
int _StatusPayment,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_V
        //12_16_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_OrderRemarks(
int _WhiteLabelDocId,
int _StatusPayment,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_W
        //12_16_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_OrderPayment(
int _WhiteLabelDocId,
int _StatusPayment,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_X
        //12_16_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentToken(
int _WhiteLabelDocId,
int _StatusPayment,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_Y
        //12_16_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentRemarks(
int _WhiteLabelDocId,
int _StatusPayment,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Q_Z
        //12_16_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_StatusPayment_Currency(
int _WhiteLabelDocId,
int _StatusPayment,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.StatusPayment = _StatusPayment; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R_S
        //12_17_18
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken_PaymentTransaction(
int _WhiteLabelDocId,
string _OrderToken,
string _PaymentTransaction , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken; 
 oOrders.PaymentTransaction = _PaymentTransaction;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_PaymentTransaction(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentTransaction));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R_T
        //12_17_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken_XmlBookRequest(
int _WhiteLabelDocId,
string _OrderToken,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R_U
        //12_17_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken_XmlBookResponse(
int _WhiteLabelDocId,
string _OrderToken,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R_V
        //12_17_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken_OrderRemarks(
int _WhiteLabelDocId,
string _OrderToken,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R_W
        //12_17_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken_OrderPayment(
int _WhiteLabelDocId,
string _OrderToken,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R_X
        //12_17_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken_PaymentToken(
int _WhiteLabelDocId,
string _OrderToken,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R_Y
        //12_17_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken_PaymentRemarks(
int _WhiteLabelDocId,
string _OrderToken,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_R_Z
        //12_17_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderToken_Currency(
int _WhiteLabelDocId,
string _OrderToken,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderToken = _OrderToken; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_S_T
        //12_18_19
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentTransaction_XmlBookRequest(
int _WhiteLabelDocId,
string _PaymentTransaction,
string _XmlBookRequest , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction; 
 oOrders.XmlBookRequest = _XmlBookRequest;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_XmlBookRequest(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_XmlBookRequest));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_S_U
        //12_18_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentTransaction_XmlBookResponse(
int _WhiteLabelDocId,
string _PaymentTransaction,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_S_V
        //12_18_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentTransaction_OrderRemarks(
int _WhiteLabelDocId,
string _PaymentTransaction,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_S_W
        //12_18_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentTransaction_OrderPayment(
int _WhiteLabelDocId,
string _PaymentTransaction,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_S_X
        //12_18_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentTransaction_PaymentToken(
int _WhiteLabelDocId,
string _PaymentTransaction,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_S_Y
        //12_18_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentTransaction_PaymentRemarks(
int _WhiteLabelDocId,
string _PaymentTransaction,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_S_Z
        //12_18_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentTransaction_Currency(
int _WhiteLabelDocId,
string _PaymentTransaction,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentTransaction = _PaymentTransaction; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_T_U
        //12_19_20
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookRequest_XmlBookResponse(
int _WhiteLabelDocId,
string _XmlBookRequest,
string _XmlBookResponse , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookRequest = _XmlBookRequest; 
 oOrders.XmlBookResponse = _XmlBookResponse;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_XmlBookResponse(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_XmlBookResponse));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_T_V
        //12_19_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookRequest_OrderRemarks(
int _WhiteLabelDocId,
string _XmlBookRequest,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookRequest = _XmlBookRequest; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_T_W
        //12_19_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookRequest_OrderPayment(
int _WhiteLabelDocId,
string _XmlBookRequest,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookRequest = _XmlBookRequest; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_T_X
        //12_19_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookRequest_PaymentToken(
int _WhiteLabelDocId,
string _XmlBookRequest,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookRequest = _XmlBookRequest; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_T_Y
        //12_19_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookRequest_PaymentRemarks(
int _WhiteLabelDocId,
string _XmlBookRequest,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookRequest = _XmlBookRequest; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_T_Z
        //12_19_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookRequest_Currency(
int _WhiteLabelDocId,
string _XmlBookRequest,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookRequest = _XmlBookRequest; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_U_V
        //12_20_21
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookResponse_OrderRemarks(
int _WhiteLabelDocId,
string _XmlBookResponse,
string _OrderRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookResponse = _XmlBookResponse; 
 oOrders.OrderRemarks = _OrderRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_OrderRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_OrderRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_U_W
        //12_20_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookResponse_OrderPayment(
int _WhiteLabelDocId,
string _XmlBookResponse,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookResponse = _XmlBookResponse; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_U_X
        //12_20_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookResponse_PaymentToken(
int _WhiteLabelDocId,
string _XmlBookResponse,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookResponse = _XmlBookResponse; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_U_Y
        //12_20_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookResponse_PaymentRemarks(
int _WhiteLabelDocId,
string _XmlBookResponse,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookResponse = _XmlBookResponse; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_U_Z
        //12_20_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_XmlBookResponse_Currency(
int _WhiteLabelDocId,
string _XmlBookResponse,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.XmlBookResponse = _XmlBookResponse; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_V_W
        //12_21_22
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderRemarks_OrderPayment(
int _WhiteLabelDocId,
string _OrderRemarks,
int _OrderPayment , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderRemarks = _OrderRemarks; 
 oOrders.OrderPayment = _OrderPayment;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks_OrderPayment(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_OrderPayment));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_V_X
        //12_21_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderRemarks_PaymentToken(
int _WhiteLabelDocId,
string _OrderRemarks,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderRemarks = _OrderRemarks; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_V_Y
        //12_21_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderRemarks_PaymentRemarks(
int _WhiteLabelDocId,
string _OrderRemarks,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderRemarks = _OrderRemarks; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_V_Z
        //12_21_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderRemarks_Currency(
int _WhiteLabelDocId,
string _OrderRemarks,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderRemarks = _OrderRemarks; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_W_X
        //12_22_23
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderPayment_PaymentToken(
int _WhiteLabelDocId,
int _OrderPayment,
string _PaymentToken , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderPayment = _OrderPayment; 
 oOrders.PaymentToken = _PaymentToken;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderPayment_PaymentToken(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_PaymentToken));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_W_Y
        //12_22_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderPayment_PaymentRemarks(
int _WhiteLabelDocId,
int _OrderPayment,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderPayment = _OrderPayment; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderPayment_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_W_Z
        //12_22_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_OrderPayment_Currency(
int _WhiteLabelDocId,
int _OrderPayment,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.OrderPayment = _OrderPayment; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_OrderPayment_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_X_Y
        //12_23_24
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentToken_PaymentRemarks(
int _WhiteLabelDocId,
string _PaymentToken,
string _PaymentRemarks , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentToken = _PaymentToken; 
 oOrders.PaymentRemarks = _PaymentRemarks;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentToken_PaymentRemarks(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken_PaymentRemarks));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_X_Z
        //12_23_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentToken_Currency(
int _WhiteLabelDocId,
string _PaymentToken,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentToken = _PaymentToken; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentToken_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }



        //M_Y_Z
        //12_24_25
        public static OrdersContainer SelectByKeysView_WhiteLabelDocId_PaymentRemarks_Currency(
int _WhiteLabelDocId,
string _PaymentRemarks,
string _Currency , bool? isActive)
        {
            OrdersContainer oOrdersContainer = new OrdersContainer();
            Orders oOrders = new Orders();
            #region Params
            
 oOrders.WhiteLabelDocId = _WhiteLabelDocId; 
 oOrders.PaymentRemarks = _PaymentRemarks; 
 oOrders.Currency = _Currency;
            #endregion 
            oOrdersContainer.Add(SelectData(Orders.GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentRemarks_Currency(oOrders), TBNames_Orders.PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentRemarks_Currency));
            #region ExtraFilters
            
if(isActive != null){
                oOrdersContainer = oOrdersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oOrdersContainer;
        }


#endregion
}

    
}
