

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class LanguagesContainer  : Container<LanguagesContainer, Languages>{
#region Extra functions

#endregion

        #region Static Method
        
        public static LanguagesContainer SelectByID(int doc_id,bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            oLanguagesContainer.Add(oLanguagesContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oLanguagesContainer;
        }

        
        public static LanguagesContainer SelectAllLanguagess(bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            oLanguagesContainer.Add(oLanguagesContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oLanguagesContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static LanguagesContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DateCreated = _DateCreated;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DateCreated(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //B
        //1
        public static LanguagesContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DocId = _DocId;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DocId(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //C
        //2
        public static LanguagesContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.IsDeleted = _IsDeleted;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_IsDeleted(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //E
        //4
        public static LanguagesContainer SelectByKeysView_Name(
string _Name , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.Name = _Name;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_Name(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_Name));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //F
        //5
        public static LanguagesContainer SelectByKeysView_Code(
string _Code , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.Code = _Code;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_Code(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_Code));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //A_B
        //0_1
        public static LanguagesContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DateCreated = _DateCreated; 
 oLanguages.DocId = _DocId;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DateCreated_DocId(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //A_C
        //0_2
        public static LanguagesContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DateCreated = _DateCreated; 
 oLanguages.IsDeleted = _IsDeleted;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //A_E
        //0_4
        public static LanguagesContainer SelectByKeysView_DateCreated_Name(
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DateCreated = _DateCreated; 
 oLanguages.Name = _Name;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DateCreated_Name(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //A_F
        //0_5
        public static LanguagesContainer SelectByKeysView_DateCreated_Code(
DateTime _DateCreated,
string _Code , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DateCreated = _DateCreated; 
 oLanguages.Code = _Code;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DateCreated_Code(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DateCreated_Code));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //B_C
        //1_2
        public static LanguagesContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DocId = _DocId; 
 oLanguages.IsDeleted = _IsDeleted;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DocId_IsDeleted(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //B_E
        //1_4
        public static LanguagesContainer SelectByKeysView_DocId_Name(
int _DocId,
string _Name , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DocId = _DocId; 
 oLanguages.Name = _Name;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DocId_Name(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //B_F
        //1_5
        public static LanguagesContainer SelectByKeysView_DocId_Code(
int _DocId,
string _Code , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.DocId = _DocId; 
 oLanguages.Code = _Code;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_DocId_Code(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_DocId_Code));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //C_E
        //2_4
        public static LanguagesContainer SelectByKeysView_IsDeleted_Name(
bool _IsDeleted,
string _Name , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.IsDeleted = _IsDeleted; 
 oLanguages.Name = _Name;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_IsDeleted_Name(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //C_F
        //2_5
        public static LanguagesContainer SelectByKeysView_IsDeleted_Code(
bool _IsDeleted,
string _Code , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.IsDeleted = _IsDeleted; 
 oLanguages.Code = _Code;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_IsDeleted_Code(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_IsDeleted_Code));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }



        //E_F
        //4_5
        public static LanguagesContainer SelectByKeysView_Name_Code(
string _Name,
string _Code , bool? isActive)
        {
            LanguagesContainer oLanguagesContainer = new LanguagesContainer();
            Languages oLanguages = new Languages();
            #region Params
            
 oLanguages.Name = _Name; 
 oLanguages.Code = _Code;
            #endregion 
            oLanguagesContainer.Add(SelectData(Languages.GetParamsForSelectByKeysView_Name_Code(oLanguages), TBNames_Languages.PROC_Select_Languages_By_Keys_View_Name_Code));
            #region ExtraFilters
            
if(isActive != null){
                oLanguagesContainer = oLanguagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oLanguagesContainer;
        }


#endregion
}

    
}
