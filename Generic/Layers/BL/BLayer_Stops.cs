

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class StopsContainer  : Container<StopsContainer, Stops>{
#region Extra functions

#endregion

        #region Static Method
        
        public static StopsContainer SelectByID(int doc_id,bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            oStopsContainer.Add(oStopsContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oStopsContainer;
        }

        
        public static StopsContainer SelectAllStopss(bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            oStopsContainer.Add(oStopsContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oStopsContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static StopsContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DateCreated = _DateCreated;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DateCreated(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //B
        //1
        public static StopsContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DocId = _DocId;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DocId(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //C
        //2
        public static StopsContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.IsDeleted = _IsDeleted;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_IsDeleted(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //E
        //4
        public static StopsContainer SelectByKeysView_Arrival(
DateTime _Arrival , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.Arrival = _Arrival;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_Arrival(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_Arrival));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //F
        //5
        public static StopsContainer SelectByKeysView_Departure(
DateTime _Departure , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.Departure = _Departure;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_Departure(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_Departure));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //G
        //6
        public static StopsContainer SelectByKeysView_FlightLegDocId(
int _FlightLegDocId , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_FlightLegDocId(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //H
        //7
        public static StopsContainer SelectByKeysView_AirportIataCode(
string _AirportIataCode , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.AirportIataCode = _AirportIataCode;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_AirportIataCode(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_AirportIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //A_B
        //0_1
        public static StopsContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DateCreated = _DateCreated; 
 oStops.DocId = _DocId;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DateCreated_DocId(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //A_C
        //0_2
        public static StopsContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DateCreated = _DateCreated; 
 oStops.IsDeleted = _IsDeleted;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //A_E
        //0_4
        public static StopsContainer SelectByKeysView_DateCreated_Arrival(
DateTime _DateCreated,
DateTime _Arrival , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DateCreated = _DateCreated; 
 oStops.Arrival = _Arrival;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DateCreated_Arrival(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DateCreated_Arrival));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //A_F
        //0_5
        public static StopsContainer SelectByKeysView_DateCreated_Departure(
DateTime _DateCreated,
DateTime _Departure , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DateCreated = _DateCreated; 
 oStops.Departure = _Departure;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DateCreated_Departure(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DateCreated_Departure));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //A_G
        //0_6
        public static StopsContainer SelectByKeysView_DateCreated_FlightLegDocId(
DateTime _DateCreated,
int _FlightLegDocId , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DateCreated = _DateCreated; 
 oStops.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DateCreated_FlightLegDocId(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DateCreated_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //A_H
        //0_7
        public static StopsContainer SelectByKeysView_DateCreated_AirportIataCode(
DateTime _DateCreated,
string _AirportIataCode , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DateCreated = _DateCreated; 
 oStops.AirportIataCode = _AirportIataCode;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DateCreated_AirportIataCode(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DateCreated_AirportIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //B_C
        //1_2
        public static StopsContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DocId = _DocId; 
 oStops.IsDeleted = _IsDeleted;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DocId_IsDeleted(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //B_E
        //1_4
        public static StopsContainer SelectByKeysView_DocId_Arrival(
int _DocId,
DateTime _Arrival , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DocId = _DocId; 
 oStops.Arrival = _Arrival;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DocId_Arrival(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DocId_Arrival));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //B_F
        //1_5
        public static StopsContainer SelectByKeysView_DocId_Departure(
int _DocId,
DateTime _Departure , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DocId = _DocId; 
 oStops.Departure = _Departure;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DocId_Departure(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DocId_Departure));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //B_G
        //1_6
        public static StopsContainer SelectByKeysView_DocId_FlightLegDocId(
int _DocId,
int _FlightLegDocId , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DocId = _DocId; 
 oStops.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DocId_FlightLegDocId(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DocId_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //B_H
        //1_7
        public static StopsContainer SelectByKeysView_DocId_AirportIataCode(
int _DocId,
string _AirportIataCode , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.DocId = _DocId; 
 oStops.AirportIataCode = _AirportIataCode;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_DocId_AirportIataCode(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_DocId_AirportIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //C_E
        //2_4
        public static StopsContainer SelectByKeysView_IsDeleted_Arrival(
bool _IsDeleted,
DateTime _Arrival , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.IsDeleted = _IsDeleted; 
 oStops.Arrival = _Arrival;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_IsDeleted_Arrival(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_IsDeleted_Arrival));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //C_F
        //2_5
        public static StopsContainer SelectByKeysView_IsDeleted_Departure(
bool _IsDeleted,
DateTime _Departure , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.IsDeleted = _IsDeleted; 
 oStops.Departure = _Departure;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_IsDeleted_Departure(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_IsDeleted_Departure));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //C_G
        //2_6
        public static StopsContainer SelectByKeysView_IsDeleted_FlightLegDocId(
bool _IsDeleted,
int _FlightLegDocId , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.IsDeleted = _IsDeleted; 
 oStops.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_IsDeleted_FlightLegDocId(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_IsDeleted_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //C_H
        //2_7
        public static StopsContainer SelectByKeysView_IsDeleted_AirportIataCode(
bool _IsDeleted,
string _AirportIataCode , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.IsDeleted = _IsDeleted; 
 oStops.AirportIataCode = _AirportIataCode;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_IsDeleted_AirportIataCode(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_IsDeleted_AirportIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //E_F
        //4_5
        public static StopsContainer SelectByKeysView_Arrival_Departure(
DateTime _Arrival,
DateTime _Departure , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.Arrival = _Arrival; 
 oStops.Departure = _Departure;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_Arrival_Departure(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_Arrival_Departure));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //E_G
        //4_6
        public static StopsContainer SelectByKeysView_Arrival_FlightLegDocId(
DateTime _Arrival,
int _FlightLegDocId , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.Arrival = _Arrival; 
 oStops.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_Arrival_FlightLegDocId(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_Arrival_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //E_H
        //4_7
        public static StopsContainer SelectByKeysView_Arrival_AirportIataCode(
DateTime _Arrival,
string _AirportIataCode , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.Arrival = _Arrival; 
 oStops.AirportIataCode = _AirportIataCode;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_Arrival_AirportIataCode(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_Arrival_AirportIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //F_G
        //5_6
        public static StopsContainer SelectByKeysView_Departure_FlightLegDocId(
DateTime _Departure,
int _FlightLegDocId , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.Departure = _Departure; 
 oStops.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_Departure_FlightLegDocId(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_Departure_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //F_H
        //5_7
        public static StopsContainer SelectByKeysView_Departure_AirportIataCode(
DateTime _Departure,
string _AirportIataCode , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.Departure = _Departure; 
 oStops.AirportIataCode = _AirportIataCode;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_Departure_AirportIataCode(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_Departure_AirportIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }



        //G_H
        //6_7
        public static StopsContainer SelectByKeysView_FlightLegDocId_AirportIataCode(
int _FlightLegDocId,
string _AirportIataCode , bool? isActive)
        {
            StopsContainer oStopsContainer = new StopsContainer();
            Stops oStops = new Stops();
            #region Params
            
 oStops.FlightLegDocId = _FlightLegDocId; 
 oStops.AirportIataCode = _AirportIataCode;
            #endregion 
            oStopsContainer.Add(SelectData(Stops.GetParamsForSelectByKeysView_FlightLegDocId_AirportIataCode(oStops), TBNames_Stops.PROC_Select_Stops_By_Keys_View_FlightLegDocId_AirportIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oStopsContainer = oStopsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStopsContainer;
        }


#endregion
}

    
}
