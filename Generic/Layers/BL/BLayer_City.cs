

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class CityContainer  : Container<CityContainer, City>{
#region Extra functions

#endregion

        #region Static Method
        
        public static CityContainer SelectByID(int doc_id,bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            oCityContainer.Add(oCityContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oCityContainer;
        }

        
        public static CityContainer SelectAllCitys(bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            oCityContainer.Add(oCityContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oCityContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static CityContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DateCreated = _DateCreated;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DateCreated(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //B
        //1
        public static CityContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DocId = _DocId;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DocId(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //C
        //2
        public static CityContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IsDeleted = _IsDeleted;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IsDeleted(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //E
        //4
        public static CityContainer SelectByKeysView_IataCode(
string _IataCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IataCode = _IataCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IataCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //F
        //5
        public static CityContainer SelectByKeysView_Name(
string _Name , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.Name = _Name;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_Name(oCity), TBNames_City.PROC_Select_City_By_Keys_View_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCityContainer = oCityContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCityContainer;
        }



        //G
        //6
        public static CityContainer SelectByKeysView_CountryCode(
string _CountryCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.CountryCode = _CountryCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_CountryCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //H
        //7
        public static CityContainer SelectByKeysView_EnglishName(
string _EnglishName , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.EnglishName = _EnglishName;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_EnglishName(oCity), TBNames_City.PROC_Select_City_By_Keys_View_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //A_B
        //0_1
        public static CityContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DateCreated = _DateCreated; 
 oCity.DocId = _DocId;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DateCreated_DocId(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //A_C
        //0_2
        public static CityContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DateCreated = _DateCreated; 
 oCity.IsDeleted = _IsDeleted;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //A_E
        //0_4
        public static CityContainer SelectByKeysView_DateCreated_IataCode(
DateTime _DateCreated,
string _IataCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DateCreated = _DateCreated; 
 oCity.IataCode = _IataCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DateCreated_IataCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DateCreated_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //A_F
        //0_5
        public static CityContainer SelectByKeysView_DateCreated_Name(
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DateCreated = _DateCreated; 
 oCity.Name = _Name;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DateCreated_Name(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCityContainer = oCityContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCityContainer;
        }



        //A_G
        //0_6
        public static CityContainer SelectByKeysView_DateCreated_CountryCode(
DateTime _DateCreated,
string _CountryCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DateCreated = _DateCreated; 
 oCity.CountryCode = _CountryCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DateCreated_CountryCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DateCreated_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //A_H
        //0_7
        public static CityContainer SelectByKeysView_DateCreated_EnglishName(
DateTime _DateCreated,
string _EnglishName , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DateCreated = _DateCreated; 
 oCity.EnglishName = _EnglishName;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DateCreated_EnglishName(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DateCreated_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //B_C
        //1_2
        public static CityContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DocId = _DocId; 
 oCity.IsDeleted = _IsDeleted;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DocId_IsDeleted(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //B_E
        //1_4
        public static CityContainer SelectByKeysView_DocId_IataCode(
int _DocId,
string _IataCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DocId = _DocId; 
 oCity.IataCode = _IataCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DocId_IataCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DocId_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //B_F
        //1_5
        public static CityContainer SelectByKeysView_DocId_Name(
int _DocId,
string _Name , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DocId = _DocId; 
 oCity.Name = _Name;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DocId_Name(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCityContainer = oCityContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCityContainer;
        }



        //B_G
        //1_6
        public static CityContainer SelectByKeysView_DocId_CountryCode(
int _DocId,
string _CountryCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DocId = _DocId; 
 oCity.CountryCode = _CountryCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DocId_CountryCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DocId_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //B_H
        //1_7
        public static CityContainer SelectByKeysView_DocId_EnglishName(
int _DocId,
string _EnglishName , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.DocId = _DocId; 
 oCity.EnglishName = _EnglishName;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_DocId_EnglishName(oCity), TBNames_City.PROC_Select_City_By_Keys_View_DocId_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //C_E
        //2_4
        public static CityContainer SelectByKeysView_IsDeleted_IataCode(
bool _IsDeleted,
string _IataCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IsDeleted = _IsDeleted; 
 oCity.IataCode = _IataCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IsDeleted_IataCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IsDeleted_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //C_F
        //2_5
        public static CityContainer SelectByKeysView_IsDeleted_Name(
bool _IsDeleted,
string _Name , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IsDeleted = _IsDeleted; 
 oCity.Name = _Name;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IsDeleted_Name(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCityContainer = oCityContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCityContainer;
        }



        //C_G
        //2_6
        public static CityContainer SelectByKeysView_IsDeleted_CountryCode(
bool _IsDeleted,
string _CountryCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IsDeleted = _IsDeleted; 
 oCity.CountryCode = _CountryCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IsDeleted_CountryCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IsDeleted_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //C_H
        //2_7
        public static CityContainer SelectByKeysView_IsDeleted_EnglishName(
bool _IsDeleted,
string _EnglishName , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IsDeleted = _IsDeleted; 
 oCity.EnglishName = _EnglishName;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IsDeleted_EnglishName(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IsDeleted_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //E_F
        //4_5
        public static CityContainer SelectByKeysView_IataCode_Name(
string _IataCode,
string _Name , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IataCode = _IataCode; 
 oCity.Name = _Name;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IataCode_Name(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IataCode_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCityContainer = oCityContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCityContainer;
        }



        //E_G
        //4_6
        public static CityContainer SelectByKeysView_IataCode_CountryCode(
string _IataCode,
string _CountryCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IataCode = _IataCode; 
 oCity.CountryCode = _CountryCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IataCode_CountryCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IataCode_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //E_H
        //4_7
        public static CityContainer SelectByKeysView_IataCode_EnglishName(
string _IataCode,
string _EnglishName , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.IataCode = _IataCode; 
 oCity.EnglishName = _EnglishName;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_IataCode_EnglishName(oCity), TBNames_City.PROC_Select_City_By_Keys_View_IataCode_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }



        //F_G
        //5_6
        public static CityContainer SelectByKeysView_Name_CountryCode(
string _Name,
string _CountryCode , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.Name = _Name; 
 oCity.CountryCode = _CountryCode;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_Name_CountryCode(oCity), TBNames_City.PROC_Select_City_By_Keys_View_Name_CountryCode));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCityContainer = oCityContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCityContainer;
        }



        //F_H
        //5_7
        public static CityContainer SelectByKeysView_Name_EnglishName(
string _Name,
string _EnglishName , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.Name = _Name; 
 oCity.EnglishName = _EnglishName;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_Name_EnglishName(oCity), TBNames_City.PROC_Select_City_By_Keys_View_Name_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oCityContainer = oCityContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oCityContainer;
        }



        //G_H
        //6_7
        public static CityContainer SelectByKeysView_CountryCode_EnglishName(
string _CountryCode,
string _EnglishName , bool? isActive)
        {
            CityContainer oCityContainer = new CityContainer();
            City oCity = new City();
            #region Params
            
 oCity.CountryCode = _CountryCode; 
 oCity.EnglishName = _EnglishName;
            #endregion 
            oCityContainer.Add(SelectData(City.GetParamsForSelectByKeysView_CountryCode_EnglishName(oCity), TBNames_City.PROC_Select_City_By_Keys_View_CountryCode_EnglishName));
            #region ExtraFilters
            
if(isActive != null){
                oCityContainer = oCityContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCityContainer;
        }


#endregion
}

    
}
