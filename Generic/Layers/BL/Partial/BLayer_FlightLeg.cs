

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost
{
    using DL_LowCost;

    public partial class FlightLegContainer : Container<FlightLegContainer, FlightLeg>
    {
        #region Extra functions

        #endregion

        #region Public Static Method

        public static TimeSpan CalculateStopTime(FlightLeg currentLeg, FlightLeg NextLeg)
        {
            if (NextLeg == null)
                return new TimeSpan(0, 0, -1);

            DateTime arrival = ConvertToValue.ConvertToDateTime(currentLeg.ArrivalDateTime_Value);
            DateTime departure = ConvertToValue.ConvertToDateTime(NextLeg.DepartureDateTime_Value);
            return (departure - arrival);
        }

        #endregion




    }


}
