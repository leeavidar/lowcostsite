

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost
{
    using DL_LowCost;

    public partial class SeoContainer : Container<SeoContainer, Seo>
    {
        #region Extra functions


        /// <summary>
        /// A method that check if the friendly url provided already exist in the database (SEO table).
        /// </summary>
        /// <param name="url">The Friendly URL to check</param>
        /// <param name="whiteLabel">The White Label</param>
        /// <returns>True - the URL is taken, or False if not.</returns>
        public static bool IsFriendlyUrlTaken(string url, int whiteLabel)
        {

            SeoContainer oSeoContainer = SelectByKeysView_WhiteLabelDocId_FriendlyUrl(whiteLabel, url, null);

            if (oSeoContainer.Count > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

    }


}
