

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class DestinationPageContainer  : Container<DestinationPageContainer, DestinationPage>{
#region Extra functions

#endregion

        #region Static Method
        
        public static DestinationPageContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            oDestinationPageContainer.Add(oDestinationPageContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oDestinationPageContainer;
        }

        
        public static DestinationPageContainer SelectAllDestinationPages(int? _WhiteLabelDocId,bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            oDestinationPageContainer.Add(oDestinationPageContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oDestinationPageContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //G_A
        //6_0
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B
        //6_1
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_C
        //6_2
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_E
        //6_4
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order(
int _WhiteLabelDocId,
int _Order , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_F
        //6_5
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode(
int _WhiteLabelDocId,
string _DestinationIataCode , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_H
        //6_7
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId(
int _WhiteLabelDocId,
int _SeoDocId , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.SeoDocId = _SeoDocId;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_I
        //6_8
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Price(
int _WhiteLabelDocId,
Double _Price , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Price = _Price;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_J
        //6_9
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_LocalCurrency(
int _WhiteLabelDocId,
string _LocalCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.LocalCurrency = _LocalCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_K
        //6_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Name(
int _WhiteLabelDocId,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_L
        //6_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Time(
int _WhiteLabelDocId,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_M
        //6_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Title(
int _WhiteLabelDocId,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_N
        //6_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_PriceCurrency(
int _WhiteLabelDocId,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_B
        //6_0_1
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.DocId = _DocId;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_C
        //6_0_2
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.IsDeleted = _IsDeleted;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_E
        //6_0_4
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Order(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Order , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.Order = _Order;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Order));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_F
        //6_0_5
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DestinationIataCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _DestinationIataCode , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DestinationIataCode(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_H
        //6_0_7
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _SeoDocId , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.SeoDocId = _SeoDocId;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_I
        //6_0_8
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Price(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _Price , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.Price = _Price;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Price(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Price));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_J
        //6_0_9
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LocalCurrency(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LocalCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.LocalCurrency = _LocalCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LocalCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_LocalCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_K
        //6_0_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Name(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_L
        //6_0_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Time(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_M
        //6_0_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Title(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_A_N
        //6_0_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PriceCurrency(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DateCreated = _DateCreated; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_C
        //6_1_2
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.IsDeleted = _IsDeleted;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_E
        //6_1_4
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Order(
int _WhiteLabelDocId,
int _DocId,
int _Order , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.Order = _Order;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_F
        //6_1_5
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_DestinationIataCode(
int _WhiteLabelDocId,
int _DocId,
string _DestinationIataCode , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DestinationIataCode(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_H
        //6_1_7
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(
int _WhiteLabelDocId,
int _DocId,
int _SeoDocId , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.SeoDocId = _SeoDocId;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_I
        //6_1_8
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Price(
int _WhiteLabelDocId,
int _DocId,
Double _Price , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.Price = _Price;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Price(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Price));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_J
        //6_1_9
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_LocalCurrency(
int _WhiteLabelDocId,
int _DocId,
string _LocalCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.LocalCurrency = _LocalCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LocalCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_LocalCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_K
        //6_1_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Name(
int _WhiteLabelDocId,
int _DocId,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_L
        //6_1_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Time(
int _WhiteLabelDocId,
int _DocId,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_M
        //6_1_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_Title(
int _WhiteLabelDocId,
int _DocId,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_B_N
        //6_1_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DocId_PriceCurrency(
int _WhiteLabelDocId,
int _DocId,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DocId = _DocId; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_E
        //6_2_4
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Order(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Order , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.Order = _Order;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Order));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_F
        //6_2_5
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationIataCode(
int _WhiteLabelDocId,
bool _IsDeleted,
string _DestinationIataCode , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationIataCode(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_H
        //6_2_7
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _SeoDocId , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.SeoDocId = _SeoDocId;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_I
        //6_2_8
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Price(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _Price , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.Price = _Price;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Price(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Price));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_J
        //6_2_9
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LocalCurrency(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LocalCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.LocalCurrency = _LocalCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LocalCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_LocalCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_K
        //6_2_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Name(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_L
        //6_2_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Time(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_M
        //6_2_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Title(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_C_N
        //6_2_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PriceCurrency(
int _WhiteLabelDocId,
bool _IsDeleted,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.IsDeleted = _IsDeleted; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_E_F
        //6_4_5
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_DestinationIataCode(
int _WhiteLabelDocId,
int _Order,
string _DestinationIataCode , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_DestinationIataCode(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_DestinationIataCode));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_E_H
        //6_4_7
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_SeoDocId(
int _WhiteLabelDocId,
int _Order,
int _SeoDocId , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order; 
 oDestinationPage.SeoDocId = _SeoDocId;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_SeoDocId(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_E_I
        //6_4_8
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_Price(
int _WhiteLabelDocId,
int _Order,
Double _Price , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order; 
 oDestinationPage.Price = _Price;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Price(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Price));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_E_J
        //6_4_9
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_LocalCurrency(
int _WhiteLabelDocId,
int _Order,
string _LocalCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order; 
 oDestinationPage.LocalCurrency = _LocalCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_LocalCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_LocalCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_E_K
        //6_4_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_Name(
int _WhiteLabelDocId,
int _Order,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_E_L
        //6_4_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_Time(
int _WhiteLabelDocId,
int _Order,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_E_M
        //6_4_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_Title(
int _WhiteLabelDocId,
int _Order,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_E_N
        //6_4_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Order_PriceCurrency(
int _WhiteLabelDocId,
int _Order,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Order = _Order; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_F_H
        //6_5_7
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode_SeoDocId(
int _WhiteLabelDocId,
string _DestinationIataCode,
int _SeoDocId , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode; 
 oDestinationPage.SeoDocId = _SeoDocId;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_SeoDocId(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_F_I
        //6_5_8
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode_Price(
int _WhiteLabelDocId,
string _DestinationIataCode,
Double _Price , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode; 
 oDestinationPage.Price = _Price;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_Price(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Price));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_F_J
        //6_5_9
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode_LocalCurrency(
int _WhiteLabelDocId,
string _DestinationIataCode,
string _LocalCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode; 
 oDestinationPage.LocalCurrency = _LocalCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_LocalCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_LocalCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_F_K
        //6_5_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode_Name(
int _WhiteLabelDocId,
string _DestinationIataCode,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_F_L
        //6_5_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode_Time(
int _WhiteLabelDocId,
string _DestinationIataCode,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_F_M
        //6_5_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode_Title(
int _WhiteLabelDocId,
string _DestinationIataCode,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_F_N
        //6_5_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_DestinationIataCode_PriceCurrency(
int _WhiteLabelDocId,
string _DestinationIataCode,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.DestinationIataCode = _DestinationIataCode; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_H_I
        //6_7_8
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_Price(
int _WhiteLabelDocId,
int _SeoDocId,
Double _Price , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.SeoDocId = _SeoDocId; 
 oDestinationPage.Price = _Price;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Price(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Price));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_H_J
        //6_7_9
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_LocalCurrency(
int _WhiteLabelDocId,
int _SeoDocId,
string _LocalCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.SeoDocId = _SeoDocId; 
 oDestinationPage.LocalCurrency = _LocalCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_LocalCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_LocalCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_H_K
        //6_7_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_Name(
int _WhiteLabelDocId,
int _SeoDocId,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.SeoDocId = _SeoDocId; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_H_L
        //6_7_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_Time(
int _WhiteLabelDocId,
int _SeoDocId,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.SeoDocId = _SeoDocId; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_H_M
        //6_7_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_Title(
int _WhiteLabelDocId,
int _SeoDocId,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.SeoDocId = _SeoDocId; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_H_N
        //6_7_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_SeoDocId_PriceCurrency(
int _WhiteLabelDocId,
int _SeoDocId,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.SeoDocId = _SeoDocId; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_I_J
        //6_8_9
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Price_LocalCurrency(
int _WhiteLabelDocId,
Double _Price,
string _LocalCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Price = _Price; 
 oDestinationPage.LocalCurrency = _LocalCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_LocalCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_LocalCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_I_K
        //6_8_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Price_Name(
int _WhiteLabelDocId,
Double _Price,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Price = _Price; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_I_L
        //6_8_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Price_Time(
int _WhiteLabelDocId,
Double _Price,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Price = _Price; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_I_M
        //6_8_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Price_Title(
int _WhiteLabelDocId,
Double _Price,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Price = _Price; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_I_N
        //6_8_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Price_PriceCurrency(
int _WhiteLabelDocId,
Double _Price,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Price = _Price; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Price_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_J_K
        //6_9_10
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_LocalCurrency_Name(
int _WhiteLabelDocId,
string _LocalCurrency,
string _Name , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.LocalCurrency = _LocalCurrency; 
 oDestinationPage.Name = _Name;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency_Name(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Name));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_J_L
        //6_9_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_LocalCurrency_Time(
int _WhiteLabelDocId,
string _LocalCurrency,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.LocalCurrency = _LocalCurrency; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_J_M
        //6_9_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_LocalCurrency_Title(
int _WhiteLabelDocId,
string _LocalCurrency,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.LocalCurrency = _LocalCurrency; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_J_N
        //6_9_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_LocalCurrency_PriceCurrency(
int _WhiteLabelDocId,
string _LocalCurrency,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.LocalCurrency = _LocalCurrency; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_K_L
        //6_10_11
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Name_Time(
int _WhiteLabelDocId,
string _Name,
string _Time , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Name = _Name; 
 oDestinationPage.Time = _Time;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Time(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_Time));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_K_M
        //6_10_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Name_Title(
int _WhiteLabelDocId,
string _Name,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Name = _Name; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name) && string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_K_N
        //6_10_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Name_PriceCurrency(
int _WhiteLabelDocId,
string _Name,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Name = _Name; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oDestinationPageContainer;
        }



        //G_L_M
        //6_11_12
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Time_Title(
int _WhiteLabelDocId,
string _Time,
string _Title , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Time = _Time; 
 oDestinationPage.Title = _Title;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Time_Title(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time_Title));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }



        //G_L_N
        //6_11_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Time_PriceCurrency(
int _WhiteLabelDocId,
string _Time,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Time = _Time; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Time_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oDestinationPageContainer;
        }



        //G_M_N
        //6_12_13
        public static DestinationPageContainer SelectByKeysView_WhiteLabelDocId_Title_PriceCurrency(
int _WhiteLabelDocId,
string _Title,
string _PriceCurrency , bool? isActive)
        {
            DestinationPageContainer oDestinationPageContainer = new DestinationPageContainer();
            DestinationPage oDestinationPage = new DestinationPage();
            #region Params
            
 oDestinationPage.WhiteLabelDocId = _WhiteLabelDocId; 
 oDestinationPage.Title = _Title; 
 oDestinationPage.PriceCurrency = _PriceCurrency;
            #endregion 
            oDestinationPageContainer.Add(SelectData(DestinationPage.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PriceCurrency(oDestinationPage), TBNames_DestinationPage.PROC_Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Title_PriceCurrency));
            #region ExtraFilters
            
if(isActive != null){
                oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oDestinationPageContainer = oDestinationPageContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oDestinationPageContainer;
        }


#endregion
}

    
}
