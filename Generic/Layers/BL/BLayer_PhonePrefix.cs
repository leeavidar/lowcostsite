

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class PhonePrefixContainer  : Container<PhonePrefixContainer, PhonePrefix>{
#region Extra functions

#endregion

        #region Static Method
        
        public static PhonePrefixContainer SelectByID(int doc_id,bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            oPhonePrefixContainer.Add(oPhonePrefixContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oPhonePrefixContainer;
        }

        
        public static PhonePrefixContainer SelectAllPhonePrefixs(bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            oPhonePrefixContainer.Add(oPhonePrefixContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oPhonePrefixContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static PhonePrefixContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.DateCreated = _DateCreated;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_DateCreated(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //B
        //1
        public static PhonePrefixContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.DocId = _DocId;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_DocId(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //C
        //2
        public static PhonePrefixContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.IsDeleted = _IsDeleted;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_IsDeleted(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //E
        //4
        public static PhonePrefixContainer SelectByKeysView_PrefixNumber(
string _PrefixNumber , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.PrefixNumber = _PrefixNumber;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_PrefixNumber(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_PrefixNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //A_B
        //0_1
        public static PhonePrefixContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.DateCreated = _DateCreated; 
 oPhonePrefix.DocId = _DocId;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_DateCreated_DocId(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //A_C
        //0_2
        public static PhonePrefixContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.DateCreated = _DateCreated; 
 oPhonePrefix.IsDeleted = _IsDeleted;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //A_E
        //0_4
        public static PhonePrefixContainer SelectByKeysView_DateCreated_PrefixNumber(
DateTime _DateCreated,
string _PrefixNumber , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.DateCreated = _DateCreated; 
 oPhonePrefix.PrefixNumber = _PrefixNumber;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_DateCreated_PrefixNumber(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_DateCreated_PrefixNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //B_C
        //1_2
        public static PhonePrefixContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.DocId = _DocId; 
 oPhonePrefix.IsDeleted = _IsDeleted;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_DocId_IsDeleted(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //B_E
        //1_4
        public static PhonePrefixContainer SelectByKeysView_DocId_PrefixNumber(
int _DocId,
string _PrefixNumber , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.DocId = _DocId; 
 oPhonePrefix.PrefixNumber = _PrefixNumber;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_DocId_PrefixNumber(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_DocId_PrefixNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }



        //C_E
        //2_4
        public static PhonePrefixContainer SelectByKeysView_IsDeleted_PrefixNumber(
bool _IsDeleted,
string _PrefixNumber , bool? isActive)
        {
            PhonePrefixContainer oPhonePrefixContainer = new PhonePrefixContainer();
            PhonePrefix oPhonePrefix = new PhonePrefix();
            #region Params
            
 oPhonePrefix.IsDeleted = _IsDeleted; 
 oPhonePrefix.PrefixNumber = _PrefixNumber;
            #endregion 
            oPhonePrefixContainer.Add(SelectData(PhonePrefix.GetParamsForSelectByKeysView_IsDeleted_PrefixNumber(oPhonePrefix), TBNames_PhonePrefix.PROC_Select_PhonePrefix_By_Keys_View_IsDeleted_PrefixNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPhonePrefixContainer = oPhonePrefixContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPhonePrefixContainer;
        }


#endregion
}

    
}
