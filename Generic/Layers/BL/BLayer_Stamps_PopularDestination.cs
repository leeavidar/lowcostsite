

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class Stamps_PopularDestinationContainer  : Container<Stamps_PopularDestinationContainer, Stamps_PopularDestination>{
#region Extra functions

#endregion

        #region Static Method
        
        public static Stamps_PopularDestinationContainer SelectByID(int doc_id,bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            oStamps_PopularDestinationContainer.Add(oStamps_PopularDestinationContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oStamps_PopularDestinationContainer;
        }

        
        public static Stamps_PopularDestinationContainer SelectAllStamps_PopularDestinations(bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            oStamps_PopularDestinationContainer.Add(oStamps_PopularDestinationContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oStamps_PopularDestinationContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static Stamps_PopularDestinationContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DateCreated = _DateCreated;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DateCreated(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //B
        //1
        public static Stamps_PopularDestinationContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DocId = _DocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //C
        //2
        public static Stamps_PopularDestinationContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.IsDeleted = _IsDeleted;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_IsDeleted(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //E
        //4
        public static Stamps_PopularDestinationContainer SelectByKeysView_PopularDestinationDocId(
int _PopularDestinationDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.PopularDestinationDocId = _PopularDestinationDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_PopularDestinationDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_PopularDestinationDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //F
        //5
        public static Stamps_PopularDestinationContainer SelectByKeysView_StampsDocId(
int _StampsDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.StampsDocId = _StampsDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_StampsDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_StampsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //A_B
        //0_1
        public static Stamps_PopularDestinationContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DateCreated = _DateCreated; 
 oStamps_PopularDestination.DocId = _DocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DateCreated_DocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //A_C
        //0_2
        public static Stamps_PopularDestinationContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DateCreated = _DateCreated; 
 oStamps_PopularDestination.IsDeleted = _IsDeleted;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //A_E
        //0_4
        public static Stamps_PopularDestinationContainer SelectByKeysView_DateCreated_PopularDestinationDocId(
DateTime _DateCreated,
int _PopularDestinationDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DateCreated = _DateCreated; 
 oStamps_PopularDestination.PopularDestinationDocId = _PopularDestinationDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DateCreated_PopularDestinationDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated_PopularDestinationDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //A_F
        //0_5
        public static Stamps_PopularDestinationContainer SelectByKeysView_DateCreated_StampsDocId(
DateTime _DateCreated,
int _StampsDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DateCreated = _DateCreated; 
 oStamps_PopularDestination.StampsDocId = _StampsDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DateCreated_StampsDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated_StampsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //B_C
        //1_2
        public static Stamps_PopularDestinationContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DocId = _DocId; 
 oStamps_PopularDestination.IsDeleted = _IsDeleted;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DocId_IsDeleted(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //B_E
        //1_4
        public static Stamps_PopularDestinationContainer SelectByKeysView_DocId_PopularDestinationDocId(
int _DocId,
int _PopularDestinationDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DocId = _DocId; 
 oStamps_PopularDestination.PopularDestinationDocId = _PopularDestinationDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DocId_PopularDestinationDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DocId_PopularDestinationDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //B_F
        //1_5
        public static Stamps_PopularDestinationContainer SelectByKeysView_DocId_StampsDocId(
int _DocId,
int _StampsDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.DocId = _DocId; 
 oStamps_PopularDestination.StampsDocId = _StampsDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_DocId_StampsDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_DocId_StampsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //C_E
        //2_4
        public static Stamps_PopularDestinationContainer SelectByKeysView_IsDeleted_PopularDestinationDocId(
bool _IsDeleted,
int _PopularDestinationDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.IsDeleted = _IsDeleted; 
 oStamps_PopularDestination.PopularDestinationDocId = _PopularDestinationDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_IsDeleted_PopularDestinationDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_IsDeleted_PopularDestinationDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //C_F
        //2_5
        public static Stamps_PopularDestinationContainer SelectByKeysView_IsDeleted_StampsDocId(
bool _IsDeleted,
int _StampsDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.IsDeleted = _IsDeleted; 
 oStamps_PopularDestination.StampsDocId = _StampsDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_IsDeleted_StampsDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_IsDeleted_StampsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }



        //E_F
        //4_5
        public static Stamps_PopularDestinationContainer SelectByKeysView_PopularDestinationDocId_StampsDocId(
int _PopularDestinationDocId,
int _StampsDocId , bool? isActive)
        {
            Stamps_PopularDestinationContainer oStamps_PopularDestinationContainer = new Stamps_PopularDestinationContainer();
            Stamps_PopularDestination oStamps_PopularDestination = new Stamps_PopularDestination();
            #region Params
            
 oStamps_PopularDestination.PopularDestinationDocId = _PopularDestinationDocId; 
 oStamps_PopularDestination.StampsDocId = _StampsDocId;
            #endregion 
            oStamps_PopularDestinationContainer.Add(SelectData(Stamps_PopularDestination.GetParamsForSelectByKeysView_PopularDestinationDocId_StampsDocId(oStamps_PopularDestination), TBNames_Stamps_PopularDestination.PROC_Select_Stamps_PopularDestination_By_Keys_View_PopularDestinationDocId_StampsDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStamps_PopularDestinationContainer = oStamps_PopularDestinationContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStamps_PopularDestinationContainer;
        }


#endregion
}

    
}
