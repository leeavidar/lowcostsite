

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class WhiteLabelContainer  : Container<WhiteLabelContainer, WhiteLabel>{
#region Extra functions

#endregion

        #region Static Method
        
        public static WhiteLabelContainer SelectByID(int doc_id,bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            oWhiteLabelContainer.Add(oWhiteLabelContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oWhiteLabelContainer;
        }

        
        public static WhiteLabelContainer SelectAllWhiteLabels(bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            oWhiteLabelContainer.Add(oWhiteLabelContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oWhiteLabelContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static WhiteLabelContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //B
        //1
        public static WhiteLabelContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //C
        //2
        public static WhiteLabelContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //E
        //4
        public static WhiteLabelContainer SelectByKeysView_Domain(
string _Domain , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Domain = _Domain;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Domain(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Domain));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //F
        //5
        public static WhiteLabelContainer SelectByKeysView_Name(
string _Name , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Name = _Name;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Name(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Name));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //G
        //6
        public static WhiteLabelContainer SelectByKeysView_LanguagesDocId(
int _LanguagesDocId , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.LanguagesDocId = _LanguagesDocId;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_LanguagesDocId(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //H
        //7
        public static WhiteLabelContainer SelectByKeysView_Phone(
string _Phone , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Phone = _Phone;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Phone(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Phone));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //I
        //8
        public static WhiteLabelContainer SelectByKeysView_Email(
string _Email , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Email = _Email;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Email(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Email));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //J
        //9
        public static WhiteLabelContainer SelectByKeysView_DiscountType(
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //K
        //10
        public static WhiteLabelContainer SelectByKeysView_Address(
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //L
        //11
        public static WhiteLabelContainer SelectByKeysView_Discount(
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //A_B
        //0_1
        public static WhiteLabelContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.DocId = _DocId;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_DocId(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //A_C
        //0_2
        public static WhiteLabelContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.IsDeleted = _IsDeleted;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //A_E
        //0_4
        public static WhiteLabelContainer SelectByKeysView_DateCreated_Domain(
DateTime _DateCreated,
string _Domain , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.Domain = _Domain;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_Domain(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Domain));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //A_F
        //0_5
        public static WhiteLabelContainer SelectByKeysView_DateCreated_Name(
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.Name = _Name;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_Name(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //A_G
        //0_6
        public static WhiteLabelContainer SelectByKeysView_DateCreated_LanguagesDocId(
DateTime _DateCreated,
int _LanguagesDocId , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.LanguagesDocId = _LanguagesDocId;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_LanguagesDocId(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_LanguagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //A_H
        //0_7
        public static WhiteLabelContainer SelectByKeysView_DateCreated_Phone(
DateTime _DateCreated,
string _Phone , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.Phone = _Phone;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_Phone(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Phone));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //A_I
        //0_8
        public static WhiteLabelContainer SelectByKeysView_DateCreated_Email(
DateTime _DateCreated,
string _Email , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.Email = _Email;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_Email(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Email));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //A_J
        //0_9
        public static WhiteLabelContainer SelectByKeysView_DateCreated_DiscountType(
DateTime _DateCreated,
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //A_K
        //0_10
        public static WhiteLabelContainer SelectByKeysView_DateCreated_Address(
DateTime _DateCreated,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //A_L
        //0_11
        public static WhiteLabelContainer SelectByKeysView_DateCreated_Discount(
DateTime _DateCreated,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DateCreated = _DateCreated; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DateCreated_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DateCreated_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //B_C
        //1_2
        public static WhiteLabelContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.IsDeleted = _IsDeleted;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_IsDeleted(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //B_E
        //1_4
        public static WhiteLabelContainer SelectByKeysView_DocId_Domain(
int _DocId,
string _Domain , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.Domain = _Domain;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_Domain(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_Domain));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //B_F
        //1_5
        public static WhiteLabelContainer SelectByKeysView_DocId_Name(
int _DocId,
string _Name , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.Name = _Name;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_Name(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //B_G
        //1_6
        public static WhiteLabelContainer SelectByKeysView_DocId_LanguagesDocId(
int _DocId,
int _LanguagesDocId , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.LanguagesDocId = _LanguagesDocId;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_LanguagesDocId(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_LanguagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //B_H
        //1_7
        public static WhiteLabelContainer SelectByKeysView_DocId_Phone(
int _DocId,
string _Phone , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.Phone = _Phone;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_Phone(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_Phone));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //B_I
        //1_8
        public static WhiteLabelContainer SelectByKeysView_DocId_Email(
int _DocId,
string _Email , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.Email = _Email;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_Email(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_Email));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //B_J
        //1_9
        public static WhiteLabelContainer SelectByKeysView_DocId_DiscountType(
int _DocId,
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //B_K
        //1_10
        public static WhiteLabelContainer SelectByKeysView_DocId_Address(
int _DocId,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //B_L
        //1_11
        public static WhiteLabelContainer SelectByKeysView_DocId_Discount(
int _DocId,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DocId = _DocId; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DocId_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DocId_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //C_E
        //2_4
        public static WhiteLabelContainer SelectByKeysView_IsDeleted_Domain(
bool _IsDeleted,
string _Domain , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted; 
 oWhiteLabel.Domain = _Domain;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted_Domain(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Domain));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //C_F
        //2_5
        public static WhiteLabelContainer SelectByKeysView_IsDeleted_Name(
bool _IsDeleted,
string _Name , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted; 
 oWhiteLabel.Name = _Name;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted_Name(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //C_G
        //2_6
        public static WhiteLabelContainer SelectByKeysView_IsDeleted_LanguagesDocId(
bool _IsDeleted,
int _LanguagesDocId , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted; 
 oWhiteLabel.LanguagesDocId = _LanguagesDocId;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted_LanguagesDocId(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_LanguagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //C_H
        //2_7
        public static WhiteLabelContainer SelectByKeysView_IsDeleted_Phone(
bool _IsDeleted,
string _Phone , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted; 
 oWhiteLabel.Phone = _Phone;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted_Phone(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Phone));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //C_I
        //2_8
        public static WhiteLabelContainer SelectByKeysView_IsDeleted_Email(
bool _IsDeleted,
string _Email , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted; 
 oWhiteLabel.Email = _Email;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted_Email(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Email));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //C_J
        //2_9
        public static WhiteLabelContainer SelectByKeysView_IsDeleted_DiscountType(
bool _IsDeleted,
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted; 
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //C_K
        //2_10
        public static WhiteLabelContainer SelectByKeysView_IsDeleted_Address(
bool _IsDeleted,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //C_L
        //2_11
        public static WhiteLabelContainer SelectByKeysView_IsDeleted_Discount(
bool _IsDeleted,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.IsDeleted = _IsDeleted; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_IsDeleted_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_IsDeleted_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //E_F
        //4_5
        public static WhiteLabelContainer SelectByKeysView_Domain_Name(
string _Domain,
string _Name , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Domain = _Domain; 
 oWhiteLabel.Name = _Name;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Domain_Name(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Domain_Name));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //E_G
        //4_6
        public static WhiteLabelContainer SelectByKeysView_Domain_LanguagesDocId(
string _Domain,
int _LanguagesDocId , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Domain = _Domain; 
 oWhiteLabel.LanguagesDocId = _LanguagesDocId;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Domain_LanguagesDocId(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Domain_LanguagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //E_H
        //4_7
        public static WhiteLabelContainer SelectByKeysView_Domain_Phone(
string _Domain,
string _Phone , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Domain = _Domain; 
 oWhiteLabel.Phone = _Phone;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Domain_Phone(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Domain_Phone));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //E_I
        //4_8
        public static WhiteLabelContainer SelectByKeysView_Domain_Email(
string _Domain,
string _Email , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Domain = _Domain; 
 oWhiteLabel.Email = _Email;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Domain_Email(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Domain_Email));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //E_J
        //4_9
        public static WhiteLabelContainer SelectByKeysView_Domain_DiscountType(
string _Domain,
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Domain = _Domain; 
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Domain_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Domain_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //E_K
        //4_10
        public static WhiteLabelContainer SelectByKeysView_Domain_Address(
string _Domain,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Domain = _Domain; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Domain_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Domain_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //E_L
        //4_11
        public static WhiteLabelContainer SelectByKeysView_Domain_Discount(
string _Domain,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Domain = _Domain; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Domain_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Domain_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //F_G
        //5_6
        public static WhiteLabelContainer SelectByKeysView_Name_LanguagesDocId(
string _Name,
int _LanguagesDocId , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Name = _Name; 
 oWhiteLabel.LanguagesDocId = _LanguagesDocId;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Name_LanguagesDocId(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Name_LanguagesDocId));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //F_H
        //5_7
        public static WhiteLabelContainer SelectByKeysView_Name_Phone(
string _Name,
string _Phone , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Name = _Name; 
 oWhiteLabel.Phone = _Phone;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Name_Phone(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Name_Phone));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //F_I
        //5_8
        public static WhiteLabelContainer SelectByKeysView_Name_Email(
string _Name,
string _Email , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Name = _Name; 
 oWhiteLabel.Email = _Email;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Name_Email(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Name_Email));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //F_J
        //5_9
        public static WhiteLabelContainer SelectByKeysView_Name_DiscountType(
string _Name,
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Name = _Name; 
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Name_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Name_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //F_K
        //5_10
        public static WhiteLabelContainer SelectByKeysView_Name_Address(
string _Name,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Name = _Name; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Name_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Name_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name) && string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //F_L
        //5_11
        public static WhiteLabelContainer SelectByKeysView_Name_Discount(
string _Name,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Name = _Name; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Name_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Name_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oWhiteLabelContainer;
        }



        //G_H
        //6_7
        public static WhiteLabelContainer SelectByKeysView_LanguagesDocId_Phone(
int _LanguagesDocId,
string _Phone , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.LanguagesDocId = _LanguagesDocId; 
 oWhiteLabel.Phone = _Phone;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_LanguagesDocId_Phone(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_Phone));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //G_I
        //6_8
        public static WhiteLabelContainer SelectByKeysView_LanguagesDocId_Email(
int _LanguagesDocId,
string _Email , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.LanguagesDocId = _LanguagesDocId; 
 oWhiteLabel.Email = _Email;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_LanguagesDocId_Email(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_Email));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //G_J
        //6_9
        public static WhiteLabelContainer SelectByKeysView_LanguagesDocId_DiscountType(
int _LanguagesDocId,
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.LanguagesDocId = _LanguagesDocId; 
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_LanguagesDocId_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //G_K
        //6_10
        public static WhiteLabelContainer SelectByKeysView_LanguagesDocId_Address(
int _LanguagesDocId,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.LanguagesDocId = _LanguagesDocId; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_LanguagesDocId_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //G_L
        //6_11
        public static WhiteLabelContainer SelectByKeysView_LanguagesDocId_Discount(
int _LanguagesDocId,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.LanguagesDocId = _LanguagesDocId; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_LanguagesDocId_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_LanguagesDocId_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //H_I
        //7_8
        public static WhiteLabelContainer SelectByKeysView_Phone_Email(
string _Phone,
string _Email , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Phone = _Phone; 
 oWhiteLabel.Email = _Email;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Phone_Email(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Phone_Email));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //H_J
        //7_9
        public static WhiteLabelContainer SelectByKeysView_Phone_DiscountType(
string _Phone,
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Phone = _Phone; 
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Phone_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Phone_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //H_K
        //7_10
        public static WhiteLabelContainer SelectByKeysView_Phone_Address(
string _Phone,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Phone = _Phone; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Phone_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Phone_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //H_L
        //7_11
        public static WhiteLabelContainer SelectByKeysView_Phone_Discount(
string _Phone,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Phone = _Phone; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Phone_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Phone_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //I_J
        //8_9
        public static WhiteLabelContainer SelectByKeysView_Email_DiscountType(
string _Email,
int _DiscountType , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Email = _Email; 
 oWhiteLabel.DiscountType = _DiscountType;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Email_DiscountType(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Email_DiscountType));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //I_K
        //8_10
        public static WhiteLabelContainer SelectByKeysView_Email_Address(
string _Email,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Email = _Email; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Email_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Email_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //I_L
        //8_11
        public static WhiteLabelContainer SelectByKeysView_Email_Discount(
string _Email,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Email = _Email; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Email_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Email_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //J_K
        //9_10
        public static WhiteLabelContainer SelectByKeysView_DiscountType_Address(
int _DiscountType,
string _Address , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DiscountType = _DiscountType; 
 oWhiteLabel.Address = _Address;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DiscountType_Address(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DiscountType_Address));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }



        //J_L
        //9_11
        public static WhiteLabelContainer SelectByKeysView_DiscountType_Discount(
int _DiscountType,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.DiscountType = _DiscountType; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_DiscountType_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_DiscountType_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oWhiteLabelContainer;
        }



        //K_L
        //10_11
        public static WhiteLabelContainer SelectByKeysView_Address_Discount(
string _Address,
Double _Discount , bool? isActive)
        {
            WhiteLabelContainer oWhiteLabelContainer = new WhiteLabelContainer();
            WhiteLabel oWhiteLabel = new WhiteLabel();
            #region Params
            
 oWhiteLabel.Address = _Address; 
 oWhiteLabel.Discount = _Discount;
            #endregion 
            oWhiteLabelContainer.Add(SelectData(WhiteLabel.GetParamsForSelectByKeysView_Address_Discount(oWhiteLabel), TBNames_WhiteLabel.PROC_Select_WhiteLabel_By_Keys_View_Address_Discount));
            #region ExtraFilters
            
if(isActive != null){
                oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oWhiteLabelContainer = oWhiteLabelContainer.FindAllContainer(R => string.Equals(R.Address, _Address));
            #endregion
            return oWhiteLabelContainer;
        }


#endregion
}

    
}
