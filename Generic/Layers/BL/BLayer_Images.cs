

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class ImagesContainer  : Container<ImagesContainer, Images>{
#region Extra functions

#endregion

        #region Static Method
        
        public static ImagesContainer SelectByID(int doc_id,bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            oImagesContainer.Add(oImagesContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oImagesContainer;
        }

        
        public static ImagesContainer SelectAllImagess(bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            oImagesContainer.Add(oImagesContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oImagesContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static ImagesContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //B
        //1
        public static ImagesContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DocId = _DocId;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DocId(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //C
        //2
        public static ImagesContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.IsDeleted = _IsDeleted;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_IsDeleted(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //E
        //4
        public static ImagesContainer SelectByKeysView_ImageFileName(
string _ImageFileName , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageFileName = _ImageFileName;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageFileName(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageFileName));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //F
        //5
        public static ImagesContainer SelectByKeysView_RelatedObjectDocId(
int _RelatedObjectDocId , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_RelatedObjectDocId(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //G
        //6
        public static ImagesContainer SelectByKeysView_ImageType(
string _ImageType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageType = _ImageType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //H
        //7
        public static ImagesContainer SelectByKeysView_RelatedObjectType(
string _RelatedObjectType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_RelatedObjectType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //I
        //8
        public static ImagesContainer SelectByKeysView_Title(
string _Title , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.Title = _Title;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_Title(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_Title));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oImagesContainer;
        }



        //J
        //9
        public static ImagesContainer SelectByKeysView_Alt(
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }



        //A_B
        //0_1
        public static ImagesContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated; 
 oImages.DocId = _DocId;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated_DocId(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //A_C
        //0_2
        public static ImagesContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated; 
 oImages.IsDeleted = _IsDeleted;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //A_E
        //0_4
        public static ImagesContainer SelectByKeysView_DateCreated_ImageFileName(
DateTime _DateCreated,
string _ImageFileName , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated; 
 oImages.ImageFileName = _ImageFileName;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated_ImageFileName(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated_ImageFileName));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //A_F
        //0_5
        public static ImagesContainer SelectByKeysView_DateCreated_RelatedObjectDocId(
DateTime _DateCreated,
int _RelatedObjectDocId , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated; 
 oImages.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated_RelatedObjectDocId(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //A_G
        //0_6
        public static ImagesContainer SelectByKeysView_DateCreated_ImageType(
DateTime _DateCreated,
string _ImageType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated; 
 oImages.ImageType = _ImageType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated_ImageType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated_ImageType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //A_H
        //0_7
        public static ImagesContainer SelectByKeysView_DateCreated_RelatedObjectType(
DateTime _DateCreated,
string _RelatedObjectType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated; 
 oImages.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated_RelatedObjectType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //A_I
        //0_8
        public static ImagesContainer SelectByKeysView_DateCreated_Title(
DateTime _DateCreated,
string _Title , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated; 
 oImages.Title = _Title;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated_Title(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated_Title));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oImagesContainer;
        }



        //A_J
        //0_9
        public static ImagesContainer SelectByKeysView_DateCreated_Alt(
DateTime _DateCreated,
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DateCreated = _DateCreated; 
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DateCreated_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DateCreated_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }



        //B_C
        //1_2
        public static ImagesContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DocId = _DocId; 
 oImages.IsDeleted = _IsDeleted;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DocId_IsDeleted(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //B_E
        //1_4
        public static ImagesContainer SelectByKeysView_DocId_ImageFileName(
int _DocId,
string _ImageFileName , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DocId = _DocId; 
 oImages.ImageFileName = _ImageFileName;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DocId_ImageFileName(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DocId_ImageFileName));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //B_F
        //1_5
        public static ImagesContainer SelectByKeysView_DocId_RelatedObjectDocId(
int _DocId,
int _RelatedObjectDocId , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DocId = _DocId; 
 oImages.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DocId_RelatedObjectDocId(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DocId_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //B_G
        //1_6
        public static ImagesContainer SelectByKeysView_DocId_ImageType(
int _DocId,
string _ImageType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DocId = _DocId; 
 oImages.ImageType = _ImageType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DocId_ImageType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DocId_ImageType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //B_H
        //1_7
        public static ImagesContainer SelectByKeysView_DocId_RelatedObjectType(
int _DocId,
string _RelatedObjectType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DocId = _DocId; 
 oImages.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DocId_RelatedObjectType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DocId_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //B_I
        //1_8
        public static ImagesContainer SelectByKeysView_DocId_Title(
int _DocId,
string _Title , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DocId = _DocId; 
 oImages.Title = _Title;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DocId_Title(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oImagesContainer;
        }



        //B_J
        //1_9
        public static ImagesContainer SelectByKeysView_DocId_Alt(
int _DocId,
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.DocId = _DocId; 
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_DocId_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_DocId_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }



        //C_E
        //2_4
        public static ImagesContainer SelectByKeysView_IsDeleted_ImageFileName(
bool _IsDeleted,
string _ImageFileName , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.IsDeleted = _IsDeleted; 
 oImages.ImageFileName = _ImageFileName;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_IsDeleted_ImageFileName(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_IsDeleted_ImageFileName));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //C_F
        //2_5
        public static ImagesContainer SelectByKeysView_IsDeleted_RelatedObjectDocId(
bool _IsDeleted,
int _RelatedObjectDocId , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.IsDeleted = _IsDeleted; 
 oImages.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_IsDeleted_RelatedObjectDocId(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_IsDeleted_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //C_G
        //2_6
        public static ImagesContainer SelectByKeysView_IsDeleted_ImageType(
bool _IsDeleted,
string _ImageType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.IsDeleted = _IsDeleted; 
 oImages.ImageType = _ImageType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_IsDeleted_ImageType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_IsDeleted_ImageType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //C_H
        //2_7
        public static ImagesContainer SelectByKeysView_IsDeleted_RelatedObjectType(
bool _IsDeleted,
string _RelatedObjectType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.IsDeleted = _IsDeleted; 
 oImages.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_IsDeleted_RelatedObjectType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_IsDeleted_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //C_I
        //2_8
        public static ImagesContainer SelectByKeysView_IsDeleted_Title(
bool _IsDeleted,
string _Title , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.IsDeleted = _IsDeleted; 
 oImages.Title = _Title;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_IsDeleted_Title(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_IsDeleted_Title));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oImagesContainer;
        }



        //C_J
        //2_9
        public static ImagesContainer SelectByKeysView_IsDeleted_Alt(
bool _IsDeleted,
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.IsDeleted = _IsDeleted; 
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_IsDeleted_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_IsDeleted_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }



        //E_F
        //4_5
        public static ImagesContainer SelectByKeysView_ImageFileName_RelatedObjectDocId(
string _ImageFileName,
int _RelatedObjectDocId , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageFileName = _ImageFileName; 
 oImages.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageFileName_RelatedObjectDocId(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageFileName_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //E_G
        //4_6
        public static ImagesContainer SelectByKeysView_ImageFileName_ImageType(
string _ImageFileName,
string _ImageType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageFileName = _ImageFileName; 
 oImages.ImageType = _ImageType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageFileName_ImageType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageFileName_ImageType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //E_H
        //4_7
        public static ImagesContainer SelectByKeysView_ImageFileName_RelatedObjectType(
string _ImageFileName,
string _RelatedObjectType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageFileName = _ImageFileName; 
 oImages.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageFileName_RelatedObjectType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageFileName_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //E_I
        //4_8
        public static ImagesContainer SelectByKeysView_ImageFileName_Title(
string _ImageFileName,
string _Title , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageFileName = _ImageFileName; 
 oImages.Title = _Title;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageFileName_Title(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageFileName_Title));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oImagesContainer;
        }



        //E_J
        //4_9
        public static ImagesContainer SelectByKeysView_ImageFileName_Alt(
string _ImageFileName,
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageFileName = _ImageFileName; 
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageFileName_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageFileName_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }



        //F_G
        //5_6
        public static ImagesContainer SelectByKeysView_RelatedObjectDocId_ImageType(
int _RelatedObjectDocId,
string _ImageType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.RelatedObjectDocId = _RelatedObjectDocId; 
 oImages.ImageType = _ImageType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_RelatedObjectDocId_ImageType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_RelatedObjectDocId_ImageType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //F_H
        //5_7
        public static ImagesContainer SelectByKeysView_RelatedObjectDocId_RelatedObjectType(
int _RelatedObjectDocId,
string _RelatedObjectType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.RelatedObjectDocId = _RelatedObjectDocId; 
 oImages.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_RelatedObjectDocId_RelatedObjectType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_RelatedObjectDocId_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //F_I
        //5_8
        public static ImagesContainer SelectByKeysView_RelatedObjectDocId_Title(
int _RelatedObjectDocId,
string _Title , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.RelatedObjectDocId = _RelatedObjectDocId; 
 oImages.Title = _Title;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_RelatedObjectDocId_Title(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_RelatedObjectDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oImagesContainer;
        }



        //F_J
        //5_9
        public static ImagesContainer SelectByKeysView_RelatedObjectDocId_Alt(
int _RelatedObjectDocId,
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.RelatedObjectDocId = _RelatedObjectDocId; 
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_RelatedObjectDocId_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_RelatedObjectDocId_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }



        //G_H
        //6_7
        public static ImagesContainer SelectByKeysView_ImageType_RelatedObjectType(
string _ImageType,
string _RelatedObjectType , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageType = _ImageType; 
 oImages.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageType_RelatedObjectType(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageType_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oImagesContainer;
        }



        //G_I
        //6_8
        public static ImagesContainer SelectByKeysView_ImageType_Title(
string _ImageType,
string _Title , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageType = _ImageType; 
 oImages.Title = _Title;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageType_Title(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageType_Title));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oImagesContainer;
        }



        //G_J
        //6_9
        public static ImagesContainer SelectByKeysView_ImageType_Alt(
string _ImageType,
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.ImageType = _ImageType; 
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_ImageType_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_ImageType_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }



        //H_I
        //7_8
        public static ImagesContainer SelectByKeysView_RelatedObjectType_Title(
string _RelatedObjectType,
string _Title , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.RelatedObjectType = _RelatedObjectType; 
 oImages.Title = _Title;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_RelatedObjectType_Title(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_RelatedObjectType_Title));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oImagesContainer;
        }



        //H_J
        //7_9
        public static ImagesContainer SelectByKeysView_RelatedObjectType_Alt(
string _RelatedObjectType,
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.RelatedObjectType = _RelatedObjectType; 
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_RelatedObjectType_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_RelatedObjectType_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }



        //I_J
        //8_9
        public static ImagesContainer SelectByKeysView_Title_Alt(
string _Title,
string _Alt , bool? isActive)
        {
            ImagesContainer oImagesContainer = new ImagesContainer();
            Images oImages = new Images();
            #region Params
            
 oImages.Title = _Title; 
 oImages.Alt = _Alt;
            #endregion 
            oImagesContainer.Add(SelectData(Images.GetParamsForSelectByKeysView_Title_Alt(oImages), TBNames_Images.PROC_Select_Images_By_Keys_View_Title_Alt));
            #region ExtraFilters
            
if(isActive != null){
                oImagesContainer = oImagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oImagesContainer = oImagesContainer.FindAllContainer(R => string.Equals(R.Title, _Title) && string.Equals(R.Alt, _Alt));
            #endregion
            return oImagesContainer;
        }


#endregion
}

    
}
