
ALTER INDEX ALL ON [dbo].[ContactDetails] REBUILD;

ALTER INDEX ALL ON [dbo].[Passenger] REBUILD;

ALTER INDEX ALL ON [dbo].[SeatPerFlightLeg] REBUILD;

ALTER INDEX ALL ON [dbo].[Luggage] REBUILD;

ALTER INDEX ALL ON [dbo].[FlightLeg] REBUILD;

ALTER INDEX ALL ON [dbo].[Airline] REBUILD;

ALTER INDEX ALL ON [dbo].[PopularDestination] REBUILD;

ALTER INDEX ALL ON [dbo].[Airport] REBUILD;

ALTER INDEX ALL ON [dbo].[City] REBUILD;

ALTER INDEX ALL ON [dbo].[Country] REBUILD;

ALTER INDEX ALL ON [dbo].[DestinationPage] REBUILD;

ALTER INDEX ALL ON [dbo].[OrderFlightLeg] REBUILD;

ALTER INDEX ALL ON [dbo].[Orders] REBUILD;

ALTER INDEX ALL ON [dbo].[Images] REBUILD;

ALTER INDEX ALL ON [dbo].[Messages] REBUILD;

ALTER INDEX ALL ON [dbo].[Stamps] REBUILD;

ALTER INDEX ALL ON [dbo].[Stamps_PopularDestination] REBUILD;

ALTER INDEX ALL ON [dbo].[WhiteLabel] REBUILD;

ALTER INDEX ALL ON [dbo].[Seo] REBUILD;

ALTER INDEX ALL ON [dbo].[IndexPagePromo] REBUILD;

ALTER INDEX ALL ON [dbo].[StaticPages] REBUILD;

ALTER INDEX ALL ON [dbo].[Stops] REBUILD;

ALTER INDEX ALL ON [dbo].[Template] REBUILD;

ALTER INDEX ALL ON [dbo].[SubBox] REBUILD;

ALTER INDEX ALL ON [dbo].[Currency] REBUILD;

ALTER INDEX ALL ON [dbo].[CompanyPage] REBUILD;

ALTER INDEX ALL ON [dbo].[Company] REBUILD;

ALTER INDEX ALL ON [dbo].[Tip] REBUILD;

ALTER INDEX ALL ON [dbo].[Users] REBUILD;

ALTER INDEX ALL ON [dbo].[HomePage] REBUILD;

ALTER INDEX ALL ON [dbo].[HomePageDestinationPage] REBUILD;

ALTER INDEX ALL ON [dbo].[Faq] REBUILD;

ALTER INDEX ALL ON [dbo].[MailAddress] REBUILD;

ALTER INDEX ALL ON [dbo].[PhonePrefix] REBUILD;

ALTER INDEX ALL ON [dbo].[OrderLog] REBUILD;

ALTER INDEX ALL ON [dbo].[Languages] REBUILD;
