﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net.Mail;
using DL_Generic;
using System.Threading;
using System.Net.Mime;

namespace Generic
{
    public static class MailHandler
    {
        #region sendMailWithAttachment
        public static void SendMailToClientWithPdf(string mailContent, string mailSubject, string clientEmail, string filePath, string OrderDocId)
        {
            MailMessage mail = new MailMessage();

            mail.To.Add(clientEmail);
            mail.Subject = mailSubject;
            mail.Body = mailContent;
            mail.IsBodyHtml = true;
            var PdfType = new ContentType { MediaType = MediaTypeNames.Application.Pdf };
            var attachmentPDF = new Attachment(filePath, PdfType);
            attachmentPDF.Name = "LowCost order #" + OrderDocId;
            mail.Attachments.Add(attachmentPDF);
            sendMessage(mail);
        }

        #endregion
        //Send relevant mail to support
        public static void SendMailToSupport(string mailContent, string mailSubject, bool withAlgoma = false)
        {
            mailContent += "<br/><br/>";
            //string subject = GetText("Order_Booking");

            string WhiteLabelEmailAddress = BasePage.WhiteLabelObj.Email_Value;
            string WhiteLabelEmailAnotherAddress = ConfigurationHandler.WhiteLabelEmailAnotherAddress; 

            List<string> sendto = new List<string>();
            sendto.Add(WhiteLabelEmailAddress);
            if (WhiteLabelEmailAnotherAddress!="")
            {
                sendto.Add(WhiteLabelEmailAnotherAddress);
            }
            if (withAlgoma)
            {
                sendto.Add(ConfigurationHandler.AlgomaEmailAddress);
            }
            MailHandler.SendMail(mailSubject, mailContent, sendto);
        }

        public static void SendMailToSupportWithPDF(string mailContent, string mailSubject, string filePath, string OrderDocId)
        {
            MailMessage mail = new MailMessage();
            string WhiteLabelEmailAddress = BasePage.WhiteLabelObj.Email_Value;
            string WhiteLabelEmailAnotherAddress = ConfigurationHandler.WhiteLabelEmailAnotherAddress; 

            mail.To.Add(WhiteLabelEmailAddress);
            if (WhiteLabelEmailAnotherAddress != "")
            {
                mail.To.Add(WhiteLabelEmailAnotherAddress);
            }
       

            mail.Subject = StringHandler.SetCommant(mailSubject);
            mail.Body = StringHandler.SetCommant(mailContent);
            mail.IsBodyHtml = true;
            var PdfType = new ContentType { MediaType = MediaTypeNames.Application.Pdf };

            var attachmentPDF = new Attachment(filePath, PdfType);
            attachmentPDF.Name = "LowCost order #" + OrderDocId;
            mail.Attachments.Add(attachmentPDF);
            sendMessage(mail);
        }

        //Send relevant mail to client
        public static void SendMailToClient(string mailContent, string mailSubject, string clientMail)
        {
            mailContent += "<br/><br/>";
            string clientEmailAddress = clientMail;
            //string clientEmailAddress = oOrderObject.oContactDetails.Email;

            List<string> sendto = new List<string>();
            sendto.Add(clientEmailAddress);

            MailHandler.SendMail(mailSubject, mailContent, sendto);

        }

        public static void SendMail(string _Subject, string _Body, List<string> _SendTo/*, MailType _MailType*/)
        {
            MailMessage mail = new MailMessage();

            foreach (var item in _SendTo)
            {
                mail.To.Add(item);
            }

            string strSubject = _Subject;
            mail.Subject = StringHandler.SetCommant(strSubject);
            mail.IsBodyHtml = true;
            string str = _Body;
            mail.Body = StringHandler.SetCommant(str);
            sendMessage(mail);
        }

        private static bool sendMessage(MailMessage mail)
        {
            string EmailUsername = System.Configuration.ConfigurationManager.AppSettings["SenderAccountEmailAddress"];
            string EmailPassword = System.Configuration.ConfigurationManager.AppSettings["SenderAccountEmailPassword"];
            int SMTPPort = ConvertToValue.ConvertToInt(System.Configuration.ConfigurationManager.AppSettings["SenderAccountSMTPPort"]);
            string SMTPHost = System.Configuration.ConfigurationManager.AppSettings["SenderAccountSMTPHost"];
            bool EnableSsl = ConvertToValue.ConvertToBool(System.Configuration.ConfigurationManager.AppSettings["SenderAccountEnableSsl"]);

            SmtpClient client = new SmtpClient();

            client.Credentials = new System.Net.NetworkCredential(EmailUsername, EmailPassword);

            string from = EmailUsername;
            mail.From = new MailAddress(from, "LowCostSupport");
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;

            client.Port = SMTPPort;
            client.Host = SMTPHost;
            client.EnableSsl = ConvertToValue.ConvertToBool(EnableSsl);
            client.Send(mail);

            //Thread oThread = new Thread(new ThreadStart(() =>
            //{
            //    try
            //    {
            //        //client.Send(mail);
            //    }
            //    catch (Exception ex)
            //    {
            //        //throw ex;
            //        //ex.WriteToLog("BasePage", "SendMail", false);
            //    } 
            //}));
            //oThread.Start();

            return true;
        }
    }

    public static class MailStrings
    {
        /// <summary>
        /// Strings For Support Mail
        /// </summary>
        public static readonly string strBookingSucceeded = "MAIL_ORDER_SUPPORT_BOOKING_SUCCEEDED";// "Booking succeed for order number ";
        public static readonly string strBookingFailed = "MAIL_ORDER_SUPPORT_BOOKING_FAILED";//"Booking failed for order number ";
        public static readonly string strBookingOnRequest = "MAIL_ORDER_SUPPORT_BOOKING_ON_REQUEST";//"Booking submmited for order number ";
        public static readonly string strBookingFailedCantFindBookingOrder = "MAIL_ORDER_SUPPORT_BOOKING_FAILED_CANT_FIND_BOOKING_ORDER";//"Booking order failed - Fatal error - cannot find the booking order number ";
        public static readonly string strBookingFailedErrorBookingWithYayaFailed ="MAIL_ORDER_SUPPORT_BOOKING_FAILED_ERROR_BOOKING_WITH_SUPPLIER_FAILED";//"Booking order failed - Fatal Error - booking action with yaya failed!!! for booking order number ";

        public static readonly string strSubjectBookingSucceeded = "MAIL_ORDER_SUPPORT_SUBJECT_BOOKING_SUCCEEDED";//"Booking succeed for order number ";
        public static readonly string strSubjectBookingFailed = "MAIL_ORDER_SUPPORT_SUBJECT_BOOKING_FAILED";//"Booking failed for order number ";
        public static readonly string strSubjectBookingOnRequest = "MAIL_ORDER_SUPPORT_SUBJECT_BOOKING_ON_REQUEST";//"Booking submmited for order number ";

        public static readonly string strSubjectBookingReceive = "MAIL_ORDER_SUBJECT_BOOKING_RECEIVE";//"There is a new oreder in the site ,the client is paying now, you will get the next email after the payment proccess. ";
        public static readonly string strBookingReceive = "MAIL_ORDER_BOOKING_RECEIVE";//"New order on site order";

        /// <summary>
        /// Strings For Client Mail
        /// </summary>
        public static readonly string strBookingSucceededClient_J4 = "MAIL_ORDER_CLIENT_BOOKING_SUCCEEDED_CLIENT_J4";//"Thank you for order at GayWayTlv, your reservation has succeeded, enjoy!";
        public static readonly string strBookingSucceededClient_J5 = "MAIL_ORDER_CLIENT_BOOKING_SUCCEEDED_CLIENT_J5";//"Thank you for order at GayWayTlv , your reservation has succeeded, we have authorized your credit card / payment method for the full amount of the reservation to ensure that it is valid. If your requst is denied or expires, you will not be charged.!";
        public static readonly string strBookingOnRequestClient_J4 = "MAIL_ORDER_CLIENT_BOOKING_ON_REQUEST_CLIENT_J4";//"Thank you, your request have been submitted, our representatives will come back to you to approve the reservation.";
        public static readonly string strBookingOnRequestClient_J5 = "MAIL_ORDER_CLIENT_BOOKING_ON_REQUEST_CLIENT_J5";//"Thank you, your request have been submitted, we have authorized your credit card / payment method for the full amount of the reservation to ensure that it is valid. If your requst is denied or expires, you will not be charged.";
        public static readonly string strBookingFailedClient = "MAIL_ORDER_CLIENT_BOOKING_FAILED_CLIENT";//"Thank you ,your booking request has failed, you will not be charge ,try again or contact us as soon as possible  ";

        public static readonly string strSubjectBookingSucceededClient = "MAIL_ORDER_CLIENT_BOOKING_SUCCEEDED";
        public static readonly string strSubjectBookingOnRequestClient = "MAIL_ORDER_CLIENT_BOOKING_ONREQUEST";

        public static readonly string strBookingOnErrorClient ="MAIL_ORDER_CLIENT_BOOKING_ON_ERROR_CLIENT";//"Thank you for order at GayWayTlv, our representatives will come back to you !";
        public static readonly string strSubjectBookingOnErrorClient = "MAIL_ORDER_CLIENT_BOOKING_SUBJECT_FOR_ERROR";

    }
}
