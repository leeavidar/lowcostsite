﻿using BL_LowCost;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;

namespace Generic
{
    public class BasePage : LangManager
    {
        public static WhiteLabel WhiteLabelObj
        {
            get
            {
                string domain = HttpContext.Current.Request.Url.Host;
                WhiteLabel oWhiteLabel = null;
                //Get the white label id from web config
                int whiteLabel = WebConfigHandler.WhiteLabelID; 
                // Get the white label object from the database
                oWhiteLabel = WhiteLabelContainer.SelectByKeysView_DocId(whiteLabel, true).Single; 
                if (oWhiteLabel == null)
                {
                    return null;
                }
                //WhiteLabel oWhiteLabel = WhiteLabelContainer.SelectByKeysView_DefaultDomain(domain, true).Single;
                //Domains oDomains = ApplicationManager.GetDomainsContainerFromApplication().FindAllContainer(R => R.Domain_UI.ToLower() == domain.ToLower()).Single;
                //if (oDomains != null)
                //{
                //    oWhiteLabel = WhiteLabelContainer.SelectByKeysView_DocId(oDomains.WhiteLabelDocId_Value, true).Single;
                //    if (oWhiteLabel == null)
                //    {
                //        HttpContext.Current.Response.Redirect(SiteDomain);
                //    }
                //}
                //else
                //{
                //    HttpContext.Current.Response.Redirect(SiteDomain);
                //}
                return oWhiteLabel;
            }
        }

        public static readonly string VirtualDirectoryPath = System.Web.Configuration.WebConfigurationManager.AppSettings["VirtualDirectoryPath"];

        public static readonly string UploadDirectoryName = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadDirectoryName"];
        
        public static readonly string XMLFileRoot = System.Web.Configuration.WebConfigurationManager.AppSettings["FileXMLRoot"];

        #region Functions
        public static int GetWhiteLabelDocID()
        {
            return WhiteLabelObj.DocId_Value;
        }
        #endregion
        

        
    }
}
