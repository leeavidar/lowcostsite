﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DL_Generic;

namespace Generic
{
    class WebConfigHandler
    {
        //TODO: Ask DUDU

        public static readonly string VirtualDirectoryPath = System.Web.Configuration.WebConfigurationManager.AppSettings["VirtualDirectoryPath"];

        public static readonly int DefaultLanguageID = ConvertToValue.ConvertToInt(System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultLanguageID"]);

        public static readonly int WhiteLabelID = ConvertToValue.ConvertToInt(System.Web.Configuration.WebConfigurationManager.AppSettings["WhiteLabel"]);
        #region Mailing Constants

        public static readonly string EmailUsername = System.Configuration.ConfigurationManager.AppSettings["EmailAddress"];
        public static readonly string EmailPassword = System.Configuration.ConfigurationManager.AppSettings["EmailPassword"];
        public static readonly int SMTPPort = ConvertToValue.ConvertToInt(System.Configuration.ConfigurationManager.AppSettings["SMTPPort"]);
        public static readonly string SMTPHost = System.Configuration.ConfigurationManager.AppSettings["SMTPHost"];
        public static readonly bool EnableSsl = ConvertToValue.ConvertToBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"]);
        public static readonly string EmailDisplayName = System.Configuration.ConfigurationManager.AppSettings["EmailDisplayName"];

        #endregion
        
    }
}
