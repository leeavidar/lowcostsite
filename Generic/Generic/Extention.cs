﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace DB_Class
{

    /// <summary>
    /// check lisst empty or null
    /// </summary>
    public static class IsNullorEmpty
    {
        public static bool List<T>(List<T> list)
        {
            return list == null || list.Count == 0;
        }
    }
    public static class MyExtensions
    {
        public static string ToCapitalFirst(this string str)
        {
            if (str == null) return null;
            return str.ToUpper()[0] + str.Substring(1);
        }
        public static string ToNoCapital(this string str)
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);
            return Regex.Replace(r.Replace(str, "_").ToLower(), "_{2,}", "_");
            //return r.Replace(str.Replace("_", string.Empty), "_").ToLower();
        }
        public static string[] SplitWithCapital(this string str)
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            return r.Replace(str, " ").Split(' ');
        }


        /// <summary>
        /// Gets a double represinting time in minutes, and convert it to a string in the form of HH:MM
        /// </summary>
        /// <param name="timeMinutes">Time in minutes</param>
        /// <returns>Returns the time in a HH:MM format</returns>
        public static string TimeMinutesToHours(double timeMinutes)
        {
            int hours = 0;
            //A variable to hold the time in a string format
            string timeinHoursMinutesFormat;
            //Every 60 minutes adding 1 hour and substracting 60 from the minutes.
            while (timeMinutes >= 60)
            {
                hours++;
                timeMinutes -= 60;
            }
            string timeMinutesStr = timeMinutes.ToString();
            if (timeMinutes<10)
            {
                // add a zero of the minutes is in 1 digit
                timeMinutesStr = "0" + timeMinutesStr;
            }
            //Buiding the time in a HH:MM format
            timeinHoursMinutesFormat = hours + ":" + timeMinutesStr;
            return timeinHoursMinutesFormat;
        }
    }


}
