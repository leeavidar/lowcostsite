﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using DL_Generic;
using LoggerManagerProject;

namespace DL_Generic
{
    public static class ProjectSettings
    {
        public static string Root { get { return "c:"; } }
        public static string Dir { get { return "CodeGenerator\\CodeGenerator\\GenericWeb\\Web"; } }
        public static string Path { get { return String.Format(@"{0}\\{1}\\", Root, Dir); } }
        public static string File_HtmlKeys { get { return Path + "FriendlyNames.xml"; } }
    }

    public class FriendlyNames
    {
        public static string FileName { get { return ProjectSettings.File_HtmlKeys.Replace(ProjectSettings.Path, "FriendlyNames file: "); } }
        public static XDocument XDoc;
        //KeyValuesType
        public static Dictionary<KeyValuesType, string> Dic_friendlyNames;

        static FriendlyNames()
        {
            try
            {
                Dic_friendlyNames = new Dictionary<KeyValuesType, string>();

                //XDoc = LoadFrindlyNamesList();

            }
            catch (Exception ex)
            {
                Console.Write(String.Format("{0}Exception at [static FriendlyNames()]: {1}", Environment.NewLine, ex.Message));

                throw;
            }
        }
        public static XDocument LoadFrindlyNamesList()
        {
            try
            {
                return XFiles.Read(ProjectSettings.File_HtmlKeys);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string GetFriendlyName(KeyValuesType oKeyValuesType)
        {
            //return "GetFriendlyName";
            throw new NotImplementedException();
        }
    }

   
}
