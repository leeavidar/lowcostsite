﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Code
{
    static class Combinations
    {
        #region Combination
        public static void GetCombinationsByLevel(out List<List<int>> listOfCombinations, int min_level, int max_level, out int totalCounter, out int counter, int baseNumer, int starter = 1)
        {
           
            listOfCombinations = new List<List<int>>();
            //baseNumer -= starter;
            List<int> arrNumber = new List<int>();
            totalCounter = 0;
            counter = 0;


            for (int i = starter; i < min_level + starter; i++)
            {
                arrNumber.Add(i);
            }

            bool isDup = false;
            for (int i = 0; (arrNumber.Count - 1) < max_level; i++)
            {
                if (arrNumber[arrNumber.Count - 1] > baseNumer + 1)
                {
                    break;
                }
                if (!isDup)
                {
                    int[] list_temp = new int[arrNumber.Count];
                    arrNumber.CopyTo(list_temp);
                    listOfCombinations.Add(list_temp.ToList<int>());
                    Console.WriteLine("+{0}", string.Join("", ConvertToBaseString(arrNumber)));
                    counter++;
                }
                else
                {
                    //Console.WriteLine("-{0}", string.Join("", arrNumber));
                }
                isDup = false;
                totalCounter++;
                bool isAdd = false;
                int digit = -1;
                for (int j = arrNumber.Count - 1; j >= 0; j--)
                {
                    isDup = false;
                    digit = arrNumber[j];
                    //also can use the != statment
                    if ((digit) < baseNumer)
                    {
                        //inc
                        arrNumber[j]++;
                        isAdd = true;
                        for (int k = (j + 1); k < arrNumber.Count; k++)
                        {
                            if ((arrNumber[k - 1] + 1) <= baseNumer)
                            {
                                //set index 
                                arrNumber[k] = arrNumber[k - 1] + 1;

                                //check if there is a duplicate number for this digit
                                isDup = arrNumber.FindAll(R => R == arrNumber[k]).Count != 1 || isDup;
                                isDup = isDup || (string.Join("", arrNumber).Substring(0, j).ToList().FindAll(R => R < arrNumber[k]).Count > 1);

                            }
                            else
                            {
                                if (j == 0)
                                    isAdd = false;
                                break;
                            }
                        }
                        //check if there is a duplicate number for this digit
                        isDup = arrNumber.FindAll(R => R == arrNumber[j]).Count != 1 || isDup;
                        isDup = isDup || (string.Join("", arrNumber).Substring(0, j).ToList().FindAll(R => R < arrNumber[j]).Count > 1);

                        if (!isDup && isAdd)
                        {
                            break;
                        }
                        else
                        {
                            if (j == 0)
                                isAdd = false;
                        }
                    }
                    else
                    {

                    }
                }
                //if we didnt inc the digit - digit is biger then the base
                if (!isAdd)
                {
                    isDup = false;
                    //set array to start
                    for (int j = arrNumber.Count - 1; j >= 0; j--)
                    {
                        //next 
                        arrNumber[j] = j + starter + 1;
                        //arrNumber[j] = starter;
                    }

                    //same like break when the min and max are equal- one level!!!
                    arrNumber.Insert(0, starter);
                }
            }
            //Console.WriteLine("actual combination {0} of {1}", counter,totalCounter);
        }
        public static List<char> ConvertToBaseString(List<int> list_num)
        {
            List<char> list = new List<char>();
            foreach (int num in list_num)
            {
                list.Add(ConvertToBaseString(num));
            }
            return list;
        }
        public static char ConvertToBaseString(int num)
        {
                return (Convert.ToChar(Convert.ToInt32('A') + num));
        }
        
        #endregion     

    }
}
