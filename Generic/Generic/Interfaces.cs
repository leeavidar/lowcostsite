﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DL_Generic
{
    #region Interfaces
    public interface ContainerAble<CONTAINER, ITEM>
    {
        ITEM EmptyObj();
        #region Add
        void Add(List<ITEM> itemList);
        void Add(CONTAINER itemContainer);
        void Add(ITEM oItem);
        #endregion
        #region Find
        List<ITEM> FindAllList(Predicate<ITEM> match);
        CONTAINER FindAllContainer(Predicate<ITEM> match);
        ITEM Find(ITEM oItem);
        #endregion
        #region Lists
        List<ITEM> ItemList { get; set; }
        int Count { get; }
        List<ITEM> DataSource { get; }
        ITEM First { get; }
        ITEM Single { get; }
        List<ITEM> DataSourceClone { get; }
        #endregion
        #region Indexing
        ITEM this[int index] { get; set; }
        void RemoveAt(int index);
        #endregion
        #region Action Default
        CONTAINER Action(DB_Actions dB_Actions);
        #endregion
        #region Clone And Convert
        //CONTAINER GetContainerClone();
        CONTAINER Clone();
        //CONTAINER Convert(List<ITEM> list);
        #endregion
    }
    public interface ContainerItemAble<ITEM> //: IComparable<ITEM>
    {
        #region Keys Actions

        bool EqualsWeek(ITEM itemCompareWith);
        bool EqualsStrong(ITEM itemCompareWith);
        bool Equals(ITEM itemCompareWith);
        object GetKey(KeyValuesType oKeyValuesType);
        void SetKey(KeyValuesType oKeyValuesType, object value);
        void SetKey(KeyValuesType oKeyValuesType, bool value);

        #endregion
        #region DB
        int Action(DB_Actions dB_Actions);
        ITEM Clone();
        //List<ITEM> Convert(DataSet ds);
        #endregion

        int _doc_id { get; set; }
        bool _is_active { get; set; }
        bool _is_deleted { get; set; }
        //public DateTime _date_created { get; set; }
        //int? DocId { get; set; }
        //bool? IsActive { get; set; }
        //bool? IsDeleted { get; set; }

        
    }
    #endregion
}
