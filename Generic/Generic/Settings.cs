﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace DL_Generic
{
    public static class Settings
    {
        public static readonly bool IsStaticTranslated = ConvertToValue.ConvertToBool(ConfigurationSettings.AppSettings["IsStaticTranslated"]);

        public static string ConnectionString
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionString"];
            }

        }

        private static int langId = 1;//ConvertToValue.ConvertToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultLanguage"]);
        public static int CurrentLanguage
        {
            get
            {
                return langId;
            }
            set
            {
                langId = value;
            }

        }

        private static bool isManualLang = ConvertToValue.ConvertToBool(System.Web.Configuration.WebConfigurationManager.AppSettings["IsManualLang"]);//ConvertToValue.ConvertToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultLanguage"]);
        public static bool IsManualLang
        {
            get
            {
                return isManualLang;
            }
            set
            {
                isManualLang = value;
            }

        }

    }
}
