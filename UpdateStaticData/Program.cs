﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.OleDb;
using Generic;
using System.IO;
using System.Xml.Serialization;
using DL_Generic;
using DL_LowCost;
using BL_LowCost;
namespace UpdateStaticData
{
    class Program
    {
        static void Main(string[] args)
        {
            // object countriesList = System.IO.File.ReadAllText(@"C:\lowcostsite\StaticData\countrieslist.xml");
            // object citiesList = System.IO.File.ReadAllText(@"C:\lowcostsite\StaticData\CitiesListNew.xml");
            // object airportsList = System.IO.File.ReadAllText(@"C:\lowcostsite\StaticData\airportsList.xml");
          //  object airlinesList = System.IO.File.ReadAllText(@"C:\lowcostsite\UpdateStaticData\StaticData\Airlines\AirlinesList.xml");

            // when there is nothing in the translator table, this will not work (there will be no ids for the translator items added)
            // to solve this, first add any row to the translator table and than call this method.

            // CreateCountries(countriesList);
          //   UpdateCountries(countriesList);

            // CreateCities(citiesList);
            //  UpdateCities(citiesList);
            // this may take a long time (about 40 seconds)

            // CreateAirports(airportsList);

        

         //   CreateAirlines(airlinesList);

        }

        private static void CreateCountries(object countriesList)
        {
            //Settings.IsManualLang = true;
            CountriesItemsTable countriesTable = GetData<CountriesItemsTable>(countriesList);
            int count = 0;
            foreach (CountriesItemRow row in countriesTable.CountriesItemRow)
            {
                Settings.CurrentLanguage = 1;
                Country Country = new Country();
                Country.Name = row.HebrewName;
                Country.CountryCode = row.code;
                int id = Country.Action(DB_Actions.Insert);

                Settings.CurrentLanguage = 2;
                Country.Name = row.EnglishName;
                Country.Action(DB_Actions.Update);
                count++;
            }
            Console.WriteLine("Inserted {0} countries", count);
            Console.Read();
        }
        private static void UpdateCountries(object countriesList)
        {
            CountriesItemsTable countriesTable = GetData<CountriesItemsTable>(countriesList);
            int count = 0;
            CountryContainer oCountryContainer = CountryContainer.SelectAllCountrys(null);
            Country oCountry;
            foreach (CountriesItemRow row in countriesTable.CountriesItemRow)
            {
                oCountry = oCountryContainer.FindFirstOrDefault(country => country.CountryCode_UI == row.code);
                if (oCountry != null)
                {
                    oCountry.Name = row.HebrewName;
                    oCountry.EnglishName = row.EnglishName;
                    oCountry.CountryCode = row.code;
                    oCountry.Action(DB_Actions.Update);
                    count++;
                }
            }
            Console.WriteLine("Updated {0} countries !!! ", count);
            Console.Read();
        }

        private static void CreateCities(object citiesList)
        {
            CitiesItemsTable citiesTable = GetData<CitiesItemsTable>(citiesList);
            int count = 0;
            foreach (CitiesItemRow row in citiesTable.CitiesItemRow)
            {
                Settings.CurrentLanguage = 1;
                City oCity = new City();
                oCity.Name = row.HebrewName;
                oCity.CountryCode = row.countryCode;
                oCity.IataCode = row.IATA;
                int id = oCity.Action(DB_Actions.Insert);

                Settings.CurrentLanguage = 2;
                oCity.Name = row.EnglishName;
                oCity.Action(DB_Actions.Update);
                count++;
            }
            Console.WriteLine("Inserted {0} Cities", count);
            Console.Read();
        }
        private static void UpdateCities(object citiesList)
        {
            CitiesItemsTable citiesTable = GetData<CitiesItemsTable>(citiesList);
            int count = 0;
            CityContainer oCityContainer = CityContainer.SelectAllCitys(null);
            City oCity;
            foreach (CitiesItemRow row in citiesTable.CitiesItemRow)
            {
                oCity = oCityContainer.FindFirstOrDefault(city => city.IataCode_UI == row.IATA);
                if (oCity != null)
                {
                    oCity.Name = row.HebrewName;
                    oCity.EnglishName = row.EnglishName;
                    oCity.CountryCode = row.countryCode;
                    oCity.Action(DB_Actions.Update);
                    count++;
                }

            }
            Console.WriteLine("Updated {0} Cities !!! ", count);
            Console.Read();
        }

        private static void CreateAirports(object airportsList)
        {
            AirportsItemsTable airportTable = GetData<AirportsItemsTable>(airportsList);
            int count = 0;
            foreach (AirportsItemRow row in airportTable.AirportsItemRow)
            {
                Settings.CurrentLanguage = 1;
                Airport oAirport = new Airport();
                oAirport.NameByLang = row.HebrewName;
                oAirport.CityIataCode = row.CITYCODE;
                oAirport.IataCode = row.CODE;
                oAirport.EnglishName = row.Name;

                int id = oAirport.Action(DB_Actions.Insert);

                count++;
            }
            Console.WriteLine("Inserted {0} Airports", count);
            Console.Read();
        }

        private static void CreateAirlines(object airlinesList)
        {
            AirlineList airlinesTable = GetData<AirlineList>(airlinesList);
            int count = 0;
            foreach (AirlineListAirline row in airlinesTable.Airline)
            {
                var oAirline = new Airline();
                oAirline.Name = row.AirlineNameList.FirstOrDefault(name => name.LanguageCode == "en").Name;
                oAirline.IataCode = row.IataCode;
                oAirline.IsActive = true;
                oAirline.IsDeleted = false;

                // if the airline isn't already in the DB, add it
                if (AirlineContainer.SelectByKeysView_IataCode(row.IataCode, null).Count == 0)
                {
                    int id = oAirline.Action(DB_Actions.Insert);
                    count++;
                }
                else
                {
                    // write a log
                    LoggerManagerProject.LoggerManager.LoggerGeneric.WriteInfo(string.Format("STATIC DATA PROJECT: The airline {0} ({1}) was already in the database", oAirline.Name, oAirline.IataCode = row.IataCode));
                }

            }
            Console.WriteLine("Inserted {0} airlines", count);
            Console.Read();
        }
       

        public static T GetData<T>(object xmlRes)
           where T : class
        {
            if (xmlRes == null) { return null; }

            T deserializedRes = null;
            if (xmlRes is string)
            {
                deserializedRes = (xmlRes as string).ToDeserialize<T>();
            }
            else
            {
                deserializedRes = null;
            }
            return deserializedRes;
        }


    }
}
