﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerManagerProject;
namespace TimeZonesUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ConfigurationZone.ConfigurationZone service = new ConfigurationZone.ConfigurationZone())
            {
                /// DO NOT DELETE !!!!
                /// --------------------------------------------------------------------------------------------
                /// Get the api key from the app.config
                /// string googleApiKey = System.Configuration.ConfigurationSettings.AppSettings["GoogleApiKey"];
                /// 
                /// ADD THIS KEY TO THE CONFIG FILE:
                ///    under <appSettings>
                ///    <add key="GoogleApiKey" value="AIzaSyCw19Lj6Ui8WyAqxAvZR-bzCnpkXgtljg4"/>


                string googleApiKey = "AIzaSyCw19Lj6Ui8WyAqxAvZR-bzCnpkXgtljg4";

          
                bool ok=false;
                try
                {
                    // update the local time differences  for all destination pages in the database
                    ok = service.UpdateLocalTimesForDestinations(googleApiKey);
                   //  give an appropriate message
                    if (ok)
                    {
                        LoggerManager.LoggerTimeZoneUpdate.WriteInfo(string.Format("The Clocks of all destination pages were updated successfully."));
                    }
                    else
                    {
                        LoggerManager.LoggerTimeZoneUpdate.WriteError(string.Format("Error: The Clocks were not updated."));
                    }
                    Console.WriteLine("LowCost Flights -  Destination pages Clock Updater\n==================================================\nThis application updates the clocks of all destination pages.\nIt should run daily.\n\nResult:");
                    Console.WriteLine(ok ? "The Clocks of all destination pages were updated successfully. " : "Error: Not updated");

                }
                catch (Exception)
                {
                   // ERROR 
                    Console.WriteLine("Some Error occurred while trying to update.");
                }
            }
        }
    }
}
