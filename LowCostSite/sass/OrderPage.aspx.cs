using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using LowCostSite.FlightsBookingService;

using LowCostSite.sass.Controls;

namespace LowCostSite.sass
{
    public partial class OrderPage : BasePage_UI
    {
        #region Session Private fields
        private SelectedItinerary _oSelectedItinerary;
        private OrderObject _oOrderObject;
        private SearchParams _oSearchParams;
        #endregion

        #region Sessions Methodes

        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or the selected itinerary from the search is not in the session
            if (SessionManager.GetSession(out _oSelectedItinerary))
            {
                // Get the search params for data about the numbers of passengers (not always provided by the booking service)
                if (!isPostBack)
                {
                    // on the first load, get thwe search params (they will be stored as part of the OrderObject)
                    SessionManager.GetSession(out _oSearchParams);
                }
                else
                {
                    SessionManager.GetSession(out _oOrderObject);
                }
            }
            else { /*the session object should already be in the session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            SessionManager.SetSession(oOrderObject);
            SessionManager.SetSession(oSelectedItinerary);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }
        #endregion
    }

    public partial class OrderPage : BasePage_UI
    {
        #region Properties
        private Dictionary<FlightsSearchService.ECustomerType, int> passengerList;
        public string LBL_SeatTotalPrice { get { return lbl_SeatsTotalPrice.Text; } set { lbl_SeatsTotalPrice.Text = value; } }
        public SelectedItinerary oSelectedItinerary { get { return _oSelectedItinerary; } set { _oSelectedItinerary = value; } }
        public SearchParams oSearchParams { get { return _oSearchParams; } set { _oSearchParams = value; } }
        public OrderObject oOrderObject { get { return _oOrderObject; } set { _oOrderObject = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                oOrderObject = new OrderObject();
                // If there is a selected itinerary object generated in the uc_FlightGroup in the search page
                if (oSelectedItinerary != null)
                {
                    // Save the search parameters in the order object
                    oOrderObject.SearchParams = oSearchParams;
                    // Set the raw selected itinerary in the order Object
                    oOrderObject.SelectedItinerarySearch = oSelectedItinerary;

                    // Log the selected itinerary object to get the prices, markups and handling fee
                    string selectedItinerary = oSelectedItinerary.SerializeToString();
                    LoggerManagerProject.LoggerManager.Logger.WriteInfo(string.Format(
                    @"SelectedItineraryObject:
                    =========================================================
                    {0}
                    =========================================================
                    ", selectedItinerary));

                    // Set the new selected object (created from the original itinerary) in the order Object
                    oOrderObject.SelectedItinerary.LoadFromObject(this.oSelectedItinerary);

                    // gets the selected itinerary details for the selected flight, from the Flights Booking Service
                    // This object contains the data needed by the service to book the flight (incuding optional data, and prices for each addition)
                    oOrderObject.PricedItineraryDetails = GetPricedItineraryDetails();
                }
            }

            // After the PricedItineraryDetails are created
            if (oOrderObject != null && oOrderObject.PricedItineraryDetails != null)
            {
                // IF there are errors in the priced itinerary object
                if (oOrderObject.PricedItineraryDetails.Errors != null && oOrderObject.PricedItineraryDetails.Errors.DataSource.Count() > 0)
                {
                    // show the itinerary error div ("Error.. Plaeses choose another flight...")
                    div_ItineraryError.Attributes["class"] = div_DuplicateBooking.Attributes["class"].Replace("display-none", "highlight rtl display_inline_block full-width");
                    LoggerManagerProject.LoggerManager.Logger.WriteError(string.Format("Error on OrderPage. The object 'oSessionItineraryDetails' has errors. \n Message: {0}", Server.HtmlDecode(oOrderObject.PricedItineraryDetails.Errors.SerializeToString())));
                    HidePassengersDetailsForm();
                }
                else
                {
                    // THERE ARE NO ERRORS:
                    SetPage();
                }

                if (!IsPostBack)
                {
                    // First time the page loads, save a hash code
                    // save the hash code of the selected itinerary object, to identify the current order
                    KeepHashCode();
                }
                else
                {
                    // on post backs, verify that the session is for the same order
                    CheckHashCode();
                }
            }
            else
            {
                // there is no PricedItineraryDetails object - redirect to the homepage
                LoggerManagerProject.LoggerManager.Logger.WriteError("There is no PricedItineraryDetails object in the order page. Redirected to homepage.");
                Response.Redirect(StaticStrings.path_IndexPage);
            }
        }

        #region Event Handlers

        /// <summary>
        /// An event handler that fires after the user fills all the passsengers and contact details, and clicks the continue button    
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Book_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                //check if the user chaacked the terms
                if (!cb_TermsOfServiceApproval.Checked)
                {
                    // one of the passengers ages is not valid (at the time of the flight)
                    return;
                }
                // Build the request for the booking, and store it in the order object
                oOrderObject.BookingParameters = BuildRequest();
                if (oOrderObject.BookingParameters == null || !ValidatePassengersAges(oOrderObject.BookingParameters))
                {
                    // one of the passengers ages is not valid (at the time of the flight)
                    return;
                }
                else
                {
                    CheckHashCode();
                    // TODO: remove this  - don't make changes in the original object. create a new updated object if needed
                    oOrderObject.BookingParameters.ExpectedTotalPrice = ConvertToValue.ConvertToDouble(oOrderObject.NetPriceAndCurrency.Price);
                    oOrderObject.BookingParameters.ExpectedTotalPriceCurrency = oOrderObject.NetPriceAndCurrency.Currency;

                    Orders dbOrder = CreateOrderInDB();
                    // Creating the order, and getting the new DocId and date created (needed to send to the payment frame - to find the serialized order file)
                    oOrderObject.OrderDocId = dbOrder.DocId_Value;
                    oOrderObject.OrderDateCreated = dbOrder.DateCreated_UI;

                    WriteLogOnDB(oOrderObject.OrderDocId, oOrderObject.BookingParameters, EnumHandler.OrderStage.BookFlight, EnumHandler.OrderXmlType.Request, EnumHandler.OrderXmlStatus.None);

                    using (var service = new LowCostSite.FlightsBookingService.FlightsBookingService())
                    {
                        oOrderObject.FlightBookDetails = service.BookFlightReservation(oSelectedItinerary.LoginToken, oOrderObject.BookingParameters, oSelectedItinerary.SearchToken);
                        if (oOrderObject.FlightBookDetails.BookingDetails.Errors != null && oOrderObject.FlightBookDetails.BookingDetails.Errors.DataSource.Count() > 0)
                        {
                            LoggerManagerProject.LoggerManager.Logger.WriteError("Error while trying to book flight (service.BookFlightReservation()). Error message from Service: " + oOrderObject.FlightBookDetails.BookingDetails.Errors.DataSource.SerializeToString());
                        }
                        if (oOrderObject.FlightBookDetails != null)
                        {
                            // Check for errors in the response
                            if (oOrderObject.FlightBookDetails.BookingDetails.Errors != null && oOrderObject.FlightBookDetails.BookingDetails.Errors.DataSource.Count() > 0)
                            {
                                // Handle each of the errors (messages, update price etc.)
                                HandleErrors(oOrderObject.FlightBookDetails);


                                // Log the selected itinerary object to get the prices, markups and handling fee
                                string flightBookDetails = oOrderObject.FlightBookDetails.SerializeToString();
                                LoggerManagerProject.LoggerManager.Logger.WriteInfo(string.Format(
                                @"flightBookDetails in OrderObject (after price change if happened):
                                =========================================================
                                {0}
                                =========================================================
                                ", flightBookDetails));
                            }
                            else
                            {
                                // NO ERRORS 
                                // continue to the ticket issue service
                                WriteLogOnDB(oOrderObject.OrderDocId, oOrderObject.FlightBookDetails, EnumHandler.OrderStage.BookFlight, EnumHandler.OrderXmlType.Response, EnumHandler.OrderXmlStatus.Succeed);

                                // Log the selected itinerary object to get the prices, markups and handling fee
                                string flightBookDetails = oOrderObject.FlightBookDetails.SerializeToString();
                                LoggerManagerProject.LoggerManager.Logger.WriteInfo(string.Format(
                                @"flightBookDetails in OrderObject (NO price change error):
                                =========================================================
                                {0}
                                =========================================================
                                ", flightBookDetails));


                                OrderContinue();
                            }
                        }
                    }
                }
            }
            else
            {
                //not valid
            }
        }

        /// <summary>
        /// A method that continues the order booking process, after the user confirms the price change.
        /// </summary>
        protected void btn_ContinueOrder_Click(object sender, EventArgs e)
        {
            OrderContinue();
        }

        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(StaticStrings.path_FlightsResults);
        }

        /// <summary>
        /// When the user clicks on "back to order page" in an error for double booking
        /// </summary>
        protected void btn_BackToOrderPage_Click(object sender, EventArgs e)
        {
            div_DuplicateBooking.Attributes.Add("class", "display-none");
            div_generalDiv.Attributes["class"].Replace("display-none", "");
        }

        #endregion

        #region Helper methods


        /// <summary>
        /// A method that gets the selected itinerary details for the selected flight, from the Flights Booking Service.
        /// </summary>
        private PricedItineraryDetails GetPricedItineraryDetails()
        {
            if (oSelectedItinerary == null)
            {
                return null;
            }
            string outwardFlightId = oSelectedItinerary.SelectedFlights.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightId;
            string returnFlightId = "";

            if (oSelectedItinerary.SelectedFlights.GroupList.DataSource != null)
            {
                if (oSelectedItinerary.SelectedFlights.GroupList.DataSource.FirstOrDefault().ReturnFlights != null)
                {
                    if (oSelectedItinerary.SelectedFlights.GroupList.DataSource.FirstOrDefault().ReturnFlights.DataSource.FirstOrDefault() != null && oSelectedItinerary.SelectedFlights.GroupList.DataSource.FirstOrDefault().ReturnFlights.DataSource.Count() > 0)
                    {
                        returnFlightId = oSelectedItinerary.SelectedFlights.GroupList.DataSource.FirstOrDefault().ReturnFlights.DataSource.FirstOrDefault().FlightId;
                    }
                }
            }

            using (var service = new LowCostSite.FlightsBookingService.FlightsBookingService())
            {
                FlightsBookingService.ItineraryDetailsParameters oItineraryDetailsParameters = new ItineraryDetailsParameters()
                {
                    GeneralParameters = new FlightsBookingService.GeneralParameters(),
                    ID = oSelectedItinerary.SelectedFlights.ID,
                    OutwardFlightId = outwardFlightId,
                    ReturnFlightId = returnFlightId
                };
                return service.GetFlightBookDetails(oSelectedItinerary.LoginToken, oItineraryDetailsParameters, oSelectedItinerary.SearchToken);
            }
        }

        /// <summary>
        /// Set all page parts (side indo, passenger info fields and get text for everything.
        /// </summary>
        public void SetPage()
        {
            SetPageInfo();
            // set the user controls with inputs for the passenger details
            SetPassengers();
            // Fill all the placeholders and text attributes of the elemets that use GetText()
            FillGetTexts();
        }

        /// <summary>
        /// A method that sets all the inforamation data that is displayed on the page (the side panel with flight details, the prices etc.)
        /// </summary>
        private void SetPageInfo()
        {
            // If there is no selected itinerary transferred from a previous page, redirect to the search results page.
            if (oSelectedItinerary == null)
            {
                Response.Redirect(StaticStrings.path_FlightsResults);
            }
            else
            {
                #region Details summary block (top right box)
                // Origin and Destinations labels
                lbl_Origin.InnerText = Extensions.GetLocationName(oSelectedItinerary.SelectedFlights.Origin);
                lbl_Destination.InnerText = Extensions.GetLocationName(oSelectedItinerary.SelectedFlights.Destination);
                // Outward Date and Return Date (with the day of week)
                #region Outward Date and Return Date (with the day of week)
                lbl_OutwardDate.InnerText = Extensions.GetDateWithDayName(oSelectedItinerary.SelectedFlights.Summary.OutwardDate);
                if (oSelectedItinerary.SelectedFlights.ReturnDate == ConvertToValue.DateTimeEmptyValue)
                {
                    FlightDetails_ReturnDate.Visible = false;
                }
                else
                {
                    lbl_ReturnDate.InnerText = Extensions.GetDateWithDayName(oSelectedItinerary.SelectedFlights.Summary.ReturnDate);
                }
                #endregion

                // Create an empty prices container and empty segments containers
                LowCostSite.FlightsSearchService.Pricing oPriceContainerOutward = null;
                LowCostSite.FlightsSearchService.Pricing oPriceContainerReturn = null;
                LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerOutward = null;
                LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerReturn = null;


                FlightsSearchService.FlightContainer outwardFlightContainer = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().OutwardFlights;
                FlightsSearchService.FlightContainer returnFlightContainer = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().ReturnFlights;

                oPriceContainerOutward = outwardFlightContainer.DataSource.FirstOrDefault().Price;
                oSegmentLegContainerOutward = outwardFlightContainer.DataSource.First().FlightSegments;

                // If there are return flight segments
                if (returnFlightContainer != null && returnFlightContainer.DataSource.FirstOrDefault() != null)
                {
                    // Get the price object
                    oPriceContainerReturn = returnFlightContainer.DataSource.FirstOrDefault().Price;
                    oSegmentLegContainerReturn = returnFlightContainer.DataSource.First().FlightSegments;
                }

                // Get the handling fee from the flight group (for a single passenger)
                var handlingFee = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().Price.HandlingFee;

                // Fill the prices breakdown for adults, children, and infants, and the handling fee
                FillPrices(handlingFee, oPriceContainerOutward, oPriceContainerReturn);
                // Fill the boxes of descriptions for the outward and return flights (general flight info and segemnts ).
                FillFlightsDetails(oSegmentLegContainerOutward, oSegmentLegContainerReturn);


                #endregion

                FillConditions();

                #region Set phone DDLs
                if (!IsPostBack)
                {
                    var prefixes = PhonePrefixContainer.SelectAllPhonePrefixs(true).DataSource;
                    ddl_PhonePrefix.DataSource = prefixes;
                    ddl_PhonePrefix.DataTextField = "PrefixNumber_UI";
                    ddl_PhonePrefix.DataValueField = "PrefixNumber_UI";
                    ddl_PhonePrefix.DataBind();

                    ddl_PhonePrefixSecond.DataSource = prefixes;
                    ddl_PhonePrefixSecond.DataTextField = "PrefixNumber_UI";
                    ddl_PhonePrefixSecond.DataValueField = "PrefixNumber_UI";
                    ddl_PhonePrefixSecond.DataBind();
                }
                #endregion
            }
        }

        /// <summary>
        /// Handle each of the errors (messages, update price etc.)
        /// </summary>
        /// <param name="oFlightBookDetails">The response object of the booking method (in the booking service)</param>
        private void HandleErrors(FlightBookedDetails oFlightBookDetails)
        {
            LowCostSite.FlightsBookingService.ErrorItem oError = oFlightBookDetails.BookingDetails.Errors.DataSource.FirstOrDefault();
            if (oError.Code.Equals(StaticStrings.PriceError))
            {
                // PRICE CHANGED
                // update prices with the new prices returned from the service
                UpdatePrice(oFlightBookDetails);

                // Show the alert for price changed
                ShowPriceError(new OrderPrice(oOrderObject.TotalPriceWithHandlingFinal, oOrderObject.NetPriceAndCurrencyFinal.Currency));
            }
            else if (oError.Code.Equals(StaticStrings.PriceCurrencyChangedError))
            {
                ShowCurrencyError();
                LoggerManagerProject.LoggerManager.Logger.WriteError(string.Format("The currency had changed in the booking proccess. Order ID: {0}", oOrderObject.OrderDocId));
                WriteLogOnDB(oOrderObject.OrderDocId, oFlightBookDetails, EnumHandler.OrderStage.BookFlight, EnumHandler.OrderXmlType.Response, EnumHandler.OrderXmlStatus.Failed);
            }
            else if (oError.Code.Equals(StaticStrings.DuplicateBooking))
            {
                // DUPLICATE BOOKING (the passenger is trying to order the same flights with the same names
                div_DuplicateBooking.Attributes["class"] = div_DuplicateBooking.Attributes["class"].Replace("display-none", "highlight rtl display_inline_block full-width");
                HidePassengersDetailsForm();
                WriteLogOnDB(oOrderObject.OrderDocId, oFlightBookDetails, EnumHandler.OrderStage.BookFlight, EnumHandler.OrderXmlType.Response, EnumHandler.OrderXmlStatus.Duplicate);
                UpdateOrderStatus(oOrderObject.OrderDocId, EnumHandler.OrderStatus.DuplicateBooking);
            }
            else
            {
                // OTHER ERRORS
                div_generalError.Attributes["class"] = div_DuplicateBooking.Attributes["class"].Replace("display-none", "highlight rtl display_inline_block full-width");
                HidePassengersDetailsForm();
                WriteLogOnDB(oOrderObject.OrderDocId, oFlightBookDetails, EnumHandler.OrderStage.BookFlight, EnumHandler.OrderXmlType.Response, EnumHandler.OrderXmlStatus.Failed);
                UpdateOrderStatus(oOrderObject.OrderDocId, EnumHandler.OrderStatus.Failed);

                #region Mails
                int mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                string strMailContentForSupport = string.Format(@"
                <h3>There was error in the order process </h3>
                <br /><br />
                <b>The customer was informed that an agent will call him back to complete the order.</b><br /><br />
                Customer contact information:<br />
                ====================== <br />
                <b>Name:</b> {0}<br />
                <b>Email Address:</b> {1}<br />
                <b>Phone Number:</b> {2}<br />
                ====================== <br /><br /> 
                <b>Order ID: {3}</b><br />",
                ddl_ContactTitle.SelectedValue + " " + txt_ContactFirstName.Text + " " + txt_ContactLastName.Text,
                txt_Email.Text, 
                ddl_PhonePrefix.SelectedValue + "-" + txt_MainPhone.Text, 
                oOrderObject.OrderDocId);

                string strMailSubjectForSupport = string.Format("{0}: Order Id:{1}", GetText(MailStrings.strSubjectBookingOnErrorClient), oOrderObject.OrderDocId);

                //Send email for support
                SendOrderMail(null, null, null, strMailContentForSupport, strMailSubjectForSupport, mailSendOptions);
                #endregion


            }

        }

        /// <summary>
        /// When there are no more errors in the booking response (no price changed or duplicate booking),
        /// this method is used to create an order object and save everything in an xml file to be later used in the payment process.
        /// It redirects to the payment.
        /// </summary>
        /// <param name="orderId"></param>
        private void OrderContinue()
        {
            //Create object for a order details and save to XML and for session.
            UpdateOrderObjectAndCreateXml();
            // Save the order in the database, and create an XML file to use later to retreive the data.
            SaveReservationToDB();

            #region Add log with the order id that was created
            if (DebugMode)
            {
                LoggerManagerProject.LoggerManager.Logger.WriteInfo("Click To PurchseTrip DebugMode Order {0} End", oOrderObject.OrderDocId);
            }
            else
            {
                LoggerManagerProject.LoggerManager.Logger.WriteInfo("Click To PurchseTrip  Order {0} End", oOrderObject.OrderDocId);
            }
            #endregion

            #region Mails
            int mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
            string strMailContentForSupport = string.Format("{1}:{2}{0} Summary of order details: {0} OderId:{2}{0} Name:{3}{0} Email:{4}{0} Phone:{5}", Environment.NewLine, GetText(MailStrings.strBookingReceive), oOrderObject.OrderDocId, ddl_ContactTitle.SelectedValue + " " + txt_ContactFirstName.Text + " " + txt_ContactLastName.Text, txt_Email.Text, ddl_PhonePrefix.SelectedValue + "-" + txt_MainPhone.Text);
            string strMailSubjectForSupport = string.Format("{0}:{1}", GetText(MailStrings.strSubjectBookingReceive), oOrderObject.OrderDocId);

            //Send email for support
            SendOrderMail(null, null, null, strMailContentForSupport, strMailSubjectForSupport, mailSendOptions);
            #endregion

            // Add script that removes the "please wait" message
            AddScript("$('#pleaseWaitModal').modal('hide')");

            //Go To Payment
            Response.Redirect(string.Format("{0}?dateCreated={1}&orderDocId={2}&wlID={3}", UrlPayment, oOrderObject.OrderDateCreated, oOrderObject.OrderDocId,1), false);
        }

        /// <summary>
        /// Create the order in the database (StatusPayment as Pending)
        /// </summary>
        /// <returns>The new order's DocId</returns>
        private Orders CreateOrderInDB()
        {
            Guid DBToken = Guid.NewGuid();
            oOrderObject.OrderToken = DBToken.ToString();
            Orders thisOrder = new Orders();
            thisOrder.OrderToken = DBToken.ToString();
            thisOrder.StatusPayment = (int)EnumHandler.PaymentStatus.Pending;
            thisOrder.Action(DB_Actions.Insert);
            return thisOrder;
        }

        /// <summary>
        /// A method that updates the status of an order by docId.
        /// </summary>
        /// <param name="orderId">The docId of the order you want to update</param>
        /// <param name="orderStatus">The new status you want to update to</param>
        protected void UpdateOrderStatus(int orderId, EnumHandler.OrderStatus orderStatus)
        {
            Orders Order = OrdersContainer.SelectByKeysView_WhiteLabelDocId_DocId(BasePage_UI.GetWhiteLabelDocID(), orderId, true).Single;
            Order.OrderStatus = (int)orderStatus;
            Order.Action(DB_Actions.Update);
        }

        /// <summary>
        /// A method that check if all the passengers ages are valid.
        /// the ages are already calculated as the age in the time of the flight.
        /// </summary>
        /// <param name="oBookingParameters"></param>
        /// <returns></returns>
        protected bool ValidatePassengersAges(BookingParameters oBookingParameters)
        {
            bool agesValid = true;
            // Hide all age error if there are any
            HideAgeErrors();
            foreach (var psngr in oBookingParameters.Passengers.DataSource)
            {
                switch (psngr.PassengerType)
                {
                    case LowCostSite.FlightsBookingService.ECustomerType.ADT:
                        break;
                    case LowCostSite.FlightsBookingService.ECustomerType.CNN:
                        if (psngr.Age > ConfigurationHandler.MaxChildAge || psngr.Age <= ConfigurationHandler.MaxInfantAge)
                        {
                            //Error The child is older for child age
                            ShowAgeError(true);
                            agesValid = false;
                        }
                        break;
                    case LowCostSite.FlightsBookingService.ECustomerType.INF:
                        if (psngr.Age > ConfigurationHandler.MaxInfantAge)
                        {
                            //Error The infant is older for infant age
                            ShowAgeError(false);
                            agesValid = false;
                        }
                        break;
                    default:
                        break;
                }
            }
            return agesValid;
        }

        /// <summary>
        /// A method that validates the phone number.
        /// </summary>
        protected void Val_PrimaryPhone_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = false;
                int NumChk;
                if (Int32.TryParse(args.Value, out NumChk))
                {
                    int PhoneWithAreaCodeLength = ddl_PhonePrefix.SelectedValue.Length + args.Value.Length;
                    // check if the length of the phone number including the area code (03, 050 etc...) is between 9 - 10 digits (when sending the number, we will add the country code, which will make it longer)
                    if (PhoneWithAreaCodeLength <= 10 && PhoneWithAreaCodeLength >= 9)
                    {
                        args.IsValid = true;
                    }
                }
            }
            catch
            {
                args.IsValid = false;
            }
        }

        /// <summary>
        ///  A method that sets the hidden field on the page to store the hash code of the _oSessionItineraryDetails object
        /// </summary>
        public void KeepHashCode()
        {
            int orderObjectHashCode = GetHashCode();
            hf_IdentityCode.Value = orderObjectHashCode.ToString();
        }

        /// <summary>
        /// A methos that gets the hash code of the session itinerarry details object.
        /// </summary>
        /// <returns>The hash code of the _oSessionItineraryDetails object, or -1 if there is no object in the session.</returns>
        public override int GetHashCode()
        {
            if (oOrderObject.PricedItineraryDetails != null)
            {
                return oOrderObject.PricedItineraryDetails.GetHashCode();
            }
            else
            {
                // if there is no object in the session, return -1;
                return -1;
            }
        }

        /// <summary>
        /// A method that saves all the details of the order in the dataabse (order,passengers, contact, flgiht legs etc.)
        /// and creates an XML file with that data also.
        /// </summary>
        private void SaveReservationToDB()
        {
            int orderId = oOrderObject.OrderDocId;
            Orders thisOrder = OrdersContainer.SelectByID(orderId, 1, true).First;
            thisOrder.OrderStatus = (int)EnumHandler.OrderStatus.BookingInProgress;
            thisOrder.DateOfBook = DateTime.Now;

            #region contact details
            string MobilePhoneTemp = null;
            if (!string.IsNullOrEmpty(txt_PhoneSecond.Text))
            {
                MobilePhoneTemp = ddl_PhonePrefixSecond.SelectedValue + "-" + txt_PhoneSecond.Text;
            }
            ContactDetails orderContact = new ContactDetails()
            {
                MainPhone = ddl_PhonePrefix.SelectedValue + "-" + txt_MainPhone.Text,
                MobliePhone = MobilePhoneTemp,
                OrdersDocId = orderId,
                Title = ddl_ContactTitle.SelectedValue.ToLower(),
                LastName = txt_ContactLastName.Text,
                FirstName = txt_ContactFirstName.Text,
                Email = txt_Email.Text,
                WhiteLabelDocId = BasePage_UI.GetWhiteLabelDocID()
            };
            orderContact.Action(DB_Actions.Insert);
            thisOrder.ContactDetailsDocId = orderContact.DocId_Value;
            #endregion

            #region passengers
            PassengerContainer oPassengerContainer = new PassengerContainer();
            foreach (var item in oOrderObject.BookingParameters.Passengers.DataSource)
            {
                DL_LowCost.Passenger oPassenger = new DL_LowCost.Passenger();
                // Common passenger properties (should always be filled):
                oPassenger.Title = item.PassengerBase.SocialTitle.ToString();
                oPassenger.FirstName = item.PassengerBase.FirstNameField;
                oPassenger.LastName = item.PassengerBase.LastNameField;
                oPassenger.Gender = item.Gender.ToString();
                oPassenger.DateOfBirth = item.BirthDate;
                oPassenger.OrdersDocId = orderId;
                // Addtional parameters - different for each vendor 
                if (item.CustomSupplierParameterList.DataSource != null)
                {
                    // For each customer parameter, add it's matching value from the input to the oPassenger object
                    foreach (var itemCustomParameter in item.CustomSupplierParameterList.DataSource)
                    {
                        var parameterName = (EnumHandler.RequiredFields)Enum.Parse(typeof(EnumHandler.RequiredFields), itemCustomParameter.Name, true);
                        switch (parameterName)
                        {
                            case Generic.EnumHandler.RequiredFields.PassportNumber:
                                oPassenger.PassportNumber = itemCustomParameter.Value;
                                break;
                            case Generic.EnumHandler.RequiredFields.PassportExpiryDate:
                                oPassenger.PassportExpiryDate = ConvertToValue.ConvertToDateTime(itemCustomParameter.Value);
                                break;
                            case Generic.EnumHandler.RequiredFields.PassportCountryOfIssue:
                                oPassenger.PassportIssueCountry = itemCustomParameter.Value;
                                break;
                        }
                        #region special services not in use by now
                        //if (itemCustomParameter.Name == Generic.EnumHandler.RequiredFields.MealType.ToString())
                        //{ }
                        //if (itemCustomParameter.Name == Generic.EnumHandler.RequiredFields.NumberOfBags.ToString())
                        //{ }
                        //if (itemCustomParameter.Name == Generic.EnumHandler.RequiredFields.FrequentFlyerType.ToString())
                        //{ }
                        //if (itemCustomParameter.Name == Generic.EnumHandler.RequiredFields.FrequentFlyerNumber.ToString())
                        //{ }
                        //if (itemCustomParameter.Name == Generic.EnumHandler.RequiredFields.InsuranceType.ToString())
                        //{ }
                        #endregion
                    }
                }
                // Add the passenger details to the list of passengers
                oPassengerContainer.Add(oPassenger);
            }
            // Add the list to the order object
            thisOrder.Passengers = oPassengerContainer;
            #endregion

            #region flight legs
            OrderFlightLegContainer oOrderFlightLegContainer = new OrderFlightLegContainer();
            foreach (var item in oSelectedItinerary.SelectedFlights.GroupList.DataSource)
            {
                decimal deltaNetPrice = oOrderObject.DeltaNetPrice;

                if (item.ReturnFlights != null && item.ReturnFlights.DataSource.FirstOrDefault() != null)
                {
                    deltaNetPrice = deltaNetPrice > 0 ? deltaNetPrice /= 2 : 0;
                    #region return flights
                    foreach (var itemReturnFlight in item.ReturnFlights.DataSource)
                    {
                        OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
                        oOrderFlightLeg.FlightType = EnumHandler.FlightTypes.Return.ToString();
                        oOrderFlightLeg.PriceNet = ConvertToValue.ConvertToDouble(itemReturnFlight.Price.Amount + deltaNetPrice);
                        oOrderFlightLeg.PriceMarkUp = ConvertToValue.ConvertToDouble(itemReturnFlight.Price.MarkUp);
                        oOrderFlightLeg.OrdersDocId = oOrderObject.OrderDocId;
                        oOrderFlightLeg.Action(DB_Actions.Insert);
                        FlightLegContainer oFlightLegContainerReturn = new FlightLegContainer();
                        foreach (var itemLeg in itemReturnFlight.FlightSegments.DataSource)
                        {
                            FlightLeg oFlightLeg = new FlightLeg();
                            oFlightLeg.OrderFlightLegDocId = oOrderFlightLeg.DocId_Value;
                            oFlightLeg.AirlineIataCode = itemLeg.OperatingAirline.Code;
                            oFlightLeg.FlightNumber = itemLeg.FlightCode;
                            oFlightLeg.DepartureDateTime = itemLeg.DepartDateTime;
                            oFlightLeg.ArrivalDateTime = itemLeg.ArrivalDateTime;
                            oFlightLeg.OriginIataCode = itemLeg.DepartureAirport.LocationCode;
                            oFlightLeg.DestinationIataCode = itemLeg.ArrivalAirport.LocationCode;
                            oFlightLeg.Duration = itemLeg.JourneyDuration;
                            oFlightLeg.Hops = itemLeg.StopsQuantity;
                            oFlightLeg.Status = EnumHandler.FlightStatus.OnTime.ToString();
                            //TODO: add marketing airline
                            oFlightLegContainerReturn.Add(oFlightLeg);
                        }
                        oOrderFlightLeg.FlightLegs = oFlightLegContainerReturn;
                        oOrderFlightLeg.FlightLegs.Action(DB_Actions.Insert);
                        oOrderFlightLegContainer.Add(oOrderFlightLeg);
                    }
                    #endregion
                }
                #region outward flights
                foreach (var itemOutwardFlight in item.OutwardFlights.DataSource)
                {
                    OrderFlightLeg oOrderFlightLeg = new OrderFlightLeg();
                    oOrderFlightLeg.FlightType = EnumHandler.FlightTypes.Outward.ToString();
                    oOrderFlightLeg.PriceNet = ConvertToValue.ConvertToDouble(itemOutwardFlight.Price.Amount + deltaNetPrice);
                    oOrderFlightLeg.PriceMarkUp = ConvertToValue.ConvertToDouble(itemOutwardFlight.Price.MarkUp);
                    oOrderFlightLeg.OrdersDocId = oOrderObject.OrderDocId;
                    oOrderFlightLeg.Action(DB_Actions.Insert);
                    FlightLegContainer oFlightLegContainerOutward = new FlightLegContainer();
                    foreach (var itemLeg in itemOutwardFlight.FlightSegments.DataSource)
                    {
                        FlightLeg oFlightLeg = new FlightLeg();
                        oFlightLeg.OrderFlightLegDocId = oOrderFlightLeg.DocId_Value;
                        oFlightLeg.AirlineIataCode = itemLeg.OperatingAirline.Code;
                        oFlightLeg.FlightNumber = itemLeg.FlightCode;
                        oFlightLeg.DepartureDateTime = itemLeg.DepartDateTime;
                        oFlightLeg.ArrivalDateTime = itemLeg.ArrivalDateTime;
                        oFlightLeg.OriginIataCode = itemLeg.DepartureAirport.LocationCode;
                        oFlightLeg.DestinationIataCode = itemLeg.ArrivalAirport.LocationCode;
                        oFlightLeg.Duration = itemLeg.JourneyDuration;
                        oFlightLeg.Hops = itemLeg.StopsQuantity;
                        oFlightLeg.Status = EnumHandler.FlightStatus.OnTime.ToString();
                        //TODO: add marketing airline
                        oFlightLegContainerOutward.Add(oFlightLeg);
                    }
                    oOrderFlightLeg.FlightLegs = oFlightLegContainerOutward;
                    oOrderFlightLeg.FlightLegs.Action(DB_Actions.Insert);
                    oOrderFlightLegContainer.Add(oOrderFlightLeg);
                }
                #endregion
            }
            thisOrder.OrderFlightLegs = oOrderFlightLegContainer;
            #endregion

            thisOrder.AirlineIataCode = oSelectedItinerary.SelectedFlights.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().MarketingAirline.Code;
            thisOrder.MarketingCabin = oSelectedItinerary.SelectedFlights.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().MarketingCabin.ClassNameGeneral;

            // TOTAL PRICE   
            thisOrder.TotalPrice = ConvertToValue.ConvertToDouble(oOrderObject.TotalPriceWithHandlingFinal);
            // Save the Currency
            thisOrder.Currency = oOrderObject.NetPriceAndCurrencyFinal.Currency;
            thisOrder.ReceiveEmail = cb_isInterestedInMails.Checked;
            thisOrder.ReceiveSms = cb_isInterestedInSMS.Checked;

            // LOGGER
            LoggerManagerProject.LoggerManager.LoggeSpecific.WriteInfo(string.Format("OrderPage => SaveReservationToDB() => thisOrder.TotalPrice: {0}{1}", thisOrder.Currency, thisOrder.TotalPrice_UI));

            //insert all book data& update the order on DB
            thisOrder.Passengers.Action(DB_Actions.Insert);
            //thisOrder.OrderFlightLegs.Action(DB_Actions.Update);
            thisOrder.Action(DB_Actions.Update);
        }

        private void SetPassengers()
        {
            if (passengerList != null && passengerList.Count > 0)
            {
                UC_PassengerDetails oUC_PassengerDetails;
                int counter = 1;
                foreach (var item in passengerList)
                {
                    for (int i = 1; i <= item.Value; i++)
                    {
                        oUC_PassengerDetails = (UC_PassengerDetails)LoadControl(StaticStrings.path_UC_PassengerDetails);
                        oUC_PassengerDetails.PassengerType = item.Key;
                        oUC_PassengerDetails.PassengerNumber = counter;

                        if (oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().ReturnFlights != null && oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().ReturnFlights.DataSource.Count() > 0)
                        {
                            // If there is a return flight, set the date for age comparison to the return date
                            oUC_PassengerDetails.CompareDate = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().ReturnFlights.DataSource[0].FlightSegments.DataSource[0].DepartDateTime;
                        }
                        else
                        {
                            // if no return flight, Set the date for age comparison to the outward date
                            oUC_PassengerDetails.CompareDate = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().OutwardFlights.DataSource[0].FlightSegments.DataSource[0].DepartDateTime;
                        }

                        oUC_PassengerDetails.RequiredParameters = oOrderObject.PricedItineraryDetails.RequiredParameters;
                        oUC_PassengerDetails.ID = string.Format("UC_{0}{1}", item.Key.ToString(), i);
                        ph_PassengerDetails.Controls.Add(oUC_PassengerDetails);
                        counter++;
                    }
                }
            }
            else
            {
                // there are no passengers in the list 
                LoggerManagerProject.LoggerManager.Logger.WriteError("Error: There are no passengers in the order object.");
                Response.Redirect(StaticStrings.path_ErrorPage);

            }
        }

        /// <summary>
        /// A method that gets the details for each passenger (filled in the boxes on the page), and returns a list of all the passengers with that data.
        /// </summary>
        /// <returns></returns>
        private PassengerList GetPassengerDetails()
        {
            // Create a new passengers list
            PassengerList oPassengerList = new PassengerList();
            int counter = 0;
            List<PassengerDetails> passengers = new List<PassengerDetails>();


            // Iterate over the user controls for all the passenger's details on the pageS
            foreach (UserControl uc in ph_PassengerDetails.Controls)
            {
                // cast the UC to the correct control
                UC_PassengerDetails thisControl = (UC_PassengerDetails)uc;
                PassengerDetails passengerDetails = new PassengerDetails();
                // Get the type of the passenger (adult, child, infant)
                passengerDetails.PassengerType = (LowCostSite.FlightsBookingService.ECustomerType)thisControl.PassengerType;
                // Set the common data to all passengers
                passengerDetails.PassengerBase = new PassengerBase()
                {
                    SocialTitle = (ESocialTitle)Enum.Parse(typeof(ESocialTitle), thisControl.TXT_Title),
                    FirstNameField = thisControl.TXT_FirstName,
                    LastNameField = thisControl.TXT_LastName,
                };

                //Set the first passenger as main passenger 
                if (counter == 0)
                {
                    passengerDetails.IsMainPassenger = true;
                    counter++;
                }
                // Set the age and the date of birth
                passengerDetails.Age = GetAge(thisControl.TXT_BirthDate, oSelectedItinerary.SelectedFlights.OutwardDate.ToShortDateString());
                passengerDetails.BirthDate = ConvertToValue.ConvertToDateTime(thisControl.TXT_BirthDate);
                #region Set the gender base on title
                switch (thisControl.TXT_Title.ToLower())
                {
                    case "mr":
                        passengerDetails.Gender = EGender.Male;
                        break;
                    case "ms":
                    case "mrs":
                        passengerDetails.Gender = EGender.Female;
                        break;
                }
                #endregion

                passengerDetails.CustomSupplierParameterList = new CustomSupplierParameterContainer();
                passengerDetails.CustomSupplierParameterList.DataSource = SetPassengerRequiredParameters(thisControl).ToArray();
                // add the passenger's parameters to the passengers list
                passengers.Add(passengerDetails);
            }
            oPassengerList.DataSource = passengers.ToArray();

            // Return an arrany with all the passengers with their details
            return oPassengerList;
        }

        /// <summary>
        /// Am method that gets a string representing the date of birth, and returns the age
        /// </summary>
        /// <param name="dateOfBirth">A string for the date of birth</param>
        /// <param name="outwardFlightDate">A string for the referece date (the ooutward flight date)</param>
        /// <returns>The Age of the passenger</returns>
        private int GetAge(string dateOfBirth, string outwardFlightDate)
        {
            DateTime birthday = ConvertToValue.ConvertToDateTime(dateOfBirth);
            DateTime reference = ConvertToValue.ConvertToDateTime(outwardFlightDate);

            int age = reference.Year - birthday.Year;
            if (reference < birthday.AddYears(age))
            {
                age--;
            }

            return age;
        }
        private int GetAge(DateTime birthday, DateTime reference)
        {
            int age = reference.Year - birthday.Year;
            if (reference < birthday.AddYears(age))
            {
                age--;
            }
            return age;
        }

        private List<CustomSupplierParameter> SetPassengerRequiredParameters(UC_PassengerDetails thisControl)
        {
            List<CustomSupplierParameter> customParams = new List<CustomSupplierParameter>();
            CustomSupplierParameter oCustomSupplierParameter;
            if (ConvertToValue.ConvertToBool(thisControl.BirthDateRequired))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.DateOfBirth.ToString(),
                    Value = thisControl.TXT_BirthDate,
                };
                customParams.Add(oCustomSupplierParameter);
            }

            if (!string.IsNullOrEmpty(thisControl.TXT_PassportNumber))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.PassportNumber.ToString(),
                    Value = thisControl.TXT_PassportNumber,
                };
                customParams.Add(oCustomSupplierParameter);
            }

            if (!string.IsNullOrEmpty(thisControl.TXT_PassportExpireDate))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.PassportExpiryDate.ToString(),
                    Value = thisControl.TXT_PassportExpireDate,
                };
                customParams.Add(oCustomSupplierParameter);
            }
            if (!string.IsNullOrEmpty(thisControl.TXT_PassportIssueCountry))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.PassportCountryOfIssue.ToString(),
                    Value = thisControl.TXT_PassportIssueCountry,
                };
                customParams.Add(oCustomSupplierParameter);
            }
            if (!string.IsNullOrEmpty(thisControl.TXT_MealType))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.MealType.ToString(),
                    Value = thisControl.TXT_MealType,
                };
                customParams.Add(oCustomSupplierParameter);
            }
            if (!string.IsNullOrEmpty(thisControl.TXT_NumberOfBags))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.NumberOfBags.ToString(),
                    Value = thisControl.TXT_NumberOfBags,
                };
                customParams.Add(oCustomSupplierParameter);
            }

            if (!string.IsNullOrEmpty(thisControl.TXT_FrequentFlyerType))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.FrequentFlyerType.ToString(),
                    Value = thisControl.TXT_FrequentFlyerType,
                };
                customParams.Add(oCustomSupplierParameter);
            }
            if (!string.IsNullOrEmpty(thisControl.TXT_FrequentFlyerNumber))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.FrequentFlyerNumber.ToString(),
                    Value = thisControl.TXT_FrequentFlyerNumber,
                };
                customParams.Add(oCustomSupplierParameter);
            }
            if (!string.IsNullOrEmpty(thisControl.TXT_InsuranceType))
            {
                oCustomSupplierParameter = new CustomSupplierParameter()
                {
                    Name = Generic.EnumHandler.RequiredFields.InsuranceType.ToString(),
                    Value = thisControl.TXT_InsuranceType,
                };
                customParams.Add(oCustomSupplierParameter);
            }
            return customParams;
        }


        private void UpdatePrice(FlightBookedDetails oFlightBookDetails)
        {
            // the Delta between the net prices (before and after the price change)
            // this and then we get the total bruto price (handling fee + markup) from the label and we add the delta

            decimal newPrice = oOrderObject.FlightBookDetails.BookingDetails.GroupList.DataSource.FirstOrDefault().Price.Amount; //  oFlightBookDetails.BookingDetails.GroupList.DataSource.FirstOrDefault().Price.Amount;
            string newCurrency = oOrderObject.FlightBookDetails.BookingDetails.GroupList.DataSource.FirstOrDefault().Price.Currency;
            decimal oldPrice = oOrderObject.NetPriceAndCurrency.Price;

            // if the currency did not chnage
            if (newCurrency == oOrderObject.NetPriceAndCurrency.Currency)
            {
                // Calculating the delta between the old net price and the new net price (after the price was changed)
                decimal deltaNetPrice = newPrice - oldPrice;
                // save the delta in the order object
                oOrderObject.DeltaNetPrice = deltaNetPrice;

                // The OrderPrice is the Bruto price, which is the older TotalPrice (stored in the view state) + the Delta between the net prices that was calculated
                // add the new calculated total price
                oOrderObject.TotalPriceWithHandlingFinal = oOrderObject.TotalPriceWithHandling + deltaNetPrice;

                // Add the new price and currency to the order object
                var TotalNetPriceNew = oOrderObject.NetPriceAndCurrency.Price + deltaNetPrice;
                oOrderObject.NetPriceAndCurrencyFinal = new OrderPrice(TotalNetPriceNew, newCurrency);
            }
            else
            {
            }
        }

        /// <summary>
        /// A method that shows an alert when the price was changed, and the saves the new price, new currency and the delta in the view state. 
        /// </summary>
        /// <param name="oFlightBookDetails"></param>
        private void ShowPriceError(OrderPrice oSessionFlightBookTotalNetPriceAndCurrency)
        {
            if (oSessionFlightBookTotalNetPriceAndCurrency != null)
            {
                // Set the new price in the "Price Changed" alert message 
                lbl_newPrice.InnerText = string.Format("{0} {1}",
                    oSessionFlightBookTotalNetPriceAndCurrency.Price.ToString("0.00"),
                    Extensions.GetCurrencySymbol(oSessionFlightBookTotalNetPriceAndCurrency.Currency));
            }
            panel_ErrorPrice.Visible = true;
            HidePassengersDetailsForm();
            RemoveAllMessagesButPrice();
        }

        /// <summary>
        /// A method that being called when after a price change, the currency also changes.
        /// It shows the error of currency change, and doesn't allow to book the order.
        /// </summary>
        private void ShowCurrencyError()
        {
            panel_ErrorCurrency.Visible = true;
            HidePassengersDetailsForm();
        }

        /// <summary>
        /// A method that adds the terms and conditions for the selected flight at the bottom of the order page.
        /// </summary>
        private void FillConditions()
        {
            if (oOrderObject.PricedItineraryDetails.SupplierInfoContainer != null && oOrderObject.PricedItineraryDetails.SupplierInfoContainer.DataSource.Count() > 0)
            {
                var supplierInfoList = oOrderObject.PricedItineraryDetails.SupplierInfoContainer.DataSource;
                foreach (var supplierInfo in supplierInfoList)
                {
                    if (supplierInfo.InfoType == "url")
                    {
                        ph_Conditions.Controls.Add(new LiteralControl(string.Format("<p> <a href='{0}' target='_blank'>{1}</a> </p>", supplierInfo.Info, supplierInfo.DisplayText)));
                    }
                    else if (supplierInfo.InfoType == "remark")
                    {
                        ph_Conditions.Controls.Add(new LiteralControl(string.Format("<p><strong>{0}: </strong> {1}</p>", supplierInfo.DisplayText, supplierInfo.Info)));
                    }
                }
            }
        }

        /// <summary>
        /// A method that sets the flight description box with the prices for each passenger type (breakdown), handling fee, and total price.e
        /// </summary>
        /// <param name="oPassengerPriceContainer"></param>
        private void FillPrices(decimal HandlingFee, LowCostSite.FlightsSearchService.Pricing oPriceContainerOutward, LowCostSite.FlightsSearchService.Pricing oPriceContainerReturn)
        {
            // passneger count
            int numOfAdults = oOrderObject.SearchParams.AdultsCount;
            int numOfChildren = oOrderObject.SearchParams.ChildrenCount;
            int numOfInfants = oOrderObject.SearchParams.InfantsCount;

            // Prices 
            decimal totalNetPrice = 0, totalHandlingFee = 0, SumAdultsPrice = 0, sumChildrenPrice = 0, sumInfantsPrice = 0;

            bool isPassengerPrices = false;

            int passengerNumber = oOrderObject.SearchParams.AdultsCount + oOrderObject.SearchParams.ChildrenCount + oOrderObject.SearchParams.InfantsCount;
            string currencyCode = oPriceContainerOutward.Currency;
            string currency = Extensions.GetCurrencySymbol(oPriceContainerOutward.Currency);
            if (oPriceContainerOutward != null)
            {
                // Add the handling fee for each passenger for the outward segment
                totalHandlingFee += passengerNumber * HandlingFee;

                // If there is a price per passenger breakdown:
                if (oPriceContainerOutward.PassengerPrices != null && oPriceContainerOutward.PassengerPrices.DataSource.Count() > 0)
                {
                    isPassengerPrices = true;
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.ADT))
                    {
                        SumAdultsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.CNN))
                    {
                        sumChildrenPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.INF))
                    {
                        sumInfantsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                }
                else
                {
                    //In case there is no price per passenger, set equal prices per passenger
                    totalNetPrice += oPriceContainerOutward.Amount;
                    // Calc the equal price
                    decimal pricePerPassenger = oPriceContainerOutward.TotalAmountWithMarkUp.Value / passengerNumber;
                    // Set this price, times the number of passenger in each category
                    SumAdultsPrice += numOfAdults * pricePerPassenger;
                    sumChildrenPrice += numOfChildren * pricePerPassenger;
                    sumInfantsPrice += numOfInfants * pricePerPassenger;
                }
            }
            if (oPriceContainerReturn != null)
            {
                // Multiply the handling fee by 2 for the return flight
                totalHandlingFee *= 2;

                if (oPriceContainerReturn.PassengerPrices != null && oPriceContainerReturn.PassengerPrices.DataSource.Count() > 0)
                {
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.ADT))
                    {
                        SumAdultsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.CNN))
                    {
                        sumChildrenPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.INF))
                    {
                        sumInfantsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                }
                else
                {
                    if (!isPassengerPrices)
                    {
                        // there is no price per passenger
                        totalNetPrice += oPriceContainerReturn.Amount;
                        // when there is no breakdown - store the total price with markup in the adult var, and in summary it will sum the SumAdultsPrice with the childer and infants, which will be both 0.
                        decimal pricePerPassenger = oPriceContainerReturn.TotalAmountWithMarkUp.Value / passengerNumber;
                        SumAdultsPrice += numOfAdults * pricePerPassenger;
                        sumChildrenPrice += numOfChildren * pricePerPassenger;
                        sumInfantsPrice += numOfInfants * pricePerPassenger;
                    }
                }
            }

            passengerList = new Dictionary<FlightsSearchService.ECustomerType, int>();
            if (numOfAdults > 0)
            {
                lbl_PriceBox_AdultCount.Text = numOfAdults.ToString();
                lbl_PriceBox_AdultPrice.Text = string.Format("{0} {1}", currency, SumAdultsPrice.ToString("0.00"));
                div_AdultPrice.Attributes["class"] = "row";
                passengerList.Add(FlightsSearchService.ECustomerType.ADT, numOfAdults);
            }
            if (numOfChildren > 0)
            {
                lbl_PriceBox_ChildCount.Text = numOfChildren.ToString();
                lbl_PriceBox_ChildPrice.Text = string.Format("{0} {1}", currency, sumChildrenPrice.ToString("0.00"));
                div_ChildPrice.Attributes["class"] = "row";
                passengerList.Add(FlightsSearchService.ECustomerType.CNN, numOfChildren);
            }
            if (numOfInfants > 0)
            {
                lbl_PriceBox_InfantCount.Text = numOfInfants.ToString();
                lbl_PriceBox_InfantPrice.Text = string.Format("{0} {1}", currency, sumInfantsPrice.ToString("0.00"));
                div_InfantPrice.Attributes["class"] = "row";
                passengerList.Add(FlightsSearchService.ECustomerType.INF, numOfInfants);
            }
            if (HandlingFee > 0)
            {
                lbl_PassengerNumber.Text = passengerNumber.ToString();
                lbl_HandlingFee.Text = string.Format("{0} {1}", currency, totalHandlingFee.ToString("0.00"));
                div_HandlingFee.Attributes["class"] = "row";
            }
            // Caculate the total price (with handling fee)
            decimal totalPriceWithHandlingFee = SumAdultsPrice + sumChildrenPrice + sumInfantsPrice + totalHandlingFee;

            // Save the total Price in the view state
            oOrderObject.TotalPriceWithHandling = totalPriceWithHandlingFee;

            lbl_TotalPrice.Text = string.Format("{0} {1}", totalPriceWithHandlingFee.ToString("0.00"), currency);
            lbl_FooterTotalPrice.Text = string.Format("{0} {1}", currency, totalPriceWithHandlingFee.ToString("0.00"));

            // only if its the first time
            if (oOrderObject.NetPriceAndCurrency == null)
            {
                oOrderObject.NetPriceAndCurrency = new OrderPrice(totalNetPrice, currencyCode);
            }
            // LOGGER
            LoggerManagerProject.LoggerManager.LoggeSpecific.WriteInfo(string.Format("OrderPAge => FillPrices() => oSessionFlightBookTotalNetPriceAndCurrency => Price: {0} | Currency: {1}", totalNetPrice, currencyCode));
        }

        /// <summary>
        /// A method that fills the flights segments on the details flights box
        /// </summary>
        /// <param name="oPassengerPriceContainer"></param>
        private void FillFlightsDetails(LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerOutward, LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerReturn)
        {
            rpd_OutwardSegments.DataSource = oSegmentLegContainerOutward.DataSource;
            rpd_OutwardSegments.DataBind();
            if (oSegmentLegContainerReturn != null)
            {
                div_outwardFlightsDetails.Attributes.Remove("class");

                rpd_ReturnSegments.DataSource = oSegmentLegContainerReturn.DataSource;
                rpd_ReturnSegments.DataBind();
            }
        }

        /// <summary>
        /// A method that creates an order object, and saves it in the session (oOrderObject).
        /// This method is called when all the details are final (no rice errors)
        /// </summary>
        /// <param name="OrderDocId"></param>
        private void UpdateOrderObjectAndCreateXml()
        {
            #region Fill the remaining properties


            // IF the price didn't change in the booking attempt, store the last values as the final values.
            if (oOrderObject.NetPriceAndCurrencyFinal == null || oOrderObject.NetPriceAndCurrencyFinal.Price == 0)
            {
                oOrderObject.NetPriceAndCurrencyFinal = oOrderObject.NetPriceAndCurrency;
                oOrderObject.DeltaNetPrice = 0;
                oOrderObject.TotalPriceWithHandlingFinal = oOrderObject.TotalPriceWithHandling;
            }

            oOrderObject.FlightBookTotalNetCurrency = oOrderObject.NetPriceAndCurrencyFinal.Currency;
            oOrderObject.FlightBookTotalNetPrice = oOrderObject.NetPriceAndCurrencyFinal.Price;
            // fill ticketing parameters for issue
            oOrderObject.TicketingParameters = new IssueTicketingParameters();
            #region Fill the ticketing params
            oOrderObject.TicketingParameters.FlightsID = oOrderObject.BookingParameters.FlightsID;
            oOrderObject.TicketingParameters.ExpectedTotalPrice = ConvertToValue.ConvertToDouble(oOrderObject.FlightBookTotalNetPrice);
            oOrderObject.TicketingParameters.ExpectedTotalPriceCurrency = oOrderObject.FlightBookTotalNetCurrency;
            oOrderObject.TicketingParameters.TfReference = oOrderObject.FlightBookDetails.BookingDetails.SupplierBookingReference;
            oOrderObject.TicketingParameters.PNR = oOrderObject.FlightBookDetails.BookingDetails.PNR;
            oOrderObject.TicketingParameters.GeneralParameters = oOrderObject.BookingParameters.GeneralParameters;
            #endregion
            oOrderObject.SelectedItinerary.LoadFromObject(this.oSelectedItinerary);
            oOrderObject.IsInterestedInMails = this.cb_isInterestedInMails.Checked;
            oOrderObject.IsInterestedInSMS = this.cb_isInterestedInSMS.Checked;
            oOrderObject.PassengerNumber = oOrderObject.SearchParams.AdultsCount + oOrderObject.SearchParams.ChildrenCount + oOrderObject.SearchParams.InfantsCount;
            oOrderObject.TotalPriceWithHandlingFinal = oOrderObject.TotalPriceWithHandling + oOrderObject.DeltaNetPrice;
            oOrderObject.FlightBookTotalNetPrice = ConvertToValue.ConvertToDecimal(oOrderObject.NetPriceAndCurrencyFinal.Price);

            #endregion

            // LOGGER
            LoggerManagerProject.LoggerManager.LoggeSpecific.WriteInfo(string.Format("OrderPage => CreateOrderObjectAndXml() => oOrderObject.DeltaNetPrice: {0} | oOrderObject.TotalPrice: {1} | oOrderObject.FlightBookTotalNetPrice", oOrderObject.DeltaNetPrice, oOrderObject.TotalPriceWithHandling, oOrderObject.FlightBookTotalNetPrice));

            //Create Xml (in order to not loose the data even if the session asended for some reason, we save it as an XML file and later convert this file back to an order object)
            oOrderObject.SaveToFile();
            // oOrderObject.SaveAsXml();
            //save to session
            SessionManager.SetSession<OrderObject>(oOrderObject);
        }

        /// <summary>
        /// A method that builds the booking request - incuding Booking parameters, and passengers list with theie required parameters filled in.
        /// </summary>
        /// <returns></returns>
        private BookingParameters BuildRequest()
        {
            // Create the main parameters object
            BookingParameters oTBookingParameters = new BookingParameters();
            // get the details filled for each passenger on the page, and create a new passenger details object for them.
            PassengerList oPassengerList = GetPassengerDetails();
            if (oPassengerList != null)
            {
                // set the parameters object to hold the passengers list.
                oTBookingParameters.Passengers = oPassengerList;
                oTBookingParameters.FlightsID = oSelectedItinerary.SelectedFlights.ID;
                return oTBookingParameters;
            }
            else
            {
                //no passenger list, can't continue
            }
            return null;
        }

        /// <summary>
        /// Write a log on the DB
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="requestOrResponse"></param>
        /// <param name="type"></param>
        /// <param name="status"></param>
        private void WriteLogOnDB(int orderId, object requestOrResponse, EnumHandler.OrderStage orderStage, EnumHandler.OrderXmlType type, EnumHandler.OrderXmlStatus status)
        {
            OrderLog oOrderLog = new OrderLog();
            oOrderLog.OrdersDocId = orderId;
            oOrderLog.XML = requestOrResponse.SerializeToString();
            oOrderLog.Type = (int)type;
            oOrderLog.Status = (int)status;
            oOrderLog.OrderStage = (int)orderStage;
            oOrderLog.Action(DB_Actions.Insert);
        }

        protected void CheckHashCode()
        {
            if (oOrderObject.PricedItineraryDetails != null)
            {
                int orderObjectHashCode = oOrderObject.PricedItineraryDetails.GetHashCode();
                int currentOrderObjectHashCode = ConvertToValue.ConvertToInt(hf_IdentityCode.Value);
                if (currentOrderObjectHashCode != orderObjectHashCode) //compare the hash code
                {
                    // if the hash code is not mach
                    LoggerManagerProject.LoggerManager.Logger.WriteWarrning("Order Object On Page Different From Order Object In Session.");
                    Response.Redirect(StaticStrings.path_IndexPage); //TODO : build OrderTimeExpiredPage.aspx for timeout errors
                }
                else
                {
                    //OK
                }
            }
            else
            {
                //Session TimeOut... redirect to home page
                Response.Redirect(StaticStrings.path_IndexPage);
            }
        }

        #region UI functions
        /// <summary>
        /// A method that hides the passengers details form by adding a 'display-none' class.
        /// </summary>
        private void HidePassengersDetailsForm()
        {
            // Hide the passengers details form
            div_generalDiv.Attributes["class"] = div_generalDiv.Attributes["class"] + " display-none";
        }
        /// <summary>
        /// A method that shows the passengers details form by removing a 'display-none' class.
        /// </summary>
        private void ShowPassengersDetailsForm()
        {
            // Show the passengers details form
            div_generalDiv.Attributes["class"] = div_generalDiv.Attributes["class"].Replace("display-none", "");
        }

        /// <summary>
        /// A method that hides all the messages abouterrors, and leaves the Price Change error (which is not an error actually)
        /// </summary>
        private void RemoveAllMessagesButPrice()
        {
            div_DuplicateBooking.Attributes["class"] = div_DuplicateBooking.Attributes["class"] + " display-none";
            div_ChildAgeError.Attributes["class"] = div_ChildAgeError.Attributes["class"] + " display-none";
            div_InfantAgeError.Attributes["class"] = div_InfantAgeError.Attributes["class"] + " display-none";
            div_generalError.Attributes["class"] = div_generalError.Attributes["class"] + " display-none";
            div_ItineraryError.Attributes["class"] = div_ItineraryError.Attributes["class"] + " display-none";
        }


        /// <summary>
        /// A method that show the age error div for child / infant
        /// </summary>
        /// <param name="IsChild">Tells the function if to show the child age error, or else show the infant age error</param>
        protected void ShowAgeError(bool IsChild)
        {
            if (IsChild)
            {
                // Show the child error message (as a validator message)
                // div_ChildAgeError.Attributes["class"] = div_ChildAgeError.Attributes["class"].Replace("display-none", "highlight rtl display_inline_block full-width");
                HidePassengersDetailsForm();
                return;
            }
            else
            {
                // Show the child error message (as a validator message)
                div_InfantAgeError.Attributes["class"] = div_InfantAgeError.Attributes["class"].Replace("display-none", "highlight rtl display_inline_block full-width");
                HidePassengersDetailsForm();
                return;
            }
        }
        /// <summary>
        /// Hide all visible age aerror divs
        /// </summary>
        private void HideAgeErrors()
        {
            if (!div_ChildAgeError.Attributes["class"].Contains("display-none"))
            {
                div_ChildAgeError.Attributes["class"] = div_ChildAgeError.Attributes["class"] + " display-none";
            }
            if (!div_InfantAgeError.Attributes["class"].Contains("display-none"))
            {
                div_InfantAgeError.Attributes["class"] = div_InfantAgeError.Attributes["class"] + " display-none";
            }
        }

        /// <summary>
        ///  A method that fills all the places that use getText() text
        /// </summary>
        protected void FillGetTexts()
        {

            btn_ContinueOrder.Text = GetText("ContinueToOrder");
            btn_BackToHome.Text = GetText("GoToHome");
            btn_Cancel.Text = GetText("Cancel");
            btn_Cancel2.Text = GetText("BackToFlightResults");
            //  btn_BackToOrderPage.Text = GetText("GoToOrderPage");
            //Btn_BackToSearchResult_Child.Text = GetText("BackToFlightResults");
            //Btn_BackToSearchResult_Infant.Text = GetText("BackToFlightResults");
            ddl_ContactTitle.Items[0].Text = GetText("Mr");
            ddl_ContactTitle.Items[1].Text = GetText("Mrs");
            txt_ContactFirstName.Attributes["placeholder"] = GetText("FirstName");
            txt_ContactLastName.Attributes["placeholder"] = GetText("LastName");

            //txt_City.Attributes["placeholder"] = GetText("City");
            //txt_PostalCode.Attributes["placeholder"] = GetText("ZIPExample");
            cb_isInterestedInMails.Text = GetText("AgreeToReceiveCommercialContentAndInfoAboutFlightChangesByEmail");
            cb_isInterestedInSMS.Text = GetText("AgreeToReceiveCommercialContentAndInfoAboutFlightChangesBySms");
            cb_TermsOfServiceApproval.Text = GetText("IagreeTo");
            btn_Book.Text = GetText("Continue_ar");

            //RequiredFieldValidator1.ErrorMessage = GetText("RequiredField");
            //RequiredFieldValidator2.ErrorMessage = GetText("RequiredField");
            //RequiredFieldValidator3.ErrorMessage = GetText("RequiredField");
            //RequiredFieldValidator4.ErrorMessage = GetText("RequiredField");
            Val_LastNameRequired.ErrorMessage = GetText("RequiredField");
            Val_FirstNameRequired.ErrorMessage = GetText("RequiredField");
            Val_EmailTextRequired.ErrorMessage = GetText("RequiredField");//
            Val_EmailRegularExpressionValidator.ErrorMessage = GetText("TheEmailAddressIsNotValid");
            Val_PrimaryPhone.ErrorMessage = GetText("InvalidPhoneNumber");
            btn_ItineraryError_Back.Text = GetText("BackToFlightResults");
        }

        /// <summary>
        /// A method that generates a string for a flight segment (from => to , 00:00)
        /// </summary>
        /// <param name="segment">The flight segment to create the string for.</param>
        /// <param name="IsArrival">Is it the arrival part (if it is, the time is time of arrival and not of departure)</param>
        /// <returns>The text describing the flight segment</returns>
        protected string GenerateSegmentString(FlightsSearchService.FlightSegment segment, bool IsArrival)
        {
            string segmentString;
            if (IsArrival)
            {
                segmentString = string.Format("{0} - {1},  {2}", Extensions.GetAirportName(segment.ArrivalAirport.LocationCode), Extensions.GetCityOfAirport(segment.ArrivalAirport.LocationCode), segment.ArrivalDateTime.ToString("HH:mm"));
            }
            else
            {
                segmentString = string.Format("{0} - {1},  {2}", Extensions.GetAirportName(segment.DepartureAirport.LocationCode), Extensions.GetCityOfAirport(segment.DepartureAirport.LocationCode), segment.DepartDateTime.ToString("HH:mm"));
            }
            return segmentString;
        }

        #endregion

        #endregion
    }
}