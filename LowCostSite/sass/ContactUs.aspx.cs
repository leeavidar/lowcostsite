﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DL_Generic;
using Generic;
using BL_LowCost;
using DL_LowCost;

namespace LowCostSite.sass
{
    public partial class ContactUs : BasePage_UI
    {
        #region Session Private fields

        private StaticPages _oSessionStaticPage;

        #endregion


        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || SessionManager.GetSession<StaticPages>(out _oSessionStaticPage) == false)
            {
                //take the data from the table in the database
                _oSessionStaticPage = GetStaticPageFromDB();
            }
            else { /*Return from session*/}
        }
        internal override void SaveToSession()
        {
            // nothing to save...
        }
        internal override void ResetAllSessions()
        {
            // nothing to reset...
        }


        private StaticPages GetStaticPageFromDB()
        {

            //selecting all the static pages with a given white label
            var oStaticPagesContainer = StaticPagesContainer.SelectAllStaticPagess(BasePage_UI.GetWhiteLabelDocID(), null);
            if (oStaticPagesContainer != null)
            {
                var oStaticContactUsPage = oStaticPagesContainer.FindAllContainer(x => x.Name_UI == "ContactUs");
                if (oStaticContactUsPage.Single != null)
                {
                    return oStaticContactUsPage.Single;
                }
                else
                {
                    // the page was not found (add it to the database) 
                    LoggerManagerProject.LoggerManager.Logger.WriteError("Error: There is no static page with the name \"ContactUs\" in the database. Add it. ");
                }
            }
            return new StaticPages();
        }


        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            LoadFromSession(IsPostBack);
        }

        #endregion
    }

    public partial class ContactUs : BasePage_UI
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetMenuClass(EnumHandler.MenuPages.ContactUs);
            // set the page meta data with the data in the seo property.
            Master.SetMetaData(_oSessionStaticPage.SeoSingle);
            ph_StaticText.Controls.Add(new LiteralControl(_oSessionStaticPage.Text_UI));
        }


        protected void btn_SendRequest_Click(object sender, EventArgs e)
        {
            // if the form validation doesn't pass, do nothing (the validation method will display the error messages)
            if (!IsValid)
            {
                //page Validation failed
                return;
            }
            else
            {
                //validation success - continue
                Details oDetails = new Details()
                {
                    FirstName = txt_FirstName.Text,
                    LastName = txt_LastName.Text,
                    Phone = txt_Phone.Text,
                    Email = txt_Email.Text,
                    Comments = txt_Remarks.InnerText,
                };

                //save the details to the session
                Generic.SessionManager.SetSession<Details>(oDetails);

                MailMessage mail = new MailMessage();
                // send to this mail
                mail.To.Add(ConfigurationHandler.ReceivingEmailAddress); //"cruisefit@eshet-tours.co.il");
                // this is the subject line
                mail.Subject = ConfigurationHandler.EmailSubject;
                // generate the messsage with all the details
                mail.Body = GenerateMessage(oDetails.FirstName, oDetails.LastName, oDetails.Phone, oDetails.Email, oDetails.Comments);
                mail.IsBodyHtml = true;

                bool isMailSent = sendMassage(mail);

                if (isMailSent)
                {
                    div_form.Attributes.CssStyle.Add("display", "none");
                    div_thanks.Attributes.CssStyle.Add("display", "normal");
                }
                else
                {
                    //
                }
            }


        }

        /// <summary>
        /// פונקציה שמקבלת אובייקט מסוג מייל ושולחת אותו
        /// </summary>
        /// <param name="mail">המייל לשליחה - כולל בתוכו את הנמען, את נושא ההודעה ואת גוף ההודעה</param>
        /// <returns>מחזיר אמת אם נשלח ושקר אם לא</returns>
        private bool sendMassage(MailMessage mail)
        {
            bool isMailSend = false;

            // למשלוח המייל SMTP הגדרת אובייקט  
            SmtpClient client = new SmtpClient();
            bool OnLive = ConfigurationHandler.IsLiveServer;
            // הגדרת השולח שיופיע לנמען - שם + כתובת
            mail.From = new System.Net.Mail.MailAddress(ConfigurationHandler.SenderAccountEmailAddress, ConfigurationHandler.SenderAccountEmailAddress);
            // קידוד נושא המייל
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            if (OnLive)
            {
                client.Host = ConfigurationHandler.SenderAccountSMTPHost;
            }
            else
            {
                // מילוי שם המשתמש והסיסמא למייל שדרכו תצא ההודעה
                client.Credentials = new System.Net.NetworkCredential(ConfigurationHandler.SenderAccountEmailAddress, ConfigurationHandler.SenderAccountEmailPassword);



                // הגדרת פורט היציאה של שרת ה - SMTP
                client.Port = ConfigurationHandler.SenderAccountSMTPPort;

                // הגדרת כתובת המארח של שרת ה - SMTP
                client.Host = ConfigurationHandler.SenderAccountSMTPHost;

                // הגדרת הרשאות אבטחה
                client.EnableSsl = ConfigurationHandler.SenderAccountEnableSsl;
            }


            try
            {
                // שליחת ההודעה
                client.Send(mail);
                isMailSend = true;
            }
            catch (Exception ex)
            {
                isMailSend = false;
                throw ex;
            }

            return isMailSend;
        }

        /// <summary>
        /// A method that generates the messsage with all the details (name, phne, email)
        /// </summary>
        /// <returns></returns>
        private string GenerateMessage(string firstName, string lastName, string phone, string email, string comments)
        {
            string message = "";
            message += "<tr><td colspan='2'>";
            message += String.Format("{0}:", GetText("CustomerCallbackDetails"));  // "פרטי הלקוח להתקשרות: <br />";
            message += "</td></tr>";

            message += "<tr><td>";
            message += String.Format("{0}:{1}", GetText("FirstName"), firstName); // "שם פרטי: ";
            message += "</td></tr>";

            message += "<tr><td>";
            message += String.Format("{0}:{1}", GetText("LastName"), lastName);   // "שם משפחה: ";
            message += "</td></tr>";

            message += "<tr><td>";
            message += String.Format("{0}:{1}", GetText("PhoneNumber"), phone);  // "טלפון: ";
            message += "</td></tr>";

            message += "<tr><td>";
            message += String.Format("{0}:{1}", GetText("EmailAddress"), email); // "כתובת דואר אלקטרוני: ";
            message += "</td></tr>";

            message += "<tr><td>";
            message += String.Format("{0}:{1}", GetText("Comments"), comments); // "הערות: ";
            message += "</td></tr>";

            return string.Format(
            @"
                <table>
                {0}
                </table>
            ", message);

        }


    }
}
