﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;
using BL_LowCost;

namespace LowCostSite.sass
{
    /// <summary>
    /// Summary description for AjaxHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AjaxHandler : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetCityInCountry(string countryCode)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            // city.Id, city.Name
            List<SimpleCity> citiesList = StaticData.CitiesListSimple.FindAll(c => c.CountryCode == countryCode).OrderBy(c => c.Name).ToList();
            string citiesString = js.Serialize(citiesList);
            return citiesString;
        }
    }
}
