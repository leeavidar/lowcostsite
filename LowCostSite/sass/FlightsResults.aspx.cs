﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LowCostSite.FlightsSearchService;
using Generic;
using DL_Generic;
using BL_LowCost;

namespace LowCostSite.sass
{
    /// <summary>
    /// Get airline company name from local db
    /// </summary>
    /// <param name="iataCode">iata code of airline company</param>
    /// <param name="airlineType">enum - type of airline: market/operated</param>
    /// <param name="serviceName">Airline name in case it does not reach the local DB</param>
    /// <returns></returns>
    public delegate string GetAirlineCompanyName(string iataCode, Generic.EnumHandler.AirlineTypes airlineType, string serviceName);
    public partial class FlightsResults : BasePage_UI
    {
        //private List<string> AirlinesCompanies { get; set; }

        //public event GetAirlineCompanyName EGetAirlineCompanyName;
        /// <summary>
        ///  Get airline company name from local db
        /// </summary>
        /// <param name="iataCode">iata code of airline company</param>
        /// <param name="airlineType">enum - type of airline: market/operated</param>
        /// <param name="serviceName">Airline name in case it does not reach the local DB</param>
        /// <returns></returns>
        public string GetAirlineCompanyName(string iataCode, Generic.EnumHandler.AirlineTypes airlineType, string serviceName)
        {
            var _airlineCodeName = _oOnlyAirlinesWasFounded.FindAllContainer(c => c.IataCode_Value == iataCode && c.AirlineTypes == airlineType).First;
            if (_airlineCodeName != null)
            {
                return _airlineCodeName.Name_Value;
            }
            else
            {
                return serviceName;
            }

        }
        #region Properties
        private FlightsAvailResults _oSessionFlightsAvailResults;
        private SearchParams _oSessionSearchParams;
        private AirlineContainer _oOnlyAirlinesWasFounded;

        #endregion

        #region Sessions Methodes

        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            ResultsToDisplay = (isPostBack && Session["ResultsToDisplay"] != null) ? (int)Session["ResultsToDisplay"] : 2;
            Generic.SessionManager.GetSession<FlightsAvailResults>(out _oSessionFlightsAvailResults);
            Generic.SessionManager.GetSession<SearchParams>(out _oSessionSearchParams);
            Generic.SessionManager.GetSession<AirlineContainer>(out _oOnlyAirlinesWasFounded);

        }

        /// <summary>
        /// save the containers to the sessions.
        /// if there are search results, the search parameters will be saved to the session also.
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<FlightsAvailResults>(oSessionFlightsAvailResults);
            Generic.SessionManager.SetSession<SearchParams>(oSessionSearchParams);
            Generic.SessionManager.SetSession<AirlineContainer>(oSessionAirlinesWasFounded);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            Session["ResultsToDisplay"] = null;
            Generic.SessionManager.ClearSession<FlightsAvailResults>(oSessionFlightsAvailResults);
            Generic.SessionManager.ClearSession<LowCostSite.FlightsBookingService.PricedItineraryDetails>(null);
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            // if there are no search params, the results are not relevant, and can be deleted
            if (!IsPostBack)
            {
                ResetAllSessions();
            }

            // if there are search params, load the search results
            LoadFromSession(IsPostBack);


        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            CallPageScript();
        }

        #endregion
    }

    public partial class FlightsResults : BasePage_UI
    {
        #region Properties
        public SearchParams oSessionSearchParams { get { return _oSessionSearchParams; } set { _oSessionSearchParams = value; } }
        public FlightsAvailResults oSessionFlightsAvailResults { get { return _oSessionFlightsAvailResults; } set { _oSessionFlightsAvailResults = value; } }
        public FlightsAvailResults FilteredFlightAvailResults { get; set; }
        public AirlineContainer oSessionAirlinesWasFounded { get { return _oOnlyAirlinesWasFounded; } set { _oOnlyAirlinesWasFounded = value; } }
        public string outwardFlightId { get { return hdn_OutwardFlightId.Value; } set { } }
        public string returnFlightId { get { return hdn_ReturnFlightId.Value; } set { } }
        public int ResultsToDisplay
        {
            get
            {
                return ConvertToValue.ConvertToInt(Session["ResultsToDisplay"]);
            }
            set
            {
                Session["ResultsToDisplay"] = value;
            }
        }
        public string SearchHashCode { get { return hf_SearchHashCode.Value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            // fill place holder and text that use GetText()
            AddTextBoxPlaceHolders();
            SetPage();
        }

        private void SetPage()
        {
            bool autoFilterAirline = false;
            //AirlinesCompanies = new List<string>();
            if (!IsPostBack)
            {
                #region first load
                // ONLY THE FIRST TIME THE PAGE LOADS:
                //----------------------------------------------------------------
                // There are two possible options: 
                // 1. The page was accessed directly (no search parameters and no results)
                // 2. The user got to the page from another search page: there are search parameters, and we need to start the search first

                DL_LowCost.City city = CityContainer.SelectByKeysView_IataCode("TLV", true).Single;
                hf_DefultTlvText.Value = string.Format("{0} (TLV)", city.Name_UI);

                // For case 1: no search results and no parameters (cusotmer got straight to the results page): 
                if (oSessionSearchParams == null && oSessionFlightsAvailResults == null)
                {
                    #region When no search was performed
                    // Fill the search panel
                    FillSearch();
                    // Tell the user to search for flights
                    DisplayNoSearchInDescriptionBox();
                    // Show search panel and hide filters panel:
                    searchPanel.Style.Add("display", "normal");
                    filterPanel.Style.Add("display", "none");
                    // In the results area- give a message that there was no search yet.
                    lbl_ResultsCounter.Text = GetText("NoSearchWasMade");
                    // Clear the search boxes and set new DDLs:
                    ClearSearchPanel();
                    #endregion
                }
                // For case 2: there are search parameters (customer selected search params in another page)
                else if (oSessionSearchParams != null)
                {
                    // Save the hash code of the search parameters on the page
                    hf_SearchHashCode.Value = oSessionSearchParams.GetHashCode().ToString();

                    // Fill the search panel and the hidden fields of the parameters
                    FillSearch();
                    // Set the DDls for use
                    SetDdls();
                    // Re-select the ddl items (numbers of passengers)
                    SelectDDLItems();

                    //Get the results from Search Service if there are no results in the session
                    if (oSessionFlightsAvailResults == null)
                    {
                        StartSearch(oSessionSearchParams);
                        hf_IsNewSeachMade.Value = "false";
                    }
                    if (oSessionFlightsAvailResults != null &&
                        oSessionFlightsAvailResults.FlightsCollection.Flights.FirstOrDefault() != null && oSessionFlightsAvailResults.FlightsCollection.Flights.Count() > 0)
                    {
                        // If there are results for the search

                        // Fill the flight description box and hide the noSearch div 
                        SetFlightDescriptionBox();
                        // Set the page to display the searcch results (using the initial number of results which is set in the web.config)

                        int countResults = SetUcFlightGroups(oSessionFlightsAvailResults, ConfigurationHandler.AmountOfSearchResultsGroups);
                        SetResultsCounter(countResults);
                        // Hide the search panel and tdisplay the filters panel
                        searchPanel.Style.Add("display", "none");
                        filterPanel.Style.Add("display", "normal");
                        // Set the filters with their initial values
                        if (_oSessionSearchParams.AirlinesFilter != "")
                        {
                            autoFilterAirline = true;
                            SetFilters(true, _oSessionSearchParams.AirlinesFilter);
                            #region Filter the results by airline
                            // Clone the search results from the session to a new object, by serializing and de-serializing it.
                            FlightsAvailResults FilteredResults = new FlightsAvailResults();
                            string strSer = oSessionFlightsAvailResults.SerializeToString();
                            FilteredResults = (FlightsAvailResults)XmlHandler.DeSerialize(strSer, typeof(FlightsAvailResults));

                            // FILTER BY AIRLINES
                            FilterByAirlines(FilteredResults);

                            // Set the page with the fileterd results
                            int ResultsDisplayedCount = SetUcFlightGroups(FilteredResults, ConfigurationHandler.AmountOfSearchResultsGroups);

                            // set the results counter (for example: "showing 5 out of 8 results")
                            SetResultsCounter(ResultsDisplayedCount);

                            // In case there are no results after filterring, display a message
                            if (FilteredResults == null || FilteredResults.FlightsCollection.Flights.Count() == 0)
                            {
                                btn_UnFiltered.Visible = true;

                                var simpleComapny = StaticData.AirlinessListSimple.FirstOrDefault(c => c.IataCode == oSessionSearchParams.AirlinesFilter);
                                string airlineName = simpleComapny != null ? simpleComapny.Name : "";
                                {
                                    lbl_ResultsCounter.Text = string.Format("{0} {1}. {2}", GetText("NoResultsFor"), airlineName, GetText("PleaseTryAnotherFilter")); GetText("NoResultsForYourFilters");
                                }

                            }
                            // Store the fileterd results for use in the auto loading function (btn_loadResults_Click)
                            FilteredFlightAvailResults = FilteredResults;
                            #endregion

                        }
                        else
                        {
                            SetFilters(true);
                        }

                        if (!IsPostBack)
                        {
                            // The first time, set the number of stops filter to 2 (maximum stops)
                            if (oSessionSearchParams.IsDirectFlight)
                            {
                                rb_Stops.SelectedIndex = 0;
                            }
                            else
                            {
                                rb_Stops.SelectedIndex = 2;
                            }
                        }
                        // Set the radio buttos and other j/s function needs to be bound for the flight in groups
                        AddScript("BindResultsAreaEvents()");
                    }
                    else
                    {
                        // The search didn't return any results- 
                        // give a messages about no resutls found
                        ph_FlightGroups.Controls.Clear();
                        lbl_ResultsCounter.Text = GetText("NoSearchResults");
                        DisplayNoSearchInDescriptionBox();
                        // Display the search panel, and hide the filters panel
                        searchPanel.Style.Add("display", "normal");
                        filterPanel.Style.Add("display", "none");
                    }
                }
                #endregion
            }
            else
            {
                #region Only on postbacks

                // if it is a post back (could be any click of a button (like showing details for a flight)
                // the user control should be re-created
                AvoidSessionConflicts();
                SetFilters(false);
                #endregion
            }

            #region Every time
            // if there are search results (they are already displayed) set the reuslts counter (above the results) and set the filter.
            if (!autoFilterAirline && oSessionFlightsAvailResults != null && oSessionFlightsAvailResults.FlightsCollection.Flights.Count() > 0 && oSessionFlightsAvailResults.FlightsCollection.Flights.FirstOrDefault() != null)
            {
                int displayedGroupsCount = SetUcFlightGroups(oSessionFlightsAvailResults, ConfigurationHandler.AmountOfSearchResultsGroups);
                SetResultsCounter(displayedGroupsCount);
            }
            #endregion
        }

        #region Event Handlers

        protected void btn_Search_Click(object sender, EventArgs e)
        {

            // save the search parameters as a SearchParams object, and in the session object
            // after this save, the search params object will store the parameters in formats that are acceptable by the search service 
            // (for dates: dd/mm/yyyy and for locations: IataCodes). This is done in the search params class setters.
            SearchParams oSearchParams = SaveSearchParams();

            if (hf_AutoCompleteDestinationCode.Value == "" || hf_AutoCompleteOriginCode.Value == "")
            {
                return;
            }

            StartSearch(oSearchParams);
            AddScript("initializationSlider()");
        }

        protected void btn_Filter_Click(object sender, EventArgs e)
        {    // if the user made a server request when he has anoher (newer) search results object, redirect him to the homepage.

            // if we check here for conflick in the hasch codes, in case the user made a new search, the hasch code will be diffenret   
            AvoidSessionConflicts();

            if (oSessionFlightsAvailResults != null)
            {
                // Clone the search results from the session to a new object, by serializing and de-serializing it.
                FlightsAvailResults FilteredResults = new FlightsAvailResults();
                string strSer = oSessionFlightsAvailResults.SerializeToString();
                FilteredResults = (FlightsAvailResults)XmlHandler.DeSerialize(strSer, typeof(FlightsAvailResults));

                // FILTER BY AIRLINES
                FilterByAirlines(FilteredResults);

                // FILTER BY NUMBER OF STOPS
                FilterByStopsNumber(FilteredResults);

                // FILTER BY PRICE RANGE
                FilterPriceRange(FilteredResults);

                // set the page with the fileterd results
                int ResultsDisplayedCount = SetUcFlightGroups(FilteredResults, ConfigurationHandler.AmountOfSearchResultsGroups);
                // set the results counter (for example: "showing 5 out of 8 results")
                SetResultsCounter(ResultsDisplayedCount);

                // in case there are no results after filterring, display a message
                if (FilteredResults == null || FilteredResults.FlightsCollection.Flights.Count() == 0)
                {
                    lbl_ResultsCounter.Text = GetText("NoResultsForYourFilters");
                    btn_UnFiltered.Visible = true;
                }
                else
                {
                    btn_UnFiltered.Visible = false;
                }

                // FINALLY:

                // For the datepickers to work again
                AddScript("BindSearchAreaEvents()");
                AddScript("initializationSlider()");
                AddScript("BindResultsAreaEvents()");

                // Set the flight radio buttons in groups
                up_Reults.Update();
                up_SideMenu.Update();

                // store the fileterd results for use in the auto loading function (btn_loadResults_Click)
                FilteredFlightAvailResults = FilteredResults;
            }
            else
            {
                // there are no results in the session.
            }
        }


        #endregion

        #region Helping methods
        public void MachSearchResultToLocal(FlightsAvailResults oFlightsAvailResults)
        {
            _oOnlyAirlinesWasFounded = new AirlineContainer();
            var oAllAirlinesContainer = AirlineContainer.SelectAllAirlines(true);
            foreach (var airlineGroups in oFlightsAvailResults.FlightsCollection.Flights)
            {
                if (airlineGroups == null)
                {
                    //for case flights list including null items
                    continue;
                }
                //yarin  28/09/14 - שמירת קונטיינר של חברות התעופה על מנת שנוכל להשתמש בשם המקומי ולא בשם מהסרויס
                var localMarketingAirline = oAllAirlinesContainer.FindAllContainer(a => a.IataCode_Value == GetIataCodeByFlight(airlineGroups, EnumHandler.AirlineTypes.MarketingAirline).IataCode).First.Clone();
                var localOperatingAirline = oAllAirlinesContainer.FindAllContainer(a => a.IataCode_Value == GetIataCodeByFlight(airlineGroups, EnumHandler.AirlineTypes.OperatingAirline).IataCode).First.Clone();
                //for MarketingAirline
                if (localMarketingAirline != null)
                {
                    localMarketingAirline.AirlineTypes = EnumHandler.AirlineTypes.MarketingAirline;
                    _oOnlyAirlinesWasFounded.Add(localMarketingAirline);
                }
                else
                {
                    localMarketingAirline = new DL_LowCost.Airline()
                    {
                        AirlineTypes = EnumHandler.AirlineTypes.MarketingAirline,
                        Name = airlineGroups.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().MarketingAirline.Name,
                        IataCode = airlineGroups.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().MarketingAirline.Code
                    };
                    _oOnlyAirlinesWasFounded.Add(localMarketingAirline);
                }
                //for localOperatingAirline
                if (localOperatingAirline != null)
                {
                    localOperatingAirline.AirlineTypes = EnumHandler.AirlineTypes.OperatingAirline;
                    _oOnlyAirlinesWasFounded.Add(localOperatingAirline);
                }
                else
                {
                    localOperatingAirline = new DL_LowCost.Airline()
                    {
                        AirlineTypes = EnumHandler.AirlineTypes.OperatingAirline,
                        Name = airlineGroups.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().OperatingAirline.Name,
                        IataCode = airlineGroups.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().OperatingAirline.Code
                    };
                    _oOnlyAirlinesWasFounded.Add(localOperatingAirline);
                }
                ///////////////////
            }

        }
        /// <summary>
        /// Searching for flight results using the provided search parameters,
        /// also displaying the results in the results page.
        /// </summary>
        /// <param name="oSearchParams">The search parameters</param>
        private void StartSearch(SearchParams oSearchParams)
        {

            if (!ValidateSearchParams(oSearchParams))
            {
                return;
            }
            //Init logger
            LoggerManagerProject.LoggerFile logger = new LoggerManagerProject.LoggerFile();

            // Get the new results for the search params object
            System.Diagnostics.Stopwatch searchTime = new System.Diagnostics.Stopwatch();
            searchTime.Start();
            FlightsAvailResults oFlightsAvailResults = Extensions.GetSearchResults(oSearchParams);
            searchTime.Stop();
            if (oFlightsAvailResults.FlightsCollection == null)
            {
                // there are no results

            }
            else
            {
                // remove empty groups (airlines without any results) - Inportant after filetering
                var _rebuildFlightCollection = new List<Flights>();
                for (int i = 0; i < oFlightsAvailResults.FlightsCollection.Flights.Count(); i++)
                {
                    if (oFlightsAvailResults.FlightsCollection.Flights[i] != null)
                    {
                        if (oFlightsAvailResults.FlightsCollection.Flights[i].Errors != null)
                        {
                            if (oFlightsAvailResults.FlightsCollection.Flights[i].Errors.DataSource.Count() > 0)
                            {
                                foreach (var item in oFlightsAvailResults.FlightsCollection.Flights[i].Errors.DataSource)
                                {
                                    logger.WriteWarrning(">> error founded in flight result , errorXML:{0}", item.XmlFile);
                                }
                                continue;
                            }
                        }
                        // if there are no results in this group, remove the group
                        if (oFlightsAvailResults.FlightsCollection.Flights[i].GroupList.DataSource.Count() == 0)
                        {
                            // oFlightsAvailResults.FlightsCollection.Flights[i] = null;
                        }
                        else
                        {
                            _rebuildFlightCollection.Add(oFlightsAvailResults.FlightsCollection.Flights[i]);
                        }
                    }
                    else
                    {
                        //this flight is null
                    }

                }
                oFlightsAvailResults.FlightsCollection.Flights = _rebuildFlightCollection.ToArray();
            }
            //math the search result wirh local airline companies
            MachSearchResultToLocal(oFlightsAvailResults);
            oSessionFlightsAvailResults = oFlightsAvailResults;

            // LOG
            if (oSessionFlightsAvailResults.FlightsCollection != null)
            {
                logger.WriteInfo(string.Format(">> Search results: Time: {0} results count: {1} >>> RESULTS:\n {2}", searchTime.Elapsed.ToString(), oSessionFlightsAvailResults.FlightsCollection.Flights.Count().ToString(), oSessionFlightsAvailResults.SerializeToString()));
            }


            // check if there are any results for the search
            if (oSessionFlightsAvailResults != null && oSessionFlightsAvailResults.FlightsCollection != null && oSessionFlightsAvailResults.FlightsCollection.Flights.Count() > 0)
            {
                //Fill the flight description box labels
                SetFlightDescriptionBox();
                // Set the paprameters of the filters
                // show the new results on the page
                int displayedResultsCount = SetUcFlightGroups(oSessionFlightsAvailResults, ConfigurationHandler.AmountOfSearchResultsGroups);
                SetFilters(true, _oSessionSearchParams.AirlinesFilter);

                // set the results counter (for example: showing 5 out of 8 results...)
                SetResultsCounter(displayedResultsCount);
                btn_UnFiltered.Visible = false;

                // set the flight radio buttons in groups
                AddScript("BindResultsAreaEvents()");


                // show the filters panel and hide the search panel
                filterPanel.Style.Remove("display");
                searchPanel.Style.Add("display", "none");
                btn_UnFiltered.Visible = false;

                // set the stops filter to the maximum (2 stops)
                if (oSessionSearchParams.IsDirectFlight)
                {
                    rb_Stops.SelectedIndex = 0;
                }
                else
                {
                    rb_Stops.SelectedIndex = 2;
                }

                up_Reults.Update();
                up_FlightDescriptionBox.Update();
                up_SideMenu.Update();
            }
            else
            {
                // In case there are no results, display a message
                lbl_ResultsCounter.Text = GetText("NoSearchResults");
                // Show the search panel again (and hide the filters)
                searchPanel.Style.Add("display", "normal");
                filterPanel.Style.Add("display", "none");

                // Show the "please search message and hide the flight description box
                DisplayNoSearchInDescriptionBox();

                up_SideMenu.Update();
                ph_FlightGroups.Controls.Clear();
                up_Reults.Update();
            }


            // Save the hash code of the search parameters in a hidden field on the page
            hf_SearchHashCode.Value = oSessionSearchParams.GetHashCode().ToString();
            // Hide the loading box
            AddScript("$('#myModal').modal('hide')");
            AddScript("BindSearchAreaEvents()");
        }

        /// <summary>
        /// A method that check the entered parameters before they are being sent to the service.
        /// if a parameters in wrong, it cancels the search.
        /// </summary>
        /// <param name="oSearchParams"></param>
        /// <returns></returns>
        private bool ValidateSearchParams(SearchParams oSearchParams)
        {
            return true;
        }

        /// <summary>
        /// A method that displays the results on the page
        /// </summary>
        /// <param name="SearchResults">The results to display</param>
        /// <returns>The number of items displayed (same as the AmountOfGroups or less)</returns>
        private int SetUcFlightGroups(FlightsAvailResults SearchResults, int AmountOfGroups)
        {
            ph_FlightGroups.Controls.Clear();
            List<Pair> uc_Dictionary = new List<Pair>();
            if (SearchResults != null)
            {
                // First sort the results by prices:
                SearchResults = OrderResultByPrice(SearchResults);

                UC_FlightGroup oUC_FlightGroup;
                int i = 0;
                // The airline groups are not displaed on the page (only the groups are displayed)
                foreach (Flights airlineGroups in SearchResults.FlightsCollection.Flights) //.Flights.Take(ResultsToDisplay))
                {
                    var oAirline = GetIataCodeByFlight(airlineGroups, EnumHandler.AirlineTypes.MarketingAirline);
                    try
                    {
                        if (airlineGroups != null)
                        {
                            foreach (Group FlightGroup in airlineGroups.GroupList.DataSource)
                            {
                                // If the flight is 2 way
                                if (!ConvertToValue.IsEmpty(oSessionSearchParams.ReturnDate) && (FlightGroup == null || (FlightGroup.OutwardFlights.DataSource == null || FlightGroup.ReturnFlights.DataSource == null) || (FlightGroup.OutwardFlights.DataSource.Count() == 0 || FlightGroup.ReturnFlights.DataSource.Count() == 0)))
                                {
                                    continue;
                                }
                                // The flight is 1 way 
                                else if (ConvertToValue.IsEmpty(oSessionSearchParams.ReturnDate) && (FlightGroup == null || (FlightGroup.OutwardFlights.DataSource == null || FlightGroup.ReturnFlights.DataSource == null) || (FlightGroup.OutwardFlights.DataSource.Count() == 0)))
                                {
                                    continue;
                                }
                                else
                                {
                                    oUC_FlightGroup = (UC_FlightGroup)LoadControl(StaticStrings.path_UC_FlightGroup);
                                    oUC_FlightGroup.AirLineIataCode = oAirline.IataCode;//localMarketingAirline.IataCode_Value;
                                    oUC_FlightGroup.AirLineLocleName = GetAirlineCompanyName(oAirline.IataCode, EnumHandler.AirlineTypes.MarketingAirline, oAirline.Name);// localMarketingAirline.Name_Value;
                                    oUC_FlightGroup.EGetAirlineCompanyName += GetAirlineCompanyName;
                                    oUC_FlightGroup.ID = string.Format("fligthGroup_{0}_{1}", airlineGroups.ID.ToString(), FlightGroup.GroupId);
                                    oUC_FlightGroup.SearchResults = SearchResults;
                                    oUC_FlightGroup.oSearchParams = oSessionSearchParams;
                                    oUC_FlightGroup.CompanyName = airlineGroups.Airline.DisplayName;
                                    // If the flights of this group have different currencies, set IsActive to false
                                    string currency = FlightGroup.OutwardFlights.DataSource.First().Price.Currency;
                                    if (ConvertToValue.IsEmpty(oSessionSearchParams.ReturnDate))
                                    {
                                        oUC_FlightGroup.IsActive = true;
                                    }
                                    else
                                    {
                                        oUC_FlightGroup.IsActive = !FlightGroup.ReturnFlights.DataSource.Any(flight => flight.Price.Currency != currency);
                                    }
                                    // ===================================================================================================================
                                    // This property's set function is using the other properties values to set the control, so it must be set after them.
                                    oUC_FlightGroup.FlightGroup = FlightGroup;
                                    // The airline id is transferred in order to make it easier to find the selected flight in the results object later (for the order button)
                                    oUC_FlightGroup.AirlineId = airlineGroups.ID.ToString();
                                    oUC_FlightGroup.Airline = airlineGroups.Airline;

                                    // Adding all the user control to a dictionary

                                    // CASE: OUTWARD FLIGHT ONLY:
                                    if (ConvertToValue.IsEmpty(oSessionSearchParams.ReturnDate))
                                    {
                                        uc_Dictionary.Add(new Pair(FlightGroup.OutwardFlights.DataSource[0].Price.TotalAmountWithMarkUp, oUC_FlightGroup));
                                    }
                                    else
                                    {
                                        uc_Dictionary.Add(new Pair(FlightGroup.OutwardFlights.DataSource[0].Price.TotalAmountWithMarkUp + FlightGroup.ReturnFlights.DataSource[0].Price.TotalAmountWithMarkUp, oUC_FlightGroup));
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        throw;
                    }
                }
                // Addinng the controls from the dictionary, to the place holder, sorted by price (the key)
                foreach (var uc in uc_Dictionary.OrderBy(d => ((decimal)d.First)))//.Take(AmountOfGroups))
                {
                    ph_FlightGroups.Controls.Add((UC_FlightGroup)uc.Second);
                }
            }
            // Return the total total number of results that are displayed
            return ph_FlightGroups.Controls.Count;
        }

        private SimpleAirline GetIataCodeByFlight(Flights airlineGroups, EnumHandler.AirlineTypes airlineType)
        {

            if (airlineGroups != null)
            {
                if (airlineGroups.GroupList != null)
                {
                    if (airlineGroups.GroupList.DataSource.Length > 0)
                    {
                        if (airlineGroups.GroupList.DataSource.First() != null)
                        {
                            if (airlineGroups.GroupList.DataSource.First().OutwardFlights != null)
                            {
                                if (airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.Length > 0)
                                {
                                    if (airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First() != null)
                                    {
                                        if (airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments != null)
                                        {
                                            if (airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments.DataSource.Length > 0)
                                            {
                                                switch (airlineType)
                                                {
                                                    case EnumHandler.AirlineTypes.MarketingAirline:
                                                        if (airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments.DataSource.First().MarketingAirline != null)
                                                        {
                                                            return new SimpleAirline()
                                                            {
                                                                IataCode = airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments.DataSource.First().MarketingAirline.Code,
                                                                Name = airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments.DataSource.First().MarketingAirline.Name
                                                            };
                                                        }
                                                        break;
                                                    case EnumHandler.AirlineTypes.OperatingAirline:
                                                        if (airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments.DataSource.First().OperatingAirline != null)
                                                        {
                                                            return new SimpleAirline()
                                                            {
                                                                IataCode = airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments.DataSource.First().OperatingAirline.Code,
                                                                Name = airlineGroups.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments.DataSource.First().OperatingAirline.Name
                                                            };
                                                        }
                                                        break;
                                                    default:
                                                        return new SimpleAirline();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return new SimpleAirline();
        }

        /// <summary>
        /// A method that stores the seach params from the text boxes in the search panel, to the oSessionSearchParams property (session object).
        /// </summary>
        private SearchParams SaveSearchParams()
        {
            if (hf_AutoCompleteOriginCode.Value == "TLV" || hf_AutoCompleteDestinationCode.Value == "TLV")
            {
                oSessionSearchParams = new SearchParams();
                oSessionSearchParams.Origin = hf_AutoCompleteOriginCode.Value;
                oSessionSearchParams.Destination = hf_AutoCompleteDestinationCode.Value;
                oSessionSearchParams.OutwardDate = txt_OutwardDate.Text;
                oSessionSearchParams.ReturnDate = txt_ReturnDate.Text;

                oSessionSearchParams.AdultsCount = ConvertToValue.ConvertToInt(ddl_Adults.SelectedValue);
                oSessionSearchParams.ChildrenCount = ConvertToValue.ConvertToInt(ddl_Children.SelectedValue);
                oSessionSearchParams.InfantsCount = ConvertToValue.ConvertToInt(ddl_Infants.SelectedValue);

                oSessionSearchParams.IsDirectFlight = cb_PreferDirectFlights.Checked;
                oSessionSearchParams.AirlinesFilter = "";

                hf_SearchHashCode.Value = oSessionSearchParams.GetHashCode().ToString();

                // the saveing to the session should be done on the page load complete event handler (in the base page)
                return oSessionSearchParams;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// A meothd that sets the labels of the flight description box at the top of the page.
        /// </summary>
        private void SetFlightDescriptionBox()
        {
            lbl_FlightOriginSmall.Text = SearchParams.GetLocationName(oSessionSearchParams.Origin);
            lbl_FlightDestSmall.Text = SearchParams.GetLocationName(oSessionSearchParams.Destination);
            lbl_OutwardDateSmall.Text = Extensions.GetDateWithDayName(ConvertToValue.ConvertToDateTime(oSessionSearchParams.OutwardDate));

            if (!ConvertToValue.IsEmpty(oSessionSearchParams.ReturnDate))
            {
                lbl_ReturnDateSmall.Text = Extensions.GetDateWithDayName(ConvertToValue.ConvertToDateTime(oSessionSearchParams.ReturnDate));
                searchDescriptionsDiv_Return_Div.Style.Add("display", "normal");
            }
            else
            {
                searchDescriptionsDiv_Return_Div.Style.Add("display", "none");
            }


            searchDescriptionsDiv.Attributes.CssStyle["display"] = "normal";
            noSearchDiv.Attributes.CssStyle["display"] = "none";
        }


        /// <summary>
        /// A method that sorrts the results of each group's outward and return flight by the price (with markup)
        /// </summary>
        /// <param name="oFlightsAvailResults">The search results to sort</param>
        /// <returns>The results after being sorted.</returns>
        private FlightsAvailResults OrderResultByPrice(FlightsAvailResults oFlightsAvailResults)
        {
            for (int i = 0; i < oFlightsAvailResults.FlightsCollection.Flights.Length; i++)
            {
                Flights flight = oFlightsAvailResults.FlightsCollection.Flights[i];
                if (flight != null)
                {
                    for (int j = 0; j < flight.GroupList.DataSource.Length; j++)
                    {
                        // Order the flight of each group by price (first is the cheapest)
                        oFlightsAvailResults.FlightsCollection.Flights[i].GroupList.DataSource[j].OutwardFlights.DataSource = oFlightsAvailResults.FlightsCollection.Flights[i].GroupList.DataSource[j].OutwardFlights.DataSource.Where(f => f != null).OrderBy(f => f.Price.TotalAmountWithMarkUp).ToArray();
                        oFlightsAvailResults.FlightsCollection.Flights[i].GroupList.DataSource[j].ReturnFlights.DataSource = oFlightsAvailResults.FlightsCollection.Flights[i].GroupList.DataSource[j].ReturnFlights.DataSource.Where(f => f != null).OrderBy(f => f.Price.TotalAmountWithMarkUp).ToArray();
                    }
                }
            }
            return oFlightsAvailResults;
        }

        /// <summary>
        /// Setting the ddls with the maximum possible passengers for each category without selecting.
        /// </summary>
        private void SetDdls()
        {
            // Set the maximum numbers using the data from the web.config:
            AddNumberItemsToDDL(ddl_Adults, ConfigurationHandler.MaxAdults, "0");
            //Remove the option for 0 adults (starts with at least 1)
            ddl_Adults.Items.RemoveAt(0);
            ddl_Adults.DataBind();
            AddNumberItemsToDDL(ddl_Children, ConfigurationHandler.MaxChildren, "0");
            AddNumberItemsToDDL(ddl_Infants, ConfigurationHandler.MaxInfants, "0");
        }

        /// <summary>
        /// A method that adds to a given DDL items that have both value and text of their index + 1 (1,2,3...) 
        /// </summary>
        /// <param name="MaxNum"></param>
        private void AddNumberItemsToDDL(DropDownList dropDown, int MaxNum, string FirstItemText, int selectedIndex = 0)
        {
            dropDown.Items.Add(new ListItem() { Value = "0", Text = FirstItemText });
            for (int i = 0; i < MaxNum; i++)
            {
                dropDown.Items.Add(new ListItem() { Value = (i + 1).ToString(), Text = (i + 1).ToString() });
            }
            dropDown.DataBind();
            dropDown.SelectedIndex = selectedIndex;
        }

        private void DisplayNoSearchInDescriptionBox()
        {
            searchDescriptionsDiv.Attributes.CssStyle["display"] = "none";
            noSearchDiv.Attributes.CssStyle["display"] = "normal";
        }

        private void DisplaySearchDescriptionsBox()
        {
            searchDescriptionsDiv.Attributes.CssStyle["display"] = "normal";
            noSearchDiv.Attributes.CssStyle["display"] = "none";
        }

        private void SelectDDLItems()
        {
            ddl_Adults.SelectedValue = oSessionSearchParams.AdultsCount.ToString();
            ddl_Children.SelectedValue = oSessionSearchParams.ChildrenCount.ToString();
            ddl_Infants.SelectedValue = oSessionSearchParams.InfantsCount.ToString();
        }

        private void ClearSearchPanel()
        {
            txt_Destination.Text = "";
            txt_Origin.Text = "";
            txt_OutwardDate.Text = "";
            txt_ReturnDate.Text = "";

            SetDdls();
        }

        /// <summary>
        /// A method that fills the search boxes with the results from the last page, and fills the hidden fields with the origin and destination codes.
        /// </summary>
        private void FillSearch()
        {
            if (oSessionSearchParams != null)
            {
                txt_Origin.Text = SearchParams.GetLocationName(oSessionSearchParams.Origin);
                txt_Destination.Text = SearchParams.GetLocationName(oSessionSearchParams.Destination);
                txt_OutwardDate.Text = oSessionSearchParams.OutwardDate;
                txt_ReturnDate.Text = oSessionSearchParams.ReturnDate;
                cb_PreferDirectFlights.Checked = oSessionSearchParams.IsDirectFlight;

                hf_AutoCompleteOriginCode.Value = oSessionSearchParams.Origin;
                hf_AutoCompleteDestinationCode.Value = oSessionSearchParams.Destination;
            }

        }

        /// <summary>
        /// A method that sets the text on the results counter to show how many results are displayed, out of a total number.
        /// </summary>
        /// <param name="displayedGroupsCount">The amount of results that are displayed (this is returned from the 'SetUcFlightGroups' method) </param>
        private void SetResultsCounter(int displayedGroupsCount)
        {
            // count the groups in the search results
            int totalResultsCount = 0;
            foreach (var flight in oSessionFlightsAvailResults.FlightsCollection.Flights)
            {
                if (flight != null)
                {
                    foreach (var group in flight.GroupList.DataSource)
                    {
                        totalResultsCount++;
                    }
                }

            }
            if (totalResultsCount != 0)
            {
                lbl_ResultsCounter.Text = string.Format("{0}: {1} {2} {3}", GetText("DisplayingResults"), displayedGroupsCount, GetText("OutOf"), totalResultsCount);
                hf_FlightsCount.Value = totalResultsCount.ToString();
                hf_FlightsDisplayedCount.Value = ResultsToDisplay.ToString();
            }

        }

        ///// <summary>
        /////  if the user made a server request when he has anoher (newer) search results object, redirect him to the homepage.
        /////  calls for this method are in: Filter click, 
        ///// </summary>
        private void AvoidSessionConflicts()
        {
            // If there are search params in the session, and there is a search hasch code in the hidden field (hf_SearchHashCode)
            if (_oSessionSearchParams != null && hf_SearchHashCode.Value != "")
            {
                // If the hash of the session searchParams object is DIFFERENT than the hash in page (in the hidden field)
                if (SearchHashCode != hf_SearchHashCode.Value)
                {
                    // If it is diffenret , it means that the page shows results from an older search, 
                    // diffenret than the results in the session, and the browser shold be redirected to the home page, 
                    // BUT ONLY if this is not intended (for example, if the user made a new search, the hash code in the page is still the old search, and the hash in the session will be of the new search params

                    // so..
                    if (hf_IsNewSeachMade.Value == "false")
                    {
                        Response.Redirect(StaticStrings.path_IndexPage);
                    }
                    else
                    {
                        hf_IsNewSeachMade.Value = "false";
                    }
                }
            }

            // IF the btn_Order button was clicked before re-entering this function, we need to check if the results are still in the session, and if not, redirect to the home page.
            if (hf_IsOrderClicked.Value == "true")
            {
                if (oSessionFlightsAvailResults == null)
                {
                    hf_IsOrderClicked.Value = "false";
                    // If there are no results in the session (and the order button was clicked)
                    Response.Redirect(StaticStrings.path_IndexPage);
                }
                else
                {
                    // If there are results + the order button was clicked,
                    //BUT the hashcode of the search params in the session is different than the hashcode of the search params for which the results are displayed 
                    if (_oSessionSearchParams != null && hf_SearchHashCode.Value != "" && _oSessionSearchParams.GetHashCode() != ConvertToValue.ConvertToInt32(hf_SearchHashCode.Value))
                    {
                        Response.Redirect(StaticStrings.path_IndexPage);
                    }
                }

            }

        }

        #region Filters


        /// <summary>
        /// A method that sets the filters using the results data.
        /// </summary>
        /// <param name="initAlso">True - if you want to set the first values also, or false to only build the filters.</param>
        private void SetFilters(bool initAlso = false, string airlineFilter = "")
        {
            #region Set the airlines filter

            if (oSessionFlightsAvailResults != null && oSessionFlightsAvailResults.FlightsCollection != null && oSessionFlightsAvailResults.FlightsCollection.Flights != null && oSessionFlightsAvailResults.FlightsCollection.Flights.FirstOrDefault() != null && oSessionFlightsAvailResults.FlightsCollection.Flights.FirstOrDefault().GroupList.DataSource.Length > 0)
            {
                Dictionary<string, string> Airlines = new Dictionary<string, string>();
                foreach (Flights airline in oSessionFlightsAvailResults.FlightsCollection.Flights)
                {
                    var airlineIataCode = airline.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().MarketingAirline;
                    if (!Airlines.ContainsKey(airlineIataCode.Code))
                    {
                        Airlines.Add(airlineIataCode.Code, GetAirlineCompanyName(airlineIataCode.Code, EnumHandler.AirlineTypes.MarketingAirline, airlineIataCode.Name));
                        //Airlines.Add(airline.Airline.DisplayName);
                        //Airlines.Add(GetCompnyNameByIataCode(airline.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().MarketingAirline.Code));
                    }
                }
                // Remove any duplicates:
                //var Airlines = AirlinesCompanies.Distinct().ToList();
                ph_AirlineFilter.Controls.Clear();

                foreach (var airlineItem in Airlines)
                {
                    CheckBox cb = new CheckBox();
                    cb.ID = string.Format("cb_AirlineFilter_{0}", airlineItem.Value.Replace(' ', '_'));
                    Label lbl = new Label() { AssociatedControlID = cb.ID, Text = airlineItem.Value };
                    if (initAlso)
                    {
                        cb.Checked = true;
                    }

                    if (airlineFilter != "")
                    {
                        // the airlineFilter contains the IATA code, and the results can only be filtered by the name, so we need to get the name by the iata code
                        var oAirline = StaticData.AirlinessListSimple.FindAll(airline => airline.IataCode == airlineFilter);
                        if (oAirline.Any())
                        {
                            // replace the iata code with the name of the airlne
                            airlineFilter = oAirline.First().IataCode.ToLower();
                        }
                        // if the current checkbox is of the airline that should be filtered, check this checkbox.
                        cb.Checked = (airlineItem.Key.ToLower().Replace(' ', '_') == airlineFilter.Replace(' ', '_'));
                    }
                    cb.Attributes.Add("data-airline_iata", airlineItem.Key);
                    cb.InputAttributes.Add("Value", airlineItem.Value);
                    cb.InputAttributes.Add("class", "cb_airline");
                    ph_AirlineFilter.Controls.Add(new LiteralControl("<div class='checkbox'>"));
                    ph_AirlineFilter.Controls.Add(cb);
                    ph_AirlineFilter.Controls.Add(lbl);
                    ph_AirlineFilter.Controls.Add(new LiteralControl("</div>"));
                }

            }
            #endregion

            // set the number of stops depending if the user checked the "Prefer direct flights" CB:
            if (_oSessionSearchParams.IsDirectFlight)
            {
                rb_Stops.Items[1].Attributes["style"] += " display:none; ";
                rb_Stops.Items[2].Attributes["style"] += " display:none; ";
            }
            else
            {
                if (rb_Stops.Items[1].Attributes["style"] != null)
                {
                    rb_Stops.Items[1].Attributes["style"] = rb_Stops.Items[1].Attributes["style"].Replace(" display:none; ", "");
                }
                if (rb_Stops.Items[2].Attributes["style"] != null)
                {
                    rb_Stops.Items[2].Attributes["style"] = rb_Stops.Items[2].Attributes["style"].Replace(" display:none; ", "");
                }
            }

            SetSliderParams();

        }

        /// <summary>
        /// A method that sets the paramsters of the price slider filter with data from the search results.
        /// </summary>
        private void SetSliderParams()
        {
            decimal? minPrice = decimal.MaxValue;
            decimal? maxPrice = 0;
            foreach (Flights airline in oSessionFlightsAvailResults.FlightsCollection.Flights)
            {
                if (airline != null)
                {
                    foreach (Group group in airline.GroupList.DataSource)
                    {
                        if (group != null)
                        {
                            foreach (Flight flight in group.OutwardFlights.DataSource)
                            {
                                if (flight != null && flight.Price.TotalAmountWithMarkUp < minPrice)
                                {
                                    minPrice = flight.Price.TotalAmountWithMarkUp;
                                }
                                if (flight != null && flight.Price.TotalAmountWithMarkUp > maxPrice)
                                {
                                    maxPrice = flight.Price.TotalAmountWithMarkUp;
                                }
                            }
                            foreach (Flight flight in group.ReturnFlights.DataSource)
                            {
                                if (flight != null && flight.Price.TotalAmountWithMarkUp < minPrice)
                                {
                                    minPrice = flight.Price.TotalAmountWithMarkUp;
                                }
                                if (flight != null && flight.Price.TotalAmountWithMarkUp > maxPrice)
                                {
                                    maxPrice = flight.Price.TotalAmountWithMarkUp;
                                }
                            }
                        }
                        else
                        {
                            // there is no group of flights.
                        }
                    }
                }
                else
                {
                    // there are no results.
                }
            }

            // set the parameters of the price slider filter
            string strMinPrice = Math.Floor(ConvertToValue.ConvertToDouble(minPrice)).ToString();
            string strMaxPrice = Math.Ceiling(ConvertToValue.ConvertToDouble(maxPrice + 1)).ToString();
            txt_PriceSlider.Attributes["data-slider-min"] = strMinPrice;
            txt_PriceSlider.Attributes["data-slider-max"] = strMaxPrice;
            txt_PriceSlider.Attributes["data-slider-value"] = string.Format("[{0},{1}]", strMinPrice, strMaxPrice);
            // To round the tooltip number (one decimal point) : in  botostrap-slider.js, in the layout option, added: .toFixed(1) 
        }

        /// <summary>
        /// A method that gets search results and filters them by selected airlines.
        /// </summary>
        /// <param name="SearchResults">The search results to filter</param>
        private void FilterByAirlines(FlightsAvailResults SearchResults)
        {
            if (SearchResults != null)
            {
                try
                {
                    List<string> selectedAirlines = new List<string>();
                    foreach (var cb in ph_AirlineFilter.Controls.OfType<CheckBox>())
                    {
                        if (cb.Checked)
                        {
                            // Add the display name to the selected airlines list
                            selectedAirlines.Add(cb.Attributes["data-airline_iata"]);
                        }
                    }
                    if (SearchResults.FlightsCollection != null && SearchResults.FlightsCollection.Flights != null)
                    {
                        SearchResults.FlightsCollection.Flights = (Flights[])(SearchResults.FlightsCollection.Flights.Where(airline => selectedAirlines.Contains(airline.GroupList.DataSource.FirstOrDefault().OutwardFlights.DataSource.FirstOrDefault().FlightSegments.DataSource.FirstOrDefault().MarketingAirline.Code)).ToArray());
                    }
                    else
                    {
                        // There is no flight collection in the results or there is no Flights object in the flight collection.
                    }
                }
                catch (Exception)
                {
                    Response.Redirect(StaticStrings.path_404);
                }

            }
            else
            {
                // There are no results to filter
            }

        }

        /// <summary>
        /// A method that gets search results and filters them by the number of stops.
        /// </summary>
        /// <param name="SearchResults">The search results to filter</param>
        private void FilterByStopsNumber(FlightsAvailResults SearchResults)
        {
            // Here we take the filtered results from the airline filter, and remove (set to null) any flight that has more then the defined amount of segments.

            int MaxStops = ConvertToValue.ConvertToInt(rb_Stops.SelectedValue);
            if (SearchResults != null && !ConvertToValue.IsEmpty(MaxStops) && SearchResults.FlightsCollection != null && SearchResults.FlightsCollection.Flights != null)
            {
                var oFlightsTempList = new List<Flights>();
                for (int i = 0; i < SearchResults.FlightsCollection.Flights.Length; i++)
                {
                    Flights oFlights = SearchResults.FlightsCollection.Flights[i];

                    if (oFlights != null && oFlights.GroupList != null && oFlights.GroupList.DataSource != null)
                    {
                        var oGroupListTemp = new List<Group>();
                        for (int j = 0; j < oFlights.GroupList.DataSource.Length; j++)
                        {
                            Group oGroup = oFlights.GroupList.DataSource[j];
                            // Create a new temp list for the matching outward flights:
                            List<Flight> oTempFlightOutward = new List<Flight>();
                            if (oGroup != null && oGroup.OutwardFlights != null && oGroup.OutwardFlights.DataSource != null)
                            {
                                for (int k = 0; k < oGroup.OutwardFlights.DataSource.Length; k++)
                                {
                                    Flight oFlight = oGroup.OutwardFlights.DataSource[k];
                                    if (oFlight.FlightSegments.DataSource.Count() - 1 <= MaxStops)
                                    {
                                        oTempFlightOutward.Add(oFlight);
                                    }
                                }
                                // Take the matching results (outward), and add them to the filtered results
                                oGroup.OutwardFlights.DataSource = oTempFlightOutward.ToArray();
                            }

                            // Create a new temp list for the matching return flights:
                            List<Flight> oTempFlightReturn = new List<Flight>();

                            if (oGroup != null && oGroup.ReturnFlights != null && oGroup.ReturnFlights.DataSource != null && oGroup.OutwardFlights.DataSource.Length > 0)
                            {
                                for (int n = 0; n < oGroup.ReturnFlights.DataSource.Length; n++)
                                {
                                    Flight oFlight = oGroup.ReturnFlights.DataSource[n];
                                    if (oFlight.FlightSegments.DataSource.Count() - 1 <= MaxStops)
                                    {
                                        oTempFlightReturn.Add(oFlight);
                                    }
                                }
                                // Take the matching results (return), and add them to the filtered results
                                oGroup.ReturnFlights.DataSource = oTempFlightReturn.ToArray();
                            }
                            if (oGroup.OutwardFlights.DataSource.Length != 0)
                            {
                                oGroupListTemp.Add(oGroup);
                            }
                        }
                        if (oGroupListTemp.Count > 0)
                        {
                            oFlights.GroupList.DataSource = oGroupListTemp.ToArray();
                            oFlightsTempList.Add(oFlights);
                        }
                    }
                }
                SearchResults.FlightsCollection.Flights = oFlightsTempList.ToArray();
            }
            else
            {
                // There are no search results to filter.
            }
        }

        /// <summary>
        /// A method that gets search results and filters them by their price.
        /// </summary>
        /// <param name="SearchResults">The search results to filter</param>
        private void FilterPriceRange(FlightsAvailResults SearchResults)
        {
            decimal minPrice = ConvertToValue.ConvertToDecimal(hf_MinPrice.Value);
            decimal maxPrice = ConvertToValue.ConvertToDecimal(hf_MaxPrice.Value);

            if (SearchResults != null && SearchResults.FlightsCollection != null && SearchResults.FlightsCollection.Flights != null)
            {
                var oFlightsTempList = new List<Flights>();
                for (int i = 0; i < SearchResults.FlightsCollection.Flights.Length; i++)
                {
                    Flights oFlights = SearchResults.FlightsCollection.Flights[i];
                    if (oFlights != null && oFlights.GroupList != null && oFlights.GroupList.DataSource != null)
                    {
                        var oGroupListTemp = new List<Group>();
                        for (int j = 0; j < oFlights.GroupList.DataSource.Length; j++)
                        {
                            Group oGroup = oFlights.GroupList.DataSource[j];
                            // Create a new temp list for the matching outward flights:
                            List<Flight> oTempFlightOutward = new List<Flight>();
                            if (oGroup != null && oGroup.OutwardFlights != null && oGroup.OutwardFlights.DataSource != null)
                            {
                                for (int k = 0; k < oGroup.OutwardFlights.DataSource.Length; k++)
                                {
                                    Flight oFlight = oGroup.OutwardFlights.DataSource[k];
                                    if (oFlight.Price.TotalAmountWithMarkUp >= minPrice && oFlight.Price.TotalAmountWithMarkUp <= maxPrice)
                                    {
                                        oTempFlightOutward.Add(oFlight);
                                    }
                                }

                                // Take the matching results (outward), and add them to the filtered results
                                oGroup.OutwardFlights.DataSource = oTempFlightOutward.ToArray();
                            }

                            // Create a new temp list for the matching return flights:
                            List<Flight> oTempFlightReturn = new List<Flight>();

                            if (oGroup != null && oGroup.ReturnFlights != null && oGroup.ReturnFlights.DataSource != null)
                            {
                                for (int n = 0; n < oGroup.ReturnFlights.DataSource.Length; n++)
                                {
                                    Flight oFlight = oGroup.ReturnFlights.DataSource[n];
                                    if (oFlight.Price.TotalAmountWithMarkUp >= minPrice && oFlight.Price.TotalAmountWithMarkUp <= maxPrice)
                                    {
                                        oTempFlightReturn.Add(oFlight);
                                    }
                                }
                                // Take the matching results (return), and add them to the filtered results
                                oGroup.ReturnFlights.DataSource = oTempFlightReturn.ToArray();
                            }
                            if (oGroup.OutwardFlights.DataSource.Length != 0)
                            {
                                oGroupListTemp.Add(oGroup);
                            }
                        }
                        if (oGroupListTemp.Count > 0)
                        {
                            oFlights.GroupList.DataSource = oGroupListTemp.ToArray();
                            oFlightsTempList.Add(oFlights);
                        }
                    }
                }
                SearchResults.FlightsCollection.Flights = oFlightsTempList.ToArray();
                txt_PriceSlider.Attributes["data-slider-value"] = string.Format("[{0},{1}]", hf_MinPrice.Value, hf_MaxPrice.Value);
            }
            else
            {
                // there are no search results to filter.
            }
        }

        /// <summary>
        ///  A method that fills all the placeholders and text attributes of the elemets that use GetText()
        /// </summary>
        protected void AddTextBoxPlaceHolders()
        {
            txt_Origin.Attributes["placeholder"] = GetText("SelectDepartureFrom");
            txt_Destination.Attributes["placeholder"] = GetText("SelectArrivalTo");
            txt_OutwardDate.Attributes["placeholder"] = GetText("SelectDepartureDate");
            txt_ReturnDate.Attributes["placeholder"] = GetText("SelectReturnDate");

            btn_Filter.Text = GetText("FilterResults");

            btn_Search.Text = GetText("Search");
            btn_UnFiltered.Text = GetText("ShowAllResults");
            rb_Stops.Items[0].Text = GetText("DirectFlightsOnly");
            rb_Stops.Items[1].Text = GetText("OneStopMax");
            rb_Stops.Items[2].Text = GetText("AllResults");

            cb_PreferDirectFlights.Text = GetText("DirectFlightsPreference");
        }

        #endregion

        #endregion
    }
}