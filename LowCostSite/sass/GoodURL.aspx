﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="GoodURL.aspx.cs" Inherits="LowCostSite.sass.GoodURL" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="innerFrame">
<head id="Head1" runat="server">
    <title></title>
    <link href="/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="/css/StyleSheet.css" rel="stylesheet" />
    <link href="/css/StyleSheetUI.css" rel="stylesheet" />
    <link href="/css/normalize.css" rel="stylesheet" type="text/css" />
    <link href="/css/wireframe.css" rel="stylesheet" type="text/css" />
    <link href="/css/main.css" rel="stylesheet" type="text/css" />
    <link href="/css/media.css" rel="stylesheet" type="text/css" />
    <link href="/css/custom.less" rel="stylesheet/less" type="text/css" />
    <!-- RoyalSlider -->
    <link rel="stylesheet" href="/royalslider/royalslider.css" />
    <link rel="stylesheet" href="/css/plugins.css" />
    <link rel="stylesheet" href="/js/plugins/pretty-photo/css/prettyPhoto.css" type="text/css" />
    <link href="/css/fonts.googleapis.css" rel="stylesheet" />
</head>
<body class="iframeBody">
    <form id="form1" runat="server" style="font-family:'Roboto Condensed';">
        <h1 class="color_blue" style="text-align: center;font-size: 60px;"><%=GetText("GOOD_URL_TEXT_1") %></h1>
        <div class="hr-breadcrumbs divider-heder" style="color:#474748; ">
            <div class="assistive-text"></div>
            <ol class="breadcrumbs wf-td text-small">
            </ol>
        </div>
        <section class="shortcode-action-box shortcode-action-bg plain-bg text-centered no-line" style="color:#474748; ">
            <div class="shortcode-action-container">
                <div class="shortcode-action-container">
                    <div class=" fs35">
                        <%=GetText("GOOD_URL_TEXT_2") %>
                        <%=GetText("GOOD_URL_TEXT_3") %>
                        <div>
                            <%=GetText("RESERVATION_NUMBER") %> <span class="color_blue"> <%=OrderDocID.ToString()%> </span>
                            <br />
                            <br />
                        </div>
                        <%=GetText("GOOD_URL_TEXT_4") %><br />
                        <br />
                        <span class="color_blue"><%=WhiteLabelEmail%> </span>
                    </div>
                </div>
            </div>
        </section>
        <iframe style="display: none; background-color: transparent" id="iframeOrderSummery" runat="server" min-height="2000" width="1000"></iframe>
    </form>
</body>
</html>

