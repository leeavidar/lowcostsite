﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using DL_LowCost;
using Generic;

namespace LowCostSite.sass
{
    public partial class Policies : BasePage_UI
    {
        #region Session Private fields

        private StaticPagesContainer _oSessionStaticPages;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions (or from the database if its the first load of the page or the session is empty)
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<StaticPagesContainer>(out _oSessionStaticPages) == false)
            {
                //take the data from the table in the database
                _oSessionStaticPages = GetStaticPagesFromDB();
            }
            else { /*Return from session*/}

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<StaticPagesContainer>(oSessionStaticPages);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            Generic.SessionManager.ClearSession<StaticPagesContainer>(oSessionStaticPages);
        }


 
        private StaticPagesContainer GetStaticPagesFromDB()
        {
            //selecting all the static pages with a given white label
            return StaticPagesContainer.SelectAllStaticPagess(BasePage_UI.GetWhiteLabelDocID(), null);
        }
        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            //CallPageScript();
        }

        #endregion

    }

    public partial class Policies : BasePage_UI
    {

        #region Properties

        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public StaticPagesContainer oSessionStaticPages { get { return _oSessionStaticPages; } set { _oSessionStaticPages = value; } }
        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetPage();
            }

        }


        /// <summary>
        /// A method that sets all the page elements with data from the DB
        /// </summary>
        private void SetPage()
        {
            Master.SetMenuClass(EnumHandler.MenuPages.Policies);

            if (oSessionStaticPages != null)
            {

                StaticPages oStaticPages = _oSessionStaticPages.FindFirstOrDefault(page => page.Name == "Policies");


                if (oStaticPages != null)
                { 
                    // set the page meta data with the data in the seo property.
                    Master.SetMetaData(oStaticPages.SeoSingle);
                    div_policies.InnerHtml = oStaticPages.Text_UI;
                }
                else
                {
                    div_policies.InnerHtml = "There is no content in the database to display.";
                }
            }
        
        }
    }

}