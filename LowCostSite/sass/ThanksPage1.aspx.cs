using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using LowCostSite.FlightsBookingService;
using LowCostSite.FlightsSearchService;
using LowCostSite.sass.Controls;

namespace LowCostSite.sass
{
    public partial class ThanksPageOld : BasePage_UI
    {
        #region Session Private fields

        private FlightIssuedDetails _oFlightIssuedDetails;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (Generic.SessionManager.GetSession<FlightIssuedDetails>(out _oFlightIssuedDetails) == true)
            {
                
            }
            else { /*Return from session*/}

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<FlightIssuedDetails>(oSessionFlightIssuedDetails);


        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions() { }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            LoadFromSession(IsPostBack);
        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            //CallPageScript();
        }

        #endregion


    }
    public partial class ThanksPageOld : BasePage_UI
    {

        #region Properties
        #region Private

        #endregion
        #region Public

        #region Inputs
        #endregion

        public FlightIssuedDetails oSessionFlightIssuedDetails { get { return _oFlightIssuedDetails; } set { _oFlightIssuedDetails = value; } }



        #endregion

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPageInfo();
        }

        private void SetPageInfo()
        {
            lbl_airlinePNR.Text = oSessionFlightIssuedDetails.TicketingDetails.PNR;
            lbl_TFPNR.Text = oSessionFlightIssuedDetails.TicketingDetails.SupplierBookingReference;
        }

    }


}