﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using DL_LowCost;
using DL_Generic;
using Generic;
using System.IO;
using LowCostSite.sass;
using LowCostSite.FlightsBookingService;


namespace LowCostSite.sass
{
    public partial class PaymentFrame : BasePage_UI
    {
        #region Session Private fields
        private SelectedItinerary _oSelectedItinerary;
        #endregion

        #region Session Methods

        /// <summary>
        /// load the containers from the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            // Must - GUID
            // TODO : Load order object (session/xml/db)
            SessionManager.GetSession<OrderObject>(out oSessionOrderObject);
            //if it's the first time the pages is loaded, or the selected itinerary from the search is not in the session
            SessionManager.GetSession(out _oSelectedItinerary);
        
        }


        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Session["OrderObject"] = oSessionOrderObject;
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {            
            //No need to delete order session - this session will be deleted after payment
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              //  ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        //protected void Page_LoadComplete(object sender, EventArgs e) Page_Loade Event is exist in the BasePage
        //{
        //    SaveToSession();
        //}
        /// <summary>
        /// A method that generates a string for a flight segment (from => to , 00:00)
        /// </summary>
        /// <param name="segment">The flight segment to create the string for.</param>
        /// <param name="IsArrival">Is it the arrival part (if it is, the time is time of arrival and not of departure)</param>
        /// <returns>The text describing the flight segment</returns>
        protected string GenerateSegmentString(FlightsSearchService.FlightSegment segment, bool IsArrival)
        {
            string segmentString;
            if (IsArrival)
            {
                segmentString = string.Format("{0} - {1},  {2}", Extensions.GetAirportName(segment.ArrivalAirport.LocationCode), Extensions.GetCityOfAirport(segment.ArrivalAirport.LocationCode), segment.ArrivalDateTime.ToString("HH:mm"));
            }
            else
            {
                segmentString = string.Format("{0} - {1},  {2}", Extensions.GetAirportName(segment.DepartureAirport.LocationCode), Extensions.GetCityOfAirport(segment.DepartureAirport.LocationCode), segment.DepartDateTime.ToString("HH:mm"));
            }
            return segmentString;
        }

        #endregion

    }

    public partial class PaymentFrame : BasePage_UI
    {
        OrderObject oSessionOrderObject;
        public SelectedItinerary oSelectedItinerary { get { return _oSelectedItinerary; } set { _oSelectedItinerary = value; } }
        private Dictionary<FlightsSearchService.ECustomerType, int> passengerList;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Dudu : Check which page should be load in the iframe
            if (oSessionOrderObject == null)
            {
                //Cannot find order session - return to home page if the seesion of the order is null
                Response.Redirect(StaticStrings.path_IndexPage, false);
            }
            else
            {
                #region Set the real white label for page display
                WhiteLabel oWhiteLabel = WhiteLabelContainer.SelectByKeysView_DocId(ConvertToValue.ConvertToInt(Request.QueryString["wlID"]), true).Single;
                if (oWhiteLabel != null)
                {
                    (this.Master as LowCostSite.Master.MasterPage).setPageByWhithLabel(oWhiteLabel);
                }
                else
                {
                    LoggerManagerProject.LoggerManager.Logger.WriteWarrning(string.Format("{0}Page: PaymentFrame{0}Error: WhiteLabel not find From QueryString, QueryString was: {1}", Environment.NewLine, Request.QueryString["wlID"]));
                }
                #endregion

                SetPage();
                // if the secure connection is required for backend and the current 
                // request doesn't use SSL, redirecting the request to be secure
                if (!HttpContext.Current.Request.IsSecureConnection && IsPaymentSecureConnection)
                {
                    string absoluteUri = Request.Url.AbsoluteUri;
                    Response.Redirect(absoluteUri.Replace("http://", "https://"), true);
                }

                //Have active ordersession - set iframe
                if (DebugMode && !DebugModeMoveToPelecard)
                {
                    //Show debug mode page
                    iFramePayment.Src = string.Format("Debugmode.aspx{0}", Request.Url.Query);
                }
                else
                {
                    //Show Pelected 
                    iFramePayment.Src = string.Format("PelecardPayment.aspx{0}", Request.Url.Query);
                }
            }
        }

        private void SetPage()
        {
            SetPageInfo();
        }
        private void SetPageInfo()
        {
           // If there is no selected itinerary transferred from a previous page, redirect to the search results page.
            if (oSelectedItinerary == null)
            {
                Response.Redirect(StaticStrings.path_FlightsResults);
            }
            else
            {
                #region Details summary block (top right box)
                // Origin and Destinations labels
                lbl_Origin.InnerText = Extensions.GetLocationName(oSelectedItinerary.SelectedFlights.Origin);
                lbl_Destination.InnerText = Extensions.GetLocationName(oSelectedItinerary.SelectedFlights.Destination);
                // Outward Date and Return Date (with the day of week)
                #region Outward Date and Return Date (with the day of week)
                lbl_OutwardDate.InnerText = Extensions.GetDateWithDayName(oSelectedItinerary.SelectedFlights.Summary.OutwardDate);
                if (oSelectedItinerary.SelectedFlights.ReturnDate == ConvertToValue.DateTimeEmptyValue)
                {
                    FlightDetails_ReturnDate.Visible = false;
                }
                else
                {
                    lbl_ReturnDate.InnerText = Extensions.GetDateWithDayName(oSelectedItinerary.SelectedFlights.Summary.ReturnDate);
                }
                #endregion

                // Create an empty prices container and empty segments containers
                LowCostSite.FlightsSearchService.Pricing oPriceContainerOutward = null;
                LowCostSite.FlightsSearchService.Pricing oPriceContainerReturn = null;
                LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerOutward = null;
                LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerReturn = null;


                FlightsSearchService.FlightContainer outwardFlightContainer = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().OutwardFlights;
                FlightsSearchService.FlightContainer returnFlightContainer = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().ReturnFlights;

                oPriceContainerOutward = outwardFlightContainer.DataSource.FirstOrDefault().Price;
                oSegmentLegContainerOutward = outwardFlightContainer.DataSource.First().FlightSegments;

                // If there are return flight segments
                if (returnFlightContainer != null && returnFlightContainer.DataSource.FirstOrDefault() != null)
                {
                    // Get the price object
                    oPriceContainerReturn = returnFlightContainer.DataSource.FirstOrDefault().Price;
                    oSegmentLegContainerReturn = returnFlightContainer.DataSource.First().FlightSegments;
                }

                // Get the handling fee from the flight group (for a single passenger)
                var handlingFee = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().Price.HandlingFee;

                // Fill the prices breakdown for adults, children, and infants, and the handling fee
                FillPrices(handlingFee, oPriceContainerOutward, oPriceContainerReturn);
                // Fill the boxes of descriptions for the outward and return flights (general flight info and segemnts ).
                FillFlightsDetails(oSegmentLegContainerOutward, oSegmentLegContainerReturn);

                #endregion
            }

        }
        /// <summary>
        /// A method that sets the flight description box with the prices for each passenger type (breakdown), handling fee, and total price.e
        /// </summary>
        /// <param name="oPassengerPriceContainer"></param>
        private void FillPrices(decimal HandlingFee, LowCostSite.FlightsSearchService.Pricing oPriceContainerOutward, LowCostSite.FlightsSearchService.Pricing oPriceContainerReturn)
        {
            // passneger count
            int numOfAdults = oSessionOrderObject.SearchParams.AdultsCount;
            int numOfChildren = oSessionOrderObject.SearchParams.ChildrenCount;
            int numOfInfants = oSessionOrderObject.SearchParams.InfantsCount;

            // Prices 
            decimal totalNetPrice = 0, totalHandlingFee = 0, SumAdultsPrice = 0, sumChildrenPrice = 0, sumInfantsPrice = 0;

            bool isPassengerPrices = false;

            int passengerNumber = oSessionOrderObject.SearchParams.AdultsCount + oSessionOrderObject.SearchParams.ChildrenCount + oSessionOrderObject.SearchParams.InfantsCount;
            string currencyCode = oPriceContainerOutward.Currency;
            string currency = Extensions.GetCurrencySymbol(oPriceContainerOutward.Currency);
            if (oPriceContainerOutward != null)
            {
                // Add the handling fee for each passenger for the outward segment
                totalHandlingFee += passengerNumber * HandlingFee;
                decimal Delta = oSessionOrderObject.DeltaNetPrice;
                decimal deltaNetPricePerPassenger = Delta / passengerNumber;
                // If there is a price per passenger breakdown:
                if (oPriceContainerOutward.PassengerPrices != null && oPriceContainerOutward.PassengerPrices.DataSource.Count() > 0)
                {
                    isPassengerPrices = true;
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.ADT))
                    {
                        SumAdultsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp) + deltaNetPricePerPassenger;
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.CNN))
                    {
                        sumChildrenPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp) + deltaNetPricePerPassenger;
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.INF))
                    {
                        sumInfantsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp) + deltaNetPricePerPassenger;
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                }
                else
                {
                    //In case there is no price per passenger, set equal prices per passenger
                    totalNetPrice += oPriceContainerOutward.Amount;
                    // Calc the equal price
                    decimal pricePerPassenger = oPriceContainerOutward.TotalAmountWithMarkUp.Value / passengerNumber;
                    // Set this price, times the number of passenger in each category
                    SumAdultsPrice += numOfAdults * pricePerPassenger;
                    sumChildrenPrice += numOfChildren * pricePerPassenger;
                    sumInfantsPrice += numOfInfants * pricePerPassenger;
                }
            }
            if (oPriceContainerReturn != null)
            {
                // Multiply the handling fee by 2 for the return flight
                totalHandlingFee *= 2;

                if (oPriceContainerReturn.PassengerPrices != null && oPriceContainerReturn.PassengerPrices.DataSource.Count() > 0)
                {
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.ADT))
                    {
                        SumAdultsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.CNN))
                    {
                        sumChildrenPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.INF))
                    {
                        sumInfantsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                        totalNetPrice += ConvertToValue.ConvertToDecimal(item.Amount);
                    }
                }
                else
                {
                    if (!isPassengerPrices)
                    {
                        // there is no price per passenger
                        totalNetPrice += oPriceContainerReturn.Amount;
                        // when there is no breakdown - store the total price with markup in the adult var, and in summary it will sum the SumAdultsPrice with the childer and infants, which will be both 0.
                        decimal pricePerPassenger = oPriceContainerReturn.TotalAmountWithMarkUp.Value / passengerNumber;
                        SumAdultsPrice += numOfAdults * pricePerPassenger;
                        sumChildrenPrice += numOfChildren * pricePerPassenger;
                        sumInfantsPrice += numOfInfants * pricePerPassenger;
                    }
                }
            }

            passengerList = new Dictionary<FlightsSearchService.ECustomerType, int>();
            if (numOfAdults > 0)
            {
                lbl_PriceBox_AdultCount.Text = numOfAdults.ToString();
                lbl_PriceBox_AdultPrice.Text = string.Format("{0} {1}", currency, SumAdultsPrice.ToString("0.00"));
                div_AdultPrice.Attributes["class"] = "row";
                passengerList.Add(FlightsSearchService.ECustomerType.ADT, numOfAdults);
            }
            if (numOfChildren > 0)
            {
                lbl_PriceBox_ChildCount.Text = numOfChildren.ToString();
                lbl_PriceBox_ChildPrice.Text = string.Format("{0} {1}", currency, sumChildrenPrice.ToString("0.00"));
                div_ChildPrice.Attributes["class"] = "row";
                passengerList.Add(FlightsSearchService.ECustomerType.CNN, numOfChildren);
            }
            if (numOfInfants > 0)
            {
                lbl_PriceBox_InfantCount.Text = numOfInfants.ToString();
                lbl_PriceBox_InfantPrice.Text = string.Format("{0} {1}", currency, sumInfantsPrice.ToString("0.00"));
                div_InfantPrice.Attributes["class"] = "row";
                passengerList.Add(FlightsSearchService.ECustomerType.INF, numOfInfants);
            }
            if (HandlingFee > 0)
            {
                lbl_PassengerNumber.Text = passengerNumber.ToString();
                lbl_HandlingFee.Text = string.Format("{0} {1}", currency, totalHandlingFee.ToString("0.00"));
                div_HandlingFee.Attributes["class"] = "row";
            }
            // Caculate the total price (with handling fee)
            decimal totalPriceWithHandlingFee = SumAdultsPrice + sumChildrenPrice + sumInfantsPrice + totalHandlingFee;

            // Save the total Price in the view state
            oSessionOrderObject.TotalPriceWithHandling = totalPriceWithHandlingFee;

            lbl_TotalPrice.Text = string.Format("{0} {1}", totalPriceWithHandlingFee.ToString("0.00"), currency);
            //lbl_FooterTotalPrice.Text = string.Format("{0} {1}", currency, totalPriceWithHandlingFee.ToString("0.00"));

            // only if its the first time
            if (oSessionOrderObject.NetPriceAndCurrency == null)
            {
                oSessionOrderObject.NetPriceAndCurrency = new OrderPrice(totalNetPrice, currencyCode);
            }
            // LOGGER
            LoggerManagerProject.LoggerManager.LoggeSpecific.WriteInfo(string.Format("OrderPAge => FillPrices() => oSessionFlightBookTotalNetPriceAndCurrency => Price: {0} | Currency: {1}", totalNetPrice, currencyCode));
        }

        /// <summary>
        /// A method that fills the flights segments on the details flights box
        /// </summary>
        /// <param name="oPassengerPriceContainer"></param>
        private void FillFlightsDetails(LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerOutward, LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerReturn)
        {
            rpd_OutwardSegments.DataSource = oSegmentLegContainerOutward.DataSource;
            rpd_OutwardSegments.DataBind();
            if (oSegmentLegContainerReturn != null)
            {
                div_outwardFlightsDetails.Attributes.Remove("class");

                rpd_ReturnSegments.DataSource = oSegmentLegContainerReturn.DataSource;
                rpd_ReturnSegments.DataBind();
            }
        }
    }
}