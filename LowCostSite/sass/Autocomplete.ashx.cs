﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

namespace LowCostSite
{
    /// <summary>
    /// Summary description for Autocomplete
    /// </summary>
    public class Autocomplete : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            List<AutocompleteItem> data = new List<AutocompleteItem>();
            data.Add(new AutocompleteItem() {  IataCode = "TLV" , Type = 1 , Text = "Tel Aviv" });
            data.Add(new AutocompleteItem() { IataCode = "LON", Type = 1, Text = "London" });
            data.Add(new AutocompleteItem() { IataCode = "BCN", Type = 1, Text = "Barcelona" });
           
           
            context.Response.ContentType = "application/json";
            JsonSerializer serializer = new JsonSerializer();

            context.Response.Write(JsonConvert.SerializeObject(data));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    [System.Runtime.Serialization.DataContract]
    [Serializable]
    public class AutocompleteItem
    {
        [System.Runtime.Serialization.DataMember]
        public string Text { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string IataCode { get; set; }
        [System.Runtime.Serialization.DataMember]
        public int Type { get; set; }
    }

}