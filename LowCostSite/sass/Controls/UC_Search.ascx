﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Search.ascx.cs" Inherits="LowCostSite.sass.UC_Search" %>
<%@ Register Src="~/sass/Controls/UC_Loading.ascx" TagPrefix="uc1" TagName="UC_Loading" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<script src="/sass/javascripts/jquery.validate.js"></script>
<script src="../../assets/typeahead/typeahead.jquery.js"></script>
<script src="/sass/javascripts/LocationAutocomplete.js"></script>
<link href="/sass/Autocomplete.css" rel="stylesheet" />
<link href="/../../assets/ExtraStyling.css" rel="stylesheet" />
<script src="/sass/javascripts/PageScripts/Search.js"></script>
<style>
    .error {
        color: #c1262a;
    }

    .mt40 {
        margin-top: 40px !important;
    }

    .border-error {
        border: 1px solid #c1262a;
    }

    .div_ChildInfants.closed {
        height: 0;
    }

    .div_ChildInfants {
        height: 50px;
        transition: height 300ms;
        -webkit-transition: height 300ms;
    }
</style>

<script type="text/javascript">
    // assigning values for the global hiddenfield vars
    hf_AutoCompleteOriginCode = '<%=hf_AutoCompleteOriginCode.ClientID %>';
    hf_AutoCompleteDestinationCode = '<%=hf_AutoCompleteDestinationCode.ClientID %>';
    hf_DefultTlvText = '<%=hf_DefultTlvText.ClientID%>';
    ChildrenInfatsPanel = '<%=hf_ChildrenInfatsPanel.ClientID %>';
    // a method that when selecting a city from the list, saves it in hidden field for use from server side
    function ClientItemSelectedOrigin(sender, e) {
        $get(hf_AutoCompleteOriginCode).value = e.get_value();
        if (e.get_value() != "TLV") {
            $get(hf_AutoCompleteDestinationCode).value = "TLV";
            $("#<%=txt_Origin.ClientID%>").val($get(hf_DefultTlvText).value);
        }
        $(".txt_Origin").removeClass('border-error');
    }

    function ClientItemSelectedDestination(sender, e) {
        $get(hf_AutoCompleteDestinationCode).value = e.get_value();
        if (e.get_value() != "TLV") {
            $get(hf_AutoCompleteOriginCode).value = "TLV";
            $("#<%=txt_Destination.ClientID%>").val($get(hf_DefultTlvText).value);
        }
        $(".txt_Destination").removeClass('border-error');
    }
</script>

<div class="search_box_wrap">
    <span id="hiddenFields">
        <asp:HiddenField ID="hf_AirlineFilter" runat="server" ClientIDMode="Static" />

        <asp:HiddenField ID="hf_AutoCompleteOriginCode" runat="server"  ClientIDMode="Static"  />
        <asp:HiddenField ID="hf_AutoCompleteDestinationCode" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hf_DefultTlvText" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hf_Places" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hf_ChildrenInfatsPanel" runat="server" ClientIDMode="Static" Value="closed" />
    </span>
    <div class="row rtl" style="display: none;">
        <span class="row_search_title"><%=GetText("AllOfTheLowCostFlightsInTheWorld")%></span>
    </div>

    <!--תיבת חיפוש-->
    <div class="row rtl bg_search_box  box-padding">
        <div class="col-sm-12 box-search-center display-inline-block" >
            <div class="col-sm-2">
                <div class="form-group">
                    <asp:TextBox ID="txt_Origin" CssClass="txt_Origin form-control Autocomplete" data-place="origin" runat="server"  placeholder="gtxt_SelectOrigin" autocomplete="off" />
                    <div class="wrap-autocomplete autocomplete-origin autocomplete-box hidden" data-place="origin">
                        <!-- Autocomplete results are displayed here -->
                    </div>
                    <asp:CustomValidator ID="val_Origin" runat="server" ErrorMessage=""
                        ControlToValidate="txt_Origin"
                        OnServerValidate="val_Origin_ServerValidate"
                        ValidateEmptyText="true"
                        EnableClientScript="true"
                        ValidationGroup="val_Search" />

                    <!-- END: PLACES AUTOCOMPLETE-->
                    <label id="error_OriginNotSelected" class="error" style="display: none;">
                        <%=GetText("NotSelectedOrigin")%>
                    </label>
                    <label class="control-label">
                        <span class="checker "></span>
                        <asp:CheckBox ID="cb_PreferDirectFlights" runat="server" CssClass="cb-lables" Text="gtxt_PreferDirectflgihts" />
                    </label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <%-- $('.popularDestination').toggleClass('hidden'); --%>
                    <asp:TextBox ID="txt_Destination" CssClass="txt_Destination Autocomplete form-control PopularDestinationInput" runat="server" data-place="destination" placeholder="gtxt_SelectReturnDate" autocomplete="off" />
                    <span class="add-on open-destinatios"><i class="glyphicon glyphicon-list PopularDestinationIcon"></i></span>
                    <div class="wrap-autocomplete autocomplete-destination autocomplete-box hidden" data-place="destination">
                        <!-- Autocomplete results are displayed here -->
                    </div>
                    <div class="popularDestination hidden">
                        <h3>יעדים מומלצים <span class="close popular-destination-close">X</span></h3>
                        <asp:Repeater ID="drp_PopularDestinations" runat="server" ItemType="DL_LowCost.PopularDestination">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li data-value="<%# Eval("IataCode_UI") %>" data-display="<%# Eval("Name_UI")%>" onclick="SelectPopularDestination(this)"><%# Eval("Name_UI") %></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                      
                        <div class="multiSelect">
                            <span>מדינה</span>
                            <asp:DropDownList ID="ddl_SelectCountry" CssClass="select-country" runat="server"></asp:DropDownList>
                        </div>
                        <div class="multiSelect">
                            <span >עיר</span><select class="select-city" data-first-row="<%=GetText("SelectCity") %>"></select>
                        </div>
                        <div class="multiSelect">
                            <input type="button" value="בחר" class="btn btn_SelectDestination" id="btn_SelectDestination"/>
                        </div>

                    </div>
                    <asp:CustomValidator ID="val_Destination" runat="server" ErrorMessage=""
                        ControlToValidate="txt_Destination"
                        OnServerValidate="val_Destination_ServerValidate"
                        ValidateEmptyText="true"
                        EnableClientScript="true"
                        ValidationGroup="val_Search" />
                    <!-- END: PLACES AUTOCOMPLETE-->
                    <label id="error_DestinationNotSelected" class="error" style="display: none;">
                        <%=GetText("NotSelectedDestination")%>
                    </label>
                    <label class="control-label">
                        <span class="checker "></span>
                        <input type="checkbox" id="cb_OneWayFlights" onclick="Toggle2WayFlight(this)" runat="server" />
                        <label for="cb_OneWayFlights" class="cb-lables"><%=GetText("OneWayFlgihtsOnly")%></label>
                    </label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group input-append date">
                    <asp:TextBox ID="txt_OutwardDate" CssClass="txt_OutwardDate form-control myDatepicker" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" runat="server" placeholder="gtxt_SelectDepartureDate" name="outwardDate" onkeypress="return false;" autocomplete="off" />
                    <span class="add-on" onclick="$('.txt_OutwardDate').datepicker('show');"><i class="glyphicon glyphicon-calendar datepicker-cal"></i></span>
                    <asp:CustomValidator ID="val_OutwardDate" ControlToValidate="txt_OutwardDate" OnServerValidate="val_OutwardDate_ServerValidate" runat="server" ErrorMessage="" ValidateEmptyText="true" ValidationGroup="val_Search"></asp:CustomValidator>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group left-inner-addon">
                    <asp:TextBox ID="txt_ReturnDate" CssClass="txt_ReturnDate form-control myDatepicker" data-date-format="dd/mm/yyyy" runat="server" placeholder="gtxt_SelectReturnDate" onkeypress="return false;" autocomplete="off" />
                    <span class="add-on" onclick="$('.txt_ReturnDate').datepicker('show');"><i class="glyphicon glyphicon-calendar datepicker-cal"></i></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <asp:DropDownList ID="ddl_Adults" runat="server" class="form-control ddl_Adults" onchange="CheckInfantsCount()"></asp:DropDownList>
                    <label class="control-label">
                        <a class="cb-lables pointer AddChildInfant"><%=GetText("AddChildrenAndInfants") %></a>
                    </label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <asp:Button ID="btn_SearchFlights" runat="server" Text="gtxt_Search"
                        class="btn btn-primary btn-lg btn-block btn_main_search btn_SearchFlights"
                        OnClick="btn_SearchFlights_Click"
                        OnClientClick="return ValidateFormAndSearch()"
                        ValidationGroup="val_Search"
                        data-backdrop="static" />
                    <%--CausesValidation="true" --%>
                </div>
            </div>
        </div>
        <!--/row-->
    <div class="col-md-6"></div>
        <div class="col-sm-6 div_ChildInfants" runat="server" id="div_ChildInfants">
            <div id="div_Children" runat="server" class="col-sm-4 div_Child" style="display: none;">
                <div class="form-group">
                    <asp:DropDownList ID="ddl_Children" runat="server" class="form-control ddl_Children"></asp:DropDownList>
                </div>
            </div>
            <div id="div_Infants" runat="server" class="col-sm-4 div_Infant" style="display: none;">
                <div class="form-group">
                    <asp:DropDownList ID="ddl_Infants" runat="server" class="form-control ddl_Infants" onchange="CheckInfantsCount()"></asp:DropDownList>
                    <span runat="server" id="infantsError" class="infantsError" style="color: #c1262a; display: none;"><%=GetText("MustBeLessThanTheAmountOfAdults") %></span>
                </div>
            </div>
            
          
            <!--/span-->
        </div>
    </div>
    <!-- THE LOADING MODAL -->
    <uc1:UC_Loading runat="server" ID="UC_Loading" />
    <!--סוף - תיבת חיפוש-->
</div>
    <script src="\sass/javascripts/jquery.placeholder.js"></script>
<script>
    function ShowDatePicker() {
        $('.txt_OutwardDate').datepicker('show');
    }
    $(':text').placeholder();


</script>





