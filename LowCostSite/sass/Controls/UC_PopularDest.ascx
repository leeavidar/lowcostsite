﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_PopularDest.ascx.cs" Inherits="LowCostSite.sass.UC_PopularDest" %>

<div class="<%=DestClass%>">

    <a href="#" id="lnk_DestinationPage" class="no_border" runat="server">
        <asp:Image ID="img_DestinationThumb" runat="server" class="img_destination" />
    </a>

    <h4>
        <a class="color_dark pointer" id="lnk_DestinationPage_Label"  runat="server">
            <asp:Label ID="lbl_Name" runat="server" Text=""></asp:Label>
        </a>
    </h4>
</div>

