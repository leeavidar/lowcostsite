﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_PassengerDetailsView.ascx.cs" Inherits="LowCostSite.sass.Controls.UC_PassengerDetailsView" %>
<%--CONTACT DETAILS--%>
<div class="row no-margin-side">
    <h3 class="padding-side-title" runat="server" id="lbl_PassengerTitle"></h3>
</div>
<div class="row no-margin-side">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="control-label"><%=GetText("PersonTitle")%>:</label>

            <asp:Label ID="lbl_title" runat="server" Text="Label"></asp:Label>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <span style="color: red">*</span>
            <label class="control-label"><%=GetText("FirstName")%> <%=GetText("InEnglish")%>:</label>
            <asp:Label ID="lbl_FirstName" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <span style="color: red">*</span><label class="control-label"><%=GetText("LastName")%> <%=GetText("InEnglish")%>:</label>
            <asp:Label ID="lbl_LastName" runat="server" Text="Label"></asp:Label>
        </div>
    </div>
</div>
<asp:Panel ID="pnl_AllRequiredParameters" runat="server">
    <div class="no-margin-side">
        <asp:Panel ID="pnl_DateOfBirth" runat="server" CssClass="">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("DateOfBirth")%>: </label><br />
                    <!-- the three DDLs for the date of birth-->
                    <asp:Label ID="lbl_BirthDay" runat="server" Text=""></asp:Label>
                    <asp:HiddenField runat="server" ID="hf_DateOfBirthIsRequired"  Value="true" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_PassportNumber" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("PassportNumber")%>:</label>
                    <asp:Label ID="lbl_PassportNumber" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_PassportExpiryDate" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("PassportExpiryDate")%>:</label>
                    <asp:Label ID="lbl_PassportExpireDate" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="no-margin-side">
        <asp:Panel ID="pnl_PassportCountryOfIssue" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span><label class="control-label"><%=GetText("CountryOfPassportIssue")%>:</label>
                    <asp:Label ID="lbl_PassportIssueCountry" runat="server" Text="Label"></asp:Label>

                </div>
            </div>
        </asp:Panel>
    </div>

    <%--EXTRAS--%>
    <br />
    <div id="Div1" class="row no-margin-side display-none" div="div_ExtrasServices" runat="server">
        <h3 class="padding-side-title"><%=GetText("ExtraServices")%></h3>
    </div>
    <div class="row no-margin-side">
        <asp:Panel ID="pnl_NumberOfBags" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("LuggageNumber")%>:</label>
                    <asp:Label ID="lbl_NumberOfBags" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_FrequentFlyerNumber" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span><label class="control-label"><%=GetText("FrequentFlyer")%>:</label>
                    <asp:Label ID="lbl_FrequentFlyerNumber" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_FrequentFlyerType" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("FrequentFlyerClub")%>:</label>
                    <asp:Label ID="lbl_FrequentFlyerType" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="row no-margin-side">
        <asp:Panel ID="pnl_MealType" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("MealType")%>:</label>
                    <asp:Label ID="lbl_MealType" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_InsuranceType" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("InsuranceType")%>:</label>
                    <asp:Label ID="lbl_InsuranceType" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_SeatOptions" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("SeatChoices")%>:</label>
                    <asp:Table ID="seatsTable" runat="server">
                    </asp:Table>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>
