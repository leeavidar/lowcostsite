﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_PassengerDetails.ascx.cs" Inherits="LowCostSite.sass.Controls.UC_PassengerDetails" %>

<%--CONTACT DETAILS--%>

<div class="row no-margin-side">
    <h3 class="padding-side-title" runat="server" id="lbl_PassengerTitle"></h3>
</div>
<div class="row no-margin-side">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="control-label"><%=GetText("PersonTitle")%>:</label>

            <asp:DropDownList ID="ddl_Title" runat="server" class="form-control ltr">
                <asp:ListItem Value="Mr">Mr</asp:ListItem>
                <asp:ListItem Value="Mrs">Mrs</asp:ListItem>
                <asp:ListItem Value="Miss">Miss</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <span class="error-msg">*</span>
            <label class="control-label"><%=GetText("FirstName")%> <%=GetText("InEnglish")%>:</label>
            <asp:TextBox ID="txt_FirstName" class="form-control" runat="server" placeholder="" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="orderPageValidation" ControlToValidate="txt_FirstName" Display="Dynamic" ErrorMessage="gtxt_RequiredField" CssClass="error-msg" />
            <!-- The regex is for eglish characters, and spaces-->
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="orderPageValidation" runat="server" ControlToValidate="txt_FirstName" Display="Dynamic" ErrorMessage="gtxt_NameInEngishOnly" CssClass="error-msg" ValidationExpression="^[A-z ]+$" />
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <span class="error-msg">*</span><label class="control-label"><%=GetText("LastName")%> <%=GetText("InEnglish")%>:</label>
            <asp:TextBox ID="txt_LastName" class="form-control" runat="server" placeholder="" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="orderPageValidation" ControlToValidate="txt_LastName" Display="Dynamic" ErrorMessage="gtxt_RequiredField" CssClass="error-msg" />
            <!-- The regex is for eglish characters, and spaces-->
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_LastName" ValidationGroup="orderPageValidation" Display="Dynamic" ErrorMessage="gtxt_NameInEngishOnly" CssClass="error-msg" ValidationExpression="^[A-z ]+$" />
            <asp:CompareValidator ID="val_FirstLastNameEqual" runat="server" ErrorMessage="gtxt_FirstAndLastNameMustBeDifferent" ValidationGroup="orderPageValidation" ControlToValidate="txt_LastName" ControlToCompare="txt_FirstName" CssClass="error-msg" Display="Dynamic" Operator="NotEqual"></asp:CompareValidator>
        </div>
    </div>
</div>

<asp:Panel ID="pnl_AllRequiredParameters" runat="server">
    <div class="no-margin-side">
        <asp:Panel ID="pnl_DateOfBirth" runat="server" CssClass="display-none">
            <div class="col-sm-6">
                <div class="form-group b-day-div">
                    <span class="error-msg">*</span>
                    <label class="control-label"><%=GetText("DateOfBirth")%>: </label>
                    <br />
                    <!-- the three DDLs for the date of birth-->
                    <div class="form-group">
                        <asp:DropDownList ID="ddl_BirthDay_Year" runat="server" CssClass="b-day year-part" onChange="SetDateOfBirthTxt($(this).closest('.b-day-div').find('.txt_DateOfBirth'))"></asp:DropDownList>/
                        <asp:DropDownList ID="ddl_BirthDay_Month" runat="server" CssClass="b-day month-part" onChange="SetDateOfBirthTxt($(this).closest('.b-day-div').find('.txt_DateOfBirth'))"></asp:DropDownList>/
                        <asp:DropDownList ID="ddl_BirthDay_Day" runat="server" CssClass="b-day day-part" onChange="SetDateOfBirthTxt($(this).closest('.b-day-div').find('.txt_DateOfBirth'))"></asp:DropDownList>
                        <asp:TextBox ID="txt_DateOfBirth" CssClass="txt_DateOfBirth display-none" runat="server"></asp:TextBox>
                        <%-- <asp:HiddenField runat="server" ID="hf_DateOfBirthIsRequired" Value="false" />--%>
                        <asp:CustomValidator ID="val_Age" runat="server" ControlToValidate="txt_DateOfBirth" ErrorMessage="gtxt_InvalidAge" ValidationGroup="orderPageValidation" ClientValidationFunction="" OnServerValidate="val_Age_ServerValidate" CssClass="error-msg"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ID="val_RequiredDateOfBirth" runat="server" ErrorMessage="gtxt_Required" ControlToValidate="txt_DateOfBirth" CssClass="error-msg val_Required"></asp:RequiredFieldValidator>
                        <asp:HiddenField ID="hidden_birthdate" Value="false" runat="server" />
                         </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_PassportNumber" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("PassportNumber")%>:</label>
                    <asp:TextBox ID="txt_PassportNumber" class="form-control" runat="server" />
                    <asp:RequiredFieldValidator ID="rfv_PassportNumber" runat="server" Enabled="false" ValidationGroup="orderPageValidation" ControlToValidate="txt_PassportNumber" Display="Dynamic" ErrorMessage="gtxt_RequiredField" CssClass="error-msg" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_PassportExpiryDate" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("PassportExpiryDate")%>:</label>
                    <asp:TextBox ID="txt_PassportExpireDate" class="form-control datepicker" runat="server" />
                    <asp:RequiredFieldValidator ID="rfv_PassportExpireDate" runat="server" Enabled="false" ValidationGroup="orderPageValidation" ControlToValidate="txt_PassportExpireDate" Display="Dynamic" ErrorMessage="gtxt_RequiredField" CssClass="error-msg" />
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="no-margin-side">
        <asp:Panel ID="pnl_PassportCountryOfIssue" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span><label class="control-label"><%=GetText("CountryOfPassportIssue")%>:</label>
                    <asp:DropDownList ID="ddl_PassportIssueCountry" runat="server" class="form-control"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfv_PassportCountryOfIssue" runat="server" Enabled="false" ValidationGroup="orderPageValidation" ControlToValidate="ddl_PassportIssueCountry" Display="Dynamic" ErrorMessage="gtxt_RequiredField" CssClass="error-msg" />
                </div>
            </div>
        </asp:Panel>
    </div>

    <%--EXTRAS--%>
    <br />
    <div class="row no-margin-side display-none" div="div_ExtrasServices" runat="server">
        <h3 class="padding-side-title"><%=GetText("ExtraServices")%></h3>
    </div>
    <div class="row no-margin-side">
        <asp:Panel ID="pnl_NumberOfBags" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("LuggageNumber")%>:</label>
                    <asp:TextBox ID="txt_NumberOfBags" TextMode="Number" class="form-control" runat="server" placeholder="" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_FrequentFlyerNumber" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span><label class="control-label"><%=GetText("FrequentFlyer")%>:</label>
                    <asp:TextBox ID="txt_FrequentFlyerNumber" class="form-control" runat="server" placeholder="" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_FrequentFlyerType" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("FrequentFlyerClub")%>:</label>
                    <asp:DropDownList ID="ddl_FrequentFlyerType" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="row no-margin-side">
        <asp:Panel ID="pnl_MealType" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("MealType")%>:</label>
                    <asp:DropDownList ID="ddl_MealType" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_InsuranceType" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("InsuranceType")%>:</label>
                    <asp:DropDownList ID="ddl_InsuranceType" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_SeatOptions" runat="server" CssClass="display-none">
            <div class="col-sm-4">
                <div class="form-group">
                    <span style="color: red">*</span>
                    <label class="control-label"><%=GetText("SeatChoices")%>:</label>
                    <asp:Table ID="seatsTable" runat="server">
                    </asp:Table>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>


