﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;
using LowCostSite.FlightsBookingService;
using BL_LowCost;
using DL_Generic;

namespace LowCostSite.sass.Controls
{
    public partial class UC_PassengerDetailsView : UC_BasePage
    {
        #region Properties
        public RequiredParameterContainer RequiredParameters { get; set; }
        public int PassengerNumber { get; set; }
        public LowCostSite.FlightsSearchService.ECustomerType PassengerType { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool BirthDateRequired { get; set; }
        private PassengerDetails _PassengerDetails;
        #endregion

        #region Inputs
        public PassengerDetails PassengerDetailsToSet { set { _PassengerDetails = value; } }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SetRequiredParameters();
            SetPassengerTitle();
            SetPassengerDetails();
        }

        private void SetPassengerDetails()
        {
            if (_PassengerDetails != null)
            {
                lbl_title.Text = _PassengerDetails.PassengerBase.SocialTitle.ToString();
                lbl_FirstName.Text = _PassengerDetails.PassengerBase.FirstNameField;
                lbl_LastName.Text = _PassengerDetails.PassengerBase.LastNameField;
                // Date of birth is sometimes a required field, but it was decided that we will send it anyways.
                lbl_BirthDay.Text = DateOfBirth.ToShortDateString();

                if (_PassengerDetails.CustomSupplierParameterList.DataSource != null)
                {
                    foreach (var itemCustomParameter in _PassengerDetails.CustomSupplierParameterList.DataSource)
                    {
                        switch (itemCustomParameter.Name)
                        {
                            //case "DateOfBirth":
                            //    lbl_BirthDay.Text = itemCustomParameter.Value;
                            //    break;
                            case "PassportNumber":
                                lbl_PassportNumber.Text = itemCustomParameter.Value;
                                break;
                            case "PassportExpiryDate":
                                lbl_PassportExpireDate.Text = itemCustomParameter.Value;
                                break;
                            case "PassportCountryOfIssue":
                                lbl_PassportIssueCountry.Text = itemCustomParameter.Value;
                                break;
                        }
                    }
                }
            }
        }
        private void SetPassengerTitle()
        {
            string passengerType;
            switch (PassengerType)
            {
                case FlightsSearchService.ECustomerType.ADT:
                    passengerType = GetText("Adult");
                    break;
                case FlightsSearchService.ECustomerType.CNN:
                    passengerType = GetText("Child");
                    break;
                case FlightsSearchService.ECustomerType.INF:
                    passengerType = GetText("Infant");
                    break;
                default:
                    passengerType = GetText("Adult");
                    break;
            }
            // set the title for each passengers' details
            if (BasePage.Lang == "English")
            {
                lbl_PassengerTitle.InnerText = string.Format("{0} {1} {2} - {3}", GetText("Passenger"), PassengerNumber, GetText("details"), passengerType);
            }
            else
            {
                // this is the order of words to display in Hebrew
                lbl_PassengerTitle.InnerText = string.Format("{0} {1} - {2}", GetText("DetailsForPassenger"), PassengerNumber, passengerType);
            }


        }
        private void SetRequiredParameters()
        {
            if (RequiredParameters != null)
            {

                foreach (var requiredParameter in RequiredParameters.DataSource)
                {
                    if (!requiredParameter.IsOptional)
                    {
                        string requiredParameterPanelName = string.Format("pnl_{0}", requiredParameter.Name);
                        string requiredParameterValidatorName = string.Format("rfv_{0}", requiredParameter.Name);
                        Panel pnl = pnl_AllRequiredParameters.FindControl(requiredParameterPanelName) as Panel; //get the control that fits the name of the parameter
                        if (pnl != null)
                        {
                            pnl.Attributes.CssStyle.Value = StaticStrings.display;//display the panel
                        }

                        if (requiredParameter.Name == EnumHandler.RequiredFields.SeatOptions.ToString())
                        {
                            SetSeatsDDLs(requiredParameter.DisplayText);
                            if (pnl != null)
                            {
                                pnl.Attributes.CssStyle.Value = StaticStrings.display;//display the panel
                            }
                        }
                        RequiredFieldValidator rfv = pnl_AllRequiredParameters.FindControl(requiredParameterValidatorName) as RequiredFieldValidator;//get the validator that fits the name of the parameter

                        if (rfv != null)//if we found the required field validator
                        {
                            rfv.Enabled = true; //if the required parameter is not optional we enable the validator
                        }
                    }
                    else
                    {
                        //is optional, no need to fill
                    }
                }
            }
        }

        private void SetValueToDDL(string name, string text)
        {
            string valueToParse = text.Trim().Substring(text.IndexOf(":") + 1);
            char[] delimiterChars = { ',' };
            string[] values = valueToParse.Split(delimiterChars);
            Dictionary<string, string> valuesToDDL = new Dictionary<string, string>();

            foreach (var item in values)
            {
                string key = item.Trim().Substring(0, 1); //get the key of the value
                string value = item.Trim().Substring(2, item.Length - 4); // start after the '(' and ends before the ')'
                valuesToDDL.Add(key, value);
            }
            string ddlName = string.Format("ddl_{0}", name);
            DropDownList ddl = pnl_AllRequiredParameters.FindControl(ddlName) as DropDownList; //get the control that fits the name of the parameter

            ddl.DataSource = valuesToDDL;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();

        }

        private void SetSeatsDDLs(string value)
        {
            string valueToParse = value.Trim().Substring(value.IndexOf(":") + 1);
            char[] delimiterChars = { ',' };
            string[] values = valueToParse.TrimStart().Split(delimiterChars);

            Dictionary<string, List<Seat>> flightsAndSeats = new Dictionary<string, List<Seat>>();

            foreach (var item in values)
            {
                Seat oSeat = new Seat();
                oSeat.FlightNumber = item.Trim().Substring(0, (item.IndexOf("-")));
                oSeat.SeatNumber = item.Trim().Substring(item.IndexOf("-") + 1, ((item.IndexOf("(") - 1) - item.IndexOf("-")));
                oSeat.Price = item.Trim().Substring(item.IndexOf("@") + 1, ((item.IndexOf(".") + 2) - item.IndexOf("@")));
                oSeat.Currency = item.Trim().Substring(item.IndexOf(".") + 3, 3);

                if (flightsAndSeats.ContainsKey(oSeat.FlightNumber))
                {
                    flightsAndSeats[oSeat.FlightNumber].Add(oSeat);
                }
                else
                {
                    flightsAndSeats.Add(oSeat.FlightNumber, new List<Seat>());
                    flightsAndSeats[oSeat.FlightNumber].Add(oSeat);
                }
            }

            if (flightsAndSeats != null)
            {
                foreach (var item in flightsAndSeats)
                {
                    DropDownList oDropDownList = new DropDownList();
                    oDropDownList.ID = "ddl_Flight_" + item.Key;
                    oDropDownList.DataSource = (from obj in item.Value
                                                select new
                                                {
                                                    Name = string.Format("{0} - {1}{2}", obj.SeatNumber, obj.Currency, obj.Price),
                                                    Value = string.Format("{0} - {1}", obj.SeatNumber, obj.Price)
                                                }).ToList();

                    oDropDownList.DataTextField = "Name";
                    oDropDownList.DataValueField = "Value";
                    oDropDownList.SelectedIndexChanged += DDL_onChange;
                    oDropDownList.DataBind();

                    Label oLabel = new Label();
                    oLabel.ID = "lbl_" + item.Key;
                    oLabel.Text = string.Format("טיסה מספר: {0}", item.Key);

                    TableRow oTableRow = new TableRow();
                    TableCell oFlightNumberCell = new TableCell();
                    oFlightNumberCell.Controls.Add(oLabel);
                    TableCell oFlightDDLCell = new TableCell();
                    oFlightDDLCell.Controls.Add(oDropDownList);

                    oTableRow.Cells.Add(oFlightNumberCell);
                    oTableRow.Cells.Add(oFlightDDLCell);

                    seatsTable.Rows.Add(oTableRow);
                }
            }

        }

        private void DDL_onChange(object sender, EventArgs e)
        {
            OrderPage oOrderPage = this.Parent as OrderPage;
            DropDownList thisDDL = sender as DropDownList;
            string selectedSeat = thisDDL.SelectedValue;
            oOrderPage.LBL_SeatTotalPrice = selectedSeat.Substring(selectedSeat.IndexOf("-")).TrimStart().TrimEnd();
        }

        /// <summary>
        ///  A method that fills all the placeholders and text attributes of the elemets that use GetText()
        /// </summary>




        #region help clasess
        public class Seat
        {
            public string FlightNumber { get; set; }
            public string SeatNumber { get; set; }
            public string Price { get; set; }
            public string Currency { get; set; }
        }
        #endregion
    }
}