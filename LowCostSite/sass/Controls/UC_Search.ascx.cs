﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;
using LowCostSite.FlightsSearchService;
using DL_Generic;
using System.Web.Services;
using System.Web.Script.Services;
using BL_LowCost;


namespace LowCostSite.sass
{
    public partial class UC_Search : UC_BasePage
    {

        public string AirlineFilter { get { return hf_AirlineFilter.Value; } set { hf_AirlineFilter.Value = value; } }

        // Setting the city iata code property will set the default destination of the search.
        public string CityIataCode
        {
            set
            {
                // If there is a city iata code, the search destination will be set to that city name, and hidden field will be set to this iata code
                if (value != null && !ConvertToValue.IsEmpty(value))
                {
                    CityContainer cities = CityContainer.SelectByKeysView_IataCode(value, true);
                    DL_LowCost.City city = null;
                    if (cities != null && cities.Count == 1 )
                    {
                        city = CityContainer.SelectByKeysView_IataCode(value, true).Single;
                    }
                    else if (cities.Count > 1)
	                {
                        city = CityContainer.SelectByKeysView_IataCode(value, true).First;
                        // log a warning that there are more than 1 city with the same IATA code
                        LoggerManagerProject.LoggerManager.Logger.WriteWarrning(string.Format("City Table:  There are {0} cities with the IATA code: {1}", cities.Count, value));
                    }
                   
                    if (city != null)
                    {
                        hf_AutoCompleteDestinationCode.Value = value;
                        txt_Destination.Text = string.Format("{0} ({1})", city.Name_UI, GetText("AllAirports"));
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region For debugging
            bool debugging = false;
            if (debugging && !IsPostBack)
            {
                hf_AutoCompleteOriginCode.Value = "AUA";
                hf_AutoCompleteDestinationCode.Value = "ATL";
                txt_Origin.Text = "ארובה";
                txt_Destination.Text = "אטלנטה";
                txt_OutwardDate.Text = "14/05/2014";
                txt_ReturnDate.Text = "23/05/2014";
            }
            #endregion

            if (!IsPostBack)
            {
                DL_LowCost.City city = CityContainer.SelectByKeysView_IataCode("TLV", true).Single;
                hf_AutoCompleteOriginCode.Value = "TLV";
                txt_Origin.Text = string.Format("{0} (TLV)", city.Name_UI);
                hf_DefultTlvText.Value = string.Format("{0} (TLV)", city.Name_UI);
                AddTextBoxPlaceHolders();
                SetDdls();

                drp_PopularDestinations.DataSource = PopularDestinationContainer.SelectAllPopularDestinations(1, true).DataSource;
                drp_PopularDestinations.DataBind();

                
            }
        }

        #region Event Handlers

        protected void btn_SearchFlights_Click(object sender, EventArgs e)
        {
            // if the search parameters are not valid, return to the page and don't search
            if (ValidateSearch() == false)
            {
                if (hf_ChildrenInfatsPanel.Value == "opened")
                {
                    // Make the add children panel spay opened.
                    div_ChildInfants.Attributes["class"] = div_ChildInfants.Attributes["class"].Replace("closed", "");
                    div_Children.Style["display"] = "block";
                    div_Infants.Style["display"] = "block";
                }
                else
                {
                    // Make the add children panel spay closed.
                    div_ChildInfants.Attributes["class"] += " closed";
                    div_Children.Style["display"] = "none";
                    div_Infants.Style["display"] = "none";
                }
                return;
            }
            else
            {
                // Save the search parameters into a SearchParams object (converting the formats to match the search service)
                SearchParams oSearchParams = SaveSearchParams();
                // save the search params object in the session
                Generic.SessionManager.SetSession<SearchParams>(oSearchParams);
                // redirect to the results page (the search is performed there)
                Response.Redirect(StaticStrings.path_FlightsResults);
            }
        }

        /// <summary>
        /// A method that saves the search parameters in a cookie
        /// to later show it as last search.
        /// </summary>
        /// <param name="oSearchParams">The search parameters to save in the cookie</param>
        private void SaveSearchInfoInCookie(SearchParams oSearchParams)
        {

            // THIS IS STILL NOT IN USE AND MAY BE INCORRECT..
            if (oSearchParams != null)
            {
                HttpCookie aCookie = new HttpCookie("prevSearch");
                aCookie.Values["OutwardDate"] = oSearchParams.OutwardDate;
                aCookie.Values["ReturnDate"] = oSearchParams.ReturnDate;
                aCookie.Values["Origin"] = oSearchParams.Origin;
                aCookie.Values["Destination"] = oSearchParams.Destination;

                aCookie.Values["AdultCount"] = ConvertToValue.ConvertToString(oSearchParams.AdultsCount);
                aCookie.Values["ChildCount"] = ConvertToValue.ConvertToString(oSearchParams.ChildrenCount);
                aCookie.Values["InfantCount"] = ConvertToValue.ConvertToString(oSearchParams.InfantsCount);

                aCookie.Expires = DateTime.Now.AddYears(1);

                Response.Cookies.Add(aCookie);
            }
        }

        #endregion

        #region Helping Methods

        /// <summary>
        /// A method that sets the drop down lists with the maximum amount of passenger of each type.
        /// </summary>
        private void SetDdls()
        {
            AddNumberItemsToDDL(ddl_Adults, ConfigurationHandler.MaxAdults, "1" + GetText("adult"), 0, GetText("adult"), GetText("adults"));
            //Remove the option for 0 adults (starts with at least 1)
            ddl_Adults.Items.RemoveAt(0);
            ddl_Adults.DataBind();
            AddNumberItemsToDDL(ddl_Children, ConfigurationHandler.MaxChildren, GetText("SelectNumberOfChildren"));
            AddNumberItemsToDDL(ddl_Infants, ConfigurationHandler.MaxInfants, GetText("SelectNumberOfInfants"));


            var allcountries = CountryContainer.SelectAllCountrys(true).DataSource.Where(c => c.Name_UI != "").OrderBy(c => c.Name_UI).ToList();
            if (allcountries != null && allcountries.Count != 0)
            {
                ddl_SelectCountry.Items.AddRange(CountryContainer.Convert(allcountries)
               .ToListItem("Name_UI", "CountryCode_UI", true, "בחר מדינה").ToArray());
                ddl_SelectCountry.DataBind();
            }
            else
            {
                // there are no countries in the popular destination table
            }
           
         
        }

        /// <summary>
        /// A method that adds to a given DDL items that have both value and text of their index + 1 (1,2,3...) 
        /// </summary>
        /// <param name="MaxNum"></param>
        private void AddNumberItemsToDDL(DropDownList dropDown, int MaxNum, string FirstItemText, int selectedIndex = 0, string extraWordSingle = "", string extraWordPlurial = "")
        {
            dropDown.Items.Add(new ListItem() { Value = "0", Text = FirstItemText });
            for (int i = 0; i < MaxNum; i++)
            {
                if (i + 1 == 1)
                {
                    dropDown.Items.Add(new ListItem() { Value = (i + 1).ToString(), Text = (i + 1).ToString() + " " + extraWordSingle });
                }
                else
                {
                    dropDown.Items.Add(new ListItem() { Value = (i + 1).ToString(), Text = (i + 1).ToString() + " " + extraWordPlurial });
                }

            }
            dropDown.DataBind();
            dropDown.SelectedIndex = selectedIndex;
        }

        /// <summary>
        /// A method that stores the seach params from the text boxes in the search panel, to the oSearchParams property.
        /// </summary>
        private SearchParams SaveSearchParams()
        {
            if (hf_AutoCompleteOriginCode.Value == "TLV" || hf_AutoCompleteDestinationCode.Value == "TLV")
            {
                SearchParams oSearchParams = new SearchParams();
                // set the airline filter to a specific company
                oSearchParams.AirlinesFilter = hf_AirlineFilter.Value;

                // take the values for origin and destination from the hidden fields (stored there by j/s event handler for the autocomplete select event)
                oSearchParams.Origin = hf_AutoCompleteOriginCode.Value;
                oSearchParams.Destination = hf_AutoCompleteDestinationCode.Value;
                oSearchParams.OutwardDate = txt_OutwardDate.Text;

                // if the "one way flights only" check-box is not checked:
                if (!cb_OneWayFlights.Checked)
                {
                    // save the return date
                    oSearchParams.ReturnDate = txt_ReturnDate.Text;
                }

                oSearchParams.AdultsCount = ConvertToValue.ConvertToInt(ddl_Adults.SelectedValue);
                oSearchParams.ChildrenCount = ConvertToValue.ConvertToInt(ddl_Children.SelectedValue);
                oSearchParams.InfantsCount = ConvertToValue.ConvertToInt(ddl_Infants.SelectedValue);

                oSearchParams.IsDirectFlight = cb_PreferDirectFlights.Checked;

                // the saveing to the session should be done on the page load complete event handler (in the base page)
                return oSearchParams;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        ///  A method that fills all the placeholders and text attributes of the elemets that use GetText()
        /// </summary>
        protected void AddTextBoxPlaceHolders()
        {
            txt_Origin.Attributes["placeholder"] = GetText("SelectOrigin");
            txt_Destination.Attributes["placeholder"] = GetText("SelectDestination");
            txt_OutwardDate.Attributes["placeholder"] = GetText("SelectDepartureDate");
            txt_ReturnDate.Attributes["placeholder"] = GetText("SelectReturnDate");
            btn_SearchFlights.Text = GetText("Search");
            cb_PreferDirectFlights.Text = GetText("DirectFlightsPreference");

            //validators:
            // rfv_OutwardDate.ErrorMessage = GetText("RequiredField");
            //  rfv_Destination.ErrorMessage = GetText("RequiredField");
            // rfv_Origin.ErrorMessage = GetText("RequiredField");





        }
        #endregion

        #region Validation functions

        /// <summary>
        /// A method that validates the search form
        /// </summary>
        /// <returns></returns>
        protected bool ValidateSearch()
        {
            bool valid = true;

            // check that both origin and destination are selected
            if (hf_AutoCompleteOriginCode.Value == "" || hf_AutoCompleteDestinationCode.Value == "")
            {
                valid = false;
            }
            // there can't be more infants than adult passengers in the search
            if (ConvertToValue.ConvertToInt(ddl_Adults.SelectedValue) < ConvertToValue.ConvertToInt(ddl_Infants.SelectedValue))
            {
                ddl_Infants.Attributes["class"] = ddl_Infants.Attributes["class"] + " border-error";
                infantsError.Style["display"] = "block";
                valid = false;
            }
            // check that the dates are correct and that there is at least outward date

            DateTime outwardDate = ConvertToValue.ConvertToDateTime(txt_OutwardDate.Text);
            DateTime returnDate = ConvertToValue.ConvertToDateTime(txt_ReturnDate.Text);
            if (ConvertToValue.IsEmpty(outwardDate) || (ConvertToValue.IsEmpty(returnDate) && txt_ReturnDate.Text != ""))
            {
                valid = false;
            }
            return valid;
        }

        protected void val_Destination_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // to validate if a destination was selected we actually need to check the hidden field and not the textbox content(in args.Value)
            if (hf_AutoCompleteDestinationCode.Value == "")
            {
                txt_Destination.CssClass += " border-error";
                args.IsValid = false;
            }
        }
        protected void val_Origin_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // to validate if an origin was selected we actually need to check the hidden field and not the textbox content(in args.Value)
            if (hf_AutoCompleteOriginCode.Value == "")
            {
                txt_Origin.CssClass += " border-error";
                args.IsValid = false;
            }
        }
        protected void val_OutwardDate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // check if the outward date is valid (can be converted to a datetime object)
            if (ConvertToValue.IsEmpty(ConvertToValue.ConvertToDateTime(args.Value)))
            {
                txt_OutwardDate.CssClass += " border-error";
                args.IsValid = false;
            }
        }
        #endregion

       


        
    }
}