﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LowCostSite.FlightsSearchService;
using DB_Class;
using DL_Generic;
using System.Web.Services;
using System.Web.Script.Services;

namespace LowCostSite.sass
{
    public partial class UC_Flight : UC_BasePage
    {
        public event GetAirlineCompanyName EGetAirlineCompanyName;
        
        #region Properties
        #region Private

        //   private FlightsAvailResults oSessionFlightsAvailResults;
        private Flight _Flight;

        #endregion
        #region Public

        public bool IsActive { get; set; }
        public int StopsCount { get; set; }

        // These 3 properties are needed for the finding of the flight in the session object, when the show details button is clicked.
        public string FlightId { get; set; }
        public string AirlineId { get; set; }
        public string GroupId { get; set; }

        // a property to that is true if the current instance is the first flight in the list (for the checkbox)
        public bool FirstFlight { get; set; }

        public string DataToggle { get; set; }
        public FlightsAvailResults SearchResults { get; set; }
        public string AirlineCode { get; set; }
        public Flight Flight
        {
            get { return _Flight; }
            set
            {
                _Flight = value;
                // Setting the logo of the airline
                //   img_AirlineLogo.ImageUrl = string.Format("{0}/p{1}.gif","http://api.travelfusion.com/images/operators", AirlineCode.ToLower());
                img_AirlineLogo.ImageUrl = string.Format("{0}/{1}.gif", "http://www.travelfusion.com/images/logos", AirlineCode);

                lbl_BoardingTime.Text = value.FlightSegments.DataSource[0].DepartDateTime.ToShortTimeString();
                lbl_FlightDuration.Text = string.Format("[{0}]", MyExtensions.TimeMinutesToHours(ConvertToValue.ConvertToDouble(value.Duration)));
                lbl_LandingingTime.Text = value.FlightSegments.DataSource.Last().ArrivalDateTime.ToShortTimeString();

                string price = value.Price.TotalAmountWithMarkUp.HasValue ? value.Price.TotalAmountWithMarkUp.ToString() : "";
                string currency = GetCurrency(this.FlightId); // !ConvertToValue.IsEmpty(value.Price.Currency) ? value.Price.Currency.ToString() : "";

                lbl_Price.Text = string.Format("{0}{1}", currency, price);

                // Each radio button hold the total price for the flight, and also the price for adult/child/infant passenger
                rb_group.Attributes.Add("data-price", string.Format("{0}", price));
                rb_group.Attributes.Add("data-currency", string.Format("{0}", currency));
                SetRadioButtonPrices();
                // setting an attribute called "group" to the radio buttons, that will be used in JS to change the name (group name) of the radio button 
                SetRadioButtonGroups();

                int count = value.FlightSegments.DataSource.Count() - 1;
                lbl_StopsCount.Text = count == 0 ? GetText("DirectFlight") : string.Format("{0} {1}", count, GetText("_stops"));    //  "ללא " : count.ToString();

                // the first flight only should be selected
                rb_group.Checked = FirstFlight;

                // Because the details are not preloaded with the page,
                // we need to give the "details" button this attribute, to make it possible to find the flight later when the "details" button is clicked 
                btn_Details.Attributes.Add("data-Toggle", value.FlightId);
                DataToggle = value.FlightId;
                // add a class: info_[flightId] for control of the styling of the details box (without effecting all other flight ucs)
                listInfoClose.Attributes["class"] = listInfoClose.Attributes["class"] + " info_" + value.FlightId;

                // if the return data is different than the boarding date, display the "i" icon, with a message "The arrival is at the next day"
                if (value.FlightSegments.DataSource[0].DepartDateTime.ToShortDateString() != value.FlightSegments.DataSource.Last().ArrivalDateTime.ToShortDateString())
                {
                    // Remove the display:none; from the info message 
                    info_NextDayLanding.Attributes["style"] = "";
                }
                if (!IsActive)
                {
                    rb_group.Style.Add("display", "none");
                }
                
                if (EGetAirlineCompanyName != null)
                {
                    EGetAirlineCompanyName.Invoke(value.FlightSegments.DataSource.FirstOrDefault().MarketingAirline.Code, Generic.EnumHandler.AirlineTypes.MarketingAirline, value.FlightSegments.DataSource.FirstOrDefault().MarketingAirline.Name);
                }
                else
                {
                    //Missing Event for setting company name
                }
                
            }
        }

        public bool IsHiddenFirst
        {
            set
            {
                if (value == true)
                {
                    // add a class that hides the row
                    mainRow.Style.Add("display", "none");
                    mainRow.Attributes["class"] += " hiddenFirst";
                    // add a class that hides the row with the info
                    InfoRow.Style.Add("display", "none");
                    InfoRow.Attributes["class"] += " hiddenFirst";
                }
            }
        }
        // a propery that stores the js name with the parameters to be bound to the click event of the button that opens the details box.
        //public string GetDetailsClientClick { get { return string.Format("toggleDetails(this,'{0}')", btn_getDetails.ClientID); } }

        #endregion
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPage();
            if (SearchResults != null)
            {
                SetRadioButtonGroups();
                SetRadioButtonPrices();
            }
        }

        /// <summary>
        /// A method that sets all the pages components with the relevant data.
        /// </summary>
        private void SetPage()
        {
            //btn_Details.Attributes.Add("onclick", GetDetailsClientClick);

            FlightId = Flight.FlightId;
            // add the client id of the button as a class so that it would be possible to open or close it
            this.details.Attributes.Add("class", FlightId);

            rb_group.Attributes.Add("data-flightId", FlightId);

        }

        #region Event Handlers
        protected void btn_Details_Click(object sender, EventArgs e)
        {
            // when clicking the button, there is a request to the server, that by the flight id it takes from the command argument of the button, it finds 
            // the flight legs and adds them into the place holder.
            // it than un-hides the second tr with the details.


            // if the user clicked on the show details button when he has anoher (newer) search results object, redirect him to the homepage.
            if ((this.Page as FlightsResults).oSessionSearchParams.GetHashCode().ToString() != (this.Page as FlightsResults).SearchHashCode)
            {
                Response.Redirect(StaticStrings.path_IndexPage);
            }

            LowCostSite.FlightsSearchService.Flight oFligth = new Flight();

            // find the flight in the results:

            Flights oFlights = SearchResults.FlightsCollection.Flights.FirstOrDefault(f => f.ID.ToString() == AirlineId);
            if (oFlights != null)
            {
                Group oGroup = oFlights.GroupList.DataSource.FirstOrDefault(grp => grp.GroupId == GroupId);
                if (oGroup != null)
                {
                    Flight oFlight;
                    oFlight = oGroup.OutwardFlights.DataSource.FirstOrDefault(f => f.FlightId == FlightId);
                    // if there is no outward flight with this id, search in the return flight:
                    if (oFlight == null)
                    {
                        oFlight = oGroup.ReturnFlights.DataSource.FirstOrDefault(f => f.FlightId == FlightId);
                    }

                    if (oFlight != null)
                    {
                        // after finding the flight, create the usec controls for it's flight legs and stops.
                        UC_FlightLeg oUC_FlightLeg;
                        UC_FlightStop oUC_FlightStop;

                        int count = 1;
                        int numOfSegments = oFlight.FlightSegments.DataSource.Count();
                        for (int i = 0; i < numOfSegments; i++)
                        {
                            // get the flight segment in index i
                            FlightSegment oFlightSegment = oFlight.FlightSegments.DataSource[i];
                            oUC_FlightLeg = new UC_FlightLeg();
                            oUC_FlightLeg = (UC_FlightLeg)LoadControl(StaticStrings.path_UC_FlightLeg);
                            oUC_FlightLeg.EGetAirlineCompanyName += EGetAirlineCompanyName;
                            oUC_FlightLeg.Index = count;
                            // set the segment property of the uc with the flight leg (the set function also sets the control's labels)
                            oUC_FlightLeg.FlightSegment = oFlightSegment;
                            //add the control to the place holder
                            ph_FlightLegs.Controls.Add(oUC_FlightLeg);
                            // Add a stop description user control:
                            if (count < numOfSegments)
                            {
                                oUC_FlightStop = new UC_FlightStop();
                                oUC_FlightStop = (UC_FlightStop)LoadControl(StaticStrings.path_UC_FlightStop);
                                oUC_FlightStop.StopNumber = count;
                                oUC_FlightStop.StopTime = MyExtensions.TimeMinutesToHours((oFlight.FlightSegments.DataSource[i + 1].DepartDateTime - oFlightSegment.ArrivalDateTime).TotalMinutes);
                                oUC_FlightStop.CityName = Extensions.GetCityOfAirport(oFlightSegment.ArrivalAirport);
                                oUC_FlightStop.ID = string.Format("flightSegments_{0}_{1}", i, oFlightSegment.FlightNumber);
                                // check if the next departure is from the same airport (for the change of airport notice):
                                if (oFlightSegment.ArrivalAirport.LocationCode != oFlight.FlightSegments.DataSource[i + 1].DepartureAirport.LocationCode)
                                {
                                    oUC_FlightStop.ChangeAirport = true;
                                }


                                // Add the flight stop line to the place holder
                                ph_FlightLegs.Controls.Add(oUC_FlightStop);
                            }
                            count++;
                        }
                    }

                    // open the details box
                    Extensions.OnBack(sender as LinkButton, this.Page as BasePage_UI);
                    // update the details panel
                    details.Update();
                }
            }

        }


        #endregion

        #region Helping Methods

        /// <summary>
        /// A meothd that sets an attribute called "group" to the radio buttons, 
        /// that will later be used in JavaScript to change the name (group name) of the radio button 
        /// </summary>
        private void SetRadioButtonGroups()
        {
            if (isUotwardFlight(this.FlightId))
            {
                this.rb_group.Attributes.Add("data-group", "outward_" + GroupId);
            }
            else
            {
                this.rb_group.Attributes.Add("data-group", "return_" + GroupId);
            }
        }

        /// <summary>
        /// Giving  data attributes to the radio button for total prices for adults, children and infants. (-1 means there is not price found)
        /// </summary>
        private void SetRadioButtonPrices()
        {
            if (this.Flight.Price.PassengerPrices == null)
            {
                //  this will happen if there is an error and there are no prices in the results (it shouldn't happen) 
                return;
            }
            PassengerPrice price;
            price = this.Flight.Price.PassengerPrices.DataSource.FirstOrDefault(p => p.PassengerType == ECustomerType.ADT);
            if (price != null && price.TotalAmountWithMarkUp.HasValue)
            {
                this.rb_group.Attributes.Add("data-adults_price", price.TotalAmountWithMarkUp.ToString());
            }
            else
            {
                this.rb_group.Attributes.Add("data-adults_price", "0");
            }
            price = this.Flight.Price.PassengerPrices.DataSource.FirstOrDefault(p => p.PassengerType == ECustomerType.CNN);
            if (price != null && price.TotalAmountWithMarkUp.HasValue)
            {
                this.rb_group.Attributes.Add("data-children_price", price.TotalAmountWithMarkUp.ToString());
            }
            else
            {
                this.rb_group.Attributes.Add("data-children_price", "0");
            }
            price = this.Flight.Price.PassengerPrices.DataSource.FirstOrDefault(p => p.PassengerType == ECustomerType.INF);
            if (price != null && price.TotalAmountWithMarkUp.HasValue)
            {
                this.rb_group.Attributes.Add("data-infants_price", price.TotalAmountWithMarkUp.ToString());
            }
            else
            {
                this.rb_group.Attributes.Add("data-infants_price", "0");
            }
        }

        /// <summary>
        /// A method that by a flight id returns if it is an outward flight 
        /// </summary>
        /// <param name="FlightId">The flightId</param>
        /// <returns>True - if it is an Outward flight, and False for return flight.</returns>
        private bool isUotwardFlight(string FlightId)
        {
            Flights oFlights = SearchResults.FlightsCollection.Flights.FirstOrDefault(f => f.ID.ToString() == AirlineId);
            if (oFlights != null)
            {
                Group oGroup = oFlights.GroupList.DataSource.FirstOrDefault(grp => grp.GroupId == GroupId);
                if (oGroup != null)
                {
                    Flight oFlight;
                    oFlight = oGroup.OutwardFlights.DataSource.FirstOrDefault(f => f.FlightId == FlightId);
                    if (oFlight != null)
                    {
                        return true;
                    }
                }

            }
            return false;
        }

        /// <summary>
        /// A method thar finds the currency for the price (because there is only the price in the flight object)
        /// </summary>
        /// <returns>The currency</returns>
        private string GetCurrency(string FlightId)
        {
            Flights oFlights = SearchResults.FlightsCollection.Flights.FirstOrDefault(f => f.ID.ToString() == AirlineId);
            if (oFlights != null)
            {
                Group oGroup = oFlights.GroupList.DataSource.FirstOrDefault(grp => grp.GroupId == GroupId);
                if (oGroup != null)
                {
                    Flight oFlight = oGroup.OutwardFlights.DataSource.Where(flight => flight.FlightId == FlightId).FirstOrDefault() ?? oGroup.ReturnFlights.DataSource.Where(flight => flight.FlightId == FlightId).FirstOrDefault();
                    string currency = oFlight.Price.Currency;
                    currency = Extensions.GetCurrencySymbol(currency);
                    return currency;
                }
            }
            return "NoCurr";

        }

        #endregion
    }
}