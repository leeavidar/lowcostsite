﻿using DL_Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LowCostSite.sass
{
    public partial class UC_FlightStop : UC_BasePage
    {

        public int StopNumber { get { return ConvertToValue.ConvertToInt(lbl_StopNumber.Text); } set { lbl_StopNumber.Text = value.ToString(); } }
        public string StopTime { get { return lbl_StopTime.Text; } set { lbl_StopTime.Text = String.Format("{0} {1}",value,GetText("Hours")); } }
        public bool ChangeAirport
        {
            set
            {
                // if the value for property ChangeAirport is true, show the alert about changing airport.
                if (value)
                {
                    lbl_ChangeAirport.Visible = true;
                }
                else
                {
                    lbl_ChangeAirport.Visible = false;
                }
            }
        }
        public string CityName { set { lbl_City.Text = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_ChangeAirport.Text = GetText("NoticeThereISAChangeOfAirportAtThisStop");
        }
    }
}