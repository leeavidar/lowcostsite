﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using DL_LowCost;
using System.Web.Script.Serialization;
using System.IO;
using System.Web.UI;
using Generic;

namespace LowCostSite.sass
{
    /// <summary>
    /// Summary description for Autocomplete
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Autocomplete : System.Web.Services.WebService
    {
        /// <summary>
        /// A method that searches for a city name in the database, based on a given string.
        /// </summary>
        /// <param name="prefixText">The string (can be part of the city name)</param>
        /// <param name="count">How many letter to start the search from (because the method is called in a change event of a textbox)</param>
        /// <returns>A list of all matching resukts</returns>
        //[ScriptMethod()]
        //[WebMethod(EnableSession = true)]
        [WebMethod]
        public List<string> AutoCompleteDestinations(string prefixText, int count)
        {
            // This list of string is created by Autocomplete Extender, and is formatted to contain in each string a text and a value.
            List<string> places = new List<string>();

            // for the cities: 
            foreach (DL_LowCost.City cityItem in StaticData.oCityList)
            {
                if (cityItem.Name_UI.ToLower().StartsWith(prefixText.ToLower()) || cityItem.IataCode_UI.StartsWith(prefixText.ToLower()))
                {
                    // create an item with this string as text and IataCode as value
                    string itemText = string.Format("{0} ({1}), {2}", Extensions.GetCityName(cityItem.IataCode_UI), cityItem.IataCode_UI, Extensions.GetCoutryOfCity(cityItem.IataCode_UI));
                    string itemValue = cityItem.IataCode_UI;
                    string city = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(itemText, itemValue);
                    // add the city to the list
                    places.Add(city);

                    // find all the airports in the city and add them to the list after the city
                    // i have the:  cityItem.IataCode_UI

                    foreach (DL_LowCost.Airport airportItem in StaticData.oAirportsList)
                    {
                        if (airportItem.CitySingle.IataCode_UI == cityItem.IataCode_UI)
                        {
                            // if the airport is in the previously found city
                            itemText = string.Format("{0} ({1}), {2}, {3}", Extensions.GetAirportName(airportItem.IataCode_UI), airportItem.IataCode_UI, Extensions.GetCityOfAirport(airportItem.IataCode_UI), Extensions.GetCoutryOfAirport(airportItem.IataCode_UI));
                            itemValue = airportItem.IataCode_UI;
                            string airport = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(itemText, itemValue);
                            places.Add(airport);
                        }
                    }
                }

            }
            if (places.Count == 0)
            {
                // add the same logic for airports 
                foreach (DL_LowCost.Airport airportItem in StaticData.oAirportsList)
                {
                    // if an airport matching the prefix was found
                    if (airportItem.EnglishName_UI.ToLower().StartsWith(prefixText.ToLower()) || airportItem.NameByLang_UI.ToLower().StartsWith(prefixText.ToLower()) || airportItem.IataCode_UI.StartsWith(prefixText.ToLower()))
                    {
                        // first search for cities using the city of this airport
                        string cityIata = airportItem.CitySingle.IataCode_UI;
                        foreach (DL_LowCost.City cityItem in StaticData.oCityList)
                        {
                            if (cityItem.IataCode_UI == cityIata)
                            {
                                // create an item with this string as text and IataCode as value
                                string itemText = string.Format("{0} ({1}), {2}", Extensions.GetCityName(cityItem.IataCode_UI), cityItem.IataCode_UI, Extensions.GetCoutryOfCity(cityItem.IataCode_UI));
                                string itemValue = cityItem.IataCode_UI;
                                string city = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(itemText, itemValue);
                                places.Add(city);

                                // for all the airports in the found city
                                foreach (DL_LowCost.Airport airportInCity in StaticData.oAirportsList.FindAll(a => a.CitySingle.IataCode_UI == cityIata))
                                {
                                    // create an item with this string as text and IataCode as value
                                    itemText = string.Format("{0} ({1}), {2}, {3}", Extensions.GetAirportName(airportInCity.IataCode_UI), airportInCity.IataCode_UI, cityItem.Name_UI, Extensions.GetCoutryOfAirport(airportInCity.IataCode_UI));
                                    itemValue = airportInCity.IataCode_UI;
                                    string airport = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(itemText, itemValue);
                                    places.Add(airport);
                                }
                                // there is only 1 city for an airport, so we can break the search for more cities.
                                break;
                            }
                        }
                    }
                }
            }
            return places;
        }


        //[WebMethod]
        //public string GetCityBoxes(string prefixText)
        //{
        //    if (prefixText.Length < 2)
        //    {
        //        // for prefixes that are sgorter than 2 letter, just give a message to keep typing.. (you shouldn't get to this point, because there should be a client side handling for short prefixes)
        //        return "<div class='airport' onclick=\"SelectLocation('','destination');\"><span class='airportName'>"+LangManager.GetLangText("ContinueTyping")+"</span></div>";
        //    }

        //    string format_autocompleteBase = "<div class='autocomplete-box'>{0}</div>";
        //    string format_city = "<div class='city'><div class='city-item' onclick=\"SelectLocation('{0}','origin');\"><img src='/sass/images/cityIcon.png' class='cityIcon' /><span class='cityName'>{1}</span><span class='cityIataCode'> ({2}), </span><span class='countryName'>{3}</span></div><span class='airports'>{4}</span></div>";
        //    string format_airport = "<div class='airport' onclick=\"SelectLocation('{0}','destination');\"><img src='/sass/images/airportIcon.png' class='airportIcon' /><span class='airportName'>{1}</span><span class='airportIataCode'> ({2}), </span><span class='cityName'>{3}</span></div>";

        //    // Convert the prefix to loser case
        //    prefixText = prefixText.ToLower();

        //    // This list of string is created by Autocomplete Extender, and is formatted to contain in each string a text and a value.
        //    List<City> cities = new List<City>();

        //    // for the cities: 
        //    // Add matching cities to a list
        //    cities.AddRange(StaticData.oCityList.FindAll(city => city.Name_UI.ToLower().StartsWith(prefixText) || city.IataCode_UI.StartsWith(prefixText)));

        //    // if the user typed an airport, find the city of this airport and add it to the list:
        //    // Get matching airports
        //    List<Airport> airports = StaticData.oAirportsList.FindAll(airport => airport.NameByLang_UI.ToLower().StartsWith(prefixText) || airport.EnglishName_UI.ToLower().StartsWith(prefixText) || airport.IataCode_UI.ToLower().StartsWith(prefixText));

        //    // Add the cities of the matching airports (only if they aren't already in the list)
        //    foreach (Airport airport in airports)
        //    {
        //        if (!cities.Exists(city => city == airport.CitySingle))
        //        {
        //            cities.Add(airport.CitySingle);
        //        }
        //    }


        //    // Build the html string
        //    string city_str= "";
        //    foreach (City city in cities)
        //    {
        //        // get the airports of the city:
        //        List<Airport> airportsList = StaticData.oAirportsList.FindAll(airport => airport.CitySingle.IataCode_UI == city.IataCode_UI); //  GetAirportsList(city.IataCode_UI);
        //        string airport_str = "";
        //        foreach (Airport airport in airportsList)
        //        {
        //            airport_str += string.Format(format_airport,airport.IataCode_UI, Extensions.GetAirportName(airport.IataCode_UI) , airport.IataCode_UI, Extensions.GetCityOfAirport(airport.IataCode_UI));
        //        }
        //        // Concat a city str with all the airports of that city.
        //        city_str += string.Format(format_city, city.IataCode_UI, city.Name_UI.Replace("'", "\'"), city.IataCode_UI, Extensions.GetCoutryOfCity(city.IataCode_UI).Replace("'", "\'"), airport_str.Replace("'", "\'"));
        //    }
        //    string result = string.Format(format_autocompleteBase, city_str);
        //    return result;



        //    // // serialize the results to json and return it
        //    //JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    //return serializer.Serialize(cities);


        //    // create an instance of a pge in order to load a user control
        //    //System.Web.UI.Page page = new System.Web.UI.Page();

        //    //UC_Autocomplete uc_Autocomplete = new UC_Autocomplete();
        //    //uc_Autocomplete = (UC_Autocomplete)(page.LoadControl(StaticStrings.path_UC_Autocomplete));
        //    //uc_Autocomplete.Direction = direction;
        //    //uc_Autocomplete.Cities = cities;

        //    //TextWriter myTextWriter = new StringWriter();
        //    //HtmlTextWriter myWriter = new HtmlTextWriter(myTextWriter);

        //    //uc_Autocomplete.RenderControl(myWriter);

        //    //// return the rendered html of the autocomplete user control
        //    //string str = myTextWriter.ToString();
        //    //  return str;

        //}


        [WebMethod]
        public string GetCityBoxesSimple(string prefixText, string place)
        {
            bool resultsExist = false;
            if (prefixText.Length < 1)
            {
                // for prefixes that are sgorter than 2 letter, just give a message to keep typing.. (you shouldn't get to this point, because there should be a client side handling for short prefixes)
                return "<div class='airport' onclick=\"SelectLocation('','destination','');\"><span class='airportName'>" + LangManager.GetLangText("ContinueTyping") + "</span></div>";
            }

            string format_autocompleteBase = "<div class='autocomplete-box3'>{0}</div>"; // "<span>{0}</span>"; // "<div class='autocomplete-box'>{0}</div>";
            string format_city = "<div class='city'><div class='city-item' onclick=\"SelectLocation('{0}','" + place + "','{1}');\"><img src='/sass/images/cityIcon.png' class='cityIcon' /><span class='cityName'>{2}</span><span class='cityIataCode'> ({3}), </span><span class='countryName'>{4}</span></div><span class='airports'>{5}</span></div>";
                             string format_airport = "<div class='airport' onclick=\"SelectLocation('{0}','" + place + "','{1}');\"><img src='/sass/images/airportIcon.png' class='airportIcon' /><span class='airportName'>{2}</span><span class='airportIataCode'> ({3}), </span><span class='cityName'>{4}</span></div>";

            // Convert the prefix to loser case
            prefixText = prefixText.ToLower();

            // This list of string is created by Autocomplete Extender, and is formatted to contain in each string a text and a value.
            List<SimpleCity> cities = new List<SimpleCity>();

            // for the cities: 
            // Add matching cities to a list

          
            cities.AddRange(StaticData.CitiesListSimple.FindAll(city => city.Name.ToLower().StartsWith(prefixText) || city.IataCode.StartsWith(prefixText)));
            if (cities.Count > 0)
            {
                resultsExist = true;
            }
            // if the user typed an airport, find the city of this airport and add it to the list:
            // Get matching airports
            List<SimpleAirport> airports = StaticData.AirportsListSimple.FindAll(airport => airport.Name.ToLower().StartsWith(prefixText) || airport.IataCode.ToLower().StartsWith(prefixText));

            // Add the cities of the matching airports (only if they aren't already in the list)
            foreach (SimpleAirport airport in airports)
            {
                    if (cities.Count == 0 || !cities.Exists(city => city.IataCode == airport.CityIataCode))
                    {
                        SimpleCity cityOfAirport = StaticData.CitiesListSimple.Find(city => city.IataCode == airport.CityIataCode);
                        if (cityOfAirport != null)
                        {
                            cities.Add(cityOfAirport);
                        }
                        else
                        {
                            // there is no city to the airport:
                            // write the city to log
                           // LoggerManagerProject.LoggerManager.Logger.WriteError("NO CITY TO AIRPORT:" + airport.Name + " (" + airport.IataCode + ") CityIataCode: " + airport.CityIataCode);
                        }
                    }
            }


            // Build the html string
            string city_str = "";
            string displayText = "";
            foreach (SimpleCity city in cities)
            {
                // get the airports of the city:
                List<SimpleAirport> airportsList = StaticData.AirportsListSimple.FindAll(airport => airport.CityIataCode == city.IataCode);
                string airport_str = "";
                foreach (SimpleAirport airport in airportsList)
                {
                 //    JSON.stringify(plainTextStr).replace(/&/, "&amp;").replace(/"/g, "&quot;")
                    displayText = string.Format("{0} ({1}), {2}", airport.Name, airport.IataCode, Extensions.GetCityOfAirport(airport.IataCode)).Replace("'", "##").Replace("\"", "###");
                    airport_str += string.Format(format_airport, airport.IataCode,displayText, Extensions.GetAirportName(airport.IataCode), airport.IataCode, Extensions.GetCityOfAirport(airport.IataCode));
                }
                // Concat a city str with all the airports of that city.
                displayText = string.Format("{0} ({1}), {2}", city.Name, city.IataCode, Extensions.GetCoutryOfCity(city.IataCode)).Replace("'", "##").Replace("\"", "###");
                city_str += string.Format(format_city, city.IataCode, displayText, city.Name, city.IataCode, Extensions.GetCoutryOfCity(city.IataCode),                  airport_str);


                //        city_str += string.Format(format_city, city.IataCode, displayText, city.Name, city.IataCode, Extensions.GetCoutryOfCity(city.IataCode),                  airport_str);
            }
            string result = string.Format(format_autocompleteBase, city_str);


            // If there are no results (no city or airport was found) - return one item (airport) with a message asking to try another value.
            if (!resultsExist)
            {
                //create an instance of any page (to use the getText method), and get the error message
                string errorMessageByLang = new Index().GetText("NoMatchingResultsTryWritingSomethingElse");
                string noResultsString = string.Format("<div class='airport' onclick=\"SelectLocation('','destination','');\"><span class='airportName'>{0}</span></div>",errorMessageByLang);
                return noResultsString;
            }
            return result;
     
        }

    }

}
