﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LowCostSite.FlightsSearchService;

namespace LowCostSite.sass
{
    public partial class UC_FlightLeg : UC_BasePage
    {
        public event GetAirlineCompanyName EGetAirlineCompanyName;
        public string DepartureCity { get; set; }
        public string ArrivalCity { get; set; }
        private FlightSegment _FlightSegment;
        public FlightSegment FlightSegment
        {
            get { return _FlightSegment; }
            set
            {
                _FlightSegment = value;

                img_AirlineLogo.ImageUrl = string.Format("{0}/p{1}.gif", "http://api.travelfusion.com/images/operators", value.MarketingAirline.Code.ToLower());
                // Setting the title row of the flight leg
                lbl_TitleAirline.Text = EGetAirlineCompanyName == null ? value.MarketingAirline.Name : EGetAirlineCompanyName.Invoke(value.MarketingAirline.Code, Generic.EnumHandler.AirlineTypes.MarketingAirline,value.MarketingAirline.Name);
                lbl_Airline.Text = EGetAirlineCompanyName == null ? value.MarketingAirline.Name : EGetAirlineCompanyName.Invoke(value.MarketingAirline.Code, Generic.EnumHandler.AirlineTypes.MarketingAirline, value.MarketingAirline.Name);
                if (value.MarketingAirline.Code!=value.OperatingAirline.Code)
                {
                    lbl_diffrentAirlinesAlert.Text = string.Format("*{0} {1}", GetText("OperatingAirlineDiffrentNote"), EGetAirlineCompanyName.Invoke(value.OperatingAirline.Code, Generic.EnumHandler.AirlineTypes.OperatingAirline, value.OperatingAirline.Name));
                }
                lbl_TitleOrigin.Text = Extensions.GetCityOfAirport(value.DepartureAirport);
                lbl_TitleDestination.Text = Extensions.GetCityOfAirport(value.ArrivalAirport);

                lbl_Origin.Text = string.Format("{0}, {1}", Extensions.GetCityOfAirport(value.DepartureAirport), Extensions.GetCoutryOfAirport(value.DepartureAirport));
                lbl_Destination.Text = string.Format("{0}, {1}", Extensions.GetCityOfAirport(value.ArrivalAirport), Extensions.GetCoutryOfAirport(value.ArrivalAirport));
                lbl_Flightnumber.Text = value.FlightCode;

                // For the class description, if the ClassNameGeneral and SupplierClassName are different- display them both
                //if (value.MarketingCabin.ClassNameGeneral != value.MarketingCabin.SupplierClassName)
                //{
                //    lbl_Class.Text = string.Format("{0} ({1})", value.MarketingCabin.ClassNameGeneral, value.MarketingCabin.SupplierClassName);
                //}
                //else
                //{
                //     If they are the same, display only one of them
                //    lbl_Class.Text = string.Format("{0}", value.MarketingCabin.ClassNameGeneral);
                //}

                lbl_BoardingTime.Text = value.DepartDateTime.ToShortTimeString();

                lbl_BoardingAirport.Text = string.Format(Extensions.IsRtl(Extensions.GetAirportName(value.DepartureAirport)) ? "{0} ({1})" : "({0} ({1}", Extensions.GetAirportName(value.DepartureAirport), value.DepartureAirport.LocationCode);
                lbl_ArrivalTime.Text = value.ArrivalDateTime.ToShortTimeString();

                // For Hebrew (RTL) or for English (LTR)
                lbl_ArrivalAirport.Text = string.Format(Extensions.IsRtl(Extensions.GetAirportName(value.ArrivalAirport)) ? "{0} ({1})" : "({0} ({1}", Extensions.GetAirportName(value.ArrivalAirport), value.ArrivalAirport.LocationCode);
            }
        }

        public int Index { set { lbl_Index.Text = value.ToString(); } }


        public string StopDescription { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            // set the index label from the outside


        }



    }
}