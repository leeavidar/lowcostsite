﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LowCostSite.FlightsSearchService;
using DL_Generic;
using System.Web.Services;
using Generic;

namespace LowCostSite.sass
{
    public partial class UC_FlightGroup : UC_BasePage
    {
        public event GetAirlineCompanyName EGetAirlineCompanyName;

        #region Properties

        // This property let's you disable the whole group (remove the order button, the checkboses for the flights, and the total price)
        public bool IsActive { get; set; }
        public bool OrderBlock { get; set; }
        private Group _FlightGroup { get; set; }
        public string CompanyName { get; set; }
        public string AirLineIataCode { get; set; } //yarin
        public string AirLineLocleName { get; set; } //yarin
        public Vendor Airline { get; set; }
        public string AirlineId;
        public FlightsAvailResults SearchResults { get; set; }
        public SearchParams oSearchParams { get; set; }
        // Setting the Group property also sets all the labels of the group.
        public Group FlightGroup
        {
            set
            {
                // check if the flight is only 1 way flight
                bool OneWay = !value.ReturnFlights.DataSource.Any();
                _FlightGroup = value;

                // set the company name at the title
                lbl_CompanyName.Text = AirLineLocleName; //CompanyName;
                lbl_TotalPrice.Text = GetTotalPrice(value);
                // Setting these attributes will enable changing of the prices dynamically (in JS) , when different flights are chosen
                lbl_TotalPrice.Attributes.Add("data-GroupId", value.GroupId);
                lbl_PriceBox_AdultPrice.Attributes.Add("data-GroupId", value.GroupId);
                lbl_PriceBox_ChildPrice.Attributes.Add("data-GroupId", value.GroupId);
                lbl_PriceBox_InfantPrice.Attributes.Add("data-GroupId", value.GroupId);

                Pricing outwardPricing = value.OutwardFlights.DataSource.First().Price;
                Pricing returnPricing = null;
                var blockingCurrencies = ConfigurationHandler.BlockCurrencyTypes;
                if (!OneWay)
                {
                    returnPricing = value.ReturnFlights.DataSource.First().Price;
                    OrderBlock = blockingCurrencies.Contains(outwardPricing.Currency);
                    OrderBlock = blockingCurrencies.Contains(returnPricing.Currency);
                }
                else
                {
                    OrderBlock = blockingCurrencies.Contains(outwardPricing.Currency);
                }

                FillPrices(outwardPricing, returnPricing);


                DateTime DepartDateTime = value.OutwardFlights.DataSource[0].FlightSegments.DataSource[0].DepartDateTime;
                lbl_OutwardDate.Text = Extensions.GetDateWithDayName(DepartDateTime);
                lbl_OutwardFlightOrigin.Text = Extensions.GetCityOfAirport(value.OutwardFlights.DataSource[0].FlightSegments.DataSource[0].DepartureAirport);
                lbl_OutwardFlightDestination.Text = Extensions.GetCityOfAirport(value.OutwardFlights.DataSource[0].FlightSegments.DataSource.Last().ArrivalAirport);
                if (!OneWay)
                {
                    DepartDateTime = value.ReturnFlights.DataSource[0].FlightSegments.DataSource[0].DepartDateTime;
                    lbl_ReturnDate.Text = Extensions.GetDateWithDayName(DepartDateTime);
                    lbl_ReturnFlightOrigin.Text = Extensions.GetCityOfAirport(value.ReturnFlights.DataSource[0].FlightSegments.DataSource[0].DepartureAirport);
                    lbl_ReturnFlightDestination.Text = Extensions.GetCityOfAirport(value.ReturnFlights.DataSource[0].FlightSegments.DataSource.Last().ArrivalAirport);
                }

                // make the group inactive if the InActive Property is false
                if (!IsActive)
                {
                    btn_Order.Style.Add("display", "none");
                    span_PriceBox.Style.Add("display", "none");
                }

            }

            get { return _FlightGroup; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPage();
        }

        /// <summary>
        /// A method that fills all the pages components with the relevant data.
        /// </summary>
        private void SetPage()
        {
            int NumOfFlights = ConfigurationHandler.NumberOfFlightsDisplayed;
            if (OrderBlock)
            {
                //this order is block
                btn_Order.Style.Add("visibility", "hidden");
            }
            else
            {
                btn_Order.CommandArgument = AirlineId;
                btn_Order.Attributes["data-GroupId"] = FlightGroup.GroupId;
            }


            // Adding all the flighs of the current flight group to the two place holders for outward and for return flights
            UC_Flight oUC_Flight;

            // if there are more then 4 results, show the "show more button" because only the first 4 are visible.
            if (FlightGroup.OutwardFlights.DataSource.Count() > NumOfFlights)
            {
                showMoreFlightsOutward.Visible = true;
            }
            else
            {
                showMoreFlightsOutward.Visible = false;
            }

            bool first = true;
            int count = 1;
            foreach (Flight flight in FlightGroup.OutwardFlights.DataSource)
            {
                if (flight == null)
                {
                    continue;
                }
                else
                {
                    oUC_Flight = SetUcFlight(flight, first, IsActive);
                    oUC_Flight.ID = string.Format("GroupId_{0}_FlightId_{1}", FlightGroup.GroupId, flight.FlightId);
                    if (count <= NumOfFlights)
                    {
                        // add the first 4 flights to the first place holder
                        ph_OutwardFlights.Controls.Add(oUC_Flight);
                    }
                    else
                    {
                        // set it to be hidden
                        oUC_Flight.IsHiddenFirst = true;
                        //add all the rest to the second place holder
                        ph_OutwardFlights_More.Controls.Add(oUC_Flight);
                    }
                    first = false;
                    count++;
                }
            }
            first = true;
            count = 1;
            if (FlightGroup.ReturnFlights.DataSource.Any())
            {
                // if there are more then 4 results, show the "show more button" because only the first 4 are visible.
                if (FlightGroup.ReturnFlights.DataSource.Count() > NumOfFlights)
                {
                    showMoreFlightsReturn.Visible = true;
                }
                else
                {
                    showMoreFlightsReturn.Visible = false;
                }

                foreach (Flight flight in FlightGroup.ReturnFlights.DataSource)
                {
                    if (flight == null)
                    {
                        continue;
                    }
                    else
                    {
                        oUC_Flight = SetUcFlight(flight, first, IsActive);
                        oUC_Flight.ID = string.Format("GroupId_{0}_FlightId_{1}", FlightGroup.GroupId, flight.FlightId);

                        if (count <= NumOfFlights)
                        {
                            // add the first 4 flights to the first place holder
                            ph_ReturnFlights.Controls.Add(oUC_Flight);
                        }
                        else
                        {
                            // set it to be hidden
                            oUC_Flight.IsHiddenFirst = true;
                            //add all the rest to the second place holder
                            ph_ReturnFlights_More.Controls.Add(oUC_Flight);
                        }
                        first = false;
                        count++;
                    }
                }
            }
            else
            {
                // hide the design of the return flight:
                img_Seperator.Style.Add("display", "none");
                div_ReturnFlightsArea.Style.Add("display", "none");
                // add class 'hidden' for the javascript function that copplapses the group to know not to toggle this and leave it hiddenn.
                div_ReturnFlightsArea.Attributes["class"] = div_ReturnFlightsArea.Attributes["class"] + " hidden";
            }

        }

        #region Event Handlers

        /// <summary>
        /// An event handler that fires when the user clicks on the order button of a group.
        /// it takes the selected flgihts of that group, and creates a selected itinerary object  to transfer to the next page.
        /// </summary>
        protected void btn_Order_Click(object sender, EventArgs e)
        {

            //if ((this.Page as FlightsResults).oSessionSearchParams.GetHashCode().ToString() != (this.Page as FlightsResults).SearchHashCode)
            //{
            //    Response.Redirect(StaticStrings.path_IndexPage);
            //}
            if ((this.Page as FlightsResults).oSessionFlightsAvailResults == null || oSearchParams.GetHashCode().ToString() != (this.Page as FlightsResults).SearchHashCode)
            {
                Response.Redirect(StaticStrings.path_IndexPage);
            }


            // in this method, we create an object that contains only the necessary data needed to countinue the order (based on the same object as the search, but only the needed data is filled)
            try
            {

                // first get the flights id (which is actually the group of the airline) from the button
                LinkButton btn = (LinkButton)sender;
                string flightsId = btn.CommandArgument;

                // Getting the outward and return from the page (SearchResults page)
                string outwardFlightId = (Page as FlightsResults).outwardFlightId;
                string returnFlightId = (Page as FlightsResults).returnFlightId;

                // Get the search results from the session
                FlightsAvailResults oFlightsAvailResults;
                Generic.SessionManager.GetSession<FlightsAvailResults>(out oFlightsAvailResults);

                // get only the object of the selected flights  (the airline object), and store it in a new 'selectedFlights' object
                Flights selectedFlights = oFlightsAvailResults.FlightsCollection.Flights.FirstOrDefault(f => f.ID.ToString() == flightsId);

                // clone this object and save it in the same variable (so it will be possible to show it again if the user comes back to this page)
                string strSer = selectedFlights.SerializeToString();
                selectedFlights = (Flights)XmlHandler.DeSerialize(strSer, typeof(Flights));
                // >> from now on we are working on the copy of the original results (for the selected flights)


                // initialize a new group list in that object
                selectedFlights.GroupList = new GroupContainer();
                // set place for 1 group inside that objext ( for the outward and return flights)
                selectedFlights.GroupList.DataSource = new Group[1];

                // Create a flight group to store only the selected outward and return flights
                Group selectedOutwardAndReturn = new Group();
                // Add the outward flight to the group
                selectedOutwardAndReturn.OutwardFlights = new FlightContainer();
                selectedOutwardAndReturn.OutwardFlights.DataSource = new Flight[1];
                selectedOutwardAndReturn.OutwardFlights.DataSource[0] = FlightGroup.OutwardFlights.DataSource.FirstOrDefault(f => f.FlightId == outwardFlightId);
                // Set the price object for the Handling fee
                selectedOutwardAndReturn.Price = FlightGroup.Price;
                // If there is a return flight selected (not 1 way flight), add it to the the group


                // IF there is a return flight chosen 
                if (!ConvertToValue.IsEmpty(returnFlightId))
                {
                    selectedOutwardAndReturn.ReturnFlights = new FlightContainer();
                    selectedOutwardAndReturn.ReturnFlights.DataSource = new Flight[1];
                    selectedOutwardAndReturn.ReturnFlights.DataSource[0] = FlightGroup.ReturnFlights.DataSource.FirstOrDefault(f => f.FlightId == returnFlightId);
                }

                // Set the Group id
                selectedOutwardAndReturn.GroupId = FlightGroup.GroupId;

                selectedFlights.GroupList.DataSource[0] = selectedOutwardAndReturn;

                // Create the object for the order
                SelectedItinerary oSelectedItinery = new SelectedItinerary();
                oSelectedItinery.LoginToken = oFlightsAvailResults.LoginToken;
                oSelectedItinery.SearchToken = oFlightsAvailResults.ResultsToken;
                oSelectedItinery.SelectedFlights = selectedFlights;

                // Save the selected itinerary object in the session
                Generic.SessionManager.SetSession<SelectedItinerary>(oSelectedItinery);
                // redirect to the order page
                Response.Redirect(StaticStrings.path_OrderPage, false);
            }
            catch (Exception ex)
            {
                // there was an error in the selection of the flights to order, or some other error.
                // redirect to error page and log an error: 
                LoggerManagerProject.LoggerManager.Logger.WriteError("Error: FlightResults => btn_Order_Click function. Message: " + ex.Message + "  |||  Full serialized exception object: " + ex.SerializeToString());
                Response.Redirect(StaticStrings.path_ErrorAndCallBack);
            }

        }

        #endregion

        #region Helping methods

        /// <summary>
        /// A method that fills the price box of each flight group with the number of passengers of each type, and the price for each type.
        /// </summary>
        /// <param name="oPriceContainerOutward">The prices object for the outward flights</param>
        /// <param name="oPriceContainerReturn">The prices object for the return flights</param>
        private void FillPrices(LowCostSite.FlightsSearchService.Pricing oPriceContainerOutward, LowCostSite.FlightsSearchService.Pricing oPriceContainerReturn)
        {
            decimal SumAdultsPrice = 0;
            decimal sumChildrenPrice = 0;
            decimal sumInfantsPrice = 0;

            int adults = 0, children = 0, infants = 0;
            // Get the currency code from the flight object (and try to find the symbol in the database)
            string currency = Extensions.GetCurrencySymbol(oPriceContainerOutward.Currency);

            // Outward flight - add the prices for each passenger type
            if (oPriceContainerOutward != null)
            {
                if (oPriceContainerOutward.PassengerPrices != null && oPriceContainerOutward.PassengerPrices.DataSource.Count() > 0)
                {
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource)
                    {
                        switch (item.PassengerType)
                        {
                            case ECustomerType.ADT:
                                adults++;
                                SumAdultsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                                break;
                            case ECustomerType.CNN:
                                children++;
                                sumChildrenPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                                break;
                            case ECustomerType.INF:
                                infants++;
                                sumInfantsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    //no price per passenger, we set the total outward 
                }
            }
            // Return flight - add the prices for each passenger type
            if (oPriceContainerReturn != null)
            {
                if (oPriceContainerReturn.PassengerPrices != null && oPriceContainerReturn.PassengerPrices.DataSource.Count() > 0)
                {
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource)
                    {
                        switch (item.PassengerType)
                        {
                            case ECustomerType.None:
                                break;
                            case ECustomerType.ADT:
                                SumAdultsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                                break;
                            case ECustomerType.CNN:
                                sumChildrenPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                                break;
                            case ECustomerType.INF:
                                sumInfantsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            // Show only the labels for the relevant passengers with their prices
            #region Show passengers prices
            if (adults != 0)
            {
                lbl_PriceBox_AdultCount.Text = adults.ToString();
                lbl_PriceBox_AdultPrice.Text = string.Format("{0} {1}", SumAdultsPrice.ToString("0.00"), currency);
            }
            else
            {
                lbl_RowAdults.Attributes["style"] = "display:none";
            }


            if (children != 0)
            {
                lbl_PriceBox_ChildCount.Text = children.ToString();
                lbl_PriceBox_ChildPrice.Text = string.Format("{0} {1}", sumChildrenPrice.ToString("0.00"), currency);
            }
            else
            {
                lbl_RowChildren.Attributes["style"] = "display:none";
            }

            if (infants != 0)
            {
                lbl_PriceBox_InfantCount.Text = infants.ToString();
                lbl_PriceBox_InfantPrice.Text = string.Format("{0} {1}", sumInfantsPrice.ToString("0.00"), currency);
            }
            else
            {
                lbl_RowInfants.Attributes["style"] = "display:none";
            }
            #endregion
        }

        /// <summary>
        /// A method that creates a user conrol for a flight, and sets its properties.
        /// </summary>
        /// <param name="flight"></param>
        /// <param name="isFirst">true- If it is the first flight in the gorup o r false otherwise.</param>
        /// <returns>A UC_Flight that has it's properties initialized</returns>
        private UC_Flight SetUcFlight(Flight flight, bool isFirst, bool isActive)
        {
            UC_Flight oUC_Flight;
            oUC_Flight = new UC_Flight();
            oUC_Flight = (UC_Flight)LoadControl(StaticStrings.path_UC_Flight);
            oUC_Flight.EGetAirlineCompanyName += EGetAirlineCompanyName;
            oUC_Flight.GroupId = this.FlightGroup.GroupId;
            oUC_Flight.AirlineId = AirlineId;
            oUC_Flight.FlightId = flight.FlightId;
            oUC_Flight.AirlineCode = Airline.Name;
            oUC_Flight.FirstFlight = isFirst;
            oUC_Flight.SearchResults = SearchResults;
            oUC_Flight.IsActive = isActive;
            // when setting the "flight" property of the object, the set method also sets all the other labels and control,
            // this means that any control that needs to be set and has a property, the property should be set before this line
            oUC_Flight.Flight = flight;
            oUC_Flight.ID = "UC_" + flight.FlightId;

            //  oUC_Flight.RB_GroupName = this.FlightGroup.GroupId;
            return oUC_Flight;
        }

        /// <summary>
        /// Fill the total price at the title of the group.
        /// The price is filled using the first outward and first return flights (that are initially selected)
        /// </summary>
        /// <param name="oGroup">The group to use to get the price from.</param>
        /// <returns>A string with the total price (first outward flight and first return flight) + the currency.</returns>
        private string GetTotalPrice(Group oGroup)
        {
            string currency = oGroup.OutwardFlights.DataSource[0].Price.Currency;
            // Convert the currency from code to symbol
            currency = Extensions.GetCurrencySymbol(currency);

            if (oGroup.ReturnFlights.DataSource.Count() == 0)
            {
                // return string.Format("{0}{1}", oGroup.OutwardFlights.DataSource[0].Price.TotalAmountWithMarkUp.Value, oGroup.OutwardFlights.DataSource[0].Price.Currency);
                return string.Format("{0} {1}", oGroup.OutwardFlights.DataSource[0].Price.TotalAmountWithMarkUp.Value, currency);
            }
            else
            {
                //return string.Format("{0}{1}", oGroup.OutwardFlights.DataSource[0].Price.TotalAmountWithMarkUp.Value + oGroup.ReturnFlights.DataSource[0].Price.TotalAmountWithMarkUp.Value, oGroup.OutwardFlights.DataSource[0].Price.Currency);
                return string.Format("{0} {1}", oGroup.OutwardFlights.DataSource[0].Price.TotalAmountWithMarkUp.Value + oGroup.ReturnFlights.DataSource[0].Price.TotalAmountWithMarkUp.Value, currency);
            }
        }


        /// <summary>
        ///  if the user made a server request when he has anoher (newer) search results object, redirect him to the homepage.
        ///  calls for this method are in: Filter click, 
        /// </summary>
        private void AvoidSessionConflicts()
        {
            // check if the Hash Code of the Search Params is different then the value stored on the hidden field (hf_SearchHashCode)
            // if it is diffenret , it means that the page shows results from an older search, diffenret than the ersults in the session, and the browser show be redirected to the home page.
            if ((this.Page as FlightsResults).oSessionSearchParams.GetHashCode().ToString() != (this.Page as FlightsResults).SearchHashCode)
            {
                Response.Redirect(StaticStrings.path_IndexPage);


                // if that doesn't get the search params, try to get them dirctly from the session (with sessionManager.GetSession)
            }
        }

        #endregion
    }
}