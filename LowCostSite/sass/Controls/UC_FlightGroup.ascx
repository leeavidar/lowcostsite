﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_FlightGroup.ascx.cs" Inherits="LowCostSite.sass.UC_FlightGroup" %>

<div class="box-wrap-content full-width-padd flight-group">
    <!-- START: PRICE BOX - The price box opens when clicking on the total price -->
    <div id='<%="price_box_"+FlightGroup.GroupId %>' class="price-box" style="display: none;">
        <div class="highlight rtl display_inline_block full-width no-margin-bottom seperate-border-top bg_search_box">
            <div id="lbl_RowAdults" class="row price-desc" runat="server">
                <div id="Div1" class=" col-sm-3" runat="server">
                    <%=GetText("Adult")%>
                </div>
                <div class="col-sm-1">
                    X
                </div>
                <div class="col-sm-1">
                    <asp:Label ID="lbl_PriceBox_AdultCount" runat="server" />
                </div>
                <div class="col-sm-5 price-color text-left">
                    <asp:Label ID="lbl_PriceBox_AdultPrice" CssClass="lbl_PriceBox_AdultPrice" runat="server" />
                </div>
            </div>
            <div id="lbl_RowChildren" class="row price-desc" runat="server">
                <div class="col-sm-3">
                    <%=GetText("Child")%>
                </div>
                <div class="col-sm-1">
                    X
                </div>
                <div class="col-sm-1">
                    <asp:Label ID="lbl_PriceBox_ChildCount" runat="server" />
                </div>
                <div class="col-sm-5 price-color text-left">
                    <asp:Label ID="lbl_PriceBox_ChildPrice" CssClass="lbl_PriceBox_ChildPrice" runat="server" />
                </div>
            </div>
            <div id="lbl_RowInfants" class="row price-desc" runat="server">
                <div class="col-sm-3">
                    <%=GetText("Infant")%>
                </div>
                <div class="col-sm-1">
                    X
                </div>
                <div class="col-sm-1">
                    <asp:Label ID="lbl_PriceBox_InfantCount" runat="server" />
                </div>
                <div class="col-sm-5 text-left price-color">
                    <asp:Label ID="lbl_PriceBox_InfantPrice" CssClass="lbl_PriceBox_InfantPrice" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <!-- END: PRICE BOX -->

    <!-- START: FLIGHT GROUP TITLE -->
    <div class="box-wrap-title no-padding ">
        <span class="btn group-collapse">
            <img class="arrow-up" style="display: none;" src="images/arrow_up.png"><img class="arrow-down" src="images/arrow_down.png" title='<%=GetText("CollapseGroup")%>'></span>
        <asp:LinkButton ID="btn_Order" runat="server" CssClass="btn_Order left order-text pointer " OnClientClick="OrderButtonClick(this)" OnClick="btn_Order_Click"><%=GetText("Order") %> ></asp:LinkButton>
        <!-- the group id is the unique value of each group, and it is used to determine which price box to display -->

        <div runat="server" id="span_PriceBox" class="display-inline">
            <div id="OpenPriceBox" class="left price-result-text pointer OpenPriceBox" onclick="TogglePriceBox('<%="price_box_"+ FlightGroup.GroupId %>',this)">
                <span class="pointer">
                    <span>סה"כ מחיר להרכב:</span>
                    <asp:Label ID="lbl_TotalPrice" CssClass="lbl_TotalPrice" runat="server" />
                    <img src="images/arrow_down.png" /></span>
            </div>
        </div>

        <asp:Label ID="lbl_CompanyName" runat="server" />
    </div>
    <!-- END: FLIGHT GROUP TITLE -->


    <div class="tips-box flight-row-box">
        <div class="table-responsive">
            <!--All Information-->
            <table class="full-width">
                <tr>
                    <td>
                        <!--START: FIRST ROW WITH DESCRIPTION -->
                        <table class="title-one-way-table">
                            <tr>
                                <td class="col-md-1">
                                    <img src="images/planUp.PNG" />
                                </td>
                                <td class="space-up-plan col-md-11 no-padding-right">
                                    <table class="desinations-row">
                                        <tr>
                                            <td colspan="3" class="font-size-large price-color rtl">
                                                <label>
                                                    <span class="font-size-large"><%=GetText("OutwardFlight") %> > </span>
                                                    <asp:Label ID="lbl_OutwardDate" runat="server" />
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="font-size-large price-color col-md-2 no-padding-right dest">
                                                <label>
                                                    <asp:Label CssClass="font-size-large" ID="lbl_OutwardFlightOrigin" runat="server" />
                                                </label>
                                            </td>
                                            <td class="font-size-large price-color col-md-1">
                                                <label><span class="font-size-large"></span></label>
                                            </td>
                                            <td class="font-size-large price-color col-md-6">
                                                <label>
                                                    <asp:Label CssClass="font-size-large" ID="lbl_OutwardFlightDestination" runat="server" />
                                                </label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--ALL ROWS RESULTS-->
                        <ul class="full-width table-row-segment list-result">
                            <!-- A place holder for the first 4 outward flight -->
                            <asp:PlaceHolder ID="ph_OutwardFlights" runat="server"></asp:PlaceHolder>
                            <!-- A place holder for the rest of the outward flight -->
                            <asp:PlaceHolder ID="ph_OutwardFlights_More" runat="server"></asp:PlaceHolder>
                            <li class="allRow showMoreFlight">
                                <div class="col-md-11">
                                    <span id="showMoreFlightsOutward" runat="server" class="ShowMoreFlights btn" onclick="ShowMoreFlights(this)"><%=GetText("ShowMoreResults")%></span>
                                    <span id="closeMoreFlightsOutward" runat="server" class="HideMoreFlights btn" onclick="ShowMoreFlights(this)" style="display: none;"><%=GetText("HideMoreResults")%></span>
                                </div>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="separatorDiv" runat="server" id="img_Seperator">
        <div class="sepL">
            &nbsp;
        </div>
        <div class="separator">
            &nbsp;
        </div>
        <div class="sepR">
            &nbsp;
        </div>
    </div>
    <div id="div_ReturnFlightsArea" runat="server" class="tips-box flight-row-box">

        <div class="table-responsive">
            <!--All Information-->
            <table class="full-width">
                <tr>
                    <td>
                        <!--Title one way table-->
                        <table class="title-one-way-table">
                            <tr>
                                <td class="col-md-1">
                                    <img src="images/planReturn.PNG" />
                                </td>
                                <td class="space-up-plan col-md-11 no-padding-right dest">
                                    <table class="desinations-row ">
                                        <tr>
                                            <td colspan="3" class="font-size-large price-color rtl">
                                                <label>
                                                    <span class="font-size-large"><%=GetText("ReturnFlight") %> ></span>
                                                    <asp:Label ID="lbl_ReturnDate" runat="server" />
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="font-size-large price-color col-md-3 no-padding-right dest">
                                                <label>
                                                    <asp:Label ID="lbl_ReturnFlightOrigin" CssClass="font-size-large" runat="server" />
                                                </label>
                                            </td>
                                            <td class="font-size-large price-color col-md-2">
                                                <label><span class="font-size-large"></span></label>
                                            </td>
                                            <td class="font-size-large price-color col-md-6">
                                                <label>
                                                    <asp:Label ID="lbl_ReturnFlightDestination" CssClass="font-size-large" runat="server" />
                                                </label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--ALL ROWS RESULTS-->
                        <ul class="full-width table-row-segment list-result">
                            <!-- A place holder for the first 4 return flight  -->
                            <asp:PlaceHolder ID="ph_ReturnFlights" runat="server"></asp:PlaceHolder>
                            <!-- A place holder for the rest of the return flight  -->
                            <asp:PlaceHolder ID="ph_ReturnFlights_More" runat="server"></asp:PlaceHolder>
                            <li class="allRow showMoreFlight">
                                <div class="col-md-11">
                                    <span id="showMoreFlightsReturn" runat="server" class="ShowMoreFlights btn" onclick="ShowMoreFlights(this)"><%=GetText("ShowMoreResults")%></span>
                                    <span id="closeMoreFlightsReturn" runat="server" class="HideMoreFlights btn" onclick="ShowMoreFlights(this)" style="display: none;"><%=GetText("HideMoreResults")%></span>
                                </div>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>





