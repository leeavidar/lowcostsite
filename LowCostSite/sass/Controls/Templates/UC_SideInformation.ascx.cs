﻿using BL_LowCost;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;

namespace LowCostSite.sass
{


    public partial class UC_SideInformation : UC_BasePage
    {

        public Template oTemplate { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                   SetPage();
            }
        }

        private void SetPage()
        {
            lbl_Title.Text = oTemplate.Title_UI;
            lbl_ShortText.Text = oTemplate.TextShort_UI;
            lbl_LongText.InnerHtml = oTemplate.TextLong_UI;

            //  give the "show more" button an attribute with the client id of the long text it shoud open with javascript
            lbl_ReadMore.Attributes.Add("data-toggle", lbl_LongText.ClientID);


            if (oTemplate.TextShort_UI != "" && oTemplate.TextLong_UI != "")
            {
                lbl_ReadMore.Style.Add("display", "normal");
            }
        }
    }
}