﻿using DL_LowCost;
using BL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LowCostSite.sass
{
    public partial class UC_CenterWithImage : UC_BasePage
    {
        public Template oTemplate { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetPage();
            }
        }

        private void SetPage()
        {
            lbl_Title.Text = oTemplate.Title_UI;
            lbl_ShortText.Text = oTemplate.TextShort_UI;
            lbl_LongText.Text = oTemplate.TextLong_UI;

            //  give the "show more" button an attribute with the client id of the long text it shoud open with javascript
            lbl_ReadMore.Attributes.Add("data-toggle", lbl_LongText.ClientID);


            // if there is a short text and also a long text: 
            if (oTemplate.TextShort_UI != "" && oTemplate.TextLong_UI != "")
            {
                // show the read more button
                lbl_ReadMore.Style.Add("display", "normal");

                // hide the long text: 
                lbl_LongText.Style.Add("display", "none");
            }
            // if there is only a long text
            else if (oTemplate.TextShort_UI == "" && oTemplate.TextLong_UI != "")
            {
                lbl_ReadMore.Style.Add("display", "none");
                // hide the short text
                lbl_ShortText.Style.Add("display", "none");
                // show the long text
                //  lbl_LongText.Style.Add("display","normal");
                lbl_LongText.Visible = true;
            }
            else if (oTemplate.TextShort_UI != "" && oTemplate.TextLong_UI == "")
            {
                // show the short text
                lbl_ReadMore.Style.Add("display", "normal");
                // hide the long text
                lbl_LongText.Style.Add("display", "none");
                // hide the read more button
                lbl_ReadMore.Style.Add("display", "none");
            }

            #region Setting the image
            Images oImage = oTemplate.Imagess.FindFirstOrDefault(img => img.ImageType_Value == EnumHandler.ImageTypes.Main.ToString());
            if (oImage != null)
            {
                lbl_ImageTitle.Text = oImage.Title_UI;
                img_Image.ImageUrl = string.Format("{0}/{1}", StaticStrings.path_Uploads, oImage.ImageFileName_UI);
                img_Image.AlternateText = oImage.Alt_UI;
            }

            #endregion
        }
    }
}