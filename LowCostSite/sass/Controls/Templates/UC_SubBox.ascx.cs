﻿using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LowCostSite.sass
{
    public partial class UC_SubBox : UC_BasePage
    {
        public SubBox oSubBox { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetPage();
            }
        }

        private void SetPage()
        {
            lbl_Title.Text = oSubBox.SubTitle_UI;
            lbl_LongText.InnerHtml = oSubBox.LongText_UI;

            //  give the "show more" button an attribute with the client id of the long text it shoud open with javascript
            lbl_ReadMore.Attributes.Add("data-toggle", lbl_LongText.ClientID);


            lbl_ShortText.Text = oSubBox.ShortText_UI;
            

            // if there is a short text and also a long text: 
            if (oSubBox.ShortText_UI != "" && oSubBox.LongText_UI != "")
            {
                // show the read more button
                lbl_ReadMore.Style.Add("display", "normal");

                // hide the long text: 
                lbl_LongText.Style.Add("display", "none");
            }
            // if there is only a long text
            else if (oSubBox.ShortText_UI == "" && oSubBox.LongText_UI != "")
            {
                lbl_ReadMore.Style.Add("display", "none");
                // hide the short text
                lbl_ShortText.Style.Add("display", "none");
                // show the long text
                //  lbl_LongText.Style.Add("display","normal");
                lbl_LongText.Visible = true;
            }
            else if (oSubBox.ShortText_UI != "" && oSubBox.LongText_UI == "")
            {
                // show the short text
                lbl_ReadMore.Style.Add("display", "normal");
                // hide the long text
                lbl_LongText.Style.Add("display", "none");
                // hide the read more button
                lbl_ReadMore.Style.Add("display", "none");
            }
        }
    }
}