﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SideWithImage.ascx.cs" Inherits="LowCostSite.sass.UC_SideWithImage" %>

<div class="highlight rtl ">
    <div class="display_inline_block row">
        <div class="col-md-12 right ">
            <h4 class="title_content">
                <asp:Label ID="lbl_Title" runat="server" Text="" />
            </h4>
                <asp:Label ID="lbl_ShortText" runat="server" Text="" />
           <br />
            <div runat="server" id="lbl_LongText" class="TemplateLongText"></div>
            <span class="read_more" id="lbl_ReadMore" runat="server" onclick="ToggleLongText(this)">קרא עוד</span>
        </div>
        <div class="col-md-12">
            <span class="img_title">
                <asp:Label ID="lbl_ImageTitle" runat="server" Text="" />
            </span>
            <asp:Image ID="img_Image" runat="server" CssClass="img-size-small-box" />
        </div>
    </div>
</div>

