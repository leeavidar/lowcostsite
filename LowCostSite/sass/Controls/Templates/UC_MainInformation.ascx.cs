﻿using BL_LowCost;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace LowCostSite.sass
{
    public partial class UC_MainInformation : UC_BasePage
    {
        public Template oTemplate { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Title.Text = oTemplate.Title_UI;

            SubBoxContainer oSubBoxContainer = oTemplate.SubBoxs;

            foreach (SubBox subBox in oSubBoxContainer.SortByKeyContainer(DL_Generic.KeyValuesType.KeySubBoxOrder,false).DataSource)
            {
                UC_SubBox oUC_SubBox = (UC_SubBox)LoadControl(StaticStrings.path_UC_SubBox);
                oUC_SubBox.oSubBox = subBox;
                ph_SubBoxes.Controls.Add(oUC_SubBox);
            }
        }
    }
}