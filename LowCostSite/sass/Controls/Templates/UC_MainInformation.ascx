﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_MainInformation.ascx.cs" Inherits="LowCostSite.sass.UC_MainInformation" %>
<%@ Register Src="~/sass/Controls/Templates/UC_SubBox.ascx" TagPrefix="uc1" TagName="UC_SubBox" %>


<div class="highlight rtl ">
    <div>
        <h4 class="title_content">
            <asp:Label ID="lbl_Title" runat="server" Text="" />
        </h4>
        <!-- Start of sub box -->
        <asp:PlaceHolder ID="ph_SubBoxes" runat="server"></asp:PlaceHolder>
    </div>
</div>
