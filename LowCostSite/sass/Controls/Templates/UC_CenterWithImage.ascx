﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_CenterWithImage.ascx.cs" Inherits="LowCostSite.sass.UC_CenterWithImage" %>

<div class="highlight rtl " >

    <div class="display_inline_block full-width row">
        <div class="col-md-8 right ">
            <h4 class="title_content">
                <asp:Label ID="lbl_Title" runat="server" Text="" />
            </h4>
            <p>
                <asp:Label ID="lbl_ShortText" runat="server" Text="" />
            </p>
            <br />
            <asp:Label ID="lbl_LongText" class="TemplateLongText" runat="server" Text=""  />
            <br />
            <span class="read_more" id="lbl_ReadMore" runat="server" style="display: none;" onclick="ToggleLongText(this)">קרא עוד</span>

        </div>
        <div class="col-md-4 left">
            <span class="img_title">
                <asp:Label ID="lbl_ImageTitle" runat="server" Text="" />
            </span>
            <asp:Image ID="img_Image" runat="server" CssClass="img-size-big-box" />
        </div>
    </div>
</div>

