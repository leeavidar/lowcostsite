﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_FlightLeg.ascx.cs" Inherits="LowCostSite.sass.UC_FlightLeg" %>

<table class="full-width segment" style="width:750px;">
    <tr>
        <td class="full-width">
            <table class="full-width-padd box-margin tableDetails">
                <thead>
                    <!-- START: TITLE LINE -->
                    <tr>
                        <th class="col-md-1">
                            <asp:Label ID="lbl_Index" runat="server" Text="1" />.
                        </th>
                        <th class="col-md-3">
                            <asp:Label ID="lbl_TitleAirline" runat="server"/>
                        </th>
                        <th class="col-md-3">
                            <asp:Label ID="lbl_TitleOrigin" runat="server" />
                        </th>
                        <th class="col-md-2">> </th>
                        <th class="col-md-3">
                            <asp:Label ID="lbl_TitleDestination" runat="server"/>
                        </th>
                    </tr>
                    <!-- END: TITLE LINE -->
                </thead>
                <tbody>
                    <tr>
                        <td class="col-md-1">
                          <%--  <img src="images/flight-logo.PNG" />--%>
                            <asp:Image ID="img_AirlineLogo" runat="server" />
                        </td>
                        <td class="col-md-3">
                            <asp:Label ID="lbl_Airline" runat="server"/>
                            <br /> <%=GetText("Flight")%>: 
                            <asp:Label ID="lbl_Flightnumber" runat="server" />
                            <br />
                            <asp:Label ID="lbl_diffrentAirlinesAlert" runat="server" />
                        </td>
                        <td class="col-md-3">
                            <asp:Label ID="lbl_Origin" runat="server" />
                            <br />
                            <asp:Label ID="lbl_BoardingTime" runat="server" />
                            <br />
                            <asp:Label ID="lbl_BoardingAirport" runat="server" />

                        </td>
                        <td class="col-md-2"></td>
                        <td class="col-md-3">
                            <asp:Label ID="lbl_Destination" runat="server" />
                            <br />
                            <asp:Label ID="lbl_ArrivalTime" runat="server" />
                            <br />
                            <asp:Label ID="lbl_ArrivalAirport" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <%--<td class="col-md-3 title_content bg-stops">עצירה 1: 01:25</td>--%>
                        <td class="col-md-3 title_content bg-stops">
                            <script>
                                var count = 1;
                            </script>
                            <asp:Repeater ID="drp_Hops" runat="server" ItemType="LowCostSite.FlightsSearchService.FlightSegment">
                                <ItemTemplate>
                                    <span id="lbl_StopDescription">
                                <%--        <%# GetText("stop") +" "+ Container.ItemIndex%>: 
                                    <%#Item.StopAirports.DataSource[Container.ItemIndex-1].LocationCodeField%>
                                        <%#GetText("ForDuration") +" "+ Item.StopAirports.DataSource[Container.ItemIndex-1].Duration + GetText("minutes") %>--%>
                                    </span>
                                </ItemTemplate>
                            </asp:Repeater>

                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="last-row"></td>
                    </tr>
                </tbody>
            </table>


        </td>
    </tr>
</table>

