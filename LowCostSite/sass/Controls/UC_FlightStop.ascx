﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_FlightStop.ascx.cs" Inherits="LowCostSite.sass.UC_FlightStop" %>

<table class="full-width segment">
    <tr>
        <td class="full-width-padd box-margin ">
            <table class="full-width-padd box-margin tableDetails">
                <tr>
                    <td class="col-md-8 text-center">
                        <label class="title_content">
                           <%=GetText("stop")%>
                            <asp:Label ID="lbl_StopNumber" runat="server" />: 
                        </label>
                        <asp:Label ID="lbl_StopTime" runat="server"  style="direction:rtl !important;"/>
                         <%=GetText("At")%> 
                        <asp:Label ID="lbl_City" runat="server"  />
                        <!-- this line appears if the arrival airport of the prevoius flight is different than the next origin airport -->
                        <asp:Label ID="lbl_ChangeAirport" style="display:none;" CssClass="title_content" runat="server"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
