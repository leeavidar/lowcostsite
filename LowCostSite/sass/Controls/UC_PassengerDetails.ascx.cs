﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;
using LowCostSite.FlightsBookingService;
using BL_LowCost;
using DL_Generic;

namespace LowCostSite.sass.Controls
{
    public partial class UC_PassengerDetails : UC_BasePage
    {
        #region Properties
        public RequiredParameterContainer RequiredParameters { get; set; }
        public int PassengerNumber { get; set; }
        public FlightsSearchService.ECustomerType PassengerType { get; set; }
        //public bool BirthDateRequired { get; set; }

        public DateTime CompareDate { get; set; }
        #endregion

        #region Inputs
        public string TXT_Title { get { return ddl_Title.SelectedValue; } set { ddl_Title.SelectedValue = value; } }
        public string TXT_LastName { get { return txt_LastName.Text; } set { txt_LastName.Text = value; } }
        public string TXT_FirstName { get { return txt_FirstName.Text; } set { txt_FirstName.Text = value; } }
        public string TXT_BirthDate { get { return string.Format("{0}/{1}/{2}", ddl_BirthDay_Day.SelectedValue, ddl_BirthDay_Month.SelectedValue, ddl_BirthDay_Year.SelectedValue); } set { ;} }
        public string TXT_PassportNumber { get { return txt_PassportNumber.Text; } set { txt_PassportNumber.Text = value; } }
        public string TXT_PassportExpireDate { get { return txt_PassportExpireDate.Text; } set { txt_PassportExpireDate.Text = value; } }
        public string TXT_PassportIssueCountry { get { return ddl_PassportIssueCountry.SelectedValue; } set { ddl_PassportIssueCountry.SelectedValue = value; } }
        public string TXT_NumberOfBags { get { return txt_NumberOfBags.Text; } set { txt_NumberOfBags.Text = value; } }
        public string TXT_FrequentFlyerNumber { get { return txt_FrequentFlyerNumber.Text; } set { txt_FrequentFlyerNumber.Text = value; } }
        public string TXT_FrequentFlyerType { get { return ddl_FrequentFlyerType.SelectedValue; } set { ddl_FrequentFlyerType.SelectedValue = value; } }
        public string TXT_MealType { get { return ddl_MealType.SelectedValue; } set { ddl_MealType.SelectedValue = value; } }
        //public string TXT_SeatOptions { get { return ddl_SeatOptions.SelectedValue; } set { ddl_SeatOptions.SelectedValue = value; } }
        public string TXT_InsuranceType { get { return ddl_InsuranceType.SelectedValue; } set { ddl_InsuranceType.SelectedValue = value; } }
        public string BirthDateRequired { get { return hidden_birthdate.Value; } set { hidden_birthdate.Value = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillGetTexts();
                SetRequiredParameters();
                SetPassengerTitle();
            }
        }
        private void SetPassengerTitle()
        {
            string passengerType;
            switch (PassengerType)
            {
                case FlightsSearchService.ECustomerType.ADT:
                    passengerType = GetText("Adult");
                    break;
                case FlightsSearchService.ECustomerType.CNN:
                    passengerType = GetText("Child");
                    break;
                case FlightsSearchService.ECustomerType.INF:
                    passengerType = GetText("Infant");
                    break;
                default:
                    passengerType = GetText("Adult");
                    break;
            }
            // set the title for each passengers' details
            if (BasePage.Lang == "English")
            {
                lbl_PassengerTitle.InnerText = string.Format("{0} {1} {2} - {3}", GetText("Passenger"), PassengerNumber, GetText("details"), passengerType);
            }
            else
            {
                // this is the order of words to display in Hebrew
                lbl_PassengerTitle.InnerText = string.Format("{0} {1} - {2}", GetText("DetailsForPassenger"), PassengerNumber, passengerType);
            }


        }
        private void SetRequiredParameters()
        { // display Date of birth parameter allways
            pnl_DateOfBirth.Attributes.CssStyle.Value = StaticStrings.display;//display the panel
            SetDateOfBirthDDL();
            if (RequiredParameters != null)
            {

                foreach (var requiredParameter in RequiredParameters.DataSource)
                {
                    if (!requiredParameter.IsOptional)
                    {
                        string requiredParameterPanelName = string.Format("pnl_{0}", requiredParameter.Name);
                        string requiredParameterValidatorName = string.Format("rfv_{0}", requiredParameter.Name);
                        Panel pnl = pnl_AllRequiredParameters.FindControl(requiredParameterPanelName) as Panel; //get the control that fits the name of the parameter
                        if (pnl != null)
                        {
                            pnl.Attributes.CssStyle.Value = StaticStrings.display;//display the panel

                            //if the parameter is type of value_select we insert the data on a dropdownlist
                            if (requiredParameter.Type.Equals(StaticStrings.valueSelectType))
                            {
                                SetValueToDDL(requiredParameter.Name, requiredParameter.DisplayText);
                            }
                            if (requiredParameter.Name == EnumHandler.RequiredFields.DateOfBirth.ToString())
                            {
                                //BirthDateRequired = true;
                                hidden_birthdate.Value = "true";
                            }
                        }

                        if (requiredParameter.Name == EnumHandler.RequiredFields.SeatOptions.ToString())
                        {
                            SetSeatsDDLs(requiredParameter.DisplayText);
                            if (pnl != null)
                            {
                                pnl.Attributes.CssStyle.Value = StaticStrings.display;//display the panel
                            }
                        }
                        RequiredFieldValidator rfv = pnl_AllRequiredParameters.FindControl(requiredParameterValidatorName) as RequiredFieldValidator;//get the validator that fits the name of the parameter

                        if (rfv != null)//if we found the required field validator
                        {
                            rfv.Enabled = true; //if the required parameter is not optional we enable the validator
                        }
                    }
                    else
                    {
                        //is optional, no need to fill
                    }
                }
            }
        }
        /// <summary>
        /// Set the values of the date of birth DDL with years, months and days, and the first item of each DDL.
        /// </summary>
        private void SetDateOfBirthDDL()
        {
            int minYear, maxYear;
            // The year of the order
            int compareYear = CompareDate.Year;
            // Set the top and lower limits for the year part of the Date of Birth 
            switch (PassengerType)
            {
                case LowCostSite.FlightsSearchService.ECustomerType.ADT:
                    minYear = compareYear - 100;
                    maxYear = compareYear - ConfigurationHandler.MaxChildAge + 1;
                    break;
                case LowCostSite.FlightsSearchService.ECustomerType.CNN:
                    minYear = compareYear - ConfigurationHandler.MaxChildAge - 1;
                    maxYear = compareYear - ConfigurationHandler.MaxInfantAge + 1;
                    break;
                case LowCostSite.FlightsSearchService.ECustomerType.INF:
                    minYear = compareYear - ConfigurationHandler.MaxInfantAge - 1;
                    maxYear = compareYear;
                    break;
                default:
                    minYear = compareYear - 100;
                    maxYear = compareYear;
                    break;
            }

            ddl_BirthDay_Day.Items.Add(new ListItem() { Text = GetText("Day"), Value = "-1" });
            for (int i = 1; i <= 31; i++)
            {
                ddl_BirthDay_Day.Items.Add(new ListItem() { Text = i.ToString(), Value = i.ToString() });
            }
            ddl_BirthDay_Day.SelectedIndex = 0;

            ddl_BirthDay_Month.Items.Add(new ListItem() { Text = GetText("Month"), Value = "-1" });
            for (int i = 1; i <= 12; i++)
            {
                ddl_BirthDay_Month.Items.Add(new ListItem() { Text = i.ToString(), Value = i.ToString() });
            }
            ddl_BirthDay_Month.SelectedIndex = 0;

            ddl_BirthDay_Year.Items.Add(new ListItem() { Text = GetText("Year"), Value = "-1" });
            for (int i = minYear; i <= maxYear; i++)
            {
                ddl_BirthDay_Year.Items.Add(new ListItem() { Text = i.ToString(), Value = i.ToString() });
            }
            ddl_BirthDay_Year.SelectedIndex = 0;
            // set the dateofbirth to be always required and displayed
            //hf_DateOfBirthIsRequired.Value = "true";
        }
        private void SetValueToDDL(string name, string text)
        {
            string valueToParse = text.Trim().Substring(text.IndexOf(":") + 1);
            char[] delimiterChars = { ',' };
            string[] values = valueToParse.Split(delimiterChars);
            Dictionary<string, string> valuesToDDL = new Dictionary<string, string>();

            foreach (var item in values)
            {
                string key = item.Trim().Substring(0, 1); //get the key of the value
                string value = item.Trim().Substring(2, item.Length - 4); // start after the '(' and ends before the ')'
                valuesToDDL.Add(key, value);
            }
            string ddlName = string.Format("ddl_{0}", name);
            DropDownList ddl = pnl_AllRequiredParameters.FindControl(ddlName) as DropDownList; //get the control that fits the name of the parameter

            ddl.DataSource = valuesToDDL;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();

        }

        private void SetSeatsDDLs(string value)
        {
            string valueToParse = value.Trim().Substring(value.IndexOf(":") + 1);
            char[] delimiterChars = { ',' };
            string[] values = valueToParse.TrimStart().Split(delimiterChars);

            Dictionary<string, List<Seat>> flightsAndSeats = new Dictionary<string, List<Seat>>();

            foreach (var item in values)
            {
                Seat oSeat = new Seat();
                oSeat.FlightNumber = item.Trim().Substring(0, (item.IndexOf("-")));
                oSeat.SeatNumber = item.Trim().Substring(item.IndexOf("-") + 1, ((item.IndexOf("(") - 1) - item.IndexOf("-")));
                oSeat.Price = item.Trim().Substring(item.IndexOf("@") + 1, ((item.IndexOf(".") + 2) - item.IndexOf("@")));
                oSeat.Currency = item.Trim().Substring(item.IndexOf(".") + 3, 3);

                if (flightsAndSeats.ContainsKey(oSeat.FlightNumber))
                {
                    flightsAndSeats[oSeat.FlightNumber].Add(oSeat);
                }
                else
                {
                    flightsAndSeats.Add(oSeat.FlightNumber, new List<Seat>());
                    flightsAndSeats[oSeat.FlightNumber].Add(oSeat);
                }
            }

            if (flightsAndSeats != null)
            {
                foreach (var item in flightsAndSeats)
                {
                    DropDownList oDropDownList = new DropDownList();
                    oDropDownList.ID = "ddl_Flight_" + item.Key;
                    oDropDownList.DataSource = (from obj in item.Value
                                                select new
                                                {
                                                    Name = string.Format("{0} - {1}{2}", obj.SeatNumber, obj.Currency, obj.Price),
                                                    Value = string.Format("{0} - {1}", obj.SeatNumber, obj.Price)
                                                }).ToList();

                    oDropDownList.DataTextField = "Name";
                    oDropDownList.DataValueField = "Value";
                    oDropDownList.SelectedIndexChanged += DDL_onChange;
                    oDropDownList.DataBind();

                    Label oLabel = new Label();
                    oLabel.ID = "lbl_" + item.Key;
                    oLabel.Text = string.Format("טיסה מספר: {0}", item.Key);

                    TableRow oTableRow = new TableRow();
                    TableCell oFlightNumberCell = new TableCell();
                    oFlightNumberCell.Controls.Add(oLabel);
                    TableCell oFlightDDLCell = new TableCell();
                    oFlightDDLCell.Controls.Add(oDropDownList);

                    oTableRow.Cells.Add(oFlightNumberCell);
                    oTableRow.Cells.Add(oFlightDDLCell);

                    seatsTable.Rows.Add(oTableRow);
                }
            }

        }

        private void DDL_onChange(object sender, EventArgs e)
        {
            OrderPage oOrderPage = this.Parent as OrderPage;
            DropDownList thisDDL = sender as DropDownList;
            string selectedSeat = thisDDL.SelectedValue;
            oOrderPage.LBL_SeatTotalPrice = selectedSeat.Substring(selectedSeat.IndexOf("-")).TrimStart().TrimEnd();
        }

        /// <summary>
        ///  A method that fills all the placeholders and text attributes of the elemets that use GetText()
        /// </summary>
        protected void FillGetTexts()
        {
            // Date of Birth required:
            val_RequiredDateOfBirth.ErrorMessage = GetText("RequiredField");
            RequiredFieldValidator1.ErrorMessage = GetText("RequiredField");
            RequiredFieldValidator6.ErrorMessage = GetText("RequiredField");

            val_FirstLastNameEqual.ErrorMessage = GetText("TheFirstAndLastNameMustBeDifferent");


            rfv_PassportNumber.ErrorMessage = GetText("RequiredField");
            rfv_PassportExpireDate.ErrorMessage = GetText("RequiredField");
            RegularExpressionValidator1.ErrorMessage = GetText("NameInEngishOnly");
            RegularExpressionValidator2.ErrorMessage = GetText("NameInEngishOnly");
            val_Age.ErrorMessage = GetText("InvalidAge");
            FillPasportDDL();
            txt_FirstName.Attributes["placeholder"] = GetText("FirstNameEnglish");
            txt_LastName.Attributes["placeholder"] = GetText("LastNameEnglish");

        }

        private void FillPasportDDL()
        {
            ddl_PassportIssueCountry.DataSource = CountryContainer.SelectAllCountrys(true).DataSource;
            ddl_PassportIssueCountry.DataTextField = "Name_UI";
            ddl_PassportIssueCountry.DataValueField = "CountryCode_UI";
            ddl_PassportIssueCountry.DataBind();
            // Add the first item
            ddl_PassportIssueCountry.Items.Insert(0, new ListItem(GetText("SelectCountry"), ""));
        }

        /// <summary>
        /// Server validation for the age of each passenger at the time of the return date (departure)
        /// </summary>
        protected void val_Age_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime minAge = ConvertToValue.DateTimeEmptyValue;
            DateTime maxAge = ConvertToValue.DateTimeEmptyValue;

            // Set the age validator:
            switch (PassengerType)
            {
                case LowCostSite.FlightsSearchService.ECustomerType.ADT:
                    // how far back can the date be
                    minAge = CompareDate.AddYears(-100).Date;
                    // how close to the compare date can it be
                    maxAge = CompareDate.AddYears(-ConfigurationHandler.MaxChildAge).Date;
                    break;
                case LowCostSite.FlightsSearchService.ECustomerType.CNN:
                    // how far back can the date be
                    minAge = CompareDate.AddYears(-ConfigurationHandler.MaxChildAge).Date;
                    // how close to the compare date can it be
                    maxAge = CompareDate.AddYears(-ConfigurationHandler.MaxInfantAge).Date;
                    break;
                case LowCostSite.FlightsSearchService.ECustomerType.INF:
                    // how far back can the date be
                    minAge = CompareDate.AddYears(-ConfigurationHandler.MaxInfantAge).Date;
                    // an infant can be as young as the same date of the compare date (flight date)
                    maxAge = CompareDate.Date;
                    break;
                default:
                    break;
            }

            try
            {
                var year = ConvertToValue.ConvertToInt(ddl_BirthDay_Year.SelectedValue);
                var month = ConvertToValue.ConvertToInt(ddl_BirthDay_Month.SelectedValue);
                var day = ConvertToValue.ConvertToInt(ddl_BirthDay_Day.SelectedValue);

                DateTime dateOfBirth = new DateTime(year, month, day);

                if (dateOfBirth > minAge && dateOfBirth < maxAge)
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }

            }
            catch (Exception)
            {
                // if the date is not in a correnct format
                args.IsValid = false;
            }


        }

        #region help clasess
        public class Seat
        {
            public string FlightNumber { get; set; }
            public string SeatNumber { get; set; }
            public string Price { get; set; }
            public string Currency { get; set; }
        }
        #endregion


    }
}