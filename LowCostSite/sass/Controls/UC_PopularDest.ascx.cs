﻿using BL_LowCost;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace LowCostSite.sass
{

    public partial class UC_PopularDest : UC_BasePage
    {

        public HomePageDestinationPage oHomePageDestinationPage { get; set; }

        public string Name { get; set; }
        public string DestClass { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (oHomePageDestinationPage != null)
            {
                // set the link of the control
                var _href = QueryStringManager.RedirectWithQuery(StaticStrings.path_DestinationPage, EnumHandler.QueryStrings.ID, oHomePageDestinationPage.DestinationPageSingle.DocId_UI);
                lnk_DestinationPage.HRef =_href;
                lnk_DestinationPage_Label.HRef = _href;
                lbl_Name.Text = Name;

                Images oImage = oHomePageDestinationPage.DestinationPageSingle.ImagesContainer.FindFirstOrDefault(d => d.ImageType == EnumHandler.ImageTypes.Thumbnail.ToString());
                if (oImage != null)
                {
                    // Set the image url in the thumbnail to the uploads folder, with the name of the image of type "thumbnail" from the DB.
                    img_DestinationThumb.ImageUrl = String.Format("{0}/{1}", StaticStrings.path_Uploads, oImage.ImageFileName_UI);

                    // Set the alt and title of the image
                    img_DestinationThumb.AlternateText = oImage.Alt_UI;
                    img_DestinationThumb.ToolTip = oImage.Title_UI;

                }
            }

        }
    }
}