﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Flight.ascx.cs" Inherits="LowCostSite.sass.UC_Flight" %>

<!-- A flight contains both the main descriptions (the title line) and all the flight details (flight legs and nstops between them) -->

<li class="allRow" runat="server" id="mainRow">
    <div class="text-center col-md-1 no-padding" >
        <asp:Image ID="img_AirlineLogo" runat="server" CssClass="logo-size" />
    </div>
    <div class="col-md-11 new-width">
        <table class="full-width row-info-time">
            <tr class="title-row">
                <!-- DEPARTURE TIME -->
                <td class="font-size-large text-space col-md-2 no-padding">
                    <asp:Label ID="lbl_BoardingTime" runat="server" />
                </td>
                <!-- DURATION -->
                <td class="font-size-large price-color col-md-2 no-padding">
                    <asp:Label ID="lbl_FlightDuration" runat="server" />
                </td>
                <!-- ARRIVAL TIME -->
                <td class="font-size-large col-md-2 no-padding">
                    <asp:Label ID="lbl_LandingingTime" runat="server"  />
                    <i runat="server" id="info_NextDayLanding" class="glyphicon glyphicon-info-sign i-next-day" title="נחיתה ביום שלמחרת" style="display:none;"></i>
                </td>
                <!-- STOPS NUMBER -->
                <td class="title_content col-md-2 no-padding ">
                    <asp:Label ID="lbl_StopsCount" runat="server" />
                  <%--  <%=GetText("_stops")%>--%>
                </td>
                <!-- PRICE -->
                <td class="font-size-large details-size col-md-2 no-padding ltr">
                    <asp:Label ID="lbl_Price" class="lbl_Price" runat="server"  />
                </td>
                <!-- MORE DETAILS AND SELECT -->
                <td class="font-size-large col-md-2 no-padding">
                    <div class="checkbox line-height-1 inline">
                        <label>
                            <input type="radio" id="rb_group" class="rb_group" runat="server" onclick="SetTotalPrice(this)"/>
                            <!-- The command argument is set when creating the user control -->
                            <asp:LinkButton ID="btn_Details" CssClass="ShowDetails" CommandArgument="" runat="server" data-onBack="toggleDetails(this)" OnClientClick='return BeforeDetails(this)' OnClick="btn_Details_Click"><%=GetText("cDetails") %></asp:LinkButton>
                        </label>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</li>
<li class="list-no-border"  runat="server" id="InfoRow">
    <div id="listInfoClose" class="full-width " runat="server">
        <asp:UpdatePanel ID="details" runat="server" UpdateMode="Conditional" RenderMode="Inline" style="display: none;" class="details">
            <ContentTemplate>
                <asp:UpdateProgress AssociatedUpdatePanelID="details" ID="uProgress_loadingDetails" runat="server">
                    <ProgressTemplate>
                       <%=GetText("LoadingDetails")%>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:PlaceHolder ID="ph_FlightLegs" runat="server">
                    <!-- PLACE HOLDER FOR ALL THE FLIGHT LEGS (UC_FlightLeg) AND THE STOP TIMES BETWEEN THEM -->
                </asp:PlaceHolder>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btn_Details" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</li>










