﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_PleaseWaitModal.ascx.cs" Inherits="LowCostSite.sass.Controls.UC_Loading" %>

<div class="modal fade" id="pleaseWaitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title text-center font-large font-bold price-color rtl" id="myModalLabel">
                    <img src="/sass/images/logo.png" /><br />
                    <%--<%=GetText("SearchingForTheBestPricesForYou") %>--%>
                    אנא המתן...
                    <br />
                    <img src="/sass/images/Untitled2.gif" />

                </h4>
            </div>
          
        </div>
    </div>
</div>
