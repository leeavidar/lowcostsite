﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Loading.ascx.cs" Inherits="LowCostSite.sass.Controls.UC_Loading" %>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title text-center font-large font-bold price-color rtl" id="myModalLabel">
                    <img src="/sass/images/logo.png" /><br />
                    <%=GetText("SearchingForTheBestPricesForYou") %>
                    <br />
                    <img src="/sass/images/Untitled2.gif" />

                </h4>
            </div>
            <h4 class="modal-body text-center font-large" style="display:none">
                <b><%=GetText("Departure") %>: </b>

                <%--יום שני 15/05/2014 תל אביב - ניו יורק--%>
                <span class="lbl_loadingOutward" id="lbl_loadingOutward" runat="server"></span>
                <br />
                <br />
                <b><%=GetText("Return") %>: </b>
                <span class="lbl_loadingReturn" id="lbl_loadingReturn" runat="server"></span>
                <%--   יום שני 15/05/2014 תל אביב - ניו יורק--%>
            </h4>
        </div>
    </div>
</div>
