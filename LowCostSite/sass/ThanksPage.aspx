﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/sass/Master/MasterPage.Master" CodeBehind="ThanksPage.aspx.cs" Inherits="LowCostSite.sass.ThanksPage" %>

<%@ Register Assembly="EO.Web" Namespace="EO.Web" TagPrefix="eo" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <script src="javascripts/PageScripts/OrderPage.js"></script>
    <link href="/sass/style.css" rel="stylesheet" />
    <script src="/assets/dist/js/bootstrap.min.js"></script>
    <script src="/sass/javascripts/PageScripts/MasterPage.js"></script>
    <script src="/sass/javascripts/tooltip.js"></script>
    <link href="../assets/ExtraStyling.css" rel="stylesheet" />
    <style>
        .b-day {
            height: 34px;
            width: 29%;
        }

        .phone-prefix {
            width: 60%;
            display: inline-block;
        }

        .h4-flight-title {
            border-bottom: 1px solid black;
            padding-bottom: 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <eo:ASPXToPDF ID="ASPXToPDF" runat="server"></eo:ASPXToPDF>
    <div>
        <br />
        <div id="areaForPdf" class="container" style="width: 850px;">
            <div class="content-page">
                <div class="row">
                    <!-- START: RIGHT MAIN COLUMN -->
                    <div class="col-sm-12">
                        <div style="background-color: rgb(187, 67, 67)">
                            <h1 style="margin: 0px;"><%=GetText("ThenksForYouChoosingLOWCOST")%></h1>
                        </div>
                        <div id="div_generalDiv" runat="server">
                            <!-- START: DETAILS -->
                            <div class="highlight rtl display_inline_block full-width no-margin-bottom">
                                <div class="box-wrap-content no-padding-bottom ">
                                    <div class="box-wrap-title"><%=GetText("FlightDetails")%>:</div>
                                    <div class="row no-margin-side">
                                        <div class="col-sm-4" style="margin-top: 14px;">
                                            <div class="row-space text-right font-bold font-large">
                                                <h4 class="padding-side-title"><%=GetText("Flight")%>  >  <span runat="server" id="lbl_Origin"></span> > <span runat="server" id="lbl_Destination"></span>
                                                </h4>
                                            </div>
                                            <div class="row-space ">
                                                <span class="image-space font-bold ">
                                                    <img src="images/out_blue.png" /></span><span runat="server" id="lbl_OutwardDate"></span>
                                            </div>
                                            <div runat="server" id="FlightDetails_ReturnDate" class="row-space">
                                                <span class="image-space font-bold ">
                                                    <img src="images/in_blue.png" /></span>
                                                <span runat="server" id="lbl_ReturnDate"></span>
                                            </div>
                                            <br />
                                            <div class=" rtl display_inline_block full-width no-margin-bottom seperate-border-top">
                                                <div class="row display-none" runat="server" id="div_AdultPrice">
                                                    <div class="col-sm-3">
                                                        <%=GetText("Adult")%>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        X
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <asp:Label ID="lbl_PriceBox_AdultCount" runat="server" />
                                                    </div>
                                                    <div class="col-sm-5 price-color text-left ltr">
                                                        <asp:Label ID="lbl_PriceBox_AdultPrice" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row display-none" runat="server" id="div_ChildPrice">
                                                    <div class="col-sm-3">
                                                        <%=GetText("Child")%>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        X
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <asp:Label ID="lbl_PriceBox_ChildCount" runat="server" />
                                                    </div>
                                                    <div class="col-sm-5 price-color text-left ltr">
                                                        <asp:Label ID="lbl_PriceBox_ChildPrice" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row display-none" runat="server" id="div_InfantPrice">
                                                    <div class="col-sm-3">
                                                        <%=GetText("Infant")%>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        X
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <asp:Label ID="lbl_PriceBox_InfantCount" runat="server" />
                                                    </div>
                                                    <div class="col-sm-5 price-color text-left ltr">
                                                        <asp:Label ID="lbl_PriceBox_InfantPrice" runat="server" />
                                                    </div>
                                                </div>
                                                <br />


                                                   <div class="row display-none rtl" runat="server" id="div_HandlingFee">
                                                    <div class="col-sm-3">
                                                          <%=GetText("HandelingFee")%>:
                                                    </div>
                                                    <div class="col-sm-1">
                                                        X
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <asp:Label ID="lbl_PassengerNumber" runat="server" />
                                                    </div>
                                                    <div class="col-sm-5 price-color text-left ltr">
                                                        <asp:Label ID="lbl_HandlingFee" runat="server" />
                                                    </div>
                                                </div>
                                               
                                                <div class="row display-none" runat="server" id="div_Seats">
                                                    <div class="col-sm-6">
                                                        <%=GetText("Seating")%>:
                                                    </div>
                                                    <div class="col-sm-4 text-left price-color ltr">
                                                        <asp:Label ID="lbl_SeatsTotalPrice" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter-title-default text-space text-right rtl">
                                                <%=GetText("TotalForPayment")%>:
                        <div class="title_content text-left price-color text-space ltr display-inline">
                            <asp:Label ID="lbl_TotalPrice" runat="server" />
                        </div>
                                            </div>
                                        </div>

                                        <!-- START: Repeater for the OUTWARD flight segments summary -->
                                        <div class="col-sm-4">
                                            <h3 class="padding-side-title"><%=GetText("OutwardFlight")%>
                                            </h3>
                                            <asp:Repeater ID="rpd_OutwardSegments" runat="server" ItemType="LowCostSite.FlightsSearchService.FlightSegment">

                                                <ItemTemplate>
                                                    <div class="row">

                                                        <div class="col-sm-12 rtl">
                                                            <h4 class="h4-flight-title"><%=GetText("FlgihtNo") %> <%# Container.ItemIndex + 1 %></h4>
                                                            <!-- יציאה: -->
                                                            <b><%=GetText("Departure")%>:</b>
                                                            <br />
                                                            <%# GenerateSegmentString(Item,false) %>
                                                            <br />
                                                            <b><%=GetText("Arrival")%>:</b><br />
                                                            <%# GenerateSegmentString(Item,true) %>
                                                            <br />
                                                            <b><%=GetText("Airline")%>:</b>
                                                            <%# Item.OperatingAirline.Name %>
                                                            <br />
                                                            <b><%=GetText("FlightNumber")%>: </b>  <%# Item.FlightCode %><br />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <!-- END: Repeater for the OUTWARD flight segments summary -->
                                        <div runat="server" id="div_outwardFlightsDetails" class="display-none">
                                            <!-- START: Repeater for the RETURN flight segments summary -->
                                            <div class="col-sm-4">
                                                <h3 class="padding-side-title"><%=GetText("ReturnFlight")%>
                                                </h3>

                                                <asp:Repeater ID="rpd_ReturnSegments" runat="server" ItemType="LowCostSite.FlightsSearchService.FlightSegment">
                                                    <ItemTemplate>
                                                        <div class="row">

                                                            <div class="col-sm-12">
                                                                <h4 class="h4-flight-title"><%=GetText("FlightNo")%> <%# Container.ItemIndex + 1 %></h4>
                                                                <b><%=GetText("Departure")%>:</b><br />
                                                                <%# GenerateSegmentString(Item,false) %>
                                                                <br />
                                                                <b><%=GetText("Arrival")%>:</b><br />
                                                                <%# GenerateSegmentString(Item,true) %>
                                                                <br />
                                                                <b><%=GetText("Airline")%>:</b>
                                                                <%# Item.OperatingAirline.Name %>
                                                                <br />
                                                                <b><%=GetText("FlightNumber")%>: </b> <%#Item.FlightCode%><br />
                                                            </div>
                                                        </div>
                                                        <hr />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <!-- END: Repeater for the RETURN flight segments summary -->
                                        </div>

                                    </div>
                                </div>
                                <div class="box-wrap-content no-padding-bottom ">
                                    <div class="box-wrap-title"><%=GetText("PassengersDetails")%>:</div>
                                    <!-- START: PLACE HOLDER FOR EACH PASSENGERS DETAILS-->
                                    <asp:PlaceHolder ID="ph_PassengerDetails" runat="server"></asp:PlaceHolder>
                                    <!-- END: PLACE HOLDER FOR EACH PASSENGERS DETAILS-->
                                    <!-- START: CONTACT DETAILS -->
                                    <div class="row no-margin-side" style="page-break-inside: avoid;">
                                        <h3 class="padding-side-title"><%=GetText("ContactDetails")%></h3>
                                    </div>
                                    <div class="col-sm-12">
                                        <%--<asp:CheckBox ID="cb_isContactFirstPax" CssClass="cb_isContactFirstPax" runat="server" Text="זהו הנוסע הראשון" />--%>
                                    </div>
                                    <!-- START: ROW FOR: TITLE, FIRST NAME, LAST NAME-->
                                    <div class="row no-margin-side">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <span style="color: red">*</span><label class="control-label"><%=GetText("FirstName")%>:</label>
                                                <asp:Label ID="lbl_ContactFirstName" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"><%=GetText("LastName")%>:</label>
                                                <asp:Label ID="lbl_ContactLastName" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END: ROW FOR: TITLE, FIRST NAME, LAST NAME-->
                                    <!-- START: ROW FOR: STREET, HOUSENUMBER, CITY-->
                                    <%-- <div class="row no-margin-side">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("Street")%>:</label>
                                        <asp:TextBox ID="txt_Street" class="form-control" runat="server"/>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="contactsDetails" ControlToValidate="txt_Street" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="Red"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("HouseNo")%>:</label>
                                        <asp:TextBox class="form-control" ID="txt_HouseNumber" runat="server"/>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="contactsDetails" ControlToValidate="txt_HouseNumber" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="Red"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("City")%>:</label>
                                        <asp:TextBox ID="txt_City" class="form-control" runat="server" placeholder='gtxt_City' />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="contactsDetails" ControlToValidate="txt_City" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="Red"/>
                                    </div>
                                </div>
                            </div>--%>
                                    <!-- END: ROW FOR: STREET, HOUSENUMBER, CITY-->
                                    <!-- START: ROW FOR: ZIPCODE, COUNTRY, EMAIL-->
                                    <div class="row no-margin-side">
                                        <%--  <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("ZIPCode")%>:</label>
                                        <asp:TextBox ID="txt_PostalCode" class="form-control" runat="server" placeholder='gtxt_ZIPExample' />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="contactsDetails" ControlToValidate="txt_PostalCode" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="Red"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("Country")%>:</label>
                                        <asp:TextBox ID="txt_Country" class="form-control" runat="server" Text="IL" />
                                    </div>
                                </div>
                                        --%>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"><%=GetText("EmailAddress")%>:</label>
                                                <asp:Label ID="lbl_Email" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END: ROW FOR: ZIPCODE, COUNTRY, EMAIL-->
                                    <!-- START: ROW FOR: PHONE NUMBER, SECONDNUMBER (AND PLACE FOR ANOTHER COL)-->
                                    <div class="row no-margin-side">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"><%=GetText("PhoneNumber")%>:</label><br />
                                                <asp:Label ID="lbl_MainPhone" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"><%=GetText("SecondPhoneNumber")%>:</label><br />
                                                <asp:Label ID="lbl_PhoneSecond" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                        <!-- START: 2 ROWS FOR Order Details -->
                                        <div class="row no-margin-side" style="page-break-after: avoid;">
                                            <h3 class="padding-side-title"><%=GetText("OrderDetails")%></h3>
                                        </div>
                                        <div class="row no-margin-side" style="page-break-inside: avoid; page-break-after: avoid;">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label"><%=GetText("LowCostPNR")%>:</label><br />
                                                    <asp:Label ID="lbl_LowCostPNR" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label"><%=GetText("SupplierPNR")%>:</label><br />
                                                    <asp:Label ID="lbl_SupplierPNR" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- START: 2 ROWS FOR Order Details -->
                                        <!-- END: ROW FOR: PHONE NUMBER, SECONDNUMBER -->
                                        <!-- START: 2 ROWS FOR CHECKBOXES -->
                                        <div class="row no-margin-side">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-margin-side">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END: 2 ROWS FOR CHECKBOXES -->
                                    
                                    <!-- END: CONTACT DETAILS -->
                                </div>
                            </div>
                            <!-- END: DETAILS -->
                            <!-- START: TOTAL PRICE ROW -->
                            <div class="row price-box-details rtl">
                                <div class="row">
                                    <div class="col-sm-12 rtl text-center">
                                        <%=GetText("TotalPriceToCharge")%>: 
                                        <span class="title_content text-center" runat="server" id="lbl_FooterTotalPrice"></span>
                                        <span class="title_content text-center" runat="server" id="lbl_FooterTotalCurrency"></span>
                                        <div class="left">
                                            <!-- START: BUTTON TO CONTINUE TO BOOKING PAYMENT -->

                                            <!-- END: BUTTON TO CONTINUE TO BOOKING PAYMENT -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END: TOTAL PRICE ROW -->
                        </div>
                    </div>
                    <!-- END: RIGHT MAIN COLUMN -->
                </div>
            </div>
        </div>
    </div>
</asp:Content>

