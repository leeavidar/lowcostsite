﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="LowCostSite.sass.Index" %>

<%@ MasterType VirtualPath="~/sass/Master/MasterPage.master" %>
<%@ Register Src="~/sass/Controls/UC_PopularDest.ascx" TagName="UC_PopularDest" TagPrefix="uc1" %>
<%@ Register Src="~/sass/Controls/UC_Search.ascx" TagName="UC_Search" TagPrefix="uc1" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script src="../assets/js/transition.js"></script>
    <script src="/sass/javascripts/PageScripts/HomePage.js"></script>
    <script src="/sass/javascripts/PageScripts/UC_Search.js"></script>
    <script src="\sass/javascripts/jquery.placeholder.js"></script>
    <script>
        // for the loading box
        $('#myModal').modal();
    </script>
    <style>
        .row-prom-size {
            display: inline-block;
            width: 100%;
            height: auto;
        }

        .img-closed {
            height: 670px !important;
            width: 100%;
        }

        .absolute {
            position: absolute;
            z-index: 0;
        }
    </style>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="carousel-example-generic" class="carousel slide absolute img-closed center-image" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="/sass/images/HomePageSlider/1.jpg" class="back-image img-closed" />
                <div class="carousel-caption">
                </div>
            </div>
            <div class="item ">
                <img src="/sass/images/HomePageSlider/2.jpg" class="back-image img-closed" />
                <div class="carousel-caption">
                </div>
            </div>
            <div class="item ">
                <img src="/sass/images/HomePageSlider/3.jpg" class="back-image img-closed" />
                <div class="carousel-caption">
                </div>
            </div>
        </div>
    </div>
    <div class="content-page size-main-page">
        <div class="row row_promotion destination-box rtl">
            <!-- START: PAGE TITLE (4 LINES) -->
            <div class="col-md-8 right">
                <div class="row">
                    <span class="row_text_gallery">
                        <asp:Label ID="txt_Line1" runat="server" Text="" /></span>
                </div>
                <div class="row">
                    <span class="row_text_gallery">
                        <asp:Label ID="txt_Line2" runat="server" Text="" /></span>
                </div>
                <div class="row">
                    <span class="row_text_gallery">
                        <asp:Label ID="txt_Line3" runat="server" Text="" /></span>
                </div>
                <div class="row">
                    <span class="row_text_gallery">
                        <asp:Label ID="txt_Line4" runat="server" Text="" /></span>
                </div>
            </div>
            <!-- END: PAGE TITLE (4 LINES) -->
            <div class="col-md-4 display-none">
                <div class="col-md-12 right">
                    <div class="list-group">
                        <a href="#" class="list-group-item ">
                            <h2 class="list-group-item-heading">חיפושים אחרונים</h2>
                        </a>
                    </div>
                    <div class="list-group">
                        <a href="#" class="list-group-item ">
                            <div class="arrow">< </div>
                            <h4 class="list-group-item-heading">תל אביב - לונדון</h4>
                            <p class="list-group-item-text">
                                01/02/2014
                            <br />
                                <span>החל מ-</span>
                                <span class="price-color">160 $</span>
                            </p>
                        </a>
                        <a href="#" class="list-group-item ">
                            <div class="arrow">< </div>
                            <h4 class="list-group-item-heading">תל אביב - לונדון</h4>
                            <p class="list-group-item-text">
                                01/02/2014
                            <br />
                                <span>החל מ-</span>
                                <span class="price-color">160 $</span>
                            </p>
                        </a>
                        <a href="#" class="list-group-item ">
                            <div class="arrow">< </div>
                            <h4 class="list-group-item-heading">תל אביב - לונדון</h4>
                            <p class="list-group-item-text">
                                01/02/2014
                            <br />
                                <span>החל מ-</span>
                                <span class="price-color">160 $</span>
                            </p>
                        </a>
                        <a href="#" class="list-group-item ">
                            <div class="arrow">< </div>
                            <h4 class="list-group-item-heading">תל אביב - לונדון</h4>
                            <p class="list-group-item-text">
                                01/02/2014
                            <br />
                                <span>החל מ-</span>
                                <span class="price-color">160 $</span>
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN: SEARCH BOX-->
        <div class="wrap-search">
            <uc1:UC_Search runat="server" ID="uc_Search" />
        </div>
        <!-- END: SEARCH BOX-->
    </div>
    <div class="container">
        <div class="content-page inline-block">
            <!--יעדים פופולארים-->
            <div class="row destinationsDiv">
                <h2 class=" hot-destination-title"><%=GetText("PopularDestinations") %></h2>
                <asp:PlaceHolder ID="ph_PopularDestinations" runat="server">
                    <!-- Here will be the populawr destinations user controls -->
                </asp:PlaceHolder>
            </div>
            <br /><br /><br /><br />
            <div class="row color_blue text-left pointer bottom_space">
                <h4 class="lnk_ShowMoreDest link-color"><span class="text-show-more"> <%=string.Format("< {0}",GetText("ShowMoreDestinations") ) %> </span><span class="text-hide-more" style="display: none;"><%=GetText("HideMoreDestinations") %> ></span>
                </h4>
            </div>
            <!-- יעדים פופולארים סוף-->

            <!--טיפים-->
            <div class="tips">
                <div class="row inline-block">
                    <div class="container tip-box-bg no_padding">
                        <div class="col-md-2 right tip_box">
                            <h4><%=GetText("OurTips")%></h4>
                        </div>
                        <div class="col-md-8 right gray_bg color_dark">
                            <span class="tip_info">
                                <h4>
                                    <asp:Label ID="lbl_Tip" runat="server" Text="Label"></asp:Label></h4>
                            </span>
                        </div>
                        <div class="col-md-2 LEFT no-padding ">
                            <a class="btn btn-default btn-lg btn-block btn-tip no-shadow no-radius" href="/sass/Tips.aspx"><%=GetText("ReadMore")%></a>
                        </div>
                    </div>
                </div>
            </div>
            <!--טיפים סוף-->
        </div>
    </div>
</asp:Content>
