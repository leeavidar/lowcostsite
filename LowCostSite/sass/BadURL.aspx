﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="BadURL.aspx.cs" Inherits="LowCostSite.BadURL" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="/css/StyleSheet.css" rel="stylesheet" />
    <link href="/css/StyleSheetUI.css" rel="stylesheet" />
    <link href="/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="/css/wireframe.css" rel="stylesheet" type="text/css">
    <link href="/css/main.css" rel="stylesheet" type="text/css">
    <link href="/css/media.css" rel="stylesheet" type="text/css">
    <link href="/css/custom.less" rel="stylesheet/less" type="text/css">
    <link rel="stylesheet" href="/royalslider/royalslider.css">
    <link rel="stylesheet" href="/css/plugins.css">
    <link rel="stylesheet" href="/js/plugins/pretty-photo/css/prettyPhoto.css" type="text/css">
    <link href="/css/fonts.googleapis.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" style="font-family:'Roboto Condensed';">

             <h1 class="color_blue" style="text-align: center;font-size: 60px;"><%=GetText("BAD_URL_TEXT_1")%></h1>
            <div class="hr-breadcrumbs divider-heder" style="color:#474748; ">
                <div class="assistive-text"></div>
                <ol class="breadcrumbs wf-td text-small">
                </ol>
            </div>
            <section class="shortcode-action-box shortcode-action-bg plain-bg text-centered no-line"  style="color:#474748; ">
                <div class="shortcode-action-container">
                    <div class="shortcode-action-container">
                        <div style="direction:rtl">
                            <%=GetText("BAD_URL_TEXT_2") %><br />
                            <br />
                            <%=GetText("BAD_URL_TEXT_3") %> :
                                <br />
                            <br />
                            <span><%=WhiteLabelEmail%> </span>
                        </div>
                    </div>
                </div>
            </section>
      
    </form>
</body>
</html>
