﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DL_Generic;
using Generic;
using BL_LowCost;
using DL_LowCost;
using PelecardInterface;
using System.Xml.Serialization;
using System.IO;
using LowCostSite.sass;
using LowCostSite.FlightsBookingService;

namespace LowCostSite.sass
{
    public partial class PelecardPayment : BasePage_UI
    {
        #region Prop
        #region Private
        //private FlightBookedDetails _oSessionFlightBookedDetails = null;
        private OrderObject _oSessionOrderObject;
        #endregion
        #endregion
        #region Session Methods

        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            if (!isPostBack && SessionManager.GetSession<OrderObject>(out _oSessionOrderObject) == false)
            {
                _oSessionOrderObject = GetOrderObjectFromFile();
            }
        }

        private OrderObject GetOrderObjectFromFile()
        {
            OrderObject oOrderObject = null;
            // If we can't find the order object from the session
            try
            {
                DateTime dateCreated = ConvertToValue.ConvertToDateTime(Request.QueryString["dateCreated"]);
                string DocId = Request.QueryString["orderDocId"];
                // get the object from file:
                oOrderObject = (OrderObject)createOrderObjectFromXml(BasePage_UI.GetXMLDirByDateAndOrderId(dateCreated, DocId) + "\\XMLOrder_" + DocId + ".xml", typeof(OrderObject));
            }
            catch (Exception e)
            {
                // Can't get the file path from the query string
                LoggerManagerProject.LoggerManager.Logger.WriteError("PelecardPayment.aspx:  Can't get the order details from the query string to build the order object. Error Message: " + e.Message);
                // Redirect to error page (outside of the iframe
                // AddScript("window.parent.location.href='" + StaticStrings.path_ErrorPage);
                Response.Redirect(StaticStrings.path_ErrorIFrame);
            }
            return oOrderObject;

        }

        /// <summary>
        /// Create (Full) Order Object From Xml
        /// </summary>        
        private object createOrderObjectFromXml(string fileName, Type type)
        {
            OrderObject obj = BinaryHandler.DeserializeBinary<OrderObject>(fileName);
            return obj;
            #region OLD - with xml serialization
            #endregion
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Session["OrderObject"] = oSessionOrderObject;
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            //No need to delete order session - this session will be deleted after payment
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        //protected void Page_LoadComplete(object sender, EventArgs e) Page_Loade Event is exist in the BasePage
        //{
        //    SaveToSession();
        //}

        #endregion
    }

    public partial class PelecardPayment : BasePage_UI
    {
        public OrderObject oSessionOrderObject { get { return _oSessionOrderObject; } set { _oSessionOrderObject = value; } }

        public int WhiteLabelDocID
        {
            get
            {
                int whiteLabelDocID = BasePage_UI.GetWhiteLabelDocID();

                return whiteLabelDocID;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (oSessionOrderObject != null)
            {
                // GetPaymentForm();
            }
            else
            {
                // TODO : Log
                //LoggerManagerLogicProject.LoggerManager.Logger.WriteWarrning("PelecardPayment.aspx - oSessionOrderObject Is Null.");
            }
        }

        #region Page Functions
        private string GetCurrencyKey(string currency)
        {
            switch (currency)
            {
                case ("EUR"):
                    return "978";
                case ("NIS"):
                    return "1";
                case ("USD"):
                    return "2";
                default:
                    return "Error";
            }
        }

        public PelecardRedirectTrans GetPaymentForm()
        {
            if (oSessionOrderObject == null)
            {
                LoggerManagerProject.LoggerManager.Logger.WriteWarrning("PelecardPayment.aspx - oSessionOrderObject Is Null.");
                return null;
            }
            else
            {
                PelecardRedirectTrans oPaymentPelecard = null;

                Orders oOrders = OrdersContainer.SelectByKeysView_WhiteLabelDocId_DocId(WhiteLabelDocID, oSessionOrderObject.OrderDocId, true).Single;
                if (oSessionOrderObject != null && oOrders != null)
                {
                    //TODO - need to add algorithm to calculate is j5 value
                    bool isJ5 = false;//oOrders.OrderPaymentType != (int)EnumTypes.OrderPaymentTypes.J4;

                    //Send GUID to pelecard - as TerminalID id
                    string guid = oOrders.OrderToken;
                    string contactEmail = BasePage_UI.WhiteLabelObj.Email_UI;
                    double totalPrice = ConvertToValue.ConvertToDouble(oSessionOrderObject.TotalPriceWithHandlingFinal) * 100;
                    string currencyKey = GetCurrencyKey(oSessionOrderObject.FlightBookTotalNetCurrency);
                    if (IsFakePalecardPayment)
                    {
                        currencyKey = "1"; //NIS
                        totalPrice = FakeAmount * 100;
                    }
                    if (currencyKey != "Error")
                    {
                        //string textForBottom = string.Format("{0}: {1} | {2}: {3}",GetText("MailForContact"), contactEmail, GetText("OrderToken"), oOrders.OrderToken_UI);
                        string textForBottom = string.Format("{0}: {1}", "Email for contact", contactEmail);

                        // Create the object for payment
                        oPaymentPelecard = new PelecardRedirectTrans(isJ5, totalPrice, currencyKey, "", textForBottom, oOrders.OrderToken_UI);
                        //TEST OBJECT
                    }
                    else
                    { 
                    //שימוש במטבע לא נתמך ע"י פלאכארד
                    }
                    
                }
                else
                {
                    //Cannot find order in file

                    //TODO: Redirect to failed order
                    //LoggerManagerLogicProject.LoggerManager.Logger.WriteWarrning("PelecardPayment.aspx - Cannot find order file.");
                }

                //return formPayment;
                return oPaymentPelecard;
            }
        }

        #endregion
    }
}