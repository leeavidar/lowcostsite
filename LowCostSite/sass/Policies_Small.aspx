﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Policies_Small.aspx.cs" Inherits="LowCostSite.sass.Policies_Small" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="height:100%">
<head runat="server">
        <link href="/sass/style.css" rel="stylesheet" />
        <script src="/sass/javascripts/jquery-1.11.0.min.js"></script>
    <script src="/sass/javascripts/jquery.validate.js"></script>
    <script src="/assets/dist/js/bootstrap.min.js"></script>
    <title>תקנון האתר</title>
</head>
<body style="height:100%">
            <div class="highlight rtl display_inline_block full-width" style="height:100%; overflow-y:scroll">
                <div class="box-wrap-content">
                    <div class="box-wrap-title">תקנון</div>

                    <div class="tips-box">
                        <div class="highlight rtl ">
                            <!-- Content for about us page will be placed here (from the DB)-->
                            <div runat="server" id="div_policies">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</body>
</html>
