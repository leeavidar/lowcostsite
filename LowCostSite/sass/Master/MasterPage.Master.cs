﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using LowCostSite.sass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LowCostSite.Master
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        #region Session Private fields

        private StaticPagesContainer _oSessionStaticPages;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions (or from the database if its the first load of the page or the session is empty)
        /// </summary>
        /// <param name="isPostBack"></param>
        private void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<StaticPagesContainer>(out _oSessionStaticPages) == false)
            {
                //take the data from the table in the database
                _oSessionStaticPages = GetStaticPagesFromDB();
            }
            else { /*Return from session*/}

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        private void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<StaticPagesContainer>(oSessionStaticPages);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        private void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<StaticPagesContainer>(oSessionStaticPages);
        }

        private StaticPagesContainer GetStaticPagesFromDB()
        {

            //selecting all the static pages with a given white label
            return StaticPagesContainer.SelectAllStaticPagess(BasePage_UI.GetWhiteLabelDocID(), null); 
        }

        #endregion

        #region Events

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            //CallPageScript();
        }

        #endregion

    }



    public partial class MasterPage : System.Web.UI.MasterPage
    {
        #region Properties
        public int WhiteLabelId { get; set; }
        #region Private
        #endregion
        #region Public
        public string PageTitle { get; set; }
        public StaticPagesContainer oSessionStaticPages { get { return _oSessionStaticPages; } set { _oSessionStaticPages = value; } }
        public HomePageDestinationPageContainer Destinations
        {
            get
            {
                // get the pages from the database:
                HomePageDestinationPageContainer oHomePageDestinationPageContainer = HomePageDestinationPageContainer.SelectAllHomePageDestinationPages(BasePage_UI.GetWhiteLabelDocID(), null).FindAllContainer(pop => pop.DestinationPageSingle != null);
                // sort by order field:
                oHomePageDestinationPageContainer = oHomePageDestinationPageContainer.SortByKeyContainer(KeyValuesType.KeyHomePageDestinationPageOrder, false);

                return oHomePageDestinationPageContainer;
            }
        }

        public CompanyPageContainer Companies
        {
            get
            {
                return CompanyPageContainer.SelectAllCompanyPages(BasePage_UI.GetWhiteLabelDocID(), true); 
            }
        }
        public string HomePageProp { get; set; }
        public string AboutUsProp { get; set; }
        public string PopularDestinationsProp { get; set; }
        public string AirlinesProp { get; set; }
        public string FaqProp { get; set; }
        public string ContactUsProp { get; set; }
        public string PoliciesProp { get; set; }

        public string FooterData { get; set; }

        public string Language { get; set; }

        #endregion

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            SetLanguage();
            #region Session handeling
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
            #endregion

            // Fill the footer (not only if not Postback, because than it will dissapear): 
            StaticPages oFooter = oSessionStaticPages.FindFirstOrDefault(page => page.Name == "Footer");
            if (oFooter != null)
            {
                FooterData = oFooter.Text_UI;
            }
            else
            {
                // the footer doesn't exist in the database or doesn't have the same doc id. check the doc id provided.
            }

            SetDestinationRepeater();
            SetCompaniesRepeater();
        }

        /// <summary>
        /// A method that sets the language determines which language the site should display (saves the name of the language in the Language property)
        /// </summary>
        private void SetLanguage()
        {
            //   Settings.CurrentLanguage = 2;
            //   Language = LangManager.Lang;
        }


        /// <summary>
        /// A method that makes the current page hilighted
        /// </summary>
        /// <param name="Page">The current page (enum)</param>
        public void SetMenuClass(EnumHandler.MenuPages Page)
        {
            clearAllPropClasses();
            string ActiveMenu = "nav_responsive active";


            switch (Page)
            {

                case EnumHandler.MenuPages.HomePage:
                    HomePageProp = ActiveMenu;
                    break;
                case EnumHandler.MenuPages.AboutUs:
                    AboutUsProp = ActiveMenu;
                    break;
                case EnumHandler.MenuPages.PopularDestination:
                    PopularDestinationsProp = ActiveMenu;
                    break;
                case EnumHandler.MenuPages.Airlines:
                    AirlinesProp = ActiveMenu;
                    break;
                case EnumHandler.MenuPages.Faq:
                    FaqProp = ActiveMenu;
                    break;
                case EnumHandler.MenuPages.ContactUs:
                    ContactUsProp = ActiveMenu;
                    break;
                case EnumHandler.MenuPages.Policies:
                    PoliciesProp = ActiveMenu;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// A methd that's being called on each page and populates the meta tags with the page's title, description and keywords.
        /// </summary>
        /// <param name="pageSeo">A seo object containing the data to insert.</param>
        public void SetMetaData(Seo pageSeo)
        {
            if (pageSeo != null)
            {
                //HtmlMeta title = new HtmlMeta() { Name = "title", Content = pageSeo.SeoTitle_UI };
                HtmlMeta description = new HtmlMeta() { Name = "description", Content = pageSeo.SeoDescription_UI };
                HtmlMeta keywords = new HtmlMeta() { Name = "keywords", Content = pageSeo.SeoKeyWords_UI };
                //MetaPlaceHolder.Controls.Add(title);
                PageTitle = pageSeo.SeoTitle_UI;
                MetaPlaceHolder.Controls.Add(description);
                MetaPlaceHolder.Controls.Add(keywords);
            }
       
        }



        /// <summary>
        /// A meothd that un-highlights all the menu items
        /// </summary>
        private void clearAllPropClasses()
        {
            string InactiveMenu = "nav_responsive";

            HomePageProp = InactiveMenu;
            AboutUsProp = InactiveMenu;
            PopularDestinationsProp = InactiveMenu;
            AirlinesProp = InactiveMenu;
            FaqProp = InactiveMenu;
            ContactUsProp = InactiveMenu;
            PoliciesProp = InactiveMenu;
        }

        protected void btn_MailingList_Click(object sender, EventArgs e)
        {
            if (txt_Email.Text != "")
            {
                MailAddress oMailAddress = new MailAddress()
                {
                    Address = txt_Email.Text
                };


                int result;
                // Check if the mail doesn't already exist:
                MailAddressContainer oMailAddressContainer = MailAddressContainer.SelectByKeysView_WhiteLabelDocId_Address(BasePage_UI.GetWhiteLabelDocID(), oMailAddress.Address_UI, null); 
                if (oMailAddressContainer.Count == 0)
                {
                    result = oMailAddress.Action(DL_Generic.DB_Actions.Insert);
                    txt_Email.Text = "";
                    if (result != -1)
                    {

                        txt_Email.Attributes.Add("placeholder", "כתובתך נוספה למערכת!");
                    }
                    else
                    {
                        txt_Email.Attributes.Add("placeholder", "אירעה שגיאה.");
                    }
                }
                else
                {
                    txt_Email.Text = "";
                    txt_Email.Attributes.Add("placeholder", "כתובת אימייל זו כבר קיימת במערכת");
                }




            }
        }

        #region help methods

        protected string setDestinationLink(object docId)
        {
            DestinationPage oDestinationPage = DestinationPageContainer.SelectByID(ConvertToValue.ConvertToInt(docId), 1, null).Single;
            if (oDestinationPage != null && oDestinationPage.SeoSingle != null)
            {
                return LowCostSite.sass.SetLinkHandler.SetLink(EnumHandler.LinkPages.Destinations, EnumHandler.QueryStrings.URL, oDestinationPage.SeoSingle.FriendlyUrl_UI, oDestinationPage.DocId_UI);
            }
            else
            {
                return QueryStringManager.RedirectWithQuery(StaticStrings.path_DestinationPage, EnumHandler.QueryStrings.ID, docId.ToString());
            }
        }

        protected string setCompanyLink(object docId)
        {
            CompanyPage oCompanyPage = CompanyPageContainer.SelectByID(ConvertToValue.ConvertToInt(docId), 1, null).Single;
            if (oCompanyPage != null && oCompanyPage.SeoSingle != null)
            {
                return LowCostSite.sass.SetLinkHandler.SetLink(EnumHandler.LinkPages.Companies, EnumHandler.QueryStrings.URL, oCompanyPage.SeoSingle.FriendlyUrl_UI, oCompanyPage.DocId_UI);
            }
            else
            {
                return QueryStringManager.RedirectWithQuery(StaticStrings.path_CompanyPage, EnumHandler.QueryStrings.ID, docId.ToString());
            }


        }

        private void SetCompaniesRepeater()
        {
            drp_Companies.DataSource = Companies.DataSource;
            drp_Companies.DataBind();
        }

        private void SetDestinationRepeater()
        {
            drp_Destinations.DataSource = Destinations.SortByKeyContainer(DL_Generic.KeyValuesType.KeyHomePageDestinationPageOrder, false).DataSource;
            drp_Destinations.DataBind();
        }
        public void setPageByWhithLabel(WhiteLabel oWhiteLabelFromCotentPage)
        {
            if (WhiteLabelId != null)
            {
                if (oWhiteLabelFromCotentPage != null)
                {
                    WhiteLabelId = oWhiteLabelFromCotentPage.DocId_Value;
                    //setPageContent();
                }

            }
        }
        #endregion

    }
}