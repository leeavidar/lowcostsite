﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using LoggerManagerProject;
using LowCostSite;
using LowCostSite.FlightsBookingService;
using LowCostSite.sass;
//using SharedDataObjects.Request.ParamSearch;
//using LoggerManagerLogicProject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

namespace LowCostSite.sass
{
    public partial class GoodURL : BasePage_UI
    {
        #region Session Methods

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            //  Session["OrderObject"] = null;
        }
        internal override void LoadFromSession(bool isPostBack)
        {

        }
        internal override void SaveToSession()
        {
        }
        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
        }

        #endregion
    }

    public partial class GoodURL : BasePage_UI
    {
        OrderObject oOrderObject;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoggerManagerProject.LoggerManager.Logger.WriteInfo("Pelecard Payment Status Succees ");
                // If there is a pelecard status code
                if (!string.IsNullOrEmpty(PelecardStatusCode))
                {
                    // 
                    initPage();
                }
                else
                {
                    //missing pelecardResult
                    LoggerManagerProject.LoggerManager.Logger.WriteWarrning("Pelecard Payment Status Success - missing pelecardResult ");
                    strMailContentForSupport = string.Format("Issue has failed -  missing pelecardResult .");
                    strMailSubjectForSupport = string.Format("{0} {1}", GetText(MailStrings.strBookingFailed), OrderByToken.DocId_UI);
                    mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                    ErrorFlag = true;
                }
            }
            else
            {
                //this page already loaded
            }
        }

        #region Properties

        #region Pelecard return params
        public bool ErrorFlag = false;
        public string OrderToken
        {
            get
            {
                if (DebugMode && !DebugModeMoveToPelecard)
                {
                    return Request.QueryString["OrderToken"];
                }
                else if (DebugMode && DebugModeMoveToPelecard)
                {
                    return Request.Form["parmx"];
                }
                else
                {
                    return Request.Form["parmx"];
                }
            }
        }
        public string PelecardResult
        {
            get
            {
                if (DebugMode && !DebugModeMoveToPelecard)
                {
                    return Request.QueryString["result"] ?? "";
                }
                else
                {
                    return Request.Form["result"] ?? "";
                }
            }
        }
        public string PaymentToken { get { return Request.Form["token"]; } }
        public string PaymentTransation { get { return Request.Form["authNum"]; } }

        #endregion

        private Orders _orderByToken;
        public Orders OrderByToken
        {
            get
            {
                if (_orderByToken == null)
                {
                    Orders oOrder = OrdersContainer.SelectByKeysView_WhiteLabelDocId_OrderToken(1, OrderToken, true).Single;//GetWhiteLabelDocID()
                    if (oOrder != null)
                    {
                        OrderDocID = oOrder.DocId_Value;
                        _orderByToken = oOrder;
                    }
                    else
                    {
                        _orderByToken = null;
                    }
                }
                else
                {
                    //Already have object token
                }
                return _orderByToken;
            }
        }
        public string PelecardStatusCode { get { return PelecardResult.Substring(0, 3); } }
        public string WhiteLabelEmail { get { return WhiteLabelObj.Email_UI; } }
        public int OrderDocID { get; set; }
        int mailSendOptions = -1;
        string clientEmail = "";
        string strMailContentForSupport = "";
        string strMailSubjectForSupport = "";
        string strMailContentForClient = "";
        string strMailSubjectForClient = "";
        #endregion

        #region Page Init

        private void initPage()
        {
            try
            {
                LoggerManagerProject.LoggerManager.Logger.WriteInfo("Pelecard Payment Status Succees - Code : 000 . OrderToken:"+OrderToken);

                if (OrderByToken != null)
                {
                    // Get the order object with all the details for the issue
                    oOrderObject = (OrderObject)createOrderObjectFromFile(BasePage_UI.GetXMLDirByDateAndOrderId(OrderByToken.DateCreated_Value, OrderByToken.DocId_UI) + "\\XMLOrder_" + OrderByToken.DocId_UI + ".xml", typeof(OrderObject));

                    // If the order object contains all the data needed for issuing
                    if (oOrderObject != null && oOrderObject.IsReady)
                    {
                        LoggerManagerProject.LoggerManager.Logger.WriteInfo("OrderToken:{0}. Order Loaded Seccessfully From XML.",OrderToken);
                        clientEmail = GetClientMail(oOrderObject.OrderDocId);  //OrderByToken.Email_UI;
                        if (DebugMode || IsFakePalecardPayment)
                        {
                            LoggerManagerProject.LoggerManager.Logger.WriteInfo("OrderToken:{0}. DebugMode Start! not do the issue! only update", OrderToken);
                            #region DebugMode-FakePayment
                            if (SaveReservationToDBAndXml(new FlightIssuedDetails()
                         {
                             TicketingDetails = new TicketedItinerary()
                             {
                                 SupplierBookingReference = "TEST",
                                 PNR = "TEST",
                                 Errors = new ErrorContainer()
                                 {
                                     DataSource = { }
                                 }
                             }
                         }))
                            {
                                //Complete!
                                Session["ThanksPageToken"] = Guid.NewGuid().ToString();
                                AddScript("window.parent.location.href='" + StaticStrings.path_ThanksPage + "?OrderDocID=" + OrderDocID + "'");
                                //   Response.Redirect(StaticStrings.path_ThanksPage + "?OrderDocID="+OrderDocID, false);

                                LoggerManagerProject.LoggerManager.Logger.WriteWarrning("DEBUG MODE - Seccess - Order%Issu Complete , OrderID: {0}", OrderByToken.DocId_UI);
                                strMailSubjectForSupport = GetText(MailStrings.strSubjectBookingSucceeded) + OrderByToken.DocId_UI;
                                strMailContentForSupport = "DEBUG MODE - " + GetText(MailStrings.strBookingSucceeded) + OrderByToken.DocId_UI;
                                mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                            }


                            #endregion
                        }
                        else
                        {
                            LoggerManagerProject.LoggerManager.Logger.WriteInfo("OrderToken:{0}. Before Start Issue Order.", OrderToken);
                            FlightIssuedDetails oFlightIssuedDetails = IssueTicket(); // issue the ticket
                            if (SaveReservationToDBAndXml(oFlightIssuedDetails))
                            {
                                LoggerManagerProject.LoggerManager.Logger.WriteInfo("OrderToken:{0}. Order Has update in DB.", OrderToken);
                                //Complete!
                                Session["ThanksPageToken"] = Guid.NewGuid().ToString();
                                AddScript("window.parent.location.href='" + StaticStrings.path_ThanksPage + "?OrderDocID=" + OrderDocID + "'");
                                //   Response.Redirect(StaticStrings.path_ThanksPage + "?OrderDocID="+OrderDocID, false);

                                LoggerManagerProject.LoggerManager.Logger.WriteWarrning("Seccess - Order%Issu Complete , OrderID: {0}", OrderByToken.DocId_UI);
                                strMailSubjectForSupport = GetText(MailStrings.strSubjectBookingSucceeded) + OrderByToken.DocId_UI;
                                strMailContentForSupport = GetText(MailStrings.strBookingSucceeded) + OrderByToken.DocId_UI;
                                mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                            }
                        }




                    }
                    else
                    {
                        // Xml of Order Object not loaded
                        #region Error - Order Object From XML is null or Order Object From XML Rooms Container is null
                        if (oOrderObject == null)
                        {
                            LoggerManagerProject.LoggerManager.Logger.WriteError("Issus Failed. Order Object From XML is null.");
                            strMailContentForSupport = string.Format("Order Object From XML is null.");
                            strMailSubjectForSupport = string.Format("{0} {1}", GetText(MailStrings.strBookingFailed), OrderByToken.DocId_UI);
                            mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                        }
                        else if (oOrderObject.IsReady == false)
                        {
                            // -- if the orderobject not contain all parameters -- 
                            LoggerManagerProject.LoggerManager.Logger.WriteError("Issus Failed. Order Object From XML. missing parameters");
                            strMailContentForSupport = string.Format("Order Object From XML Rooms Container is null.");
                            strMailSubjectForSupport = string.Format("{0} {1}", GetText(MailStrings.strBookingFailed), OrderByToken.DocId_UI);
                            mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                        }
                        #endregion
                        ErrorFlag = true;
                        UpdateErrorStatusInDB();
                    }
                }
                else
                {
                    #region error - Cannot find order from db to match with site payment token
                    LoggerManagerProject.LoggerManager.Logger.WriteError(string.Format("Order empty - Cannot find order from db to match with site payment token : {0}", OrderToken));
                    strMailContentForSupport = string.Format("Order empty - Cannot find order from db to match with site payment token : {0}", OrderToken);
                    strMailSubjectForSupport = GetText(MailStrings.strBookingFailed);
                    mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                    ErrorFlag = true;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Error - Exeption in payment page(goodUrl)
                LoggerManagerProject.LoggerManager.Logger.WriteError("Error - Exeption:GoodUrl - ", ex);
                strMailSubjectForSupport = string.Format("Exeption in payment page(goodUrl)");
                strMailContentForSupport = string.Format("{0} {1} Fatal Error Exception - GooURL Faild {1} {2} {1} {3}", strMailContentForSupport, Environment.NewLine, ex.Message, "OrderId = " + OrderByToken.DocId_UI);
                mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                ErrorFlag = true;
                #endregion
            }
            finally
            {
                SendOrderMail(strMailContentForClient, strMailSubjectForClient, clientEmail, strMailContentForSupport, strMailSubjectForSupport, mailSendOptions);
                if (ErrorFlag)
                {

                    Response.Redirect(StaticStrings.path_ErrorAndCallBack, false);
                }
            }
        }

        #endregion

        #region Page Functions

        /// <summary>
        /// Create (Full) Order Object From Xml
        /// </summary>        
        //private void 
        private void UpdateErrorStatusInDB()
        {
            OrderByToken.OrderStatus = (int)EnumHandler.OrderStatusType.BookingFailed;
            OrderByToken.StatusPayment = (int)EnumHandler.OrderPaymentStatus.Paid;
            OrderByToken.Action(DB_Actions.Update);
        }
        private object createOrderObjectFromFile(string root, Type type)
        {
            try
            {
                OrderObject obj = BinaryHandler.DeserializeBinary<OrderObject>(root);
                return obj;
                //LoggerManagerProject.LoggerManager.Logger.WriteInfo("Good URL - createOrderObjectFromXml: file name[{0}] ", root);

                //XmlSerializer xmlFormat = new XmlSerializer(type);

                //using (Stream fStream = File.OpenRead(root))
                //{
                //    var fromDisk = xmlFormat.Deserialize(fStream);
                //    return fromDisk;
                //}

            }
            catch (Exception ex)
            {
                LoggerManagerProject.LoggerManager.Logger.WriteError("ERROR:Good URL - createOrderObjectFromXml: ", ex);
                //throw ex;
            }
            return null;
        }
        private string GetClientMail(int OrderDocId)
        {
            ContactDetails oContactDetails = ContactDetailsContainer.SelectByKeysView_WhiteLabelDocId_OrdersDocId(GetWhiteLabelDocID(), OrderDocId, true).Single;
            return oContactDetails.Email_UI;
        }

        /// <summary>
        /// The method that issues the ticket.
        /// </summary>
        /// <returns></returns>
        private FlightIssuedDetails IssueTicket()
        {
            try
            {
                using (var service = new LowCostSite.FlightsBookingService.FlightsBookingService())
                {
                    TicketingParameters ticketing = new TicketingParameters()
                    {
                        #region Props
                        ExpectedTotalPrice = oOrderObject.TicketingParameters.ExpectedTotalPrice,
                        ExpectedTotalPriceCurrency = oOrderObject.TicketingParameters.ExpectedTotalPriceCurrency,
                        FlightsID = oOrderObject.TicketingParameters.FlightsID,
                        TfReference = oOrderObject.TicketingParameters.TfReference,
                        GeneralParameters = oOrderObject.TicketingParameters.GeneralParameters,
                        PNR = oOrderObject.TicketingParameters.PNR
                        #endregion
                    };
                    // Issue the order, and get back the details
                    FlightIssuedDetails oFlightIssuedDetails = service.IssueFlightReservation(oOrderObject.SelectedItinerary.LoginToken, ticketing, oOrderObject.SelectedItinerary.SearchToken);
                    return oFlightIssuedDetails;
                }
            }
            catch (Exception ex)
            { //The issus is failed!!
                LoggerManagerProject.LoggerManager.Logger.WriteError(string.Format("Issus Failed - Exception when trying to Issu  : {0} OrderTokn: {1} ", ex, OrderToken));
                ErrorFlag = true;
                return null;
            }
        }
        private bool SaveReservationToDBAndXml(FlightIssuedDetails oFlightIssuedDetails)
        {
            //first we need to charge the client with pelecard and then issue the ticket
            //we need to set a counter of maximun 9 mins (between the BookFlight and the IssueFlight services we have a maximum session of 9 mins)
            if (oFlightIssuedDetails != null)
            {
                // Save the response to the issue in the order object
                oOrderObject.FlightIssuedDetails = oFlightIssuedDetails;
                // IF there are no errors in the response from the issues method
                if (oFlightIssuedDetails.TicketingDetails.Errors.DataSource.Length == 0 || oFlightIssuedDetails.TicketingDetails.Errors.DataSource[0].Code == null)
                {
                    //save on session in order to get the PNR on the thanks page
                    Generic.SessionManager.SetSession<FlightIssuedDetails>(oFlightIssuedDetails);

                    int orderId = OrderDocID;
                    WriteLogOnDB(orderId, oFlightIssuedDetails, EnumHandler.OrderStage.IssueFlight, EnumHandler.OrderXmlType.Response, EnumHandler.OrderXmlStatus.None);
                    Orders thisOrder = OrdersContainer.SelectByKeysView_WhiteLabelDocId_OrderToken(BasePage_UI.GetWhiteLabelDocID(), oOrderObject.OrderToken, true).First;
                    thisOrder.StatusPayment = (int)EnumHandler.OrderPaymentStatus.Paid;
                    thisOrder.OrderStatus = (int)EnumHandler.OrderStatus.Succeeded;
                    thisOrder.PaymentToken = PaymentToken;
                    thisOrder.PaymentTransaction = PaymentTransation;
                    thisOrder.OrderToken = null;
                    thisOrder.DateOfBook = DateTime.Now;
                    thisOrder.LowCostPNR = oFlightIssuedDetails.TicketingDetails.SupplierBookingReference;
                    thisOrder.SupplierPNR = oFlightIssuedDetails.TicketingDetails.PNR;
                    thisOrder.Action(DB_Actions.Update);
                    return true;

                }
                else
                { //Critical Error!!!!! the costumer has payes and the order not completed! issus failed.
                    LoggerManagerProject.LoggerManager.Logger.WriteError("Error - the issu object has returned from the service contain Errors!! , OrderID: {0}", OrderByToken.DocId_UI);
                    LoggerManagerProject.LoggerManager.Logger.WriteError("Errors: {0}", oFlightIssuedDetails.TicketingDetails.Errors.DataSource.Length.ToString());
                    string Info = "";
                    foreach (var item in oFlightIssuedDetails.TicketingDetails.Errors.DataSource)
                    {
                        LoggerManagerProject.LoggerManager.Logger.WriteError("Error Code: {0} Info: {1}", item.Code, item.Message);
                        Info = Environment.NewLine + string.Format("Error Code: {0} Info: {1}", item.Code, item.Message);
                    }
                    strMailSubjectForSupport = string.Format("Exeption in payment page(goodUrl) - Essu Failed");
                    strMailContentForSupport = string.Format("{0} {1} Fatal Error Exception - Issu failded. {1} {2} {1} {3}", strMailContentForSupport, Environment.NewLine, Info, "OrderId = " + OrderByToken.DocId_UI);
                    mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                    ErrorFlag = true;
                    UpdateErrorStatusInDB();
                    return false;
                }
            }
            else
            {
                LoggerManagerProject.LoggerManager.Logger.WriteWarrning("Error - the issu object has returned from the service is null! , OrderID: {0}", OrderByToken.DocId_UI);
                strMailSubjectForSupport = string.Format("Exeption in payment page(goodUrl)");
                strMailContentForSupport = string.Format("{0} {1} Fatal Error Exception - GooURL Faild {1} {2} {1} {3}", strMailContentForSupport, Environment.NewLine, "the issu object has returned from the service is null!", "OrderId = " + OrderByToken.DocId_UI);
                mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                ErrorFlag = true;
                UpdateErrorStatusInDB();
                return false;
            }
        }
        private void WriteLogOnDB(int orderId, object requestOrResponse, EnumHandler.OrderStage orderStage, EnumHandler.OrderXmlType type, EnumHandler.OrderXmlStatus status)
        {
            OrderLog oOrderLog = new OrderLog();
            oOrderLog.OrdersDocId = orderId;
            oOrderLog.XML = requestOrResponse.SerializeToString();
            oOrderLog.Type = (int)type;
            oOrderLog.Status = (int)status;
            oOrderLog.OrderStage = (int)orderStage;
            oOrderLog.Action(DB_Actions.Insert);
        }
        #endregion
    }
}