﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="OrderPage.aspx.cs" Inherits="LowCostSite.sass.OrderPage" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script src="javascripts/PageScripts/OrderPage.js"></script>
    <link href="../assets/ExtraStyling.css" rel="stylesheet" />
    <script>
        function TermsCheacked() {
            if ($("#<%=cb_TermsOfServiceApproval.ClientID%>").is(':checked')) {
                return true;
            } else { return false; }

        }
        function OpenTerms() {
            var myWindow = window.open("Policies_Small.aspx", "", "width=800, height=600");
        }
        function OpenConditions() {
            $("#div_Conditions").slideToggle("fast");
        }
        
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--  <div class="modal fade" id="TicketConditionModal" tabindex="-1" role="dialog" data-width="800" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><%=GetText("TicketConditions")%></h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default"><%=GetText("close")%></button>
            </div>
        </div>
    </div>--%>
    <asp:HiddenField runat="server" ID="hf_IdentityCode" />
    <!-- Hidden for hash code -->
    <div class="container">
        <div class="content-page">
            <div class="line-title"></div>
            <div class="row">
                <!-- START: RIGHT SIDE COLUMN -->
                <div class="col-sm-3">
                    <div class="title-open full-width box-padding">
                        <div class="row-space ">
                            <!-- סיכום פרטי הטיסה -->
                            <%=GetText("FlightDetails")%>
                        </div>
                    </div>
                    <div class="highlight rtl display_inline_block full-width no-margin-bottom ">
                        <div class="row-space text-right font-bold font-large">
                            <%=GetText("Flight")%>  >  <span runat="server" id="lbl_Origin"></span>> <span runat="server" id="lbl_Destination"></span>
                        </div>
                        <div class="row-space ">
                            <span class="image-space font-bold ">
                                <img src="images/out_blue.png" /></span><span runat="server" id="lbl_OutwardDate"></span>
                        </div>
                        <div runat="server" id="FlightDetails_ReturnDate" class="row-space">
                            <span class="image-space font-bold ">
                                <img src="images/in_blue.png" /></span>
                            <span runat="server" id="lbl_ReturnDate"></span>
                        </div>
                        <br />
                        <div class="highlight rtl display_inline_block full-width no-margin-bottom seperate-border-top">
                            <div class="row display-none" runat="server" id="div_AdultPrice">
                                <div class="col-sm-3">
                                    <%=GetText("Adult")%>
                                </div>
                                <div class="col-sm-1">
                                    X
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="lbl_PriceBox_AdultCount" runat="server" />
                                </div>
                                <div class="col-sm-5 price-color text-left ltr price-right-box">
                                    <asp:Label ID="lbl_PriceBox_AdultPrice" runat="server" />
                                </div>
                            </div>
                            <div class="row display-none" runat="server" id="div_ChildPrice">
                                <div class="col-sm-3">
                                    <%=GetText("Child")%>
                                </div>
                                <div class="col-sm-1">
                                    X
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="lbl_PriceBox_ChildCount" runat="server" />
                                </div>
                                <div class="col-sm-5 price-color text-left ltr price-right-box">
                                    <asp:Label ID="lbl_PriceBox_ChildPrice" runat="server" />
                                </div>
                            </div>
                            <div class="row display-none" runat="server" id="div_InfantPrice">
                                <div class="col-sm-3">
                                    <%=GetText("Infant")%>
                                </div>
                                <div class="col-sm-1">
                                    X
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="lbl_PriceBox_InfantCount" runat="server" />
                                </div>
                                <div class="col-sm-5 price-color text-left ltr price-right-box">
                                    <asp:Label ID="lbl_PriceBox_InfantPrice" runat="server" />
                                </div>
                            </div>
                            <br />


                            <div class="row display-none rtl" runat="server" id="div_HandlingFee">
                                <div class="col-sm-3">
                                    <%=GetText("HandelingFee")%>:
                                </div>
                                <div class="col-sm-1">
                                    X
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="lbl_PassengerNumber" runat="server" />
                                </div>
                                <div class="col-sm-5 price-color text-left ltr price-right-box">
                                    <asp:Label ID="lbl_HandlingFee" runat="server" />
                                </div>
                            </div>




                            <div class="row display-none" runat="server" id="div_Seats">
                                <div class="col-sm-6">
                                    <%=GetText("Seating")%>:
                                </div>
                                <div class="col-sm-4 text-left price-color ltr">
                                    <asp:Label ID="lbl_SeatsTotalPrice" runat="server" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="filter-title-default text-space text-right rtl">
                        <%=GetText("TotalForPayment")%>:
                    <div class="title_content text-left price-color text-space ltr display-inline">
                        <asp:Label ID="lbl_TotalPrice" runat="server" />
                    </div>
                    </div>
                    <div class="title-open full-width box-padding" onclick="ToggleArea('outwardFlightDiv')">
                        <div class="row-space ">
                            <%=GetText("OutwardFlight")%>
                            <span class="arrow-down-filter">
                                <img src="images/arrow-down-white.PNG">
                            </span>
                        </div>
                    </div>
                    <!-- START: Repeater for the OUTWARD flight segments summary -->
                    <div class="highlight rtl display_inline_block full-width no-margin-bottom " data-toggle="outwardFlightDiv">
                        <asp:Repeater ID="rpd_OutwardSegments" runat="server" ItemType="LowCostSite.FlightsSearchService.FlightSegment">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-sm-12 rtl">
                                        <h4 class="h4-flight-title outwardSegment"><%=GetText("FlgihtNo") %> <%# Container.ItemIndex + 1 %></h4>
                                        <!-- יציאה: -->
                                        <b><%=GetText("Departure")%>:</b>
                                        <br />
                                        <%# GenerateSegmentString(Item,false) %>
                                        <br />
                                        <b><%=GetText("Arrival")%>:</b><br />
                                        <%# GenerateSegmentString(Item,true) %>
                                        <br />
                                        <b><%=GetText("Airline")%>:</b>
                                        <%# Item.OperatingAirline.Name %>
                                        <br />
                                        <b><%=GetText("FlightNumber")%>: </b><%# Item.FlightCode %><br />
                                    </div>
                                </div>
                                <hr />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <!-- END: Repeater for the OUTWARD flight segments summary -->
                    <div runat="server" id="div_outwardFlightsDetails" class="display-none">
                        <div class="title-close full-width box-padding" onclick="ToggleArea('returnFlightDiv')">
                            <div class="row-space ">
                                <%=GetText("ReturnFlight")%>
                                <span class="arrow-down-filter">
                                    <img src="images/arrow-down-white.PNG">
                                </span>
                            </div>
                        </div>
                        <!-- START: Repeater for the RETURN flight segments summary -->
                        <div class="highlight rtl display_inline_block full-width no-margin-bottom " data-toggle="returnFlightDiv">
                            <asp:Repeater ID="rpd_ReturnSegments" runat="server" ItemType="LowCostSite.FlightsSearchService.FlightSegment">
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="h4-flight-title returnSegment "><%=GetText("FlightNo")%> <%# Container.ItemIndex + 1 %></h4>
                                            <b><%=GetText("Departure")%>:</b><br />
                                            <%# GenerateSegmentString(Item,false) %>
                                            <br />
                                            <b><%=GetText("Arrival")%>:</b><br />
                                            <%# GenerateSegmentString(Item,true) %>
                                            <br />
                                            <b><%=GetText("Airline")%>:</b>
                                            <%# Item.OperatingAirline.Name %>
                                            <br />
                                            <b><%=GetText("FlightNumber")%>: </b><%# Item.FlightCode %><br />
                                        </div>
                                    </div>
                                    <hr />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <!-- END: Repeater for the RETURN flight segments summary -->
                    </div>
                </div>
                <!-- END: RIGHT SIDE COLUMN -->

                <!-- START: LEFT MAIN COLUMN -->
                <div class="col-sm-9">
                    <!-- START: STATUS MESSAGES -->
                    <!-- START: MESSAGE: CHANGE IN PRICE -->
                    <asp:Panel ID="panel_ErrorPrice" Visible="false" runat="server">
                        <div class="highlight rtl display_inline_block full-width">
                            <div class="box-wrap-content text-center ">
                                <div class="tips-box">
                                    <h1 class="title_content"><%=GetText("ThereHasBeenAChangeInTheOrderPrice")%></h1>
                                    <div>
                                        <h3><%=GetText("TheNewTotalPriceIs")%>: <span runat="server" id="lbl_newPrice"></span>
                                        </h3>
                                    </div>

                                    <asp:Button runat="server" ID="btn_ContinueOrder" CssClass="btn btn-primary " Text="gtxt_ContinueToOrder" OnClick="btn_ContinueOrder_Click" />
                                    <asp:Button runat="server" ID="btn_Cancel" Text="gtxt_Cancel" CssClass="btn" OnClick="btn_Cancel_Click" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <!-- END: MESSAGE: CHANGE IN PRICE -->

                    <!-- START: MESSAGE: CHANGE IN CURRENCY -->
                    <asp:Panel ID="panel_ErrorCurrency" Visible="false" runat="server">
                        <div class="highlight rtl display_inline_block full-width">
                            <div class="box-wrap-content text-center ">
                                <div class="tips-box">
                                    <h1 class="title_content"><%=GetText("ErrorInBooking")%></h1>
                                    <div>
                                        <h3><%=GetText("CantBookThisOrderSorryForTheInconvenience")%>
                                        </h3>
                                    </div>
                                    <asp:Button runat="server" ID="btn_Cancel2" Text="gtxt_Cancel" CssClass="btn" OnClick="btn_Cancel_Click" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <!-- END: MESSAGE: CHANGE IN CURRENCY -->


                    <!-- START: MESSAGE: DOUBLE BOOKING -->
                    <div runat="server" id="div_DuplicateBooking" class="error-message display-none">
                        <div class="box-wrap-content text-center ">
                            <div class="tips-box">
                                <h1 class="title_content"><%=GetText("DoubleBooking")%></h1>
                                <h3><%=GetText("AirlineHasAnOrderForThesePassengers")%>
                                </h3>
                                <input type="button" onclick="BackToPassengerDetails()" class="btn" value="<%=GetText("back")%>" />

                            </div>
                        </div>
                    </div>
                    <!-- END: MESSAGE: DOUBLE BOOKING -->

                    <!-- START: MESSAGE: Error In Age for child -->
                    <div runat="server" id="div_ChildAgeError" class="error-message display-none">
                        <div class="box-wrap-content text-center ">
                            <div class="tips-box">
                                <h1 class="title_content"><%=GetText("AgeGroupErrorTitle")%></h1>
                                <h3><%=GetText("AgeGroupErrorChildMassage")%>
                                </h3>
                                <input type="button" onclick="BackToPassengerDetails()" class="btn" value="<%=GetText("back")%>" />
                            </div>
                        </div>
                    </div>
                    <!-- END: MESSAGE: Error In Age for child -->

                    <!-- START: MESSAGE: Error In Age for infant -->
                    <div runat="server" id="div_InfantAgeError" class="error-message display-none">
                        <div class="box-wrap-content text-center ">
                            <div class="tips-box">
                                <h1 class="title_content"><%=GetText("AgeGroupErrorTitle")%></h1>
                                <h3><%=GetText("AgeGroupErrorInfantMassage")%>
                                </h3>
                                <input type="button" onclick="BackToPassengerDetails()" class="btn" value="<%=GetText("back")%>" />
                            </div>
                        </div>
                    </div>
                    <!-- END: MESSAGE: Error In Age for infant -->

                    <!-- START: MESSAGE: ORDER IN PROCESS -->
                    <div runat="server" id="div_generalError" class="error-message display-none">
                        <div class="box-wrap-content text-center ">
                            <div class="tips-box">
                                <h1 class="title_content"><%=GetText("ErrorInBooking")%></h1>
                                <h3><%=GetText("TheOrderIsInProcessAgentWillContactYou")%> <span runat="server" id="Span1"></span>
                                </h3>
                                <asp:Button runat="server" ID="btn_BackToHome" Text="gtxt_Cancel" CssClass="btn" PostBackUrl="~/sass/Index.aspx" />
                            </div>
                        </div>
                    </div>
                    <!-- END: MESSAGE: ORDER IN PROCESS -->

                    <!-- START: MESSAGE: Errors in Selected itinerary -->
                    <div runat="server" id="div_ItineraryError" class="error-message display-none">
                        <div class="box-wrap-content text-center ">
                            <div class="tips-box">
                                <h1 class="title_content"><%=GetText("Error")%></h1>
                                <h3><%=GetText("ThereHasBeenAnErrorPleaseChooseAnotherFlight")%>: <span runat="server" id="Span2"></span>
                                </h3>
                                <asp:Button runat="server" ID="btn_ItineraryError_Back" Text="gtxt_Cancel" CssClass="btn" PostBackUrl="~/sass/FlightsResults.aspx" />
                            </div>
                        </div>
                    </div>
                    <!-- END: MESSAGE: ORDER IN PROCESS -->
                    <!-- END: STATUS MESSAGES -->

                    <div id="div_generalDiv" class="passengers-details" runat="server">
                        <!-- START: DETAILS -->
                        <div class="highlight rtl display_inline_block full-width no-margin-bottom">
                            <div class="box-wrap-content no-padding-bottom ">
                                <div class="box-wrap-title"><%=GetText("PassengersDetails")%>:</div>
                                <!-- START: PLACE HOLDER FOR EACH PASSENGERS DETAILS-->
                                <asp:PlaceHolder ID="ph_PassengerDetails" runat="server"></asp:PlaceHolder>
                                <!-- END: PLACE HOLDER FOR EACH PASSENGERS DETAILS-->
                                <!-- START: CONTACT DETAILS -->
                                <div class="row no-margin-side">
                                    <h3 class="padding-side-title"><%=GetText("ContactDetails")%></h3>
                                </div>
                                <div class="col-sm-12">
                                    <%--<asp:CheckBox ID="cb_isContactFirstPax" CssClass="cb_isContactFirstPax" runat="server" Text="זהו הנוסע הראשון" />--%>
                                </div>
                                <!-- START: ROW FOR: TITLE, FIRST NAME, LAST NAME-->
                                <div class="row no-margin-side">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label"><%=GetText("PersonTitle")%>:</label>
                                            <asp:DropDownList ID="ddl_ContactTitle" runat="server" class="form-control">
                                                <asp:ListItem Value="Mr"></asp:ListItem>
                                                <asp:ListItem Value="Mrs"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <span style="color: red">*</span><label class="control-label"><%=GetText("FirstName")%>:</label>
                                            <asp:TextBox ID="txt_ContactFirstName" class="form-control txt_ContactFirstName" runat="server" />
                                            <asp:RequiredFieldValidator ID="Val_FirstNameRequired" CssClass="rfv_ContactFirstName" runat="server" ValidationGroup="orderPageValidation" ControlToValidate="txt_ContactFirstName" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="#c63a43" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label"><%=GetText("LastName")%>:</label>
                                            <asp:TextBox ID="txt_ContactLastName" class="form-control txt_ContactLastName" runat="server" placeholder='gtxt_LastName' />
                                            <asp:RequiredFieldValidator ID="Val_LastNameRequired" CssClass="rfv_ContactLastName" runat="server" ValidationGroup="orderPageValidation" ControlToValidate="txt_ContactLastName" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="#c63a43" />
                                        </div>
                                    </div>
                                </div>
                                <!-- END: ROW FOR: TITLE, FIRST NAME, LAST NAME-->
                                <!-- START: ROW FOR: STREET, HOUSENUMBER, CITY-->
                                <%-- <div class="row no-margin-side">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("Street")%>:</label>
                                        <asp:TextBox ID="txt_Street" class="form-control" runat="server"/>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="contactsDetails" ControlToValidate="txt_Street" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="Red"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("HouseNo")%>:</label>
                                        <asp:TextBox class="form-control" ID="txt_HouseNumber" runat="server"/>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="contactsDetails" ControlToValidate="txt_HouseNumber" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="Red"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("City")%>:</label>
                                        <asp:TextBox ID="txt_City" class="form-control" runat="server" placeholder='gtxt_City' />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="contactsDetails" ControlToValidate="txt_City" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="Red"/>
                                    </div>
                                </div>
                            </div>--%>
                                <!-- END: ROW FOR: STREET, HOUSENUMBER, CITY-->
                                <!-- START: ROW FOR: ZIPCODE, COUNTRY, EMAIL-->
                                <div class="row no-margin-side">
                                    <%--  <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("ZIPCode")%>:</label>
                                        <asp:TextBox ID="txt_PostalCode" class="form-control" runat="server" placeholder='gtxt_ZIPExample' />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="contactsDetails" ControlToValidate="txt_PostalCode" Display="Dynamic" ErrorMessage='gtxt_requiredField' ForeColor="Red"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><%=GetText("Country")%>:</label>
                                        <asp:TextBox ID="txt_Country" class="form-control" runat="server" Text="IL" />
                                    </div>
                                </div>
                                    --%>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label"><%=GetText("EmailAddress")%>:</label>
                                            <asp:TextBox ID="txt_Email" class="form-control" runat="server" placeholder="example@lowcost.co.il" />
                                            <asp:RequiredFieldValidator ID="Val_EmailTextRequired" runat="server" ErrorMessage='gtxt_requiredField' ForeColor="#c63a43" ControlToValidate="txt_Email" ValidationGroup="orderPageValidation" Display="Dynamic" />
                                            <asp:RegularExpressionValidator ID="Val_EmailRegularExpressionValidator" runat="server" ErrorMessage='gtxt_TheEmailAddressIsNotValid' ForeColor="#c63a43" ControlToValidate="txt_Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="orderPageValidation" Display="Dynamic" />
                                        </div>
                                    </div>
                                </div>
                                <!-- END: ROW FOR: ZIPCODE, COUNTRY, EMAIL-->
                                <!-- START: ROW FOR: PHONE NUMBER, SECONDNUMBER (AND PLACE FOR ANOTHER COL)-->
                                <div class="row no-margin-side">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label"><%=GetText("PhoneNumber")%>:</label><br />
                                            <asp:TextBox ID="txt_MainPhone" class="form-control phone-prefix" runat="server" />
                                            <asp:DropDownList ID="ddl_PhonePrefix" runat="server" CssClass="b-day"></asp:DropDownList>
                                            <asp:CustomValidator ID="Val_PrimaryPhone" runat="server" OnServerValidate="Val_PrimaryPhone_ServerValidate" ForeColor="#c63a43" ControlToValidate="txt_MainPhone" ValidateEmptyText="true" ValidationGroup="orderPageValidation" ErrorMessage="gtxt_requiredField"></asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label"><%=GetText("SecondPhoneNumber")%>:</label><br />
                                            <asp:TextBox ID="txt_PhoneSecond" class="form-control phone-prefix" runat="server" />
                                            <asp:DropDownList ID="ddl_PhonePrefixSecond" CssClass="b-day" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <!-- END: ROW FOR: PHONE NUMBER, SECONDNUMBER -->


                                <div class="row no-margin-side">
                                    <div class="col-md-7">
                                        <div class="row no-margin-side">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <asp:CheckBox ID="cb_isInterestedInMails" runat="server" Checked="true" Text='gtxt_AgreeToReceiveCommercialContentAndInfoAboutFlightChangesByEmail' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-margin-side">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <asp:CheckBox ID="cb_isInterestedInSMS" runat="server" Checked="true" Text='gtxt_AgreeToReceiveCommercialContentAndInfoAboutFlightChangesBySms' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-margin-side">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <asp:CheckBox ID="cb_TermsOfServiceApproval" runat="server" Checked="false" Text='gtxt_IagreeTo' />
<%--                                                <span id="lbl_Conditions" class="conditions-link" style="text-decoration: underline;" onclick="ShowTicketConditiontModal()"><%=GetText("TicketConditions") %></span>--%>
                                                    <span id="lbl_Conditions" class="conditions-link" style="text-decoration: underline;" onclick="OpenConditions()"><%=GetText("TicketConditions") %></span>
                                                    <span id="lbl_Terms" class="conditions-link" style="text-decoration: underline;" onclick="OpenTerms()"><%=GetText("AndAgreeToTerms") %></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="msg_termsNotChacked" class="row no-margin-side" style="display: none">
                                            <div class="col-sm-12">
                                                <span style="color: #be1212; font-size: medium; font-weight: bold"><%=GetText("TermsOfServiceNotChackedMessage") %></span>
                                            </div>
                                        </div>
                                        <!-- END: 2 ROWS FOR CHECKBOXES -->
                                    </div>
                                    <!-- START: 2 ROWS FOR CHECKBOXES -->
                                    <div class="col-md-5 pull-left">
                                        <!-- START: TOTAL PRICE ROW -->
                                        <div class="row text-center">
                                            <div class="col-sm-12 ltr">
                                                <div class="row total-price-row">
                                                    <asp:Label runat="server" ID="lbl_FooterTotalPrice" CssClass="title_content text-center h3"></asp:Label>
                                                    <span class="h3">:<%=GetText("TotalPriceToCharge")%></span>
                                                </div>
                                                <div class="row">
                                                    <!-- START: BUTTON TO CONTINUE TO BOOKING PAYMENT -->
                                                    <asp:Button ID="btn_Book" runat="server" Text='gtxt_Continue_ar' CssClass="btn_Book btn btn-pay left" OnClick="btn_Book_Click" ValidationGroup="orderPageValidation" />
                                                    <!-- END: BUTTON TO CONTINUE TO BOOKING PAYMENT -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END: TOTAL PRICE ROW -->
                                    </div>
                                    <div id="div_Conditions" style="display:none" class="row no-margin-side">
                                        <div class="col-sm-12 text-left">
                                            <asp:PlaceHolder ID="ph_Conditions" runat="server"></asp:PlaceHolder>
                                        </div>
                                    </div>
                                </div>

                                <!-- END: CONTACT DETAILS -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: LEFT MAIN COLUMN -->
            </div>
        </div>
    </div>
</asp:Content>
