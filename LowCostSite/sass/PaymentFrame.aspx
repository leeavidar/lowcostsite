﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="PaymentFrame.aspx.cs" Inherits="LowCostSite.sass.PaymentFrame" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="javascripts/PageScripts/OrderPage.js"></script>
    <link href="../assets/ExtraStyling.css" rel="stylesheet" />
    <link href="bootstrap/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="bootstrap/bootstrap-modal.css" rel="stylesheet" />
    <script src="bootstrap/bootstrap-modal.js"></script>
    <script src="bootstrap/bootstrap-modalmanager.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="modal fade" id="TicketConditionModal" tabindex="-1" role="dialog" data-width="800" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><%=GetText("TicketConditions")%></h4>
            </div>
            <div class="modal-body">
                <div id="div_Conditions" class="row no-margin-side">
                    <div class="col-sm-12 text-left">
                        <asp:PlaceHolder ID="ph_Conditions" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default"><%=GetText("close")%></button>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hf_IdentityCode" />
    <!-- Hidden for hash code -->
    <div class="container">
        <div class="content-page">
            <div class="line-title"></div>
            <div class="row">

                <!-- START: RIGHT SIDE COLUMN -->
                <div class="col-sm-3">
                    <div class="title-open full-width box-padding">
                        <div class="row-space ">
                            <!-- סיכום פרטי הטיסה -->
                            <%=GetText("FlightDetails")%>
                        </div>
                    </div>
                    <div class="highlight rtl display_inline_block full-width no-margin-bottom ">
                        <div class="row-space text-right font-bold font-large">
                            <%=GetText("Flight")%>  >  <span runat="server" id="lbl_Origin"></span>> <span runat="server" id="lbl_Destination"></span>
                        </div>
                        <div class="row-space ">
                            <span class="image-space font-bold ">
                                <img src="images/out_blue.png" /></span><span runat="server" id="lbl_OutwardDate"></span>
                        </div>
                        <div runat="server" id="FlightDetails_ReturnDate" class="row-space">
                            <span class="image-space font-bold ">
                                <img src="images/in_blue.png" /></span>
                            <span runat="server" id="lbl_ReturnDate"></span>
                        </div>
                        <br />
                        <div class="highlight rtl display_inline_block full-width no-margin-bottom seperate-border-top">
                            <div class="row display-none" runat="server" id="div_AdultPrice">
                                <div class="col-sm-3">
                                    <%=GetText("Adult")%>
                                </div>
                                <div class="col-sm-1">
                                    X
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="lbl_PriceBox_AdultCount" runat="server" />
                                </div>
                                <div class="col-sm-5 price-color text-left ltr price-right-box">
                                    <asp:Label ID="lbl_PriceBox_AdultPrice" runat="server" />
                                </div>
                            </div>
                            <div class="row display-none" runat="server" id="div_ChildPrice">
                                <div class="col-sm-3">
                                    <%=GetText("Child")%>
                                </div>
                                <div class="col-sm-1">
                                    X
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="lbl_PriceBox_ChildCount" runat="server" />
                                </div>
                                <div class="col-sm-5 price-color text-left ltr price-right-box">
                                    <asp:Label ID="lbl_PriceBox_ChildPrice" runat="server" />
                                </div>
                            </div>
                            <div class="row display-none" runat="server" id="div_InfantPrice">
                                <div class="col-sm-3">
                                    <%=GetText("Infant")%>
                                </div>
                                <div class="col-sm-1">
                                    X
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="lbl_PriceBox_InfantCount" runat="server" />
                                </div>
                                <div class="col-sm-5 price-color text-left ltr price-right-box">
                                    <asp:Label ID="lbl_PriceBox_InfantPrice" runat="server" />
                                </div>
                            </div>
                            <br />


                            <div class="row display-none rtl" runat="server" id="div_HandlingFee">
                                <div class="col-sm-3">
                                    <%=GetText("HandelingFee")%>:
                                </div>
                                <div class="col-sm-1">
                                    X
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="lbl_PassengerNumber" runat="server" />
                                </div>
                                <div class="col-sm-5 price-color text-left ltr price-right-box">
                                    <asp:Label ID="lbl_HandlingFee" runat="server" />
                                </div>
                            </div>




                            <div class="row display-none" runat="server" id="div_Seats">
                                <div class="col-sm-6">
                                    <%=GetText("Seating")%>:
                                </div>
                                <div class="col-sm-4 text-left price-color ltr">
                                    <asp:Label ID="lbl_SeatsTotalPrice" runat="server" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="filter-title-default text-space text-right rtl">
                        <%=GetText("TotalForPayment")%>:
                    <div class="title_content text-left price-color text-space ltr display-inline">
                        <asp:Label ID="lbl_TotalPrice" runat="server" />
                    </div>
                    </div>
                    <div class="title-open full-width box-padding" onclick="ToggleArea('outwardFlightDiv')">
                        <div class="row-space ">
                            <%=GetText("OutwardFlight")%>
                            <span class="arrow-down-filter">
                                <img src="images/arrow-down-white.PNG">
                            </span>
                        </div>
                    </div>
                    <!-- START: Repeater for the OUTWARD flight segments summary -->
                    <div class="highlight rtl display_inline_block full-width no-margin-bottom " data-toggle="outwardFlightDiv">
                        <asp:Repeater ID="rpd_OutwardSegments" runat="server" ItemType="LowCostSite.FlightsSearchService.FlightSegment">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-sm-12 rtl">
                                        <h4 class="h4-flight-title outwardSegment"><%=GetText("FlgihtNo") %> <%# Container.ItemIndex + 1 %></h4>
                                        <!-- יציאה: -->
                                        <b><%=GetText("Departure")%>:</b>
                                        <br />
                                        <%# GenerateSegmentString(Item,false) %>
                                        <br />
                                        <b><%=GetText("Arrival")%>:</b><br />
                                        <%# GenerateSegmentString(Item,true) %>
                                        <br />
                                        <b><%=GetText("Airline")%>:</b>
                                        <%# Item.OperatingAirline.Name %>
                                        <br />
                                        <b><%=GetText("FlightNumber")%>: </b><%# Item.FlightCode %><br />
                                    </div>
                                </div>
                                <hr />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <!-- END: Repeater for the OUTWARD flight segments summary -->
                    <div runat="server" id="div_outwardFlightsDetails" class="display-none">
                        <div class="title-close full-width box-padding" onclick="ToggleArea('returnFlightDiv')">
                            <div class="row-space ">
                                <%=GetText("ReturnFlight")%>
                                <span class="arrow-down-filter">
                                    <img src="images/arrow-down-white.PNG">
                                </span>
                            </div>
                        </div>
                        <!-- START: Repeater for the RETURN flight segments summary -->
                        <div class="highlight rtl display_inline_block full-width no-margin-bottom " data-toggle="returnFlightDiv">
                            <asp:Repeater ID="rpd_ReturnSegments" runat="server" ItemType="LowCostSite.FlightsSearchService.FlightSegment">
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="h4-flight-title returnSegment "><%=GetText("FlightNo")%> <%# Container.ItemIndex + 1 %></h4>
                                            <b><%=GetText("Departure")%>:</b><br />
                                            <%# GenerateSegmentString(Item,false) %>
                                            <br />
                                            <b><%=GetText("Arrival")%>:</b><br />
                                            <%# GenerateSegmentString(Item,true) %>
                                            <br />
                                            <b><%=GetText("Airline")%>:</b>
                                            <%# Item.OperatingAirline.Name %>
                                            <br />
                                            <b><%=GetText("FlightNumber")%>: </b><%# Item.FlightCode %><br />
                                        </div>
                                    </div>
                                    <hr />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <!-- END: Repeater for the RETURN flight segments summary -->
                    </div>
                </div>
                <!-- END: RIGHT SIDE COLUMN -->
              

                <!-- START: LEFT MAIN COLUMN -->
                <div class="col-sm-9">
                    <!-- START: STATUS MESSAGES -->
                    <!-- START: MESSAGE: CHANGE IN PRICE -->
                    <asp:Panel ID="panel_ErrorPrice" Visible="false" runat="server">
                        <div class="highlight rtl display_inline_block full-width">
                            <div class="box-wrap-content text-center ">
                                <div class="tips-box">
                                    <h1 class="title_content"><%=GetText("ThereHasBeenAChangeInTheOrderPrice")%></h1>
                                    <div>
                                        <h3><%=GetText("TheNewTotalPriceIs")%>: <span runat="server" id="lbl_newPrice"></span>
                                        </h3>
                                    </div>

    <%--                                <asp:Button runat="server" ID="btn_ContinueOrder" CssClass="btn btn-primary " Text="gtxt_ContinueToOrder" OnClick="btn_ContinueOrder_Click" />
                                    <asp:Button runat="server" ID="btn_Cancel" Text="gtxt_Cancel" CssClass="btn" OnClick="btn_Cancel_Click" />--%>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <!-- END: MESSAGE: CHANGE IN PRICE -->

                    <!-- END: STATUS MESSAGES -->

                    <div id="div_generalDiv" class="passengers-details" runat="server">
                        <!-- START: DETAILS -->
                        <div class="highlight rtl display_inline_block full-width no-margin-bottom">
                            <div class="box-wrap-content no-padding-bottom ">
                                <div class="box-wrap-title"><%=GetText("PaymentPageTitle")%>:</div>
                                  <iframe style="min-height: 500px; width: 100%; border:none;" scrolling="no"  id="iFramePayment" runat="server" ></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: LEFT MAIN COLUMN -->
            </div>
        </div>
    </div>
</asp:Content>

