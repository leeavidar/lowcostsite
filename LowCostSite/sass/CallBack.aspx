﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CallBack.aspx.cs" Inherits="LowCostSite.sass.CallBack" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1 style="text-align:center"><%=GetText("TheOrderIsInProcessAgentWillContactYou")%></h1>
    </div>
    </form>
</body>
</html>
