﻿var SelectedOutwardIataCode;
var SelectedReturnIataCode;
var hf_AutoCompleteOriginCode;
var hf_AutoCompleteDestinationCode;
var hf_DefultTlvText;
$(function () {
    InitAutocomplete();
});


function InitAutocomplete() {
    // Hides the autocomplete boxes when the user clicks anywhere oon the document outside the autocomplete.
    $(document).on("click", function () {
        if ($('.autocomplete-origin').is(":visible")) {
            $('.autocomplete-origin').addClass("hidden");
        }
        if ($('.autocomplete-destination').is(":visible")) {
            $('.autocomplete-destination').addClass("hidden");
        }
        if ($('.popularDestination').is(":visible")) {
            $('.popularDestination').addClass("hidden");
        }
    });

    // select the content of the textbox when clicking on it
    $(".Autocomplete").focus(function () { this.select(); });

    // bind the text keyup event of the textboxes to the ajax function that gets the autocomplete values
    $('.txt_Origin').on('keypress keydown keyup', function (e) {
        if ($('.autocomplete-origin').hasClass('hidden') && $(".txt_Origin").val().replace(' ', '') != '') {
            $('.autocomplete-origin').removeClass('hidden');
        }
        // if the user clicks enter
        if (e.keyCode == 13) {
            // if the user presses "enter" - the first city will be selected
            // trigger the click event of the first city on the list (linked to the SelectLocation function)
            $('.autocomplete-origin').find('.city-item').first().click();
            $('.autocomplete-origin').addClass('hidden');
            // to prevent the submittion of the form (search)
            return false;
        }
        // for tab, shift+tab and escape keys
        else if (e.keyCode == 9 || (e.keyCode == 9 && e.shiftKey) || e.keyCode == 27) {
            $('.autocomplete-origin').addClass('hidden');
        }
        GenerateItems($(".txt_Origin").val(), "origin");
    });

    $('.txt_Destination').on('keypress keydown keyup', function (e) {

        $('.popularDestination').addClass('hidden');
        if ($('.autocomplete-destination').hasClass('hidden') && $(".txt_Destination").val().replace(' ', '') != '') {
            $('.autocomplete-destination').removeClass('hidden');
        }
        // if the user clicks enter or
        if (e.keyCode == 13) {
            // if the user presses "enter" - the first city will be selected
            // trigger the click event of the first city on the list (linked to the SelectLocation function)
            $('.autocomplete-destination').find('.city-item').first().click();
            $('.autocomplete-destination').addClass('hidden');
            // to prevent the submittion of the form (search)
            return false;
        }
        // for tab, shift+tab and escape keys
        else if (e.keyCode == 9 || (e.keyCode == 9 && e.shiftKey) || e.keyCode == 27) {
            $('.autocomplete-destination').addClass('hidden');
        }
        GenerateItems($(".txt_Destination").val(), 'destination');
    });
};

/// A function that makes an ajax call for autocomplete items.
function GenerateItems(prefixText, place) {
    // Start to autocomplete when the number of characters entered is greated than 2
    if (prefixText.length < 1) {
        return;
    }
    else {
        $.ajax({
            type: "post",
            url: "/sass/Controls/Autocomplete.asmx/GetCityBoxesSimple",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ 'prefixText': prefixText, 'place': place }),
            dataType: "json",
            success: function (result) {
                OnSuccess(result.d, place);
            },
            error: function (xhr, status, error) {
                OnFailure(error);
            }
        });
    }
}
/// The function that's being called when the ajax call for autocomplete results is succeeded.
function OnSuccess(Results, place) {

    //   Results = Results.replace(/&/, "&amp;").replace(/"/g, "&#34;");

    var height = 300;
    if (place == "origin") {
        $(".autocomplete-origin").html(Results);
        if ($('.autocomplete-origin :nth-child(1)').height() < 300) {
            height = $(".autocomplete-origin :nth-child(1)").height() + 5;
            $('.autocomplete-origin').height(height);
            $('.autocomplete-origin').width(340);
        }
        else {
            // add to the width (because there is a scrollbar that takes from the width)
            $(".autocomplete-origin").width(340);
        }

    }
    else if (place == "destination") {
        $(".autocomplete-destination").html(Results);
        if ($(".autocomplete-destination :nth-child(1)").height() < 300) {
            height = $(".autocomplete-destination :nth-child(1)").height() + 5;
            $(".autocomplete-destination").height(height);
            $(".autocomplete-destination").width(340);
        }
        else {
            // add to the width (because there is a scrollbar that takes from the width)
            $(".autocomplete-destination").width(340);
            $(".autocomplete-destination").height(300);
        }
    }
}
/// When the autocomplete ajax request has failed
function OnFailure(error) {
    //  alert(error);
}

/// A function that is called when the user clicks on a city or an airport from the autocomplete box.
/// It adds the iata code of the clicked location to matching hidden fields (for origin or destination).
function SelectLocation(iataCode, place, displayText) {
    displayText = displayText.replace(/###/g, '"').replace(/##/g, '\'');  // replace all ## with ' and all ### with "
    if (place == "origin") {
        $('.autocomplete-origin').addClass('hidden');
        $(".txt_Origin").removeClass('border-error');
        if (iataCode != "TLV") {
            $('.txt_Destination').val($("#" + hf_DefultTlvText).val());
            $("#" + hf_AutoCompleteDestinationCode).val("TLV")
           
        } 
        $("#" + hf_AutoCompleteOriginCode).val(iataCode);
        $('.txt_Origin').val(displayText);
       
    }
    else if (place == "destination") {
        $('.autocomplete-destination').addClass('hidden');
        $(".txt_Destination").removeClass('border-error');
        if (iataCode != "TLV") {
            $('.txt_Origin').val($("#" + hf_DefultTlvText).val());
            $("#" + hf_AutoCompleteOriginCode).val("TLV");
        }
        $("#" + hf_AutoCompleteDestinationCode).val(iataCode);
        $('.txt_Destination').val(displayText);
        
    }

}
