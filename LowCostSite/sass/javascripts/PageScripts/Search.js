﻿// A method that's called efore the form is sent to the server.
// It validates all the search form, and if it's valid, shows the loading modal, and returns true. otherwise, returns false.
function ValidateFormAndSearch() {

    var originOk = true;
    var destOk = true;
    var outwardDateOk = true;
    var returnDateOk = true;

    //VALIDATE ORIGIN
    if ($("#" + hf_AutoCompleteOriginCode).val() == undefined || $("#" + hf_AutoCompleteOriginCode).val() == '' || $(".txt_Origin").val() == '') {
        $(".txt_Origin").addClass('border-error');
        originOk = false;
    }
    else {
        $(".txt_Origin").removeClass('border-error');
        originOk = true;
    }

    //VALIDATE DESTINATION
    if ($("#" + hf_AutoCompleteDestinationCode).val() == undefined || $("#" + hf_AutoCompleteDestinationCode).val() == '' || $(".txt_Destination").val() == '') {
        $(".txt_Destination").addClass('border-error');
        destOk = false;
    }
    else {
        $(".txt_Destination").removeClass('border-error');
        destOk = true;
    }

    //VALIDATE OUTWARD DATE
    if ($(".txt_OutwardDate").val() == undefined || $(".txt_OutwardDate").val() == '') {
        $(".txt_OutwardDate").addClass('border-error');
        outwardDateOk = false;
    }
    else {
        $(".txt_OutwardDate").removeClass('border-error');
        outwardDateOk = true;
    }

    //VALIDATE RETURN DATE
    // return date is not validated (it is not required) 


    // Checks if the number of infants is smaller than the number of adults.
    var infatsToAdultsRatio = CheckInfantsCount(); // $(".val_InfatsCount").IsValid;

    var allValid = originOk && destOk & outwardDateOk && returnDateOk && infatsToAdultsRatio;

    if (allValid) {
        $('#myModal').modal({
            backdrop: 'static'
        }).modal('show');
        return true;
    }
    return false;
}

// A function thats checks if the number of infants chosen is greater than the number of adults (doesn't prevent going to the server)
function CheckInfantsCount() {
    var ddl_Infants = $(".ddl_Infants option:selected").val();
    var ddl_Adults = $(".ddl_Adults option:selected").val();
    // if the number of infants is larger than the number of adults
    if (ddl_Infants > ddl_Adults) {
        // Show error
        $('.ddl_Infants').addClass('border-error');
        $('.infantsError').show();
        $(".val_InfatsCount").IsValid = false;
        return false;
    }
    else {
        // Remove error
        $('.ddl_Infants').removeClass('border-error');
        $('.infantsError').hide();
        $(".val_InfatsCount").IsValid = true;
        return true;
    }
}