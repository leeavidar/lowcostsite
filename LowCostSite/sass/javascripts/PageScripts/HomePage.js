﻿// --------------------------------- START: HOME PAGE ----------------------------------------------------------

//BINDING MOETHODS TO ELEMENTS:
$(function () {
    // When te home page loads for the first time, there should be only 4 destinations displayed. 
    ShowOnlyFourDestinations();
    $(".lnk_ShowMoreDest").click(ToggleMoreDestinations);


    // BIND MORE EVENTS TO METHODS HERE
});


//
function ShowOnlyFourDestinations() {
    $('.pop-dest').css("display", "normal");
    $('.pop-dest.extra-destinations').css("display", "none");   
}


function ToggleMoreDestinations() {
    $('.pop-dest.extra-destinations').slideToggle(200);

    // switch between the show more and hide texts
    $(".text-show-more").toggle();
    $(".text-hide-more").toggle();
}




// --------------------------------- END: HOME PAGE ----------------------------------------------------------
