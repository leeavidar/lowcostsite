﻿// the 2 global variables are for te 2 hidden fields for the autocomplete selection validation.
// the variables are initizlized on the page. (UC_Search)
var hf_AutoCompleteOriginCode;
var hf_AutoCompleteDestinationCode;
// holds the state of the infants and children choosing panel ('opened' or 'closed')
var ChildrenInfatsPanel;


$(function () {
    // click on the document to close the destination chooser
    $(document).on("click", function () {
        if ($('.popularDestination').is(':visible') == true) {
            $('.popularDestination').addClass('hidden');
        }
    });



    $('.open-destinatios, .txt_Destination').click(function (e) {
        $('.popularDestination').toggleClass('hidden');
        if ($('.autocomplete-destination').is(":visible")) {
            $('.autocomplete-destination').addClass("hidden");
        }
        return false;
    });
    $('.popularDestination').click(function (e) {
        return false;
    });
    $('.popular-destination-close').click(function () { $('.popularDestination').addClass('hidden'); });

    // When clicking the "Select destination" button, the selected item from the DDL will be saved in the destination hidden field.
    $('.btn_SelectDestination').click(function () {
        
        $('#' + hf_AutoCompleteDestinationCode).val($('.select-city').find(":selected").val());
        $('.txt_Destination').val($('.select-city').find(":selected").text());
        $('.popularDestination').addClass('hidden');
    });



    $('.select-country').change(function () {
        var countryId = $('.select-country').val();

        $.ajax({
            type: "post",
            url: "/sass/AjaxHandler.asmx/GetCityInCountry",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ 'countryCode': countryId }),
            dataType: "json",
            success: function (result) {
                OnSuccess(result);
            },
            error: function (xhr, status, error) {
                // OnFailure(error);
            }
        });
        function OnSuccess(citiesString) {

            var jsonCities = jQuery.parseJSON(citiesString.d); //  JSON.stringify(eval("(" + citiesString + ")"));
            $(".select-city").empty();
            // first row:
            $(".select-city").append('<option value="">' + $(".select-city").data('first-row') + '</option>');
            
            for (var i = 0; i < jsonCities.length; i++) {
                $(".select-city").append('<option value=' + jsonCities[i].IataCode + '>' + jsonCities[i].Name + '</option>');
            }
            $(".select-city").trigger("chosen:updated");
        }


    });



    SetDatePickers();

    // If the hidden field stores the value closed, the panel for children and infants will be closed
    if ($("#" + ChildrenInfatsPanel).val() == 'closed') {
        $('.div_ChildInfants').addClass('closed');
    }
    // binds the click event of the "add children and infants" button to the showing and hiding of the panel.
    $(".AddChildInfant").click(function () {
        $('.div_ChildInfants').toggleClass('closed');
        if ($('.div_ChildInfants').hasClass('closed')) {
            // add the 'closed' value to the hidden field
            $("#" + ChildrenInfatsPanel).val('closed');
        }
        else {
            // add the 'opened' value to the hidden field
            $("#" + ChildrenInfatsPanel).val('opened');
        }
        // toggle the visibility of the text boxes for children/infants also.
        $('.div_Child').fadeToggle('slow');
        $('.div_Infant').fadeToggle('slow');
    });
});


// A method that sets the properties of the date pickers
function SetDatePickers() {
    // initialize the datepicker and set some options
    $.fn.datepicker.dates['he'] = {
        days: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת", "ראשון"],
        daysShort: ["א", "ב", "ג", "ד", "ה", "ו", "ש", "א"],
        daysMin: ["א", "ב", "ג", "ד", "ה", "ו", "ש", "א"],
        months: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
        monthsShort: ["ינו", "פבר", "מרץ", "אפר", "מאי", "יונ", "יול", "אוג", "ספט", "אוק", "נוב", "דצמ"],
        today: "היום",
        rtl: true
    };

    //Select all the datePickers
    var dp1 = $(".txt_OutwardDate");
    dp1.datepicker({
        format: "dd/mm/yyyy",
        weekStart: 0,
        startDate: '-0d',
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        language: "he"
    }).on('changeDate', function (ev) {
        var startDate = $(".txt_OutwardDate").val(); //DD/MM/YYYY
        dp2.datepicker('setStartDate', startDate);

        // if the return date is earlier that the outward date, set the return date to the same as the outward date
        if (GetDate(startDate).getTime() > GetDate($(".txt_ReturnDate").val()).getTime()) {
            dp2.datepicker('update', startDate);
            // Open the return flight date-picker to change selection
            dp2.datepicker('show');
        }

        // when selecting a date, close the datepicker
        $(this).blur();
        $(this).datepicker('hide');
    });


    ////Select all the datePickers
    var dp2 = $(".txt_ReturnDate");
    dp2.datepicker({
        format: "dd/mm/yyyy",
        weekStart: 0,
        startDate: '-0d',
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        language: "he"
    }).on('changeDate', function (ev) {
        // when selecting a date, close the datepicker
        $(this).blur();
        $(this).datepicker('hide');
    });
}

// A method that gets a tstring representing a date in the format: DD/MM/YYYY
// and returns a Date object
function GetDate(strDate) {
    var split = strDate.split('/');
    // Month is zero-indexed so subtract one from the month inside the constructor
    var date = new Date(split[2], split[1] - 1, split[0]); //Y M D 

    return date;
}

// A function that gets the checkbox for 1 way flight, and if it's checked, disables the return date text box
function Toggle2WayFlight(checkBox) {
    var CB = $(checkBox);
    if (CB.is(":checked")) {
        $(".txt_ReturnDate").attr("disabled", "disabled");
    }
    else {
        $(".txt_ReturnDate").removeAttr('disabled');
    }
}

// a function that sets the hidden field for destination to the selected city (from the DDL)
function SelectPopularDestination(listItem) {
    
    var name = $(listItem).data('display');
    var iata = $(listItem).prop('value');
    if (iata == "" || iata == 0) {
        iata = $(listItem).data('value');
    }
    // fill the destination text box and the hidden field value
    $('.txt_Destination').val(name);
    $('#' + hf_AutoCompleteDestinationCode).val(iata);
    if (iata != "TLV") {
        $('.txt_Origin').val($("#" + hf_DefultTlvText).val());
        $("#" + hf_AutoCompleteOriginCode).val("TLV");
    }
    // hide the popular destination div
    $('.popularDestination').addClass('hidden');

}

