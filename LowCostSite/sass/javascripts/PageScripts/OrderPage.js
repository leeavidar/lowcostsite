﻿// --------------------------------- START: ORDER PAGE  ----------------------------------------------------------


// BINDING ELEMENTS TO EVENTS:
$(function () {
    $("#ContentPlaceHolder1_cb_isContactFirstPax").click(SetFirstPassenger);

    //bind the buttons to the please wait message
    $('.btn_Book').click(function () {
        var form = $('#form1');
        form.validate();
        if (TermsCheacked()) {
           
            if (ValidatePage()) {
                ShowPleaseWaitModal();
            }
        } else {
            $("#msg_termsNotChacked").slideDown();
            return false;
        }
    });

    // don't show the count of the flight legs if there is only 1
    if ($('.outwardSegment').length == 1) {
        $('.outwardSegment').hide();
    }
    if ($('.returnSegment').length == 1) {
        $('.returnSegment').hide();
    }

});


function SetFirstPassenger() {
    if ($("#ContentPlaceHolder1_cb_isContactFirstPax").attr('checked') == 'checked') {
        $(".txt_ContactFirstName").attr("disabled", "disabled");
        $(".txt_ContactLastName").attr("disabled", "disabled");
        $(".rfv_ContactFirstName").enabled = false;
        $(".rfv_ContactLastName").enabled = false;
    }
    else {
        $(".txt_ContactFirstName").removeAttr("disabled");
        $(".txt_ContactLastName").removeAttr("disabled");
        $(".rfv_ContactFirstName").enabled = true;
        $(".rfv_ContactLastName").enabled = true;
    }
}


function ToggleArea(dataToggle) {
    $('*[data-toggle="' + dataToggle + '"]').slideToggle(200);
}

function SetDateOfBirthTxt(txt_BirthDate) {
    // the hidden text box for the date
    var dateTxt = $(txt_BirthDate);
    // first time set the value to empty.
    $(dateTxt).val("");
    // find the values for day,month and year
    var year = $(dateTxt).closest('.b-day-div').find('.year-part').val();
    var month = $(dateTxt).closest('.b-day-div').find('.month-part').val();
    var day = $(dateTxt).closest('.b-day-div').find('.day-part').val();
    // if one parameter was not selected, empty the hidden date text
    if (year == -1 || month == -1 || day == -1) {
        $(dateTxt).val("");
    }
    else {
        // add leading zeros and set the value of the hidden text field
        if (month.length == 1) {
            month = '0' + month;
        }
        if (day.length == 1) {
            day = '0' + day;
        }
        $(dateTxt).val(day + '/' + month + '/' + year);

        // remove the required field message if there is any
        $(dateTxt).closest('.b-day-div').find('.val_Required').hide();
    }
}
function ShowTicketConditiontModal() {
    $('#TicketConditionModal').modal('show');
    $('#TicketConditionModal').modal({ backdrop: 'static', keyboard: false });
}

// A function that hides the erroe message and shows the passenger details form
function BackToPassengerDetails() {
    
    $('.error-message').addClass('display-none');
    $('.passengers-details').removeClass('display-none');
}


// --------------------------------- END: RESULTS PAGE  ----------------------------------------------------------