﻿// --------------------- START: TEMPLATES --------------------- 

function ToggleLongText(btn) {
    // get the id of the long text from the data-toggle attribute of the button
    var longTextId = $(btn).data("toggle");
    // toggle the long text
    $("#" + longTextId).slideToggle();

    // change the text on the button from show to hide (or from hide to show)
    var buttonText = $(btn).text();
    if (buttonText == "קרא עוד") {
        $(btn).text("הסתר");
    }
    else if (buttonText == "הסתר") {
        $(btn).text("קרא עוד");
    }
}
