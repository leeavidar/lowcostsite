﻿// --------------------------------- START: RESULTS PAGE  ----------------------------------------------------------

// A global variable for the onBack event (to store the object that a function should get after returning from the server)
// for example: when clicking on the show details button of a flight, there is a server call that gets the details, and after returning to the page, 
// the J/S function that opens the box for the details is fired, and it uses the 'scriptThis' object (that has the button in it) to know what to toggle.
var scriptThis = null;

// BINDING ELEMENTS TO EVENTS:
$(function () {
    // after every update panel update, all the related scripts are not bound, and needs to re-bound (from code-behind: AddScript(...); ) 
    BindResultsAreaEvents();
    BindSearchAreaEvents();
    // The slider needs to be initialized only once (other times are using AddScript in C#)
    initializationSlider();
});

// A function that binds the search area events (in the update panel)
function BindSearchAreaEvents() {
    SetDatePickers();
    InitAutocomplete();
}

// A function that binds the results area events (in the update panel)
function BindResultsAreaEvents() {
    $(".group-collapse").unbind();
    $(".group-collapse").click(CollapseGroup);

    // if there are no results, hide the collapse all button:
    if ($('.flight-group').length == 0) {
        $('.collapse-all').hide();
    }

    setRadioButtons();

    // bind the order buttons to the function that shows the please wait modal
    $(".btn_Order").click(ShowPleaseWaitModal);
}

// for the loading box
$('#myModal').modal({ backdrop: 'static', keyboard: false });

// initializing the price filter (slider)
function initializationSlider() {
    //$('.slider').slider({ step: 1 });

    $('input.slider').slider();
}

// A method that sets the radio buttons name attribute 
function setRadioButtons() {
    $(".rb_group").each(function () {
        $(this).attr('name', $(this).data("group"));
    });
}

// A method that sets the properties of the date pickers
function SetDatePickers() {
    // initialize the datepicker and set some options
    $.fn.datepicker.dates['he'] = {
        days: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת", "ראשון"],
        daysShort: ["א", "ב", "ג", "ד", "ה", "ו", "ש", "א"],
        daysMin: ["א", "ב", "ג", "ד", "ה", "ו", "ש", "א"],
        months: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
        monthsShort: ["ינו", "פבר", "מרץ", "אפר", "מאי", "יונ", "יול", "אוג", "ספט", "אוק", "נוב", "דצמ"],
        today: "היום",
        rtl: true
    };

    //Select all the datePickers
    var dp1 = $(".txt_OutwardDate");
    dp1.datepicker({
        format: "dd/mm/yyyy",
        weekStart: 0,
        startDate: '-0d',
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        language: "he"
    }).on('changeDate', function (ev) {
        var startDate = $(".txt_OutwardDate").val(); //DD/MM/YYYY
        dp2.datepicker('setStartDate', startDate);

        // if the return date is earlier that the outward date, set the return date to the same as the outward date
        if (GetDate(startDate).getTime() > GetDate($(".txt_ReturnDate").val()).getTime()) {
            dp2.datepicker('update', startDate);
            // Open the return flight date-picker to change selection
            dp2.datepicker('show');
        }

        // when selecting a date, close the datepicker
        $(this).blur();
        $(this).datepicker('hide');
    });


    ////Select all the datePickers
    var dp2 = $(".txt_ReturnDate");
    dp2.datepicker({
        format: "dd/mm/yyyy",
        weekStart: 0,
        startDate: '-0d',
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        language: "he"
    }).on('changeDate', function (ev) {
        // when selecting a date, close the datepicker
        $(this).blur();
        $(this).datepicker('hide');
    });
}

// A method that toggles the search area
function ShowSearch() {
    $(".searchPanel").slideToggle();
}


/// Fired when clicking on a "details" button for one of the flights
function BeforeDetails(btn)
{
    // save the button in a global var, to use after coming back from the server
    scriptThis = $(btn);
    var RB = $(btn).closest('label').find('input');
    // if the details are opened, only close them and set the total price
    var openElementId = $(btn).data("toggle");
    if ($("." + openElementId).is(':visible')) {
        var btn = scriptThis;
        // the class of the more info row is stored in the button, in its data-toggle attribute (added from code-behind):
        $("." + openElementId).slideToggle();
        // toggle style for the box of details. The class that we select by is added to the details box from code-behind, ( 'info_[flightId]' )
        $(".info_" + openElementId).toggleClass("row-info-list");
        var RB = $(btn).prev();
        RB.click();
        SetTotalPrice(RB);
        return false;
    }
    
    // save the btn in a global var to use after returning from the server.
    $(btn).prev().click();
    SetTotalPrice(RB);
}

// A method that toggles the more information panel under each flight row
function toggleDetails() {
    var btn = scriptThis;
    // the class of the more info row is stored in the button, in its data-toggle attribute (added from code-behind):
    var openElementId = btn.data("toggle");
    $("." + openElementId).toggle();
    // toggle style for the box of details. The class that we select by is added to the details box from code-behind, ( 'info_[flightId]' )
    $(".info_" + openElementId).toggleClass("row-info-list");
    // fire the radio button event:
    var RB = $(btn).prev();
    RB.click();
    SetTotalPrice(RB);
       
}


// A method that toggles the price box for each flgiht group
function TogglePriceBox(obj, botton) {
    id = obj.toString();
    $("#" + id).fadeToggle(100);
}


// a function that fires when the user clicks on the order button for one of the flight groups
// it raises a flag that the button was clicked (as hidden field), and finds the selected flights.
function OrderButtonClick(obj) {
    // First to flag that an "order" button was clicked - set the hidden field to true:
    // (It is used in the page load to check if the session didn't end before clicking the button, and if true and the session had ended, redirect to the homepage)
    $('#' + IsOrderClickedId).val('true');
    GetSelectedFlight(obj);
}

// A method thats storees the selected outward and return flights of the selected flight group, 
// when clicking on the order button of this group.
function GetSelectedFlight(obj) {
    var groupId = $(obj).data("groupid");
    var radios = $(".rb_group").filter('[data-group="outward_' + groupId + '"], [data-group="return_' + groupId + '"]').filter('input:checked');
    for (var i = 0; i < radios.length; i++) {
        if ($(radios[i]).data("group") == "outward_" + groupId){ 
            // store the flight id in the hidden field for outward flight
            $("#ContentPlaceHolder1_hdn_OutwardFlightId").val($(radios[i]).data("flightid"));
        }
        else if ($(radios[i]).data("group") == "return_" + groupId){ 
            // store the flight id in the hidden field for return flight
            $("#ContentPlaceHolder1_hdn_ReturnFlightId").val($(radios[i]).data("flightid"));
        }
    }
}

/// A function that sets the prices in the price box and the total price, when the user clicks on a radio button
function SetTotalPrice(obj) {
    obj = $(obj);
    // selecting the radio button that was selected
    var RB = $("#" + obj.prop('id'));
    // get the airlineId (with 'outward_' or 'return_' prefix)
    var groupId =RB.data("group");
    // remove the prefix:
    groupId = groupId.replace('outward_', '');
    groupId = groupId.replace('return_', '');

    var returnPrice = 0;
    var returnAdlutsPrice = 0;
    var returnChildrenPrice = 0;
    var returnInfantsPrice = 0;

    // get the prices of the 2 selected flights (outward, return) 
    // first select all the radio buttons with attribute data-group of outward_xxx or return_xxx (where xxx is groupId)
    var radios = $('.rb_group[data-group=outward_' + groupId + '], .rb_group[data-group=return_' + groupId + ']');

    var CountTwo = 0;
    for (var i = 0; i < radios.length; i++) {
        if (($('.txt_ReturnDate').val() != '' && CountTwo == 2) || ($('.txt_ReturnDate').val() == '' && CountTwo == 1)) {
            break;
        }
        
        if ($(radios[i]).data("group") == "outward_" + groupId && $(radios[i]).is(":checked")) {
            // alert("aa");
            // get the price of this selected flight
            var outwardPrice = parseFloat($(radios[i]).data("price"));//.data("price");
            var outwardAdlutsPrice = parseFloat($(radios[i]).data("adults_price"));
            var outwardChildrenPrice = parseFloat($(radios[i]).data("children_price"));
            var outwardInfantsPrice = parseFloat($(radios[i]).data("infants_price"));
            var currency = ' '+ $(radios[i]).data("currency").trim();
            CountTwo++;
        }
        else if ($(radios[i]).data("group") == "return_" + groupId && $(radios[i]).is(":checked")) {
            // get the price of this selected flight (if there is no use of parsefloat, it will later be a string and no calculations could be made using it)
            returnPrice = parseFloat($(radios[i]).data("price"));
            returnAdlutsPrice = parseFloat($(radios[i]).data("adults_price"));
            returnChildrenPrice = parseFloat($(radios[i]).data("children_price"));
            returnInfantsPrice = parseFloat($(radios[i]).data("infants_price"));
            CountTwo++;
        }
    }

    // set the price labels 
    var totalAdultsPrice = (parseFloat(outwardAdlutsPrice) + parseFloat(returnAdlutsPrice));
    var totalChildrenPrice = (parseFloat(outwardChildrenPrice) + parseFloat(returnChildrenPrice));
    var totalInfantsPrice = (parseFloat(outwardInfantsPrice) + parseFloat(returnInfantsPrice));
    var totalPrice = (outwardPrice + returnPrice).toFixed(2);
    $('.lbl_TotalPrice[data-groupid=' + groupId + ']').text(totalPrice + currency);
    $('.lbl_PriceBox_AdultPrice[data-groupid=' + groupId + ']').text(totalAdultsPrice.toFixed(2) + currency);
    $('.lbl_PriceBox_ChildPrice[data-groupid=' + groupId + ']').text(totalChildrenPrice.toFixed(2) + currency);
    $('.lbl_PriceBox_InfantPrice[data-groupid=' + groupId + ']').text(totalInfantsPrice.toFixed(2) + currency);
}

// A function that when selecting a city from the list, saves it in hidden field for use from server side
function ClientItemSelectedOrigin(sender, e) {
    $get(hf_AutoCompleteOriginCode).value = e.get_value();
}
function ClientItemSelectedDestination(sender, e) {
    $get(hf_AutoCompleteDestinationCode).value = e.get_value();
}

// A function that toggles the flights di splayed for each group (only 4 or all the flights)
function ShowMoreFlights(obj) {
    var count = 1;
    var allRows = $(obj).closest("ul").find("li").toArray();
    $(allRows).each(function () {
        if ($(this).hasClass("hiddenFirst")) {
            $(this).fadeToggle("300");
        }
    });

    // switch the show and hide buttons
    if ($(obj).hasClass("ShowMoreFlights")) {
        $(obj).next().show();
        $(obj).hide();
    }
    else if ($(obj).hasClass("HideMoreFlights")) {
        $(obj).prev().show();
        $(obj).hide();
    }
}


/// A function that toggles the groups between open and collapsed states
function CollapseGroup() {
    // for 2 way flights:
    if (!$(this).closest('.box-wrap-title').next().next().next().hasClass('hidden')) {
        $(this).closest('.box-wrap-title').next().slideToggle().next().slideToggle().next().slideToggle();
    }
    else {
        // for 1 way class (toggle only the outward flight (the rest is always hidden)
        $(this).closest('.box-wrap-title').next().slideToggle();
    }
    // Change the down arrow to an up arrow (different images)
    $(this).children('.arrow-up').toggle();
    $(this).children('.arrow-down').toggle();
    // when the group is collapsed, some style changes have to be made
    $(this).closest('.box-wrap-content').toggleClass("collapsed-group");
}

function CollapseAll() {
    // Change the down arrow to an up arrow (different images)
    
    $('.arrow-up').show();
    $('.arrow-down').hide();
    // hide the flights:
    if (!$(this).closest('.box-wrap-title').next().next().next().hasClass('hidden')) {
        $('.flight-row-box').hide(200);
        $('.separatorDiv').hide(200);
    }
    else {
        $('.flight-row-box').hide(200);
    }
    // when the group is collapsed, some style changes have to be made
    $('.box-wrap-content').addClass("collapsed-group");
}
// A method that gets a tstring representing a date in the format: DD/MM/YYYY
// and returs a Date object
function GetDate(strDate) {
    var split = strDate.split('/');
    // Month is zero-indexed so subtract one from the month inside the constructor
    var date = new Date(split[2], split[1] - 1, split[0]); //Y M D 

    return date;
}

// a function that validates the form first, and if it's valid, chenged the hidden field for newSearchMade to true, and submits the search 
function OnSearchClick() {
    var isValid = ValidateFormAndSearch();
    if (isValid) {
        $('#' + hf_IsNewSearchMadeId).val('true');
        return true;
    }
    return false;
}

// A method that's called efore the form is sent to the server.
// It validates all the search form, and if it's valid, shows the loading modal, and returns true. otherwise, returns false.
function ValidateFormAndSearch() {
    var originOk = true;
    var destOk = true;
    var outwardDateOk = true;
    var returnDateOk = true;

    //VALIDATE ORIGIN
    if ($("#" + hf_AutoCompleteOriginCode).val() == undefined || $("#" + hf_AutoCompleteOriginCode).val() == '' || $(".txt_Origin").val() == '') {
        $(".txt_Origin").addClass('border-error');
        originOk = false;
    }
    else {
        $(".txt_Origin").removeClass('border-error');
        originOk = true;
    }

    //VALIDATE DESTINATION
    if ($("#" + hf_AutoCompleteDestinationCode).val() == undefined || $("#" + hf_AutoCompleteDestinationCode).val() == '' || $(".txt_Destination").val() == '') {
        $(".txt_Destination").addClass('border-error');
        destOk = false;
    }
    else {
        $(".txt_Destination").removeClass('border-error');
        destOk = true;
    }

    //VALIDATE OUTWARD DATE
    if ($(".txt_OutwardDate").val() == undefined || $(".txt_OutwardDate").val() == '') {
        $(".txt_OutwardDate").addClass('border-error');
        outwardDateOk = false;
    }
    else {
        $(".txt_OutwardDate").removeClass('border-error');
        outwardDateOk = true;
    }

    //VALIDATE RETURN DATE
    // return date is not validated (it is not required) 


    // Checks if the number of infants is smaller than the number of adults.
    var infatsToAdultsRatio = CheckInfantsCount(); // $(".val_InfatsCount").IsValid;

    var allValid = originOk && destOk & outwardDateOk && returnDateOk && infatsToAdultsRatio;

    if (allValid) {
        $('#myModal').modal({
            backdrop: 'static'
        }).modal('show');
        return true;
    }
    return false;
}


// A function thats checks if the number of infants chosen is greater than the number of adults (doesn't prevent going to the server)
function CheckInfantsCount() {
    var ddl_Infants = $(".ddl_Infants option:selected").val();
    var ddl_Adults = $(".ddl_Adults option:selected").val();
    // if the number of infants is larger than the number of adults
    if (ddl_Infants > ddl_Adults) {
        // Show error
        $('.ddl_Infants').addClass('border-error');
        $('.infantsError').show();
        $(".val_InfatsCount").IsValid = false;
        return false;
    }
    else {
        // Remove error
        $('.ddl_Infants').removeClass('border-error');
        $('.infantsError').hide();
        $(".val_InfatsCount").IsValid = true;
        return true;
    }
}


function UnfilterResults()
{
    //location.reload();
    $('.cb_airline').prop('checked', true);
    // Filter the results
    $('.btn_filter').click();
    $('#cvote').val(3);
    $('#cvote').slider('refresh');
    return false;
}
// --------------------------------- END: RESULTS PAGE  ----------------------------------------------------------