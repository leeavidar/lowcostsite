﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using LowCostSite.FlightsBookingService;
using LowCostSite.FlightsSearchService;
using LowCostSite.sass.Controls;
using System.Xml.Serialization;
using System.IO;
using EO.Pdf;
using System.Drawing;

namespace LowCostSite.sass
{
    public partial class ThanksPage : BasePage_UI
    {
        #region Session Private fields
        private FlightIssuedDetails _oFlightIssuedDetails;
        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (Generic.SessionManager.GetSession<FlightIssuedDetails>(out _oFlightIssuedDetails) == true)
            {

            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<FlightIssuedDetails>(oSessionFlightIssuedDetails);
            Generic.SessionManager.SetSession<OrderObject>(oOrderObject);
            Generic.SessionManager.SetSession<Orders>(Order);

        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        { }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (Session["ThanksPageToken"] == null || Session["ThanksPageToken"].ToString().Length == 0)
            {
                Response.Redirect(StaticStrings.path_IndexPage);
            }

            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            // Clear the session, so if the user tries to access this page again, he will be redirected to the homepage (in the page preLoad)
            Session.Clear();
        }

        #endregion
    }

    public partial class ThanksPage : BasePage_UI
    {
        #region Properties
        #region Private
        private OrderObject oOrderObject;
        private Orders _Order;
        #endregion
        #region Public

        public FlightIssuedDetails oSessionFlightIssuedDetails { get { return _oFlightIssuedDetails; } set { _oFlightIssuedDetails = value; } }
        int orderDetailsCount = 1;
        public int OrderDetailsCount { get { return orderDetailsCount; } set { orderDetailsCount = value; } }

        public int OrderDocID
        {
            get
            {
                return ConvertToValue.ConvertToInt(Request.QueryString["OrderDocID"]);
            }
        }

        public Orders Order
        {
            get
            {
                if (_Order != null)
                {
                    return _Order;
                }
                if (Session["OrderPDF"] != null)
                {
                    return Session["OrderPDF"] as Orders;
                }
                else
                {
                    Orders oOrder = OrdersContainer.SelectByKeysView_WhiteLabelDocId_DocId(GetWhiteLabelDocID(), OrderDocID, true).Single;
                    _Order = oOrder;
                    return oOrder;
                }
            }
        }

        // to het ifo like email, facebook etc. for the customer email. not in use right now.
        public WhiteLabel WhiteLabelObj { get { return WhiteLabelObj; } }
        public string WhiteLabelEmail { get { return WhiteLabelObj.Email_UI; } }


        #endregion

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                initPage();
            }
        }

        #region Init Functions

        private void initPage()
        {
            // setting for the aspx to pdf convertor: size to A4, margins, and area to render (id of the div)
            EO.Pdf.HtmlToPdf.Options.PageSize = EO.Pdf.PdfPageSizes.A4;
            EO.Pdf.HtmlToPdf.Options.OutputArea = new RectangleF(0.3f, 0.3f, 7.8f, 10.8f);
            HtmlToPdf.Options.VisibleElementIds = "areaForPdf";
            setPage();
            // create PDF from HTML
            ASPXToPDF.RenderAsPDF(false);
            ASPXToPDF.AfterRender += new EventHandler(ASPXToPDF_AfterRender);
        }

        private void setPage()
        {
            string strMailContentForSupport = "תוכן מייל לשירות לקוחות";
            string strMailSubjectForSupport = "נושא הודעה שירות לקוחות";

            oOrderObject = (OrderObject)createOrderObjectFromXml(BasePage_UI.GetXMLDirByDateAndOrderId(Order.DateCreated_Value, Order.DocId_UI) + "\\XMLOrder_" + Order.DocId_UI + ".xml", typeof(OrderObject));

            if (oOrderObject != null && oOrderObject.IsReady)
            {
                //setFlightsDetails
                SetFlightInfo();

                #region set main passenger details

                if (oOrderObject.BookingParameters.Passengers.DataSource != null)
                {
                    UC_PassengerDetailsView oUC_PassengerDetails;
                    int counter = 1;
                    foreach (var item in oOrderObject.BookingParameters.Passengers.DataSource)
                    {
                        oUC_PassengerDetails = (UC_PassengerDetailsView)LoadControl(StaticStrings.path_UC_PassengerDetailsView);
                        oUC_PassengerDetails.PassengerType = (LowCostSite.FlightsSearchService.ECustomerType)item.PassengerType;
                        oUC_PassengerDetails.PassengerNumber = counter;
                        oUC_PassengerDetails.PassengerDetailsToSet = item;
                        oUC_PassengerDetails.DateOfBirth = item.BirthDate;
                        oUC_PassengerDetails.RequiredParameters = oOrderObject.PricedItineraryDetails.RequiredParameters;
                        oUC_PassengerDetails.ID = string.Format("UC_PassengerView_{0}", counter);
                        ph_PassengerDetails.Controls.Add(oUC_PassengerDetails);
                        counter++;
                    }
                }

                #endregion

                #region set contact details
                //lbl_ContactTitle.Text = "Mr";//Order.ContactDetailss.Single. -- the title of contact details is missing.
                lbl_ContactFirstName.Text = Order.ContactDetails.FirstName_UI;
                lbl_ContactLastName.Text = Order.ContactDetails.LastName_UI;
                lbl_Email.Text = Order.ContactDetails.Email_UI;
                lbl_MainPhone.Text = Order.ContactDetails.MainPhone_UI;
                lbl_PhoneSecond.Text = Order.ContactDetails.MobliePhone_UI;
                #endregion

                #region Order Details
                //lbl_LowCostPNR.Text = Order.LowCostPNR_UI;
                lbl_LowCostPNR.Text = Order.DocId_UI;
                lbl_SupplierPNR.Text = Order.SupplierPNR_UI;
                #endregion

            }
            else
            {
                LoggerManagerProject.LoggerManager.Logger.WriteWarrning("OrderSummary.aspx (setPage()) : oOrderObject is null or oOrderObject.MainPassenger is null or oOrderObject.ContactDetails is null.");
                strMailContentForSupport = string.Format("OrderSummary.aspx : One of oOrderObject objects is null or oOrderObject object is null.");
                strMailSubjectForSupport = string.Format("Warrning : OrderSummary.aspx (setPage()) : oOrderObject is null or oOrderObject.MainPassenger is null or oOrderObject.ContactDetails is null.");
                MailHandler.SendMailToSupport(strMailContentForSupport, strMailSubjectForSupport, true);
            }
        }

        #endregion

        #region Page Functions

        /// <summary>
        /// show room for order
        /// </summary>

        /// <summary>
        /// Get MAil Content For Order To send in the email
        /// </summary>        
        private string getMAilContent()
        {
            string outwardDate = lbl_OutwardDate.InnerText; //oOrderObject.CheckInDate.ToString("MM/dd/yy");
            string returnDate = lbl_ReturnDate.InnerText; // oOrderObject.CheckOutDate.ToString("MM/dd/yy");
            // The flight that was ordered:
            string name = lbl_Origin.InnerText + " > " + lbl_Destination.InnerText; //  string.Empty;
            // Logo Image source: 
            string src = "/sass/images/logo.png";
            string content = string.Format(@"
<html><head></head>
    <body style='font-family:arial; direction:rtl;'>
        <p style='text-align:center; margin-bottom:0'>
            <a href='http://{0}' style='border:none' >
                <img style='width:150px; text-align:center; margin:0 auto;' src='{1}/{2}'>
            </a>
        </p>
        <p style='width:100%; height:15px; background-color:#2595dc; margin-top:0'></p>
            <h1 style=\'text-align:center\'>
                {3} 
                <a href='http://{4}' target='blank' style='color:#2595dc'> {5} </a>
            </h1>
            <table style='width:100%; text-align:center; margin:0 auto;'>
            <tbody>
                <tr>
                    <td><h3>{6} : {7}</h3></td>
                </tr>
                <tr>
                    <td>
                    <h3 style='color:rgb(119, 119, 119)'>
                        {8}
                    </h3></td>
                </tr>
                <tr>
                    <td style=border-top:1px solid rgb(119, 119, 119); border-bottom:1px solid rgb(119, 119, 119); background-color:#f7f7f7; padding: 50px;'>
                        <table style='width:100%; margin:0 auto; text-align:center'>
                            <tbody>
                                <tr>
                                    <td><b>{9} : </b>{10} </td>
                                    <td><b>{11} : </b>{12}</td>
                                </tr>
                                <tr>
                                    <td colspan='3&quot;'></td>
                                </tr>
                                <tr>
                                    <td colspan='3&quot;'></td>
                                </tr>
                                <tr>
                                    <td><b>{13} : </b>{14}</td>
                                    <td><b>{15} : </b>{16}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><h3 style='direction:rtl; color:rgb(119, 119, 119)'><b>{17} : </b> <span style=' color:#2595dc'>{18}{19}</span></h3></td>
                </tr>
            </tbody>
        </table>
        <p style='width:100%; height:50px; background-color:#2595dc; text-align:center'>
            <a href='{20}' target='blank' style='color:white; text-align:center;line-height: 3;'>Visit us in Facebook</a>
        </p>
    </body>
</html>",
                /*0*/  ConfigurationHandler.DomainAddress, // The address of the domain (logo link) 
                /*1*/ "http://" + ConfigurationHandler.DomainAddress, // same address for the base address for the logo image
                /*2*/src, // logo image path
                /*3*/GetText("ORDER_SUMMARY_TEXT_1"), // thank you for choosing 
                /*4*/ConfigurationHandler.DomainAddress,
                /*5*/ConfigurationHandler.DomainAddress, // the text in the link (the site name)
                /*6*/GetText("ORDER_SUMMARY_TEXT_2"), // your order number is
                /*7*/Order.DocId_UI, // the order number
                /*8*/name, // name of the destination ordered (from - to)
                /*9*/GetText("Outward_date"),
                /*10*/outwardDate,
                /*11*/GetText("Return_date"),
                /*12*/returnDate,
                /*13*/GetText("ORDER_SUMMARY_TEXT_3"), // "name of the customer:"
                /*14*/ string.Format("{0} {1} {2}", oOrderObject.BookingParameters.Passengers.DataSource[0].PassengerBase.SocialTitle, oOrderObject.BookingParameters.Passengers.DataSource[0].PassengerBase.LastNameField, oOrderObject.BookingParameters.Passengers.DataSource[0].PassengerBase.FirstNameField),   //.MainPassenger.FullName,
                /*15*/GetText("ORDER_SUMMARY_TEXT_4"),  // "Number of passengers:" 
                /*16*/oOrderObject.BookingParameters.Passengers.DataSource.Count(), //.NumOfPassengers,
                /*17*/GetText("ORDER_SUMMARY_TEXT_5"),
                /*18*/ "", //oOrderObject.Currency,
                /*19*/ string.Format("{0} {1}", oOrderObject.TotalPriceWithHandlingFinal.ToString("0.00"), Extensions.GetCurrencySymbol(lbl_FooterTotalCurrency.InnerText)),
                /*20*/ "" // WhiteLabelObj.Facebook_UI
            );

            return content;
            //   return ConfigurationHandler.CustomerMailContent;
        }

        /// <summary>
        /// Create (Full) Order Object From Xml
        /// </summary>        
        private object createOrderObjectFromXml(string fileName, Type type)
        {
            OrderObject obj = BinaryHandler.DeserializeBinary<OrderObject>(fileName);
            return obj;
            #region OLD - with xml serialization
            //XmlSerializer xmlFormat = new XmlSerializer(type);

            //using (Stream fStream = File.OpenRead(fileName))
            //{
            //    var fromDisk = xmlFormat.Deserialize(fStream);
            //    return fromDisk;
            //}

            //  return null; 
            #endregion
        }

        /// <summary>
        /// A method that generates a string for a flight segment (from => to , 00:00)
        /// </summary>
        /// <param name="segment">The flight segment to create the string for.</param>
        /// <param name="IsArrival">Is it the arrival part (if it is, the time is time of arrival and not of departure)</param>
        /// <returns>The text describing the flight segment</returns>
        protected string GenerateSegmentString(FlightsSearchService.FlightSegment segment, bool IsArrival)
        {
            string segmentString;
            if (IsArrival)
            {
                segmentString = string.Format("{0} - {1},  {2}", Extensions.GetAirportName(segment.ArrivalAirport.LocationCode), Extensions.GetCityOfAirport(segment.ArrivalAirport.LocationCode), segment.ArrivalDateTime.ToString("HH:mm"));
            }
            else
            {
                segmentString = string.Format("{0} - {1},  {2}", Extensions.GetAirportName(segment.DepartureAirport.LocationCode), Extensions.GetCityOfAirport(segment.DepartureAirport.LocationCode), segment.DepartDateTime.ToString("HH:mm"));
            }
            return segmentString;
        }

        private void SetFlightInfo()
        {
            if (Order == null)
            {
                LoggerManagerProject.LoggerManager.Logger.WriteError("THanksPage.aspx => SetFlightInfo() method => Error: Order == null");
            }
            if (oOrderObject.SelectedItinerarySearch != null)
            {
                #region Details summary block (right top box)
                // fill the origin and destinations label
                lbl_Origin.InnerText = Extensions.GetLocationName(oOrderObject.SelectedItinerarySearch.SelectedFlights.Origin);
                lbl_Destination.InnerText = Extensions.GetLocationName(oOrderObject.SelectedItinerarySearch.SelectedFlights.Destination);
                // fill the outward andn return dates (with the day of week)
                lbl_OutwardDate.InnerText = Extensions.GetDateWithDayName(oOrderObject.SelectedItinerarySearch.SelectedFlights.Summary.OutwardDate);
                if (oOrderObject.SelectedItinerarySearch.SelectedFlights.ReturnDate == ConvertToValue.DateTimeEmptyValue)
                {
                    FlightDetails_ReturnDate.Visible = false;
                }
                else
                {
                    lbl_ReturnDate.InnerText = Extensions.GetDateWithDayName(oOrderObject.SelectedItinerarySearch.SelectedFlights.Summary.ReturnDate);
                }
                // create an empty prices container and empty segments container for the return flight
                LowCostSite.FlightsSearchService.Pricing oPriceContainerReturn = null;
                LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerReturn = null;

                FlightsSearchService.FlightContainer returnFlightContainer = oOrderObject.SelectedItinerarySearch.SelectedFlights.GroupList.DataSource.First().ReturnFlights;
                // if there are return flight segments
                if (returnFlightContainer != null && returnFlightContainer.DataSource != null && returnFlightContainer.DataSource.FirstOrDefault() != null)
                {
                    // get the price object
                    oPriceContainerReturn = returnFlightContainer.DataSource.FirstOrDefault().Price;
                    oSegmentLegContainerReturn = returnFlightContainer.DataSource.First().FlightSegments;
                }
                // fill the prices for adults, children, and infants
                FillPrices(oOrderObject.SelectedItinerarySearch.SelectedFlights.GroupList.DataSource.First().Price.HandlingFee, oOrderObject.SelectedItinerarySearch.SelectedFlights.GroupList.DataSource.First().OutwardFlights.DataSource.First().Price, oPriceContainerReturn);
                FillFlightsDetails(oOrderObject.SelectedItinerarySearch.SelectedFlights.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments, oSegmentLegContainerReturn);
                #endregion
            }

        }
        private void FillFlightsDetails(LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerOutward, LowCostSite.FlightsSearchService.SegmentLegContainer oSegmentLegContainerReturn)
        {
            rpd_OutwardSegments.DataSource = oSegmentLegContainerOutward.DataSource;
            rpd_OutwardSegments.DataBind();
            if (oSegmentLegContainerReturn != null)
            {
                div_outwardFlightsDetails.Attributes.Remove("class");

                rpd_ReturnSegments.DataSource = oSegmentLegContainerReturn.DataSource;
                rpd_ReturnSegments.DataBind();
            }
        }
        private void FillPrices(decimal HandlingFee, LowCostSite.FlightsSearchService.Pricing oPriceContainerOutward, LowCostSite.FlightsSearchService.Pricing oPriceContainerReturn)
        {
            bool isPassengerPrices = false;
            // passneger count
            int numOfAdults = oOrderObject.SearchParams.AdultsCount;
            int numOfChildren = oOrderObject.SearchParams.ChildrenCount;
            int numOfInfants = oOrderObject.SearchParams.InfantsCount;
            // int adults = 0, children = 0, infants = 0;


            int passengerNumber = oOrderObject.PassengerNumber;
            decimal SumAdultsPrice = 0, totalHandlingFee = 0, sumChildrenPrice = 0, sumInfantsPrice = 0;
            string currency = oOrderObject.FlightBookTotalNetCurrency;


            decimal Delta = oOrderObject.DeltaNetPrice;
            decimal deltaNetPricePerPassenger = Delta / passengerNumber;
            decimal pricePerPassenger; 

            if (oPriceContainerOutward != null)
            {
                // Add the handling fee for each passenger for the outward segment
                totalHandlingFee += passengerNumber * HandlingFee;

                if (oPriceContainerOutward.PassengerPrices != null && oPriceContainerOutward.PassengerPrices.DataSource.Count() > 0)
                {
                    isPassengerPrices = true;
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.ADT))
                    {
                        SumAdultsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp) + deltaNetPricePerPassenger;
                    }
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.CNN))
                    {
                        sumChildrenPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp) + deltaNetPricePerPassenger;
                    }
                    foreach (var item in oPriceContainerOutward.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.INF))
                    {
                        sumInfantsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp) + deltaNetPricePerPassenger;
                    }
                }
                else
                {
                    // Calculate the equal price per passenger
                    pricePerPassenger = oPriceContainerOutward.TotalAmountWithMarkUp.Value / passengerNumber;
                    // Set this price, times the number of passenger in each category
                    SumAdultsPrice += numOfAdults * pricePerPassenger;
                    sumChildrenPrice += numOfChildren * pricePerPassenger;
                    sumInfantsPrice += numOfInfants * pricePerPassenger;
                }
            }
            if (oPriceContainerReturn != null)
            {
                // Multiply the handling fee by 2 for the return flight
                totalHandlingFee *= 2;
                if (oPriceContainerReturn.PassengerPrices != null && oPriceContainerReturn.PassengerPrices.DataSource.Count() > 0)
                {
                    // Attention: the delta-per-passenger only needs to be added once (for the outward flight)
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.ADT))
                    {
                        SumAdultsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                    }
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.CNN))
                    {
                        sumChildrenPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                    }
                    foreach (var item in oPriceContainerReturn.PassengerPrices.DataSource.Where(passenger => passenger.PassengerType == FlightsSearchService.ECustomerType.INF))
                    {
                        sumInfantsPrice += ConvertToValue.ConvertToDecimal(item.TotalAmountWithMarkUp);
                    }
                }
                else
                {
                    if (!isPassengerPrices)
                    {
                        // Calculate the equal price per passenger
                        pricePerPassenger = oPriceContainerReturn.TotalAmountWithMarkUp.Value / passengerNumber;
                        // Set this price, times the number of passenger in each category
                        SumAdultsPrice += numOfAdults * pricePerPassenger;
                        sumChildrenPrice += numOfChildren * pricePerPassenger;
                        sumInfantsPrice += numOfInfants * pricePerPassenger;
                    }
                }
            }
            #region Set the Labels for the passengers
            currency = Extensions.GetCurrencySymbol(currency);
            if (numOfAdults > 0)
            {
                lbl_PriceBox_AdultCount.Text = numOfAdults.ToString();
                lbl_PriceBox_AdultPrice.Text = string.Format("{0} {1}", currency, SumAdultsPrice.ToString("0.00"));
                div_AdultPrice.Attributes["class"] = "row";
            }
            if (numOfChildren > 0)
            {
                lbl_PriceBox_ChildCount.Text = numOfChildren.ToString();
                lbl_PriceBox_ChildPrice.Text = string.Format("{0} {1}", currency, sumChildrenPrice.ToString("0.00"));
                div_ChildPrice.Attributes["class"] = "row";
            }
            if (numOfInfants > 0)
            {
                lbl_PriceBox_InfantCount.Text = numOfInfants.ToString();
                lbl_PriceBox_InfantPrice.Text = string.Format("{0} {1}", currency, sumInfantsPrice.ToString("0.00"));
                div_InfantPrice.Attributes["class"] = "row";
            }
            if (HandlingFee > 0)
            {
                lbl_PassengerNumber.Text = passengerNumber.ToString();
                lbl_HandlingFee.Text = string.Format("{0} {1}", currency, totalHandlingFee.ToString("0.00"));
                div_HandlingFee.Attributes["class"] = "row";
            }

            lbl_TotalPrice.Text = string.Format("{0} {1}", (SumAdultsPrice + sumChildrenPrice + sumInfantsPrice + totalHandlingFee).ToString("0.00"), currency);
            lbl_FooterTotalCurrency.InnerText = currency;
            decimal TotalPrice = SumAdultsPrice + sumChildrenPrice + sumInfantsPrice + totalHandlingFee;
            lbl_FooterTotalPrice.InnerText = TotalPrice.ToString("0.00"); 
            #endregion
        }

        #endregion

        #region Events

        private void ASPXToPDF_AfterRender(object sender, EventArgs e)
        {
            int MailSendOptions = -1;
            string strMailContentForSupport = "";
            string strMailSubjectForSupport = "";

            try
            {
                if (Order != null)
                {
                    string content = getMAilContent();

                    string mailTitle = ConfigurationHandler.CustomerMailSubject; //GetText("orderEmailText") + Order.DocId_UI;

                    DateTime today = DateTime.Today;
                    HtmlToPdfResult result = (HtmlToPdfResult)ASPXToPDF.Result;
                    string filePath = string.Format(@"{0}\OrderSummery_{1}.pdf", BasePage_UI.GetXMLDirByDateAndOrderId(Order.DateCreated_Value, Order.DocId_UI), Order.DocId_UI);

                    result.PdfDocument.Save(filePath);

                    if (oOrderObject != null && oOrderObject.IsReady != null)
                    {
                        MailHandler.SendMailToSupportWithPDF(content, mailTitle, filePath, Order.DocId_UI);
                        MailHandler.SendMailToClientWithPdf(content, mailTitle, Order.ContactDetails.Email_UI, filePath, Order.DocId_UI);
                    }
                    else
                    {
                        LoggerManagerProject.LoggerManager.Logger.WriteWarrning("OrderSummary.aspx : oOrderObject is null or oOrderObject.ContactDetails is null.");
                        MailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                        strMailContentForSupport = string.Format("OrderSummary.aspx : oOrderObject is null or oOrderObject.ContactDetails is null.");
                        strMailSubjectForSupport = string.Format("OrderSummary.aspx : PDF dont sended.");
                    }
                }
                else
                {
                    LoggerManagerProject.LoggerManager.Logger.WriteWarrning("OrderSummary.aspx : Order object empty.");
                    MailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                    strMailContentForSupport = string.Format("OrderSummary.aspx : Order object empty.");
                    strMailSubjectForSupport = string.Format("Error : OrderSummary.aspx - Order object empty.");
                }
            }
            catch (Exception ex)
            {
                LoggerManagerProject.LoggerManager.Logger.WriteWarrning("OrderSummary.aspx - ASPXToPDF_AfterRender", ex);
                MailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                strMailContentForSupport = string.Format("OrderSummary.aspx - ASPXToPDF_AfterRender. Error : {0}", ex);
                strMailSubjectForSupport = string.Format("Error : OrderSummary.aspx - ASPXToPDF_AfterRender.");
            }
            finally
            {
                try
                {
                    switch (MailSendOptions)
                    {
                        case (int)EnumHandler.MailSendOptions.OnlySupport:
                            MailHandler.SendMailToSupport(strMailContentForSupport, strMailSubjectForSupport);
                            break;
                        case (int)EnumHandler.MailSendOptions.SupportAndAlgoma:
                            MailHandler.SendMailToSupport(strMailContentForSupport, strMailSubjectForSupport, true);
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    //Cannot send email!!!!
                    LoggerManagerProject.LoggerManager.Logger.WriteError(string.Format("EX : {0}  {2} Cannot send email. Email Option : {1}", ex, MailSendOptions, Environment.NewLine));
                }
            }
        }

        #endregion
    }
}