﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using Generic;
using DL_LowCost;

namespace LowCostSite.sass
{
    public partial class Tips : BasePage_UI
    {
        #region Session Private fields

        private TipContainer _oSessionTipPage;
        private StaticPagesContainer _oSessionStaticPages;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<TipContainer>(out _oSessionTipPage) == false)
            {
                //take the data from the table in the database
                _oSessionTipPage = GetTipsFromDB();

            }
            else { /*Return from session*/}
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<StaticPagesContainer>(out _oSessionStaticPages) == false)
            {
                //take the data from the table in the database
                _oSessionStaticPages = GetStaticPagesFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<TipContainer>(oSessionTipPage);
            Generic.SessionManager.SetSession<StaticPagesContainer>(oSessionStaticPages);

        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            Generic.SessionManager.ClearSession<TipContainer>(oSessionTipPage);
            Generic.SessionManager.ClearSession<StaticPagesContainer>(oSessionStaticPages);
        }


        /// <summary>
        /// A method that gets the active tip from the database. Only 1 tip should be set as active by the administrator.
        /// </summary>
        /// <returns></returns>
        private TipContainer GetTipsFromDB()
        {
            TipContainer allTips = TipContainer.SelectAllTips(BasePage_UI.GetWhiteLabelDocID(), true); 
            if (allTips != null && allTips.Count > 0)
            {
                return allTips;
            }
            return null;
        }
        private StaticPagesContainer GetStaticPagesFromDB()
        {
            //selecting all the static pages with a given white label
            return StaticPagesContainer.SelectAllStaticPagess(BasePage_UI.GetWhiteLabelDocID(), null);
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            //CallPageScript();
        }

        #endregion


    }

    public partial class Tips : BasePage_UI
    {

        #region Properties

        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public TipContainer oSessionTipPage { get { return _oSessionTipPage; } set { _oSessionTipPage = value; } }
        public StaticPagesContainer oSessionStaticPages { get { return _oSessionStaticPages; } set { _oSessionStaticPages = value; } }

        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetPage();
            }

        }


        /// <summary>
        /// A method that sets all the tips elements with data from the DB
        /// </summary>
        private void SetPage()
        {
            if (oSessionStaticPages != null)
            {
                StaticPages oStaticPages = _oSessionStaticPages.FindFirstOrDefault(page => page.Name == "Tips");
                if (oStaticPages != null)
                {
                    Master.SetMetaData(oStaticPages.SeoSingle);
                    if (string.IsNullOrEmpty(oStaticPages.Text_UI))
                    {
                        pnl_ContentText.Visible = false;
                    }
                    else
                    {
                        ph_StaticText.Controls.Add(new LiteralControl(oStaticPages.Text_UI));
                    }
                }
                else
                {
                    pnl_ContentText.Visible = false;
                }
            }
            if (oSessionTipPage != null)
            {
                drp_Tips.DataSource = oSessionTipPage.SortByKeyContainer(DL_Generic.KeyValuesType.KeyTipOrder, false).DataSource;
                drp_Tips.DataBind();
            }
        
        }
    }
}