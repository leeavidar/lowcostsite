﻿
using BL_LowCost;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;
using DL_Generic;

namespace LowCostSite.sass
{
    public partial class Index : BasePage_UI
    {
        #region Session Private fields

        private Tip _oSessionTip;
        private HomePage _oSessionHomePage;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            if (!isPostBack || Generic.SessionManager.GetSession<HomePage>(out _oSessionHomePage) == false)
            {
                //take the data from the table in the database
                _oSessionHomePage = GetHomePageFromDB();
            }

            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<Tip>(out _oSessionTip) == false)
            {
                //take the data from the table in the database
                _oSessionTip = GetTipFromDB();
            }
            else { /*Return from session*/}

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<HomePage>(oSessionHomePage);
            Generic.SessionManager.SetSession<Tip>(oSessionTip);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            Generic.SessionManager.ClearSession<HomePage>(oSessionHomePage);
            Generic.SessionManager.ClearSession<Tip>(oSessionTip);

        }


        /// <summary>
        /// A method that gets the active tip from the database. Only 1 tip should be set as active by the administrator.
        /// </summary>
        /// <returns></returns>
        private Tip GetTipFromDB()
        {
            TipContainer allTips;
            //selecting all the Tips with a given white label
            if (oSessionHomePage.TipDocId.HasValue)
            {
                 allTips = BL_LowCost.TipContainer.SelectByID(oSessionHomePage.TipDocId.Value, BasePage_UI.GetWhiteLabelDocID(), null); 
            }
            else
            {
                allTips = BL_LowCost.TipContainer.SelectAllTips(1,true);
            }
            
            if (allTips != null && allTips.Count > 0)
            {
                // return the frist active tip. Only 1 tip should be set as active by the administrator.
                return allTips.First;
            }
            return null;
        }

        /// <summary>
        /// A method that gets the home page from the database. Only 1 tip should be 1 home page per white label
        /// </summary>
        /// <returns></returns>
        private HomePage GetHomePageFromDB()
        {
            //selecting all the HomePages with a given white label (should be 1 homepage)
            HomePageContainer allHomePages = BL_LowCost.HomePageContainer.SelectAllHomePages(BasePage_UI.GetWhiteLabelDocID(), null); 
            if (allHomePages != null && allHomePages.Count > 0)
            {
                return allHomePages.First;
            }
            return null;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected new void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            CallPageScript();
        }

        #endregion


    }

    public partial class Index : BasePage_UI
    {

        #region Properties

        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public Tip oSessionTip { get { return _oSessionTip; } set { _oSessionTip = value; } }
        public HomePage oSessionHomePage { get { return _oSessionHomePage; } set { _oSessionHomePage = value; } }

        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion

        #endregion

      
        protected void Page_Load(object sender, EventArgs e)
        {

            SetPage();
        }

        private void SetPage()
        {
            Master.SetMenuClass(EnumHandler.MenuPages.HomePage);


            if (oSessionTip!=null)
            {
                lbl_Tip.Text = StringHandler.ShortCommand(oSessionTip.ShortText_UI, 70);
                lbl_Tip.ToolTip = oSessionTip.ShortText_UI;
            }
            if (oSessionHomePage!=null)
            {
                txt_Line1.Text = oSessionHomePage.TextLine1_UI;
                txt_Line2.Text = oSessionHomePage.TextLine2_UI;
                txt_Line3.Text = oSessionHomePage.TextLine3_UI;
                txt_Line4.Text = oSessionHomePage.TextLine4_UI;   
                // set the page meta data with the data in the seo property.
                Master.SetMetaData(oSessionHomePage.SeoSingle);
            }
            SetPopularDestinations();
        }

        private void SetPopularDestinations()
        {
            if (oSessionHomePage !=null)
            {
                // Get the selected destination pages (as HomePageDestinationPageContainer) and sort them by the order key
                HomePageDestinationPageContainer oHomePageDestinationPageContainer = oSessionHomePage.HomePageDestinationPages.SortByKeyContainer(DL_Generic.KeyValuesType.KeyHomePageDestinationPageOrder, false);
                // For each destination page, create a user control and fill it's properties
                int count = 1;

               
                foreach (var item in oHomePageDestinationPageContainer.DataSource)
                {
                    if ((count-1) % 4  == 0)
                    {
                        if (count == 1)
                        {
                            ph_PopularDestinations.Controls.Add(new LiteralControl("<div class='col-md-12 pop-dest '>"));
                        }
                        else
                        {
                            ph_PopularDestinations.Controls.Add(new LiteralControl("<div class='col-md-12 pop-dest extra-destinations'>"));
                        }
                       
                    }
                    if (item.DestinationPageSingle == null)
                    {
                        continue;
                    }
                    UC_PopularDest uc_PopularDest = (UC_PopularDest)LoadControl(StaticStrings.path_UC_PopularDestination);

                    // In order to show the destinations in groups of 4 in a row, give them an id with a number;
                    uc_PopularDest.ID = "pop_dest_" + count;

                    uc_PopularDest.Name = item.DestinationPageSingle.Name_UI;

                    if (count % 4 != 0)
                    {
                        uc_PopularDest.DestClass = "col-md-3 right box_shadow padd0 box_destination_size box_destination_space bottom_space";
                    }
                    else
                    {
                        uc_PopularDest.DestClass = "col-md-3 right box_shadow padd0 box_destination_size bottom_space";
                       
                    }
                    // Set the HomePageDestinationPage of the user control:
                    uc_PopularDest.oHomePageDestinationPage = item;
                    // Add the UC to the placeholder
                    ph_PopularDestinations.Controls.Add(uc_PopularDest);

                    if (count % 4 == 0)
                    {
                        ph_PopularDestinations.Controls.Add(new LiteralControl("</div>"));
                    }
                    count++;
                }
            } 
        }




    }
}