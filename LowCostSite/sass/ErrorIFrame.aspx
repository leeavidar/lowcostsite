﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorIFrame.aspx.cs" Inherits="LowCostSite.sass.ErrorIFrame" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="/sass/style.css" rel="stylesheet" />
</head>
<body>
 <div class="container">
        <div class="content-page">
            <div class="line-title"></div>
            <div class="highlight rtl display_inline_block full-width">

                <div class="box-wrap-content text-center">
                    <div class="tips-box">
                        <h1 class="title_content">אירעה שגיאה</h1>
                        <h3>אנו מתנצלים על אי הנוחות . לחזרה לדף הבית <a href="Index.aspx" target="_parent">לחץ כאן</a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
