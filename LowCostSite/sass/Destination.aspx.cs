﻿using System.Web.Caching;
using System.Web.UI.HtmlControls;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;
using BL_LowCost;
using DL_Generic;

namespace LowCostSite.sass
{
    public partial class Destination : BasePage_UI
    {
        #region Session Private fields

        private DestinationPage _oSessionDestinationPage;

        #endregion


        #region URL Get

        public int QueryStringID
        {
            get
            {
                return ConvertToValue.ConvertToInt(QueryStringManager.QueryString(EnumHandler.QueryStrings.ID));
            }
        }

        public string QueryStingURL
        {
            get
            {
                return QueryStringManager.QueryString(EnumHandler.QueryStrings.URL);
            }
        }

        #endregion


        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<DestinationPage>(out _oSessionDestinationPage) == false)
            {
                //take the data from the table in the database
                _oSessionDestinationPage = GetDestinationPageFromDB();
            }
            else { /*Return from session*/}

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<DestinationPage>(oSessionDestinationPage);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            Generic.SessionManager.ClearSession<DestinationPage>(oSessionDestinationPage);
        }

        /// <summary>
        /// A method that gets a destination page from the database.
        /// </summary>
        /// <returns></returns>
        private DestinationPage GetDestinationPageFromDB()
        {
            DestinationPageContainer oDestinationPageContainer = null;
            if (QueryStringID > 0)
            {
                oDestinationPageContainer = DestinationPageContainer.SelectByID(QueryStringID, BasePage_UI.GetWhiteLabelDocID(), null); 
                if (oDestinationPageContainer.Single != null)
                {
                    if (oDestinationPageContainer.Single.SeoSingle != null && oDestinationPageContainer.Single.SeoSingle.FriendlyUrl_Value != null)
                    {
                        if (oDestinationPageContainer.Single.SeoSingle.FriendlyUrl_Value.Length > 0)
                        {
                            Response.RedirectPermanent(SetLinkHandler.SetLink(EnumHandler.LinkPages.Destinations, EnumHandler.QueryStrings.URL, oDestinationPageContainer.Single.SeoSingle.FriendlyUrl_Value, oDestinationPageContainer.Single.DocId_UI));
                        }
                    }
                }
            }
            else
            {
                if (QueryStingURL != "" && QueryStingURL != null)
                {
                    oDestinationPageContainer = DestinationPageContainer.SelectAllDestinationPages(1, null).FindAllContainer(page => page.SeoSingle != null && page.SeoSingle.FriendlyUrl_Value == QueryStingURL);  // .SelectByKeysView_UrlFriendly(ShipURL);
                    if (oDestinationPageContainer.Single == null && ConvertToValue.ConvertToInt(QueryStingURL) > 0)
                    {
                        oDestinationPageContainer = DestinationPageContainer.SelectByID(ConvertToValue.ConvertToInt(QueryStingURL), BasePage_UI.GetWhiteLabelDocID(), null); 
                        if (oDestinationPageContainer.Single != null)
                        {
                            if (oDestinationPageContainer.Single.SeoSingle.FriendlyUrl_UI != "" && oDestinationPageContainer.Single.SeoSingle.FriendlyUrl_UI != null)
                            {
                                Response.RedirectPermanent(SetLinkHandler.SetLink(EnumHandler.LinkPages.Destinations, EnumHandler.QueryStrings.URL, oDestinationPageContainer.Single.SeoSingle.FriendlyUrl_UI, oDestinationPageContainer.Single.DocId_UI));
                            }
                        }
                        else
                        {
                            Response.Redirect(StaticStrings.path_404);
                        }
                    }
                }
                else
                {
                    Response.Redirect(StaticStrings.path_404);
                }
            }

            if (oDestinationPageContainer.Single != null)
            {
                return oDestinationPageContainer.Single;
            }
            else
            {
                return null;
            }
        }
  
        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            CallPageScript();
        }

        #endregion


    }

    public partial class Destination : BasePage_UI
    {
        #region Properties

        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public DestinationPage oSessionDestinationPage { get { return _oSessionDestinationPage; } set { _oSessionDestinationPage = value; } }

        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.SetMenuClass(EnumHandler.MenuPages.PopularDestination);
                SetPage();
            }
        }

        /// <summary>
        /// A method that sets all the page elements with data from the DB
        /// </summary>
        private void SetPage()
        {
            if (oSessionDestinationPage != null)
            {
                // Set the page meta data with the data in the seo property.
                if (oSessionDestinationPage.SeoSingle != null)
                {
                    Master.SetMetaData(oSessionDestinationPage.SeoSingle);
                }
                lbl_Title.Text = oSessionDestinationPage.Title_UI;
                if (!ConvertToValue.IsEmpty(oSessionDestinationPage.Price_Value) && oSessionDestinationPage.PriceCurrencyObject != null)
                {
                    lbl_Price.Text = oSessionDestinationPage.Price_UI + oSessionDestinationPage.PriceCurrencyObject.Sign_UI;
                }
                else
                {
                    // Hide the line foro minimup price
                    MinPriceInfo.Style.Add("display", "none");
                }
                // Calculate the current time based on the time difference from the DB
                int secondsTimeDiff = ConvertToValue.ConvertToInt(oSessionDestinationPage.Time_Value);
                string theTime = "";
                if (!ConvertToValue.IsEmpty(secondsTimeDiff))
                {
                    // the time field in the database stores the time difference from Israel time, so we need to calculate by adding the current server time to the time difference.
                    theTime = DateTime.Now.Add(new TimeSpan(0, 0, secondsTimeDiff)).ToShortTimeString();
                }
                lbl_Clock.Text = theTime;

                uc_Search.CityIataCode = oSessionDestinationPage.DestinationIataCode_UI;
                // Setting the cover image:

                // 1. get the image: 
                Images coverImage = oSessionDestinationPage.ImagesContainer.FindFirstOrDefault(image => image.ImageType_Value == EnumHandler.ImageTypes.Main.ToString());
                // 2. if the image exist, set it as the cover image
                if (coverImage != null && coverImage.ImageFileName_UI != "")
                {
                    string imagePath = string.Format("{0}/{1}", StaticStrings.path_Uploads, coverImage.ImageFileName_UI);
                    img_Cover.ImageUrl = imagePath;
                }
                // Set the content templates
                SetTemplates();
            }
        }


        #region Event Handlers


        #endregion

        #region Helping Methods

        /// <summary>
        /// A method that set the templates of the destination page in 2 columns, by the order field of each column.
        /// </summary>
        private void SetTemplates()
        {
            TemplateContainer RightTemplates = oSessionDestinationPage.Templates.FindAllContainer(template => template.Type == EnumHandler.TemplateType.MainInformation.ToString() || template.Type == EnumHandler.TemplateType.CenterWithImage.ToString());
            TemplateContainer LeftTemplates = oSessionDestinationPage.Templates.FindAllContainer(template => template.Type == EnumHandler.TemplateType.SideInformation.ToString() || template.Type == EnumHandler.TemplateType.SideWithImage.ToString());

            // Ordering the templates for each column by their order
            RightTemplates = RightTemplates.SortByKeyContainer(KeyValuesType.KeyTemplateOrder, false);
            LeftTemplates = LeftTemplates.SortByKeyContainer(KeyValuesType.KeyTemplateOrder, false);

            // Adding each column of templates to the matching place holder
            // Right side templates:
            foreach (Template oTemplate in RightTemplates.DataSource)
            {
                if (oTemplate.Type_Value == EnumHandler.TemplateType.MainInformation.ToString())
                {
                    UC_MainInformation oUC_MainInformation = (UC_MainInformation)LoadControl(StaticStrings.path_UC_MainInformation);
                    oUC_MainInformation.oTemplate = oTemplate;
                    ph_TemplateRight.Controls.Add(oUC_MainInformation);
                }
                if (oTemplate.Type_Value == EnumHandler.TemplateType.CenterWithImage.ToString())
                {
                    UC_CenterWithImage oUC_CenterWithImage = (UC_CenterWithImage)LoadControl(StaticStrings.path_UC_CenterWithImage);
                    oUC_CenterWithImage.oTemplate = oTemplate;
                    ph_TemplateRight.Controls.Add(oUC_CenterWithImage);
                }
            }

            // Left side templates:
            foreach (Template oTemplate in LeftTemplates.DataSource)
            {
                if (oTemplate.Type_Value == EnumHandler.TemplateType.SideInformation.ToString())
                {
                    UC_SideInformation oUC_SideInformation = (UC_SideInformation)LoadControl(StaticStrings.path_UC_SideInformation);
                    oUC_SideInformation.oTemplate = oTemplate;
                    ph_TemplateLeft.Controls.Add(oUC_SideInformation);
                }
                if (oTemplate.Type_Value == EnumHandler.TemplateType.SideWithImage.ToString())
                {
                    UC_SideWithImage oUC_SideWithImage = (UC_SideWithImage)LoadControl(StaticStrings.path_UC_SideWithImage);
                    oUC_SideWithImage.oTemplate = oTemplate;
                    ph_TemplateLeft.Controls.Add(oUC_SideWithImage);
                }
            }


        }

        #endregion
    }
}