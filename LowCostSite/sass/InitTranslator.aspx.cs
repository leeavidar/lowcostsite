﻿using DL_Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LowCostSite.sass
{
    public partial class InitTranslator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // Init the translation list (from translate table)
            TranslatorApp.InitTranslatedList();

            // Init the static data (cities, countries, airport etc.)
            StaticData.InitStaticData();

            Response.Redirect(StaticStrings.path_IndexPage);
        }
    }
}