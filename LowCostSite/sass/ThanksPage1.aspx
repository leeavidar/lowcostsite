﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="ThanksPage.aspx.cs" Inherits="LowCostSite.sass.ThanksPage" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">

    <script src="javascripts/PageScripts/OrderPage.js"></script>

    <style>
        .b-day
        {
            height: 34px;
            width: 29%;
        }

        .phone-prefix
        {
            width: 60%;
            display: inline-block;
        }

        .h4-flight-title
        {
            border-bottom: 1px solid black;
            padding-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="content-page">
            <div class="line-title"></div>
            <div class="row">

                <div class="col-sm-9">
                    <div class="highlight rtl display_inline_block full-width no-margin-bottom">

                        <div class="box-wrap-content no-padding-bottom ">
                            <h3>Thank you for choosing Low Cost</h3>
                            <h4>Your airline reference number is: <asp:Label ID="lbl_airlinePNR" runat="server"></asp:Label></h4>
                            <h4>TravelFusion reference number is: <asp:Label ID="lbl_TFPNR" runat="server"></asp:Label></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
