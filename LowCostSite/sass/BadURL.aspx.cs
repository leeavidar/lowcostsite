﻿using BL_LowCost;
using DL_LowCost;
using Generic;
using LowCostSite;
using LowCostSite.sass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

namespace LowCostSite
{
    public partial class BadURL : BasePage_UI
    {
        #region Session Methods
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            if (Generic.SessionManager.GetSession<OrderObject>(out oSessionOrderObject) == false)
            { 
              oSessionOrderObject = (OrderObject)createOrderObjectFromXml(BasePage_UI.GetXMLDirByDateAndOrderId(Order.DateCreated_Value, Order.DocId_UI) + "\\XMLOrder_" + Order.DocId_UI + ".xml", typeof(OrderObject));
            }
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Session["OrderObject"] = oSessionOrderObject;
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {

        }
        #endregion

        #region Events
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }

    public partial class BadURL : BasePage_UI
    {
        OrderObject oSessionOrderObject;

        #region Properties
        public Orders OrderByToken
        {
            get
            {
                Orders oOrder = OrdersContainer.SelectByKeysView_WhiteLabelDocId_OrderToken(GetWhiteLabelDocID(), OrderToken, true).Single;
                return oOrder;
            }
        }

        #region Pelecard return params

        public string OrderToken
        {
            get
            {
                if (DebugMode && !DebugModeMoveToPelecard)
                {
                    return Request.QueryString["OrderToken"];
                }
                else if (DebugMode && DebugModeMoveToPelecard)
                {
                    return Request.Form["parmx"];
                }
                else
                {
                    return Request.Form["parmx"];
                }
            }
        }
        public string PelecardResult
        {
            get
            {
                if (DebugMode && !DebugModeMoveToPelecard)
                {
                    return Request.QueryString["result"] ?? "";
                }
                else
                {
                    return Request.Form["result"] ?? "";
                }
            }
        }
        public string PaymentToken { get { return Request.Form["token"]; } }
        public string PaymentTransation { get { return Request.Form["authNum"]; } }

        #endregion

        public string PelecardStatusCode { get { return PelecardResult.Substring(0, 3); } }
        public string WhiteLabelEmail { get { return WhiteLabelObj.Email_UI; } }
        private Orders _order;
        public Orders Order
        {
            get
            {
                if (_order==null)
                {
                    var _wl = GetWhiteLabelDocID();
                    LoggerManagerProject.LoggerManager.Logger.WriteInfo("Pelecard BadURL. Load Order From DB. orderToken: " + OrderToken + " WL:"+_wl);
                    _order = OrdersContainer.SelectByKeysView_WhiteLabelDocId_OrderToken(_wl, OrderToken, true).Single;
                    if (_order==null)
                    {
                        LoggerManagerProject.LoggerManager.Logger.WriteWarrning("Pelecard BadURL. Load Order From DB Failed!. orderToken: " + OrderToken + " WL:" + _wl);
                    }
                }
                // 14/04/15 - Yarin , Improve Processing...
                //Orders oOrder = OrdersContainer.SelectByKeysView_WhiteLabelDocId_OrderToken(GetWhiteLabelDocID(), OrderToken, true).Single;
                return _order;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoggerManagerProject.LoggerManager.Logger.WriteInfo("Pelecard Payment Status Failed. Arrived to BadURL ");

                if (!string.IsNullOrEmpty(PelecardStatusCode))
                {
                    LoggerManagerProject.LoggerManager.Logger.WriteInfo("Pelecard Payment Status Code: " + PelecardStatusCode);
                    initPage();
                }
                else
                {
                    //missing pelecardResult
                    LoggerManagerProject.LoggerManager.Logger.WriteWarrning("Pelecard Payment Status Failed - missing palecard Error Status code. OrderToken:"+OrderToken);
                }
            }
            else
            {
                //this page already loaded
            }
        }

        private void initPage()
        {
            int mailSendOptions = -1;

            string strMailContentForSupport = "";
            string strMailSubjectForSupport = "";
            string strMailContentForClient = "";
            string strMailSubjectForClient = "";
            string clientEmail = "";

            try
            {
                List<string> lOrderRemarks = new List<string>();
                lOrderRemarks.Add("Admin Remarks:");

                //Update :
                //Set Payment Status
                //Set Payment Transation number from pelecard
                LoggerManagerProject.LoggerManager.Logger.WriteInfo("Pelecard BadURL. InitPage. OrderToken:" + OrderToken + " PalecardReault:" + PelecardResult);

                if (Order != null)
                {
                    #region Set Email params
                    clientEmail = Order.ContactDetails.Email_UI;
                    strMailContentForSupport = GetText(MailStrings.strBookingFailed) + Order.DocId_UI;
                    strMailSubjectForSupport = GetText(MailStrings.strSubjectBookingFailed) + Order.DocId_UI;
                    strMailSubjectForClient = GetText(MailStrings.strSubjectBookingOnErrorClient);
                    strMailContentForClient = GetText(MailStrings.strBookingFailedClient);
                    mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndClient;
                    #endregion

                    #region Update order
                    lOrderRemarks.Add("pelecard fail Authentication Number: " + PaymentTransation);
                    Order.AdminRemarks = string.Format("{0}", string.Join(Environment.NewLine, lOrderRemarks)); ;
                    //Check with dudu if recived or need to check if it's J5 ,add field option to Enum framCatcher
                    Order.StatusPayment = (int)EnumHandler.OrderPaymentStatus.Failed;
                    Order.OrderStatus = (int)EnumHandler.OrderStatus.Failed;
                    Order.Action(DL_Generic.DB_Actions.Update);
                    #endregion
                }
                else
                {
                    #region Fatal Error - Pelecard Payment Status Failed - Cannot find order token
                    //Error - oOrderObjectFromXML is null or oOrderObjectFromXML.oYayaRoom or oOrderObjectFromXML.oYayaRoom.SupplierOriginalHotelXML is empty.
                    strMailContentForSupport = string.Format("Pelecard Payment Status Failed - Cannot find order token. please check logs.");
                    strMailSubjectForSupport = string.Format("Pelecard Payment Status Failed - Cannot find order token");
                    mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                    #endregion

                    //Cannot find token order
                    LoggerManagerProject.LoggerManager.Logger.WriteWarrning("Pelecard Payment Status Failed - Cannot find order token ");
                }
            }
            catch (Exception ex)
            {

                strMailContentForSupport = string.Format("{0} {1} XML Render Faild {1} {2}", strMailContentForSupport, Environment.NewLine, ex.Message);
                mailSendOptions = (int)EnumHandler.MailSendOptions.SupportAndAlgoma;
                LoggerManagerProject.LoggerManager.Logger.WriteError("Error - Exeption:BadUrl - ", ex);
            }
            finally
            {
                SendOrderMail(strMailContentForClient, strMailSubjectForClient, clientEmail, strMailContentForSupport, strMailSubjectForSupport, mailSendOptions);
            }
        }

        #region Page Functions

        /// <summary>
        /// Create (Full) Order Object From Xml
        /// </summary>        
        private object createOrderObjectFromXml(string fileName, Type type)
        {
            XmlSerializer xmlFormat = new XmlSerializer(type);

            using (Stream fStream = File.OpenRead(fileName))
            {
                var fromDisk = xmlFormat.Deserialize(fStream);
                return fromDisk;
            }

            return null;
        }

        #endregion
    }
}