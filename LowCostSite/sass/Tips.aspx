﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="Tips.aspx.cs" Inherits="LowCostSite.sass.Tips" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="content-page">

            <div class="line-title"></div>
            <div class="highlight rtl display_inline_block full-width">

                <div class="box-wrap-content">
                    <div class="box-wrap-title"><%=GetText("EshetLowCostTips") %></div>
                    <div class="tips-box">
                        <asp:Panel ID="pnl_ContentText" runat="server">
                            <div class="highlight rtl">
                                <div>
                                    <asp:PlaceHolder ID="ph_StaticText" runat="server"></asp:PlaceHolder>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Repeater ID="drp_Tips" runat="server" ItemType="DL_LowCost.Tip">
                            <ItemTemplate>
                                <div class="highlight rtl ">
                                    <div>
                                        <h4 class="title_content" runat="server" id="lbl_Title"><%# Eval("Title_UI") %>
                                            <span class="left" runat="server" id="lbl_Date" style="display:none;"><%# Eval("Date_UI") %></span>
                                        </h4>
                                        <div runat="server" id="div_TipContent">
                                            <%# Eval("LongText_UI") %>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
