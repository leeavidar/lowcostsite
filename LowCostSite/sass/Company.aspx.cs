﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LowCostSite.sass
{

    public partial class Company : BasePage_UI
    {
        #region Session Private fields

        private CompanyPage _oSessionCompanyPage;

        #endregion

        #region URL Get

        public int QueryStringID
        {
            get
            {
                return ConvertToValue.ConvertToInt(QueryStringManager.QueryString(EnumHandler.QueryStrings.ID));
            }
        }

        public string QueryStingURL
        {
            get
            {
                return QueryStringManager.QueryString(EnumHandler.QueryStrings.URL);
            }
        }

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<CompanyPage>(out _oSessionCompanyPage) == false)
            {
                //take the data from the table in the database
                _oSessionCompanyPage = GetCompanyPageFromDB();
            }
            else { /*Return from session*/}

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<CompanyPage>(oSessionCompanyPage);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            Generic.SessionManager.ClearSession<CompanyPage>(oSessionCompanyPage);
        }

        /// <summary>
        /// A method that gets a company page from the database
        /// </summary>
        /// <returns></returns>
        private CompanyPage GetCompanyPageFromDB()
        {
            CompanyPageContainer oCompanyPageContainer = null;
            if (QueryStringID > 0)
            {
                oCompanyPageContainer = CompanyPageContainer.SelectByID(QueryStringID, BasePage_UI.GetWhiteLabelDocID(), null); 
                if (oCompanyPageContainer.Single != null)
                {
                    if (oCompanyPageContainer.Single.SeoSingle != null && oCompanyPageContainer.Single.SeoSingle.FriendlyUrl_Value != null)
                    {
                        if (oCompanyPageContainer.Single.SeoSingle.FriendlyUrl_Value.Length > 0)
                        {
                            Response.RedirectPermanent(SetLinkHandler.SetLink(EnumHandler.LinkPages.Companies, EnumHandler.QueryStrings.URL, oCompanyPageContainer.Single.SeoSingle.FriendlyUrl_Value, oCompanyPageContainer.Single.DocId_UI));
                        }
                    }
                }
            }
            else
            {
                if (QueryStingURL != "" && QueryStingURL != null)
                {
                    oCompanyPageContainer = CompanyPageContainer.SelectAllCompanyPages(1, null).FindAllContainer(page => page.SeoSingle != null && page.SeoSingle.FriendlyUrl_Value == QueryStingURL);  
                    if (oCompanyPageContainer.Single == null && ConvertToValue.ConvertToInt(QueryStingURL) > 0)
                    {
                        oCompanyPageContainer = CompanyPageContainer.SelectByID(ConvertToValue.ConvertToInt(QueryStingURL), BasePage_UI.GetWhiteLabelDocID(), null);
                        if (oCompanyPageContainer.Single != null)
                        {
                            if (oCompanyPageContainer.Single.SeoSingle.FriendlyUrl_UI != "" && oCompanyPageContainer.Single.SeoSingle.FriendlyUrl_UI != null)
                            {
                                Response.RedirectPermanent(SetLinkHandler.SetLink(EnumHandler.LinkPages.Companies, EnumHandler.QueryStrings.URL, oCompanyPageContainer.Single.SeoSingle.FriendlyUrl_UI, oCompanyPageContainer.Single.DocId_UI));
                            }
                        }
                        else
                        {
                            Response.Redirect(StaticStrings.path_404);
                        }
                    }
                }
                else
                {
                    Response.Redirect(StaticStrings.path_404);
                }
            }

            if (oCompanyPageContainer.Single != null)
            {
                return oCompanyPageContainer.Single;
            }
            else
            {
                return null;
            }
        }


        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            //CallPageScript();
        }

        #endregion


    }


    public partial class Company : BasePage_UI
    {

        #region Properties

        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public CompanyPage oSessionCompanyPage { get { return _oSessionCompanyPage; } set { _oSessionCompanyPage = value; } }

        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion

        #endregion



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.SetMenuClass(EnumHandler.MenuPages.Airlines);
            }
           
            SetPage();
        }

        private void SetPage()
        {
            // set the page meta data with the data in the seo property.
            Master.SetMetaData(oSessionCompanyPage.SeoSingle);

            // set the search filter (to automatically filter the results to the current airline
            if (oSessionCompanyPage.AirlineSingle != null)
            {
                uc_Search.AirlineFilter = oSessionCompanyPage.AirlineSingle.IataCode_UI;
            }
            else
            {
                uc_Search.AirlineFilter = "";
            }
                    
    
            // set the labels
            lbl_Name.Text = oSessionCompanyPage.Name_UI;
            lbl_Title.Text = oSessionCompanyPage.Title_UI;
            lbl_Description.InnerHtml = oSessionCompanyPage.Text_UI;
            // image
            ImagesContainer oImagesContainer = oSessionCompanyPage.Imagess;

            if (oImagesContainer != null && oImagesContainer.Count > 0)
            {
                // Set the small logo image
                Images logoImage = oSessionCompanyPage.Imagess.FindFirstOrDefault(img => img.ImageType_Value == EnumHandler.ImageTypes.Logo.ToString());
                if (logoImage!=null)
                {
                    lbl_ImageTitle.Text = logoImage.Title_UI;
                    string imagePath = string.Format("{0}/{1}", StaticStrings.path_Uploads, logoImage.ImageFileName_UI);
                    img_Logo.ImageUrl = imagePath;
                }

                // Setting the cover image:

                // 1. Get the image
                Images coverImage = oSessionCompanyPage.Imagess.FindFirstOrDefault(img => img.ImageType_Value == EnumHandler.ImageTypes.Cover.ToString());
                // 2. If the image exist, set it as the cover image
                if (coverImage != null)
                {
                    lbl_ImageTitle.Text = coverImage.Title_UI;
                    string imagePath = string.Format("{0}/{1}", StaticStrings.path_Uploads, coverImage.ImageFileName_UI);
                    img_Cover.ImageUrl = imagePath;
                }
            }

            // Set the content templates
            SetTemplates();
        }

        #region Event Handlers


        #endregion

        #region Helping Methods

        /// <summary>
        /// A method that set the templates of the destination page in 2 columns, by the order field of each column.
        /// </summary>
        private void SetTemplates()
        {
            TemplateContainer oTemplateContainer = oSessionCompanyPage.Templates.FindAllContainer(template => template.Type == EnumHandler.TemplateType.MainInformation.ToString() || template.Type == EnumHandler.TemplateType.CenterWithImage.ToString());

            // Ordering the templates for each column by their order
            oTemplateContainer = oTemplateContainer.SortByKeyContainer(KeyValuesType.KeyTemplateOrder, false);
          
            // Adding the templates to the place holder:
            foreach (Template oTemplate in oTemplateContainer.DataSource)
            {
                if (oTemplate.Type_Value == EnumHandler.TemplateType.MainInformation.ToString())
                {
                    UC_MainInformation oUC_MainInformation = (UC_MainInformation)LoadControl(StaticStrings.path_UC_MainInformation);
                    oUC_MainInformation.oTemplate = oTemplate;
                    ph_Templates.Controls.Add(oUC_MainInformation);
                }
            }
        }

        #endregion
    }
}