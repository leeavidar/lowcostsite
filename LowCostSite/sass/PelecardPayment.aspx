﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PelecardPayment.aspx.cs" Inherits="LowCostSite.sass.PelecardPayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form4" name="form4" action="<%=PelecardInterface.PelecardRedirectTrans.DestinationUrl %>" method="post">
        <%
            PelecardInterface.PelecardRedirectTrans oPaymentPelecard = GetPaymentForm();
            if (oPaymentPelecard == null)
            {
                Response.Write("<h2 style='text-align:center'>סוג מטבע לא נתמך</h2>");
                //Some problem with pelecard object - null
                LoggerManagerProject.LoggerManager.Logger.WriteWarrning("PelecardPayment.aspx - oPaymentPelecard Is Null.");
                return;
            }
            else
            {
                string postData = oPaymentPelecard.GetPostData();
                // Create a request using a URL that can receive a post. 
                System.Net.WebRequest request = System.Net.WebRequest.Create(PelecardInterface.PelecardRedirectTrans.DestinationUrl);
                // Set the Method property of the request to POST.
                request.Method = "POST";
                // Create POST data and convert it to a byte array.
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.
                request.ContentType = "application/x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;
                // Get the request stream.
                System.IO.Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
                // Get the response.
                System.Net.WebResponse response = request.GetResponse();
                // Display the status.
                Response.Write(((System.Net.HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                Response.Write(responseFromServer);
                // Clean up the streams.
                reader.Close();
                dataStream.Close();
                response.Close();
            }
        %>
        <input type="hidden" name="noCheck" value="true" id="noCheck" />
    </form>
    <%
        Response.Write("<script type='text/javascript'>");
        Response.Write("function submitForm()");
        Response.Write("{");
        Response.Write("document.form4.submit();");
        Response.Write("}");
        Response.Write("submitForm();");
        Response.Write("</script>");
    %>
</body>
</html>
