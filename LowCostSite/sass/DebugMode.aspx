﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DebugMode.aspx.cs" Inherits="LowCostSite.sass.DebugMode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>debug mode</title>
</head>
<body class="iframeBody">
    <form runat="server"> 
        <div>
            paymentType: 
            <asp:Label ID="lbl_paymentType" runat="server"></asp:Label>
        </div>
        <div>
            payment status:
            <asp:DropDownList runat="server" ID="ddl_paymentMethode"></asp:DropDownList>
        </div>
        <div>
            order status:
            <asp:DropDownList runat="server" ID="ddl_OrderMethode" ></asp:DropDownList>
        </div>
        <div>
            reservation status:
            <asp:DropDownList ID="ddl_reservation" runat="server"></asp:DropDownList>
        </div>
        <asp:Button ID="btn_continue" runat="server" Text="choose" OnClick="Btn_Continue_Click" />
    </form>

</body>
</html>

