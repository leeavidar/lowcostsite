﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="FlightsResults.aspx.cs" Inherits="LowCostSite.sass.FlightsResults" %>

<%@ Register Src="~/sass/Controls/UC_Loading.ascx" TagPrefix="uc1" TagName="UC_Loading" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script src="javascripts/PageScripts/FlightsResults.js"></script>
    <script src="javascripts/PageScripts/Search.js"></script>
    <script type="text/javascript">
        hf_AutoCompleteOriginCode = '<%=hf_AutoCompleteOriginCode.ClientID %>';
        hf_AutoCompleteDestinationCode = '<%=hf_AutoCompleteDestinationCode.ClientID %>';
        hf_DefultTlvText = '<%=hf_DefultTlvText.ClientID%>';
        var hf_IsNewSearchMadeId = '<%=hf_IsNewSeachMade.ClientID %>';
        // Set the hidden fields for max and min values of the slider (to be used in on server side- for the filter to know what prices are selected).
        function GetSliderPrices() {
            var values = $("#<%=txt_PriceSlider.ClientID%>").data('slider').getValue();
            $("#<%=hf_MinPrice.ClientID%>").val(values[0]);
            $("#<%=hf_MaxPrice.ClientID%>").val(values[1]);
        };
        $(function () { $("[data-toggle='tooltip']").tooltip(); });

        function UnfilterResults2() {
            //location.reload();
            $('.cb_airline').prop('checked', true);
            // Filter the results
            var min = $('input.slider').attr('data-slider-min');
            var max = $('input.slider').attr('data-slider-max');
            $('input.slider').slider("value", 0,min);
            $('input.slider').slider("value", 1, max);
            $('.btn_filter').click();
           
            return false;
        }
    </script>
    <script src="/sass/javascripts/LocationAutocomplete.js"></script>
    <link href="/sass/Autocomplete.css" rel="stylesheet" />
    <link href="../assets/ExtraStyling.css" rel="stylesheet" />
    <style>
        .collapse-all {
            cursor: pointer;
            background-color: #428bca;
            color: white;
            margin: 5px;
            padding: 3px;
        }
    </style>
    <style>
    li {
    list-style:none;
    }
    .allRow {
margin-bottom: 10px;
height: 30px;
border-bottom: 1px solid #ddd;
}
    .border-list {
    border-bottom: 0px solid #ddd;
    }
    .row-info-list {
margin-bottom: 15px;
border-bottom: 1px solid #ddd;
padding-bottom: 10px;
}
    .list-no-border {
        border-bottom: 0px solid #ddd !important;
        margin-bottom: 0px !important;
        padding-bottom: 0px !important;
    }
    .showMoreFlight {
    border-bottom:0px;
    }
    .list-result {

        padding-right: 24px;
    }
    .desinations-row {
        width: 375px;
    }
    .dest {
        width: 165px;
    }
    .image-space {
        margin-left: 5px;    
    }
    .highlight .title_content {
        font-size: 16px;

    }
</style>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <span id="hiddenFields">
        <!-- Store the codes for origin and destination (fills when clicking the "order" button of each group) -->
        <asp:HiddenField ID="hf_AutoCompleteOriginCode" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hf_AutoCompleteDestinationCode" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hf_DefultTlvText" runat="server" ClientIDMode="Static" />
        <!-- Stores the ids of the outward and return flights which are selected in the selected group (when the "order" button is clicked) -->
        <asp:HiddenField ID="hdn_OutwardFlightId" runat="server" Value="" />
        <asp:HiddenField ID="hdn_ReturnFlightId" runat="server" />
        <!-- Stores the maximum and minimum prices (used in the price filter)-->
        <asp:HiddenField ID="hf_MaxPrice" runat="server" />
        <asp:HiddenField ID="hf_MinPrice" runat="server" />
        <!-- The number of groups that are displayed on the page (for the load-on-scroll) -->
        <asp:HiddenField runat="server" ID="hf_FlightsCount" />
        <!--  Order button clicked -->
        <asp:HiddenField runat="server" ID="hf_IsOrderClicked" Value="false" />
        <!--  New search was made -->
        <asp:HiddenField runat="server" ID="hf_IsNewSeachMade" Value="false" />

        <script>
            var IsOrderClickedId = '<%=hf_IsOrderClicked.ClientID%>'; 
        </script>
    </span>
    <div class="container">
        <div class="content-page">
            <div class="line-title"></div>
            <div class="row">
                <div class="col-md-3">
                    <!-- START: SIDE MENU -->
                    <!-- START - FLIGHT DESCRIPTION BOX -->
                    <asp:UpdatePanel ID="up_FlightDescriptionBox" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="searchDescriptionsDiv" class="flight-info-order full-width box-padding" runat="server">
                                <div class="row-space text-right font-large rtl">
                                    <%-- טיסה  >  תל אביב  >  ניו יורק--%>
                                    <%=GetText("Flight") %> >
                                    <!-- From-->
                                    <asp:Label ID="lbl_FlightOriginSmall" runat="server" />
                                    > 
                                    <!-- To-->
                                    <asp:Label ID="lbl_FlightDestSmall" runat="server" />
                                </div>
                                <div class="row-space ">
                                    <asp:Label ID="lbl_OutwardDateSmall" runat="server" />
                                    <span class="image-space">
                                        <img src="images/out.png" /></span>
                                </div>
                                <div class="row-space" id="searchDescriptionsDiv_Return_Div" runat="server">
                                    <asp:Label ID="lbl_ReturnDateSmall" runat="server" />
                                    <span class="image-space">
                                        <img src="images/in.png" /></span>
                                </div>
                            </div>
                            <!-- A div that is displayed when there aer no search results, instead of the flight description -->
                            <div id="noSearchDiv" class=" flight-info-order full-width box-padding" runat="server">
                                <div class="row-space text-right font-large">
                                    <strong><%=GetText("PerformSearch") %>
                                    </strong>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btn_Search" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <!-- END -  FLIGHT DESCRIPTION BOX -->

                    <!-- START - SIDE MENU SEARCH -->
                    <asp:UpdatePanel ID="up_SideMenu" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="filter-title text-space text-right" onclick='$(".searchPanel").slideToggle();'>
                                <%=GetText("ChangeFlightDetails") %>
                                <span class="arrow-down-filter">
                                    <img src="images/arroe-down-blue.PNG" />
                                </span>
                            </div>
                            <div id="searchPanel" runat="server" class="searchPanel highlight rtl display_inline_block full-width no-margin-bottom">
                                <div class="bs-example" role="form">
                                    <div class="form-group">
                                        <label><%=GetText("DepartureFrom") %>:</label>
                                        <asp:TextBox ID="txt_Origin" CssClass="txt_Origin form-control Autocomplete" data-place="origin" runat="server" placeholder="gtxt_DepartureAt" autocomplete="off" />
                                        <div class="wrap-autocomplete autocomplete-origin autocomplete-box hidden" data-place="origin">
                                            <!-- Autocomplete results are displayed here -->
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label><%=GetText("ArrivalTo") %>:</label>
                                        <asp:TextBox ID="txt_Destination" CssClass="txt_Destination Autocomplete form-control" runat="server" data-place="destination" placeholder="gtxt_ArrivalTo" autocomplete="off" />
                                        <div class="wrap-autocomplete autocomplete-destination autocomplete-box hidden" data-place="destination">
                                            <!-- autocomplete results are displayed here -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label><%=GetText("DepartureDate")%>:</label>
                                        <asp:TextBox ID="txt_OutwardDate" CssClass="date txt_OutwardDate form-control myDatepicker" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" runat="server" placeholder="gtxt_DepartureDate" onkeypress="return false;" />
                                        <%--name="outwardDate" --%>
                                    </div>
                                    <div class="form-group">
                                        <label><%=GetText("ReturnDate")%>:</label>
                                        <asp:TextBox ID="txt_ReturnDate" CssClass="date txt_ReturnDate form-control myDatepicker" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" runat="server" placeholder="gtxt_ReturnDate" onkeypress="return false;" />
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4">
                                            <label><%=GetText("Adult")%>:</label>
                                            <asp:DropDownList ID="ddl_Adults" CssClass="form-control form-control-padding" runat="server" />

                                        </div>
                                        <div class="col-md-4">
                                            <label id="tt_child" data-toggle="tooltip" data-placement="top" title="2-12"><%=GetText("Child")%>:</label>
                                            <asp:DropDownList ID="ddl_Children" CssClass="form-control form-control-padding" runat="server" />
                                        </div>
                                        <div class="col-md-4">
                                            <label data-toggle="tooltip" data-placement="top" title="0-2"><%=GetText("Infant")%>:</label>
                                            <asp:DropDownList ID="ddl_Infants" CssClass="form-control form-control-padding" runat="server" />
                                        </div>

                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <label class="control-label">
                                                <span class="checker "></span>
                                                <asp:CheckBox ID="cb_PreferDirectFlights" runat="server" CssClass="cb-lables" Text="gtxt_PreferdirectFlights" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- the data-backdrop="static" makes it impossible to close the loading window by clicking on the side while it is still loading. // data-toggle="modal" data-target="#myModal" data-backdrop="static" -->
                                        <asp:Button ID="btn_Search" UseSubmitBehavior="true" CssClass="btn btn-default btn-block btn-primary btn_Search" runat="server" Text="gtxt_Search" OnClientClick="return OnSearchClick()" OnClick="btn_Search_Click" data-backdrop="static" /><%-- ViewStateMode="Disabled" --%>
                                    </div>
                                </div>
                            </div>
                            <!-- START: FILTERS-->
                            <div id="filterPanel" runat="server">
                                <div class="filter-title text-space text-right">
                                    <%=GetText("FilterResults") %>
                                    <span class="arrow-down-filter">
                                        <img src="images/arroe-down-blue.PNG" />
                                    </span>
                                </div>
                                <div class="highlight rtl display_inline_block full-width no-margin-bottom">
                                    <div class="filter-title-inner"><%=GetText("_stops")%></div>
                                    <asp:RadioButtonList ID="rb_Stops" CssClass="rb-group" runat="server">
                                        <asp:ListItem Text='gtxt_DirectFlightsOnly' Value="0"></asp:ListItem>
                                        <asp:ListItem Text="gtxt_OneStopMax" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="TwoStopsAndMore" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <div class="filter-title-inner"><%=GetText("Price")%></div>
                                    <!-- PRICE FILTER - SLIDER -->
                                    <input type="text" id="txt_PriceSlider" runat="server" class="slider slider-horizontal" data-slider-step="5" />
                                    <div class="filter-title-inner"><%=GetText("Airlines")%></div>
                                    <!-- A Place Holder for the Airline filters -->
                                    <asp:PlaceHolder ID="ph_AirlineFilter" runat="server"></asp:PlaceHolder>
                                </div>
                                <asp:Button ID="btn_Filter" CssClass="btn btn-default btn-block btn-primary btn_filter" runat="server" Text="gtxt_FilterResults" OnClientClick="GetSliderPrices()" OnClick="btn_Filter_Click" />
                            </div>
                            <!-- END: FILTERS-->

                            <!-- END - SIDE MENU CHANGE SEARCH -->
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <!-- END: SIDE MENU -->
                    <!-- Modal for loading -->
                    <uc1:UC_Loading runat="server" ID="uc_LoadingControl" />

                    <asp:UpdatePanel ID="up_Reults" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:HiddenField runat="server" ID="hf_FlightsDisplayedCount" />
                            <!-- search hash code (to avoid conflicts between pages in the same session) -->
                            <asp:HiddenField runat="server" ID="hf_SearchHashCode" />
                            <div id="div_Results" runat="server" class="col-md-9">
                                <div class="highlight rtl display_inline_block full-width ">
                                    <%--box-padding--%>
                                    <b>
                                        <asp:Label ID="lbl_ResultsCounter" runat="server" />
                                    </b>
                                    <asp:Button ID="btn_UnFiltered"  runat="server" Text='gtxt_ShowAllResults' Visible="false" CssClass="btn btn-default" OnClientClick="return UnfilterResults2()"/>

                                    <span onclick='CollapseAll();' class="font-bold pull-left collapse-all"><%=GetText("CollapseAll")%></span>

                                    <!-- Place holder for all the flight groups -->
                                    <asp:PlaceHolder ID="ph_FlightGroups" runat="server"></asp:PlaceHolder>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btn_Search" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <!-- END - Menues  -->
    </div>
</asp:Content>
