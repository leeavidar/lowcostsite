﻿using DL_Generic;
using DL_LowCost;
using Generic;
using LowCostSite.sass;
//using SharedDataObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LowCostSite.sass
{
    public partial class DebugMode : BasePage_UI
    {
        #region Session Props
        private OrderObject _oSessionOrderObject;
        #endregion

        #region Session Methods
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            if (!SessionManager.GetSession(out _oSessionOrderObject))
            {
                // If we can't find the order object from the session
                try
                {
                    DateTime dateCreated = ConvertToValue.ConvertToDateTime(Request.QueryString["dateCreated"]);
                    string DocId = Request.QueryString["orderDocId"];
                    // get the object from file:
                    _oSessionOrderObject = (OrderObject)createOrderObjectFromXml(BasePage_UI.GetXMLDirByDateAndOrderId(dateCreated, DocId) + "\\XMLOrder_" + DocId + ".xml", typeof(OrderObject));
                }
                catch (Exception e)
                {
                    // Can't get the file path from the query string
                    LoggerManagerProject.LoggerManager.Logger.WriteError("DebugMode.aspx:  Can't get the order details from the query string to build the order object. Error Message: "+ e.Message);
                  // Redirect to error page (outside of the iframe
                    // AddScript("window.parent.location.href='" + StaticStrings.path_ErrorPage);
                   Response.Redirect(StaticStrings.path_ErrorIFrame);
                }
            }
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Session["OrderObject"] = oSessionOrderObject;
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        private void ResetAllSession()
        {
            //No need to delete order session - this session will be deleted after payment
        }
        #endregion

        #region Events
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //  ResetAllSession();
            }
            LoadFromSession(IsPostBack);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            //CallPageScript();
        }
        #endregion

        internal override void ResetAllSessions()
        {
            throw new NotImplementedException();
        }
    }
    public partial class DebugMode : BasePage_UI
    {
        #region Properties
        public OrderObject oSessionOrderObject { get { return _oSessionOrderObject; } set { _oSessionOrderObject = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (oSessionOrderObject == null)
            {
                //Cannot find order session - return to home page if the seesion of the order is null
                Response.Redirect("/Index.aspx");
            }
            else
            {
                //Have active ordersession - set iframe
                lbl_paymentType.Text = "J5";//oSessionOrderObject.OrderPaymentTypeName;

                if (!IsPostBack)
                {
                    ListItem li1 = new ListItem("success", "0");
                    ListItem li2 = new ListItem("failed", "1");
                    ddl_paymentMethode.Items.Insert(0, li1);
                    ddl_paymentMethode.Items.Insert(1, li2);

                    //ddl_OrderMethode.DataSource = Generic.EnumUtil.GetEnumToListItems<EORDER_STATUS>();
                    //ddl_OrderMethode.DataValueField = "Value";
                    //ddl_OrderMethode.DataTextField = "Text";
                    //ddl_OrderMethode.DataBind();

                    //ddl_reservation.DataSource = Generic.EnumUtil.GetEnumToListItems<EReservationStatus>();
                    //ddl_reservation.DataValueField = "Value";
                    //ddl_reservation.DataTextField = "Text";
                    //ddl_reservation.DataBind();
                }
            }
        }

        protected void Btn_Continue_Click(object sender, EventArgs e)
        {
            int paymentMethode = ConvertToValue.ConvertToInt(ddl_paymentMethode.SelectedValue);

            switch (paymentMethode)
            {
                case 0://success
                    Response.Redirect("GoodURL.aspx?result='000'&orderStatus=" + ddl_OrderMethode.SelectedValue + "&reservationStatus=" + ddl_reservation.SelectedValue + "&OrderToken=" + oSessionOrderObject.OrderToken);
                    break;
                case 1://failed
                    Response.Redirect("BadURL.aspx?result='000'" + "&OrderToken=" + oSessionOrderObject.OrderToken);
                    break;
            }
        }

        /// <summary>
        /// Create (Full) Order Object From Xml
        /// </summary>        
        private object createOrderObjectFromXml(string fileName, Type type)
        {
            OrderObject obj = BinaryHandler.DeserializeBinary<OrderObject>(fileName);
            return obj;
            #region OLD - with xml serialization
            #endregion
        }
    }
}