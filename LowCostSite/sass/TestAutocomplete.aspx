﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="TestAutocomplete.aspx.cs" Inherits="LowCostSite.sass.TestAutocomplete" %>

<%@ Register Src="~/sass/Controls/Autocomplete/UC_Autocomplete.ascx" TagPrefix="uc1" TagName="UC_Autocomplete" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script src="javascripts/LocationAutocomplete.js"></script>
    <link href="../../assets/ExtraStyling.css" rel="stylesheet" />
    <link href="Autocomplete.css" rel="stylesheet" />
    <script>
        hf_AutoCompleteOriginCode = '<%=hf_Origin.ClientID %>';
        hf_AutoCompleteDestinationCode = '<%=hf_Destination.ClientID %>';
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hf_Origin" runat="server" />
    <asp:HiddenField ID="hf_Destination" runat="server" />

    <div class="row rtl bg_search_box  box-padding">
        <div class="col-sm-12 box-search-center">
            <div class="col-sm-2">
                <div class="form-group">
                    <asp:TextBox ID="txt_Origin" CssClass="Autocomplete txt_Origin form-control " data-place="origin" runat="server" placeholder="טיסת הלוך" />
                    <div class="wrap-autocomplete autocomplete-origin autocomplete-box hidden" data-place="origin">
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <asp:TextBox ID="txt_Destination" CssClass="Autocomplete txt_Destination form-control " data-place="destination" runat="server" placeholder="טיסת חזור" />
                    <div class="wrap-autocomplete autocomplete-destination autocomplete-box hidden" data-place="destination">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

