﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="Policies.aspx.cs" Inherits="LowCostSite.sass.Policies" %>
<%@ MasterType VirtualPath="~/sass/Master/MasterPage.master" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="content-page">

            <div class="line-title"></div>
            <div class="highlight rtl display_inline_block full-width">

                <div class="box-wrap-content">
                    <div class="box-wrap-title">תקנון</div>

                    <div class="tips-box">
                        <div class="highlight rtl ">
                            <!-- Content for about us page will be placed here (from the DB)-->
                            <div runat="server" id="div_policies">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
