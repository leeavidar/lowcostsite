﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using Generic;
using DL_LowCost;

namespace LowCostSite.sass
{
    public partial class Faq : BasePage_UI
    {
        #region Session Private fields

        private FaqContainer _oSessionFaqPages;
        private StaticPagesContainer _oSessionStaticPages;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<FaqContainer>(out _oSessionFaqPages) == false)
            {
                //take the data from the table in the database
                _oSessionFaqPages = GetFaqsFromDB();
            }
            else { /*Return from session*/}
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<StaticPagesContainer>(out _oSessionStaticPages) == false)
            {
                //take the data from the table in the database
                _oSessionStaticPages = GetStaticPagesFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<FaqContainer>(oSessionFaqPage);
            Generic.SessionManager.SetSession<StaticPagesContainer>(oSessionStaticPages);

        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void ResetAllSessions()
        {
            Generic.SessionManager.ClearSession<FaqContainer>(oSessionFaqPage);
            Generic.SessionManager.ClearSession<StaticPagesContainer>(oSessionStaticPages);

        }


        /// <summary>
        /// A method that gets the active tip from the database. Only 1 tip should be set as active by the administrator.
        /// </summary>
        /// <returns></returns>
        private FaqContainer GetFaqsFromDB()
        {
            FaqContainer allFaqs = FaqContainer.SelectAllFaqs(BasePage_UI.GetWhiteLabelDocID(), null);
            if (allFaqs != null && allFaqs.Count > 0)
            {
                return allFaqs;
            }
            return null;
        }

        private StaticPagesContainer GetStaticPagesFromDB()
        {
            //selecting all the static pages with a given white label
            return StaticPagesContainer.SelectAllStaticPagess(BasePage_UI.GetWhiteLabelDocID(), null);
        }
        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        /// <summary>
        /// update the sessions when page load completes 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
        }

        #endregion


    }

    public partial class Faq : BasePage_UI
    {

        #region Properties

        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public FaqContainer oSessionFaqPage { get { return _oSessionFaqPages; } set { _oSessionFaqPages = value; } }
        public StaticPagesContainer oSessionStaticPages { get { return _oSessionStaticPages; } set { _oSessionStaticPages = value; } }
        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetPage();
            }

        }


        /// <summary>
        /// A method that sets all the page elements with data from the DB
        /// </summary>
        private void SetPage()
        {
            Master.SetMenuClass(EnumHandler.MenuPages.Faq);

            if (oSessionStaticPages != null)
            {
                StaticPages oStaticPages = _oSessionStaticPages.FindFirstOrDefault(page => page.Name == "Faq");
                if (oStaticPages != null)
                {
                    Master.SetMetaData(oStaticPages.SeoSingle);
                    if (string.IsNullOrEmpty(oStaticPages.Text_UI))
                    {
                        pnl_ContentText.Visible = false;
                    }
                    else
                    {
                        ph_StaticText.Controls.Add(new LiteralControl(oStaticPages.Text_UI));
                    }
                }
                else
                {
                    pnl_ContentText.Visible = false;
                }
            }
            // Set the faq page in the menu as the active page
            Master.SetMenuClass(EnumHandler.MenuPages.Faq);
            // bind the repeater to the q&a from the database
            if (oSessionFaqPage != null)
            {
                drp_FAQ.DataSource = oSessionFaqPage.SortByKeyContainer(DL_Generic.KeyValuesType.KeyFaqOrder, false).DataSource;
                drp_FAQ.DataBind();
            }

        }
    }
}