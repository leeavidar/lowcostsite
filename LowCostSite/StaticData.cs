﻿using BL_LowCost;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace LowCostSite
{
    public class StaticData
    {
        #region Properties
        //A property holding a collection of all the airports (userd by the autocomplete)
        public static List<Airport> oAirportsList { get; set; }
        //A property holding a json string representation of all the airports
        public static string JsonAirports { get; set; }
        //A property holding a collection of all the cities (userd by the autocomplete)
        public static List<City> oCityList { get; set; }
        //A property holding a json string representation of all the cities 
        public static string JsonCities{ get; set; }

        public static List<SimpleCity> CitiesListSimple { get; set; }
        public static List<SimpleAirport> AirportsListSimple { get; set; }

        public static List<Airline> oAirlinesList { get; set; }
        public static List<SimpleAirline> AirlinessListSimple { get; set; }
        #endregion

        /// <summary>
        /// A static constructor that filles the properties (containers of Airports and Cities), to be used by the autocomplete in the search engine.
        /// This method is being called in the Global.asax file.
        /// </summary>
        static StaticData()
        {
            InitStaticData();
        }

        public static void InitStaticData()
        {
            StaticData.oAirportsList = GetAirports();
            StaticData.JsonAirports = GetAirportsJson(oAirportsList);
            StaticData.AirportsListSimple = GetSimpleAirports(oAirportsList);

            StaticData.oCityList = GetCities();
            StaticData.JsonCities = GetCitiesJson(oCityList);
            StaticData.CitiesListSimple = GetSimpleCities(oCityList);

            
            StaticData.oAirlinesList = GetAirlines();
            StaticData.AirlinessListSimple = GetSimpleAirlines(oAirlinesList);
        }

        private static List<SimpleCity> GetSimpleCities(List<City> oCityList)
        {
            List<SimpleCity> sCities = new List<SimpleCity>();
            foreach (City city in oCityList)
            {
                
                sCities.Add(new SimpleCity() { IataCode = city.IataCode_UI, Name = city.Name_UI, CountryCode = city.CountryCode_UI });
            }
            return sCities;
        }

        private static List<SimpleAirport> GetSimpleAirports(List<Airport> oAirportsList)
        {
            List<SimpleAirport> sAirporst = new List<SimpleAirport>();
            string name;
            foreach (Airport airport in oAirportsList)
            {
                // if there is a translated name, take it as the name, or else take the english name
                name = airport.NameByLang_UI != "" ? airport.NameByLang_UI : airport.EnglishName_UI;
                sAirporst.Add(new SimpleAirport() { IataCode = airport.IataCode_UI, Name = name, CityIataCode = airport.CityIataCode_UI });
            }
            return sAirporst;
        }

        private static List<SimpleAirline> GetSimpleAirlines(List<Airline> oAirlinesList)
        {
            var sAirlines = new List<SimpleAirline>();
            string name;
            foreach (Airline airline in oAirlinesList)
            {
                // if there is a translated name, take it as the name, or else take the english name
                name = airline.Name_UI;
                sAirlines.Add(new SimpleAirline() { IataCode = airline.IataCode_UI, Name = name});
            }
            return sAirlines;
        }

      

        public static string GetAirportsJson(List<Airport> airportsList)
        {

            List<SimpleAirport> sAirporst = new List<SimpleAirport>();
            string name;
            foreach (Airport airport in airportsList)
            {
                // if there is a translated name, take it as the name, or else take the english name
                name = airport.NameByLang_UI != "" ? airport.NameByLang_UI : airport.EnglishName_UI;
                sAirporst.Add(new SimpleAirport() { IataCode = airport.IataCode_UI, Name = name, CityIataCode = airport.CityIataCode_UI });
            }

            JavaScriptSerializer jSerialize = new JavaScriptSerializer();
            string jsonAirportsString = jSerialize.Serialize(sAirporst);
           return jsonAirportsString;
        }

        public static string GetCitiesJson(List<City> citiesList)
        {
            List<SimpleCity> sCities = new List<SimpleCity>();
            foreach (City city in citiesList)
            {
                sCities.Add(new SimpleCity() { IataCode = city.IataCode_UI, Name = city.Name_UI , CountryCode = city.CountryCode_UI });
            }

            JavaScriptSerializer jSerialize = new JavaScriptSerializer();
            string jsonAirportsString = jSerialize.Serialize(sCities);
            return jsonAirportsString;
        }


        /// <summary>
        /// This method gets a list of all the cities
        /// </summary>
        /// <returns>A list of all the cities</returns>
        public static List<City> GetCities()
        {
            return CityContainer.SelectAllCitys(true).DataSource;
        }
        /// <summary>
        /// This method gets a list of airports
        /// </summary>
        /// <returns>A list of all airports</returns>
        public static List<Airport> GetAirports()
        {
            AirportContainer oAirportContainer = AirportContainer.SelectAllAirports(true);
            return oAirportContainer.DataSource;
        }
        /// <summary>
        /// This method gets a list of airlines.
        /// </summary>
        /// <returns>A list of all airlines</returns>
        public static List<Airline> GetAirlines()
        {
            AirlineContainer oAirlineContainer = AirlineContainer.SelectAllAirlines(true);
            return oAirlineContainer.DataSource;
        }

    }
}

/*
 GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[translator_select_langs]
 AS BEGIN
 select lang_id as Lang_id FROM dbo.Translator 
  group by lang_id
 order by dbo.Translator.lang_id
END
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[translator_select_by_lang]
@prm_lang_id int
 AS BEGIN
 select id as Translate_id,value as Translate_value FROM dbo.Translator 
 where lang_id = @prm_lang_id
  group by id ,value
  order by dbo.Translator.id
END
 */