﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using DL_Generic;
using Generic;
using LowCostSite.FlightsSearchService;
using LowCostSite.sass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BL_LowCost;

namespace LowCostSite
{
    public static class Extensions
    {
        #region For flights results

        /// <summary>
        /// A method that gets the search results from the DB.
        /// </summary>
        /// <returns></returns>
        public static FlightsAvailResults GetSearchResults(SearchParams oSearchParams)
        {

            using (var service = new LowCostSite.FlightsSearchService.FlightsSearchService())
            {
                // Login to the flight search service
                LowCostSite.FlightsSearchService.Login oLogin = new FlightsSearchService.Login()
                {
                    UserId = ConfigurationHandler.SearchServiceUserID,
                    Password = ConfigurationHandler.SearchServiceUserPassword
                };

                // Get a toen for the service
                LoginToken oToken = service.OpenSession(oLogin);
                // Create the main search parameters object to send later to the search
                FlightsAvilibilityBySegmentsParameters oFlightsAvilibilityBySegmentsParameters = new FlightsAvilibilityBySegmentsParameters();

                // Create the container for flight segments (outward / return)
                SearchSegmentContainer searchSegs = new SearchSegmentContainer();

                #region Set the search to 1 or 2 ywa flight
                // if there is atleast an outward date
                if (!ConvertToValue.IsEmpty(Convert.ToDateTime(oSearchParams.OutwardDate)))
                {
                    //  if there is also a return date
                    if (!ConvertToValue.IsEmpty(oSearchParams.ReturnDate))
                    {
                        // if there are 2 dates, set the search to a 2 way flight
                        searchSegs.DataSource = new OriginDestinationInformationParameters[2];
                    }
                    else
                    {
                        // if there is only outward date, set the search to 1 way flight
                        searchSegs.DataSource = new OriginDestinationInformationParameters[1];
                    }

                }
                #endregion

                // create the information parameters for the outward flight (the 0 is the first place in the array of segments):
                CreateOrigininDestinationInfoParams(searchSegs, 0, oSearchParams);

                // Check if there is a return flight in the search (if the return flight is not empty there is a return flight)
                bool isReturn = !ConvertToValue.IsEmpty(oSearchParams.ReturnDate);

                // if there is a return flight
                if (isReturn)
                {
                    // create the information parameters for the return flight:
                    CreateOrigininDestinationInfoParams(searchSegs, 1, oSearchParams);
                }

                oFlightsAvilibilityBySegmentsParameters.SearchSegments = searchSegs;
                oFlightsAvilibilityBySegmentsParameters.TravelClass = ETravelClass.Economy;


                // set the trip type (1 or 2 ways)
                oFlightsAvilibilityBySegmentsParameters.TripType = isReturn ? ETripType.Return : ETripType.OneWay;

                // set the maximum number of changes in a flight, and maximum number of hops in a flight segment

                // if the user checked the "Prefer direct flights" check box, set the search for only direct flights
                if (oSearchParams.IsDirectFlight)
                {
                    oFlightsAvilibilityBySegmentsParameters.MaxChanges = 0;
                }
                else
                {
                    // if the check box is not checked, set the changes to the maximum (value stored in the web.config)
                    oFlightsAvilibilityBySegmentsParameters.MaxChanges = ConfigurationHandler.MaxChanges; //  = 2;
                }

                oFlightsAvilibilityBySegmentsParameters.MaxHops = ConfigurationHandler.MaxHops; //  = 2;


                // Handling the passengers:
                #region Passengers in the search
                PassengerBasicSearchContainer oPassengers = new PassengerBasicSearchContainer();

                oPassengers.DataSource = new Passenger[oSearchParams.AdultsCount + oSearchParams.ChildrenCount + oSearchParams.InfantsCount];

                int counter = 0;
                // Adding all the passengers to the search parameters for passengers (1 array for all types of passengers)
                for (int i = 0; i < oSearchParams.AdultsCount; i++)
                {
                    oPassengers.DataSource[counter] = new Passenger() { CustomerType = ECustomerType.ADT };
                    counter++;
                }
                for (int i = 0; i < oSearchParams.ChildrenCount; i++)
                {
                    oPassengers.DataSource[counter] = new Passenger() { CustomerType = ECustomerType.CNN };
                    counter++;
                }
                for (int i = 0; i < oSearchParams.InfantsCount; i++)
                {
                    oPassengers.DataSource[counter] = new Passenger() { CustomerType = ECustomerType.INF };
                    counter++;
                }
                // adding the passengers list to the search paremeters objext
                oFlightsAvilibilityBySegmentsParameters.Passengers = oPassengers;
                #endregion


                // set general parameters for the search
                oFlightsAvilibilityBySegmentsParameters.GeneralParameters = new GeneralParameters();

                // Getting the results from the service
                FlightsAvailResults oFlightsAvailResults;
                try
                {
                    System.Diagnostics.Stopwatch searchTime = new System.Diagnostics.Stopwatch();
                    searchTime.Start();
                    oFlightsAvailResults = service.GetFlightsAvailability(oFlightsAvilibilityBySegmentsParameters, oToken.ID);
                    searchTime.Stop();

                    LoggerManagerProject.LoggerFile logger = new LoggerManagerProject.LoggerFile();
                    logger.WriteInfo(string.Format("Search Time: {0}", searchTime.Elapsed.ToString()));

                }
                catch (Exception e)
                {
                    LoggerManagerProject.LoggerManager.Logger.WriteError("Error in Extensions.GetSearchResults() function. Message: "+ e.Message);
                    throw e;
                    //  oFlightsAvailResults = new FlightsAvailResults();
                }

                // return the results for the search
                return oFlightsAvailResults;

            }
        }


        /// <summary>
        /// Creates a Origin or destination segments for the use of the search.
        /// </summary>
        /// <param name="searchSegs">The container where the created segment will be added to.</param>
        /// <param name="placeInArray">The number in the array to add the segment to. also used to calculate and set the RPH (number + 1) </param>
        private static void CreateOrigininDestinationInfoParams(SearchSegmentContainer searchSegs, int placeInArray, SearchParams oSearchParams)
        {
            OriginDestinationInformationParameters seg = new OriginDestinationInformationParameters();
            seg.OriginLocation = new Location();
            seg.OriginLocation.Code = placeInArray == 0 ? oSearchParams.Origin : oSearchParams.Destination;
            seg.OriginLocation.Type = oSearchParams.OriginType;

            // if the place in the array is 0 - it is the outward flight, and if 1 - the return flight
            // the start date is the date of the flight segment (there is only 1 date in a segment)
            DateTime startDate = placeInArray == 0 ? Convert.ToDateTime(oSearchParams.OutwardDate) : Convert.ToDateTime(oSearchParams.ReturnDate);
            if (!ConvertToValue.IsEmpty(startDate))
            {
                // take only the date part of the datetime
                seg.DepartureDateTime = startDate.Date;
            }

            seg.DestinationLocation = new Location();
            seg.DestinationLocation.Code = placeInArray == 0 ? oSearchParams.Destination : oSearchParams.Origin;
            seg.DestinationLocation.Type = oSearchParams.DestinationType;
            seg.RPH = placeInArray + 1;
            searchSegs.DataSource[placeInArray] = seg;
        }

        #endregion

        /// <summary>
        /// A method that converts from  system.DayOfWeek to EnumHandler.DaysOfWeek
        /// </summary>
        public static EnumHandler.DaysOfWeek GetDayOfWeek(DayOfWeek oDayOfWeek)
        {
            switch (oDayOfWeek)
            {
                case DayOfWeek.Friday:
                    return EnumHandler.DaysOfWeek.Friday;

                case DayOfWeek.Monday:
                    return EnumHandler.DaysOfWeek.Monday;
                case DayOfWeek.Saturday:
                    return EnumHandler.DaysOfWeek.Saturday;

                case DayOfWeek.Sunday:
                    return EnumHandler.DaysOfWeek.Sunday;

                case DayOfWeek.Thursday:
                    return EnumHandler.DaysOfWeek.Thursday;

                case DayOfWeek.Tuesday:
                    return EnumHandler.DaysOfWeek.Tuesday;

                case DayOfWeek.Wednesday:
                    return EnumHandler.DaysOfWeek.Wednesday;
                default:
                    return EnumHandler.DaysOfWeek.Sunday;
            }
        }


        /// <summary>
        /// A method that gets a datetime objext and returns a stirng with the date folowwd by the day name (using gettext)
        /// </summary>
        /// <param name="DepartDateTime"></param>
        /// <returns></returns>
        public static string GetDateWithDayName(DateTime DepartDateTime)
        {
            //EnumHandler.DaysOfWeek dayOfWeek = Extensions.GetDayOfWeek(ConvertToValue.ConvertToDateTime(DepartDateTime).DayOfWeek);
            string dayName = (new UC_BasePage()).GetText(DepartDateTime.DayOfWeek.ToString()); // (dayOfWeek.ToString());
            return string.Format("{0} {1}", DepartDateTime.ToShortDateString(), dayName);
        }


        #region for searchservice LocationWithOrigin object

        /// <summary>
        /// Get the name of the location by its type.
        /// Fallback (if not found by type): City name, Airport Name, airport code
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public static string GetLocationName(LocationWithOrigin location)
        {
            string name = "";
            if (location.Type == ELocationType.city)
            {
                name = GetCityName(location);
                if (name!= "")
                {
                    return name;
                }
            }
            else if (location.Type == ELocationType.airport)
            {
                name = GetAirportName(location);
                if (name != "")
                {
                    return name;
                }
            }

            if (name == "")
            {
                name = GetCityName(location.LocationCode);
            }
            if (name == "")
            {
                name = GetAirportName(location.LocationCode);
            }
            if (name == "")
            {
                name = location.LocationCode;
            }
            return name;
        }
        /// <summary>
        /// Get the loaction name by code only (better send a location object).
        /// </summary>
        /// <param name="iataCode"></param>
        /// <returns>The name of a city with that code, and if not found, return the name of an airport with that code (or the code itself)</returns>
        public static string GetLocationName(string iataCode)
        {
            string name = GetCityName(iataCode);
            if (ConvertToValue.IsEmpty(name))
            {
                name = GetAirportName(iataCode);
            }
            if (ConvertToValue.IsEmpty(name))
            {
                name = iataCode;
            }
            return name;
        }
       
        /// <summary>
        /// A function that gets a iata code and returns the city of the iata code, and if no city is found with that code, it returns the name of the airport.
        /// </summary>
        /// <param name="iataCode">An airport iata Code</param>
        /// <returns></returns>
        public static string GetCityOfAirportOrAirport(string iataCode)
        {
            string name = GetCityOfAirport(iataCode);
            if (ConvertToValue.IsEmpty(name))
            {
                name = GetAirportName(iataCode);
            }
            return name;
        }

        /// <summary>
        /// A function that gets a iata code and returns the city name if it's a city iata code.
        /// otherwise it looks for an airport with that code.
        /// </summary>
        /// <param name="iataCode">A iata code iata Code</param>
        /// <returns></returns>
        public static string GetCityOrAirportName(string iataCode)
        {
            string name = GetCityName(iataCode);
            if (ConvertToValue.IsEmpty(name))
            {
                name = GetAirportName(iataCode);
            }
            return name;
        }


        public static string GetAirportName(LocationWithOrigin location)
        {
            return GetAirportName(location.LocationCode);
        }
        public static string GetAirportName(string iataCode)
        {
            DL_LowCost.Airport oAirport = StaticData.oAirportsList.FirstOrDefault(airport => airport.IataCode_Value == iataCode);
            if (oAirport != null)
            {
                if (oAirport.NameByLang_UI != "")
                {
                    return oAirport.NameByLang_UI;
                }
                else
                {
                    return oAirport.EnglishName_UI;
                }

            }
            return "";
        }
        public static string GetCityName(LocationWithOrigin location)
        {
            return GetCityName(location.LocationCode);
        }
        public static string GetCityName(string iataCode)
        {
            DL_LowCost.City tempCity = StaticData.oCityList.FirstOrDefault(c => c.IataCode_UI == iataCode);
            if (tempCity != null)
            {
                return tempCity.Name_UI;
            }
            return "";
        }
        public static string GetCityOfAirport(LocationWithOrigin location)
        {
            return GetCityOfAirport(location.LocationCode);
        }
        public static string GetCityOfAirport(string iataCode)
        {
            // get the airport from the airport list
            DL_LowCost.Airport oAirport = StaticData.oAirportsList.FirstOrDefault(airport => airport.IataCode_Value == iataCode);
            if (oAirport != null)
            {
                // if there is a city for the airport
                if (oAirport.CitySingle.Name_UI != "")
                {
                    // return the city name
                    return oAirport.CitySingle.Name_UI;
                }
                else
                {
                    // no city with the matching iata code was found in the database.
                }

            }
            return "";
        }
        public static string GetCoutryOfAirport(LocationWithOrigin location)
        {
            return GetCoutryOfAirport(location.LocationCode);
        }
        public static string GetCoutryOfAirport(string iataCode)
        {
            DL_LowCost.Airport oAirport = StaticData.oAirportsList.FirstOrDefault(airport => airport.IataCode_UI == iataCode);
            if (oAirport != null)
            {
                if (oAirport.CitySingle != null)
                {
                    DL_LowCost.Country oCountry = oAirport.CitySingle.CountrySingle;
                    if (oCountry != null && oCountry.Name_UI != "")
                    {
                        return oAirport.CitySingle.CountrySingle.Name_UI;
                    }

                }
                else
                {
                    // no country with the matching iata code was found in the database.
                }

            }
            return ""; // when we will get the countries list to fill in the DB, this will return the real country name.
        }

        internal static string GetCoutryOfCity(string iataCode)
        {
            DL_LowCost.City oCity = StaticData.oCityList.FirstOrDefault(city => city.IataCode_UI == iataCode);
            if (oCity != null)
            {
                DL_LowCost.Country oCountry = oCity.CountrySingle;
                if (oCountry != null && oCountry.Name_UI != "")
                {
                    return oCity.CountrySingle.Name_UI;
                }
            }
            return ""; // when we will get the countries list to fill in the DB, this will return the real country name.
        }



        // extension for city object:
        public static string GetCityNameByIataCode(string IataCode)
        {
            DL_LowCost.City tempCity = StaticData.oCityList.FirstOrDefault(c => c.IataCode_UI == IataCode);
            if (tempCity != null)
            {
                return tempCity.Name_UI;
            }

            return "";
        }

        #endregion

        /// <summary>
        /// A function that fires the function in the control's data-OnBack attribute (when returning from the server)
        /// </summary>
        /// <param name="theControl">The control (button, link button etc.) </param>
        /// <param name="thePage">The page to add the script to </param>
        public static void OnBack(System.Web.UI.WebControls.WebControl theControl, BasePage_UI thePage)
        {
            string backScript = theControl.Attributes["data-OnBack"];
            // if there is a back script, add it
            backScript = ConvertToValue.ConvertToString(backScript);
            if (!ConvertToValue.IsEmpty(backScript))
            {
                (thePage as BasePage_UI).AddScript(backScript);
            }
        }

        /// <summary>
        /// A method that checks if a string is in hebrew (RTL) by checking if the string's first char is from the hebrwe alphabet.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        internal static bool IsRtl(string text)
        {
            return "אבגדהוזחטיכךלמםנןסעפףצץקרשת".Contains(text[0]);
        }


        public static string GetCurrencySymbol(string code)
        {
            CurrencyContainer oCurrencyContainer = CurrencyContainer.SelectByKeysView_Code(code, true);
            if (oCurrencyContainer != null && oCurrencyContainer.Count > 0)
            {
                return oCurrencyContainer.Single.Sign_UI;
            }
            return code;
        }


    }



}