﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="Faq.aspx.cs" Inherits="LowCostSite.sass.Faq" %>

<%@ MasterType VirtualPath="~/sass/Master/MasterPage.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .bs-example {
            margin: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="content-page">
            <div class="line-title"></div>
            <div class="highlight rtl display_inline_block full-width">
                <div class="box-wrap-content">
                    <div class="box-wrap-title">שאלות ותשובות:</div>
                    <div class="bs-example no-margin-top">
                        <asp:Panel ID="pnl_ContentText" runat="server">
                            <div class="highlight rtl">
                                <div>
                                    <asp:PlaceHolder ID="ph_StaticText" runat="server"></asp:PlaceHolder>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="panel-group rtl" id="accordion">
                            <asp:Repeater ID="drp_FAQ" runat="server" ItemType="DL_LowCost.Faq">
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <!-- START: Question -->
                                        <div class="panel-heading">
                                            <h4 class="panel-title">

                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href='#<%# Eval("DocId_UI") %>'>
                                                    <!-- In the HREF there should be the id of the DIV around the answer-->
                                                    <span><%# Eval("Question_UI") %></span>
                                                </a>
                                            </h4>
                                        </div>
                                        <!-- END: Question -->
                                        <!-- START: Asnwer Area (the id of this div is the id of the repeater item, so that the link of the question will know what div to open)-->
                                        <div id='<%# Eval("DocId_UI")%>' class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <%#Eval("Answer_UI")%>
                                            </div>
                                        </div>
                                        <!-- END: Answer Area-->
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
