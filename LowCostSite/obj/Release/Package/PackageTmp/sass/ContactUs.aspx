﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="LowCostSite.sass.ContactUs" %>
<%@ MasterType VirtualPath="~/sass/Master/MasterPage.master" %> 

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
     
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="content-page">

            <div class="line-title"></div>
            <div class="highlight rtl display_inline_block full-width">

                <div class="box-wrap-content">
                    <div class="box-wrap-title">צור קשר</div>

                    <div class="tips-box">
                        <div class="highlight rtl ">
                            <div>
                                <asp:PlaceHolder ID="ph_StaticText" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>

                        <div class="row rtl bg_search_box  inline-block">
                            <div class="col-sm-12" id="div_form"  runat="server">
                                <div class="col-sm-6" >
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label"><%=GetText("FirstName")%>: </label>
                                                <asp:TextBox ID="txt_FirstName" class="form-control" runat="server" placeholder="שם פרטי"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="שדה חובה" ForeColor="Red" ControlToValidate="txt_FirstName" ValidationGroup="requestForm"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label"><%:GetText("LastName")%>: </label>
                                                <asp:TextBox ID="txt_LastName" class="form-control" runat="server" placeholder="שם משפחה" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="שדה חובה" ForeColor="Red" ControlToValidate="txt_LastName" ValidationGroup="requestForm"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label"><%:GetText("EmailAddress")%>: </label>
                                                <asp:TextBox ID="txt_Email" class="form-control" runat="server" placeholder="דואר אלקטרוני" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="שדה חובה" ForeColor="Red" ControlToValidate="txt_Email" ValidationGroup="requestForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage='<%#GetText("EmailAddress")%>' ForeColor="Red" ControlToValidate="txt_Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="requestForm" Display="Dynamic"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label"><%:GetText("PhoneNumber")%>: </label>
                                                <asp:TextBox ID="txt_Phone" class="form-control" runat="server" placeholder="טלפון" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="שדה חובה" ForeColor="Red" ControlToValidate="txt_Email" ValidationGroup="requestForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="טלפון לא תקין" ControlToValidate="txt_Phone" ForeColor="Red" ValidationGroup="requestForm" Display="Dynamic" ValidationExpression="^0\d([\d]{0,1})([-]{0,1})\d{7}$"></asp:RegularExpressionValidator>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="col-sm-12 ">
                                            <div class="form-group">
                                                <label class="control-label"><%=GetText("Comments")%>: </label>
                                                <textarea id="txt_Remarks" runat="server" class="form-control" rows="4"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 ">
                                        <div class="col-sm-12 ">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button ID="btn_SendRequest" runat="server" ValidationGroup="requestForm" Text="שלח" class="btn btn-primary btn-lg btn-block" OnClick="btn_SendRequest_Click" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="highlight rtl">
                                        <h4>  כתובת: <small>נחלת יצחק 12, תל אביב</small> </h4>
                                        <h4>  טלפון: <small>03-7771800</small></h4>
                                         </div>
                               </div>
                            </div>
                            <!--/row-->

                             <div class="col-sm-12" id="div_thanks" runat="server" style="display:none">
                                    <h1>פנייתך התקבלה בהצלחה!</h1>
                                    <h2>נציגנו יחזרו אליכם בהקדם. תודה</h2>
                                </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
