﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="LowCostSite.sass.ErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="content-page">

            <div class="line-title"></div>
            <div class="highlight rtl display_inline_block full-width">

                <div class="box-wrap-content text-center">
                    <div class="tips-box">
                        <h1 class="title_content">אירעה שגיאה</h1>
                        <h3>
                           אנו מתנצלים על אי הנוחות . לחזרה לדף הבית <a href="Index.aspx">לחץ כאן</a>
                        </h3>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
