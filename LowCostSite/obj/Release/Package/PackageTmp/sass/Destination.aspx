﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="Destination.aspx.cs" Inherits="LowCostSite.sass.Destination" %>

<%@ Register Src="~/sass/Controls/UC_Search.ascx" TagPrefix="uc1" TagName="UC_Search" %>
<%@ MasterType VirtualPath="~/sass/Master/MasterPage.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script src='<%=ResolveUrl("~/sass/javascripts/PageScripts/TemplatesScripts.js") %>'></script>
    <script src="/sass/javascripts/PageScripts/UC_Search.js"></script>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN: Background image -->
    <div class="center-image">
        <ul class="ul_image">
            <li>
                <asp:Image ID="img_Cover" runat="server" class="back-image img-closed" />
            </li>
        </ul>
    </div>
    <!-- END: Background image -->
    <div class="content-page size-main-page">
        <!-- BEGIN: Main Details -->
        <div class="row row_promotion destination-box rtl">
            <div class="col-md-10 right">
                <div class="row">
                  <h1><asp:Label ID="lbl_Title" class="row_text_gallery" runat="server" Text="" /></h1>  
                </div>
                <div class="row">
                    <!-- Starting price -->
                <span runat="server" id="MinPriceInfo" class="row_text_gallery">החל מ-
                        <asp:Label ID="lbl_Price" runat="server" Text=""></asp:Label>
                    </span>
                </div>
            </div>
            <div class="col-md-1 ">
                <b>
                    <!-- Time difference-->
                    <asp:Label ID="lbl_Clock" class="clock_text" runat="server" /></b>
                <img src="/sass/images/clock.PNG" />
            </div>
        </div>
        <!-- END: Main Details -->

        <!-- BEGIN: SEARCH BOX-->
        <div class="wrap-search">
            <uc1:UC_Search runat="server" ID="uc_Search" />
        </div>
        <!-- END: SEARCH BOX-->
    </div>
    <div class="container">
        <!-- BEGIN: Templates area-->
        <div class="row full-width inline-block mt14 ">

            <!-- BEGIN: RIGHT SIDE Templates -->
            <div class="col-md-9 right no-padding-right ">
                <asp:PlaceHolder ID="ph_TemplateRight" runat="server"></asp:PlaceHolder>
            </div>
            <!-- END: RIGHT SIDE Templates -->

            <!-- BEGIN: LEFT SIDE Templates -->
            <div class="col-md-3 right no-padding-right no-padding-left">
                <asp:PlaceHolder ID="ph_TemplateLeft" runat="server"></asp:PlaceHolder>
            </div>
            <!-- END: LEFT SIDE Templates -->

        </div>
        <!-- END: Templates area-->
    </div>

    <script>
        // Moving the clock
        $(function () {
            // set an interval of 1 minute
            setInterval(function () {
                // get the clock label
                var clock = $('.clock_text');
                // get the time from the clock
                var myTime = $('.clock_text').html();
                if (myTime == '') {
                    return;
                }
                // split to hours and minutes
                var ss = myTime.split(':');
                // create a date object and set it's time to that of the clock
                var dt = new Date();
                dt.setHours(ss[0]);
                dt.setMinutes(ss[1]);
                // in a new date object, set the same time and add 1 minute
                var dt2 = new Date(dt.valueOf() + 60000);
                // take only the time part of the datetime string, and set in a string object
                var ts = dt2.toTimeString().split(' ')[0];
                // take only the hours and minutes of the time...
                var timeStringArray = ts.toString().split(':');
                // and put it back in the clock
                clock.html(timeStringArray[0] + ':' + timeStringArray[1]);

            }, 60000); // run this every 1 minute
        });
    </script>
</asp:Content>
