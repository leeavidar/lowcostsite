﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SideInformation.ascx.cs" Inherits="LowCostSite.sass.UC_SideInformation" %>
<div class="highlight rtl ">
    <div class="display_inline_block row">
        <div class="col-md-12 right ">
            <h4 class="title_content">
                <asp:Label ID="lbl_Title" runat="server" Text="" />
            </h4>
                <asp:Label ID="lbl_ShortText" runat="server" Text="" />
            <br />
            <div runat="server" id="lbl_LongText" class="TemplateLongText"></div>
            <span class="read_more" id="lbl_ReadMore" runat="server" style="display: none;" onclick="ToggleLongText(this)">קרא עוד</span>
        </div>
    </div>
</div>
