﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sass/Master/MasterPage.Master" AutoEventWireup="true" CodeBehind="Company.aspx.cs" Inherits="LowCostSite.sass.Company" %>

<%@ Register Src="~/sass/Controls/Templates/UC_MainInformation.ascx" TagPrefix="uc1" TagName="UC_MainInformation" %>
<%@ Register Src="~/sass/Controls/UC_Search.ascx" TagPrefix="uc1" TagName="UC_Search" %>
<%@ MasterType VirtualPath="~/sass/Master/MasterPage.master" %> 

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
       <script src='<%=ResolveUrl("~/sass/javascripts/PageScripts/TemplatesScripts.js") %>' ></script>
    <script src="/sass/javascripts/PageScripts/UC_Search.js"></script>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="center-image">
            <!-- BEGIN: Background image -->
            <ul class="ul_image">
                <li>
                    <asp:Image ID="img_Cover" runat="server" class="back-image img-closed" />
                </li>
            </ul>
                </div>
   
        <div class="content-page size-main-page">
         
            <!-- END: Background image -->


            <!-- BEGIN: Main Details -->
            <div class="row row_promotion destination-box rtl">
                <div class="col-md-10 right">
                    <div class="row">
                       <h1><asp:Label ID="lbl_Title" class="row_text_gallery" runat="server" Text="" style="direction:rtl;"/></h1> 
                    </div>
                </div>
            </div>
            <!-- END: Main Details -->


             <!-- BEGIN: SEARCH BOX-->
             <div class="wrap-search">
                <uc1:UC_Search runat="server" ID="uc_Search" />
            </div>
             </div>
            <!-- END: SEARCH BOX-->
      <div class="container">
            <!-- BEGIN: Templates area-->
            <div class="row full-width inline-block mt14 ">
                <div class="col-md-12 right no-padding-right ">
                    <div class="highlight rtl display_inline_block full-width">
                        <div>

                            <div class="row ">
                                <div class="col-md-9 right ">
                                    <h4 class="title_content">
                                        <asp:Label ID="lbl_Name" runat="server" Text="" />
                                    </h4>
                                 <span runat="server" id="lbl_Description"></span>
                                </div>
                                <div class="col-md-3 left ">
                                    <span class="img_title ">
                                        <asp:Label ID="lbl_ImageTitle" runat="server" Text=""/>
                                    </span>
                                    <asp:Image ID="img_Logo" runat="server" CssClass="img-size-small-box left"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <asp:PlaceHolder ID="ph_Templates" runat="server"></asp:PlaceHolder>
                </div>
            </div>
            <!-- END: Templates area-->
       
   </div>
</asp:Content>
