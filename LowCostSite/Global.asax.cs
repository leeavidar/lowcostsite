﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace LowCostSite
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // fills the static data properties (cities, airports...)
            StaticData oStaticData = new StaticData();
            LoggerManagerProject.LoggerManager.Logger.WriteInfo("LowCost Application Start!");


            // Code that runs on application startup
            #region Licen to 2013 version
            EO.Pdf.Runtime.AddLicense(
        "Weic3PIEEOR3hI6xy59Zs+X9F+6wtZGby59Zl6Sxy5912Oj1y/Oy5+nOzcSI" +
        "pdT1EaFZ7ekDHuio5cGz4K1pmaTA6YxDl6Sxy7to2PD9GvZ3hI6xy59Zs/MD" +
        "D+SrwPL3Gp+d2Pj26KFqqbPD4a5rp7XDzZ+v3PYEFO6ntKbFzZ9otZGby59Z" +
        "l8AEFOan2PgGHeR3wvDC7+CJ77r1HOij3uvfBcCJv/nJ/Lx2s7MEFOan2PgG" +
        "HeR3hI7N2uui2un/HuR3hI514+30EO2s3MKetZ9Zl6TNF+ic3PIEEMidtbfZ" +
        "4rFsqbzF4MprsLbK4bJws7P9FOKe5ff29ON3hI6xy59Zs/D6DuSn6un26cCr" +
        "4PKx7eSnl8X5DPGo5cDAFw==");
            #endregion
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            CultureInfo newCulture = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            newCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            newCulture.DateTimeFormat.DateSeparator = "/";
            Thread.CurrentThread.CurrentCulture = newCulture;
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

    }
}