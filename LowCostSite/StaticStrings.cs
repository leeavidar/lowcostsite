﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LowCostSite
{
    public class StaticStrings
    {
        #region Folder paths
        public static readonly string path_Root = @"http://localhost:8239/";
        public static string path_Uploads { get { return System.Configuration.ConfigurationManager.AppSettings["ImageUploads"].ToString(); } }
        public static string LogoSitePath { get { return System.Configuration.ConfigurationManager.AppSettings["LogoPath"].ToString(); } }
        #endregion

        #region User Controls path

        public static readonly string path_UC_PopularDestination = @"/sass/Controls/UC_PopularDest.ascx";
        public static readonly string path_404 = @"/sass/NotFoundPage.aspx";
        public static readonly string path_UC_MainInformation = @"/sass/Controls/Templates/UC_MainInformation.ascx";
        public static readonly string path_UC_CenterWithImage = @"/sass/Controls/Templates/UC_CenterWithImage.ascx";
        public static readonly string path_UC_SideInformation = @"/sass/Controls/Templates/UC_SideInformation.ascx";
        public static readonly string path_UC_SideWithImage = @"/sass/Controls/Templates/UC_SideWithImage.ascx";
        public static readonly string path_UC_SubBox = @"/sass/Controls/Templates/UC_SubBox.ascx";
        public static string path_UC_Faq = @"/sass/Controls/UC_Faq.ascx";

        public static string path_UC_FlightGroup = @"/sass/Controls/UC_FlightGroup.ascx";
        public static string path_UC_Flight = @"/sass/Controls/UC_Flight.ascx";
        public static string path_UC_FlightLeg = @"/sass/Controls/UC_FlightLeg.ascx";
        public static string path_UC_FlightStop = @"/sass/Controls/UC_FlightStop.ascx";
        public static string path_UC_PassengerDetails = @"/sass/Controls/UC_PassengerDetails.ascx";
        public static string path_UC_PassengerDetailsView = @"/sass/Controls/UC_PassengerDetailsView.ascx";
        

        
        #endregion

        #region Page addresses
        public static readonly string path_AirlineImages = @"http://www.travelfusion.com/images/logos/";
        public static readonly string path_DestinationPage = @"/sass/Destination.aspx";
        public static readonly string path_CompanyPage = @"/sass/Company.aspx";
        public static readonly string path_FlightsResults = @"~/sass/FlightsResults.aspx";
        public static readonly string path_OrderPage = @"/sass/OrderPage.aspx";
        public static readonly string path_IndexPage = @"/sass/Index.aspx";
        public static readonly string path_ThanksPage = @"/sass/ThanksPage.aspx";
        public static readonly string path_ErrorAndCallBack = @"/sass/CallBack.aspx";
        public static readonly string path_ErrorPage = @"/sass/ErrorPage.aspx";
        public static readonly string path_ErrorIFrame = @"/sass/ErrorIFrame.aspx";

        #endregion

        #region OrderPage
        public static readonly string valueSelectType = "value_select";
        public static readonly string display = "display: block";
        public static readonly int IsraelPhonePrefix = 972;
        public static readonly string PriceError = "1010";
        public static readonly string PriceCurrencyChangedError = "1014";
        public static readonly string DuplicateBooking = "1012";
        public static readonly string StatusPending = "StatusPending";


        #endregion


        #region Session Names
        public static string ses_oSelectedItinerary = @"oSelectedItinerary";
        #endregion

    }
}