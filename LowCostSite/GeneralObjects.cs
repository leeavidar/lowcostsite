﻿using Generic;
using LowCostSite.FlightsSearchService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LowCostSite
{
    [Serializable]
    public class Details
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
    }

    [Serializable]
    public class SelectedItinerary
    {
        public Guid LoginToken { get; set; }
        public Guid SearchToken { get; set; }
        public Flights SelectedFlights { get; set; } // airline
    }

    [Serializable]
    public class SearchParams
    {
        /// <summary>
        /// A method thats sets the location for origin or destination.
        /// it gets iata code or names in english or other languages, and looks for them in the list of cities and list of airports.
        /// if a location exists in one of the lists, it's Iata Code will be returned from the method.
        /// </summary>
        /// <param name="value">The value repersenting the location (could be city or airport name or iata code)</param>
        /// /// <param name="isOrigin">true if the location is origin or false for destination</param>
        /// <returns>The Iata code if the location was found, or empty string ("") otherwise </returns>
        private string SetLocation(string value, bool isOrigin)
        {
            // find a city with the value as it's iata code or name 
            DL_LowCost.City oCity = StaticData.oCityList.FirstOrDefault(city => city.IataCode_UI == value || city.Name_UI == value);
            if (oCity != null)
            {
                SetLocationType(isOrigin, ELocationType.city);
                return oCity.IataCode_Value;
            }
            else
            {
                // find an airport with the value as it's iata code or name (english or other language)
                DL_LowCost.Airport oAirport = StaticData.oAirportsList.FirstOrDefault(airport => airport.IataCode_UI == value || airport.EnglishName_UI == value || airport.NameByLang_UI == value);
                // if an airport was found
                if (oAirport != null)
                {
                    SetLocationType(isOrigin, ELocationType.airport);
                    // store it in the private field
                    return oAirport.IataCode_Value;
                }
                else
                {
                    // the default is city, event if there is no location.
                    SetLocationType(isOrigin, ELocationType.city);
                    // in case there was no city and no airport found
                    return "";
                }
            }
        }

        private void SetLocationType(bool isOrigin, ELocationType oLocationType)
        {
            if (isOrigin)
            {
                OriginType = oLocationType;
            }
            else
            {
                DestinationType = oLocationType;
            }
        }

        private string _origin;
        public string Origin
        {
            get
            {
                return _origin;
            }
            set
            {
              _origin =  SetLocation(value, true);
            }
        }
        public ELocationType OriginType { get; set; }

        private string _destination;
        public string Destination
        {
            get
            {
                return _destination;
            }
            set
            {
                _destination = SetLocation(value,false);
            }
        }
        public ELocationType DestinationType { get; set; }

        private string _outwardDate;
        public string OutwardDate
        {
            get { return _outwardDate; }
            set
            {
                string DateModified = value.Replace('-', '/');
                _outwardDate = DateModified;
            }
        }

        private string _returnDate;
        public string ReturnDate
        {
            get
            {
                return _returnDate;
            }
            set
            {
                string DateModified = value.Replace('-', '/');
                _returnDate = DateModified;
            }
        }

        public int AdultsCount { get; set; }
        public int ChildrenCount { get; set; }
        public int InfantsCount { get; set; }
        // IF the return date is an empty string - return true. Otherwise return false
        public bool OneWay { get { return ReturnDate == ""; } }


        public string AirlinesFilter { get; set; }
        public static string GetLocationName(string location)
        {
            // find a city with the value as it's iata code or name 
            DL_LowCost.City oCity = StaticData.oCityList.FirstOrDefault(city => city.IataCode_UI == location || city.Name_UI == location);
            if (oCity != null)
            {
                // return the city name
                return oCity.Name_UI;
            }
            else
            {
                // find an airport with the value as it's iata code or name (english or other language)
                DL_LowCost.Airport oAirport = StaticData.oAirportsList.FirstOrDefault(airport => airport.IataCode_UI == location || airport.EnglishName_UI == location || airport.NameByLang_UI == location);
                // if an airport was found
                if (oAirport != null)
                {
                    // return the other language name if it exists, and if not, return the english name
                    return oAirport.NameByLang_UI != "" ? oAirport.NameByLang_UI : oAirport.EnglishName_UI;
                }
                else
                {
                    // in case there was no city and no airport found
                    return "";
                }
            }
        }

        public bool IsDirectFlight { get; set; }
    
    }


    [Serializable]
    public class SimpleAirport
    {
        public string IataCode { get; set; }
        public string Name { get; set; }
        public string CityIataCode { get; set; }
    }

    [Serializable]
    public class SimpleCity
    {
        public string IataCode { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }
    }

    [Serializable]
    public class SimpleAirline
    {
        public string IataCode { get; set; }
        public string Name { get; set; }
    }




}