﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Generic;
using DL_LowCost;
using BL_LowCost;
using LowCostSite.FlightsSearchService;
using System.Web.Compilation;
using System.CodeDom;
using System.Web.UI;
using DL_Generic;
using System.Configuration;
using LoggerManagerProject;
using System.IO;


namespace LowCostSite.sass
{
    public abstract class BasePage_UI : BasePage
    {
        #region Scripter
        public List<string> ScriptListToCall { get; set; }
        public void AddScript(string script)
        {
            if (ScriptListToCall == null)
            {
                ScriptListToCall = new List<string>();
            }
            ScriptListToCall.Add(script);
        }
        /// <summary>
        /// Set scripts on the page
        /// </summary>
        public void CallPageScript()
        {
            if (ScriptListToCall != null)
            {
                
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "MyScript_PAGE", string.Format("{0};", string.Join(";", ScriptListToCall)), true);
            }
        }
        #endregion

        #region GetText

        public override string GetText(string text)
        {
            try
            {
                return StringHandler.ConvertToFriendlyString(GetGlobalResourceObject(Lang, text).ToString());
            }
            catch (Exception)
            {
                return "";
            }
        }

        #endregion

        #region Session Methods

        /// <summary>
        /// update the sessions when page load complite 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            CallPageScript();
        }

        /// <summary>
        /// Load the all containers that the page uses (preferably only 1) from the session.
        /// </summary>
        /// <param name="isPostBack"></param>
        internal abstract void LoadFromSession(bool isPostBack);

        /// <summary>
        /// Save the containers to the sessions
        /// </summary>
        internal abstract void SaveToSession();
       

        /// <summary>
        /// A method that resets all session properties. 
        /// </summary>
        internal abstract void ResetAllSessions();
       

        #endregion

        #region Main Object Session Handler
        public Users AdminObject
        {
            get
            {
                Users _AdminObject;
                if (SessionManager.IsAdminExist())
                {
                    _AdminObject = SessionManager.GetAdminFromSession();
                }
                else
                {
                    _AdminObject = null;
                    Response.Redirect("Login.aspx");
                }
                return _AdminObject;
            }
        }


        
        #endregion

        #region ExtraMethod

        public static string GetXMLDirByDateAndOrderId(DateTime dt, string orderDocId)
        {
            try
            {
                string XMLDir = string.Format(@"{0}\{1}\{2}\{3}\{4}", BasePage.VirtualDirectoryPath, BasePage.XMLFileRoot, dt.Year, dt.Month, orderDocId);
                LoggerManager.Logger.WriteInfo("UI Base Page - CreateXMLDirByDateAndOrderId: file path - [{0}] ", XMLDir);
               if(!Directory.Exists(XMLDir))
               {
                   Directory.CreateDirectory(XMLDir);
               }
                DL_Generic.Files.CreateDir(null, XMLDir);
                return XMLDir;
            }
            catch (Exception ex)
            {
                LoggerManager.Logger.WriteError("ERROR:UI Base Page - CreateXMLDirByDateAndOrderId: ", ex);
                //throw ex;
            }
            return null;
        }
        public static void SendOrderMail(string strMailContentForClient, string strMailSubjectForClient, string clientEmail, string strMailContentForSupport, string strMailSubjectForSupport, int mailSendOptions)
        {
            try
            {
                switch (mailSendOptions)
                {
                    case (int)EnumHandler.MailSendOptions.SupportAndClient:
                        MailHandler.SendMailToClient(strMailContentForClient, strMailSubjectForClient, clientEmail);
                        goto case (int)EnumHandler.MailSendOptions.OnlySupport;
                    case (int)EnumHandler.MailSendOptions.OnlySupport:
                        MailHandler.SendMailToSupport(strMailContentForSupport, strMailSubjectForSupport);
                        break;
                    case (int)EnumHandler.MailSendOptions.SupportAndAlgoma:
                        MailHandler.SendMailToSupport(strMailContentForSupport, strMailSubjectForSupport, true);
                        break;
                    default:
                        break;
                }

                if (mailSendOptions != (int)EnumHandler.MailSendOptions.SupportAndClient && !string.IsNullOrWhiteSpace(clientEmail))
                {
                    strMailSubjectForClient = MailStrings.strSubjectBookingOnErrorClient;
                    strMailContentForClient = MailStrings.strBookingOnErrorClient;
                    //Send email to client that we recived his order 
                  //  MailHandler.SendMailToClient(strMailContentForClient, strMailSubjectForClient, clientEmail);
                }
               
            }
            catch (Exception ex)
            {
                //Cannot send email!!!!
                LoggerManagerProject.LoggerManager.Logger.WriteError(string.Format("EX : {0}  {2} Cannot send email. Email Option : {1}", ex, mailSendOptions, Environment.NewLine));
            }
           
        }

        #endregion

        #region Code Expression Builder
        [ExpressionPrefix("Code")]
        public class CodeExpressionBuilder : ExpressionBuilder
        {
            public override CodeExpression GetCodeExpression(BoundPropertyEntry entry,
               object parsedData, ExpressionBuilderContext context)
            {
                return new CodeSnippetExpression(entry.Expression);
            }
        } 
        #endregion


        #region static params

        public static readonly string UrlPayment = ConfigurationSettings.AppSettings["UrlPayment"];

        public static readonly bool IsPaymentSecureConnection = ConvertToValue.ConvertToBool(ConfigurationSettings.AppSettings["IsPaymentSecureConnection"]);

        public static readonly bool DebugMode = ConvertToValue.ConvertToBool(ConfigurationSettings.AppSettings["DebugMode"]);

        public static readonly bool IsFakePalecardPayment = ConvertToValue.ConvertToBool(ConfigurationSettings.AppSettings["FakePalecardPayment"]);

        public static readonly bool DebugModeMoveToPelecard = ConvertToValue.ConvertToBool(ConfigurationSettings.AppSettings["DebugModeMoveToPelecard"]);

        public static readonly bool IsFakePayment = DL_Generic.ConvertToValue.ConvertToBool(System.Web.Configuration.WebConfigurationManager.AppSettings["IsFakePayment"]);

        public static readonly double FakeAmount = DL_Generic.ConvertToValue.ConvertToDouble(System.Web.Configuration.WebConfigurationManager.AppSettings["FakeAmount"]);

        #endregion

        //     public override string GetText(string text)
        //     {
        //         try
        //         {
        //             System.Resources.ResourceManager temp =
        //                 new System.Resources.ResourceManager("Resources." + Lang,
        //System.Reflection.Assembly.GetExecutingAssembly());
        //             return temp.GetString(text);

        //         }
        //         catch (Exception)
        //         {
        //             return "";
        //         }
        //     }
    }
}