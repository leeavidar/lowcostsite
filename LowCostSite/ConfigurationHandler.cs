﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LowCostSite
{
    public class ConfigurationHandler
    {

        public static string DomainAddress { get { return System.Configuration.ConfigurationManager.AppSettings["DomainAddress"]; } }

        // connecting to the search service
        public static string SearchServiceUserID { get { return System.Configuration.ConfigurationManager.AppSettings["SearchServiceUserID"]; } }
        public static string SearchServiceUserPassword { get { return System.Configuration.ConfigurationManager.AppSettings["SearchServiceUserPassword"]; } }

        // for mail sending
        public static bool IsLiveServer
        {
            get
            {
                bool _isLive = false;
                bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsLiveServer"], out _isLive);
                return _isLive;
            }
        }
        public static string SenderAccountEmailAddress { get { return System.Configuration.ConfigurationManager.AppSettings["SenderAccountEmailAddress"].ToString(); } }
        public static string SenderAccountEmailPassword { get { return System.Configuration.ConfigurationManager.AppSettings["SenderAccountEmailPassword"].ToString(); } }
        public static int SenderAccountSMTPPort { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SenderAccountSMTPPort"].ToString()); } }
        public static string SenderAccountSMTPHost { get { return System.Configuration.ConfigurationManager.AppSettings["SenderAccountSMTPHost"].ToString(); } }
        public static bool SenderAccountEnableSsl { get { return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SenderAccountEnableSsl"].ToString()); } }
        public static string EmailSubject { get { return System.Configuration.ConfigurationManager.AppSettings["EmailSubject"]; } }
        public static string ReceivingEmailAddress { get { return System.Configuration.ConfigurationManager.AppSettings["ReceivingEmailAddress"]; } }
        public static string CustomerMailSubject { get { return System.Configuration.ConfigurationManager.AppSettings["CustomerMailSubject"]; } }
        public static string CustomerMailContent { get { return System.Configuration.ConfigurationManager.AppSettings["CustomerMailContent"]; } }



        // for flights search parameters
        public static int MaxAdults { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxAdults"].ToString()); } }
        public static int MaxChildren { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxChildren"].ToString()); } }
        public static int MaxInfants { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxInfants"].ToString()); } }
        public static int MaxChanges { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxChanges"].ToString()); } }
        public static int MaxHops { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxHops"].ToString()); } }
        public static string[] BlockCurrencyTypes { get { return System.Configuration.ConfigurationManager.AppSettings["BlockCurrencyTypes"].Split(';'); } }

        // Max ages for each group
        public static int MaxChildAge { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxChildAge"].ToString()); } }
        public static int MaxInfantAge { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxInfantAge"].ToString()); } }

        public static int AmountOfSearchResultsGroups { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["AmountOfSearchResultsGroups"].ToString()); } }
        public static int NumberOfFlightsDisplayed { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NumberOfFlightsDisplayed"].ToString()); } }

        public static int DefaultLanguageID { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultLanguageID"].ToString()); } }


        public static string DefaultLanguage { get { return System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"]; } }
    }
}