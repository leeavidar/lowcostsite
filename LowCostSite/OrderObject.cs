﻿using DL_Generic;
using Generic;
using LowCostSite.sass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LowCostSite
{

    [Serializable]
    public class OrderObject
    {
        #region Properties

        public SearchParams SearchParams { get; set; }
        // Generated from the SelectedItinerary object, containing the data about the flights selected.

        public SelectedItinerary SelectedItinerarySearch { get; set; }


        public OrderSelectedItinerary SelectedItinerary;
        [XmlIgnore]
        public LowCostSite.FlightsBookingService.PricedItineraryDetails PricedItineraryDetails { get; set; }
        public OrderPrice NetPriceAndCurrency { get; set; }
        public OrderPrice NetPriceAndCurrencyFinal { get; set; }

        // The Currency part of the FlightBookTotalNetPric 
        public string FlightBookTotalNetCurrency { get; set; }
        // The total price (not including the handling fee)
        public decimal FlightBookTotalNetPrice { get; set; }
        // The total price including the Handling fee
        public decimal TotalPriceWithHandling { get; set; }
        // the total price with handling fee after price changes
        public decimal TotalPriceWithHandlingFinal { get; set; }
        // The difference between the original net price, and the price after price change
        public decimal DeltaNetPrice { get; set; }


        // The doc id that's available after saving the order in the database
        public int OrderDocId { get; set; }
        public string OrderToken { get; set; }
        public string OrderDateCreated { get; set; }
        public bool IsInterestedInMails { get; set; }
        public bool IsInterestedInSMS { get; set; }


        // The Request parameters for the booking

        LowCostSite.FlightsBookingService.BookingParameters _bookingParameters;
        public LowCostSite.FlightsBookingService.BookingParameters BookingParameters { get { return _bookingParameters; } set { _bookingParameters = value; } }
        // The response after booking
        [XmlIgnore]
        public LowCostSite.FlightsBookingService.FlightBookedDetails FlightBookDetails { get; set; }

        // The Request parameters for the issue
        public IssueTicketingParameters TicketingParameters { get; set; }

        // The object thats returnned from the issues process after the payment (with the PNR)
        [XmlIgnore]
        public LowCostSite.FlightsBookingService.FlightIssuedDetails FlightIssuedDetails { get; set; }

        public bool IsReady
        {
            get
            {
                return !string.IsNullOrEmpty(FlightBookTotalNetCurrency) && SelectedItinerary.LoginToken != null;
                //return BookingParameters != null && !string.IsNullOrEmpty(FlightBookTotalNetCurrency) && SelectedItinerary.LoginToken != null;
                // OLD 
                //return SessionBookingParameters != null && SessionFlightBookDetails != null && !string.IsNullOrEmpty(FlightBookTotalNetPriceAndCurrencyKey) && SelectedItinerary.LoginToken != null;
            }
        }

        public int PassengerNumber { get; set; }
        #endregion

        public OrderObject()
        {
            SelectedItinerary = new OrderSelectedItinerary();
        }

        #region XML
        public void SaveToFile()
        {
            try
            {
                string path = string.Format(@"{0}\{1}{2}{3}", BasePage_UI.GetXMLDirByDateAndOrderId(DateTime.Now, ConvertToValue.ConvertToString(OrderDocId)), "\\XMLOrder_", ConvertToValue.ConvertToString(OrderDocId), ".xml");
                BinaryHandler.SerializeBinary<OrderObject>(this,path);
                //string strXML = this.SerializeToString();
                //File.AppendAllText(string.Format(@"{0}\{1}{2}{3}", BasePage_UI.GetXMLDirByDateAndOrderId(DateTime.Now, ConvertToValue.ConvertToString(OrderDocId)), "\\XMLOrder_", ConvertToValue.ConvertToString(OrderDocId), ".xml"), strXML);
            }
            catch (Exception ex)
            {
                LoggerManagerProject.LoggerManager.Logger.WriteError("CreateOrderObject: Cannot write file or OrderObject SerializeToString", ex);
                //Response.Redirect("/OrderTimeExpiredPage.aspx");
            }
        }

        //private string SerializeToString(object obj)
        //{
        //    XmlSerializer serializer = new XmlSerializer(obj.GetType());

        //    using (StringWriter writer = new Utf8StringWriter())
        //    {
        //        serializer.Serialize(writer, obj);

        //        return writer.ToString();
        //    }
        //}


        #endregion


    }

    [Serializable]
    public class OrderPrice
    {
        public decimal Price { get; set; }
        public string Currency { get; set; }
        #region Methods

        public OrderPrice(decimal price, string currency)
        {
            Price = price;
            Currency = currency;
        }

        public OrderPrice()
        {
            Price = -1;
            Currency = "NONE";
        }

        public void update(decimal price, string currency)
        {
            this.Price = price;
            this.Currency = currency;
        }
        #endregion
    }

    [Serializable]
    public class IssueTicketingParameters
    {
        public Guid FlightsID { get; set; }
        public Double ExpectedTotalPrice { get; set; }
        public string ExpectedTotalPriceCurrency { get; set; }
        public string TfReference { get; set; }
        public LowCostSite.FlightsBookingService.GeneralParameters GeneralParameters { get; set; }
        public string PNR { get; set; }
    }

    [Serializable]
    public class OrderSelectedItinerary
    {
        #region PublicProp
        public Guid SearchToken
        {
            get { return _SearchToken; }
            set { _SearchToken = value; }
        }
        public Guid LoginToken
        {
            get { return _LoginToken; }
            set { _LoginToken = value; }
        }
        public string OriginCity
        {
            get { return _OriginCity; }
            set { _OriginCity = value; }
        }
        public string DestinationCity
        {
            get { return _DestinationCity; }
            set { _DestinationCity = value; }
        }
        public DateTime SummaryOutwardDate
        {
            get { return _SummaryOutwardDate; }
            set { _SummaryOutwardDate = value; }
        }
        public DateTime ReturnDate
        {
            get { return _ReturnDate; }
            set { _ReturnDate = value; }
        }
        public DateTime SummaryReturnDate
        {
            get { return _SummaryReturnDate; }
            set { _SummaryReturnDate = value; }
        }
        public bool ReturnFlightFlag
        {
            get { return _ReturnFlightFlag; }
            set { _ReturnFlightFlag = value; }
        }
        [XmlIgnore]
        public LowCostSite.FlightsSearchService.Pricing Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        [XmlIgnore]
        public LowCostSite.FlightsSearchService.SegmentLegContainer FlightSegments
        {
            get { return _FlightSegments; }
            set { _FlightSegments = value; }
        }
        public decimal HandlingFee
        {
            get { return _HandlingFee; }
            set { _HandlingFee = value; }
        }
        [XmlIgnore]
        public LowCostSite.FlightsSearchService.Pricing OutWardFlights
        {
            get { return _OutWardFlights; }
            set { _OutWardFlights = value; }
        }
        [XmlIgnore]
        public LowCostSite.FlightsSearchService.SegmentLegContainer OutwardFlightsSegments
        {
            get { return _OutwardFlightsSegments; }
            set { _OutwardFlightsSegments = value; }
        }

        #endregion
        #region PrivateProp
        private Guid _LoginToken;
        private Guid _SearchToken;
        private string _OriginCity;
        private string _DestinationCity;
        private DateTime _SummaryOutwardDate;
        private DateTime _ReturnDate;
        private DateTime _SummaryReturnDate;
        private bool _ReturnFlightFlag;
        [XmlIgnore]
        private LowCostSite.FlightsSearchService.Pricing _Price;
        [XmlIgnore]
        private LowCostSite.FlightsSearchService.SegmentLegContainer _FlightSegments;
        private decimal _HandlingFee;
        [XmlIgnore]
        private LowCostSite.FlightsSearchService.Pricing _OutWardFlights;
        [XmlIgnore]
        private LowCostSite.FlightsSearchService.SegmentLegContainer _OutwardFlightsSegments;


        #endregion

        public void LoadFromObject(SelectedItinerary oSelectedItinerary)
        {
            _LoginToken = oSelectedItinerary.LoginToken;
            _SearchToken = oSelectedItinerary.SearchToken;
            _OriginCity = oSelectedItinerary.SelectedFlights.OriginCity;
            _DestinationCity = oSelectedItinerary.SelectedFlights.DestinationCity;
            _SummaryOutwardDate = oSelectedItinerary.SelectedFlights.Summary.OutwardDate;
            _ReturnDate = oSelectedItinerary.SelectedFlights.ReturnDate;
            _SummaryReturnDate = oSelectedItinerary.SelectedFlights.Summary.ReturnDate;
            FlightsSearchService.FlightContainer returnFlightContainer = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().ReturnFlights;
            ReturnFlightFlag = false;
            if (returnFlightContainer != null && returnFlightContainer.DataSource != null && returnFlightContainer.DataSource.FirstOrDefault() != null)
            {
                ReturnFlightFlag = true;
                // get the price object
                _Price = returnFlightContainer.DataSource.FirstOrDefault().Price;
                _FlightSegments = returnFlightContainer.DataSource.First().FlightSegments;
            }
            _HandlingFee = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().Price.HandlingFee;
            _OutWardFlights = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().OutwardFlights.DataSource.First().Price;
            _OutwardFlightsSegments = oSelectedItinerary.SelectedFlights.GroupList.DataSource.First().OutwardFlights.DataSource.First().FlightSegments;
        }
    }
}
