﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BL_LowCost;
using DL_Generic;
using Generic;

namespace LowCostSite
{
    /// <summary>
    /// Summary description for ConfigurationZone
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    //[System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ConfigurationZone : System.Web.Services.WebService
    {

        [WebMethod]
        public bool UpdateLocalTimesForDestinations(string GoogleApiKey)
        {
            try
            {
                DestinationPageContainer oDestinationPageContainer = DestinationPageContainer.SelectAllDestinationPages(1, true);
                foreach (var dest in oDestinationPageContainer.DataSource)
                {
                    dest.Time = Generic.GoogleApi.GetLocalTimeDiffrenceCityName(Extensions.GetCityName(dest.DestinationIataCode_UI), GoogleApiKey);
                    dest.Action(DB_Actions.Update);
                }
                return true;
            }
            catch (Exception)
            {
                // couldn't update..

                // log..
                return false;
            }
        }
    }
}
