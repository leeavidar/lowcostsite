﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerManagerProject
{
   public class ConfigurationHandler
    {
        public static string LogPath { get { return System.Configuration.ConfigurationManager.AppSettings["LogPath"]; } }
        public static string LogSpecificPath { get { return System.Configuration.ConfigurationManager.AppSettings["LogSpecificPath"]; } }
        public static string LogTimeZoneUpdatePath { get { return System.Configuration.ConfigurationManager.AppSettings["LogTimeZoneUpdatePath"]; } }
        public static string LogPathGeneric { get { return System.Configuration.ConfigurationManager.AppSettings["LogPathGeneric"]; } }
        public static string ProjectName { get { return System.Configuration.ConfigurationManager.AppSettings["ProjectName"]; } }
       
    }
}
